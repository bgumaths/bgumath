#!rails r
# frozen_string_literal: true

require 'optparse'

VERSION = '1.0.0'

res = {}
rel = Impression.order(created_at: :desc)
                .where.not(action_name: %w[new edit])
                .where("params->>'_method' IS NULL")

OptionParser.new do |opts|
  opts.on('-c', '--count=COUNT', Integer, 'count up to COUNT newest event') do |c|
    rel = rel.limit(c)
  end
  opts.on('-s', '--start=DAYS', Integer, 'ignore entries more than DAYS days back') do |d|
    rel = rel.where('created_at >= ?', d.days.ago)
  end
  opts.on('-f', '--finish=DAYS', Integer, 'ignore entries less than DAYS days back') do |d|
    rel = rel.where('created_at <= ?', d.days.ago)
  end

  opts.separator ''
  opts.on_tail('-e', '--environment=NAME', 'specify rails environment ("development" by default)')

  opts.on_tail('-H', 'Show this message') do
    puts opts
    exit
  end
  # Another typical switch to print the version.
  opts.on_tail('--version', 'Show version') do
    puts VERSION
    exit
  end
end.parse!

#puts rel.to_sql

rel.each do |x|
  id = x.params.values.select { |v| v.is_a?(String) }.join('/') +
       "/#{x.controller_name}/#{x.action_name}/#{x.impressionable_id.presence}"
  res[id] = (res[id] || 0) + 1
end

res.each do |k, v|
  puts "#{v}\t#{k}\n"
end

