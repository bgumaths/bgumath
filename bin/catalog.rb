#!rails r
# frozen_string_literal: true

require 'course_catalog'

puts CourseCatalog.course_details(ARGV[0]).to_yaml

