#!/bin/env ruby
# frozen_string_literal: true

require 'optparse'
require 'rest-client'
require 'json'
require 'yaml'
require 'uri'

CONF_FILE = "#{ENV['HOME']}/.bgumath.yml"

@config = begin
            YAML.load_file(CONF_FILE)
          rescue Errno::ENOENT
            {}
          end

BASE = ENV['BGUMATH'] || @config['base'] || 'https://www.math.bgu.ac.il'
TOKEN = begin
          @config['tokens'][BASE]
        rescue StandardError
          nil
        end

API = "#{BASE}/api/v1"

options = { timeout: nil }

OptionParser.new do |opts|
  opts.banner = <<~EOF
    Usage: bgumath <action> <resources> [<id>]
    Input data read from STDIN. To create an auth token:
      # echo '{"auth":{"email":"dude","password":"foobar"}}' | bgumath create user_token
    Then add the result to #{CONF_FILE}
  EOF

  opts.on('-h', '--help', 'Prints this help') do
    puts opts
    exit
  end

  opts.on('-u', '--url', 'Prints the URL for the action') do
    options[:url] = true
  end

  opts.on('-m', '--method', 'Prints the http method (GET, POST,...)') do
    options[:method] = true
  end

  opts.on('-d', '--debug', 'Add debugging info') do
    @debug = true
  end

  opts.on('-r', '--relations', 'Update relationships') do
    @relations = true
  end

  opts.on('-t', '--timeout INTEGER', 'Specify request timeout in seconds') do |t|
    options[:timeout] = t
  end
end.parse!

def debug(str)
  puts str if @debug
end

@action = ARGV.shift.to_sym
@resources = ARGV.shift

ACTION_METHOD = {
  index: :get,
  show: :get,
  create: :post,
  update: :patch,
  destroy: :delete,
}.freeze

@url = "#{API}/#{@resources}"

def has_id(action = @action)
  %i[show update destroy].include?(action)
end

def has_payload(action = @action)
  %i[create update create_rel].include?(action) || @relations
end

if has_id
  @id = ARGV.shift
end

if ARGV.count > 0
  @url = URI(@url)
  @url.query = URI.encode_www_form(ARGV.map { |x| x.split(/=/) })
  @url = @url.to_s
end

debug "Using url #{@url}"

@method = ACTION_METHOD[@action]

debug "Method: #{@method}"

if options[:url] || options[:method]
  out = options[:method] ? @method.to_s.upcase + ' ' : ''
  out += @url if options[:url]
  puts out
  exit
end

@headers = {
  accept: 'application/vnd.api+json',
  content_type: 'application/vnd.api+json',
}

@headers[:Authorization] = "Bearer #{TOKEN}" if TOKEN

debug "Auth token: #{@headers[:Authorization]}"

if @action == :index
  inp = begin
          ARGF.read
        rescue StandardError
          ''
        end
  if inp.present?
    params = JSON.parse(inp)
    @headers[:params] = params
    debug "Params: #{params}"
  end
end

@req_opts = { method: @method, url: @url, timeout: options[:timeout],
              headers: @headers, }

def reqst(payload = nil, url = nil)
  cur_req = @req_opts.clone
  if payload
    cur_req = cur_req.merge(payload: payload.to_json)
    debug "Payload:\n#{cur_req[:payload].to_json}"
  end
  cur_req[:url] += "/#{url}" if url
  debug "req opts:\n#{cur_req}"
  RestClient::Request.execute(cur_req)
rescue RestClient::ExceptionWithResponse => e
  e.response
end

if has_payload
  ARGF.each_line do |imp|
    @payload = JSON.parse(imp)
    unless @payload['auth'] || @payload['data']
      defp = { type: @resources }
      defp[:id] = @id if @id
      @payload = { data: defp.merge(@payload) }
    end
    if @relations
      @rels = @payload[:data]['relationships']
      @rels.each do |k, v|
        puts reqst(v, "/#{@payload[:data]['id']}/relationships/#{k}")
      end
    elsif @payload[:data]
      puts reqst(@payload, "/#{@payload[:data]['id']}")
    else
      puts reqst(@payload)
    end
  end
else
  puts reqst(nil, @id ? "/#{@id}" : nil)
end

