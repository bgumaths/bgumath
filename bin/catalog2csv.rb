#!rails r
# frozen_string_literal: true

require 'optparse'
require 'course_catalog'

@main_headers = %i[dept id hours points name ename]
@group_headers = [:lecturer]
@meeting_headers = %i[time place]

options = { full: false }

OptionParser.new do |opts|
  opts.banner = <<~EOF
    Usage: catalog2csv

    Dumps handbook catalog as csv

  EOF
  opts.on('-h', '--help', 'Prints this help') do
    puts opts
    exit
  end

  opts.on('-D', '--debug', 'Print debug info') do
    options[:debug] = true
    @debug = true
  end

  opts.on('-y', '--years STRING', 'Years to dump (current)') do |yy|
    options[:years] = yy
  end

  opts.on('-d', '--depts STRING', 'Departments to dump (math)') do |yy|
    options[:depts] = yy
  end

  opts.on('-l', '--levels STRING', 'Levels to dump (1/2, default: all)') do |yy|
    options[:levels] = yy
  end

  opts.on('-t', '--terms STRING', 'Terms to dump (fall/spring/1/2, default: all)') do |yy|
    options[:terms] = yy
  end

  opts.on('-c', '--campuses STRING', 'Campuses to dump (0)') do |yy|
    options[:campuses] = yy
  end

  opts.on('-f', '--full', 'include lecturer/classes info (false)') do
    options[:full] = true
  end
end.parse!

def log(msg)
  warn msg if @debug
end

def hours(st)
  mt = st.match(/(\d\d:\d\d) *- *(\d\d:\d\d)/)
  return 'UNKNOWN' unless mt

  (Time.zone.parse(mt[1]) - Time.zone.parse(mt[2])) / 3600
end

log("Options: #{options.inspect}")

@headers = options[:full] ? @main_headers + %i[year term group] + @group_headers + @meeting_headers + [:mhours] : @main_headers

log("Headers: #{@headers.inspect}")

CSV.instance($stdout, write_headers: true, force_quotes: true, col_sep: ',',
                      headers: @headers) do |csv|
  CourseCatalog.crawl(options) do |cc|
    if cc.is_a?(Exception)
      warn "Exception: #{cc}"
      next
    end
    warn "Errors: #{cc[:errors]}" if cc[:errors]
    vals = cc.values_at(*@main_headers)
    if options[:full]
      year = cc[:params][:rn_year].to_i
      term = cc[:params][:rn_semester].to_i
      year -= 1 if term == 1
      vals.push(year, term)
      if cc[:groups].blank?
        warn "  NO groups info for #{cc.inspect}"
        next
      end
      cc[:groups].values.each do |gg|
        next unless gg[:kind] == 'שעור'

        group_vals = vals + gg.values_at(:id, *@group_headers)
        gg[:meetings].each do |mm|
          csv << (group_vals + mm.values_at(*@meeting_headers) + [hours(mm[:time])])
        end
      end
    else
      csv << vals
    end
  end
end

$stdout

