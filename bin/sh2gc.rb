#!rails r
# frozen_string_literal: true

gc = GenericCourse.find_or_create_from_shnaton(ARGV.shift, advanced: false)
cc = Course.new
cc.generic_course = gc
cc.init_from_generic!
term = ARGV.shift
cc.term = term ? Term.find(term) : Term.default_no_exams
cc.init_from_shnaton!
cc.save!
#puts gc.to_h.to_yaml
puts cc.to_h.to_yaml

