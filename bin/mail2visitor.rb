#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
require 'mail'
require 'rest-client'
require 'json'

# login and password in .netrc
BASE_URL = 'https://www.math.bgu.ac.il/en'
#BASE_URL = "http://192.168.14.215:3000"
#BASE_URL = "http://localhost:3000"
MEMBERS_URL = "#{BASE_URL}/people/members.json"
POST_URL = "#{BASE_URL}/people/visitors.json"
ALLOWED_FROM = ['sshirly@bgu.ac.il'].freeze
ALLOWED_SUBJECTS = ['guest visitor'].freeze

mail = Mail.read_from_string(ARGF.read)

def authorized(mail)
  mail.from.count > 0 &&
    mail.from.reject { |x| ALLOWED_FROM.include?(x.downcase) }.empty? &&
    ALLOWED_SUBJECTS.include?(mail.subject.downcase)
end

def fix_date(d)
  Date.strptime(d, '%d.%m.%y').to_s
end

def visitor(mail)
  data = {}
  mail.body.decoded.split("\n").each do |l|
    if l =~ /^([A-Za-z ]+?) *: *(.*?) *$/
      data[$1.downcase.to_sym] = $2
    end
  end
  data.delete(:tel)
  data[:origin] ||= %i[university country].map { |x| data.delete(x) }.reject { |x| x.to_s.empty? }.join(', ')
  data[:centre] = !data[:center].nil? && !(data.delete(:center) =~ /^y/i).nil?
  raise "Unable to parse visitor name '#{data[:name]}'" unless data[:name] =~ /^((dr|prof|mr|ms)\.? +)?(.*) (\S+)/i

  data[:rank] ||= $2
  data[:first] ||= $3
  data[:last] ||= $4
  data.delete(:name)
  data[:duration] ||= {
    first: fix_date(data.delete(:'arrival date')),
    last: fix_date(data.delete(:'departure date')),
  }
  if data[:host]
    # if host has no whitespace, assume email
    if data[:host].match?(/\s/)
      words = data[:host].downcase.split
      resp = JSON.parse RestClient.get(MEMBERS_URL).body
      # discard deceased
      resp['objects'].pop
      res = []
      resp['objects'].each do |l|
        l.each do |r|
          res.push(r) if words.include?(r['last'].downcase)
        end
      end
      raise "Host '#{data[:host]}' not found" if res.count == 0
      raise "Host '#{data[:host]}' ambiguous, options: #{res}" if res.count > 1

      data[:host_ids] = [res[0]['id']]
    else
      data[:host_emails] = [data[:host]]
    end
    data.delete(:host)
  end
  data
end

#RestClient.add_before_execution_proc do |req, params|
#  req.each_header do |k,v|
#    puts "  #{k}: #{v}"
#  end
#end

resp = RestClient.post POST_URL, visitor: visitor(mail) if authorized(mail)
puts resp

