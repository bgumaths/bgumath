#!rails r
# frozen_string_literal: true

require 'nokogiri'
require 'down'

URL = 'https://www.math.bgu.ac.il/~amyekut/alg-geom-seminar/current.html'

doc = Nokogiri::HTML(down(URL)) or raise "Unable to open '#{URL}'"

rows = doc.css('table')[2].css('tr')
year = nil
seminar = Term.default.seminars.find('AGeNT')
rows.each do |row|
  item = {}
  rcells = row.css('td')
  cells = rcells.map { |x| x.text.tr("\n\t", ' ').strip.squeeze(' ') }
  dd = cells[0]
  item[:date] = begin
           Date.strptime(dd, '%e %b %Y')
                rescue ArgumentError
                  begin
                    Date.strptime(dd + " #{year}", '%e %b %Y')
                  rescue ArgumentError => e
                    warn "#{e}: #{dd}"
                    next
                  end
         end
  year = item[:date].year
  speaker = cells[1]
  next if speaker.empty?

  from = nil
  if speaker.sub!(/ *\((.*)\)/, '')
    from = $1
  end
  item[:speaker] = speaker
  item[:from] = from
  item[:title] = cells[2]
  abs = nil
  if item[:title].sub!(/ *\(abstract\)/, '')
    abs = rcells[2].css('a')[0]['href']
  end
  mm = seminar.meetings.find_or_create_by!(slug: item[:date])
  mm.update(item)
  if abs
    mm.remote_descfiles_urls = [abs]
    mm.save!
  end
  puts item.to_json
end

