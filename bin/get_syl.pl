#!/usr/bin/env perl

# $Id$

use utf8;
use 5.10.0;
use warnings;
use integer;
use English qw(-no_match_vars);
use IPC::System::Simple qw(system);
use autodie qw(:all);

BEGIN {
    our ($VERSION) = '$Revision$' =~ m{\$Revision: \s+ (\S+)}x; ## no critic
}

use Getopt::Long qw(:config gnu_getopt auto_help auto_version);
our %Opts = ();
GetOptions(\%Opts);

use LWP::UserAgent;
use HTML::TreeBuilder 5 -weak;

my $id = shift;
$id =~ s/\D//g;
my $url ="http://oldweb.cs.bgu.ac.il/academics/bgu_course/$id";

my $r = LWP::UserAgent->new(agent => 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0')->get($url); # sigh...

#warn $r->status_line;
#warn $r->content_type;
#warn $r->decoded_content(charset => 'ISO-8859-8');

my $t = HTML::TreeBuilder->new_from_content($r->decoded_content(charset => 'ISO-8859-8'));

#print $t->dump;
my @l = $t->look_down(_tag => 'td', sub { my $cc = ($_[0]->content_list)[0]; $cc and not ref $cc and $cc =~ /:/ });
my %s;
for my $i ( @l) {
    my $key = $i->as_text;
    $key =~ s/://g;
    #print "$key ---- ";
    $i = $i->parent until $i->attr('_tag') eq 'tr';
    $s{$key} = ($i->content_list)[0]->as_text;
}
#say "$_ - $s{$_}" foreach keys %s;
printf(";%s;%s;%s;;;;%s;%s\n", map { s/;/#/g;$_ } @s{'שם לועזי', 'סילבוס לועזי', 'מספר קורס', 'סילבוס', 'שם קורס'});

__DATA__

# start of POD

=head1 NAME

get_syl -

=head1 SYNOPSIS

    get_syl

=head1 OPTIONS

=over

=item --help,-?

Give a short usage message and exit with status 1

=item --version

Print a line with the program name and exit with status 0

=back

=head1 ARGUMENTS

=head1 DESCRIPTION

=head1 FILES

=head1 BUGS

=head1 SEE ALSO

=head1 AUTHOR

Moshe Kamensky  (E<lt>samvimes@fastmail.fmE<gt>) - Copyright (c) 2015

=head1 LICENSE

This program is free software. You may copy or 
redistribute it under the same terms as Perl itself.

=cut

