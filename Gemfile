# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }
ruby '>=3.2.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'bundler', '>=2.0.2'
gem 'rails', '>=7.1'

### Security
gem 'bcrypt'
gem 'scrypt', github: 'pbhogan/scrypt'
gem 'rpam-ruby19'
# authentication, roles and authorization
gem 'authlogic'
gem 'rolify'
gem 'pundit'
# allow redirecting http->https
gem 'open_uri_redirections'
# avoid open-uri security issues
gem 'down'

gem 'saml_idp'
# protect against brute-force and dos
# versions 5.3.0-1 require redis
gem 'rack-attack' #, ['!=5.3.0', '!=5.3.1']

# decorators
gem 'draper', '>= 3.0.0.pre1'

# controller logic
gem 'decent_exposure'

# duplicate records
gem 'amoeba'

# memoize
gem 'memoist'
gem 'paper_trail'
gem 'paper_trail-association_tracking'
# recursive sql
# https://github.com/take-five/activerecord-hierarchical_query/issues/33
#gem 'activerecord-hierarchical_query', github: 
#'walski/activerecord-hierarchical_query', branch: 'rails-6-1'
gem 'activerecord-hierarchical_query', '>= 1.3.0'

# get version 2, with ruby3 support
#gem 'que', github: 'greensync/que', branch: 'ruby3'
# now in que-rb master
#gem 'que', github: 'que-rb/que'
# now released
gem 'que'

# env variables
gem 'dotenv-rails'

## HTML generators
# nice forms
gem 'simple_form', '>= 3.4.0'
gem 'x-editable-rails'

# markdown
# we maintain our modified version in lib/
#gem 'pagedown-bootstrap-rails'
gem 'kramdown'

# graphviz
# https://github.com/glejeune/Ruby-Graphviz/issues/115
#gem 'ruby-graphviz'
gem 'ruby-graphviz', github: 'mkamensky/Ruby-Graphviz'

gem 'image_processing'

# for x-editable (css only)
gem 'bootstrap-datepicker-rails'

### Doc

group :doc do
  #gem 'sdoc'
  gem 'yard'
  gem 'yard-activerecord'
end

### API
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# For some reason even with Graphiti we need it here, otherwise we sometimes 
# get empty response (TODO)
gem 'jbuilder'
gem 'rack-cors', require: 'rack/cors'

gem 'graphiti'
gem 'rescue_registry'
# https://github.com/graphiti-api/graphiti-rails/issues/110
gem 'graphiti-rails', github: 'mkamensky/graphiti-rails'
#gem 'graphiti-rails'
#gem 'graphiti_errors', github: 'graphiti-api/graphiti_errors'
gem 'kaminari'
gem 'responders'

# cal feed
gem 'icalendar'
# latex, pdf
gem 'rails-latex'

# nice urls
# Version 5.4 broken
#gem 'friendly_id', github: 'norman/friendly_id'
gem 'friendly_id'

## DB utils
gem 'pg', '>=1.0'
gem 'smarter_csv', require: false
gem 'csv', require: false
gem 'google_drive', require: false
gem 'yaml_db', '>=0.6.0'
gem 'migration_data'
gem 'json'
gem 'activemodel-serializers-xml'
#gem 'active_model_serializers'

# time of day
gem 'tod'

## I18n
gem 'i18n'
gem 'http_accept_language'
gem 'rails-i18n'
gem 'trasto'
#gem 'hebrew_date'

# static pages
gem 'high_voltage'

# display dates nicely. Our version supports also time
gem 'time_will_tell', github: 'mkamensky/time_will_tell'

# cmdline tools
gem 'nokogiri'
gem 'mail', '~>2.7.1'
gem 'mail-gpg'
# https://github.com/codetriage/maildown/issues/53
# https://github.com/codetriage/maildown/issues/69
#gem 'maildown'
gem 'maildown', github: 'mkamensky/maildown'
gem 'rest-client'

gem 'exception_notification'

gem 'activerecord-session_store'

# misc
# fix url param encoding
gem 'rack-utf8_sanitizer'

# profiling
#gem 'stackprof'

# contradicts with better_errors
#gem 'awesome_print'
gem 'amazing_print'
gem 'rails_semantic_logger'

# caching
#gem 'lightly'
# use github version until a release later than 1.0.2 is made
gem 'filecache' #, github: 'leighmcculloch/filecache-ruby'

gem 'bootsnap', require: false

gem 'rails-pg-extras'

group :development do
  gem 'bundler-audit', require: false
  gem 'brakeman', require: false
  gem 'database_consistency', require: false
  #gem 'rails_time_travel'

  gem 'lefthook', require: false

  gem 'web-console'

  gem 'rails-erd', require: false
  # gchrome integration
  gem 'meta_request', require: false

  # better console io
  #gem 'highline'
  # web server
  #gem 'thin'
  gem 'puma'
  # automatically reload browser
  gem 'rack-livereload'
  gem 'guard', require: false
  gem 'guard-livereload', require: false
  gem 'ruby-lint', require: false
  gem 'rubocop', '>=1.8', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-md', require: false
  gem 'rubocop-performance', require: false
  gem 'rubocop-capybara', require: false
  gem 'rubocop-factory_bot', require: false
  gem 'rubocop-rspec_rails', require: false
  
  # update rubocop
  gem 'mry', require: false

  #gem 'habtm_generator', require: false

  # show which partials are used
  #gem 'view_source_map'

  # open email in the browser
  #gem 'letter_opener'

  # show page structure, edit template
  #gem 'xray-rails'

  
  gem 'capistrano-rails', require: false
  gem 'capistrano-passenger', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rbenv', require: false
  gem 'capistrano-rbenv-install', require: false
  gem 'capistrano-secrets-yml', require: false
  gem 'capistrano-postgresql', require: false
  gem 'sitemap_generator', require: false

  # https://github.com/net-ssh/net-ssh/issues/565
  gem 'ed25519', '>= 1.2'
  gem 'bcrypt_pbkdf', '>= 1.0'

  # uses sudo
  #gem 'capistrano-safe-deploy-to'
end

group :development, :staging do
  # provide debugger in browser. Don't include in :test or :production!
  # this does not work well with graphiti's error handling
  gem 'better_errors', require: false
  gem 'binding_of_caller'
  # this breaks cSP...
  #gem 'rvt'
end

group :development, :staging, :test do
  ## debugging
  gem 'debug'
end

group :development, :test do
  gem 'timecop'
  # Spring speeds up development by keeping your application running in the
  # background. Read more: https://github.com/rails/spring
  gem 'spring'

  ### Testing
  # rails 6 compat: https://github.com/rspec/rspec-rails/issues/2155
  %w[rspec-core rspec-expectations rspec-mocks rspec-rails 
     rspec-support].each do |lib|
    gem lib #, github: "rspec/#{lib}" #, tag: 'v4.0.0.beta2'
  end
  gem 'rubocop-rspec', require: false
  gem 'rubocop-faker', require: false
  # allow rspec-4
  gem 'graphiti_spec_helpers', github: 'mkamensky/graphiti_spec_helpers'

  gem 'rspec-console'

  gem 'ruby-saml'
end

group :production, :staging, :beta do
  gem 'passenger'
  gem 'dalli'
end

group :test do
  gem 'faker'
  #gem 'guard-rspec'
  gem 'launchy'
  gem 'database_cleaner'
  gem 'rails-controller-testing'
  gem 'pundit-matchers'
  gem 'shoulda-matchers'
  gem 'rspec-retry'
  # system specs
  gem 'capybara'
  gem 'selenium-devtools'
  #gem 'webdrivers'
  gem 'factory_bot_rails'
  gem 'factory_trace'
end

gem 'rexml', '>= 3.2.5'
gem 'mini_mime', '>=1.1.0'

#gem 'webpacker'
#gem 'shakapacker'
gem 'vite_rails'
gem 'inertia_rails', '>= 1.11'

gem 'js-routes', '>= 2.2', group: :development
gem 'net-smtp', '~> 0.3.1'

gem 'net-pop', '~> 0.1.1'

gem 'net-imap', '~> 0.2.3'

# TODO
gem 'base64', '~>0.1.1'

