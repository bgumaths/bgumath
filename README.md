# BGU Math dept web page

This is the code for the BGU math web page. For information on using the
webpage, please look under [about](https://www.math.bgu.ac.il/about)
on the page itself.

## Namespaces

There are four main namespaces. They apply to controllers and views, but not 
models.

### People

Involves all people in the department, including visitors, but not including 
users of the issues system (who are kept in a separate system)

### Research

Includes:

- Research groups
- Seminars (and seminar meetings)
- Events

### Teaching

Includes:

- Terms
- Courses (Generic and term-specific)
- The students' issues system (_MIR_)
- The handbook

### Admin

Administration of the website. Includes:

- Roles
- Templates
- Versioning
- Settings

## Components

### Authentication, Authorization and roles

User authentication is taken care of by the 
[authlogic](https://github.com/binarylogic/authlogic) gem. Configuration is 
in {BaseUser}, {User} and {UserSession} for regular users, and {IssuesUser}, 
{IssuesUserSession} for external users of the issues system.

## HOWTOs

### Adding a field `foo` to model `Bar`

A _trasto field_ is a field that has translations to the different locales. 
It has the form `foo_i18n` instead of `foo`, and is a hash, with keys the 
locales, and values the translations. Use the following steps to add either a 
regular field or a trasto field:

0. Add to the field to `spec/factories/bars.rb`
1. Add tests. For a trasto field, add `:foo` to the array returned by 
   `self.trasto_fields`. Check that the tests fail.
2. For a regular field, run something like `rails g migration add_foo_to_bars 
   foo:string`. For a trasto field, `rails g migration add_foo_to_bars 
   foo_i18n:jsonb:index`, and edit the migration to add `default: {}, null: 
   false`. Run the migration (`rake db:migrate`).
3. add `foo` to the `Bar` model if this is an association (and possibly `bar` 
   to `Foo` model). For a trasto field, add `translates :foo`. 
4. add `:foo` to `permitted_attributes` in `bar_policy.rb`. For a trasto 
   field, add `foo_i18n: I18n.available_locales`.
5. Add validations. For a trasto field, there are `trasto_presence` and 
   `trasto_weak_presence` to validate that all or some locales are non-blank.
6. add foo to `app/views/.../_form.html.erb`. For trasto, use `trasto_field`, 
   which creates components in each locale.
7. Possibly add `foo` to index and to `_bar`
8. Possibly modify `bar_decorator.rb`
9. Possibly modify mailers, printed versions, calendar entries, schema...

To change a field from a regular field to a trasto field, follow the first 
three steps above, but before running the migration, add something like the 
following to the migration (assuming originally `foo` was in English):

    def data
      Bar.find_each do |b|
        b.foo_i18n['en'] = b[:foo]
        b.save!
      end
    end

and run the migration. Then, add a migration to remove the original field:

    rails g migration remove_foo_from_bar foo:string

and run that as well. Continue with the other steps.

### Upgrading ruby

    rbenv update
    rbenv install <ver>
    rbenv local <ver>

Edit `Gemfile` and `DeployGems` to set the new version

    bundle update --gemfile=DeployGems
    bundle update

Edit ruby version in `.gitlab-ci.yml`

On the production machine, as gitlab-runner:

    rbenv install <ver>
    rbenv global <ver>
    gem install passenger
