# frozen_string_literal: true

namespace :que do
  desc 'print systemd unit file'
  task :print_unit do
    on roles(:all) do
      puts unit_content
    end
  end
  
  desc 'install systemd unit file'
  task :install do
    on roles(:app) do
      execute :mkdir, '-p', unit_path
      upload!(unit_content, unit_path(unit_name))
      systemctl 'daemon-reload', nil
      systemctl :enable
    end
  end

  desc 'uninstall systemd unit file'
  task :uninstall do
    on roles(:app) do
      systemctl :disable
      execute :rm, unit_path(unit_name)
    end
  end

  desc 'reload que service'
  task :reload do
    on roles(:app) do
      systemctl 'reload-or-restart', raise_on_non_zero_exit: false
    end
  end

  %i[stop start restart].each do |tsk|
    desc "#{tsk} que service"
    task tsk do
      on roles(:app) do
        systemctl tsk
      end
    end
  end

  def systemctl(cmd, file = fetch(:systemd_unit_file, 'que.service'), **_opts)
    execute(*(%w[systemctl --user] + [cmd, file].compact))
  end

  def unit_path(*args)
    File.join(remote_home, *%w[.config systemd user], *args)
  end

  def unit_name
    fetch(:systemd_unit_file, 'que.service')
  end

  def remote_home
    @remote_home ||= capture(:pwd)
  end

  def rbenv_path
    @rbenv_path ||= if (pt = fetch(:rbenv_path))
                      capture(:echo, pt)
                    else
                      "#{remote_home}/.rbenv"
                    end
  end

  def unit_content
    <<~EOF
      [Unit]
      Description=que for #{fetch(:application)} (#{fetch(:stage)})
      After=syslog.target network.target
      
      [Service]
      Type=simple
      Environment=RAILS_ENV=#{fetch(:rails_env)}
      WorkingDirectory=#{fetch(:deploy_to)}/current
      ExecStart=#{fetch(:bundler_path, "#{rbenv_path}/shims/bundle")} exec que
      
      RestartSec=1
      Restart=on-failure
      
      SyslogIdentifier=que
      
      [Install]
      WantedBy=default.target
    EOF
  end
end

