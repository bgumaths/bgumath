# frozen_string_literal: true

require 'kramdown/converter'
# don't escape url in href
# The following doesn't work, since math inside is still escaped
#ESCAPE_MAP['$'] = '$'
module Kramdown
  module Converter
    class Xelatex < Latex
      # the original escapes the url, which is wrong
      def convert_a(el, opts)
        url = el.attr['href']
        if url.start_with?('#')
          "\\hyperlink{#{url[1..]}}{#{inner(el, opts)}}"
        else
          "\\href{#{url}}{#{inner(el, opts)}}"
        end
      end

      # The following doesn't work, since math inside is still escaped
      #ESCAPE_MAP['$'] = '$'
    end

  end
end
