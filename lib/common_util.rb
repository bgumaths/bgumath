# frozen_string_literal: true

# common utilities for app and tools
module CommonUtil
  # month when the new academic year starts
  YEAR_START = 8

  def self.cur_year
    d = Date.current
    (d.month < YEAR_START) ? d.year : d.year + 1
  end

  # if it is the weekend, we show next week, by default
  def self.week_start(date =
                            Date.current + ((Date.current.wday > 4) ? 1.week : 0))
    date.beginning_of_day.beginning_of_week
  end

  def self.week_end(date =
                          Date.current + ((Date.current.wday > 4) ? 1.week : 0))
    date.beginning_of_day.end_of_week
  end

  def self.uri_mtime(uri)
    Time.zone.parse(RestClient.head(uri).headers[:last_modified])
  rescue StandardError
    nil
  end

  SCHOLARS_URI = URI('https://scholars.bgu.ac.il/individual/')

  def self.scholars_info(id)
    @scholars_info ||= {}
    @scholars_info[id] ||= JSON.parse(
      RestClient.get(SCHOLARS_URI.to_s + "#{id}/#{id}.jsonld").body,
    )
  rescue RestClient::NotFound, RestClient::Exceptions::ReadTimeout
    []
  end
end

