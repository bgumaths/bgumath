# frozen_string_literal: true

module CourseCatalog
  module V1
    def self.action
      '!sc.AnnualCoursesAdv'
    end

    def self.search_params(**args)
      level = args[:level] || (if args[:advanced].nil?
                                 ''
                               else
                                 (args[:advanced] ? 2 : 1)
                               end)
      term = args[:term] || ''
      term = CourseCatalog.term_num(term)
      year = args[:year]
      if year && term == 1
        year = year.to_i + 1
      end
      res = {
        lang: args[:lang] || 'en',
        on_course_ins: 0, # BGU
        on_course_department: args[:dept] || 201, # math
        on_year: year || CourseCatalog.cur_year,
        on_semester: term || '',
        on_course_degree_level: level,
        oc_course_name: '',
        oc_start_time: '',
        oc_end_time: '',
        oc_lecturer_first_name: '',
        oc_lecturer_last_name: '',
        on_campus: '',
        on_course: args[:course] || '', # course number
        on_credit_points: '',
        on_hours: '',
        step: 1,
        type: 'new',
        st: 'a', # ???
      }.merge(args[:raw] || {})
      if args[:debug]
        warn "Search params: #{res.inspect}"
      end
      res
    end

    # id: e.g., 201.1.6061, or 1.6061, or 6061.
    # term: eg fall2017, spring2018 or `{ rn_year: 2018, rn_semester: 2 }`
    #       fall2017 means 2018 in the terminology of the catalog, i.e., same
    #       as {rn_year: 2018, rn_semester: 1}
    # opts:
    #   - campus: 0
    #   - year
    #   - term fall/spring/summer or 1/2/3
    #   - details: false/true whether to fetch lecturer and classes details
    #     the default is true if the term argument is given, false otherwise
    #
    # So the term and year can be specified either as fall2017 for the second
    # argument, or as `term: 1, year: 2017` in opts. In the first case,
    # `details` defaults to true, in the second to false. If none is specified,
    # the catalog is searched, and the most recent version is chosen (and
    # details defaults to false)
    def self.course_params(id, term = nil, **opts)
      raise ArgumentError, 'Blank course id' unless id

      if id.respond_to?(:split)
        # id is the string id
        comp = id.split('.')
        raise ArgumentError, "Invalid course id '#{id}'" if comp.count > 3 || comp.count.zero?

        comp.unshift('1') if comp.count == 1 # undergrad by default
        comp.unshift('201') if comp.count == 2 # math dep. by default
        id = { rn_course_department: comp[0], rn_course_degree_level: comp[1],
               rn_course: comp[2], }
        id[:rn_course_ins] = opts[:campus] if opts[:campus]
      end
      year = termn = nil
      if term
        if term.respond_to?(:match) &&
           (md = term.match(/(fall|spring|summer)(\d+)/i))
          year = if (termn = CourseCatalog.term_num(md[1])) == 1
                   md[2].to_i + 1
                 else
                   md[2].to_i
                 end
          year += 2000 if year < 1000
          term = { rn_year: year, rn_semester: termn }
        else
          begin
            year = term[:rn_year].try(:to_i)
            termn = term[:rn_semester].try(:to_i)
            # following seems to have changed
            #if year
            #  year -= 1 if termn == 1
            #end
          rescue TypeError => e
            raise e.exception(e.to_s + ". Term: #{term.inspect}, #{term[:rn_year]}")
          end
        end
      end
      year = opts[:year] || year
      termn = CourseCatalog.term_num(opts[:term]) || termn
      unless year.present? && termn.present?
        begin
          sd = search_details(
            dept: id[:rn_course_department] || 201,
            level: id[:rn_course_degree_level] || 1,
            course: id[:rn_course],
            year: CourseCatalog.cur_year,
            debug: opts[:debug],
          )
          md = sd[0][2].match(/(\d*)-(\d*)/)
          year = md[1].to_i
          termn = md[2].to_i
          year -= 1 if termn == 1
        rescue StandardError
          year = CourseCatalog.cur_year
          termn = CourseCatalog.cur_term
        end
      end

      details = opts.key?(:details) ? opts[:details] : term.present?
      # defaults
      id = {
        rn_course_department: '201',
        rn_course_degree_level: '1',
        rn_course_ins: '0',
        rn_institution: 0,
        rn_course_details: '1', # WHAT IS THIS?
        type: 'new',
        step: details ? 3 : 2,
        rn_year: year,
        rn_semester: termn,
      }.merge(id).merge(opts[:raw] || {})
      warn "Course params: #{id.inspect}" if opts[:debug]
      id
    end

    def self.course_link(*)
      params = course_params(*)
      return nil unless params

      # revert year change for fall in link
      params[:rn_year] += 1 if params[:rn_semester] == 1
      puri = uri(
        params[:rn_year] ? '!sc.AnnualCourseSemester' : '!sc.AnnualSelctedCourseDetails',
      )
      puri.query = URI.encode_www_form(params)
      puri
    end

    @@keytrans = {
      'שם קורס' => :name,
      'מספר קורס' => :id,
      'נקודות זכות' => :points,
      'נק"ז' => :points,
      'שעות שבועיות' => :hours,
      'שעות' => :hours,
      'שם לועזי' => :ename,
      'סטטוס' => :status,
      'תקציר' => :syllabus,
      'תקציר לועזי' => :esyllabus,
      'ביבליוגרפיה' => :bib,
      'אופני הוראה' => :time_info,
      'קורסים קשורים' => :related,
      'מחלקת אם' => :dept,
      'עדכון אחרון בשנת' => :updated,
      'ציון מעבר' => :pass_grade,
      'חובת מעבר' => :pass_grade,
      'חובת קדם' => :prereq,
      'מטלות בקורס' => :assignments,
      'מוסד לימוד' => :institutions,
      'מוסדות לימוד' => :institutions,
      'פרטי הקורס בסמסטר' => :cur_term,

    }

    def self.parse_course(doc)
      res = {}
      tables = doc.css('table table')
      return res unless tables[5]

      tables[5].css('tr').each do |row|
        items = row.css('td').map(&:text)
        next unless items[2]

        key = CourseCatalog.deblank(items[2]).sub(':', '')
        next if /^[[:space:]]*$/.match?(key)

        key = @@keytrans[key] || key
        res[key] = CourseCatalog.deblank(items[0])
      end
      if (mm = (res[:time_info] || ''
               ).match(/(\d+)\.\d\d *שעות.*(\d+)\.\d\d *שעות/))
        res[:lectures] = mm[1]
        res[:exercises] = mm[2]
      end
      if (ll = (res[:related] || '').scan(/(\d\d\d\.\d\.\d\d\d\d).*?קורס קדם/m))
        res[:requires] = ll.flatten
      end
      return res unless tables[6] # groups

      res[:groups] = {}
      group = nil
      tables[6].css('tr').each do |row|
        items = row.css('td').map(&:text)
        kind = CourseCatalog.deblank(items[9])
        next unless kind

        if CLASS_KINDS.include?(kind)
          res[:groups][group[:id]] = group if group
          group = {
            lecturer: CourseCatalog.deblank(items[5]),
            id: CourseCatalog.deblank(items[7]),
            kind:,
            meetings: [],
          }
        elsif kind.present?
          next
        end
        next unless group

        group[:meetings].push(time: CourseCatalog.deblank(items[3]), place: CourseCatalog.deblank(items[1]))
      end
      res[:groups][group[:id]] = group if group
      return res unless tables[8] # exam details

      res[:exams] = {}
      group = nil
      tables[8].css('tr').each do |row|
        items = row.css('td').map(&:text)
        groupn = CourseCatalog.deblank(items[4])
        next if groupn == 'קבוצה' # header

        if group.blank?
          group = res[:exams][groupn || :unknown] = {}
        end
        id = CourseCatalog.deblank(items[3])
        group[id] = {
          place: CourseCatalog.deblank(items[1]),
          time: CourseCatalog.deblank(items[2]),
        }
      end
      res
    end

    def self.parse_search(doc)
      tables = doc.css('table table')
      tables[1].css('tr')[5..].map do |row|
        row.css('td').map { |x| CourseCatalog.deblank(x.text) }
      end
    end
  end
end

