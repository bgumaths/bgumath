# frozen_string_literal: true

module CourseCatalog
  module V2
    def self.action
      '!app.ann'
    end

    def self.search_params(**args)
      level = args[:level] || (if args[:advanced].nil?
                                 nil
                               else
                                 (args[:advanced] ? 2 : 1)
                               end)
      term = args[:term] || ''
      term = CourseCatalog.term_num(term)
      year = args[:year]
      if year && term == 1
        year = year.to_i + 1
      end
      res = {
        step: 998,
        lang: args[:lang],
        ex_institution: 0, # BGU
        ex_department: args[:dept] || 201, # math
        ex_year: year || CourseCatalog.cur_year,
        # must be empty and not null for the whole year
        ex_semester: term || '',
        ex_degree_level: level,
        ex_course: args[:course] || nil, # course number
      }.merge(args[:raw] || {})
      if args[:debug]
        warn "Search params: #{res.inspect}"
      end
      res
    end

    def self.course_params(id, term = nil, **opts)
      return nil unless id

      if id.respond_to?(:split)
        # id is the string id
        comp = id.split('.')
        return nil if comp.count > 3 || comp.count.zero?

        comp.unshift('1') if comp.count == 1 # undergrad by default
        comp.unshift('201') if comp.count == 2 # math dep. by default
        id = { ex_department: comp[0], ex_degree_level: comp[1],
               ex_course: comp[2], }
        id[:ex_institution] = opts[:campus] if opts[:campus]
      end
      year = termn = nil
      if term
        if term.respond_to?(:match) &&
           (md = term.match(/(fall|spring|summer)(\d+)/i))
          year = if (termn = CourseCatalog.term_num(md[1])) == 1
                   md[2].to_i + 1
                 else
                   md[2].to_i
                 end
          year += 2000 if year < 1000
        else
          begin
            year = (term[:rn_year] || term[:ex_year]).try(:to_i)
            termn = (term[:rn_semester] || term[:ex_semester]).try(:to_i)
            # following seems to have changed
            #if year
            #  year -= 1 if termn == 1
            #end
          rescue TypeError => e
            raise e.exception(e.to_s + ". Term: #{term.inspect}, #{term[:rn_year]}")
          end
        end
      end
      year = opts[:year] || year
      termn = CourseCatalog.term_num(opts[:term]) || termn
      unless year.present? && termn.present?
        begin
          sd = search_details(
            dept: id[:ex_department] || 201,
            level: id[:ex_degree_level] || 1,
            course: id[:ex_course],
            year: CourseCatalog.cur_year,
            debug: opts[:debug],
          )
          md = sd[0][2].match(/(\d*)-(\d*)/)
          year = md[1].to_i
          termn = md[2].to_i
          year -= 1 if termn == 1
        rescue StandardError
          year = CourseCatalog.cur_year
          termn = CourseCatalog.cur_term
        end
      end

      #details = opts.key?(:details) ? opts[:details] : term.present?
      # defaults
      id = {
        step: 999,
        ex_department: '201',
        ex_degree_level: '1',
        ex_institution: 0,
        ex_year: year,
        ex_semester: termn,
      }.merge(id).merge(opts[:raw] || {})
      warn "Course params: #{id.inspect}" if opts[:debug]
      id
    end

    @@keytrans = {
      'שם הקורס' => :name,
      'מספר קורס' => :id,
      'אתר' => :web,
      'נקודות זכות' => :points,
      'נק"ז' => :points,
      'שעות שבועיות' => :hours,
      'שעות' => :hours,
      'שם לועזי' => :ename,
      'סטטוס' => :status,
      'תקציר' => :syllabus,
      'תקציר לועזי' => :esyllabus,
      'ביבליוגרפיה' => :bib,
      'אופני הוראה' => :time_info,
      'קורסים קשורים' => :related,
      'מחלקת אם' => :dept,
      'עדכון אחרון בשנת' => :updated,
      'ציון מעבר' => :pass_grade,
      'חובת מעבר' => :pass_grade,
      'חובת קדם' => :prereq,
      'מטלות בקורס' => :assignments,
      'מוסד' => :institutions,
      'מוסד לימוד' => :institutions,
      'מוסדות לימוד' => :institutions,
      'פרטי הקורס בסמסטר' => :cur_term,
      'קובץ סילבוס' => nil,
      'פתוח למחלקות' => :open_for,
      'מאפיינים נוספים' => :extra,
    }.freeze

    def self.parse_course(doc)
      res = {}
      tables = doc.css('table table')
      return res unless tables[0]

      tables[0].css('li').each do |row|
        row.search('.offscreen').each(&:remove)
        key = row.css('.key').text
        key = CourseCatalog.deblank(key).sub(':', '')
        #next if /^[[:space:]]*$/.match?(key)

        key = @@keytrans[key] if @@keytrans.key?(key)
        next unless key

        res[key] = CourseCatalog.deblank(row.css('.val').to_a.join(' '))
      end
      if (mm = (res[:time_info] || '').match(
        /שעור[[:space:]]*-[[:space:]]*(\d+\.\d\d)[[:space:]]*שעות/,
      ))
        res[:lectures] = mm[1]
      end
      if (mm = (res[:time_info] || '').match(
        /תרגיל[[:space:]]*-[[:space:]]*(\d+\.\d\d)[[:space:]]*שעות/,
      ))
        res[:exercises] = mm[1]
      end
      if (ll = (res[:related] || '').scan(/(\d\d\d\.\d\.\d\d\d\d).*?קורס קדם/m))
        res[:requires] = ll.flatten.uniq
      end
      return res unless tables[2] # groups

      res[:groups] = {}
      group = nil
      tables[2].css('tr').each do |row|
        items = row.css('td')
        meeting = nil
        if items.length == 5
          kind = CourseCatalog.deblank(items[1].text)
          next unless kind

          if CLASS_KINDS.include?(kind)
            res[:groups][group[:id]] = group if group
            group = {
              lecturer: CourseCatalog.deblank(items[2].text),
              id: CourseCatalog.deblank(items[0].text),
              kind:,
              meetings: [],
            }
          elsif kind.present?
            next
          end
          meeting = items[3]
        else
          meeting = items[0]
        end
        next unless group

        mt = meeting.text
        next unless (mm = mt.match(
          /^זמני לימוד:[[:space:]]*(.*)מקום לימוד:[[:space:]]*(.*)אופן לימוד:[[:space:]](.*)$/,
        ))

        group[:meetings].push(time: mm[1], place: mm[2], method: mm[3])
      end
      res[:groups][group[:id]] = group if group
      return res unless tables[3] # exam details

      res[:exams] = {}
      group = nil
      tables[3].css('tr').each do |row|
        items = row.css('td').map(&:text)
        grpn = CourseCatalog.deblank(items[0])

        group = res[:exams][grpn] = {} if grpn.present?
        next unless group

        id = CourseCatalog.deblank(items[1])
        group[id] = {
          place: CourseCatalog.deblank(items[3]),
          time: CourseCatalog.deblank(items[2]),
        }
      end
      res
    end

    def self.parse_search(doc)
      tables = doc.css('table table')
      ini = ['-']
      tables[0].css('tr')[4..-2].map do |row|
        # TODO
        ini +
          row.css('td').map { |x| CourseCatalog.deblank(x.text) }.reverse
      end.select { |x| /\d\d\d\.\d\.\d\d\d\d/.match?(x[-1]) }
    end
  end
end

