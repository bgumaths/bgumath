# frozen_string_literal: true

class Bgu::User
  # env vars might be in .env
  AUTH_URL = ENV['BGU_AUTH_SERVER'] || 'https://shizafon.bgu.ac.il:89' # 'https://apps4cloud.bgu.ac.il:81'
  AUTH_ENDPOINT = ENV['BGU_AUTH_ENDPOINT'] || '/api/validateUserWithID' # '/api/Auth'

  attr_accessor :login, :id, :valid, :details, :error, :type, :opts

  def initialize(login, id, **opts)
    @valid = {}
    @login = login
    @id = id
    @opts = opts
    @faraday = opts[:faraday] || {}
  end

  def client
    require 'faraday'
    @client ||= Faraday.new(url: AUTH_URL, **@faraday)
  end

  def auth!(pass)
    resp = JSON.parse(
      client.post(
        AUTH_ENDPOINT, UserName: login, Password: pass, Id: id
      ).body
    )
    # in some versions, this will itself be a string
    #resp = JSON.parse(resp.sub(/,}$/, '}')) if resp.respond_to?(:sub)
    # Following is a temporary fix until Dror replies with proper
    valid[pass] = resp.include?('True') ? self : false
    return valid[pass]
    resp = JSON.parse(resp) if resp.respond_to?(:sub)
    valid[pass] = 
      if resp.delete('AuthResultsDetails')['Authenticate']
             .casecmp('true').zero?
        self
      else
        false
      end
    if valid[pass]
      resp.each do |k, v|
        next unless k =~ /^(.*)Details$/

        @type = $1
        @details = v
        break
      end
    else
      @error = resp['ErrorMessage']
    end
    valid[pass]
  rescue Faraday::Error => e
    @error = e
    false
  end

  def auth?(pass)
    valid[pass].nil? ? auth!(pass) : valid[pass]
  end

  def respond_to_missing?(method, *)
    details&.key?(method.to_s.camelcase) || super
  end

  def method_missing(method, *)
    meth = method.to_s.camelcase
    details&.key?(meth) ? details[meth] : super
  rescue NoMethodError
    nil
  end
end

