# frozen_string_literal: true

module Bgu
  class Pure
    PURE_URI = URI('https://cris.bgu.ac.il/ws/api/524/')

    attr_accessor :args, :params, :headers, :error

    def initialize(key: Rails.application.credentials.pure[:key], faraday: {})
      @params = { apiKey: key }
      @headers = { Accept: 'application/json' }
      @args = { params: @params, headers: @headers }.deep_merge(faraday)
    end

    def client(**opts)
      require 'faraday'
      @client ||= Faraday.new(url: PURE_URI, **@args.deep_merge(opts)) do |f|
        f.request :json
        f.response :json
        f.response :raise_error
      end
    end

    def get_person(id)
      resp = client.get("persons/#{id}")
      resp&.body
      #JSON.parse(resp.body)
    rescue Faraday::Error => e
      @error = e.response
      false
    end

    def search_person(src)
      resp = client.get('persons', q: src)
      resp.body['items']
    rescue Faraday::Error => e
      @error = e.response
      false
    end

    class ApiError < StandardError
      attr_accessor :resp

      def initialize(resp)
        @resp = resp
        super(resp.try(:[], :body))
      end
    end

    class Person
      attr_accessor :pure

      def initialize(id = nil, search: nil)
        pr = Bgu::Pure.new
        @pure = id ? pr.get_person(id) : pr.search_person(search)[0]
        raise Bgu::Pure::ApiError, pr.error unless @pure
      end

      def first
        name['firstName']
      end

      def last
        name['lastName']
      end

      SCOPUS_TYPE = 4970
      WOS_TYPE = 4972

      def ids
        pure['ids'] || []
      end

      def web_id(type)
        ids
          &.find { |id| id.dig('type', 'pureId') == type }
          &.dig('value', 'value')
      end

      def scopus
        web_id(SCOPUS_TYPE)
      end

      def wos
        web_id(WOS_TYPE)
      end

      def respond_to_missing?(method, *)
        @pure.key?(method.to_s) || super
      end

      def method_missing(method, *)
        meth = method.to_s
        pure&.key?(meth) ? pure[meth] : super
      rescue NoMethodError
        nil
      end
    end
  end
end

