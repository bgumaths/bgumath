# frozen_string_literal: true

module StiPreload
  extend ActiveSupport::Concern

  included do
    cattr_accessor :preloaded, instance_accessor: false
  end

  class_methods do
    def type_condition(...)
      unless @_sti_preloaded
        types_in_db = base_class.unscoped.select(inheritance_column).distinct.pluck(inheritance_column).compact
        @_sti_preloaded = types_in_db.map(&:safe_constantize).all?
      end
      super
    end
  end
end

