require "rails/generators"
require "rails/generators/rails/resource/resource_generator"
require "rails/generators/rails/controller/controller_generator"
require "rails/generators/rails/scaffold/scaffold_generator"
require "rails/generators/rails/scaffold_controller/scaffold_controller_generator"

module Rails
  module Generators
    class ControllerGenerator
      [:policy, :decorator].each do |hook|
        hook_for hook, type: :boolean, default: true do |generator|
          args = @_initializer[0]
          args[0] = name.classify.demodulize
          invoke generator, args
        end
      end
    end

    class ResourceGenerator
      def initialize(*args)
        super
        unless options[:model_name]
          self.name = name.classify.demodulize
          assign_names!(name)
        end
      end

      hook_for :test_framework, as: :model do |test|
        args = @_initializer[0]
        args[0] = name
        invoke test, args
      end

      hook_for :resource_route, required: true do |route|
        invoke route, [ controller_name ]
      end
    end

    class ScaffoldControllerGenerator
      [:policy, :decorator].each do |hook|
        hook_for hook, type: :boolean, default: true do |generator|
          args = @_initializer[0]
          args[0] = name.classify.demodulize
          invoke generator, args
        end
      end

      hook_for :template_engine, as: :scaffold do |eng|
        invoke eng, @_initializer[0] unless options.api?
      end

    end

    class ScaffoldGenerator
      hook_for :scaffold_controller, required: true do |controller|
        args = @_initializer[0]
        args[0] = controller_name.singularize
        invoke controller, args
      end
    end
  end
end

require "generators/factory_bot/model/model_generator"

module FactoryBot
  module Generators
    class ModelGenerator < Base
      def factory_attributes
        attributes.map do |attribute|
          if attribute.type == :trasto
            "#{attribute.name}_i18n { {en: Faker::Company.catch_phrase, he: Faker::Company.catch_phrase } }"
          else
            "#{attribute.name} { #{attribute.default.inspect} }"
          end
        end.join("\n")
      end
    end
  end
end

require 'rails/generators/erb/scaffold/scaffold_generator'

module Erb
  module Generators
    class ScaffoldGenerator
      def available_views
        %w(index show _form)
      end
    end
  end
end


