# frozen_string_literal: true

require "rails/generators/active_record/model/model_generator"

class Rails::ModelGenerator < ActiveRecord::Generators::ModelGenerator

  def create_migration_file
    old = attributes.deep_dup
    trasto_attributes.each do |attr|
      if attr.name == name_field_name
        attributes.push(Rails::Generators::GeneratedAttribute.new(
          'slug', :string, 'uniq'))
      end
      attr.attr_options[:default] = {}
      attr.attr_options[:null] = false
      attr.name = "#{attr.name}_i18n"
      attr.type = :jsonb
    end
    ret = super
    self.attributes = old.deep_dup
    ret
  end

  protected
  # TODO: this is private...
  def parent_class_name
    options[:parent] || 'Base'
  end

  def self.name_field_name
    'name'
  end

  def name_field_name
    self.class.name_field_name
  end

  def trasto_attributes
    attributes.select{|a| a.type == :trasto }
  end


end
