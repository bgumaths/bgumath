module Rails
  module Generators
    class PolicyGenerator < NamedBase
      source_root File.expand_path('../templates', __FILE__)

      argument :attributes, type: :array, default: [], banner: "field[:type][:index] field[:type][:index]" 
      check_class_collision suffix: 'Policy'

      def create_policy_file
        template 'policy.rb', 
          File.join('app/policies', class_path, "#{file_name}_policy.rb")
      end

      def permitted_attr
        hash = {}
        res = []
        attributes.each do |attr|
          res.push(":#{attr.name}#{attr.reference? ? '_id' : ''}")
          hash["#{attr.name}_i18n"] = 'I18n.available_locales' if attr.type == :trasto
          res.push(':slug') if attr.name.to_s == 'name'
        end

        res + hash.map{|k,v| "#{k}: #{v}"}
      end
          
      hook_for :test_framework
    end
  end

end
