<%- module_namespacing do -%>
class <%= class_name %>Decorator < BaseDecorator
  <% attributes = shell.base.send(:attributes) rescue [] -%>

<% attributes.select(&:reference?).each do |attr| %>
  decorates_association :<%= attr.name %>
<% end %>

  def self.namespace
    '<%= (shell.base.send(:controller_name) rescue name).deconstantize.underscore %>'
  end

<% if attr = attributes.find{|a| a.name.to_sym == :name } -%>
  def to_s(loc = I18n.locale)
    name_i18n[loc] || name
  end
<% end -%>
end
<% end -%>
