<%- module_namespacing do -%>
class <%= class_name %>Policy < ApplicationPolicy
  def permitted_attributes
    [<%= permitted_attr.join(', ') %>]
  end
end
<% end -%>

