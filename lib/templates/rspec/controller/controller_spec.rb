require 'controllers/shared_examples'

<% module_namespacing do -%>
RSpec.describe <%= class_name %>Controller, <%= type_metatag(:controller) %> do

  it_behaves_like 'a regular controller'
end
<% end -%>
