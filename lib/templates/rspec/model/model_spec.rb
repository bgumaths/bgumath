require 'rails_helper'
require 'models/shared_examples'

<% module_namespacing do -%>
RSpec.describe <%= class_name %>, <%= type_metatag(:model) %> do
  def self.trasto_fields
    [<%= attributes.select{|a| a.type == :trasto}.map{|a| ":#{a.name}"}.join(', ') -%>]
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'

  describe 'Validations' do
  <% if attributes.find{|a| a.name == 'name'} -%>
    test_trasto_presence :name_i18n
  <% end -%>
  end
end

<% end -%>

