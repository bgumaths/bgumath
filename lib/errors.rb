# frozen_string_literal: true

module Errors
  class NotAuthenticated < StandardError
  end

  class MissingAuthorizationToken < Errors::NotAuthenticated
    def message
      'No Authorization token provided.'
    end
  end

  class InvalidAuthorizationToken < Errors::NotAuthenticated
    def message
      'Access is denied due to invalid token.'
    end
  end

  class Bgumath::NoIssuesRespondersError < StandardError
    def message
      I18n.t('errors.messages.issues_noresponders', locale: :en)
    end
  end

  class NotAuthorized < StandardError
    def message
      'You do not have permission to perform that action.'
    end
  end

  class UnwritableAttribute < NotAuthorized
    attr_reader :errors

    def initialize(errors)
      super
      @errors = errors
    end

    def message
      attrs = errors.details.keys.map do |x|
        x.to_s.sub(/^data\.attributes\./, '')
      end.sort
      I18n.t('errors.messages.attr_unwritable',
             attrs: attrs.join(', '))
    end
  end

  class UnpermittedParameters < NotAuthorized
    attr_reader :exc

    def initialize(exc)
      super
      @exc = exc
    end

    def message
      I18n.t('errors.messages.attr_unwritable',
             attrs: exc.params.sort.join(', '))
    end
  end
end

