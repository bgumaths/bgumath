# frozen_string_literal: true

# this is needed to maintain compatibility between json serialization and
# postgre range serialization
class Range
  def as_json(_options = nil)
    "[#{first},#{last}" + (exclude_end? ? ')' : ']')
  end
end

