# frozen_string_literal: true

class ResearchProfile::Person
  SOURCES = %i[orcid arxiv publons scopus gs].freeze

  attr_accessor(*(SOURCES.map { |x| :"#{x}_id" }))

  def initialize(**opts)
    SOURCES.each do |src|
      instance_variable_set(:"@#{src}_id", opts[src]) if opts.key?(src)
    end
  end
end

