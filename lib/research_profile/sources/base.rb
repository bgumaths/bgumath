# frozen_string_literal: true

## frozen_string_literal: true

require 'faraday'

class ResearchProfile::Sources::Base
  def client_params
    {
      url: self.class.base_url,
    }
  end

  def client
    @client ||= Faraday.new(client_params)
  end
end

