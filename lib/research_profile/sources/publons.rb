# frozen_string_literal: true

require 'pry'

class ResearchProfile::Sources::Publons < Base
  def self.base_url
    'https://publons.com/api/v2/'
  end

  # https://publons.com/api/v2/
  def client_params
    super.deep_merge(
      headers: {
        'Content-Type' => 'application/json',
        'Authorization' => 'Token 7a2fac91aeac4abcc2c786326e3a79f0a57b2445',
      }
    )
  end

  def person(id)
    JSON.parse(client.get("academic/#{id}/").body)
  end

  def self.person(*)
    new.person(*)
  end
end

