# frozen_string_literal: true

desc 'Generate all docs'
task docs: %w[doc/mir.md yard]

desc 'Generate MIR docs'
file 'doc/mir.md' => %w[spec/system/issues_spec.rb 
                        spec/policies/issue_policy_spec.rb
                        spec/mailers/teaching/issue_mailer_spec.rb
                        spec/mailers/teaching/issues_user_mailer_spec.rb] do |tt|
  puts "Generating '#{tt.name}' from #{tt.prerequisites}..."
  out = File.open(tt.name, 'w')
  out.puts <<~EOF
    # @title The Issues System (MIR)
    # @markup markdown
   
    {::comment}
    This file is generated by 'rake #{tt.name}' from relevant spec files,
    please make any changes needed there. Modifications here will be
    destroyed.
    {:/comment}

    # The Issues System (MIR)
    
    This is a description of MIR, the issues response system used by the 
    department to manage issues that come up between students and lecturers 
    in the courses offered by the department.

  EOF
  tt.prerequisites.each do |sp|
    File.foreach(sp) do |ll|
      next unless ll.gsub!(/^ *# /, '')

      out.puts ll
    end
    out.puts ''
  end
  puts '  ...done!'
end

