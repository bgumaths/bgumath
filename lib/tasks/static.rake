# frozen_string_literal: true

SKIP_ACTIONS = %w[template new edit create update destroy gsheets_update].freeze

namespace :static do
  desc 'Generate static route data'
  task routes: :environment do
    routes = {}
    Rails.application.routes.routes.each do |rr|
      route = ActionDispatch::Routing::RouteWrapper.new(rr)
      next if route.engine?
      next if SKIP_ACTIONS.include?(route.action)

      path = route.path.gsub(/\([^()]*\)/, '')
      next if route.action == 'show' && route.controller != 'pages' && route.path.include?(':id')
      next if path.start_with?('/api')
      next if route.name.start_with?('rails_')

      name = route.name
      next if name.blank?

      name.gsub!(/(people|research|teaching|internal|community)_/, '')
      name = route.name if name.blank? || routes[name]
      routes[name] = path
    end
    puts routes.to_yaml
  end

  desc 'Generate static settings'
  task settings: :environment do
    settings = Setting.v_public.select(%i[var value]).to_h do |ss| 
      [ss.var, ss.value]
    end
    settings['root_url'] = 'https://www.math/bgu.ac.il'
    puts settings.to_yaml
  end

  FileList['content/**/*.md'].each do |md|
    target = md.ext('html').gsub(/^content/, 'public/content')
    file target => md do |ff|
      puts "Generating #{ff.name}"
      FileUtils.mkdir_p(ff.name.pathmap('%d'))
      File.write(ff.name,
                 Kramdown::Document.new(File.read(ff.prerequisites[0])).to_html)
    end
    manifest = "#{target.pathmap('%d')}/manifest.json" 
    file manifest => target do |f|
      puts "Generating #{f.name}"
      File.write(f.name,
                 f.prerequisites.map do |pp| 
                   {
                     title: pp.pathmap('%n').tr('-', ' '),
                     route: pp.gsub(%r{^public/content}, ''),
                   }
                 end.to_json)
    end
    desc 'Generate static html and manifests from markdown content'
    task content: manifest
  end

  
end

