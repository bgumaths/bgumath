# frozen_string_literal: true

namespace :mir do
  desc 'Print mir health status'
  task health: :environment do
    puts Admin::MirMailer.health.text_part.body
  end

  desc 'Email mir health status'
  task health_mail: :environment do
    Admin::MirMailer.health.deliver_now
  end
end

