# frozen_string_literal: true

def docu(msg)
  warn msg
  puts "# #{msg}"
end

def putsh(cmd, doc = nil)
  docu doc if doc
  puts "echo '#{cmd.gsub("'", %q('"'"'))}'"
  puts cmd
end

def assoc_msg(join, assoc, scope: false)
  " Please add the line
  has_many :#{join}, dependent: :destroy
and change the line
  has_and_belongs_to_many :#{assoc} ...
to
  has_many :#{assoc}, through: :#{join} ...
" + (if scope
       "
Note that the has_and_belongs_to_many statement includes a scope.
It needs to be moved to the #{assoc} field in the join model 
#{join.to_s.classify}
"
     else
       "\n"
     end)
end

def fix_join_model(jcls, attr, model_name)
  @jcls[jcls] = {}
  return unless attr.classify != model_name && @jcls[jcls][attr].blank?

  putsh %[ruby -i -p -e '$_.gsub!(/^( *belongs_to :#{attr}) *$/, %q(\\1, class_name: %q<#{model_name}>))' app/models/#{jcls.underscore}.rb], 
        "    Fix '#{attr}' association in join model to use correct class"
  @jcls[jcls][attr] = true
  
end

desc 'Generate sh script to replace habtm with has_many :through'
task :remove_habtms, %i[model attr] => [:environment] do |_task, args|
  puts '#!/bin/sh'
  puts ''
  puts 'migrations=""'

  models = if args.model
             [args.model.constantize]
           else
             warn __dir__
             Dir[File.expand_path('app/models/*.rb')].each { |s| require s }

             Base.descendants
           end

  @jcls = {}
  @modifications = {}
  models.each do |model|
    docu "Considering model #{model.name}"
    asss = model.reflect_on_all_associations(:has_and_belongs_to_many)
    asss = asss.select { |a| a.name.to_sym == args.attr.to_sym } if args.attr
    next if asss.blank?

    asss.each do |assoc|
      docu "  Association #{assoc.name}"
      this = assoc.foreign_key.to_s.delete_suffix('_id')
      other = assoc.association_foreign_key.to_s.delete_suffix('_id')
      new_name = assoc.join_table
      jcls_name = new_name.to_s.classify
      if @jcls[jcls_name]
        docu "    join class '#{jcls_name}' already produced"
      else
        # generate the model
        putsh "rails g model #{jcls_name} #{other}:references #{this}:references --skip-migration",
              "    generate '#{jcls_name}' as join class"
        fix_join_model(jcls_name, other, assoc.class_name)
        fix_join_model(jcls_name, this, model.name)

        # generate migration
        migname = "AddIdTimestampsTo#{jcls_name}"
        putsh "rails g migration #{migname} id:primary_key created_at:datetime updated_at:datetime",
              "    generate migration to add id to '#{assoc.join_table}'"
        putsh "migname=`ls -1 db/migrate/*#{migname.underscore}.rb`"
        putsh %[ruby -i -pe '$_.gsub!(/:datetime *$/, ":datetime, null: false, default: Time.zone.now")' ${migname}]
        putsh %(migrations="${migrations}\n${migname}")

        # generate graphiti resources
        putsh "rails g graphiti:resource #{jcls_name}",
              "    generate graphiti items for #{jcls_name}"
        @modifications[model.name] ||= []
        @modifications[model.name] << assoc_msg(
          new_name, other.pluralize, scope: assoc.scope
        )
        @modifications[assoc.class_name] ||= []
        @modifications[assoc.class_name] << assoc_msg(new_name, this.pluralize)
      end
    end
    puts ''
  end
  puts <<~EOR
    cat <<EOF
    
    If this script ran successfully, it created the join models
      #{@jcls.keys.join("\n  ")}
    and the migrations ${migrations}
    
    Please review the models and the migrations for correctness, then run
      rake db:migrate
    The following changes should be made manually:
    
  EOR
  @modifications.each do |mod, v|
    puts "#{mod} (file app/models/#{mod.underscore}.rb):"
    v.each { |tt| puts "  #{tt}" }
    puts ''
  end
  puts 'EOF'
end

