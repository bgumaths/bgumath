# frozen_string_literal: true

namespace :cw2as do
  desc 'Convert Carrierwave to ActiveStorage'
  task :convert, %i[model attr] => [:environment] do |_task, args|
    args.model.constantize.find_each do |r|
      r.public_send(:"old_#{args.attr}").each do |f|
        name = f.file.filename
        file = f.file.file.sub("old_#{args.attr}", args.attr)
        puts "-- Converting #{name} from #{args.attr} of #{r.decorate}"
        r.public_send(:"add_#{args.attr}=",
                      io: File.open(file),
                      filename: name)
      rescue Errno::ENOENT => e
        warn "    #{e}"
      end
    end
  end

  desc 'rename field name'
  task :rename, %i[model attr] => [:environment] do |_task, args|
    ActiveStorage::Attachment.where(record_type: args.model,
                                    name: "new_#{args.attr}") do |as|
      as.name = args.attr
      as.save!
    end
  end

  desc 'update default attached'
  task :default, %i[model attr] => [:environment] do |_task, args|
    args.model.constantize.find_each do |r|
      puts "-- Setting default #{args.attr} of #{args.model} '#{r.decorate}'"
      r.public_send(:"#{args.attr}=", r.public_send(:"#{args.attr}_s"))
      r.save!
    end
  end

  desc 'update crop data'
  task :crop_data, %i[model attr] => [:environment] do |_task, args|
    args.model.constantize.find_each do |r|
      puts "-- Setting crop data for #{args.attr} of #{args.model} '#{r.decorate}'"
      r.public_send(args.attr).find_each do |pp|
        fname = pp.filename.to_s
        data = r.public_send(:"crop_data_#{args.attr}")[fname]
        if data.present?
          puts "    #{fname}: #{data}"
          pp.update(data)
        else
          puts "    #{fname}: NO DATA FOUND!"
        end
      end
    end
  end
    
end

