# frozen_string_literal: true

require 'rest-client'
require 'nokogiri'
require 'filecache'
require 'course_catalog/v1'
require 'course_catalog/v2'

# there are two kinds of methods, for searching and for particular course.
# The arguments for each are documented above `search_params` and
# `course_params`, resp.
module CourseCatalog
  # api2 is newer and 'official', but api1 has more info, so we use api2 for 
  # links and api1 for the rest
  @@api1 = CourseCatalog::V1
  @@api2 = CourseCatalog::V2
  @@api = @@api1

  def self.api(val = '')
    if val.to_s.match?(/1$/)
      @@api = @@api1
    elsif val.to_s.match?(/2$/)
      @@api = @@api2
    end
    @@api
  end

  TERM_NUMS = {
    fall: 1,
    spring: 2,
    summer: 3,
  }.freeze
  TERM_NAMES = %w[fall spring summer].freeze
  CLASS_KINDS = %w[שעור תרגיל].freeze

  def self.uri(action = api.action)
    URI("https://bgu4u.bgu.ac.il/pls/scwp/#{action}")
  end

  # current catalog year
  def self.cur_year
    cur = Date.current
    (cur.month < 7) ? cur.year : cur.year + 1
  end

  def self.cur_term
    cur = Date.current
    (2..8).cover?(cur.month) ? 2 : 1
  end

  def self.term_num(trm)
    TERM_NUMS[trm.try(:to_s).try(:downcase).try(:to_sym)]
  end

  # * campus:
  #   - 0: bgu
  # * dept:
  #   - 201: math
  # * course
  # * year (physical: fall2017, spring2018 same ac year)
  # * term (fall, spring, summer or 1,2,3)
  # * level
  # * advanced (bool)
  def self.search_params(...)
    api.search_params(...)&.compact
  end

  # id: e.g., 201.1.6061, or 1.6061, or 6061.
  # term: eg fall2017, spring2018 or `{ rn_year: 2018, rn_semester: 2 }`
  #       fall2017 means 2018 in the terminology of the catalog, i.e., same
  #       as {rn_year: 2018, rn_semester: 1}
  # opts:
  #   - campus: 0
  #   - year
  #   - term fall/spring/summer or 1/2/3
  #   - details: false/true whether to fetch lecturer and classes details
  #     the default is true if the term argument is given, false otherwise
  #
  # So the term and year can be specified either as fall2017 for the second
  # argument, or as `term: 1, year: 2017` in opts. In the first case,
  # `details` defaults to true, in the second to false. If none is specified,
  # the catalog is searched, and the most recent version is chosen (and
  # details defaults to false)
  def self.course_params(...)
    api.course_params(...)&.compact
  end

  def self.parse_search(...)
    api.parse_search(...)
  end

  def self.parse_course(...)
    api.parse_course(...)
  end

  def self.get_request
    RestClient::Resource.new(uri.to_s)
  end

  @@response = {}

  def self.responses
    @@response
  end

  @@cachedir = File.join (Dir.home || '/tmp'), '.cache'
  @@cachemin = 24 * 60

  def self.get_response!(params)
    req = get_request
    warn("Getting info from #{req.url}") if @debug
    begin
      resp = req.post(params)
    rescue SocketError
      # try again
      sleep 3
      resp = req.post(params)
    rescue Errno::ECONNRESET
      resp = nil
    end
    resp.respond_to?(:body) ? resp.body : nil
  end

  def self.get_response(params)
    return nil unless params

    @@fc ||= FileCache.new 'course_catalog', @@cachedir, @@cachemin * 60
    @@fc.get_or_set params.to_json do
      get_response!(params)
    end
  end

  def self.get_doc(...)
    Nokogiri::HTML(get_response(...), nil, 'WINDOWS-1255', &:noent)
  end

  def self.get_doc!(...)
    Nokogiri::HTML(get_response!(...), nil, 'WINDOWS-1255', &:noent)
  end

  def self.course_doc(...)
    get_doc(course_params(...))
  end

  def self.course_doc!(...)
    get_doc!(course_params(...))
  end

  def self.search_doc(...)
    get_doc(search_params(...))
  end

  def self.search_link(...)
    params = @@api2.search_params(...)&.compact
    return nil unless params

    puri = uri(@@api2.action)
    puri.query = URI.encode_www_form(params)
    puri
  end

  def self.course_link(...)
    params = @@api2.course_params(...)&.compact
    return nil unless params

    puri = uri(@@api2.action)
    puri.query = URI.encode_www_form(params)
    puri
  end

  def self.deblank(str)
    str.try(:strip)
  end

  def self.search_details(*, **opts)
    doc = search_doc(*, **opts)
    res = parse_search(doc)
    warn "  FOUND #{res.inspect}" if opts[:debug]
    res
  end

  def self.course_details(*args, **opts)
    full = opts.delete(:full)
    skip_cache = opts.delete(:skip_cache)
    @debug = opts[:debug]
    opts[:details] = false if full
    doc = skip_cache ? course_doc!(*args, **opts) : course_doc(*args, **opts)
    res = begin
      parse_course(doc)
    rescue NoMethodError => e
      raise e.exception(e.to_s + ". Failed to parse for #{args}")
    end
    if full
      opts[:details] = true
      doc = skip_cache ? course_doc!(*args, **opts) : course_doc(*args, **opts)
      res.merge!(
        begin
          parse_course(doc)
        rescue NoMethodError => e
          { errors: e.exception(e.to_s + ". Failed to parse details for #{args}") }
        end,
      )
      res[:params] = course_params(*args, **opts)
    end
    res
  end

  def self.s_to_a(s)
    return s if s.is_a?(Array)
    return [nil] unless s

    s.to_s.split(/ *, */).map do |x|
      r = x.split(/ *(-|\.\.) */)
      (r.count > 1) ? Array(r[0].to_i..r[-1].to_i).map(&:to_s) : r
    end.flatten
  end

  def self.crawl(**opts)
    years = s_to_a(opts[:years])
    depts = s_to_a(opts[:depts])
    levels = s_to_a(opts[:levels])
    terms = s_to_a(opts[:terms])
    terms = [1, 2, 3] unless terms[0]
    campuses = s_to_a(opts[:campuses])
    full = opts.key?(:full) ? opts[:full] : true
    years.each do |year|
      warn "Crawling year #{year || 'current'}..." if opts[:debug]
      depts.each do |dept|
        warn "  ... #{dept || 'default'} department..." if opts[:debug]
        levels.each do |level|
          warn "  ... level #{level || 'any'}..." if opts[:debug]
          terms.each do |term|
            warn "  ... #{term || 'any'} term ..." if opts[:debug]
            campuses.each do |campus|
              warn "  ... campus #{campus || 'any'}..." if opts[:debug]
              items = search_details(**{ year:, campus:,
                                         term:, level:,
                                         dept:,
                                         debug: opts[:debug], }.select { |_k, v| v })
              items.each do |item|
                course = nil
                if term
                  term = 'fall' if term.to_s == '1'
                  term = 'spring' if term.to_s == '2'
                  term = 'summer' if term.to_s == '3'
                  begin
                    course = course_details(
                      item[3], "#{term}#{year || Date.current.year}",
                      full:, debug: opts[:debug]
                    )
                    yield course
                  rescue e
                    yield e
                  end
                else
                  TERM_NAMES.each do |trm|
                    course = course_details(
                      item[3], "#{trm}#{year || Date.current.year}",
                      debug: opts[:debug], full:
                    )
                    yield course
                  rescue e
                    yield e
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end

