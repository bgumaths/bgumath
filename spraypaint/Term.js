import { hasMany, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'terms'
  },
  attrs: {
    roles: hasMany(),
    courses: hasMany(),
    seminars: hasMany(),
    starts: attr(),
    ends: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    exams: attr(),
    slug: attr(),
    nameI18n: attr(),
    name: attr(),
    web: attr(),
    allowedLocales: attr(),
    titleI18n: attr(),
  },

})

