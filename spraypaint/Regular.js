import { hasMany } from 'spraypaint/dist/spraypaint'
import Member from '@/models/Member'

export default Member.extend({
  static: {
    jsonapiType: 'regulars'
  },
  attrs: {
    issues: hasMany(),
  },

})

