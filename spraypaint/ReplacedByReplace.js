import { hasMany, belongsTo, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'replaced_by_replaces'
  },
  attrs: {
    roles: hasMany(),
    replacedBy: belongsTo('generic_courses'),
    replace: belongsTo('generic_courses'),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    web: attr(),
    allowedLocales: attr(),
  },

})

