import { hasMany, belongsTo, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'students_supervisors'
  },
  attrs: {
    roles: hasMany(),
    supervisor: belongsTo('members'),
    student: belongsTo(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    web: attr(),
    allowedLocales: attr(),
  },

})

