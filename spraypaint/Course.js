import { hasMany, belongsTo, hasOne, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'courses'
  },
  attrs: {
    roles: hasMany(),
    term: belongsTo(),
    genericCourse: belongsTo(),
    lecturer: belongsTo('academics'),
    studentRep: belongsTo('issues_student_reps'),
    agudaRep: hasOne('issues_aguda_reps'),
    departments: hasMany(),
    issuesStaffs: hasMany(),
    issues: hasMany(),
    hours: attr(),
    web: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    titleI18n: attr(),
    title: attr(),
    contentI18n: attr(),
    content: attr(),
    slug: attr(),
    abstractI18n: attr(),
    abstract: attr(),
    allowedLocales: attr(),
    shid: attr(),
    shnatonUrl: attr(),
  },

})

