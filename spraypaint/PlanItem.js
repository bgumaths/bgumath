import { hasMany, belongsTo, hasOne, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'plan_items'
  },
  attrs: {
    roles: hasMany(),
    termPlan: belongsTo(),
    genericCourse: belongsTo(),
    acYear: hasOne(),
    shnaton: hasOne(),
    degree: hasOne(),
    degreePlan: hasOne(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    web: attr(),
    allowedLocales: attr(),
  },

})

