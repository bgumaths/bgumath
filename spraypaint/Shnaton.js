import { hasMany, belongsTo, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'shnatons'
  },
  attrs: {
    roles: hasMany(),
    acYear: belongsTo(),
    courseLists: hasMany(),
    degrees: hasMany(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    degreesOrder: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

