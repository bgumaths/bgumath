import { hasMany, belongsTo, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'meetings'
  },
  attrs: {
    roles: hasMany(),
    seminar: belongsTo(),
    speaker: attr(),
    title: attr(),
    abstract: attr(),
    room: attr(),
    from: attr(),
    duration: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    slug: attr(),
    speakerWeb: attr(),
    announcementI18n: attr(),
    announcement: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

