import { hasMany, belongsTo, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'seminars'
  },
  attrs: {
    roles: hasMany(),
    term: belongsTo(),
    researchGroupsSeminars: hasMany(),
    researchGroups: hasMany(),
    meetings: hasMany(),
    day: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    room: attr(),
    duration: attr(),
    nameI18n: attr(),
    name: attr(),
    descriptionI18n: attr(),
    description: attr(),
    slug: attr(),
    listId: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

