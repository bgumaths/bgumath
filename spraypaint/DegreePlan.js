import { hasMany, belongsTo, hasOne, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'degree_plans'
  },
  attrs: {
    roles: hasMany(),
    degree: belongsTo(),
    termPlans: hasMany(),
    shnaton: hasOne(),
    acYear: hasOne(),
    nameI18n: attr(),
    name: attr(),
    remarksI18n: attr(),
    remarks: attr(),
    slug: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    web: attr(),
    allowedLocales: attr(),
  },

})

