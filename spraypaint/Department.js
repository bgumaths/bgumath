import { hasMany, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'departments'
  },
  attrs: {
    roles: hasMany(),
    departmentsGenericCourses: hasMany(),
    courses: hasMany('generic_courses'),
    people: hasMany('issues_staffs'),
    nameI18n: attr(),
    name: attr(),
    catalogId: attr(),
    slug: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    webpage: attr(),
    bguName: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

