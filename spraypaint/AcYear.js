import { hasMany, hasOne, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'ac_years'
  },
  attrs: {
    roles: hasMany(),
    addedCourses: hasMany('generic_courses'),
    removedCourses: hasMany('generic_courses'),
    shnaton: hasOne(),
    year: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    web: attr(),
    allowedLocales: attr(),
  },

})

