import { hasMany, belongsTo, hasOne, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'term_plans'
  },
  attrs: {
    roles: hasMany(),
    degreePlan: belongsTo(),
    planItems: hasMany(),
    freePlanItems: hasMany(),
    genericCourses: hasMany(),
    degree: hasOne(),
    shnaton: hasOne(),
    acYear: hasOne(),
    year: attr(),
    remarksI18n: attr(),
    remarks: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    term: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

