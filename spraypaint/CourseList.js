import { hasMany, belongsTo, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'course_lists'
  },
  attrs: {
    roles: hasMany(),
    courseListsCourses: hasMany(),
    courses: hasMany('generic_courses'),
    courseListsProgPrereqs: hasMany(),
    prereqs: hasMany('prog_prereqs'),
    shnaton: belongsTo(),
    nameI18n: attr(),
    name: attr(),
    remarksI18n: attr(),
    remarks: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    slug: attr(),
    dcredits: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

