import { belongsTo, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'photos'
  },
  attrs: {
    imageable: belongsTo(),
    imageableType: attr(),
    x: attr(),
    y: attr(),
    width: attr(),
    height: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    rotate: attr(),
    scaleX: attr(),
    scaleY: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

