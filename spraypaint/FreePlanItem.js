import { hasMany, belongsTo, hasOne, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'free_plan_items'
  },
  attrs: {
    roles: hasMany(),
    termPlan: belongsTo(),
    acYear: hasOne(),
    shnaton: hasOne(),
    degree: hasOne(),
    degreePlan: hasOne(),
    titleI18n: attr(),
    title: attr(),
    minp: attr(),
    maxp: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    web: attr(),
    allowedLocales: attr(),
  },

})

