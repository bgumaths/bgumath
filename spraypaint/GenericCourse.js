import { hasMany, belongsTo, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'generic_courses'
  },
  attrs: {
    roles: hasMany(),
    courseListsCourses: hasMany(),
    courseLists: hasMany(),
    planItems: hasMany(),
    termPlans: hasMany(),
    requirements: hasMany('allows_requires'),
    requires: hasMany('generic_courses'),
    allowances: hasMany('allows_requires'),
    allows: hasMany('generic_courses'),
    courses: hasMany(),
    departmentsGenericCourses: hasMany(),
    departments: hasMany(),
    issuesStaffs: hasMany(),
    agudaRep: belongsTo('issues_aguda_reps'),
    yearAdded: belongsTo('ac_years'),
    yearRemoved: belongsTo('ac_years'),
    replacements: hasMany('replaced_by_replaces'),
    replacedBy: hasMany('generic_courses'),
    renewals: hasMany('replaced_by_replaces'),
    replaces: hasMany('generic_courses'),
    shid: attr(),
    graduate: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    prereq: attr(),
    core: attr(),
    nameI18n: attr(),
    name: attr(),
    descriptionI18n: attr(),
    description: attr(),
    slug: attr(),
    advanced: attr(),
    fall: attr(),
    spring: attr(),
    lectures: attr(),
    exercises: attr(),
    dcredits: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

