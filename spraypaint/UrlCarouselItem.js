import { hasMany, hasOne, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'url_carousel_items'
  },
  attrs: {
    roles: hasMany(),
    image: hasOne('photos'),
    activates: attr(),
    expires: attr(),
    interval: attr(),
    order: attr(),
    url: attr(),
    resourceType: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    web: attr(),
    allowedLocales: attr(),
  },

})

