import { hasMany } from 'spraypaint/dist/spraypaint'
import Academic from '@/models/Academic'

export default Academic.extend({
  static: {
    jsonapiType: 'members'
  },
  attrs: {
    studentsSupervisors: hasMany(),
    students: hasMany(),
    hostsPostdocs: hasMany(),
    postdocs: hasMany(),
    hostsVisitors: hasMany(),
    visitors: hasMany(),
  },

})

