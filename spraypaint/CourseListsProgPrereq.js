import { hasMany, belongsTo, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'course_lists_prog_prereqs'
  },
  attrs: {
    roles: hasMany(),
    courseList: belongsTo(),
    prereq: belongsTo('prog_prereqs'),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    web: attr(),
    allowedLocales: attr(),
  },

})

