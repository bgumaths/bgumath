import { attr } from 'spraypaint/dist/spraypaint'
import BaseEvent from '@/models/BaseEvent'

export default BaseEvent.extend({
  static: {
    jsonapiType: 'events'
  },
  attrs: {
    title: attr(),
    location: attr(),
    web: attr(),
    description: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    centre: attr(),
    duration: attr(),
    slug: attr(),
    expires: attr(),
    allowedLocales: attr(),
    writable: attr(),
  },

})

