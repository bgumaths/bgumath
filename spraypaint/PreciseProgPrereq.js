import { hasMany, belongsTo, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'precise_prog_prereqs'
  },
  attrs: {
    roles: hasMany(),
    degree: belongsTo(),
    subreqs: hasMany('prog_prereqs'),
    parent: belongsTo('prog_prereqs'),
    courseListsProgPrereqs: hasMany(),
    courseLists: hasMany(),
    nameI18n: attr(),
    name: attr(),
    descriptionI18n: attr(),
    description: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    slug: attr(),
    courseListsOrder: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

