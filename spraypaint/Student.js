import { hasMany } from 'spraypaint/dist/spraypaint'
import Academic from '@/models/Academic'

export default Academic.extend({
  static: {
    jsonapiType: 'students'
  },
  attrs: {
    studentsSupervisors: hasMany(),
    supervisors: hasMany('members'),
  },

})

