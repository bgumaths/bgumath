import { attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'settings'
  },
  attrs: {
    var: attr(),
    value: attr(),
    thingId: attr(),
    thingType: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    visibility: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

