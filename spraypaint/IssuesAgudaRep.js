import { hasMany, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'issues_aguda_reps'
  },
  attrs: {
    roles: hasMany(),
    genericCourses: hasMany(),
    courses: hasMany(),
    issues: hasMany(),
    login: attr(),
    cryptedPassword: attr(),
    passwordSalt: attr(),
    persistenceToken: attr(),
    loginCount: attr(),
    failedLoginCount: attr(),
    lastRequestAt: attr(),
    currentLoginAt: attr(),
    lastLoginAt: attr(),
    currentLoginIp: attr(),
    lastLoginIp: attr(),
    active: attr(),
    approved: attr(),
    confirmed: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    fullname: attr(),
    passChanged: attr(),
    departmentId: attr(),
    apiKey: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

