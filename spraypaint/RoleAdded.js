import RoleEvent from '@/models/RoleEvent'

export default RoleEvent.extend({
  static: {
    jsonapiType: 'role_addeds'
  },

})
