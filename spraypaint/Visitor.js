import { hasMany, attr } from 'spraypaint/dist/spraypaint'
import BaseEvent from '@/models/BaseEvent'

export default BaseEvent.extend({
  static: {
    jsonapiType: 'visitors'
  },
  attrs: {
    hostsVisitors: hasMany(),
    hosts: hasMany('members'),
    email: attr(),
    firstI18n: attr(),
    first: attr(),
    lastI18n: attr(),
    last: attr(),
    office: attr(),
    phone: attr(),
    rank: attr(),
    webpage: attr(),
    origin: attr(),
    centre: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    duration: attr(),
    web: attr(),
    allowedLocales: attr(),
    writable: attr(),
  },

})

