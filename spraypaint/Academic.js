import { hasMany } from 'spraypaint/dist/spraypaint'
import User from '@/models/User'

export default User.extend({
  static: {
    jsonapiType: 'academics'
  },
  attrs: {
    courses: hasMany(),
    membersResearchGroups: hasMany(),
    researchGroups: hasMany(),
  },

})

