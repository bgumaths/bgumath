import { hasMany, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'research_groups'
  },
  attrs: {
    roles: hasMany(),
    membersResearchGroups: hasMany(),
    members: hasMany('academics'),
    researchGroupsSeminars: hasMany(),
    seminars: hasMany(),
    name: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    description: attr(),
    slug: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

