import { hasMany, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'roles'
  },
  attrs: {
    roles: hasMany(),
    usersRoles: hasMany(),
    users: hasMany(),
    name: attr(),
    resourceType: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    visibility: attr(),
    titleI18n: attr(),
    title: attr(),
    descriptionI18n: attr(),
    description: attr(),
    email: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

