import { hasMany, belongsTo, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'degrees'
  },
  attrs: {
    roles: hasMany(),
    prereqs: hasMany('prog_prereqs'),
    plans: hasMany('degree_plans'),
    shnaton: belongsTo(),
    nameI18n: attr(),
    name: attr(),
    descriptionI18n: attr(),
    description: attr(),
    slug: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    credits: attr(),
    prereqsOrder: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

