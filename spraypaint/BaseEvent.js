import { hasMany } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'base_events'
  },
  attrs: {
    roles: hasMany(),
  },

})

