import { hasMany, belongsTo, hasOne, attr } from 'spraypaint/dist/spraypaint'
import ApplicationRecord from '@/models/ApplicationRecord'

export default ApplicationRecord.extend({
  static: {
    jsonapiType: 'issues'
  },
  attrs: {
    roles: hasMany(),
    course: belongsTo(),
    reporter: belongsTo('issues_student_reps'),
    replier: belongsTo('regulars'),
    issuesIssuesStaffs: hasMany(),
    staffs: hasMany('issues_staffs'),
    agudaRep: belongsTo('issues_aguda_reps'),
    lecturer: hasOne('academics'),
    studentRep: hasOne('issues_student_reps'),
    departments: hasMany(),
    term: hasOne(),
    title: attr(),
    content: attr(),
    reply: attr(),
    published: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    slug: attr(),
    repliedAt: attr(),
    publishedAt: attr(),
    correspondence: attr(),
    web: attr(),
    allowedLocales: attr(),
  },

})

