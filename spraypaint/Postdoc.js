import { hasMany } from 'spraypaint/dist/spraypaint'
import Academic from '@/models/Academic'

export default Academic.extend({
  static: {
    jsonapiType: 'postdocs'
  },
  attrs: {
    hostsPostdocs: hasMany(),
    hosts: hasMany('members'),
  },

})

