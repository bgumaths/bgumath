#!/usr/bin/perl

use v5.10.0;

BEGIN {
    our $/ = '';
}

while (<>) {
    next if /403 Forbidden/ or /Valid: filtered/;
    if ( /404 Not Found/ ) {
        if ( /URL\s*`([^']*)'\nName\s*([^\n]*)\nParent URL.*(http[^\n]*)\n/ ) {
            say "Broken link on $3:: <$1>  ($2)";
            print STDERR "$1\n";
        } else {
            warn "unparsable broken link: $_\n";
        }
        next;
    }
    if ( /Timeout/ ) {
        $to++;
        next;
    }
    print;
}

say "!! $to timed out";

