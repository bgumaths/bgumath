## frozen_string_literal: true

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __dir__)
require "rack/test"

RSpec.describe ValidateRequestParams do
  include Rack::Test::Methods

  let(:app) { Bgumath::Application }

  context 'posting params' do
    before { post '/research/events', params }

    context "WITH invalid characters" do
      def self.null_byte
        "\u0000"
      end

      def self.title
        "A very interesting #{null_byte} event"
      end

      def self.add_case(desc, param)
        context "for #{desc}" do
          let(:params) { {title: param} }
          it "responds with 400 BadRequest" do
            expect(last_response).to be_bad_request
          end
        end
      end

      add_case('string values', title)
      add_case('hashes of strings', {inner: title})
      add_case('arrays of strings', [title])
      add_case('arrays of hashes of strings', [{inner: title}])
    end

    context "WITHOUT invalid characters" do
      def self.null_byte
        ''
      end

      def self.title
        "A very interesting #{null_byte} event"
      end

      def self.add_case(desc, param)
        context "for #{desc}" do
          let(:params) { {title: param} }
          it "responds does not return bad request for strings" do
            expect(last_response).not_to be_bad_request
          end
        end
      end

      add_case('string values', title)
      add_case('hashes of strings', {inner: title})
      add_case('arrays of strings', [title])
      add_case('arrays of hashes of strings', [{inner: title}])
    end

    context "with no parameters" do
      let(:params) { nil }
      it "responds does not respond with bad request" do
        expect(last_response).not_to be_bad_request
      end
    end
  end

  context 'getting with session' do
    before {
      set_cookie "_bgumath_session=adfec7as9413db963b5#{null_byte}"
      get '/'
    }

    context "with invalid characters in the session" do
      let(:null_byte) { "%00" }
      it 'responds with 400 BadRequest' do
        expect(last_response).to be_bad_request
      end
    end

    context "with valid characters in the session" do
      let(:null_byte) { '' }
      it 'does not respond with 400 BadRequest' do
        expect(last_response).not_to be_bad_request
      end
    end
  end
end

