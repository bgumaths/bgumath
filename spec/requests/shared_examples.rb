# frozen_string_literal: true

require 'rails_helper'

RSpec.configure do |c|
  c.before(:example) do
    ActionMailer::Base.default_url_options[:locale] = I18n.locale
    ActionMailer::Base.default_url_options[:host] = 'test.host'
    ActionMailer::Base.default_url_options[:protocol] = 'https'
    Rails.application.routes.default_url_options = ActionMailer::Base.default_url_options
  end
end

JSONAPI_ERROR = {
  bad_request: {
    status: '400',
    title: 'Request Error',
    code: 'bad_request',
  },
  unauthorized: {
    status: '401',
    title: 'Unauthorized',
    code: 'unauthorized',
  },
  forbidden: {
    status: '403',
    title: 'Forbidden',
    code: 'forbidden',
  },
  not_found: {
    status: '404',
    title: 'Not Found',
    code: 'not_found',
  },
  unprocessable_entity: {
    status: '422',
    title: 'Validation Error',
    code: 'unprocessable_entity',
  },
  service_unavailable: {
    status: '503',
    title: 'Service Unavailable',
    code: 'service_unavailable',
  },
}.each_value(&:stringify_keys!).freeze

RSpec.shared_examples 'reject with error' do |opts|
  let(:err) { opts.keys[0] }
  if opts[:unprocessable_entity]
    let(:err_attr) { error['meta']['attribute'] || '' }
    let(:msg) { "#{err_attr.camelcase}: #{opts.values[0]}" }
  else
    let(:msg) { opts.values[0] }
  end

  it 'is rejected' do
    expect(response).to have_http_status err
  end

  it 'returns correct jsonapi error' do
    exp = JSONAPI_ERROR[err.to_sym].dup
    exp['detail'] = msg if msg
    expect(**error).to include(exp)
  end

  if opts[:unauthorized]
    it 'requests authentication' do
      expect(response.headers['WWW-Authenticate'])
        .to eq('Token realm="bgumath"')
    end
  end

end

RSpec.shared_examples 'regular request includes' do

  include_examples 'general includes'

  let(:json) { JSON.parse(response.body) }
  let(:data) { json['data'] }
  let(:error) { json['errors'][0] }
  let(:id) { data['id'] }
  let(:attributes) { binding.pry unless data;data['attributes'] }
  # :user, :type, :objects and :object should be defined in examples
  let(:object_id) { object.id }
  let(:policy) { Pundit.policy!(user, object) rescue nil }
  let(:api_key) { user.create_api_key }

  let(:pol_attributes) {
    (policy&.allowed_attributes || []).reject{|x| x.to_s =~ /_id$/}
  }

  def obj_attrs(lattr = pol_attributes - %w[slug web])
    obj = object.decorate
    Hash[lattr.map{|att| [att, obj.send(att)] }]
  end

  let(:headers) {
    res = {
      'Content-Type' => 'application/vnd.api+json',
      'Accept' => 'application/vnd.api+json'
    }
    res[:Authorization] = ActionController::HttpAuthentication::Token
      .encode_credentials(api_key) if user.present?
    res
  }

  def to_jsonapi(obj, usr = User.admin, **opts)
    params = obj.to_perm_params(usr, **opts)
    rels = {}
    params.keys.select{|x| x.to_s =~ /_id$/}.each do |irel|
      id = params.delete(irel)
      next unless id
      rel = irel.to_s.gsub(/_id$/, '')
      rrel = object.send(rel)
      #rels[rel.to_s.dasherize.to_sym] = { data: {
      rels[rel.to_sym] = { data: {
        type: rrel.class.name.underscore.pluralize, id: rrel.id
      } }
    end
    res = { data: {
      type: obj.class.table_name,
      attributes: params, relationships: rels
    }}
    res[:data][:id] = obj.id if obj.id.present?
    res[:debug] = true
    res
  end

  let(:params) { to_jsonapi(object).to_json }

end

RSpec.shared_examples 'get all objects with user' do

  it 'is successful' do
    expect(response).to be_successful, response.body
  end

  it 'returns all objects' do
    expect(data.count).to eq(objects.count)
    data.each do |item|
      expect(**item).to include('type' => type)
    end
  end
end

RSpec.shared_examples 'get object with user' do
  context 'when the object exists' do
    it 'is successful' do
      expect(response).to be_successful
    end

    it 'returns the object' do
      expect(**data).to include('type' => type)
      expect(id).to eq(object_id)
    end

    it 'returns the correct fields' do
      expect(**attributes).to include(obj_attrs(pol_attributes - ['aguda_rep']))
      expect(attributes.keys).to match_array(pol_attributes + %w[writable])
    end
  end

  context 'when the object does not exist' do
    let(:object_id) { SecureRandom.uuid }
    include_examples 'reject with error', not_found: nil
  end
end


