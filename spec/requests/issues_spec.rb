## frozen_string_literal: true

require 'requests/shared_examples'
require 'requests/issues_shared_examples'

RSpec.describe 'Issues API' do

  include_examples 'regular request includes'

  before(:context) do
    @vice = create(:regular, :simple, :vice, email: 'bibi')
    @term = Term.current || create(:term, :simple, :current)
    @lect1 = create(:regular, :simple, course_count: 0, email: 'dony',
                    password: 'foobar')
    @lect2 = create(:regular, :simple, course_count: 0, email: 'dude',
                    password: 'foobar')
    @issues_student_rep1 = create(:issues_student_rep, login: 'monk',
                                  course_count: 0)
    @issues_student_rep2 = create(:issues_student_rep, login: 'davis',
                                  course_count: 0)
    @issues_aguda_rep = create(
      :issues_aguda_rep, login: 'coltrane',
      course_count: 0, password: 'foobar')
    @gc1 = create(:generic_course, :basic, :simple, 
                  aguda_rep: @issues_aguda_rep)
    @gc2 = create(:generic_course, :basic, :simple,
                  aguda_rep: nil)
    @gc3 = create(:generic_course, :basic, :simple,
                  aguda_rep: @issues_aguda_rep)
    @department1 = create(:department, catalog_id: '123',
                          courses: [@gc1, @gc2])
    @department2 = create(:department, catalog_id: '456',
                          courses: [@gc3, @gc2])
    @issues_staff1 = create(:issues_staff, login: 'ganz', 
                            department: @department1)
    @issues_staff2 = create(:issues_staff, login: 'boogy',
                            department: @department1)
    @issues_staff3 = create(:issues_staff, login: 'lapid',
                            department: @department2, pass_changed: false)
    @issues_staff4 = create(:issues_staff, login: 'hendel',
                            department: @department2)
    @course1 = create(:course, lecturer: @lect1, term: @term,
                      student_rep: @issues_student_rep1, generic_course: @gc1)
    @course2 = create(:course, lecturer: @lect2, term: @term,
                      generic_course: @gc2, student_rep: @issues_student_rep1)
    @course3 = create(:course, lecturer: @lect1, term: @term,
                      student_rep: @issues_student_rep2,
                      generic_course: @gc3)
    @responder1 = create(:regular, :simple, :responder, password: 'foobar', 
                         email: 'bach')
    @issue3 = create(:issue, course: @course3, reporter: @issues_student_rep2)
    @issue4 = create(:issue, :published, course: @course3,
                     reporter: @issues_student_rep2)
    @issue5 = create(:issue, :published, course: @course2,
                     reporter: @issues_student_rep1)
  end
  let(:unsaved) { build(:issues_student_rep, password: '', login: 'brubeck',
                        courses: [@course2]) }
  let(:issue1) { build(:issue, course: @course1) }
  let(:issue2) { build(:issue, course: @course2) }
  let(:issue6) { build(:issue, course: @course3) }
  let(:responder1) { @responder1 }
  let(:responder2) {
    build(:regular, :simple, :responder, password: 'barfoo', email: 'chopin')
  }

  let(:type) { 'issues' }
  let(:objects) { [@issue3, @issue4, @issue5] }
  let(:object) { objects[0] }

  ###################

  describe 'GET /issues' do

    before { get '/api/v1/issues', headers: headers }

    context 'when user is vice head:' do

      let(:user) { @vice }

      include_examples 'get all objects with user'
    end

    context 'when user is a lecturer:' do
      let(:user) { @lect2 }
      let(:objects) { [@issue5] }

      include_examples 'get all objects with user'
    end

    include_examples 'reject without user'
  end

  ###################

  describe 'GET /issues/:id' do

    before {
      get "/api/v1/issues/#{object_id}", params: { debug: true },
      headers: headers
    }

    context 'when user is vice head:' do
      let(:user) { @vice }

      include_examples 'get object with user'
    end

    context 'when user is a lecturer:' do
      let(:user) { @lect2 }

      context 'when object belongs to user:' do
        let(:objects) { [@issue5] }

        include_examples 'get object with user'
      end

      context 'when object does not belong to user:' do
        let(:objects) { [@issue3] }

        include_examples 'reject with error',
          forbidden: 'not allowed to show? this Issue'
      end
    end

    context 'when user is a student rep:' do
      let(:user) { @issues_student_rep1 }

      context 'when object belongs to user:' do
        let(:objects) { [@issue5] }

        include_examples 'get object with user'
      end

      context 'when object does not belong to user:' do
        let(:objects) { [@issue3] }

        include_examples 'reject with error',
          forbidden: 'not allowed to show? this Issue'
      end
    end

    include_examples 'reject without user'
  end

  ###################

  describe 'POST /issues' do
    let(:object) { issue1 }

    let(:params) {
      to_jsonapi(object, user, except: %i[slug]).to_json
    }

    context 'when no responders exist' do

      before(:each) {
        User.repliers.each do |rep|
          rep.issues.delete_all
          rep.destroy!
        end
        post '/api/v1/issues', params: params, headers: headers
      }

      context 'when user is a student rep:' do
        let(:user) { @issues_student_rep1 }

        context 'with own course' do

          include_examples 'reject with error', service_unavailable: I18n.t(
            :issues_noresponders, scope: %i[errors messages],
            locale: :en
          )
        end
      end
    end

    context 'when responders are registered' do

      before {
        post '/api/v1/issues', params: params, headers: headers
      }

      context 'when user is a student rep:' do
        let(:user) { @issues_student_rep1 }

        context 'with own course' do

          it 'is successful' do
            expect(response).to be_successful, response.body
          end

          it 'creates the object' do
            created = Issue.find(id)
            expect(created.title).to eq(object.title)
          end

          it 'responds with created object' do
            expect(**attributes).to include(obj_attrs)
            expect(attributes.keys)
              .to match_array(pol_attributes + %w[writable])
          end
        end

        context 'with foreign course' do
          let(:params) {
            res = to_jsonapi(object, user)
            res[:data][:relationships][:course][:data][:id] = @course3.id
            res.to_json
          }

          include_examples 'reject with error', unprocessable_entity:
            I18n.t(
              'reporter_courses',
              scope: %i[activerecord errors models issue attributes course],
              reporter: 'monk', locale: :en
          )
        end
      end

      context 'when user is a lecturer:' do
        let(:user) { @lect2 }
        let(:params) { to_jsonapi(object, @issues_student_rep1).to_json }

        include_examples 'reject with error',
          forbidden: 'not authorized to modify the following attributes: content, correspondence, course_id, title'
      end

      include_examples 'reject without user'
    end
  end

  ###################

  describe 'PUT /api/v1/issues/:id' do
    let(:object) {
      res = @issue5.dup
      res.title = 'foobar'
      res.id = @issue5.id
      res
    }
    let(:params) { to_jsonapi(object, object.reporter,
                                          only: :title).to_json }

    before {
      put "/api/v1/issues/#{object_id}", params: params, headers: headers
    }

    context 'when user is a student:' do
      let(:user) { @issues_student_rep1 }

      include_examples 'reject with error', 
        forbidden: 'not allowed to update? this Issue'
    end

    context 'when user is a responder:' do
      let(:user) { responder1 }

      context 'when modifying the title:' do

        include_examples 'reject with error', 
          forbidden: 'not authorized to modify the following attributes: title'
      end
    end

    context 'when user is a lecturer:' do
      let(:user) { @lect2 }

      include_examples 'reject with error',
        forbidden: 'not authorized to modify the following attributes: title'
    end

    include_examples 'reject without user'
  end

end


