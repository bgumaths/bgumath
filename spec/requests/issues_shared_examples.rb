# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'reject without user' do
  context 'when no user is given:' do
    let(:user) { nil }

    ## TODO this needs to change to unauthenticated, see issues_policy
    include_examples 'reject with error',
      forbidden: I18n.t(:must_login, locale: :en)

  end
end


