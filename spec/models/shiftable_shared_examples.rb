# frozen_string_literal: true

require 'models/durable_shared_examples'

RSpec.shared_examples 'a shiftable model' do
  include_examples 'a durable model'

  before do
    @now = DateTime.current
    @obj = build_object(starts: @now, date: @now)
  end

  describe '#date' do
    it 'returns the shift date' do
      expect(@obj.date).to eq(@now.to_date)
    end
  end

  describe '#shift=' do
    context 'when input is a shift' do
      it 'assigns it with the previous date' do
        sh = Tod::Shift.new(Tod::TimeOfDay('11:33'), Tod::TimeOfDay('12:47'))
        @obj.shift = sh
        expect(@obj.shift).to eq sh
        expect(@obj.date).to eq @now.to_date
      end
    end

    context 'when input is a range' do
      it 'assigns it with the previous date' do
        @obj.shift = '11:31'..'12:35'
        expect(@obj.starts).to eq Tod::TimeOfDay('11:31')
        expect(@obj.ends).to eq Tod::TimeOfDay('12:35')
        expect(@obj.date).to eq @now.to_date
      end
    end

    context 'when input is a hash' do
      it 'assigns the shift given by the :beginning and :ending keys' do
        @obj.shift = { beginning: '11:32', ending: '12:37' }
        expect(@obj.starts).to eq Tod::TimeOfDay('11:32')
        expect(@obj.ends).to eq Tod::TimeOfDay('12:37')
        expect(@obj.date).to eq @now.to_date
      end
    end

    context 'when input is blank' do
      it 'is nil' do
        @obj.shift = nil
        expect(@obj.starts).to be_nil
        expect(@obj.ends).to be_nil
      end
    end
  end
end
