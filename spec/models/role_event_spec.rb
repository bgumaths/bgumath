require 'rails_helper'
require 'models/shared_examples'

RSpec.describe RoleEvent, type: :model do
  before(:context) do
    DatabaseCleaner.start
    @admin = User.admin || create(:virtual, :admin)
    @adrole = @admin.roles[0]
    @grole = create(:role)
    @srole = create(:role, :for_seminar)
    @crole = create(:role, :for_course)
    @ev = [@grole, @srole, @crole].map{|rr| create(:role_event, role: rr)}
  end

  after(:context) do
    DatabaseCleaner.clean
  end

  def self.trasto_fields
    []
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'
  it_behaves_like 'a model with a user association', :user
  it_behaves_like 'a model with a user association', :creator

  describe 'Validations' do
    test_presence :role

    # user is optional
    #test_presence :user

    it 'is invalid without a type' do
      expect(build(:role_event, type: nil)).not_to be_valid
    end

    it 'is invalid with illegal type' do
      expect(build(:role_event, type: 'Foo')).not_to be_valid
    end
  end

  describe 'Scopes' do

    describe '.global' do
      it 'returns events with global role' do
        expect(RoleEvent.global.all).to match_array(@adrole.events + [@ev[0]])
      end
    end
  end
end


