# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'
require 'models/termed_shared_examples'

RSpec.describe Course, type: :model do
  def self.trasto_fields
    %i[title content name description abstract]
  end

  include_examples 'regular includes'
  it_behaves_like 'a regular model'
  it_behaves_like 'a termed model'
  it_behaves_like 'a model with a user association', :lecturer

  describe 'creation' do
    let(:generic) { create(:generic_course, slug: 'foobar') }
    let(:course) { create(:course, generic_course: generic) }
    it 'copies the slug from the generic course' do
      expect(course.slug).to eq('foobar')
    end
  end

  describe 'Validations' do
    test_presence :generic_course

    let(:course) { create(:course, term: default_term) }

    it 'is invalid with duplicate generic_courses per term' do
      d = build(:course, term: default_term,
                generic_course: course.generic_course)
      expect(d).not_to be_valid
    end
  end

  describe 'Scopes:' do
    before do
      @grad = create(:course, :grad, :current)
      @undergrad = create(:course, :undergrad, :current)
    end

    describe '.grad' do
      it 'returns only graduate courses' do
        expect(Course.grad).to eq [@grad]
      end
    end

    describe '.undergrad' do
      it 'returns only undergrad courses' do
        expect(Course.undergrad).to eq [@undergrad]
      end
    end
  end

  describe '#to_find_param' do
    let(:course) { create(:course, term: default_term) }
    it 'returns parameters to find the course' do
      expect(course.to_find_param).to eq(term_id: course.term.slug, 
                                         id: course.id)
    end
  end

end

