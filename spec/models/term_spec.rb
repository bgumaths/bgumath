# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.describe Term do
  
  def self.trasto_fields
    %i[name]
  end

  include_examples 'general includes'

  before(:context) do
    @today = default_term_date
    @old = create(:term, :simple, today: 1.year.ago(@today))
    @prev = create(:term, :simple, today: 6.months.ago(@today))
    @cur = create(:term, :simple, :following, prev: @prev)
    @next = create(:term, :simple, :following, prev: @cur)
    @future = create(:term, :simple, :current, today: 18.months.since(@today))
    @spring = create(:term, :simple, :spring, year: 2000) # spring2000
    @fall = create(:term, :simple, :fall, year: 2000) # fall2001
    @spring1 = create(:term, :simple, :spring, year: 2001, 
                      starts: @fall.ends + 7.weeks + 2.days) # spring2001
  end

  def local_build(*args)
    args.empty? ? default_term : build(model_param, *args)
  end

  it_behaves_like 'a regular model'

  describe 'validated' do
    it 'is invalid when "starts" is in a different term' do
      t = build(:term, :simple, starts: @future.starts + 1.week)
      expect(t).not_to be_valid
    end

    it 'is invalid when "ends" is in a different term' do
      t = build(:term, :simple, starts: @spring.starts - 16.weeks, 
                ends: @spring.starts + 1.week)
      expect(t).not_to be_valid
    end

    it 'is invalid when too short' do
      start = @future.ends + 8.weeks
      t = build(:term, :simple, starts: start,
                ends: start + Term.min_len - 1.week)
      expect(t).not_to be_valid
    end

    it 'is invalid when too long' do
      start = @future.ends + 8.weeks
      t = build(:term, :simple, starts: start, 
                ends: start + Term.max_len + 1.week)
      expect(t).not_to be_valid
    end

    it 'is invalid with non-standard months' do
      start = Date.new(2005, 8, 1)
      t = build(:term, :simple, starts: start,
                ends: start + Term.min_len + 1.week)
      expect(t).not_to be_valid
    end
  end

  describe '#part' do
    context 'for fall term' do
      it 'returns "fall"' do
        expect(@fall.part).to eq(:fall)
      end
    end

    context 'for spring term' do
      it 'returns "spring"' do
        expect(@spring.part).to eq(:spring)
      end
    end
  end

  describe '#fall?' do
    context 'for a fall term' do
      it 'is true' do
        expect(@fall).to be_fall
      end
    end

    context 'for a spring term' do
      it 'is false' do
        expect(@spring).not_to be_fall
      end
    end
  end

  describe '#fall' do
    context 'for a fall term' do
      it 'returns itself' do
        expect(@fall.fall).to eq @fall
      end
    end

    context 'for a spring term' do
      it 'returns the fall term for that academic year' do
        expect(@spring1.fall).to eq @fall
      end
    end

    context 'for a spring term with no fall term' do
      it 'is nil' do
        expect(@spring.fall).to be_nil
      end
    end
  end

  describe '#spring' do
    context 'for a spring term' do
      it 'returns itself' do
        expect(@spring.spring).to eq @spring
      end
    end

    context 'for a fall term' do
      it 'returns the spring term for that academic year' do
        expect(@fall.spring).to eq @spring1
      end
    end
  end

  describe '#other' do
    context 'for a fall term' do
      it 'returns the spring term of that academic year' do
        expect(@fall.other).to eq(@spring1)
      end
    end

    context 'for a spring term' do
      it 'returns the fall term of that academic year' do
        expect(@spring1.other).to eq(@fall)
      end
    end

    context 'for a term with no other term that year' do
      it 'is nil' do
        expect(@spring.other).to be_nil
      end
    end
  end

  describe '#year' do
    it 'returns the academic year' do
      expect(@fall.year).to eq(2001)
      expect(@spring.year).to eq(2000)
    end

    it 'is nil without a start date' do
      nterm = build(:term, starts: nil, ends: nil)
      expect(nterm.year).to be_nil
    end
  end

  describe 'scopes' do
    describe '.before' do
      it 'returns the terms ending strictly before today (including exam period)' do
        expect(Term.before(@cur.ends + 1.day)).to eq [@prev, @old, @spring1, @fall, @spring]
      end
    end

    describe '.before_no_exams' do
      it 'returns the terms with end date strictly before today' do
        expect(Term.before_no_exams(@cur.ends + 1.day))
          .to eq [@cur, @prev, @old, @spring1, @fall, @spring]
      end
    end

    describe '.after' do
      it 'returns the terms starting strictly after the given day' do
        expect(Term.after(@cur.starts))
          .to eq [@next, @future]
      end
    end

    describe '.ends_after' do
      it 'returns the terms ending (with exams) after the given day' do
        expect(Term.ends_after(@cur.starts))
          .to eq [@cur, @next, @future]
      end
    end

    describe '.ends_no_exams_after' do
      it 'returns the terms with end date after the given day' do
        expect(Term.ends_no_exams_after(@cur.ends + 1.day))
          .to eq [@next, @future]
      end
    end

    describe '.starts_before' do
      it 'returns the terms with start date before the given day' do
        expect(Term.starts_before(@cur.ends + 1.day))
          .to match_array [@spring, @fall, @spring1, @old, @prev, @cur]
      end
    end

  end

  let(:date) { @cur.starts + 1 }

  describe '.current' do
    it 'returns the term on the given date' do
      expect(Term.current(date)).to eq(@cur)
    end
  end

  describe '.current_no_exams' do
    it 'returns the term on the given date, excluding exams' do
      expect(Term.current_no_exams(date)).to eq(@cur)
      expect(Term.current_no_exams(@cur.ends + 1.day)).to be_nil
    end
  end

  describe '.fall' do
    it 'returns the fall term for the given academic year' do
      expect(Term.fall(2001)).to eq(@fall)
      expect(Term.fall(1999)).to be_nil
    end
  end

  describe '.spring' do
    it 'returns the spring term for the given academic year' do
      expect(Term.spring(1999)).to be_nil
      expect(Term.spring(2000)).to eq(@spring)
    end
  end

  describe '.next' do
    it 'returns the first term after the given date' do
      expect(Term.next(@cur.starts)).to eq(@next)
      expect(Term.next(@future.starts)).to be_nil
    end
  end

  describe '#next' do
    it 'returns the term following this one' do
      expect(@cur.next).to eq(@next)
      expect(@future.next).to be_nil
    end
  end

  describe '#after' do
    it 'returns all terms following this one' do
      expect(@cur.after).to eq([@next, @future])
      expect(@future.after).to be_empty
    end
  end

  describe '.prev' do
    it 'returns the first term before the given date' do
      expect(Term.prev(@cur.starts)).to eq(@prev)
      expect(Term.prev(@spring.starts)).to be_nil
    end
  end

  describe '#prev' do
    it 'returns the term before this one' do
      expect(@cur.prev).to eq(@prev)
      expect(@spring.prev).to be_nil
    end
  end

  describe '#before' do
    it 'returns all terms before this one' do
      expect(@cur.before).to eq [@prev, @old, @spring1, @fall, @spring]
      expect(@spring.before).to be_empty
    end
  end

  describe '.default' do
    context 'when there is a current term' do
      it 'returns the current term' do
        expect(Term.default(@cur.starts)).to eq(@cur)
      end
    end

    context 'when there is no current term, but there is a next term' do
      it 'returns the next term' do
        expect(Term.default(@spring.starts - 1.week)).to eq(@spring)
      end
    end
  end

  describe '.default_no_exams' do
    context 'when there is a current term' do
      it 'returns the current term' do
        expect(Term.default_no_exams(@cur.starts)).to eq(@cur)
      end
    end

    context 'when there is no current term, but there is a next term' do
      it 'returns the next term' do
        expect(Term.default_no_exams(@cur.ends + 1.day)).to eq(@next)
      end
    end

    context 'when there is no current with exams excluded or next term' do
      it 'returns the term with current exam period' do
        expect(Term.default_no_exams(@future.ends + 3.days)).to eq(@future)
      end
    end
  end

  describe '.next_or_cur_id' do
    context 'when there is a next term' do
      it 'returns the id of the next term' do
        expect(Term.next_or_cur_id(@cur.starts)).to eq(@next.id)
      end
    end

    context 'when there is no next term' do
      it 'returns the id of the current term' do
        expect(Term.next_or_cur_id(@future.starts)).to eq(@future.id)
      end
    end

    # we now always have a next or current term, the last term is eventually 
    # current
    context 'when we are far into the future' do
      it 'returns the id of the last term' do
        expect(Term.next_or_cur_id(@future.ends + 1.year)).to eq(@future.id)
      end
    end
  end

  describe '#include?' do
    context 'when date is included in the term' do
      it 'is true' do
        expect(@cur).to include(@cur.starts)
      end
    end

    context 'when date is not included in the term' do
      it 'is false' do
        expect(@cur).not_to include(@cur.starts - 1.day)
      end
    end
  end

  describe '#recent?' do
    context 'when date is included in the term' do
      it 'is true' do
        expect(@cur).to be_recent(@cur.starts)
      end
    end

    context 'when date is included in the next term' do
      it 'is true' do
        expect(@cur).to be_recent(@next.starts)
      end
    end

    context 'when date is not included in the current or next term' do
      it 'is false' do
        expect(@cur).not_to be_recent(@cur.starts - 1.day)
      end
    end
  end

  describe '#nearby?' do
    context 'when date is included in the term' do
      it 'is true' do
        expect(@cur).to be_nearby(@cur.starts)
      end
    end

    context 'when date is included in the next term' do
      it 'is true' do
        expect(@cur).to be_nearby(@next.starts)
      end
    end

    context 'when date is included in the previous term' do
      it 'is true' do
        expect(@cur).to be_nearby(@prev.starts)
      end
    end

    context 'when date is not included in a nearby term' do
      it 'is false' do
        expect(@cur).not_to be_nearby(@cur.starts - 2.years)
      end
    end
  end

  describe '#probably_next_date' do
    context 'when there is a following term' do
      it 'returns its start date' do
        binding.pry unless @cur.probably_next_date == @next.starts
        expect(@cur.probably_next_date).to eq(@next.starts)
      end
    end

    context 'when there is no following term' do
      context 'for a spring term' do
        it 'returns a date approximately 13 weeks after the term ends' do
          expect(@spring1.probably_next_date).to eq(@spring1.ends + 93.days)
        end
      end
    end
  end


end
