# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.describe Department, type: :model do
  def self.trasto_fields
    [:name]
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'

  describe 'Validations' do
    test_trasto_presence :name_i18n
  end
end
