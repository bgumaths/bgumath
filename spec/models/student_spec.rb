# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'
require 'models/generic_user_shared_examples'

RSpec.describe Phd, type: :model do
  include_examples 'regular includes'
  it_behaves_like 'a regular model'
  it_behaves_like 'a generic user'
  it_behaves_like 'a model with a users association', :supervisor
end

RSpec.describe Msc, type: :model do
  include_examples 'regular includes'
  it_behaves_like 'a regular model'
  it_behaves_like 'a generic user'
  it_behaves_like 'a model with a users association', :supervisor
end
