require 'rails_helper'
require 'models/shared_examples'

RSpec.describe IssueResponse, type: :model do
  def self.trasto_fields
    []
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'

  describe 'Validations' do
    test_presence :content
  end

  let(:issue) { create(:issue, :replied) }
  let(:response) { issue.response }


  context 'when the response is published' do
    it 'enqueues a save as pdf' do
      expect { response.publish! }.to have_enqueued_job(SaveIssuePdfJob)
    end
  end

  context 'when the response is updated without publishing' do
    it 'does not enqueue a save as pdf' do
      expect {
        response.update(content: 'foobar')
      }.to_not have_enqueued_job(SaveIssuePdfJob)
    end
  end

end


