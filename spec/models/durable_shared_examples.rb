# frozen_string_literal: true

require 'rails_helper'
require 'models/duration_shared_examples'

RSpec.shared_examples 'a durable model' do
  let(:lower) { DateTime.current.in_time_zone }

  include_examples 'generic duration'
end
