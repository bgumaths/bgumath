# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.shared_examples 'termed model includes' do

  def local_build(**opts)
    build(model_param, term: default_term, **opts)
  end
end

RSpec.shared_examples 'a termed model' do
  include_examples 'regular includes'
  include_examples 'termed model includes'

  describe 'Validations' do
    test_presence :term
  end

  describe 'Scopes:' do
    before do
      @obj = create(model_param, term: default_term)
      @obj1 = create(model_param, term: following_term)
      @obj2 = create(model_param, term: following_term(following_term))
    end

    describe '.this_term' do
      it "returns #{model_param.to_s.pluralize} for the given term" do
        expect(described_class.this_term(@obj.term)).to eq [@obj]
      end
    end

    describe '.current' do
      it "returns #{model_param.to_s.pluralize} for the given date" do
        expect(described_class.current(@obj.term.starts)).to eq [@obj]
      end
    end

    describe '.current_or_following' do
      it "returns #{model_param.to_s.pluralize} for the given date or following" do
        expect(described_class.current_or_following(@obj.term.starts)).to(
          match_array [@obj, @obj1, @obj2]
        )
      end
    end
    describe '.this_year' do
      it "returns #{model_param.to_s.pluralize} for the year of the given term" do
        items = @obj1.term.fall? ? [@obj1, @obj2] : [@obj, @obj1]
        items.each do |item|
          expect(described_class.this_year(item.term))
            .to match_array items
        end
      end
    end
  end
end

