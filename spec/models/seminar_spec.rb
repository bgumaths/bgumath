# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'
require 'models/termed_shared_examples'
require 'models/shiftable_shared_examples'

RSpec.describe Seminar, type: :model do
  def self.trasto_fields
    %i[name description]
  end

  include_examples 'regular includes'
  include_examples 'termed model includes'
  it_behaves_like 'a regular model'
  it_behaves_like 'a termed model'
  it_behaves_like 'a shiftable model'

  describe 'Validations' do
    test_trasto_weak_presence :name_i18n

    it 'is invalid with day out of range' do
      expect(build_object(day: 42)).not_to be_valid
    end
  end

  describe 'Scopes' do
    before(:context) do
      DatabaseCleaner.start
      I18n.locale = :en
      @term = default_term
      @nterm = @term.next || create(:term, :simple, :following, prev: @term)
      @sem1 = create(:seminar, name: 'foo seminar', term: @term)
      @nsem1 = create(:seminar, name: 'foo seminar', term: @nterm)
      @sem2 = create(:seminar, name: 'bar seminar', term: @term)
      @colloq = create(:seminar, name: 'Colloquium', term: @term)
      @ncolloq = create(:seminar, name: 'Colloquium', term: @nterm)
    end

    after(:context) { DatabaseCleaner.clean }

    describe '.colloq' do
      it 'returns all colloquiums on the model' do
        expect(Seminar.colloq).to match_array [@colloq, @ncolloq]
      end
      it 'returns the term colloquiums on a given term' do
        expect(@term.seminars.colloq).to match_array [@colloq]
        expect(@nterm.seminars.colloq).to match_array [@ncolloq]
      end
    end

    describe '.regular' do
      it 'returns all reqular seminars' do
        expect(Seminar.regular).to match_array [@sem2, @sem1, @nsem1]
      end
      it 'returns the term regular seminars on a given term' do
        expect(@term.seminars.regular).to match_array [@sem2, @sem1]
        expect(@nterm.seminars.regular).to match_array [@nsem1]
      end
    end
  end

  describe 'Date functions' do
    before do
      @first = Date.new(2016, 10, 5)
      @term = create(:term, :simple, starts: @first - 3.days, # Sunday
                     ends: @first + 13.weeks + 2.days)
      @sem = create(:seminar, day: 3, term: @term)
      @m1 = create(:meeting, seminar: @sem, date: @first)
      @m2 = create(:meeting, seminar: @sem, date: @m1.date + 1.week)
      @m3 = create(:meeting, seminar: @sem, date: @m2.date + 2.weeks)
      @m4 = create(:meeting, seminar: @sem, date: @m3.date + 6.days)
    end

    describe '#next_date' do
      context 'when argument is at most on the seminar day' do
        it 'returns the meeting date this week' do
          expect(@sem.next_date(@first - 1.day)).to eq @first
          expect(@sem.next_date(@first)).to eq @first
        end
      end

      context 'when argument is after the seminar day' do
        it 'returns the meeting date next week' do
          expect(@sem.next_date(@first + 1.day)).to eq @first + 1.week
        end
      end
    end

    describe '#first_available' do
      # TODO: test with extra terms, before: argument
      context 'when argument is before the term start' do
        it 'returns first available date of the term' do
          expect(@sem.first_available(@term.starts - 1.month))
            .to eq(@first + 2.weeks)
        end
      end

      context 'when argument is in the middle of the term' do
        it 'returns first available date from that date' do
          expect(@sem.first_available(@first + 3.weeks))
            .to eq(@first + 4.weeks)
        end
      end

      context 'when argument is in the middle of the term, allowing only one meeting per week' do
        it 'returns first available date from that date' do
          expect(@sem.first_available(@first + 3.weeks, one_per_week: true))
            .to eq(@first + 5.weeks)
        end
      end
    end

    describe '#default_date' do
      it 'returns the first available date for the seminar' do
        expect(@sem.default_date(@first + 3.weeks)).to eq @first + 4.weeks
      end
    end
  end

  describe '#create_ml_email' do
    let(:obj) { create(:seminar, :simple) }
    let(:creator) { create(:regular, :simple) }
    subject(:email) { obj.create_ml_email }
    before {
      creator.add_role 'Admin', obj
    }
    it 'creates an email with the correct fields' do
      expect(email.subject).to eq(
        I18n.t(:subject, scope: [:mailing_list_mailer, :create],
               list: obj.list_id)
      )
      expect(email[:reply_to].to_s).to(
        eq(creator.decorate.email_address(true))
      )
    end
  end
end

