# frozen_string_literal: true

require 'models/shared_examples'

RSpec.describe Job, type: :model do
  def self.trasto_fields
    []
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'

  describe 'Validations' do
    end
end


