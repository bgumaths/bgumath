# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.shared_examples 'a base event' do
  include_examples 'regular includes'

  def c_args(date)
    try(:create_args, date) || { starts: date }
  end

  describe 'Scopes' do
    before(:context) do
      DatabaseCleaner.start
      @now = Date.new(2016, 9, 30)
      @old = create_object(**c_args(@now - 2.years))
      @recent = create_object(**c_args(@now - 6.months))
      @cur = create_object(**c_args(@now - 5.days))
      @coming = create_object(**c_args(@now + 3.weeks))
      @future = create_object(**c_args(@now + 1.year))
    end

    after(:context) { DatabaseCleaner.clean }

    def self.add_scope(name, desc, expected)
      describe ".#{name}" do
        it "returns #{desc}" do
          name = [name, @now] unless name.is_a?(Array)
          expect(described_class.public_send(*name).asc).to eq(eval(expected))
        end
      end
    end

    describe '.before' do
      it "returns all #{model_params} starting before #{@now}" do
        expect(described_class.before(@now).asc).to eq([@old, @recent, @cur])
      end
    end

    describe '.from' do
      it "returns all #{model_params} starting on #{@now} or later" do
        expect(described_class.from(@now).asc).to eq([@coming, @future])
      end
    end

    describe '.between' do
      it "returns all #{model_params} between the two dates" do
        expect(described_class.between(@now - 1.week, @now + 2.months).asc)
          .to eq([@cur, @coming])
      end
    end

    describe '.on' do
      it "returns all #{model_params} on the give date" do
        expect(described_class.on(@now - 5.days).asc.all).to eq([@cur])
      end
    end

    describe '.this_week' do
      it "returns all #{model_params} on the week of the give date" do
        expect(described_class.this_week(@now - 5.days).asc.all).to eq([@cur])
      end
    end

    describe '.year' do
      it "returns all #{model_params} in the given academic year" do
        expect(described_class.year(@now.year + 1).asc.all).to eq([@cur, @coming])
      end
    end
  end
end
