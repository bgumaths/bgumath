require 'rails_helper'
require 'models/shared_examples'

RSpec.describe CarouselItem, type: :model do
  include_examples 'regular includes'

  describe 'Validations' do
    it_validates_presence_of :type
  end

  describe 'Scopes' do
    describe '.starts_before' do
      it 'returns the items activating before today' do
        skip 'TODO'
      end
    end

    describe '.ends_after' do
      it 'returns the items expiring after today' do
        skip 'TODO'
      end
    end

    describe '.current' do
      it 'returns the items that should be active now' do
        skip 'TODO'
      end
    end
  end

end

RSpec.describe UrlCarouselItem, type: :model do
  def self.trasto_fields
    []
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'

  describe 'Validations' do
    it_validates_presence_of :url
  end
end

RSpec.describe ResourceCarouselItem, type: :model do
  def self.trasto_fields
    []
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'
end

RSpec.describe ImageCarouselItem, type: :model do
  def self.trasto_fields
    []
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'

  describe 'Validations' do
    it_validates_presence_of :image
  end
end

