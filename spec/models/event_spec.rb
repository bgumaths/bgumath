# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'
require 'models/durable_shared_examples'
require 'models/base_event_shared_examples'

RSpec.describe Event, type: :model do
  it_behaves_like 'a regular model'

  it_behaves_like 'a durable model'

  it_behaves_like 'a base event'

  describe 'Scopes' do
    before do
      @past = create(:event, :past, centre: false)
      @recent = create(:event, :recent, centre: true)
      @future = create(:event, :future, centre: false)
      @expired = create(:event, :expired, centre: true)
    end

    describe '.recent' do
      it 'returns recent events' do
        expect(Event.recent).to eq [@recent]
      end
    end

    describe '.past' do
      it 'returns past events' do
        expect(Event.past).to eq [@past]
      end
    end

    describe '.expired' do
      it 'returns expired events' do
        expect(Event.expired).to eq [@expired]
      end
    end

    describe '.centre' do
      it 'returns events supported by the centre' do
        expect(Event.centre.desc).to eq [@recent, @expired]
      end
    end
  end
end
