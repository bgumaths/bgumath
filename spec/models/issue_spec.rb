# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.describe Issue, type: :model do
  def self.trasto_fields
    []
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'

  describe 'Validations' do
    test_presence :title, :content, :correspondence
  end

  describe 'before_create' do
    let(:department) { create(:department, people_count: 3) }
    let(:generic_course) {
      create(:generic_course, :basic, :simple, departments: [department])
    }
    let(:course) { create(:course, generic_course: generic_course) }
    let(:issue) { build(:issue, course: course) }

    it 'sets only active staff members' do
      staffs = issue.departments.map(&:people).flatten
      inactive = staffs.first
      inactive.active = false
      inactive.save
      issue.save
      issue.reload
      expect(issue.staffs).to match_array staffs - [inactive]
    end
  end


end
