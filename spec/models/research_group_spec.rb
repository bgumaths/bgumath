# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.describe ResearchGroup, type: :model do
  include_examples 'regular includes'
  it_behaves_like 'a regular model'

  it_behaves_like 'a model with a users association', :member

  describe 'Validations' do
    test_presence :name
    test_max_len name: NAME_MAX_LEN
  end
end
