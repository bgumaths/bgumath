# frozen_string_literal: true

require 'rails_helper'
require 'models/duration_shared_examples'

RSpec.shared_examples 'a ddurable model' do
  let(:lower) { Faker::Date.between(from: 0.days.ago, to: 0.days.ago) }

  include_examples 'generic duration'
end
