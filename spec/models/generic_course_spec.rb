# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.describe GenericCourse, type: :model do
  def self.trasto_fields
    %i[name description]
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'

  # ensure we have a date that falls within a term
  let(:today) { default_term_date }

  describe '#current' do
    let(:generic) { course.generic_course }
    let(:term) { course.term }
    subject { generic.current(default_term_date) }

    context 'when there is a current instance of this course' do
      let(:course) { create(:course, term: default_term) }

      before {
        create(
          :course, generic_course: generic, term: following_term
        ) unless generic.in_term(following_term)
      }

      it 'returns it' do
        expect(subject).to eq(course)
      end
    end

    context 'when there is no current instance of this course' do
      let(:course) {
        create(:course, :current, date: default_term_date(1.year.ago))
      }

      it 'returns nil' do
        expect(subject).to be_nil
      end
    end
  end

  describe '#current_or_next' do
    let(:generic) { course.generic_course }
    let(:term) { course.term }
    subject { generic.current_or_next(today) }

    context 'when there is a current instance of this course' do
      let(:course) { create(:course, :current, date: today) }

      before {
        # create another instance in the following term
        t = following_term(term)
        generic.in_term(t) ||
          create(:course, term: t, generic_course: generic)
      }

      it 'returns it' do
        expect(subject).to eq(course)
      end
    end

    context 'when there is no current instance of this course' do
      context 'but there is a future instance' do
        let(:course) {
          create(:course, :current, 
                 date: default_term_date(Time.zone.today + 6.months))
        }

        before {
          # create another instance in the following term
          t = following_term(term)
          generic.in_term(t) ||
            create(:course, term: t, generic_course: generic)
        }

        it 'returns the first one' do
          expect(subject).to eq(course)
        end
      end

      context 'and there is no future instance' do
        let(:course) {
          create(:course, :current, date: default_term_date(1.year.ago))
        }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end
    end
  end

  describe 'Validations' do
    test_trasto_presence :name_i18n

    # other validations cannot fail
    it 'is invalid when graduate is nil' do
      g = build(:generic_course, shid: nil, graduate: nil)
      expect(g).not_to be_valid
    end
  end
end

