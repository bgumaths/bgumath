# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.shared_examples 'generic duration' do
  include_examples 'regular includes'

  describe 'Validations' do
    test_presence :duration
  end

  describe '#duration=' do
    before do
      @object ||= build_object(duration: nil)
    end

    context 'when argument is a string' do
      it 'assigns the duration when the string gives a range' do
        st = lower
        en = st + 1.day
        @object.duration = "#{st}..#{en}"
        # time comparison directly doesn't work, probably because of
        # miliseconds
        expect(@object.duration.first.to_s).to eq st.to_s
        expect(@object.duration.last.to_s).to eq en.to_s
      end

      # skip this for the moment because of a bug in rails
      # https://github.com/rails/rails/pull/37648#issuecomment-749254618
      xit 'is invalid if the string is not a range' do
        @object.duration = 'foobar'
        expect(@object).not_to be_valid
      end

      it 'is invalid if the string is not a range of dates' do
        @object.duration = 'foo..bar'
        expect(@object).not_to be_valid
      end
    end

    context 'when argument is a hash' do
      it 'assigns a duration if format is correct' do
        st = lower
        en = st + 1.day
        @object.duration = { first: st, last: en }
        # time comparison directly doesn't work, probably because of
        # miliseconds
        expect(@object.duration.first.to_s).to eq st.to_s
        expect(@object.duration.last.to_s).to eq en.to_s
      end

      it 'is invalid if values are not datetime' do
        @object.duration = { first: 'foo', last: 'bar' }
        expect(@object).not_to be_valid
      end

      it 'is invalid if keys are wrong' do
        @object.duration = { foo: :bar }
        expect(@object).not_to be_valid
      end
    end

    context 'when argument is a range' do
      it 'assigns a duration if format is correct' do
        st = lower
        en = st + 1.day
        @object.duration = st..en
        # time comparison directly doesn't work, probably because of
        # miliseconds
        expect(@object.duration.first.to_s).to eq st.to_s
        expect(@object.duration.last.to_s).to eq en.to_s
      end

      it 'is invalid if range boundaries are wrong' do
        @object.duration = 1..3
        expect(@object).not_to be_valid
      end
    end
  end
end
