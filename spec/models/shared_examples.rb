# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'regular includes' do

  include_examples 'general includes'

  class << self
    def test_max_len(h)
      h.keys.each do |field|
        it "is invalid when #{field} is longer than #{h[field]}" do
          expect(
            build_object(field => 'foo' * h[field] + 'a')
          ).to_not be_valid
        end
      end
    end

    alias it_validates_max_lengths_of test_max_len

    def test_invalid_with(h)
      h.keys.each do |field|
        it "is invalid when #{field} is '#{h[field]}'" do
          expect(build_object(field => h[field])).to_not be_valid
        end
      end
    end

    alias it_validates_invalid_with test_invalid_with

    def test_presence(*fields)
      fields.each do |field|
        it "is invalid without a #{field}" do
          expect(build_object(field => nil)).to_not be_valid
        end
      end
    end

    alias it_validates_presence_of test_presence

    def test_trasto_presence(*fields)
      opts = fields.last.respond_to?(:has_key?) ? fields.pop : {}
      opts[:locales] ||= I18n.available_locales
      fields.each do |field|
        context "when #{field} has empty values" do
          it 'is invalid' do
            obj = build_object
            obj[field][opts[:locales].sample.to_s] = ''

            expect(obj).to_not be_valid
          end
        end
      end
    end

    alias it_validates_trasto_presence_of test_trasto_presence

    def test_trasto_weak_presence(*fields)
      opts = fields.last.respond_to?(:has_key?) ? fields.pop : {}
      opts[:locales] ||= I18n.available_locales
      fields.each do |field|
        context "when all values of #{field} are empty" do
          it 'is invalid' do
            expect(
              build_object(
                field => Hash[opts[:locales].map { |k| [k.to_s, ''] }])
            ).to_not be_valid
          end
        end
      end
    end

    alias it_validates_trasto_weak_presence_of test_trasto_weak_presence

    def test_presences(*fields)
      fields.each do |field|
        it "is invalid without a #{field}" do
          expect(build_object(field => [])).to_not be_valid
        end
      end
    end

    alias it_validates_presence_of_array test_presences

    # test that this is destroyed when any of the fields (owners) is
    def test_dep_destroy(*fields)
      fields.each do |field|
        it "is destroyed when '#{field}' is destroyed" do
          obj = create(model_param)
          expect(obj).to be_a(described_class)
          expect(obj).to be_valid
          owner = obj.public_send(field)
          owner.destroy
          expect(owner.public_send(model_params). find_by(id: obj.id)).to be_nil
        end
      end
    end

    alias it_validates_dep_destruction test_dep_destroy

    def model_param
      described_class.model_name.param_key.to_sym
    end

    def model_params
      described_class.model_name.collection
    end
  end

  def model_param
    self.class.model_param
  end

  def model_params
    self.class.model_params
  end

  def build_object(...)
    try(:local_build, ...) || build(model_param, ...)
  end

  def create_object(...)
    obj = build_object(...)
    obj.save! && obj
  end
end

RSpec.shared_examples 'a regular model' do
  include_examples 'regular includes'

  def self.tra_fields
    try(:trasto_fields) || []
  end

  let(:object) { build_object }

  it 'has a valid factory' do
    expect(object).to be_valid
  end

  tra_fields.each do |field|
    describe "##{field}" do

      I18n.available_locales.each do |loc|
        context "when locale is '#{loc}'" do
          it "returns the '#{loc}' #{field}" do
            I18n.locale = loc
            expect(object.public_send(field))
              .to eq object.public_send("#{field}_i18n")[loc.to_s]
          end
        end
      end
    end
  end
end

RSpec.shared_examples 'a model with a users association' do |field|
  describe "##{field}_emails=" do
    let(:object) {
      begin
        create_object(:simple)
      rescue KeyError
        create(model_param)
      end
    }

    let(:user) { create(field, :simple) }

    before { create(field, :simple) }

    it "sets #{field.to_s.pluralize} by emails" do
      object.public_send(
        "#{field}_emails=", [user.email, user.email + 'foobar']
      )
      expect(object.public_send(field.to_s.pluralize)).to eq([user])
    end
  end
end

RSpec.shared_examples 'a model with a user association' do |field|
  describe "##{field}_email=" do
    let(:object) {
      begin
        create_object(:simple)
      rescue KeyError
        create(model_param)
      end
    }

    let(:user) { create(field, :simple) }

    before { create(field, :simple) }

    it "sets #{field} by email" do
      object.public_send("#{field}_email=", user.email)
      expect(object.public_send(field)).to eq(user)
    end

    it "sets #{field} to nil if email does not exist" do
      object.public_send("#{field}_email=", user.email + 'foobar')
      expect(object.public_send(field)).to be_nil
    end
  end
end

