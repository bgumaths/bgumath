# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'
require 'models/ddurable_shared_examples'
require 'models/base_event_shared_examples'

RSpec.describe Leave, type: :model do
  def self.trasto_fields
    [:note]
  end

  include_examples 'regular includes'
  it_behaves_like 'a regular model'

  it_behaves_like 'a ddurable model'

  it_behaves_like 'a base event'

  describe 'Validations' do
    test_presence :user

    # does not work, for some reason...
    #test_dep_destroy :user

    it 'is destroyed when the user is destroyed' do
      user = create(:user, :onleave)
      l = user.leaves.first
      expect(l).to be_a(Leave)
      expect(l).to be_valid
      user.destroy
      expect(user.leaves.find_by(id: l.id)).to be_nil
    end
  end
end
