# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.describe Role, type: :model do
  include_examples 'regular includes'

  def self.trasto_fields
    %i[title description]
  end

  it_behaves_like 'a regular model'

  describe 'Validations' do
    test_presence :visibility
    test_max_len name: NAME_MAX_LEN
  end

  describe 'Scopes' do
    before do
      @r1 = create(:role, visibility: :auto, resource: nil)
      @r2 = create(:role, visibility: :open, resource: nil)
      @r3 = create(:role, visibility: :internal, resource: 
                   create(:seminar, term: default_term))
    end

    describe '.manual' do
      it 'returns all manual roles' do
        expect(Role.manual).to match_array([Role.admin, @r2, @r3])
      end
    end

    describe '.global' do
      it 'returns all roles not attached to a resource' do
        expect(Role.global).to match_array([Role.admin, @r1, @r2])
      end
    end
  end

  describe '#nearby?' do
    subject { role.nearby? }

    let(:term) { default_term }
    let(:resource) { build(:seminar, term: term) }
    let(:role) { build(:role, resource: resource) }

    context 'when global' do
      let(:resource) { nil }

      it { is_expected.to be_truthy }
    end

    context 'when attached to a non-termed resource' do
      let(:resource) { build(:research_group) }

      it { is_expected.to be_truthy }
    end

    context 'when attached to a current termed resource' do
      it { is_expected.to be_truthy }
    end

    context 'when attached to an old termed resource' do
      # create the current and prev terms, so that the old one is not 
      # current or nearby
      let(:curterm) { default_term }
      let(:prevterm) { create(:term, :simple, :past, prev: curterm) }
      let(:term) { build(:term, :simple, :past, prev: prevterm) }

      it { is_expected.to be_falsey }
    end
  end
end

