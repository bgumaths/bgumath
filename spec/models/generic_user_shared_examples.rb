# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.shared_examples 'a base user' do
  include_examples 'regular includes'

  def self.trasto_fields
    %i[first last]
  end
end

RSpec.shared_examples 'a generic user' do
  include_examples 'a base user'

  describe '#onleave?' do
    context 'when user has no leave' do
      it 'returns false' do
        user = create(model_param, :simple, leaves: [])
        expect(user).not_to be_onleave
      end
    end

    context 'when user has a leave' do
      before do
        @user = create(model_param, :simple, :onleave,
                       leave_starts: Time.zone.today,
                       leave_ends: Time.zone.today + 2.days)
      end

      it 'returns false for argument outside the leave' do
        expect(@user).not_to be_onleave(Time.zone.today + 4.days)
      end
      it 'returns true for argument in the leave' do
        expect(@user).to be_onleave(Time.zone.today)
      end
    end
  end

  describe '#onleave' do
    context 'when user has no leave' do
      it 'returns false' do
        user = create(model_param, :simple, leaves: [])
        expect(user.onleave).to be_falsey
      end
    end

    context 'when leave is not saved' do
      it 'returns false' do
        user = create(model_param, :simple)
        user.leaves.build
        expect(user.onleave).to be_falsey
      end
    end

    context 'when user has a saved leave' do
      it 'returns true' do
        user = create(model_param, :simple, :onleave)
        expect(user.onleave).to be_truthy
      end
    end
  end

  describe '#syspass' do
    context 'when no password exists' do
      it 'returns true' do
        user = create(model_param, :simple)
        expect(user.syspass).to be_truthy
      end
    end

    context 'when password exists' do
      it 'returns false' do
        user = create(model_param, :simple, :password)
        expect(user.syspass).to be_falsey
      end
    end
  end

  describe '#syspass=' do
    context 'when value is 1' do
      it 'removes the password' do
        user = create(model_param, :simple, :password)
        user.syspass = 1
        expect(user.password_digest).to be_nil
      end
    end

    context 'when value is not 1' do
      it 'does not change the password digest' do
        user = create(model_param, :simple, :password)
        prev = user.password_digest
        user.syspass = 0
        expect(user.password_digest).to eq(prev)
      end
    end
  end

  describe '#alive?' do
    context 'when user is alive' do
      it 'returns true' do
        user = build(model_param, :simple)
        expect(user).to be_alive
      end
    end

    context 'when user is deceased' do
      it 'returns false' do
        u1 = build(model_param, :simple, :deceased)
        u2 = build(model_param, :simple, state: :_deceased)
        expect(u1).not_to be_alive
        expect(u2).not_to be_alive
      end
    end
  end

  describe 'Validations' do
    test_presence :email

    it 'is invalid with a duplicate email' do
      uu = create(model_param, :simple)
      expect(build(model_param, :simple, email: uu.email)).not_to be_valid
    end

    it 'is invalid with more than one leave' do
      expect(build(model_param, :simple, leaves: build_list(:leave, 2))).not_to be_valid
    end
  end
end
