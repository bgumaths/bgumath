# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'
require 'models/shiftable_shared_examples'
require 'models/base_event_shared_examples'

RSpec.describe Meeting, type: :model do
  def self.trasto_fields
    [:announcement]
  end

  it_behaves_like 'a regular model'

  it_behaves_like 'a shiftable model'

  def create_args(date)
    term = default_term(date)
    seminar = Seminar.this_term(term).take ||
      create(:seminar, term: term)
    { starts: date, date: date, seminar: seminar }
  end

  def default_seminar
    @default_seminar ||= create(:seminar, term: default_term)
  end

  def local_build(**opts)
    build(model_param, seminar: default_seminar, **opts)
  end


  include_examples 'regular includes'
  it_behaves_like 'a base event'

  describe 'Validations' do
    test_presence :seminar

    test_dep_destroy :seminar

    it "is invalid when date is not in term" do
      # make sure the following term exists
      following_term
      expect(build_object(date: Date.today + 1.year)).to_not be_valid
    end
  end

  describe 'Scopes' do
    before(:context) do
      DatabaseCleaner.start
      I18n.locale = :en
      @t1 = default_term
      @t2 = following_term
      @s1 = create(:seminar, name: 'foo', term: @t1)
      @s2 = create(:seminar, name: 'foo', term: @t2)
      @s3 = create(:seminar, name: 'bar', term: @t1)
      @s4 = create(:seminar, name: 'baz', term: @t2)
      @m1 = create(:meeting, seminar: @s1)
      @m2 = create(:meeting, seminar: @s1, sdate: @m1.date + 1.day)
      @m3 = create(:meeting, seminar: @s2)
      @m4 = create(:meeting, seminar: @s3)
    end

    after(:context) { DatabaseCleaner.clean }

    describe '.of_seminar' do
      context 'when the seminar has meetings in one term' do
        it 'returns them' do
          expect(Meeting.of_seminar('bar')).to eq([@m4])
        end
      end

      context 'when the seminar has meetings in across terms' do
        it 'returns them all' do
          expect(Meeting.of_seminar('foo')).to eq([@m1, @m2, @m3])
        end
      end

      context 'when the seminar has no meetings' do
        it 'returns an empty list' do
          expect(Meeting.of_seminar('baz')).to be_empty
        end
      end
    end
  end
end

