# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.describe AcYear, type: :model do
  def self.trasto_fields
    []
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'

  describe 'Validations' do
  end
end
