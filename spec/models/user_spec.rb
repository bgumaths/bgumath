# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'
require 'models/generic_user_shared_examples'

RSpec.describe User, type: :model do
  before(:context) do
    @wadmin = User.admin || create(:virtual, :admin)
    @chair = create(:regular, :chair, :simple, email: 'chair')
    @center_dir = create(:regular, :center_dir, :simple, email: 'center_dir')
    @secretary = create(:admin, :secretary, email: 'secretary')
    @admin = create(:admin, :depadmin, email: 'sadmin') # admin is FriendlyId reserved
    @student_coord = create(:admin, :student_coord, email: 'student_coord')
    @regular = create(:regular, :simple, email: 'regular')
    @kamea = create(:kamea, :simple, email: 'kamea')
    @emeritus = create(:regular, :emeritus, :simple, email: 'emeritus')
    @oemeritus = create(:regular, :simple, state: :_retired, hours: nil, email: 'oemeritus')
    @future_retire = create(:regular, :simple, :future_retire, email: 'future_retire')
    @phd = create(:phd, supervisor_count: 1, supervisors: [@regular], email: 'phd')
    @past_phd = create(:phd, state: :_retired, supervisors: [@kamea], email: 'past_phd')
    @non_teach = create(:regular, :simple, hours: '', email: 'non_teach')
    @postdoc = create(:postdoc, hosts: [@regular], email: 'postdoc')
    @deceased = create(:regular, :simple, :deceased, email: 'deceased')
    @odeceased = create(:regular, :simple, state: :_deceased, email: 'odeceased')
    @adjunct = create(:adjunct, email: 'adjunct', mail_domain: 'unseen.bgu.ac.il')
    @onleave = create(:regular, :simple, :onleave, leave_starts: 1.week.ago, email: 'onleave')
    @all = [@chair, @regular, @kamea, @emeritus, @oemeritus, @future_retire,
            @phd, @past_phd, @non_teach, @postdoc, @admin, @deceased,
            @odeceased, @adjunct, @onleave, @center_dir, @secretary,
            @student_coord, @wadmin]
    @staff = [@admin, @secretary, @student_coord]
  end

  it_behaves_like 'a regular model'
  it_behaves_like 'a generic user'

  describe 'scopes' do
    def self.add_scope(name, desc, expected)
      describe ".#{name}" do
        it "returns #{desc}" do
          expect(User.public_send(name)).to match_array(eval(expected)) # rubocop: disable Security/Eval
        end
      end
    end

    add_scope :alive, 'living people', '@all - [@deceased, @odeceased]'
    add_scope :deceased, 'deceased people', '[@deceased, @odeceased]'
    add_scope :active, 'currently active (non-retired) people',
              '@all - [@deceased, @odeceased, @emeritus, @oemeritus, @past_phd]'
    add_scope :retired, 'retired people', '[@emeritus, @oemeritus, @past_phd]'
    add_scope :can_host, 'anyone who can have a visitor',
              '[@regular, @kamea, @onleave, @emeritus, @oemeritus, @future_retire, @non_teach, @chair, @center_dir]'
    add_scope :around, 'people who are around for teach etc',
              '@all - [@deceased, @odeceased, @past_phd]'
    add_scope :members, 'active fulltime members',
              '[@regular, @kamea, @non_teach, @onleave, @chair, @center_dir, @future_retire]'
    add_scope :regulars, 'active regular members',
              '[@regular, @non_teach, @onleave, @chair, @center_dir, @future_retire]'
    add_scope :kameas, 'active kamea members', '[@kamea]'
    add_scope :emeriti, 'retired fulltime members', '[@emeritus, @oemeritus]'
    add_scope :students, 'active students', '[@phd]'
    add_scope :past_students, 'past students', '[@past_phd]'
    add_scope :corporeal, 'biological people', '@all - [@wadmin]'
    add_scope :teachers, 'people with office hours',
              '[@regular, @kamea, @phd, @onleave, @adjunct, @past_phd, @chair, @center_dir, @future_retire]'
    add_scope :lecturers, 'people who can teach a course',
              '[@regular, @kamea, @non_teach, @adjunct, @onleave, @postdoc, @chair, @center_dir, @emeritus, @oemeritus, @future_retire]'
    add_scope :teaching, 'anyone who could have office hours',
              '[@regular, @kamea, @non_teach, @phd, @adjunct, @onleave, @postdoc, @chair, @center_dir, @emeritus, @oemeritus, @future_retire]'
    add_scope :academic, 'all acamdemics',
              '@all - [@admin, @wadmin, @secretary, @student_coord]'
    add_scope :on_leave, 'anyone currently on leave', '[@onleave]'
    add_scope :present, 'anyone not currently on leave', '@all - [@onleave]'
  end

  describe '#can_host?' do
    it 'returns true for users who can host' do
      User.can_host.each do |u|
        expect(u).to be_can_host
      end
    end

    it 'returns false for users who cannot host' do
      (@all - User.can_host).each do |u|
        expect(u).not_to be_can_host
      end
    end
  end

  describe '.chair' do
    it 'returns the department chair' do
      expect(User.chair).to eq @chair
    end
  end

  describe '.admin' do
    it 'returns the web admin' do
      expect(User.admin).to eq @wadmin
    end
  end

  describe '.secretaries' do
    it 'returns the secretaries' do
      expect(User.secretaries).to eq [@secretary]
    end
  end

  describe '.administrator' do
    it 'returns the department administrator' do
      expect(User.administrator).to eq @admin
    end
  end

  describe '.coordinator' do
    it 'returns the student coordinator' do
      expect(User.coordinator).to eq @student_coord
    end
  end

  describe '.director' do
    it 'returns the center director' do
      expect(User.director).to eq @center_dir
    end
  end

  describe '#is_staff?' do
    it 'is true when user has a staff role' do
      @staff.each do |u|
        expect(u).to be_is_staff
      end
    end

    it 'is false when user has no staff role' do
      (@all - @staff).each do |u|
        expect(u).not_to be_is_staff
      end
    end
  end

  describe '#mail_domain' do
    it 'is the default domain when unspecified' do
      expect(@regular.mail_domain).to eq Setting.mail_domain
    end

    it 'is the specified domain when specified' do
      expect(@adjunct.mail_domain).to eq 'unseen.bgu.ac.il'
    end
  end

end
