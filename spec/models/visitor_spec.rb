# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'
require 'models/ddurable_shared_examples'
require 'models/base_event_shared_examples'

RSpec.describe Visitor, type: :model do
  def self.trasto_fields
    %i[first last]
  end

  include_examples 'regular includes'

  it_behaves_like 'a regular model'

  it_behaves_like 'a ddurable model'

  it_behaves_like 'a base event'

  describe 'Validations' do
    test_trasto_weak_presence :first_i18n, :last_i18n
  end

  describe '.centre' do
    it 'returns the visitors supported by the centre' do
      cen = create(:visitor, centre: true)
      create(:visitor, centre: false)
      expect(Visitor.centre).to eq [cen]
    end
  end

  it_behaves_like 'a model with a users association', :host
end
