# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples'

RSpec.describe CourseList, type: :model do
  def self.trasto_fields
    %i[name remarks]
  end
  include_examples 'regular includes'
  it_behaves_like 'a regular model'
end
