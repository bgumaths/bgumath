# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe Teaching::CoursesController, type: :routing do
  include_examples 'a regular resource', :teaching, :courses
  include_examples 'a termed resource', :teaching, :courses
end
