# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe People::UsersController, type: :routing do
  include_examples 'a regular resource', :people, :users

  describe 'extra routing' do
    include_examples 'get route', :hours, 'teaching/hours', 'people/users', 'teaching_hours'
  end
end
