# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe UserSessionsController, type: :routing do
  describe 'routing' do
    include_examples 'generic route', :post, :create, 'login', 'user_sessions'
    include_examples 'get route', :destroy, 'logout', 'user_sessions', 'logout'
    include_examples 'generic route', :delete, :destroy, 'logout', 'user_sessions', 'logout'
  end
end
