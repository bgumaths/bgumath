#!/home/moshe/.rbenv/shims/ruby
# frozen_string_literal: true

require 'active_support/inflector'
namespace, resources = ARGV
cls = "#{namespace}/#{resources}_controller".camelize
puts <<~EOF
  require 'routing/shared_examples'

  RSpec.describe #{cls}, type: :routing do
    include_examples 'a regular resource', :#{namespace}, :#{resources}
  end
EOF
