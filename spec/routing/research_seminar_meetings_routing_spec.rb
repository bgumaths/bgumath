# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe Research::Seminar::MeetingsController, type: :routing do
  include_examples 'a nested resource', :research, :seminar, :meetings

  include_examples 'a termed nested resource', :research, :seminar, :meetings

  describe 'extra routing' do
    include_examples 'get route', :email,
                     'research/seminars/2/meetings/1/email',
                     'research/seminar/meetings', 'email_research_seminar_meeting',
                     seminar_id: '2', id: '1'
    include_examples 'get route', :email,
                     'research/fall2016/seminars/2/meetings/1/email',
                     'research/seminar/meetings', 'email_research_term_seminar_meeting',
                     seminar_id: '2', id: '1', term_id: 'fall2016'
  end
end
