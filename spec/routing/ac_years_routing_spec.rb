# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AcYearsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/ac_years').to route_to('ac_years#index')
    end

    it 'routes to #new' do
      expect(get: '/ac_years/new').to route_to('ac_years#new')
    end

    it 'routes to #show' do
      expect(get: '/ac_years/1').to route_to('ac_years#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/ac_years/1/edit').to route_to('ac_years#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/ac_years').to route_to('ac_years#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/ac_years/1').to route_to('ac_years#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/ac_years/1').to route_to('ac_years#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/ac_years/1').to route_to('ac_years#destroy', id: '1')
    end
  end
end
