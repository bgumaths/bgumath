# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe People::VisitorsController, type: :routing do
  describe 'routing' do
    include_examples 'get route', :index, 'people/visitors', 'people/postdocs'
    include_examples 'get route', :new, 'people/visitors/new', 'people/visitors', 'new_people_visitor'
    include_examples 'get route', :edit, 'people/visitors/1/edit', 'people/visitors', 'edit_people_visitor', id: '1'
    include_examples 'generic route', :post, :create, 'people/visitors', 'people/visitors'
    include_examples 'generic route', :put, :update, 'people/visitors/1', 'people/visitors', 'people_visitor', id: '1'
    include_examples 'generic route', :patch, :update, 'people/visitors/1', 'people/visitors', 'people_visitor', id: '1'
    include_examples 'generic route', :delete, :destroy, 'people/visitors/1', 'people/visitors', 'people_visitor', id: '1'
  end
end
