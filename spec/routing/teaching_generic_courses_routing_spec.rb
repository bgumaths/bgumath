# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe Teaching::GenericCoursesController, type: :routing do
  include_examples 'a regular resource', :teaching, :generic_courses
end
