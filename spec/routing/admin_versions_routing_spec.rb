# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe Admin::VersionsController, type: :routing do
  describe 'routing' do
    include_examples 'get route', :index, 'admin/versions', 'admin/versions'
    include_examples 'get route', :show, 'admin/versions/1', 'admin/versions', 'admin_version', id: '1'
    include_examples 'generic route', :delete, :destroy, 'admin/versions/1', 'admin/versions', 'admin_version', id: '1'
  end
end
