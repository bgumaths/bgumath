# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe Research::ResearchGroupsController, type: :routing do
  include_examples 'a regular resource', :research, :research_groups

  describe 'extra routing' do
    include_examples 'get route', :faculty, 'research/research_groups/faculty', 'research/research_groups', 'faculty_research_research_groups'
  end
end
