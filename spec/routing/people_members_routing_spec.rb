# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe People::MembersController, type: :routing do
  include_examples 'a regular resource', :people, :members

  describe 'extra routing' do
    include_examples 'get route', :research, 'research/faculty', 'people/members'
  end
end
