# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'generic route' do |verb, action, route, controller, prefix = nil, args = {}|
  context 'with locale' do
    it "routes '<loc>/#{route}' to #{controller}##{action} via :#{verb}" do
      I18n.available_locales.each do |loc|
        expect(verb => "#{loc}/#{route}").
          to route_to("#{controller}##{action}", { locale: loc.to_s }.merge(args))
      end
    end
  end

  context 'without locale' do
    it "routes '#{route}' to #{controller}##{action} via :#{verb}" do
      expect(verb => route).to route_to("#{controller}##{action}", args)
    end
  end

  unless args[:format]
    context 'with format' do
      it "routes '#{route}.json' to #{controller}##{action} via :#{verb}" do
        expect(verb => "#{route}.json").
          to route_to("#{controller}##{action}", { format: 'json' }.merge(args))
      end
    end
  end

  context 'with named routes' do
    it "routes the path given by #{(prefix || route.sub('/', '_')) + '_path'} to #{controller}##{action} via :#{verb}" do
      expect(verb => public_send((prefix || route.sub('/', '_')) + '_path', { locale: I18n.locale }.merge(args))).
        to route_to("#{controller}##{action}", { locale: I18n.locale.to_s }.merge(args))
    end
  end
end

RSpec.shared_examples 'get route' do |*args|
  include_examples 'generic route', :get, *args
end

RSpec.shared_examples 'a regular resource' do |namespace, resources|
  describe 'routing' do
    include_examples 'get route', :index, "#{namespace}/#{resources}", "#{namespace}/#{resources}"
    include_examples 'get route', :new, "#{namespace}/#{resources}/new", "#{namespace}/#{resources}", "new_#{namespace}_#{resources.to_s.singularize}"
    include_examples 'get route', :show, "#{namespace}/#{resources}/1", "#{namespace}/#{resources}", "#{namespace}_#{resources.to_s.singularize}", id: '1'
    include_examples 'get route', :edit, "#{namespace}/#{resources}/1/edit", "#{namespace}/#{resources}", "edit_#{namespace}_#{resources.to_s.singularize}", id: '1'
    include_examples 'generic route', :post, :create, "#{namespace}/#{resources}", "#{namespace}/#{resources}"
    include_examples 'generic route', :put, :update, "#{namespace}/#{resources}/1", "#{namespace}/#{resources}", "#{namespace}_#{resources.to_s.singularize}", id: '1'
    include_examples 'generic route', :patch, :update, "#{namespace}/#{resources}/1", "#{namespace}/#{resources}", "#{namespace}_#{resources.to_s.singularize}", id: '1'
    include_examples 'generic route', :delete, :destroy, "#{namespace}/#{resources}/1", "#{namespace}/#{resources}", "#{namespace}_#{resources.to_s.singularize}", id: '1'
  end
end

RSpec.shared_examples 'a nested resource' do |namespace, nesting, resources|
  describe 'routing' do
    include_examples 'get route', :index,
                     "#{namespace}/#{nesting.to_s.pluralize}/2/#{resources}",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_#{nesting}_#{resources}",
                     "#{nesting}_id".to_sym => '2'
    include_examples 'get route', :new,
                     "#{namespace}/#{nesting.to_s.pluralize}/2/#{resources}/new",
                     "#{namespace}/#{nesting}/#{resources}",
                     "new_#{namespace}_#{nesting}_#{resources.to_s.singularize}",
                     "#{nesting}_id".to_sym => '2'
    include_examples 'get route', :show,
                     "#{namespace}/#{nesting.to_s.pluralize}/2/#{resources}/1",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_#{nesting}_#{resources.to_s.singularize}", id: '1',
                                                                              "#{nesting}_id".to_sym => '2'
    include_examples 'get route', :edit,
                     "#{namespace}/#{nesting.to_s.pluralize}/2/#{resources}/1/edit",
                     "#{namespace}/#{nesting}/#{resources}",
                     "edit_#{namespace}_#{nesting}_#{resources.to_s.singularize}", id: '1',
                                                                                   "#{nesting}_id".to_sym => '2'
    include_examples 'generic route', :post, :create,
                     "#{namespace}/#{nesting.to_s.pluralize}/2/#{resources}",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_#{nesting}_#{resources}",
                     "#{nesting}_id".to_sym => '2'
    include_examples 'generic route', :put, :update,
                     "#{namespace}/#{nesting.to_s.pluralize}/2/#{resources}/1",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_#{nesting}_#{resources.to_s.singularize}", id: '1',
                                                                              "#{nesting}_id".to_sym => '2'
    include_examples 'generic route', :patch, :update,
                     "#{namespace}/#{nesting.to_s.pluralize}/2/#{resources}/1",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_#{nesting}_#{resources.to_s.singularize}", id: '1',
                                                                              "#{nesting}_id".to_sym => '2'
    include_examples 'generic route', :delete, :destroy,
                     "#{namespace}/#{nesting.to_s.pluralize}/2/#{resources}/1",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_#{nesting}_#{resources.to_s.singularize}", id: '1',
                                                                              "#{nesting}_id".to_sym => '2'
  end
end

RSpec.shared_examples 'a termed resource' do |namespace, resources|
  describe 'termed routing' do
    include_examples 'get route', :index,
                     "#{namespace}/fall2016/#{resources}",
                     "#{namespace}/#{resources}",
                     "#{namespace}_term_#{resources}",
                     term_id: 'fall2016'
    include_examples 'get route', :new,
                     "#{namespace}/fall2016/#{resources}/new",
                     "#{namespace}/#{resources}",
                     "new_#{namespace}_term_#{resources.to_s.singularize}",
                     term_id: 'fall2016'
    include_examples 'get route', :show,
                     "#{namespace}/fall2016/#{resources}/1",
                     "#{namespace}/#{resources}",
                     "#{namespace}_term_#{resources.to_s.singularize}", id: '1',
                                                                        term_id: 'fall2016'
    include_examples 'get route', :edit,
                     "#{namespace}/fall2016/#{resources}/1/edit",
                     "#{namespace}/#{resources}",
                     "edit_#{namespace}_term_#{resources.to_s.singularize}", id: '1',
                                                                             term_id: 'fall2016'
    include_examples 'generic route', :post, :create,
                     "#{namespace}/fall2016/#{resources}",
                     "#{namespace}/#{resources}",
                     "#{namespace}_term_#{resources}",
                     term_id: 'fall2016'
    include_examples 'generic route', :put, :update,
                     "#{namespace}/fall2016/#{resources}/1",
                     "#{namespace}/#{resources}",
                     "#{namespace}_term_#{resources.to_s.singularize}", id: '1',
                                                                        term_id: 'fall2016'
    include_examples 'generic route', :patch, :update,
                     "#{namespace}/fall2016/#{resources}/1",
                     "#{namespace}/#{resources}",
                     "#{namespace}_term_#{resources.to_s.singularize}", id: '1',
                                                                        term_id: 'fall2016'
    include_examples 'generic route', :delete, :destroy,
                     "#{namespace}/fall2016/#{resources}/1",
                     "#{namespace}/#{resources}",
                     "#{namespace}_term_#{resources.to_s.singularize}", id: '1',
                                                                        term_id: 'fall2016'
  end
end

RSpec.shared_examples 'a termed nested resource' do |namespace, nesting, resources|
  describe 'routing' do
    include_examples 'get route', :index,
                     "#{namespace}/fall2016/#{nesting.to_s.pluralize}/2/#{resources}",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_term_#{nesting}_#{resources}",
                     "#{nesting}_id".to_sym => '2', term_id: 'fall2016'
    include_examples 'get route', :new,
                     "#{namespace}/fall2016/#{nesting.to_s.pluralize}/2/#{resources}/new",
                     "#{namespace}/#{nesting}/#{resources}",
                     "new_#{namespace}_term_#{nesting}_#{resources.to_s.singularize}",
                     "#{nesting}_id".to_sym => '2', term_id: 'fall2016'
    include_examples 'get route', :show,
                     "#{namespace}/fall2016/#{nesting.to_s.pluralize}/2/#{resources}/1",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_term_#{nesting}_#{resources.to_s.singularize}", id: '1',
                                                                                   "#{nesting}_id".to_sym => '2', term_id: 'fall2016'
    include_examples 'get route', :edit,
                     "#{namespace}/fall2016/#{nesting.to_s.pluralize}/2/#{resources}/1/edit",
                     "#{namespace}/#{nesting}/#{resources}",
                     "edit_#{namespace}_term_#{nesting}_#{resources.to_s.singularize}", id: '1',
                                                                                        "#{nesting}_id".to_sym => '2', term_id: 'fall2016'
    include_examples 'generic route', :post, :create,
                     "#{namespace}/fall2016/#{nesting.to_s.pluralize}/2/#{resources}",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_term_#{nesting}_#{resources}",
                     "#{nesting}_id".to_sym => '2', term_id: 'fall2016'
    include_examples 'generic route', :put, :update,
                     "#{namespace}/fall2016/#{nesting.to_s.pluralize}/2/#{resources}/1",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_term_#{nesting}_#{resources.to_s.singularize}", id: '1',
                                                                                   "#{nesting}_id".to_sym => '2', term_id: 'fall2016'
    include_examples 'generic route', :patch, :update,
                     "#{namespace}/fall2016/#{nesting.to_s.pluralize}/2/#{resources}/1",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_term_#{nesting}_#{resources.to_s.singularize}", id: '1',
                                                                                   "#{nesting}_id".to_sym => '2', term_id: 'fall2016'
    include_examples 'generic route', :delete, :destroy,
                     "#{namespace}/fall2016/#{nesting.to_s.pluralize}/2/#{resources}/1",
                     "#{namespace}/#{nesting}/#{resources}",
                     "#{namespace}_term_#{nesting}_#{resources.to_s.singularize}", id: '1',
                                                                                   "#{nesting}_id".to_sym => '2', term_id: 'fall2016'
  end
end
