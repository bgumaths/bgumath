# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe Admin::RolesController, type: :routing do
  include_examples 'a regular resource', :admin, :roles
end
