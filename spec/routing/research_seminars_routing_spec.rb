# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe Research::SeminarsController, type: :routing do
  include_examples 'a regular resource', :research, :seminars

  include_examples 'a termed resource', :research, :seminars

  describe 'extra routing' do
    include_examples 'get route', :weekly, 'research/weekly', 'research/seminars'
  end
end
