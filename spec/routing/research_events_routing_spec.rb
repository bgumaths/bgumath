# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe Research::EventsController, type: :routing do
  include_examples 'a regular resource', :research, :events
end
