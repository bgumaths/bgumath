# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe People::VirtualsController, type: :routing do
  include_examples 'a regular resource', :people, :virtuals
end
