# frozen_string_literal: true

require 'routing/shared_examples'

RSpec.describe Teaching::TermsController, type: :routing do
  describe 'routing' do
    include_examples 'get route', :index,
                     'teaching',
                     'teaching/terms',
                     'teaching_terms'
    include_examples 'get route', :new,
                     'teaching/new',
                     'teaching/terms',
                     'new_teaching_term'
    include_examples 'get route', :show,
                     'teaching/fall2016',
                     'teaching/terms',
                     'teaching_term',
                     id: 'fall2016'
    include_examples 'get route', :edit,
                     'teaching/fall2016/edit',
                     'teaching/terms',
                     'edit_teaching_term',
                     id: 'fall2016'
    include_examples 'generic route', :post, :create,
                     'teaching',
                     'teaching/terms',
                     'teaching_terms'
    include_examples 'generic route', :put, :update,
                     'teaching/fall2016',
                     'teaching/terms',
                     'teaching_term',
                     id: 'fall2016'
    include_examples 'generic route', :patch, :update,
                     'teaching/fall2016',
                     'teaching/terms',
                     'teaching_term',
                     id: 'fall2016'
    include_examples 'generic route', :delete, :destroy,
                     'teaching/fall2016',
                     'teaching/terms',
                     'teaching_term',
                     id: 'fall2016'
  end

  describe 'extra routing' do
    include_examples 'get route', :populate, 'teaching/fall2016/populate', 'teaching/terms', 'populate_teaching_term', id: 'fall2016'
    include_examples 'get route', :show, 'teaching/term', 'teaching/terms', 'teaching_cterm'
    include_examples 'get route', :edit, 'teaching/term/edit', 'teaching/terms', 'edit_teaching_cterm'
    include_examples 'generic route', :patch, :update, 'teaching/term', 'teaching/terms', 'teaching_cterm'
    include_examples 'generic route', :put, :update, 'teaching/term', 'teaching/terms', 'teaching_cterm'
  end
end
