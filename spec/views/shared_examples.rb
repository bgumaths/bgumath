# frozen_string_literal: true

require 'rails_helper'
require 'support/default_url'

RSpec.shared_examples 'view includes' do
  before do
    %i[controller action].each do |x|
      params[x] = controller.request.path_parameters[x]
    end
    view.lookup_context.prefixes.unshift controller.controller_path
    allow(view).to receive(:policy).and_return(
      double(edit?: false, destroy?: false)
    )
    def view.title_opts
      {}
    end
    def view.object_cache_key(foo)
      []
    end
  end
end
