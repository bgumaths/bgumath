# frozen_string_literal: true

require 'views/shared_examples'

RSpec.describe 'people/hours.html.erb' do
  before do
    def view.title_opts
      { locale: :he }
    end

    def view.title_id
      'people.users#hours'
    end
  end

  include_examples 'view includes'

  context 'when there are no teachers' do
    it 'displays no table' do
      assign(:teachers, [])
      render
      expect(rendered).not_to have_css('table')
    end
  end

  context 'when there are teachers' do
    it 'displays a table with office hours' do
      assign(:teachers, create_list(:academic, 20).map(&:decorate))
      render
      # office hours always in he
      expect(rendered)
        .to have_css('div', text: tt('teaching.hours', locale: :he))
    end
  end

  it 'has a title' do
    assign(:teachers, create_list(:academic, rand(10..20)).map(&:decorate))
    render
    # office hours always in he
    expect(rendered).to have_css('h1', text: tt('teaching.hours', locale: :he))
    #.and have_css( 'title', text: 'BGU Math department | Office Hours')
  end
end
