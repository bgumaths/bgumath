# frozen_string_literal: true

require 'views/shared_examples'

RSpec.describe 'people/research.html.erb' do
  before do
    view.lookup_context.prefixes.unshift 'people/members'
  end

  include_examples 'view includes'

  it 'has a title' do
    assign(:members, create_list(:regular, rand(10..20)).map(&:decorate))
    render
    expect(rendered).to have_css('h1', tt('research.faculty'))
  end
end
