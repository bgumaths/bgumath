# frozen_string_literal: true

RSpec.shared_examples 'people preamble' do
  def self.include_ex
    [:people, model_param]
  end

  def self.object_name
    'person'
  end

  def self.objects_name
    'people'
  end

  # build an active user so that ml_file is determined
  let(:local_build) { build(model_param, state: :_active) }

  let(:local_valid) {
    build_object.to_perm_params.merge(status: build_object.status)
  }

  let(:invalid_attributes) { valid_attributes.merge(email: nil) }

  let(:new_attributes) do
    {
      email: 'duder@big',
      rank: 'Prof',
    }
  end

  let(:ml_file) do
    File.file?(build_object.class.ml_file) ? build_object.class.ml_file
    : File.file?(model.ml_file) ? model.ml_file : nil
  end
end

RSpec.shared_examples 'people create' do
  # cancel ML lists for now, since DB is not cleaned properly (TODO)
  context 'Mailing Lists', if: false do
    context 'with valid params' do
      it 'generates an updated mailing list' do
        spost :create, params: { model_param => valid_attributes }
        if ml_file
          expect(File.read(ml_file).chomp).to eq(
            valid_attributes[:email].downcase
          )
        end
      end
    end

    context 'with invalid params' do
      it 'generates an empty mailing list' do
        spost :create, params: { model_param => invalid_attributes }
        expect(File.read(ml_file).chomp).to be_empty if ml_file
      end
    end
  end

  context 'with valid params' do
    it 'creates user with correct roles' do
      role1 = create(:role, visibility: :open)
      create(:role, visibility: :open)
      spost :create, params: { model_param => valid_attributes.merge(admins: [role1.id]), use_route: :people_users }
      expect(controller.send(objname)).to have_role(role1.name)
    end
  end
end

RSpec.shared_examples 'people update' do
  context 'Mailing Lists', if: false do
    context 'with valid params' do
      it 'generates an updated mailing list' do
        object = create_object
        sput :update, params: {
          id: object.to_param, model_param => new_attributes,
        }
        if ml_file
          expect(File.read(ml_file).chomp).to eq(
            new_attributes[:email].downcase
          )
        end
      end
    end

    context 'with invalid params' do
      it 'generates an updated mailing list' do
        object = create_object
        sput :update, params: {
          id: object.to_param, model_param => invalid_attributes,
        }
        if ml_file
          expect(File.read(ml_file).chomp).to eq(
            valid_attributes[:email].downcase
          )
        end
      end
    end
  end

  before do
    @seminar = create(:seminar, term: default_term)
    @role1 = create(:role, visibility: :open)
    @role2 = create(:role, visibility: :open)
    @object.add_role :Admin, @seminar
    @object.add_role @role1.name
  end

  context 'with valid params' do
    it 'updates roles correctly' do
      sput :update, params: @object.to_find_param(model_param => new_attributes.merge(admins: [@role2.id]), use_route: :edit_people_users)
      expect(controller.send(objname)).to have_role(@role2.name)
      expect(controller.send(objname)).to have_role(:Admin, @seminar)
    end
  end
end

RSpec.shared_examples 'people destroy' do

  context 'Mailing Lists', if: false do
    it 'generates an updated mailing list' do
      object = create_object
      sdelete :destroy, params: { id: object.to_param, use_route: :delete_people_users }
      expect(File.read(ml_file).chomp).to eq '' if ml_file
    end
  end
end
