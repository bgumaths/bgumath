# frozen_string_literal: true

require 'rails_helper'
require 'authlogic/test_case'
include Authlogic::TestCase
require 'rails'
include Rails.application.routes.url_helpers
default_url_options[:host] = 'test.host'
#default_url_options[:locale] = I18n.locale
default_url_options[:protocol] = 'https'

RSpec.configure do |c|
  c.before(:example, type: :controller) do
    request.env['HTTPS'] = 'on'
    default_url_options[:locale] = I18n.locale
    default_url_options[:host] = 'test.host'
    default_url_options[:protocol] = 'https'
  end
end

RSpec.shared_examples 'a regular controller' do
  def self.test_invalid
    respond_to?(:skip_invalid) ? !skip_invalid : true
  end

  def self.model
    try(:mmodel) || controller_class.model
  end

  def self.objnames
    try(:objects_name) || model.model_name.plural
  end

  def self.objname
    try(:object_name) || model.model_name.singular
  end

  def self.model_param
    try(:mmodel_param) || controller_class.model_param
  end

  def self.def_params(**par)
    (try(:default_params) || {}).deep_merge(par)
  end

  def model
    self.class.model
  end

  def objnames
    self.class.objnames
  end

  def objname
    self.class.objname
  end

  def model_param
    self.class.model_param
  end

  def def_params(*args)
    self.class.def_params(*args)
  end

  def index_location(**opts)
    super
  rescue NoMethodError
    { action: 'index', controller: controller_class_name }.deep_merge(opts)
  end

  def object_location(obj, **opts)
    #url_for({
    #  action: :show, controller: controller_class_name
    #}.merge(obj.to_find_param(opts)))
    action = opts[:action] || 'show'
    obj.decorate.try("#{action}_url", 
                     {host: 'test.host', protocol: 'https'}.deep_merge(opts))
  end

  def self.includes
    try(:include_ex) || [model_param]
  end

  def default_term
    Term.current || create(:term, :simple, :current)
  end


  let(:build_object) do
    try(:local_build) || build(model_param)
  end

  let(:valid_attributes) do
    try(:local_valid) || build_object.to_perm_params
  end

  let(:create_object) do
    obj = build_object
    obj.save! && obj
  end

  def self.include_opt(action)
    includes.each do |c|
      begin
        include_examples "#{c} #{action}"
      rescue ArgumentError
      end
    end
  end

  def self.include_opt_context(action)
    include_context "#{action} context"
  rescue ArgumentError
  end

  def mget(*args)
    sget(*args)
  rescue ActionController::UrlGenerationError
    skip 'route does not exist'
  end

  def mpost(*args)
    spost(*args)
  rescue ActionController::UrlGenerationError
    skip 'route does not exist'
  end

  def mput(*args)
    sput(*args)
  rescue ActionController::UrlGenerationError
    skip 'route does not exist'
  end

  def mdelete(*args)
    sdelete(*args)
  rescue ActionController::UrlGenerationError
    skip 'route does not exist'
  end


  describe 'GET #index' do
    setup :activate_authlogic
    include_opt_context :index
    it "assigns all #{objnames} to 'bare_objects'" do
      object = create_object
      mget :index, params: def_params
      expect(controller.send(:bare_objects).all).to eq([object])
    end

    it "assigns a new #{objname} to 'form_object'" do
      mget :index, params: def_params
      expect(controller.send(:form_object)).to be_a_new(model)
    end
    include_opt :index
  end

  describe 'GET #show' do
    setup :activate_authlogic
    include_opt_context :show
    it "assigns the requested #{objname} as bare_object" do
      object = create_object
      mget :show, params: object.to_find_param
      expect(controller.send(:bare_object)).to eq(object)
    end
    include_opt :show
  end

  describe 'GET #new' do
    setup :activate_authlogic
    include_opt_context :new
    it "assigns a new #{objname} as 'bare_object'" do
      mget :new, params: def_params
      expect(controller.send(:bare_object)).to be_a_new(model)
    end

    context 'with parameters' do
      it "initialises the new #{objname} with the given parameters" do
        mget :new, params: def_params(model_param => new_attributes)
        new_attributes.each do |k, v|
          expect(controller.send(:bare_object).public_send(k)).to eq(v)
        end
      end
    end

    include_opt :new
  end

  describe 'GET #edit' do
    setup :activate_authlogic
    include_opt_context :edit

    it "assigns the requested #{objname} as 'bare_object'" do
      object = create_object
      mget :edit, params: object.to_find_param
      expect(controller.send(:bare_object)).to eq(object)
    end

    # we don't seem to use the following functionality anymore
#    context 'with parameters' do
#      it "initialises the #{objname} with the given parameters" do
#        object = create_object
#        mget :edit, params: object.to_find_param(
#          model_param => new_attributes
#        )
#        new_attributes.each do |k, v|
#          expect(controller.send(:bare_object).public_send(k)).to eq(v)
#        end
#      end
#    end

    include_opt :edit
  end

  describe 'POST #create' do
    setup :activate_authlogic
    include_opt_context :create

    context 'with valid params' do
      it "creates a new #{objname}" do
        expect do
          mpost :create, params: def_params(model_param => valid_attributes)
        end.to change(model, :count).by(1)
      end

      it "assigns a newly created #{objname} as 'bare_object'" do
        mpost :create, params: def_params(model_param => valid_attributes)
        expect(controller.send(:bare_object)).to be_a(model)
      end

      it "persists the newly created #{objname} as 'bare_object'" do
        mpost :create, params: def_params(model_param => valid_attributes)
        expect(controller.send(:bare_object)).to be_persisted
      end

      it "sets the creator of the new #{objname}" do
        mpost :create, params: def_params(model_param => valid_attributes)
        creator = User.with_role(:creator, controller.send(:bare_object)).first
        expect(creator).to eq(controller.send(:current_user))
      end

      it 'redirects to the object page' do
        mpost :create, params: def_params(model_param => valid_attributes)
        expect(response).to redirect_to(object_location(controller.send(:bare_object)))
      end
    end

    context 'with invalid params', if: test_invalid do
      it "assigns a newly created but unsaved #{objname} as 'bare_object'" do
        mpost :create, params: def_params(model_param => invalid_attributes)
        expect(controller.send(:bare_object)).to be_a_new(model)
      end

      it "renders the 'new' action" do
        mpost :create, params: def_params(model_param => invalid_attributes)
        expect(response).to render_template(:new)
      end
    end

    context 'with existing slug' do
      before do
        @object = create_object
      end

      it "switches to editing the existing #{objname}" do
        if @object.respond_to?(:slug)
          mpost :create,
            params: def_params(
              model_param => valid_attributes.merge(slug: @object.slug)
          )
          expect(response).to redirect_to(object_location(
            @object, action: :edit
          ))
        else
          puts "        SKIPPING since object does not respond to :slug:"
        end
      end
    end

    include_opt :create
  end

  describe 'PUT #update' do
    setup :activate_authlogic
    include_opt_context :update
    before do
      @object = create_object
    end

    context 'with valid params' do
      with_versioning do
        it "updates the requested #{objname}" do
          mput :update, params: @object.to_find_param(
            model_param => new_attributes
          )

          @object.reload
          new_attributes.each do |k, v|
            expect(@object.public_send(k)).to eq(v)
          end
        end

        it 'keeps a version with the old params' do
          mput :update, params: @object.to_find_param(
            model_param => new_attributes
          )

          new_attributes.each do |k, v|
            old = @object.public_send(k)
            expect(@object).to have_a_version_with k => old unless old == v
          end
        end

        it "assigns the requested #{objname} as 'bare_object'" do
          mput :update, params: @object.to_find_param(
            model_param => valid_attributes
          )
          expect(controller.send(:bare_object)).to eq(@object)
        end

        it 'redirects to the object page' do
          mput :update, params: @object.to_find_param(
            model_param => valid_attributes
          )
          expect(response).to redirect_to(object_location(@object))
        end
      end
    end

    context 'with invalid params', if: test_invalid do
      it "assigns the #{objname} as 'bare_object'" do
        mput :update, params: @object.to_find_param(
          model_param => invalid_attributes
        )
        expect(controller.send(:bare_object)).to eq(@object)
      end

      it "renders the 'edit' action" do
        mput :update, params: @object.to_find_param(
          model_param => invalid_attributes
        )
        expect(response).to render_template(:edit)
      end
    end

    include_opt :update
  end

  describe 'DELETE #destroy' do
    setup :activate_authlogic
    include_opt_context :destroy
    before { @object = create_object }

    it "destroys the requested #{objname}" do
      expect do
        mdelete :destroy, params: @object.to_find_param
      end.to change(model, :count).by(-1)
    end

    it "redirects to the #{objnames} index" do
      mdelete :destroy, params: @object.to_find_param
      expect(response).to redirect_to(index_location.merge(locale: I18n.locale))
    end

    include_opt :destroy
  end
end
