# frozen_string_literal: true

RSpec.shared_context 'new context' do
  before { create(:term, :current, :simple) }
end

RSpec.shared_context 'create context' do
  before { create(:term, :current, :simple) }
end

RSpec.shared_context 'index context' do
  before { create(:term, :current, :simple) }
end
