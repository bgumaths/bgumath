# frozen_string_literal: true

require 'controllers/shared_examples'

RSpec.describe Research::ResearchGroupsController, type: :controller do
  let(:invalid_attributes) { valid_attributes.merge(name: nil) }

  let(:new_attributes) do
    {
      name: 'foobar',
    }
  end

  it_behaves_like 'a regular controller'
end
