# frozen_string_literal: true

require 'controllers/people_shared_examples'
require 'controllers/shared_examples'

RSpec.describe People::AdjunctsController, type: :controller do
  include_examples 'people preamble'
  it_behaves_like 'a regular controller'
end
