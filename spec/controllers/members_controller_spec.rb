# frozen_string_literal: true

require 'controllers/people_shared_examples'
require 'controllers/shared_examples'

RSpec.shared_examples 'member index' do
  it 'assigns other members' do
    kamea = create(:kamea, :simple, state: :_active)
    emeritus = create(:regular, :emeritus, :simple)
    deceased = create(:regular, :deceased, :simple)
    sget :index
    expect(controller.send('kamea').object.all).to eq([kamea])
    expect(controller.send('emeriti').object.all).to eq([emeritus])
    expect(controller.send('deceased').object.all).to eq([deceased])
  end
end

RSpec.describe People::MembersController, type: :controller do
  include_examples 'people preamble'

  # cannot build a member, since it's abstract
  let(:local_build) { build(:regular, state: :_active) }

  # status should be Regular after massage_params in members_controller
  let(:invalid_attributes) { valid_attributes.merge(email: nil, status: 'Regular') }

  it_behaves_like 'a regular controller'

  describe 'GET #research' do
    it 'assigns all members to @members' do
      member = create(:regular, :simple, state: :_active)
      get :research
      expect(controller.send('members')).to eq([member])
    end
  end
end
