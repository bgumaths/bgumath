# frozen_string_literal: true

require 'controllers/shared_examples'
require 'controllers/term_shared_context'
require 'controllers/term_shared_examples'

RSpec.describe Research::SeminarsController, type: :controller do
  let(:invalid_attributes) do
    valid_attributes.merge(day: 8)
  end

  let(:new_attributes) do
    {
      name_i18n: { 'en' => 'some new seminar' },
    }
  end

  include_examples 'termwise preamble'

  it_behaves_like 'a regular controller'

  describe 'Extra Vars' do
    before(:context) do
      DatabaseCleaner.start
      @prev = create(:term, starts: Date.current - 6.months)
      @cur = create(:term, :following, prev: @prev)
      @future = create(:term, :following, prev: @cur)
      @dfuture = create(:term, :following, prev: @future)
      @seminar = create(:seminar, term: @cur, meeting_count: 3)
      @pseminar = create(:seminar, name_i18n: @seminar.name_i18n,
                         term: @prev)
      @fseminar = create(:seminar, name_i18n: @seminar.name_i18n, 
                         term: @dfuture)
    end

    after(:context) { DatabaseCleaner.clean }

    describe 'GET #index' do
      it 'assigns correct terms' do
        get :index
        expect(controller.send('term')).to eq(@cur)
      end
    end

    describe 'GET #show' do
      it 'assigns correct variables in #show' do
        get :show, params: @seminar.to_find_param
        cur = @seminar.meetings
        expect(controller.send('meetings')).to eq cur
        expect(controller.send('coming')).to eq(cur.this_week)
        expect(controller.send('following')).to eq(cur.from_time)
        expect(controller.send('past')).to eq(cur.before)
      end
    end
  end
end
