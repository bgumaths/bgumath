# frozen_string_literal: true

require 'controllers/people_shared_examples'
require 'controllers/shared_examples'

RSpec.shared_examples 'postdoc index' do
  it 'assigns current and future visitors to `visitors`' do
    visitor = create(:visitor)
    sget :index
    expect(controller.send('visitors').all).to match_array([visitor])
  end
end

RSpec.describe People::PostdocsController, type: :controller do
  include_examples 'people preamble'
  it_behaves_like 'a regular controller'
end
