# frozen_string_literal: true

require 'controllers/shared_examples'

RSpec.describe UserSessionsController, type: :controller do
  describe 'POST #create' do
    setup :activate_authlogic
    let(:user) { create(:user, :password, state: :_active) }

    context 'with valid password' do
      it 'sets current user to the given one' do
        post :create, params: { user_session: { email: user.email,
                                                password: user.password, } }

        expect(controller.send(:current_user)).to eq(user)
      end
      it 'redirects to the root url' do
        post :create, params: { user_session: { email: user.email,
                                                password: user.password, } }
        expect(response).to redirect_to(root_url)
      end
      it 'flashes last login' do
        post :create, params: { user_session: { email: user.email,
                                                password: user.password, } }
        expect(flash[:notice]).to match(
          /^#{I18n.t('session.loggedin',
            user: '.*', last_login_at: '.*', last_login_ip: '.*'
          ).gsub(/[()]/, '.')}/)
        #expect(flash[:notice]).to match(/^Logged in as /)
      end
    end

    context 'with invalid password' do
      it 'leaves current user unchanged' do
        old = controller.send(:current_user)
        post :create, params: {
          user_session: {
            email: user.email,
            password: user.password + 'foobar',
          }
        }
        expect(controller.send(:current_user)).to eq(old)
      end

      it 'redirects to the root url' do
        post :create, params: {
          user_session: {
            email: user.email,
            password: user.password + 'foobar',
          } 
        }
        expect(response).to redirect_to(root_url)
      end

      it 'flashes an error message' do
        post :create, params: {
          user_session: {
            email: user.email,
           password: user.password + 'foobar',
          }
        }
        expect(flash[:alert]).to eq(
          I18n.t('session.invalid', message: 'password: is not valid')
        )
      end
    end
  end
end
