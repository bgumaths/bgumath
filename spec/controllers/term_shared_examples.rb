# frozen_string_literal: true

RSpec.shared_examples 'termwise preamble' do
  def self.include_ex
    [:termwise, model_param]
  end

  def self.default_params
    { term_id: Term.current.id }
  end

  let(:local_build) do
    build(model_param, term: default_term)
  end


end

RSpec.shared_examples 'termwise create' do
  it 'uses the object term when it exists' do
    tnext = Term.current&.next || create(:term, :simple, :following,
                                         prev: Term.current)
    id = tnext.id
    mpost :create, params: def_params(
      model_param => valid_attributes.merge(term_id: id)
    )
    expect(model.last.term_id).to eq(id)
  end
end

