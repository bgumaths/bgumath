# frozen_string_literal: true

require 'controllers/shared_examples'
require 'controllers/term_shared_context'
require 'controllers/term_shared_examples'

RSpec.shared_examples 'course index' do
  it 'assigns correct terms' do
    fall = Term.fall || create(:term, :simple, starts: Date.new(CommonUtil.cur_year.to_i - 1, 10, 1))
    spring = Term.spring || create(:term, :simple, starts: fall.exams + 2.days)
    sget :index
    expect(controller.send('fterm')).to eq(fall)
    expect(controller.send('sterm')).to eq(spring)
  end
end

RSpec.shared_examples 'course new' do
  context 'when params contain a generic course id' do
    it 'assign the generic course defaults' do
      gc = create(:generic_course, course_count: 0)
      mget :new, params: def_params(generic_course_id: gc.id, shid: nil)
      expect(controller.send(:bare_object).generic_course).to eq(gc)
      expect(controller.send(:bare_object).title_i18n).to eq(gc.name_i18n)
      expect(controller.send(:bare_object).content_i18n).to eq(gc.description_i18n)
    end
  end

  context 'when params contain a course id' do
    it 'duplicates the course' do
      old = create_object
      mget :new, params: def_params(course_id: old.id)
      expect(controller.send(:bare_object).title_i18n).to eq(old.title_i18n)
      expect(controller.send(:bare_object).content_i18n).to eq(old.content_i18n)
    end
  end

  context 'when params contain a generic course id and a course id' do
    it 'uses fields from the course, but the given GC as its GC' do
      gc = create(:generic_course, course_count: 0)
      old = create_object
      mget :new, params: def_params(generic_course_id: gc.id,
                                    course_id: old.id, shid: nil)
      expect(controller.send(:bare_object).generic_course).to eq(gc)
      expect(controller.send(:bare_object).title_i18n).to eq(old.title_i18n)
      expect(controller.send(:bare_object).content_i18n).to eq(old.content_i18n)
    end
  end
end

RSpec.shared_examples 'course create' do
  context 'when updating from generic course' do
    it "redirects to the 'new' url" do
      spost :create, params: def_params(
        commit: 'generic_course', model_param => { generic_course_id: 1 }
      )
      expect(response).to redirect_to(
        url_for(def_params(action: 'new', controller: controller_class_name,
                           generic_course_id: 1, shnaton: 1,
                           model_param => { generic_course_id: 1 }))
      )
    end
  end
end

RSpec.shared_examples 'course update' do
  context 'when updating from generic course' do
    it "redirects to the 'edit' url" do
      sput :update, params: @object.to_find_param(commit: 'generic_course', model_param => { generic_course_id: 1 })
      expect(response).to redirect_to(@object.decorate.edit_path(generic_course_id: 1, shnaton: 1, model_param => { generic_course_id: 1 }))
    end
  end
end

RSpec.describe Teaching::CoursesController, type: :controller do
  let(:invalid_attributes) do
    # shid should be nil, since otherwise the generic course is set from it
    valid_attributes.merge(generic_course_id: nil, shid: nil)
  end

  let(:new_attributes) do
    {
      title_i18n: { 'en' => 'some new course' },
    }
  end

  before(:context) { DatabaseCleaner.start }
  after(:context) { DatabaseCleaner.clean }
  include_examples 'termwise preamble'

  it_behaves_like 'a regular controller'
end

