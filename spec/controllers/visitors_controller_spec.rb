# frozen_string_literal: true

require 'controllers/shared_examples'

RSpec.describe People::VisitorsController, type: :controller do
  def self.skip_invalid
    true
  end

  let(:new_attributes) do
    {
      email: 'duder@big',
      first_i18n: {
        'en' => 'Jeff',
        'he' => "ג'ף",
      },
      last_i18n: {
        'he' => 'לבובסקי',
        'en' => 'Lebowski',
      },
    }
  end

  def index_location(**opts)
    {controller: 'people/postdocs', action: 'index'}.deep_merge(opts)
  end

  it_behaves_like 'a regular controller'

end

