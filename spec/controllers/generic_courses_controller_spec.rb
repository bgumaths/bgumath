# frozen_string_literal: true

require 'controllers/shared_examples'
require 'controllers/term_shared_context'
require 'controllers/term_shared_examples'

RSpec.shared_examples 'generic_course index' do
  it 'assigns correct lists' do
    undergrad1 = create(:generic_course, :undergrad)
    undergrad2 = create(:generic_course, :undergrad)
    grad = create(:generic_course, :grad)
    sget :index
    expect(controller.send('undergrad'))
      .to match_array([undergrad1, undergrad2])
    expect(controller.send('grad')).to match_array([grad])
  end
end

RSpec.shared_examples 'generic_course create' do
  context 'when updating from shnaton' do
    it "redirects to the 'new' url" do
      spost :create, params: def_params(commit: 'shnaton')
      expect(response).to redirect_to(url_for(def_params(action: 'new', controller: controller_class_name)))
    end
  end
end

RSpec.shared_examples 'generic_course update' do
  context 'when updating from shnaton' do
    it "redirects to the 'edit' url" do
      sput :update, params: @object.to_find_param(commit: 'shnaton')
      expect(response).to redirect_to(@object.decorate.edit_path)
    end
  end
end

RSpec.describe Teaching::GenericCoursesController, type: :controller do
  let(:invalid_attributes) do
    valid_attributes.merge(name_i18n: { 'en' => nil })
  end

  let(:new_attributes) do
    {
      name_i18n: { 'en' => 'some new generic course', 'he' => 'hebrew version' },
    }
  end

  it_behaves_like 'a regular controller'

  describe 'POST #create' do
  end
end
