# frozen_string_literal: true

require 'controllers/shared_examples'

RSpec.shared_examples 'event index' do
  it 'assigns timed events' do
    past = create(:event, :past)
    recent = create(:event, :recent)
    future = create(:event, :future)
    sget :index
    expect(controller.send('cur').object.all).to match_array([future])
    expect(controller.send('past').object.all).to match_array([past])
    expect(controller.send('recent').object.all).to match_array([recent])
  end
end

RSpec.describe Research::EventsController, type: :controller do
  def self.skip_invalid
    true
  end

  let(:new_attributes) do
    {
      title: 'some new event',
    }
  end

  it_behaves_like 'a regular controller'

  describe 'GET #email' do
    setup :activate_authlogic
    it 'sends an email announcement' do
      event = create(:event)
      expect { sget :email, params: { id: event.to_param } }.to change(ActionMailer::Base.deliveries, :count).by(1)
    end
  end
end
