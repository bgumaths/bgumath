# frozen_string_literal: true

module ViewPrefixes
  extend ActiveSupport::Concern

  included do
    before do
      view.lookup_context.prefixes << 'base' << 'application'
    end
  end
end

RSpec.configure do |c|
  c.include ViewPrefixes, type: :view
end
