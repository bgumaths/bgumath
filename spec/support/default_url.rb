# frozen_string_literal: true

# workaround, to set default locale for ALL spec

class ActionView::TestCase::TestController
  def default_url_options(**_options)
    {
      locale: I18n.locale.presence || I18n.default_locale,
      host: 'test.host',
      protocol: 'https',
    }
  end
end

class ActionMailer::Base
  def default_url_options(**_options)
    {
      locale: I18n.locale.presence || I18n.default_locale,
      host: 'test.host',
      protocol: 'https',
    }
  end
end
