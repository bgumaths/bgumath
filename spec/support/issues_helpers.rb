# frozen_string_literal: true

def remove_responders
  User.repliers.each do |rep|
    rep.issue_responses.destroy_all
    rep.destroy!
  end
end
