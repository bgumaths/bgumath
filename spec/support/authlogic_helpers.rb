# frozen_string_literal: true

# https://stackoverflow.com/questions/18751297/rails4-authlogic-rspec
module Authlogic
  module TestHelper
    # You can call this anything you want, I chose this name as it was similar
    # to how AuthLogic calls it's objects and methods
    def login(user)
      # Assuming you have this defined in your routes, otherwise just use:
      #   '/your_login_path'
      post login_path, params: { login: user.email, password: user.password }
    end
  end

  module ControllerHelper
    def make_session
      @admin = User.admin || create(:virtual, :admin)
      UserSession.create(@admin)
    end

    def sget(*args)
      make_session
      get(*args)
    end

    def spost(*args)
      make_session
      post(*args)
    end

    def sput(*args)
      make_session
      put(*args)
    end

    def sdelete(*args)
      make_session
      delete(*args)
    end
  end
end

# Make this available to just the request and feature specs
RSpec.configure do |config|
  config.include Authlogic::TestHelper, type: :request
  config.include Authlogic::TestHelper, type: :feature
  config.include Authlogic::ControllerHelper, type: :controller
end
