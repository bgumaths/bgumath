module TermMethods
  def default_term_date(date = Date.current)
    # a "today" date that falls within a potential term
    [1, 2, 7, 8].include?(date.month) ? date - (date.month % 6 + 1).months
                                      : date
  end

  def default_term(date = default_term_date, **opts)
    Term.current(date) || create(:term, :simple, today: date, **opts)
  end

  def following_term(term = default_term)
    term&.next || create(:term, :simple, :following, prev: term)
  end

end

