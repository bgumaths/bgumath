## frozen_string_literal: true

require 'rails_helper'
require 'policies/shared_examples'

RSpec.describe IssueResponsePolicy do

  include_examples "general includes"

  subject(:policy) { described_class.new(issues_user || user, issue_response) }

  let(:term) { default_term }
  let(:course) { issue.course }
  let(:generic_course) { course.generic_course }
  let(:lecturer) { course.lecturer }
  let(:reporter) { issue.reporter }
  let(:issue) { issue_response.issue } 
  let(:record) { issue_response }
  let(:issue_response) { create(:issue_response) }

  context 'with no issues user' do
    let(:issues_user) { nil }

    context 'and no user' do
      let(:user) { nil }

      it 'is expected raise an authorization error' do
        expect { policy }.to raise_error(Pundit::NotAuthorizedError)
      end
    end

    context 'and arbitrary user' do
      let(:user) { create(:lecturer, course_count: 0) }

      include_examples 'permit precisely', []
    end

    context 'and lecturer in the course' do
      let(:user) { lecturer }

      include_examples 'permit precisely', %i[show]
    end

    context 'and head, vice head or admin' do
      let(:user) { create(:regular, :simple, :vice) }

      include_examples 'permit precisely', %i[show]
    end

    context 'and responder' do
      let(:user) { create(:regular, :simple, :responder) }

      include_examples 'permit precisely',
        %i[show new create edit update destroy]
    end
  end

  context 'with issues student' do
    context 'when issue belongs to student' do
      let(:issues_user) { reporter }

      context 'when response is unpublished' do
        include_examples 'permit precisely', %i[]
      end

      context 'when issue is published' do
        before { issue_response.publish! }
        include_examples 'permit precisely', %i[show]
      end
    end

    context 'when issue does not belong to student' do
      let(:issues_user) { create(:issues_student_rep) }

      include_examples 'permit precisely', %i[]
    end
  end

  context 'with issues aguda rep' do
    context 'when issue related to rep' do
      let(:issues_user) { generic_course.aguda_rep }

      context 'when response is unpublished' do
        include_examples 'permit precisely', %i[]
      end

      context 'when issue is published' do
        before { issue_response.publish! }
        include_examples 'permit precisely', %i[show]
      end
    end

    context 'when issue is not related to the rep' do
      let(:issues_user) { create(:issues_aguda_rep, course_count: 0) }

      include_examples 'permit precisely', []
    end
  end

  context 'with issues staff' do
    context 'when issue belongs to staff' do
      let(:department) { generic_course.departments.first }
      let(:issues_user) { create(:issues_staff, department: department) }

      include_examples 'permit precisely', %i[show]
    end

    context 'when issue does not belong to staff' do
      let(:issues_user) { create(:issues_staff) }

      include_examples 'permit precisely', []
    end
  end

  context 'with new issues student' do
    let(:issues_user) do
      create(:issues_student_rep, courses: [issue.course],
                                  pass_changed: false)
    end

    include_examples 'permit precisely', []
  end

  context 'with new issues aguda rep' do
    let(:issues_user) do
      create(:issues_aguda_rep, generic_courses: [generic_course],
                                pass_changed: false)
    end

    include_examples 'permit precisely', []
  end

  context 'with new issues staff' do
    let(:issues_user) do
      create(:issues_staff, department: generic_course.departments.first,
             pass_changed: false)
    end

    include_examples 'permit precisely', []
  end
end

