# frozen_string_literal: true

require 'rails_helper'
require 'policies/shared_examples'

RSpec.describe CoursePolicy do
  subject(:policy) { described_class.new(user, record) }

  let(:resolved_scope) do
    described_class::Scope.new(user, Course.all).resolve
  end

  let(:lecturer) { create(:lecturer) }
  let(:record) { create(:course, :simple, lecturer: lecturer) }

  # ## Policy
  # The operations permitted on a course depend on the logged in user.
  # The possible operations are:
  # 
  # List
  # : See a listing of the courses. Note that this only controls access to 
  # the listing _page_. The actual courses listed depend on the `show` 
  # permission
  # 
  # Show
  # : See the content of the course
  # 
  # Create
  # : Create a new course
  # 
  # Update
  # : Make changes to a course
  # 
  # Destroy
  # : Delete a course in an abnormal way. The course will disappear from the 
  # system (This is not part of the normal operation)
  # 
  # 
  #   |-----------+-------+-------+:------:+---------+---------|
  #   | User      | List  | Show  | Create | Update  | Destroy |
  #   |:----------+:-----:+:-----:+:------:+:-------:+:-------:|
 
  context 'when user is not the lecturer' do
    # | None      |   ✓   |   ✓   |    ✗   |    ✗    |    ✗    |
    context 'with no user it' do
      let(:user) { nil }

      include_examples 'permit precisely', %i[index show]
    end

    # | Member    |   ✓   |   ✓   |   ✓    |    ✗    |    ✗    |
    context 'with a regular member' do
      let(:user) { create(:regular, :simple) }
      include_examples 'permit precisely', %i[index show new create]
    end

    # | T. Comm.  |   ✓   |   ✓   |   ✓    |    ✓    |    ✓    |
    context 'with a teaching committee member' do
      let(:user) { create(:regular, :simple, :teaching_committee) }
      include_examples 'permit precisely', %i[index show new create edit update]
    end

    # | Responder |   ✓   |   ✓   |   ✓    |    ✓    |    ✓    |
    context 'with an issues responder' do
      let(:user) { create(:regular, :simple, :responder) }
      include_examples 'permit precisely', %i[index show new create edit update]
    end
  end


  context 'when user is the lecturer' do
    let(:user) { lecturer }
    # | Lecturer  |   ✓   |   ✓   |   ✓    |    ✓    |    ✓    |
    context 'with lecturer a regular member' do
      let(:lecturer) { create(:regular, :simple) }
      include_examples 'permit precisely', %i[index show new create edit update]
    end

    # | Adj Lect. |   ✓   |   ✓   |   ✗    |    ✓    |    ✗    |
    context 'with an adjunct course lecturer' do
      let(:lecturer) { create(:adjunct) }
      include_examples 'permit precisely', %i[index show edit update]
    end
  end
  #   |===========+=======+=======+=========+========+=========|
end

