## frozen_string_literal: true

require 'rails_helper'
require 'policies/shared_examples'

RSpec.describe IssuesUserPolicy do

  include_examples "general includes"

  subject(:policy) { described_class.new(current_user, issues_user) }

  let(:resolved_scope) do
    described_class::Scope.new(current_user, IssuesUser.all).resolve
  end

  # the issues user on which we operate
  let(:record) { issues_user }

  # the issues user who is currently logged in (whose permissions we are 
  # checking). By default, same as the one we would like to operate on
  let(:current_issues_user) { issues_user }
  let(:current_user) { current_issues_user || user }

  let(:issues_user) { create(:issues_user) }

  # ## Policy
  # List
  # : See a listing of the users.
  # 
  # Create
  # : Create a new user
  # 
  # Update
  # : Make changes to the user
  # 
  # Destroy
  # : Delete a user
  # 
  context 'with no logged in issues user' do
    # 
    # ### Department users
    # 
    # The following table lists the permitted actions for various members of 
    # the department. Note that the most permissive role wins (e.g., if a 
    # responder is the lecturer in a course, they will have the permissions 
    # of a responder). Note also that no department member can create new 
    # issues, so this is not listed.
    # 
    # |-----------+-------+-------+---------+---------+---------|
    # | User      | List  | Show  | Create  | Update  | Destroy |
    # |:----------+:-----:+:-----:+:-------:+:-------:+:-------:|
    let(:current_issues_user) { nil }

    # | None      |   ✗   |   ✗   |    ✗    |    ✗    |    ✗    |
    context 'and no user' do
      let(:user) { nil }

      it 'is expected raise an authorization error' do
        expect { policy }.to raise_error(Pundit::NotAuthorizedError)
      end
      it 'excludes record from scope' do
        expect(resolved_scope).not_to include(record)
      end
    end

    # | Member    |   ✓   |   ✓   |    ✗    |    ✗    |    ✗    |
    context 'and arbitrary user' do
      let(:user) { create(:lecturer, course_count: 0) }

      include_examples 'permit precisely', %i[index show]
    end

    # | Admin     |   ✓   |   ✓   |    ✓    |    ✓    |    ✗    |
    context 'and head, vice head or admin' do
      let(:user) { create(:regular, :simple, :vice) }

      include_examples 'permit precisely',
        %i[index show edit update new create]
    end

    # |===========+=======+=======+=========+=========+=======+=======|
    # 
    # The interpretation of the users is as follows:
    # 
    # None
    # : No user (or issues user) logged in
    # 
    # Member
    # : A member of the department who is unrelated to the issue
    # 
    # Admin
    # : A department member with a special role (e.g., the department head 
    #   or vice head)
    # 
  end

  ### TODO!!
  # 
  # ### Issues users
  # 
  # The following table documents permissions for the external users who are 
  # the clients of this system. Operations not listed in the table are 
  # denied for all such users. The `Reply` column shows whether the response 
  # is visible.
  # 
  # Note that if both a department user and an issues user are logged in 
  # (this should not happen normally), the system ignores the department 
  # user.
    # 
    # |------------+-------+------+--------|
    # | User/Issue | List  | Show | Update |
    # |:-----------+:-----:+:----:+:------:|
  
  context 'with issues student' do
    context 'when issue belongs to student' do
      let(:issues_user) { issues_student_rep }
    end

    context 'when issue does not belong to student' do
      let(:issues_user) { create(:issues_student_rep) }

    end
  end

  context 'with issues aguda rep' do
    context 'when issue related to rep' do
      let(:issues_user) {
        create(:issues_aguda_rep, generic_courses: gcs)
      }


    end

    context 'when issue is not related to the rep' do
      let(:issues_user) { create(:issues_aguda_rep, course_count: 0) }

    end
  end

  context 'with issues staff' do
    # | Staff      |   ✓   |   ✓  |   ✓   |    ✗    |
    context 'when issue belongs to staff' do
      let(:department) { create(:department, 
                                courses: [issue.course.generic_course]) }
      let(:issues_user) { create(:issues_staff, department: department) }

    end

    context 'when issue does not belong to staff' do
      let(:issues_user) { create(:issues_staff) }

    end
  end

    # | Unrelated  |   ✓   |  ✗   |   ✗   |    ⍻    |
  context 'with new issues student' do
    let(:issues_user) do
      create(:issues_student_rep, courses: [issue.course],
                                  pass_changed: false)
    end

  end

  context 'with new issues aguda rep' do
    let(:issues_user) do
      create(:issues_aguda_rep, generic_courses: gcs,
                                pass_changed: false)
    end

  end

  context 'with new issues staff' do
    let(:department) { create(:department, 
                              courses: [issue.course.generic_course]) }
    let(:issues_user) do
      create(:issues_staff, department: department,
                            pass_changed: false)
    end

  end
    # |============+=======+======+=======+=========|
    # 
  # The user types are as follows:
  # 
  # Student/X
  # : A student representative who reported the issue. The letters `O` and 
  #   `P` stand for the situation when the issue is Open or Published, 
  #   respectively.
  # 
  # Aguda/X
  # : A representative from the students' union. The letters `O` and `P` 
  #   stand for the situation when the issue is Open or Published, 
  #   respectively.
  # 
  # Staff
  # : A staff member from a relevant department.
  # 
  # Unrelated
  # : An external user that is unrelated to the issue (only the `show` 
  #   column is relevant)
  # 
end

