# frozen_string_literal: true

require 'pundit/rspec'
require 'pundit/matchers'

RSpec.shared_examples 'permit precisely' do |allowed|
  let(:all_actions) do
    (try(:resource_actions) || %i[
        index show new create edit update destroy
    ]) + (try(:extra_actions) || []) - (try(:excluded_actions) || [])
  end
  let(:forbidden) { all_actions - allowed }

  it { is_expected.to forbid_actions(forbidden) if forbidden.present? }
  it { is_expected.to permit_actions(allowed) if allowed.present? }

  if allowed.include?(:index)
    if allowed.include?(:show)
      it 'includes record in scope' do
        expect(resolved_scope).to include(record)
      end
    else
      it 'excludes record from scope' do
        expect(resolved_scope).not_to include(record)
      end
    end
  end
end


