## frozen_string_literal: true

require 'rails_helper'
require 'policies/shared_examples'

RSpec.describe IssuePolicy do
  def self.extra_actions
    %i[send_created show_staff]
  end

  include_examples "general includes"

  subject(:policy) { described_class.new(issues_user || user, issue) }

  let(:resolved_scope) do
    described_class::Scope.new(issues_user || user, Issue.all).resolve
  end

  let(:term) { default_term }
  let(:gcs) { create_list(:generic_course, [*1..4].sample, :simple) }
  let(:courses) {
    gcs.map{|gc| create(:course, generic_course: gc, term: term)}
  }
  let(:issues_student_rep) { create(:issues_student_rep,
                                    courses: courses) }
  let(:issue) { create(:issue, reporter: issues_student_rep,
                       course: issues_student_rep.courses.sample) }
  let(:record) { issue }

  # ## Policy
  # The operations permitted on an issue depends on the logged in user, and 
  # also on the status of the issue. The possible operations are:
  # 
  # List
  # : See a listing of the issues. Note that this only controls access to 
  #   the listing _page_. The actual issues listed depend on the `show` 
  #   permission
  # 
  # Show
  # : See the content of the issue, the reply and other details
  # 
  # Create
  # : Create a new issue
  # 
  # Update
  # : Make changes to the original issue. This is not usually necessary
  # 
  # Destroy
  # : Delete an issue in an abnormal way. The issue will disappear from the 
  #   system (This is not part of the normal operation)
  # 
  # Email
  # : Resend notification emails about creation or update of issues
  # 
  # Staff
  # : See the external staff associated with an issue (on the `show` page)
  #
  context 'with no issues user' do
    # 
    # ### Department users
    # 
    # The following table lists the permitted actions for various members of 
    # the department. Note that the most permissive role wins (e.g., if a 
    # responder is the lecturer in a course, they will have the permissions 
    # of a responder). Note also that no department member can create new 
    # issues, so this is not listed.
    # 
    # |-----------+-------+-------+---------+---------+-------+-------|
    # | User      | List  | Show  | Respond | Destroy | Email | Staff |
    # |:----------+:-----:+:-----:+:-------:+:-------:+:-----:+:-----:|
    let(:issues_user) { nil }

    # | None      |   ✗   |   ✗   |    ✗    |    ✗    |   ✗   |   ✗   |
    context 'and no user' do
      let(:user) { nil }

      it 'is expected raise an authorization error' do
        expect { policy }.to raise_error(Pundit::NotAuthorizedError)
      end
      it 'excludes issue from scope' do
        expect(resolved_scope).not_to include(issue)
      end
    end

    # | Member    |   ✓   |   ✗   |    ✗    |    ✗    |   ✗   |   ✗   |
    context 'and arbitrary user' do
      let(:user) { create(:lecturer, course_count: 0) }

      include_examples 'permit precisely', [:index]
    end

    # | Lecturer  |   ✓   |   ✓   |    ✗    |    ✗    |   ✗   |   ✗   |
    context 'and lecturer in the course' do
      let(:user) { create(:lecturer, courses: [issue.course]) }

      include_examples 'permit precisely', %i[index show]
    end

    # | Admin     |   ✓   |   ✓   |    ✗    |    ✓    |   ✓   |   ✓   |
    context 'and head, vice head or admin' do
      let(:user) { create(:regular, :simple, :vice) }

      context 'when issues is unpublished' do
        include_examples 'permit precisely',
          %i[index show edit update destroy send_created show_staff]
      end

      context 'when issue is published' do
        before do
          create(:issue_response, :published, issue: issue)
        end

        include_examples 'permit precisely',
          %i[index show destroy send_created show_staff]
      end
    end

    # | Responder |   ✓   |   ✓   |    ✓    |    ✓    |   ✓   |   ✓   |
    context 'and responder' do
      let(:user) { create(:regular, :simple, :responder) }

      include_examples 'permit precisely',
        %i[index show destroy send_created show_staff]
      ## response tested in issue_response policy
    end
    # |===========+=======+=======+=========+=========+=======+=======|
    # 
    # The interpretation of the users is as follows:
    # 
    # None
    # : No user (or issues user) logged in
    # 
    # Member
    # : A member of the department who is unrelated to the issue
    # 
    # Lecturer
    # : The lecturer in the course that the issue refers to. In 
    #   multi-section courses, this is the head of the course.
    # 
    # Admin
    # : A department member with a special role (e.g., the department head 
    #   or vice head)
    # 
    # Responder
    # : A member in charge of responding to issues
    # 
  end

  # 
  # ### Issues users
  # 
  # The following table documents permissions for the external users who are 
  # the clients of this system. Operations not listed in the table are 
  # denied for all such users. The `Reply` column shows whether the response 
  # is visible.
  # 
  # Note that if both a department user and an issues user are logged in 
  # (this should not happen normally), the system ignores the department 
  # user.
    # 
    # |------------+-------+------+-------+---------|
    # | User/Issue | List  | Show | Reply | Create  |
    # |:-----------+:-----:+:----:+:-----:+:-------:|
  
  context 'with issues student' do
    context 'when issue belongs to student' do
      let(:issues_user) { issues_student_rep }

      include_examples 'permit precisely', %i[index show new create]

    # | Student/O  |   ✓   |  ✓   |   ✗   |    ✓    |
      context 'when issue is open' do
        it 'hides the reply' do
          create(:issue_response, issue: issue)
          expect(issue.response).to be_present
          expect(policy).not_to be_visible(:response_id)
        end
      end

    # | Student/P  |   ✓   |  ✓   |   ✓   |    ✓    |
      context 'when issue is published' do
        it 'shows the reply' do
          create(:issue_response, :published, issue: issue)
          expect(issue.response).to be_present
          expect(policy).to be_visible(:response_id)
        end
      end
    end

    context 'when issue does not belong to student' do
      let(:issues_user) { create(:issues_student_rep) }

      include_examples 'permit precisely', %i[index new create]
    end
  end

  context 'with issues aguda rep' do
    context 'when issue related to rep' do
      let(:issues_user) {
        create(:issues_aguda_rep, generic_courses: gcs)
      }

      include_examples 'permit precisely', %i[index show]

    # | Aguda/O    |   ✓   |  ✓   |   ✗   |    ✗    |
      context 'when issue is open' do
        it 'hides the reply' do
          create(:issue_response, issue: issue)
          expect(issue.response).to be_present
          expect(policy).not_to be_visible(:response_id)
        end
      end

    # | Aguda/P    |   ✓   |  ✓   |   ✓   |    ✗    |
      context 'when issue is published' do
        it 'shows the reply' do
          create(:issue_response, :published, issue: issue)
          expect(issue.response).to be_present
          expect(policy).to be_visible(:response_id)
        end
      end
    end

    context 'when issue is not related to the rep' do
      let(:issues_user) { create(:issues_aguda_rep, course_count: 0) }

      include_examples 'permit precisely', [:index]
    end
  end

  context 'with issues staff' do
    # | Staff      |   ✓   |   ✓  |   ✓   |    ✗    |
    context 'when issue belongs to staff' do
      let(:department) { create(:department, 
                                courses: [issue.course.generic_course]) }
      let(:issues_user) { create(:issues_staff, department: department) }

      include_examples 'permit precisely', %i[index show]
    end

    context 'when issue does not belong to staff' do
      let(:issues_user) { create(:issues_staff) }

      include_examples 'permit precisely', [:index]
    end
  end

    # | Unrelated  |   ✓   |  ✗   |   ✗   |    ⍻    |
  context 'with new issues student' do
    let(:issues_user) do
      create(:issues_student_rep, courses: [issue.course],
                                  pass_changed: false)
    end

    include_examples 'permit precisely', [:index]
  end

  context 'with new issues aguda rep' do
    let(:issues_user) do
      create(:issues_aguda_rep, generic_courses: gcs,
                                pass_changed: false)
    end

    include_examples 'permit precisely', [:index]
  end

  context 'with new issues staff' do
    let(:department) { create(:department, 
                              courses: [issue.course.generic_course]) }
    let(:issues_user) do
      create(:issues_staff, department: department,
                            pass_changed: false)
    end

    include_examples 'permit precisely', [:index]
  end
    # |============+=======+======+=======+=========|
    # 
  # The user types are as follows:
  # 
  # Student/X
  # : A student representative who reported the issue. The letters `O` and 
  #   `P` stand for the situation when the issue is Open or Published, 
  #   respectively.
  # 
  # Aguda/X
  # : A representative from the students' union. The letters `O` and `P` 
  #   stand for the situation when the issue is Open or Published, 
  #   respectively.
  # 
  # Staff
  # : A staff member from a relevant department.
  # 
  # Unrelated
  # : An external user that is unrelated to the issue (only the `show` 
  #   column is relevant)
  # 
end

