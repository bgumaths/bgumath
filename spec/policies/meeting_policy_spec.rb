# frozen_string_literal: true

require 'rails_helper'
require 'policies/shared_examples'

RSpec.describe MeetingPolicy do
  subject(:policy) { described_class.new(user, record) }

  let(:resolved_scope) do
    described_class::Scope.new(user, seminar.meetings.all).resolve
  end

  let(:seminar) { create(:seminar, :current, meeting_count: 3) }
  let(:record) { seminar.meetings.first }

  # ## Policy
  # The operations permitted on a seminar meeting depend on the logged in 
  # user. The possible operations are:
  # 
  # List
  # : See a listing of the meetings in the seminar. Note that this only 
  # controls access to the listing _page_. The actual meetings listed depend 
  # on the `show` permission
  # 
  # Show
  # : See the content of the meeting
  # 
  # Create
  # : Create a new meeting
  # 
  # Update
  # : Make changes to a meeting
  # 
  # Destroy
  # : Delete a meeting.
  #
  # Email
  # : Send email to self
  #
  # Email_ml
  # : Send email to the mailing list
  # 
  # 
  #   |-----------+-------+-------+:------:+---------+---------|
  #   | User      | List  | Show  | Create | Update  | Destroy |
  #   |:----------+:-----:+:-----:+:------:+:-------:+:-------:|
 
  #   | None      |   ✓   |   ✓   |    ✗   |    ✗    |    ✗    |
  context 'with no user it' do
    let(:user) { nil }

    include_examples 'permit precisely', %i[index show]
  end

  #   | Member    |   ✓   |   ✓   |   ✗    |    ✗    |    ✗    |
  context 'with a regular member' do
    let(:user) { create(:regular, :simple) }
    include_examples 'permit precisely', %i[index show email]
  end

  #   | Sem. Admin|   ✓   |   ✓   |   ✓    |    ✓    |    ✓    |
  context 'with a seminar admin' do
    let(:user) { create(:regular, :simple) }

    before do
      user.add_role('Admin', seminar)
    end

    include_examples 'permit precisely',
      %i[index show new create edit update destroy email email_ml]
  end

#   |===========+=======+=======+=========+========+=========|
end

