# frozen_string_literal: true

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../config/environment', __dir__)
# Prevent database truncation if the environment is production
abort('The Rails environment is running in production mode!') if Rails.env.production?

#GraphitiSpecHelpers::RSpec.schema!

require 'rspec/rails'
# Add additional requires below this line. Rails is not loaded until this
# point!
# allow turning on/off of papertrail
require 'paper_trail/frameworks/rspec'

require 'pundit/matchers'

# pry when spec fails
#require 'pry-rescue/rspec'

require 'rspec/retry'

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
Dir[Rails.root.join('spec', 'support', '**', '*.rb')].each { |f| require f }

# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

# when building `foo` that has an association `bar`, bar should be created 
# rather than built, so that we may save foo.
FactoryBot.use_parent_strategy = false

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
  # Remove this line if you're not using ActiveRecord or ActiveRecord 
  # fixtures
  #config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  # clean db from previous runs
  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:context) do
    create(:virtual, :admin, password: 'foobar', email: 'jesus') unless User.admin.present?
  end

  # clean db after a whole test, so that different tests do not interact
  config.after(:context) do
    DatabaseCleaner.clean_with(:truncation)
    # clean activestorage
    FileUtils.rm_rf(Rails.root.join('tmp', 'storage'))
  end

  # show retry status in spec process
  config.verbose_retry = true
  # show exception that triggers a retry if verbose_retry is set to true
  config.display_try_failure_messages = true

  config.exceptions_to_retry = [Net::ReadTimeout]

  # run retry only on features
  config.around :each, :js do |ex|
    ex.run_with_retry retry: 2
  end

  config.include Graphiti::Rails::TestHelpers

  config.before(:each, type: :request) do
    handle_request_exceptions(true)
  end

  # callback to be run between retries  
  config.retry_callback = proc do |ex|
    # run some additional clean up task - can be filtered by example metadata
    if ex.metadata[:js]
      Capybara.reset!     
    end
  end

  config.include ActiveJob::TestHelper

end

class FactoryBot::SyntaxRunner
  include TermMethods
end

RSpec.shared_examples 'general includes' do
  let(:default_term) { Term.current || create(:term, :simple) }
  let(:following_term) {
    default_term&.next ||
    create(:term, :simple, :following, prev: default_term)
  }

  def default_term_date(date = Date.current)
    # a "today" date that falls within a potential term
    [1, 2, 7, 8].include?(date.month) ? date - (date.month % 6 + 1).months
                                      : date
  end

  def default_term(date = default_term_date, **opts)
    Term.current(date) || create(:term, :simple, today: date, **opts)
  end

  def following_term(term = default_term)
    term&.next || create(:term, :simple, :following, prev: term)
  end

end

