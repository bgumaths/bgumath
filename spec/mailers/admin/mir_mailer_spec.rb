## frozen_string_literal: true

require 'mailers/shared_examples'

RSpec.describe Admin::MirMailer do

  let(:mail) { Admin::MirMailer.public_send(described_class) }
  let(:subject) { I18n.t('admin.mir#index', locale: :en)}

  include_examples 'regular mailer includes'

  before(:context) do
    @term = Term.take || create(:term, :current, :simple)
    @vice = User.vice.take || create(:regular, :simple, :vice, email: 'bibi')
    @vice_role = Role.vice
    @admin = create(:admin, :depadmin)
    @secretary = create(:admin, :secretary)
    @generic_courses = create_list(:generic_course, 3, :basic, course_count: 1)
  end

  describe :health do
    it_behaves_like 'a regular mailer:', []

    let(:msg_class) { 'mir' }
    let(:to) { [@secretary] }
    let(:cc) { [User.administrator, admin_email, @vice_role] }
    let(:bcc) { [] }
  end

end

