## frozen_string_literal: true

require 'mailers/shared_examples'

RSpec.describe Teaching::IssuesUserMailer do

  let(:mail) { Teaching::IssuesUserMailer.public_send(described_class, object) }
  let(:subject) {
    I18n.t(described_class, scope: %i[teaching issue email_subj],
           user: object, locale: :he)
  }
  include_examples 'regular mailer includes'

  before(:context) do
    @vice = User.vice.take || create(:regular, :simple, :vice, email: 'bibi')
    @vice_role = Role.vice
    @responder = User.repliers.first || 
      create(:regular, :simple, :responder, email: 'gantz')
    @aguda_rep = IssuesAgudaRep.take || 
      create(:issues_aguda_rep, login: 'coltrane', course_count: 0)
    @student = IssuesStudentRep.take ||
      create(:issues_student_rep, login: 'monk', course_count: 0)
    @generic_course = create(:generic_course, :basic, :simple, 
                             aguda_rep: @aguda_rep)
    @department = create(:department, courses: [@generic_course])
    @issues_staff = IssuesStaff.find_by(login: 'feynman') || 
      create(:issues_staff, login: 'feynman', department: @department)
    @course = build(:course, term: default_term,
                    generic_course: @generic_course, student_rep: @student)
  end

  let(:issuesuser_user) { object.to_s }
  let(:msg_class) { 'issuesuser' }

  it 'has existing people' do
    ([@aguda_rep, @student, @generic_course, @department, @issues_staff, 
      @course, @responder, @vice_role]
    ).each do |it|
      expect(it).to be_present
    end
  end

  let(:to) { [object] }
  let(:cc) { [admin_email, @vice_role] }
  let(:bcc) { [] }

  # ### Account email
  # 
  # Email messages are also sent on certain events related to issues users 
  # accounts. The recepients of these messages are always the same: The 
  # owner of the account is the main recepient, and the department vice-head 
  # is cc'ed. The addition header `X-bgumath-issuesuser-user` is set to the 
  # login of the user. The types of messages are as follows:
  # 
  # - `account`: A new account was created for the user
  describe :account do
    context 'of a student representative' do
      let(:object) { @student.decorate }
      it_behaves_like 'a regular mailer:', %i[issuesuser_user]

    end

    context 'of an external staff member' do
      let(:object) { @issues_staff.decorate }
      it_behaves_like 'a regular mailer:', %i[issuesuser_user]

    end

    context 'of an aguda representative' do
      let(:object) { @aguda_rep.decorate }
      it_behaves_like 'a regular mailer:', %i[issuesuser_user]

    end
  end

  # - `reset_password`: The password for the user was reset. The message 
  #   will contain a new temporary password that the user will need to change 
  #   before using the account.
  describe :reset_password do
    context 'of a student representative' do
      let(:object) { @student.decorate }
      it_behaves_like 'a regular mailer:', %i[issuesuser_user]

    end

    context 'of an external staff member' do
      let(:object) { @issues_staff.decorate }
      it_behaves_like 'a regular mailer:', %i[issuesuser_user]

    end

    context 'of an aguda representative' do
      let(:object) { @aguda_rep.decorate }
      it_behaves_like 'a regular mailer:', %i[issuesuser_user]

    end
  end
  
  # - `changed_password`: The password for the user was changed (to warn the 
  #   user if something funny is going on)
  describe :changed_password do
    context 'of a student representative' do
      let(:object) { @student.decorate }
      it_behaves_like 'a regular mailer:', %i[issuesuser_user]

    end

    context 'of an external staff member' do
      let(:object) { @issues_staff.decorate }
      it_behaves_like 'a regular mailer:', %i[issuesuser_user]

    end

    context 'of an aguda representative' do
      let(:object) { @aguda_rep.decorate }
      it_behaves_like 'a regular mailer:', %i[issuesuser_user]

    end
  end
  # 
  # For extra security, these messages are all pgp-signed with the system's 
  # key, fingerprint `10C0CFBE3BABED00A323338A5B6371DA151D574A`
  ## TODO: 1. Add the fingerprint to the site, rather than hardcoding it 
  #here. 2. Test this
end

