## frozen_string_literal: true

require 'mailers/shared_examples'

# ## Issues Email
# 
# The email messages sent by the system are listed below. To facilitate 
# email filtering, the following headers are added:
# 
# - `X-bgumath-msg-class`: The class to which the message refers, usually 
#   this will be `issue`, but for user-related email it will be 'issuesuser`.
# - `X-bgumath-msg-type`: The type of message, e.g. `created`. Possible 
#   values are listed below.
# - `X-bgumath-issue-id`: The id of the issue, something like `123456-2020`
# - `X-bgumath-issue-course-id`: The BGU catalog id of the issue's course, 
#   similar to `201.1.0201`.
# - `X-bgumath-issue-error`: An error message, if this is an error report
# 
# In addition, the subject of every message is prefixed with `[פת]'.
# 
RSpec.describe Teaching::IssueMailer do

  let(:mail) { Teaching::IssueMailer.public_send(described_class, object) }
  let(:subject) {
    I18n.t(described_class, scope: %i[teaching issue email_subj],
           id: object.slug, issue: object, course: object.course.to_s(:he),
           locale: :he)
  }
  include_examples 'regular mailer includes'

  before(:context) do
    @term = Term.take || create(:term, :current, :simple)
    @vice = User.vice.take || create(:regular, :simple, :vice, email: 'bibi')
    @vice_role = Role.vice
    @responder = create(:regular, :simple, :responder, email: 'gantz')
    create(:regular, :simple, :responder, email: 'bennet')
    @responders = User.repliers
    @lecturer = create(:regular, :simple, course_count: 0)
    @department = create(:department, course_count: 1)
    @department.reload
    @generic_course = @department.courses.take
    @issues_staff = IssuesStaff.find_by(login: 'feynman') || 
      create(:issues_staff, login: 'feynman', department: @department)
    @inactive_staff = IssuesStaff.find_by(login: 'curie') || 
      create(:issues_staff, login: 'curie', department: @department,
            active: false)
    @course = @generic_course.in_term(@term) ||
      create(:course, lecturer: @lecturer, term: @term,
                    generic_course: @generic_course)
    @issue = create(:issue, :replied, course: @course, replier: @responder)
  end

  let(:department) { Department.take }
  let(:generic_course) { department.courses.take }
  let(:object) { Issue.take.decorate }
  let(:course) { object.course }
  let(:issue_id) { object.slug }
  let(:issue_course_id) { course.shid }
  let(:lecturer) { course.lecturer }
  let(:issues_staffs) { @issues_staff }
  let(:aguda_rep) { course.aguda_rep }
  let(:reporter) { object.reporter }

  it 'has existing people' do
    (%i[reporter lecturer issues_staffs aguda_rep] + [@responder, @vice_role]
    ).each do |it|
      expect(it).to be_present
    end
  end

  # ### Message recepients
  # 
  # |----------------+--------+----------+----------+-------+-------+----|
  # | type           |reporter|responders| lecturer | staff | union | vh |
  # |:---------------+:------:+:--------:+:--------:+:-----:+:-----:+:--:|
  # | created        |   T    |    T     |    C     |   C   |   C   | C  |
  describe :created do
    it_behaves_like 'a regular mailer:', %i[issue_id issue_course_id]

    let(:to) { [reporter, *@responders] }
    let(:cc) { [lecturer, *issues_staffs, aguda_rep, @vice_role] }
    let(:reply_to) { [*@responders, @vice_role] }
  end

  # | replied        |   ✗    |    C     |    T     |   T   |   ✗   | C  |
  describe :replied do
    it_behaves_like 'a regular mailer:', %i[issue_id issue_course_id]

    let(:to) { [lecturer, *issues_staffs] }
    let(:cc) { [*@responders, @vice_role] }
    let(:reply_to) { [@responder, @vice_role] }
  end

  # | published      |   T    |    T     |    C     |   C   |   C   | C  |
  describe :published do
    it_behaves_like 'a regular mailer:', %i[issue_id issue_course_id]

    let(:to) { [reporter, *@responders] }
    let(:cc) { [lecturer, *issues_staffs, aguda_rep, @vice_role] }
    let(:reply_to) { [@responder, @vice_role] }
  end

  # | reminder       |   ✗    |    t     |    C     |   ✗   |   ✗   | C  |
  describe :reminder do
    it_behaves_like 'a regular mailer:', %i[issue_id issue_course_id]

    let(:object) { create(:issue, :replied, replier: @responder, 
                          course: @course).decorate }
    let(:to) { [@responder] }
    let(:cc) { [lecturer, @vice_role] }
  end

  # |missing_repliers|   -    |    ✗     |    -     |   -   |   -   | T  |
  describe :missing_repliers do
    let(:object) { @issue.reporter }
    let(:msg_class) { 'issue' }
    let(:issue_error) { 'responders-missing' }
    let(:subject) {
      I18n.t(
        described_class, scope: %i[teaching issue email_subj], locale: :he
      )
    }

    it_behaves_like 'a regular mailer:', %i[issue_error]

    let(:to) { [admin_email, @vice_role] }
    let(:cc) { [] }
    let(:bcc) { [] }
  end

  # |missing_lecturer|   -    |    ✗     |    -     |   -   |   -   | T  |
  describe :missing_lecturer do
    let(:issue_error) { 'lecturer-missing' }
    it_behaves_like 'a regular mailer:', %i[issue_error]

    let(:to) { [admin_email, @vice_role] }
    let(:cc) { [] }
    let(:bcc) { [] }
  end
  # |----------------+--------+----------+----------+-------+-------+----|
  # 
  # In this table, `T` means To, `C` means Cc, and ✗ means not a recepient.  
  # `reporter` is the student who submitted the issue, `responders` are the 
  # issue responders (all of them get all email, even the ones they are not 
  # assigned to, except for the publish reminder), `lecturer` is the lecturer 
  # in that course, `staff` is the external staff (relevant to this course) 
  # and `union` is the union (aguda) representative for this course.  `vh` is 
  # the department vice-head. The types are:
  # 
  # - `created`: An issue was created
  # - `replied`: A reply to the issue was submitted. This is also sent if a 
  #   reply was modified
  # - `published`: The reply was published to the students
  # - `reminder`: A reminder to the responder to publish the reply, 24 hours 
  #   after it was last modified. Only the responder who gave the reply 
  #   receives it.
  # - `missing_repliers`: A message sent in case there are no responders 
  #   defined. The department administrator is also included in the 
  #   recepients. Since there is no issue involved here, the issue related 
  #   headers listed above will not appear in this case, but 
  #   `X-bgumath-issue-error` will have the value `responders-missing`.
  # 

end

