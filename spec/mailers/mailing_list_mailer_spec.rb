require 'mailers/shared_examples'

RSpec.describe MailingListMailer, type: :mailer do
  let(:list) { 'foobar' }
  let(:owner) { 'foo@example.org' }
  let(:mail) {
    MailingListMailer.public_send(described_class, list, owner)
  }
  let(:subject) { I18n.t(
    :subject, scope: [:mailing_list_mailer, described_class], list: list
  ) }
  let(:to) { [Setting.ml_admin] }
  let(:cc) { [owner] }
  let(:reply_to) { [owner] }

  include_examples 'regular mailer includes'

  describe :create do
    let(:msg_class) { 'mailinglist' }
    it_behaves_like 'a regular mailer:'
  end

end
