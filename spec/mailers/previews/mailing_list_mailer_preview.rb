# Preview all emails at http://localhost:3000/rails/mailers/mailing_list_mailer
class MailingListMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/mailing_list_mailer/create
  def create
    MailingListMailer.create('foobar', 'Jeff Lebowski <dude@math>')
  end

end
