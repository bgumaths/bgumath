# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/teaching/issue_mailer
class Teaching::IssuesUserMailerPreview < ActionMailer::Preview
  def account
    Teaching::IssuesUserMailer.account(user)
  end

  def reset_password
    Teaching::IssuesUserMailer.reset_password(user)
  end

  def changed_password
    Teaching::IssuesUserMailer.changed_password(user)
  end

  protected

  def user
    IssuesUser.take.decorate
  end

  def default_url_options
    super.merge(locale: :he)
  end
end

