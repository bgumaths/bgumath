# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/teaching/issue_mailer
class Teaching::IssueMailerPreview < ActionMailer::Preview
  def created
    @issue = Issue.last
    Teaching::IssueMailer.created(@issue)
  end

  def replied
    @issue = Issue.replied.last
    Teaching::IssueMailer.replied(@issue)
  end

  def published
    @issue = Issue.published.last
    Teaching::IssueMailer.published(@issue)
  end

  def reminder
    @issue = Issue.published.last
    Teaching::IssueMailer.reminder(@issue)
  end

  def missing_repliers
    @student = IssuesStudentRep.take
    Teaching::IssueMailer.missing_repliers(@student)
  end

  def missing_lecturer
    @issue = Issue.last
    Teaching::IssueMailer.missing_lecturer(@issue)
  end

  protected

  def default_url_options
    super.merge(locale: :he)
  end
end
