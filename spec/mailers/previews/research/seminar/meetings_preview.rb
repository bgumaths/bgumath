# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/research/seminar/meetings
class Research::Seminar::MeetingsPreview < ActionMailer::Preview
  def announce
    meeting = Meeting.where.not(abstract: nil).take.decorate
    Research::Seminar::MeetingMailer.announce(meeting, 'deligne@ias.edu')
  end
end

