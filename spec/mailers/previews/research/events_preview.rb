# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/research/events
class Research::EventsPreview < ActionMailer::Preview
  def announce
    event = Event.take.decorate
    Research::EventMailer.announce(event, 'deligne@ias.edu')
  end
end

