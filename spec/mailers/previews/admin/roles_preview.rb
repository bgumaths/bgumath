# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/admin/roles
class Admin::RolesPreview < ActionMailer::Preview
  def role_changed
    change = RoleEvent.take
    Admin::RoleMailer.role_changed(change)
  end
end

