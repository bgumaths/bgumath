# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/admin/mir
class Admin::MirPreview < ActionMailer::Preview
  def health
    Admin::MirMailer.health
  end
end

