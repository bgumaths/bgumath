# frozen_string_literal: true

require 'rails_helper'

RSpec.configure do |c|
  c.before(:example) do
    ActionMailer::Base.default_url_options[:locale] = I18n.locale
    ActionMailer::Base.default_url_options[:host] = 'test.host'
    ActionMailer::Base.default_url_options[:protocol] = 'https'
    Rails.application.routes.default_url_options = ActionMailer::Base.default_url_options
  end
end

RSpec.shared_examples 'regular mailer includes' do
  include_examples 'general includes'

  def expect_extra(key, val)
    #$stderr.puts "        #{key} ==> #{val}"
    hdr = "X-bgumath-#{key}"
    expect(mail.header.fields).to include(be_responsible_for(hdr))
    expect(mail.header[hdr].to_s).to be == val.to_s
  end

  def email_address(what)
    what.try(:decorate).try(:email_address) || what
  end

  let(:admin_email) { email_address(User.admin) }

  let(:msg_class) { object.model_name.singular }

end

RSpec.shared_examples 'a regular mailer:' do |extra|

  # must define: object, subject, to, cc
  # may define: from, bcc (default to email_admin)
  # extra: additional set headers key/value

  it 'generates a multipart message (plain text and html)' do
    expect(mail.body.parts.map(&:content_type)).to contain_exactly(
      'text/plain; charset=UTF-8', 'text/html; charset=UTF-8'
    )
  end

  it 'sets standard headers' do
    expect_extra('msg-class', msg_class)
    expect_extra('msg-type', described_class)
  end
  
  it 'has the right subject' do
    expect(mail.subject).to eq(subject)
  end

  it 'comes from the correct address' do
    expect(mail.from)
      .to eq(try(:from) || [admin_email])
  end

  let(:dbcc) { try(:bcc) || [admin_email] }
  it 'has the correct recepients' do
    expect(mail.to).to match_array(to.map{|x| email_address(x)})
    if try(:cc).present?
      expect(mail.cc).to match_array(cc.map{|x| email_address(x)})
    else
      expect(mail.cc).to_not be_present
    end
    if dbcc.present?
      expect(mail.bcc).to match_array(dbcc)
    else
      expect(mail.bcc).to_not be_present
    end
  end

  it 'has the correct Reply-To' do
    if try(:reply_to).present?
      expect(mail.reply_to || [])
        .to match_array(reply_to.map{|x| email_address(x)})
    else
      expect(mail.reply_to).to_not be_present
    end
  end

  if extra.present?
    it "sets extra headers #{extra}" do
      extra.each do |k|
        expect_extra(k.to_s.tr('_', '-'), try(k))
      end
    end
  end
end

