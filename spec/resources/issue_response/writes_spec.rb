require 'rails_helper'

RSpec.describe IssueResponseResource, type: :resource do
  describe 'creating' do
    let(:payload) do
      {
        data: {
          type: 'issue_responses',
          attributes: attributes_for(:issue_response)
        }
      }
    end

    let(:instance) do
      IssueResponseResource.build(payload)
    end

    it 'works' do
      expect {
        expect(instance.save).to eq(true), instance.errors.full_messages.to_sentence
      }.to change { IssueResponse.count }.by(1)
    end
  end

  describe 'updating' do
    let!(:issue_response) { create(:issue_response) }

    let(:payload) do
      {
        data: {
          id: issue_response.id.to_s,
          type: 'issue_responses',
          attributes: { } # Todo!
        }
      }
    end

    let(:instance) do
      IssueResponseResource.find(payload)
    end

    xit 'works (add some attributes and enable this spec)' do
      expect {
        expect(instance.update_attributes).to eq(true)
      }.to change { issue_response.reload.updated_at }
      # .and change { issue_response.foo }.to('bar') <- example
    end
  end

  describe 'destroying' do
    let!(:issue_response) { create(:issue_response) }

    let(:instance) do
      IssueResponseResource.find(id: issue_response.id)
    end

    it 'works' do
      expect {
        expect(instance.destroy).to eq(true)
      }.to change { IssueResponse.count }.by(-1)
    end
  end
end
