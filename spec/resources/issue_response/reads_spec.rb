require 'rails_helper'

RSpec.describe IssueResponseResource, type: :resource do
  describe 'serialization' do
    let!(:issue_response) { create(:issue_response) }

    it 'works' do
      render
      data = jsonapi_data[0]
      expect(data.id).to eq(issue_response.id)
      expect(data.jsonapi_type).to eq('issue_responses')
    end
  end

  describe 'filtering' do
    let!(:issue_response1) { create(:issue_response) }
    let!(:issue_response2) { create(:issue_response) }

    context 'by id' do
      before do
        params[:filter] = { id: { eq: issue_response2.id } }
      end

      it 'works' do
        render
        expect(d.map(&:id)).to eq([issue_response2.id])
      end
    end
  end

  describe 'sorting' do
    describe 'by id' do
      let!(:issue_response1) { create(:issue_response) }
      let!(:issue_response2) { create(:issue_response) }

      context 'when ascending' do
        before do
          params[:sort] = 'id'
        end

        it 'works' do
          render
          expect(d.map(&:id)).to eq([
            issue_response1.id,
            issue_response2.id
          ])
        end
      end

      context 'when descending' do
        before do
          params[:sort] = '-id'
        end

        it 'works' do
          render
          expect(d.map(&:id)).to eq([
            issue_response2.id,
            issue_response1.id
          ])
        end
      end
    end
  end

  describe 'sideloading' do
    # ... your tests ...
  end
end
