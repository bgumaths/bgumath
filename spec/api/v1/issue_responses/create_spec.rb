require 'api_helper'

RSpec.describe "issue_responses#create", type: :request do
  include_context 'request context'

  before :context do
    @responder ||= create(:regular, :simple, :responder)
    @issue ||= create(:issue)
  end

  let(:model) { IssueResponse }
  let(:object) { build(:issue_response, issue: @issue) }

  before :each do
    expect(resource).to receive(:build).and_call_original if user
    jsonapi_post path, payload, headers: headers
  end

  context 'when user is a responder' do
    let(:user) { @responder }

    it 'creates the object' do
      created = model.find(id)
      expect(created.content).to eq(object.content)
    end

    include_examples 'success with correct data'
  end

  context 'when user is the lecturer' do
    let(:user) { @issue.course.lecturer }
    let(:payload) { to_jsonapi(object, @responder) }

    include_examples 'reject with error',
      forbidden: 'not allowed to create? this IssueResponse'
  end

  include_examples 'reject without user'
end
