require 'api_helper'

RSpec.describe "issue_responses#index", type: :request do
  include_context 'request context'

  before(:context) do
    @issue_responses = create_list(:issue_response, 3)
  end

  let(:objects) { @issue_responses }

  before :each do
    jsonapi_get "/api/v1/issue_responses", headers: headers
  end

  context 'when user is vice head:' do

    before(:context) do
      @vice ||= create(:regular, :simple, :vice, email: 'bibi')
    end

    let(:user) { @vice }

    include_examples 'reject with error',
      forbidden: 'not allowed to index? this Class'
  end

  context 'when user is a lecturer:' do
    let(:user) { objects[0].issue.course.lecturer }
    let(:objects) { [@issue_responses[0]] }

    include_examples 'reject with error',
      forbidden: 'not allowed to index? this Class'
  end

  include_examples 'reject without user'

end


