require 'api_helper'

RSpec.describe "issue_responses#update", type: :request do
  include_context 'request context'

  before :context do
    @responder ||= create(:regular, :simple, :responder, password: 'foobar', 
                          email: 'bach')
    @issue_response ||= create(:issue_response)
  end

  let(:model) { IssueResponse }

  let(:object) {
    res = @issue_response.dup
    res.content = res.content + 'foobar'
    res.id = @issue_response.id
    res
  }
  let(:issue) { object.issue }
  let(:course) { issue.course }

  let(:payload) { to_jsonapi(object, @responder) }

  subject(:make_request) do
    expect(resource).to receive(:find).and_call_original if user
    jsonapi_put "#{path}/#{object_id}", payload, headers: headers
  end

  before(:each) { make_request }

  context 'when user is a student:' do
    let(:user) { issue.reporter }

    include_examples 'reject with error',
      forbidden: 'not allowed to update? this IssueResponse'
  end

  context 'when user is a responder:' do
    let(:user) { @responder }

    it 'modifies the object' do
      modified = model.find(id)
      expect(modified.content).to eq(object.content)
    end

    include_examples 'success with correct data'
  end

  context 'when user is a lecturer:' do
    let(:user) { course.lecturer }

    include_examples 'reject with error',
      forbidden: 'not allowed to update? this IssueResponse'
  end

  include_examples 'reject without user'
end
