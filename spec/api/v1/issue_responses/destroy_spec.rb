require 'api_helper'

RSpec.describe "issue_responses#destroy", type: :request do
  include_context 'request context'

  before(:context) do
    @responder ||= create(:regular, :simple, :responder)
    @issue_response ||= create(:issue_response)
  end

  let(:model) { IssueResponse }
  let(:object) { @issue_response }

  subject(:make_request) {
    expect(resource).to receive(:find).and_call_original if user
    jsonapi_delete "/api/v1/issue_responses/#{object_id}", headers: headers
  }


  before :each do
    make_request
  end

  context 'when user is a responder' do
    let(:user) { @responder }

    include_examples 'destroy object'
  end

  include_examples 'reject without user'
end
