require 'api_helper'

RSpec.describe "issue_responses#show", type: :request do
  include_context 'request context'

  subject(:make_request) do
    expect(resource).to receive(:find).and_call_original
    jsonapi_get "/api/v1/issue_responses/#{object_id}", headers: headers
  end

  before(:context) do
    handle_request_exceptions(false)
    @issue_response = create(:issue_response)
  end

  let(:model) { IssueResponse }
  let(:object) { @issue_response }
  let(:issue) { object.issue }
  let(:course) { issue.course }

  before(:each) { make_request }

  context 'when user is vice head:' do
    before(:context) do
      @vice ||= create(:regular, :simple, :vice, email: 'bibi')
    end

    let(:user) { @vice }

    include_examples 'get object with user'
  end

  context 'when user is a lecturer:' do
    context 'when object belongs to user:' do
      let(:user) { course.lecturer }

      include_examples 'get object with user'
    end

    context 'when object does not belong to user:' do
      let(:user) { build(:lecturer, :simple) }

      include_examples 'reject with error',
        forbidden: "not allowed to show? this IssueResponse"
    end
  end

  context 'when user is a student rep:' do
    context 'when object belongs to user:' do
      let(:user) { issue.reporter }

      context 'when object is not published' do
        include_examples 'reject with error',
          forbidden: "not allowed to show? this IssueResponse"
      end

      context 'when object is published' do
        let(:object) { create(:issue_response, :published) }

        include_examples 'get object with user'
      end
    end

    context 'when object does not belong to user:' do
      let(:user) { build(:issues_student_rep) }

      include_examples 'reject with error',
        forbidden: 'not allowed to show? this IssueResponse'
    end
  end

  include_examples 'reject without user'
end

