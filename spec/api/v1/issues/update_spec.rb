require 'api_helper'

RSpec.describe "issues#update", type: :request do
  include_context 'request context'

  before :context do
    @responder ||= create(:regular, :simple, :responder, password: 'foobar', 
                          email: 'bach')
    @issue ||= create(:issue)
  end

  let(:model) { Issue }

  let(:object) {
    res = @issue.dup
    res.title = res.title + 'foobar'
    res.id = @issue.id
    res
  }

  let(:payload) { to_jsonapi(object, object.reporter) }

  subject(:make_request) do
    expect(resource).to receive(:find).and_call_original if user
    jsonapi_put "/api/v1/issues/#{object_id}", payload, headers: headers
  end

  before(:each) { make_request }

  context 'when user is a student:' do
    let(:user) { object.reporter }

    include_examples 'reject with error',
      forbidden: 'not allowed to update? this Issue'
  end

  context 'when user is a responder:' do
    let(:user) { @responder }

    include_examples 'reject with error',
      forbidden: 'not authorized to modify the following attributes: content, correspondence, course_id, title'
  end

  context 'when user is a lecturer:' do
    let(:user) { object.course.lecturer }

    include_examples 'reject with error',
      forbidden: 'not authorized to modify the following attributes: content, correspondence, course_id, title'
  end

  include_examples 'reject without user'
end
