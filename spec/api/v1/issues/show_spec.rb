require 'api_helper'

RSpec.describe "issues#show", type: :request do
  include_context 'request context'

  subject(:make_request) do
    expect(resource).to receive(:find).and_call_original
    jsonapi_get "/api/v1/issues/#{object_id}", headers: headers
  end

  before(:context) do
    @issue = create(:issue)
  end

  let(:object) { @issue }
  let(:model) { Issue }
  let(:type) { 'issues' }

  before(:each) { make_request }

  context 'when user is vice head:' do
    before(:context) do
      @vice ||= create(:regular, :simple, :vice, email: 'bibi')
    end

    let(:user) { @vice }

    include_examples 'get object with user'
  end

  context 'when user is a lecturer:' do
    context 'when object belongs to user:' do
      let(:user) { object.course.lecturer }

      include_examples 'get object with user'
    end

    context 'when object does not belong to user:' do
      let(:user) { build(:lecturer, :simple) }

      include_examples 'reject with error',
        forbidden: 'not allowed to show? this Issue'
    end
  end

  context 'when user is a student rep:' do
    context 'when object belongs to user:' do
      let(:user) { object.reporter }

      include_examples 'get object with user'
    end

    context 'when object does not belong to user:' do
      let(:user) { build(:issues_student_rep) }

      include_examples 'reject with error',
        forbidden: 'not allowed to show? this Issue'
    end
  end

  include_examples 'reject without user'
end


