require 'api_helper'

RSpec.describe "issues#index", type: :request do
  include_context 'request context'

  before(:context) do
    @issues = create_list(:issue, 3)
  end


  let(:model) { Issue }
  let(:objects) { @issues }

  before :each do
    expect(resource).to receive(:all).and_call_original if user
    jsonapi_get "/api/v1/issues", headers: headers
  end

  context 'when user is vice head:' do

    before(:context) do
      @vice ||= create(:regular, :simple, :vice, email: 'bibi')
    end

    let(:user) { @vice }

    include_examples 'get all objects with user'
  end

  context 'when user is a lecturer:' do
    let(:user) { @issues[0].course.lecturer }
    let(:objects) { [@issues[0]] }

    include_examples 'get all objects with user'
  end

  include_examples 'reject without user'

end
