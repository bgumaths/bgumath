require 'api_helper'

RSpec.describe "issues#create", type: :request do
  include_context 'request context'

  before :context do
    @responder ||= create(:regular, :simple, :responder, password: 'foobar', 
                         email: 'bach')
    @course ||= create(:course, term: default_term)
  end

  let(:lecturer) { @course.lecturer }
  let(:student_rep) { @course.student_rep }
  let(:object) { build(:issue, course: @course) }
  let(:model) { Issue }
  
  before :each do
    unless try(:cancel_post)
      expect(resource).to receive(:build).and_call_original if user
      jsonapi_post "/api/v1/issues", payload, headers: headers
    end
  end

  context 'when user is a student rep:' do
    let(:user) { student_rep }

    context 'with own course' do
      context 'when no responders exist' do
        let(:cancel_post) { true }

        before(:each) {
          User.repliers.each do |rep|
            rep.issues.delete_all
            rep.destroy!
          end
          jsonapi_post "/api/v1/issues", payload, headers: headers
        }

        include_examples 'reject with error', service_unavailable: I18n.t(
          :issues_noresponders, scope: %i[errors messages],
          locale: :en
        )
      end
      context 'when responders are registered' do
        it 'is successful' do
          expect(response).to be_successful, json
        end

        it 'creates the object' do
          created = model.find(id)
          expect(created.title).to eq(object.title)
        end

        it 'responds with created object' do
          expect(attributes).to include(obj_attrs)
          expect(attributes.keys)
            .to match_array(pol_attributes + %w[writable])
        end
      end
    end

    context 'with foreign course' do

      let(:course) {
        create(:course, lecturer: lecturer, term: default_term)
      }
      let(:payload) {
        res = default_payload
        res[:data][:relationships][:course][:data][:id] = course.id
        res
      }

      let(:err_msg) {
        I18n.t('reporter_courses',
               scope: %i[activerecord errors models issue attributes course],
               reporter: student_rep.login, locale: :en)
      }

      include_examples 'reject with error', unprocessable_entity: :err_msg
    end

  end
  context 'when user is a lecturer:' do
    let(:user) { lecturer }
    let(:payload) { to_jsonapi(object, student_rep) }

    include_examples 'reject with error',
      forbidden: 'not authorized to modify the following attributes: content, correspondence, course_id, title'
  end

  include_examples 'reject without user'

end

