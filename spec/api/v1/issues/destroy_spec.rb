require 'api_helper'

RSpec.describe "issues#destroy", type: :request do
  include_context 'request context'

  before(:context) { @issue ||= create(:issue) }

  let(:object) { @issue }
  let(:model) { Issue }

  subject(:make_request) {
    expect(resource).to receive(:find).and_call_original if user
    jsonapi_delete "/api/v1/issues/#{object_id}", headers: headers
  }

  before :each do
    #expect { make_request }.to change { model.count }.by(-1)
    make_request
  end

  context 'when user is vice head:' do
    before(:context) do
      @vice ||= create(:regular, :simple, :vice, email: 'bibi')
    end

    let(:user) { @vice }

    include_examples 'destroy object'
  end
end

