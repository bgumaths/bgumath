## frozen_string_literal: true

require 'system/shared_examples'

RSpec.describe 'research/seminars' do

  let(:form_opts) do
    { 
      text: %i[name description web room slug list_id],
      select: %i[term day],
      timerange: %i[shift],
      url: %i[url],
    }.freeze
  end


  ## ensure current term exists
  include_examples 'regular system includes'
  before(:context) do
    @term = default_term
  end

  it_behaves_like 'an admined component:'
end

