# frozen_string_literal: true

require 'rails_helper'
require 'capybara/rspec'

RSpec.configure do |c|
  c.before(:suite) do
    ActionMailer::Base.default_url_options[:locale] = I18n.locale
    ActionMailer::Base.default_url_options[:host] = 'test.host'
    ActionMailer::Base.default_url_options[:protocol] = 'https'
    Rails.application.routes.default_url_options = ActionMailer::Base.default_url_options
  end

  c.after(:example) do |ex|
    if ENV['RSPEC_VISIBLE'] && ex.exception.present?
      debugger
    end
  end

  # To see what's going on, run with RSPEC_VISIBLE=firefox
  if ENV['RSPEC_VISIBLE']
    c.before(:example, type: :system) do
      @sleep_delay = (ENV['RSPEC_SLEEP'] || 1).to_i
      @js = true
      driven_by(:selenium, using: ENV['RSPEC_VISIBLE'].to_sym)
    end
  else
    c.before(:example, type: :system) do
      @sleep_delay = (ENV['RSPEC_SLEEP'] || 0).to_i
      @js = false
      driven_by(:rack_test)
    end

    c.before(:example, js: true, type: :system) do
      @sleep_delay = (ENV['RSPEC_SLEEP'] || 0).to_i
      @js = true
      driven_by(:selenium_chrome_headless)
    end
  end

end

MODAL_NEW = ENV['RSPEC_REGULAR_NEW'].blank?
puts "** #{MODAL_NEW ? 'U' : 'Not u'}sing modals for creating new objects by default **"

USE_JS = ENV['RSPEC_USE_JS'].presence || false
puts "** #{USE_JS ? 'U' : 'Not u'}sing js for form elements by default **"

RSpec.shared_examples 'regular system includes' do

  def default_url_options
    Rails.application.routes.default_url_options
  end

  include_examples 'general includes'
  let(:js) { @js }

  def to_emails(*args)
    args.map{|aa| aa.try(:decorate).try(:email_address) || aa}
  end

  def flash
    find('.flash').text
  end

  def current_user
    find('div#loginbox span.userid')&.text
  rescue Capybara::ElementNotFound
    nil
  end
  
  def current_object
    File.basename(current_path)
  end

  def added_row
    find('tr.added')
  rescue Capybara::ElementNotFound
    nil
  end

  def mail_matches?(mail, **options)
    opts = options.dup
    opts['X-bgumath-msg-type'] = opts.delete(:type) if opts[:type]
    %i[to cc body].each{|it| opts.delete(it)}
    opts.each do |k,v|
      return false unless mail.header[k.to_s.tr('_', '-')].decoded == v.to_s
    end
    true
  end

  def check_mail(**opts)
    perform_enqueued_jobs(
      queue: Rails.configuration.action_mailer.deliver_later_queue_name
    )
    # there may be more than one email waiting, we need to verify that one 
    # of them matches the conditions
    mail = ActionMailer::Base.deliveries.find{|mm| mail_matches?(mm, **opts)}
    ActionMailer::Base.deliveries = []
    expect(mail).to be_present
    expect(mail.to).to match_array(to_emails(*opts.delete(:to))) if opts[:to]
    expect(mail.cc).to match_array(to_emails(*opts.delete(:cc))) if opts[:cc]
    opts['X-bgumath-msg-type'] = opts.delete(:type) if opts[:type]
    if (body = opts.delete(:body))
      decoded = (mail.parts.first.parts.first || mail.parts.first ||
                 mail.body).decoded
      [*body].each do |str|
        # check only the first 50 chars, since there may be formatting
        expect(decoded).to include(str[0..50])
      end
    end
    yield mail if block_given?
  end

  def check_no_mail(**)
    perform_enqueued_jobs(
      queue: Rails.configuration.action_mailer.deliver_later_queue_name
    )
    mail = ActionMailer::Base.deliveries.find{|mm| mail_matches?(mm, **)}
    ActionMailer::Base.deliveries = []
    expect(mail).to_not be_present
  end

  def commit(button = 'commit', delay = @sleep_delay)
    click_on(button)
    sleep(delay)
  end

  def bootstrap_select_button(from = nil)
    ## if from is not given, assume the only required one
    from ||= 'required'
    find("div.select.#{from} " + (
      js ? 'div.bootstrap-select button.dropdown-toggle' : 'select'
    ))
  end

  def bootstrap_select(val, **options)
    btn = bootstrap_select_button(options.delete(:from))
    if js
      btn.click
      opt = find('div.dropdown-menu.show div.inner.show ul > li', text: val)
      opt.click
    else
      btn.select(val)
    end
  end

  def bootstrap_selected(*args)
    btn = bootstrap_select_button(*args)
    btn = btn.find('option[selected]') unless js
    btn.text
  end

  ## following does not work with bootstrap select
  def select_by_val(val, **options)
    opt = "option[value='#{val}']"
    if options.has_key?(:from)
      from = options.delete(:from)
      find(:select, from, options)
        .find(opt, options)
        .select_option
    else
      find(opt, options)
    end
  end

  def with_locale(loc, &block)
    oloc = I18n.locale
    I18n.locale = loc
    res = yield
    I18n.locale = oloc
    res
  end

  def outer_locale
    return $1 if current_path =~ /^\/?(en|he)/
  end

  def expect_edit_page(what)
    form_path = what.decorate.show_path(locale: outer_locale)
    expect(page).to have_selector("form[action='#{form_path}']")
  end

  # TODO
  def expect_flash_success
    #expect(page).to have_selector("div.alert-success")
    #expect(page).to_not have_selector("div.alert-warning")
    true
  end

  def expect_flash_failure
    #expect(page).to_not have_selector("div.alert-success")
    #expect(page).to have_selector("div.alert-warning")
    true
  end

  def vIsit(path)
    Rails.logger.debug("Visiting #{path}")
    visit(path)
  end

  def fIll_in(field, **opts)
    Rails.logger.debug("Fillin #{opts[:with]} into #{field}")
    fill_in(field, **opts)
  end

  def clIck_button(what)
    Rails.logger.debug("Clicking on #{what}")
    click_button(what)
  end

  def user_login(user, **opts)
    opts = (
      user.present? ? {email: user.email, password: user.password} : {}
           ).deep_merge(opts)

    # ensure we have a login box
    if all('div#footer').blank?
      vIsit(root_path)
    end
    user_logout
    fIll_in 'user_session_email', with: opts[:email]
    fIll_in 'user_session_password', with: opts[:password]
    clIck_button 'login'
  end

  def user_login_success(user, **opts)
    user_login(user, **opts)
    # doesn't work, for some reason
    expect_flash_success
  end

  def user_logout(**opts)
    click_link('Logout')
  rescue Capybara::ElementNotFound
    raise if opts[:strict]
  end

  def inner_locale
    find('div#maincontent')[:lang]
  end

  def start_new_object(cls = class_decorator, **opts)
    modal = opts.key?(:modal) ? opts[:modal] : MODAL_NEW
    if modal
      visit(cls.index_path(nesting: 0)) if opts[:visit]
      I18n.locale = inner_locale
      click_on(I18n.t('actions.new', **cls.to_trans))
    else
      visit(cls.new_path(nesting: 0)) unless opts.key?(:visit) && !opts[:visit]
    end
    modal
  end

  def fill_form(obj, name = (obj.try(:object) || obj).class.name.parameterize,
                **opts)
    object = obj.try(:decorate) || obj
    opts = opts.presence || form_opts
    (opts[:text] || []).each do |ff|
      if object.respond_to?("#{ff}_i18n")
        I18n.available_locales.each do |loc|
          fill_in("#{name}_#{ff}_i18n_#{loc}",
                  with: object.public_send("#{ff}_i18n")[loc.to_s].to_s)
        end
      else
        fill_in("#{name}_#{ff}", with: object.public_send(ff).to_s)
      end
    end
    (opts[:select] || []).each do |ff|
      bootstrap_select(object.public_send(ff).to_s, from: "#{name}_#{ff}")
    end
    (opts[:timerange] || []).each do |ff|
      fill_form(object.public_send(ff), "#{name}_#{ff}", 
                text: %i[beginning ending])
    end
  end

end

RSpec.shared_examples 'an admined component:' do |res = module_parent.description|
  before(:context) do
    @dude = create(:regular, :simple, password: 'foobar', email: 'dude')
    @dony = create(:regular, :simple, password: 'foobar', email: 'dony')
  end

  let(:dude) { Regular.find('dude').decorate }
  let(:dony) { Regular.find('dony').decorate }

  include_examples 'regular system includes'

  def self.simp(res)
    res.sub(/.*\//, '').singularize 
  end

  let(:resource) { self.class.simp(res) }

  let(:namespace) { $1 if res =~ /(.*)\// }

  let(:class_decorator) { (resource.camelize + 'Decorator').constantize }

  let(:cls) { resource.camelize.constantize }

  it "sets dude as the admin of the #{simp(res)} he created", js: USE_JS do
    user_login_success(dude, password: 'foobar')
    start_new_object(visit: true)
    expect(bootstrap_selected("#{resource}_admins")).to eq(dude.to_s)
    fill_form(try(:object) || build(resource, :simple))
    commit
    expect_flash_success
    created = cls.last
    expect(dude).to have_role(:Admin, created)
  end

  it "allows dude to set dony as the admin of the #{simp(res)} he created", 
    js: USE_JS do
    user_login_success(dude, password: 'foobar')
    start_new_object(visit: true)
    expect(bootstrap_selected("#{resource}_admins")).to eq(dude.to_s)
    fill_form(try(:object) || build(resource, :simple))
    bootstrap_select(dony.to_s, from: "#{resource}_admins")
    commit
    expect_flash_success
    created = cls.last
    expect(dude).to have_role(:Admin, created)
    expect(dony).to have_role(:Admin, created)
  end
end

