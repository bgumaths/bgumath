## frozen_string_literal: true

require 'system/shared_examples'

RSpec.describe 'teaching/issues' do

  include_examples 'regular system includes'

  def current_issues_user
    find('#issues-login-id > a')&.text
  rescue Capybara::ElementNotFound
    nil
  end

  def added_issue(modal)
    (modal && js) ? added_row.all('td')[0].text : current_object
  end

  def current_issue
    Issue.find(current_object)
  end

  def visit_issue(issue)
    visit(teaching_issue_path(issue))
  end

  def submit_issue(issue, **opts)
    modal = start_new_object(IssueDecorator, **opts)

    ## expectations about the form
    expect(page).to have_content(
      I18n.t(:correspondence, scope: %i[simple_form hints issue])
    )

    with_locale(:he) do
      val = issue.course.decorate.to_label
      val.gsub!(/<span[^>]*>/, '')
      val.gsub!(/<\/span>/, '')
      bootstrap_select(val, from: 'issue_course')
    rescue Capybara::ElementNotFound
      if opts[:status] == :failure
        return
      else
        raise
      end
    end

    %i[title content correspondence].each do |ff|
      fill_in("issue_#{ff}", with: issue.public_send(ff))
    end
    commit
    if opts[:status] == :success
      created = Issue.find(added_issue(modal))
      expect(created.title).to eq(issue.title)
      if modal
        visit_issue(created)
      else
        expect_flash_success
      end
      return created
    end
    if opts[:status] == :failure
      expect_flash_failure
      expect(page).to have_current_path(new_teaching_issue_path)
    end
    if opts[:status] == :blocked
      expect(page).to have_selector('p.error')
    end

  end

  def display_status
    within('dl') do
      items = all('*').to_a
      itm = items.shift.text until (itm == 'סטטוס' || items.empty?)
      return items.shift.text if items.present?
    end
  end

  def index_issues
    all('table#item-list > tbody > tr > td:first-child').map(&:text)
    .reject{|x| x == 'No data available in table' || x == 'לא נמצאו רשומות מתאימות' }
  end

  def issues_user_login(user, **opts)
    opts = (
      user.present? ? {login: user.login, password: user.password} : {}
           ).deep_merge(opts)

    visit(login_teaching_issues_path)
    fill_in 'issues_user_session_login', with: opts[:login]
    fill_in 'issues_user_session_password', with: opts[:password]
    commit
  end

  def issues_user_logout
    visit logout_teaching_issues_path
  end

  def issues_user_login_success(user, **opts)
    issues_user_login(user, **opts)
    expect(page).to have_selector("object[data*='issues_flowchart']")
    expect(current_issues_user).to eq(user.fullname)
    expect(index_issues).to match_array(user.issues.map(&:slug))
  end

  before(:context) do
    @vice = create(:regular, :simple, :vice, email: 'bibi')
    @term = Term.current || create(:term, :simple, :current)
    @lect1 = create(:regular, :simple, course_count: 0, email: 'dony',
                   password: 'foobar')
    @lect2 = create(:regular, :simple, course_count: 0, email: 'dude',
                   password: 'foobar')
    @issues_student_rep1 = create(:issues_student_rep, login: 'monk',
                                  course_count: 0)
    @issues_student_rep2 = create(:issues_student_rep, login: 'davis',
                                  course_count: 0)
    @issues_aguda_rep = create(
      :issues_aguda_rep, login: 'coltrane',
      course_count: 0, password: 'foobar')
    @gc1 = create(:generic_course, :basic, :simple, 
                  aguda_rep: @issues_aguda_rep)
    @gc2 = create(:generic_course, :basic, :simple,
                  aguda_rep: nil)
    @gc3 = create(:generic_course, :basic, :simple,
                  aguda_rep: @issues_aguda_rep)
    @gc4 = create(:generic_course, :basic, :simple,
                  aguda_rep: @issues_aguda_rep)
    @department1 = create(:department, catalog_id: '123',
                          courses: [@gc1, @gc2])
    @department2 = create(:department, catalog_id: '456',
                          courses: [@gc3, @gc2])
    @issues_staff1 = create(:issues_staff, login: 'ganz', 
                            department: @department1)
    @issues_staff2 = create(:issues_staff, login: 'boogy',
                            department: @department1)
    @issues_staff3 = create(:issues_staff, login: 'lapid',
                            department: @department2, pass_changed: false)
    @issues_staff4 = create(:issues_staff, login: 'hendel',
                            department: @department2)
    @course1 = create(:course, lecturer: @lect1, term: @term,
                      student_rep: @issues_student_rep1, generic_course: @gc1)
    @course2 = create(:course, lecturer: @lect2, term: @term,
                        generic_course: @gc2, student_rep: nil)
    @course3 = create(:course, lecturer: @lect1, term: @term,
                        student_rep: @issues_student_rep2,
                        generic_course: @gc3)
    @course4 = create(:course, lecturer: nil, term: @term,
                        student_rep: @issues_student_rep1,
                        generic_course: @gc4)
    @issue3 = create(:issue, course: @course3)
    ## ensure that the replier is _not_ an issue repsonder
    travel(-2.day) do
      @issue4 = create(:issue, :replied, course: @course3, replier: @lect2)
    end
    @issue5 = create(:issue, :replied, course: @course3, replier: @lect2)
    remove_responders
  end
  let(:unsaved) { build(:issues_student_rep, password: '', login: 'brubeck',
                        courses: [@course2]) }
  let(:issue) { build(:issue, course: @course2, reporter: unsaved) }
  let(:issue1) { build(:issue, course: @course1) }
  let(:issue2) { build(:issue, course: @course2) }
  let(:issue_no_lect) { build(:issue, course: @course4) }
  let(:responder1) {
    build(:regular, :simple, :responder, password: 'foobar', email: 'bach')
  }
  let(:responder2) {
    build(:regular, :simple, :responder, password: 'barfoo', email: 'chopin')
  }
  
  # 
  # ## Logging in
  # 
  # To use most features of the issues system, an external user must login, 
  # via the {https://www.math.bgu.ac.il/teaching/issues/login Issues User 
  # login page}.  This login is different and independent from the math 
  # department members login, located at the bottom of each page.
  describe 'Logging in:' do
    it 'logs in a valid issues user' do
      # 
      # ### User types
      # 
      # There are three types of issues users:
      # 
      # 1. Student representatives: These are students studying the courses 
      #    on which issues can be filed, elected by the students, one for 
      #    each course. Their login id must be a valid login at the 
      #    `post.bgu.ac.il` domain.
      @issues_student_rep1.password = 'foobar'
      @issues_student_rep1.password_confirmation = 'foobar'
      @issues_student_rep1.save!
      issues_user_login_success(@issues_student_rep1)

      # 
      # 2. Issues staff: Staff from other departments whose students are 
      #    attending math courses. Each such user belongs to a department, 
      #    and each department has a list of (generic) courses associated to 
      #    it. Login id must be a valid user at the `bgu.ac.il` domain.
      issues_user_login_success(@issues_staff1)

      # 
      # 3. Aguda representatives: Representatives from the students' union.  
      #    Assigned per (generic) courses. Login id should be a valid 
      #    `aguda.bgu.ac.il` id.
      issues_user_login_success(@issues_aguda_rep)
    end


    # 
    # ### Normal flow
    # 
    # When the user logs in, they are presented with the 
    # {https://www.math.bgu.ac.il/teaching/issues list of issues} relevant 
    # to them.
    # In addition, it displays a flowchart describing the issue submission 
    # process.
    # 
    # If the login id does not exist, or the password is wrong, the user is 
    # redirected to the {https://www.math.bgu.ac.il/teaching/issues/login 
    # login page}.
    it 'does not login a non-existing user' do
      issues_user_login(@issues_student_rep1, login: 'foo')
      expect(page).to have_current_path(login_teaching_issues_path)
      expect_flash_failure
    end

    it 'does not login with wrong password' do
      issues_user_login(@issues_student_rep1, password: 'foo')
      expect(page).to have_current_path(login_teaching_issues_path)
      expect_flash_failure
    end

    it 'does not login with empty password when the password is empty' do
      issues_user_login(unsaved, password: '')
      expect(page).to have_current_path(login_teaching_issues_path)
      expect_flash_failure
    end

    context 'Generated passwords:' do
      # 
      # ### Generated passwords
      # 
      it "allows the web admin to reset an issues user's password",
        js: true do
        html_id = "reset-password-#{@issues_student_rep1.decorate.to_html_id}"
        ## fail to reset password without privileges
        visit(teaching_issues_user_path(@issues_student_rep1))
        expect(page).to have_no_selector("##{html_id}")

        # It is possible for a suitably privileged user (for instance, the 
        # site admin) to reset an issues user's password, by clicking the 
        # `reset password` button next to that user's name. This is useful if 
        # the user does not wish to use their University password to login.
        # 
        user_login_success(User.admin, password: 'foobar')

        visit(teaching_issues_user_path(@issues_student_rep1))
        accept_confirm do
          click_on(html_id)
        end
        expect_flash_success

        # When this is done, a new random password is generated for the user.
        @issues_student_rep1.reload
        expect(@issues_student_rep1).to_not be_pass_changed

        # An email with this password is sent to their University address, so 
        # that they can login.
        check_mail type: :reset_password,
          body: [@issues_student_rep1.login, 'סיסמא'] do |mail|
          @pass = $1 if mail.parts.first.parts.first.decoded =~ /סיסמא.*: `(.*)`/
          @pass.chomp!
        end

        # 
        # When the user does login, they are redirected to their details page, 
        # so that they can change their password.
        issues_user_login(@issues_student_rep1, password: @pass)
        expect_flash_success
        expect_edit_page(@issues_student_rep1)

        # Until they had done so, the user is blocked from performing any 
        # issues action,
        ## no modal button on the index page
        visit(teaching_issues_path)
        expect(page).to have_no_selector('button.btn-new-item')
        start_new_object(IssueDecorator, modal: false)
        expect_flash_failure
        # and is redirect to change the password if such an action is 
        # attempted.
        expect_edit_page(@issues_student_rep1)

        # This form also requires the user to accept the terms of usage.
        fill_in(:issues_user_password, with: 'foobar')
        fill_in(:issues_user_password_confirmation, with: 'foobar')
        commit
        expect_flash_failure
        expect_edit_page(@issues_student_rep1)
        # 
        # After successfully changing the password and accepting the terms of 
        # usage,
        fill_in(:issues_user_password, with: 'foobar')
        fill_in(:issues_user_password_confirmation, with: 'foobar')
        #check('issues_user_terms_of_usage')
        check('קראתי ואני מסכים לתנאי השימוש', allow_label_click: true)
        commit
        expect_flash_success
        @issues_student_rep1.reload
        @issues_student_rep1.password = 'foobar'
        @issues_student_rep1.password_confirmation = 'foobar'
        expect(@issues_student_rep1).to be_pass_changed

        # a confirmation email is sent to the user,
        check_mail type: 'changed_password'
        # the user may login again
        issues_user_login_success(@issues_student_rep1)
        # and may use the issues system normally.
        responder1.save!
        submit_issue(issue1, status: :success)
        # 
        # *Remark*: In a previous version of the system, empty passwords were 
        # not allowed, and a password was generated for the user upon their 
        # creation. This is no longer the case, since issues users may now 
        # identify using the University services, using their University 
        # password.
      end
    end
  end

  # 
  # ## The issue life cycle
  describe 'The issue life cycle:' do
    # 
    # An issue goes through the following stages.
    # 
    # ### Creation
    # 
    context 'Creation:' do
      # A new issue can be created by a student representative (and only by 
      # them). Such a representative may only create an issue in one of 
      # their courses.
      it 'can only be created if the student rep belongs to the course',
        js: USE_JS do
        @issues_student_rep1.password = 'foobar'
        @issues_student_rep1.password_confirmation = 'foobar'
        @issues_student_rep1.save!
        issues_user_login_success(@issues_student_rep1)
        submit_issue(issue2, status: :failure)
      end

      it 'can be created by a student representative', js: USE_JS do
        @issues_student_rep1.password = 'foobar'
        @issues_student_rep1.password_confirmation = 'foobar'
        @issues_student_rep1.save!
        issues_user_login_success(@issues_student_rep1)
        responder1.save!
        created_issue = submit_issue(issue1, status: :success)
        # When an issue is created, its status is `open`.
        # 
        expect(created_issue).to be_open
        expect(display_status).to eq('פתוחה')
        # A creation email is sent to the reporter and the responders, with 
        # `cc` to the lecturer of the relevant course, the relevant external 
        # issue staff, the relevant aguda representative, and the vice head 
        # of the department.
        check_mail to: [@issues_student_rep1, responder1],
          cc: [@lect1, @issues_aguda_rep, @issues_staff1, @issues_staff2,
               @vice],
               type: 'created'
      end

      it 'can be created for a course without a lecturer', js: USE_JS do
        @issues_student_rep1.password = 'foobar'
        @issues_student_rep1.password_confirmation = 'foobar'
        @issues_student_rep1.save!
        issues_user_login_success(@issues_student_rep1)
        responder1.save!
        created_issue = submit_issue(issue_no_lect, status: :success)
        # When an cissue is created for a course whose lecturer is not 
        # updated, the issue is still created
        expect(created_issue).to be_open
        expect(display_status).to eq('פתוחה')
        # but an email is sent to the department staff, warning about the
        # situation
        check_mail type: 'missing_lecturer', to: [User.admin, Role.vice]
      end

      # If no issue responders are defined in the system,
      it 'refuses to create an issue if there are no responders', js: USE_JS do
        ## reload loses password...
        #@issues_student_rep1.reload
        @issues_student_rep1.password = 'foobar'
        @issues_student_rep1.password_confirmation = 'foobar'
        @issues_student_rep1.save!
        issues_user_login_success(@issues_student_rep1)
        # it refuses to open a new issue, 
        submit_issue(issue1, status: :blocked)
        # and a warning message about missing responders is sent to the 
        # department staff.
        check_mail type: 'missing_repliers', to: [User.admin, Role.vice]
      end

      # 
      # After the issue has been created, it is possible for a responder to 
      # resend the creation email, either to a particular recepient,
      it 'can be recalled to an individual by an issue responder', js: true do
        responder1.save!
        user_login_success(responder1)
        visit_issue(@issue3)
        el = all("[id^='#{@issues_student_rep2.login}-created-button-']")[0]
        accept_confirm do
          el.click
        end
        expect(page).to have_selector('h5#modal-label')
        ## close report
        find('#modal-close').click
        check_mail type: :created, to: [@issues_student_rep2],
          body: @issue3.content
      end

      # or to all of them, by clicking the appropriate button on the issue 
      # page.
      it 'can be recalled to all participants by an issue responder',
        js: true do
        responder1.save!
        user_login_success(responder1)
        visit_issue(@issue3)
        accept_confirm do
          click_on('send_created_button')
        end
        expect(page).to have_selector('h5#modal-label')
        ## close report
        find('#modal-close').click
        check_mail type: :created,
          to: [@issues_student_rep2, responder1],
          cc: [@lect1, @issues_aguda_rep, @issues_staff3, @issues_staff4,
               @vice],
               body: @issue3.content
      end
    end

    # 
    # ### Response
    # 
    context 'Response:' do
      # After the issue is created, any of the _issues responders_ may post 
      # a reply, by logging in and visiting the issue page (login for the 
      # issues responders is via the bottom login box, like for other 
      # department members)
      it 'does not allow non-responders to enter a reply' do
        visit_issue(@issue3)
        expect(page).to have_no_selector('textarea#issue_response_content')
      end

      it "is replied to by an issue responder" do
        ## login
        responder1.save!
        responder2.save!
        user_login_success(responder2)
        visit_issue(@issue3)
        expect(page).to have_selector('textarea#issue_response_content')
        fill_in('issue_response_content', with: 'This is a reply')
        commit
        expect_flash_success
        issue3 = current_issue
        expect(issue3).to eq(@issue3)
        expect(issue3.response.content).to eq 'This is a reply'
        # 
        # An email message is sent about the update to the course lecturer 
        # and the external staff (but **not** the student or aguda rep).
        check_mail type: :replied,
          to: [@lect1, @issues_staff3, @issues_staff4],
          cc: [responder1, responder2, Role.vice]
        # The status of the issue is changed to `replied`.
        expect(display_status).to eq('בטיפול')
        expect(issue3).to be_replied
      end

      #    
      # When an issue is `replied`, the reply is visible to the external 
      # staff involved
      it 'is visible to external staff' do
        @issues_staff4.reload
        issues_user_login_success(@issues_staff4)
        visit_issue(@issue4)
        expect(page).to have_content(@issue4.response.content)
      end

      # and to the lecturer in the course,
      it 'is visible to the course lecturer' do
        user_login_success(@lect1)
        visit_issue(@issue4)
        expect(page).to have_content(@issue4.response.content)
      end

      # but not to the students,
      it 'is not visible to the issues student' do
        issues_user_login_success(@issues_student_rep2)
        visit_issue(@issue4)
        expect(page).to_not have_content(@issue4.response.content)
      end

      # the aguda representative,
      it 'is not visible to the aguda rep' do
        issues_user_login_success(@issues_aguda_rep)
        visit_issue(@issue4)
        expect(page).to_not have_content(@issue4.response.content)
      end

      # irrelevant external staff,
      it 'is not visible to irrelevant external staff' do
        issues_user_login_success(@issues_staff1)
        visit_issue(@issue4)
        expect(page).to_not have_content(@issue4.response.content)
      end

      # or other department members
      it 'is not visible to other department members' do
        user_login_success(@lect2)
        visit_issue(@issue4)
        expect(page).to_not have_content(@issue4.response.content)
      end

    end
    # 
    # ### Publishing
    #
    context 'Publishing:' do
      # 
      # When the response is given, the lecturer and the external stuff are 
      # given time to review it. They may discuss it with the responders, 
      # and if needed, the responders may modify it.
      it 'can be modified by an issue responder' do
        responder1.save!
        user_login_success(responder1)
        visit_issue(@issue4)
        fill_in('issue_response_content', with: 'A modified reply')
        commit
        expect_flash_success
        issue4 = current_issue
        expect(issue4).to eq @issue4
        expect(issue4.response.content).to eq 'A modified reply'
        # When this happens, an update email is once again sent to the 
        # relevant parties
        check_mail type: :replied,
          to: [@lect1, @issues_staff3, @issues_staff4],
          cc: [responder1, Role.vice]
        expect(display_status).to eq('בטיפול')
        expect(issue4).to be_replied
      end

      # unless committed via the ``update without alert'' button
      it 'can be modified without email by an issue responder' do
        responder1.save!
        user_login_success(responder1)
        visit_issue(@issue4)
        fill_in('issue_response_content', with: 'A modified reply')
        commit 'commit_no_email'
        expect_flash_success
        issue4 = current_issue
        expect(issue4).to eq @issue4
        expect(issue4.response.content).to eq 'A modified reply'
        check_no_mail type: :replied
        expect(display_status).to eq('בטיפול')
        expect(issue4).to be_replied
      end

      # 
      # Once again, after the response was given, it is possible for a 
      # responder to resend the update email, either to a particular 
      # recepient,
      it 'can be recalled to an individual by an issue responder', js: true do
        responder1.save!
        user_login_success(responder1)
        visit_issue(@issue4)
        el = all("[id^='#{@lect1.email}-update-button-']")[0]
        accept_confirm do
          el.click
        end
        expect(page).to have_selector('h5#modal-label')
        ## close report
        find('#modal-close').click
        check_mail type: :replied, to: [@lect1], body: @issue4.response.content
      end

      # or to all of them
      it 'can be recalled to all participants by an issue responder',
        js: true do
        responder1.save!
        user_login_success(responder1)
        visit_issue(@issue4)
        accept_confirm do
          click_on('send_update_button')
        end
        expect(page).to have_selector('h5#modal-label')
        ## close report
        find('#modal-close').click
        check_mail type: :replied,
          to: [@lect1, @issues_staff3, @issues_staff4],
          cc: [responder1, Role.vice],
          body: @issue4.response.content
      end
      # 
      # After a set amount of time (24 hours) with no changes to the 
      # response, the issue is considered publishable.
      # The responders receive an email reminder when this time period has 
      # passed.
      it 'sends a publish email at the right time' do
        responder1.save!
        user_login_success(responder1)
        visit_issue(@issue4)
        fill_in('issue_response_content', with: 'A modified reply')
        expect { commit }.to have_enqueued_job(SendPublishReminderJob).once
          .and have_enqueued_job(
            SendPublishReminderJob
        ).with(
          @issue4
        ).at(a_value_within(1.minute).of(24.hours.from_now))
      end

      # At this point, a responder may publish it to the students by 
      # clicking the `publish` button.
      it 'becomes public when the publish button is pressed' do
        responder1.save!
        user_login_success(responder1)
        visit_issue(@issue4)
        expect(display_status).to eq('בטיפול')
        expect(page).to have_selector("button.btn[value='publish']")
        # 
        # When the button is clicked, the issue changes status to 
        # `published`.
        # 
        # A pdf copy of the issue is saved to `issues/<term>/<issue>.pdf` in 
        # the project root (on the production, this is symlinked inside 
        # `~wwwmath/db/` on the cs servers).
        expect { commit('פירסום') }.to have_enqueued_job(SaveIssuePdfJob)
        expect(current_path).to eq(teaching_issue_path(@issue4))
        expect(display_status).to eq('סגורה')
        issue4 = current_issue
        expect(issue4).to be_published
        # 
        # An email message with the reply is sent to the student and the 
        # other people involved
        check_mail type: :published,
          to: [@issues_student_rep2, responder1],
          cc: [@lect1, @issues_staff3, @issues_staff4, @issues_aguda_rep, 
               Role.vice],
               body: @issue4.content

        # 
        # At this point, it is no longer possible to modify the issue
        expect(page).to_not have_selector 'textarea'
      end
      # 
      # The reply can also be published before the time interval has passed. 
      it 'can be published prematurely', js: true do
        responder1.save!
        user_login_success(responder1)
        visit_issue(@issue5)
        expect(display_status).to eq('בטיפול')
        # In this case, a confirmation dialog pops up, to confirm 
        # publishing.
        dismiss_confirm do
          click_on('פירסום')
        end
        expect(display_status).to eq('בטיפול')
        expect {
          accept_confirm { click_on('פירסום') }
          sleep(1)
        }.to have_enqueued_job(SaveIssuePdfJob)
        expect(current_path).to eq(teaching_issue_path(@issue5))
        expect(display_status).to eq('סגורה')
        issue = current_issue
        expect(issue).to be_published
        check_mail type: :published,
          to: [@issues_student_rep2, responder1],
          cc: [@lect1, @issues_staff3, @issues_staff4, @issues_aguda_rep, 
               Role.vice],
               body: @issue5.content

        expect(page).to_not have_selector 'textarea'
        #expect(File).to exist(Rails.root.join('issues', issue.term.slug,
        #"#{issue.slug}.pdf"))

      end
    end
  end
end

