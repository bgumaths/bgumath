{::options auto_ids="true" parse_block_html="true" /}

Homepage of <%= @person %><%= @person.action_list %>
====================================================

Some stuff about the dude.

<div class="row-flex justify-content-center">
<div class="col-sm-8 col-md-6 col-lg-4 bordered">
<dl class="row details-dl">
<dt class="col-sm-5"><%= tt :office %></dt>
<dd class="col-sm-7">
<%= @person.office %>
<% unless @person.office == @person.oh_location || @person.oh_location.blank? %>
(<%= tt 'teaching.hours' %>: <%= @person.oh_location %>)
<% end %>
</dd>
<dt class="col-sm-5"><%= tt :phone %></dt>
<dd class="col-sm-7"><%= @person.phone %></dd>
<dt class="col-sm-5"><%= tt :email %></dt>
<dd class="col-sm-7"><%= @person.mailed(@person.email) %></dd>
<% unless @person.hours.blank? %>
<dt class="col-sm-5"><%= tt 'teaching.hours' %></dt>
<dd class="col-sm-7"><%= @person.hours %></dd>
<% end %>
</dl>
</div>
</div>

<% unless (@person.interests.blank? and @research_groups.blank?) %>
<div class="row-flex justify-content-center my-3">
<div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
<div class="card">
  <div class="card-header">
<h4 class="card-title"><%= tt 'research.title' %></h4>
  </div>
  <div class="card-block">
<div dir="auto" class="card-text mathjax">
  <%= markdown(@person.interests) %>
</div>
<% unless @research_groups.blank? %>
  <div class="pb-2">
  <h5 class="card-subtitle"><%= tt 'research.research_groups' %></h5>
  <div dir="auto">
    <%= list_to_l(@research_groups) %>
  </div>
  </div>
<% end %>
<%= render 'show_extra' %>
  </div>
</div>
</div>
</div>
<% end %>

<% unless @courses.blank? %>
<div class="row-flex justify-content-center">
<div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
<h4><%= tt('teaching.generic_courses') %> <%= tt('teaching.cterm') %></h4>
<div class="card" id="teaching.courses.panel">
<div class="card-block">
<ul class="card-text">
  <% @courses.each do |c| %>
    <li><%= c.linked %></li>
  <% end %>
</ul>
</div>
</div>
</div>
</div>
<% end %>

<h5 class="text-center"><%= @person.roles_html %></h5>

<% # vim: ft=eruby.markdown
%>

