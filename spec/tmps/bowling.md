{::options auto_ids="true" parse_block_html="true" /}


<% content_for :head do %>
<%= auto_discovery_link_tag :atom, @seminar.meetings_url(format: :atom) %>
<%= auto_discovery_link_tag :rss, @seminar.meetings_url( format: :rss) %>
<% end %>
<%= h1 do %>
<%= get_title %>
<span class="formats">
<%= link_to glyphicon(:ical),
@seminar.meetings_url(format: :ics, protocol: 'webcal') %>
<%= link_to glyphicon(:atom), @seminar.meetings_url(format: :atom) %>
<%= link_to glyphicon(:ml, tt(:ml)), @seminar.ml_path.to_s if @seminar.ml_path %>
</span>
<%= @seminar.action_list *%w[edit duplicate] %>
<% end %>

Yoohoo
======

<% if @seminar.description %>
<div class="row-flex justify-content-center">
<div class="col col-sm-10 col-md-8 col-lg-6 bordered well mathjax"
dir="auto">
<%= markdown(@seminar.description) %>
</div>
</div>
<% end %>

<div class="lead" dir="auto">
<%= t 'research.seminar.coords_html', day: @seminar.day,
starts: l(@seminar.starts), ends: l(@seminar.ends),
room: @seminar.place %>
</div>

<% unless @coming.blank? %>
<div>
<h2 dir="auto"><%= tt :thisweek %></h2>
<% @coming.each do |m| %>
<div>
<hr/>
<% if m.announcement.blank? %>
<div class="lead">
<%= m.unusual_html %>
</div>

<p>
<h4 class="text-center"><%= m.full_speaker(from_tag: :em) %></h4>
</p>

<div class="row-flex justify-content-center">
<div class="col-sm-10 col-md-8 seminar bordered mt-1">
<h3><%= m.title %></h3>
<div class="mathjax seminar-abstract" dir="auto">
<%= markdown(m.abstract) %>
</div>
</div>
</div>
<% else %>
<div class="lead text-center mathjax seminar-announcement"
dir="auto">
<%= markdown(m.announcement) %>
</div>
<% end %>
</div>
<% end %>
</div>
<% end %>

<hr/>
<h2><%= tt 'research.meetings', term: @seminar.term %></h2>
<div class="text-center">
<%= render partial: 'term_pager',
locals: { to_path: ->(tt) { @seminar.show_path(term: tt) } } %>
</div>

<% if @following.present? %>
<% if @past.present? %>
<h3 dir="auto" class="mt-1"><%= tt 'research.meetings!upcoming' %></h3>
<% end %>
<%= render 'research/seminar/meetings/meeting_table', meetings: @following %>
<% end %>

<% if @past.present? %>
<% if @following.present? %>
<h3 dir="auto" class="mt-1"><%= tt 'research.meetings!past' %></h3>
<% end %>
<%= render 'research/seminar/meetings/meeting_table', meetings: @past %>
<% end %>

<% if @seminar.admins.present? %>
<div class="my-2 lead seminar-admins">
<p>
<%= t('runby_html', what: tt('research.seminar.title'),
who: @seminar.linked_admins) %>
</p>
</div>
<% end %>

<div class="text-center">
<%= new_button('Meeting', @seminar.new_meeting_path) %>
<%= link_to(tt(:back, where: tt('research.seminars')), @seminar.index_path,
class: %w[btn btn-primary]) %>
<%= alt_fmt('PDF') %>
<%= alt_fmt('LaTeX', :tex) %>

</div>



<% # vim: ft=eruby.markdown
%>

