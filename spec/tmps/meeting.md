{::options auto_ids="true" parse_block_html="true" /}

*SOME AWESOME STUFF HERE*

<%= h1 %>

<% if @meeting.announcement.blank? %>
<% content_for :head do %>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "EducationEvent",
"name": "<%= markdown(@meeting, inline: true) %>",
"startDate" : "<%= @meeting.starts(true).to_s(:iso8601) %>",
"endDate" : "<%= @meeting.ends(true).to_s(:iso8601) %>",
"location" : {
"@type" : "Place",
"sameAs" : "<%= root_url(locale: '') %>",
"name" : "<%= t(:depname) %>",
"address" : "<%= @meeting.room %>, <%= t(:fdepname) %>"
},
"performer" : {
"@type" : "Person",
"name" : "<%= @meeting.speaker %>",
"sameAs" : "<%= @meeting.speaker_web %>",
"affiliation" : "<%= @meeting.from %>"
}
}
</script>
<% end %>
<h2 class="mathjax my-3">
<%= markdown(@meeting, inline: true) %><%= @meeting.action_list %>
</h2>

<h3 class="my-2"><%= @meeting.full_speaker(true) %></h3>

<div class="row justify-content-center">
<div class="col-md-10">
<div class="coords">
<p>
<em><%= l @meeting.date, format: :daylong %>,
<%= l @meeting.starts %> &ndash; <%= l @meeting.ends %></em>
</p>
<p>
<b><%= @meeting.room %></b>
</p>
</div>
<% if @unusual %>
<div class="lead text-center">
<p>
<strong><%= tt :unusual, what: @unusual %>!</strong>
</p>
</div>
<% end %>
</div>
</div>

<% if @meeting.has_abstract? %>
<div class="row justify-content-center">
<div class="col col-md-8 my-2">
<div class="seminar bordered mathjax lead p-1" dir="auto">
<strong><%= tt :abstract %>: </strong>
<div dir="auto">
<%= markdown(@meeting.abstract) %>
</div>
<%= render partial: 'embed_files', object: @meeting.descfiles %>

</div>
</div>
</div>
<% end %>

<% else %>

<div class="row justify-content-center">
<div class="col col-md-8 my-2">
<div class="bordered well mathjax lead text-center" dir="auto">
<%= markdown(@meeting.announcement) %>
</div>
</div>
</div>

<% end %>

<div class="text-center my-3">
<%= link_to(tt(:back, where: @meeting.seminar),
@meeting.seminar.show_path,
class: %w[btn btn-primary]) %>
<%= alt_fmt('PDF') %>
<%= alt_fmt('LaTeX', :tex) %>

<% if current_user %>
<%= link_to(tt(:sendemail),
email_research_term_seminar_meeting_path(@meeting.term, @meeting.seminar, @meeting),
class: %w[btn btn-primary]) %>
<% end %>
<%= addr = policy(@meeting).email_ml? ? @msg.to.first : ''
mail_to(addr.blank? ? 'some@one' : addr,
tt(:emailto, who: addr.present? ? tt(:ml) : '...'),
subject: @meeting.seminar.to_s,
body: @msg.decoded,
encode: 'javascript',
class: 'btn btn-primary')+
content_tag(:noscript,
mail_to(addr.blank? ? 'some@one' : addr.gsub('.', '_DOT_'),
tt(:emailto, who: addr.present? ? tt(:ml) : '...'),
subject: @meeting.seminar.to_s,
body: @msg.decoded,
class: 'btn btn-primary')).html_safe
%>


</div>




<% # vim: ft=eruby.markdown
%>

