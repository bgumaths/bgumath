{::options auto_ids="true" parse_block_html="true" /}


**SOMETHING EXTRAA**

<%= underlined("#{@what.seminar} Seminar") %>

On <%= l @what.date, format: :daylong %>,
At <%= l @what.starts %> -- <%= l @what.ends %>,
In <%= @what.room %>

<%= centered("#{@what.speaker} (#{@what.from})") %>

will talk about

<%= underlined(@what.title, '-') %>
<% if @what.abstract %>

Abstract:
=========
<%= word_wrap(@what.abstract || '', line_width: 72) %>
<% end %>
<% if @what.unusual %>

    Please note the unusual <%= @what.unusual %>!
<% end %>








<% # vim: ft=eruby.markdown
%>

