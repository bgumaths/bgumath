require 'rails_helper'

RSpec.describe SaveIssuePdfJob, type: :job do

  let(:issue) { create(:issue, :published) }

  describe '#perform_now' do
    it 'creates a pdf report' do
      file = SaveIssuePdfJob.perform_now(issue)
      expect(File).to exist(file)
    end
  end

end

