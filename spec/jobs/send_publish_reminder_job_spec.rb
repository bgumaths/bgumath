require 'rails_helper'

RSpec.configure do |c|
  c.before(:example) do
    ActionMailer::Base.default_url_options[:locale] = I18n.locale
    ActionMailer::Base.default_url_options[:host] = 'test.host'
    ActionMailer::Base.default_url_options[:protocol] = 'https'
    Rails.application.routes.default_url_options = ActionMailer::Base.default_url_options
  end
end


RSpec.describe SendPublishReminderJob, type: :job do
  let(:held) { create(:issue, :replied) }
  let(:replied) {
    travel(-2.days) do
      create(:issue, :replied)
    end
  }
  let(:published) { create(:issue, :published) }
  let(:message) { Teaching::IssueMailer.reminder(replied) }

  describe '#perform_later' do
    it 'enqueues the job' do
      expect {
        SendPublishReminderJob.set(wait: held.publish_delay.hours)
          .perform_later(held)
      }.to have_enqueued_job(SendPublishReminderJob)
    end
  end

  describe '#perform_now' do
    let(:mail) {
      mail = ActionMailer::Base.deliveries.last
      ActionMailer::Base.deliveries = []
      mail
    }

    it 'sends a message if issue is not published' do
      SendPublishReminderJob.perform_now(replied)
      expect(mail).to be_present
      expect(mail.subject).to eq(message.subject)
    end

    it 'does nothing if issue is published' do
      SendPublishReminderJob.perform_now(published)
      expect(mail).to be_blank
    end

    it 'does nothing if issue is held' do
      SendPublishReminderJob.perform_now(held)
      expect(mail).to be_blank
    end
  end
end

