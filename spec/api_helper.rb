# frozen_string_literal: true

require 'rails_helper'
require 'graphiti_spec_helpers/rspec'

RSpec.configure do |config|
  config.include GraphitiSpecHelpers::RSpec
  config.include GraphitiSpecHelpers::Sugar
  config.before :each do
    handle_request_exceptions(true)
    ActionMailer::Base.default_url_options[:locale] = I18n.locale
    ActionMailer::Base.default_url_options[:host] = 'test.host'
    ActionMailer::Base.default_url_options[:protocol] = 'https'
    Rails.application.routes.default_url_options = ActionMailer::Base.default_url_options
  end
end

JSONAPI_ERROR = {
  bad_request: {
    status: '400',
    title: 'Request Error',
    code: 'bad_request',
  },
  unauthorized: {
    status: '401',
    title: 'Unauthorized',
    code: 'unauthorized',
  },
  forbidden: {
    status: '403',
    title: 'Forbidden',
    code: 'forbidden',
  },
  not_found: {
    status: '404',
    title: 'Not Found',
    code: 'not_found',
  },
  unprocessable_entity: {
    status: '422',
    title: 'Validation Error',
    code: 'unprocessable_entity',
  },
  service_unavailable: {
    status: '503',
    title: 'Service Unavailable',
    code: 'service_unavailable',
  },
}.each_value(&:stringify_keys!).freeze

RSpec.shared_examples 'reject with error' do |opts|
  let(:err) { opts.keys[0] }
  let(:bare_msg) { opts[err].is_a?(Symbol) ? try(opts[err]) : opts[err] }
  if opts[:unprocessable_entity]
    let(:err_attr) { error['meta']['attribute'] || '' }
    let(:msg) { "#{err_attr.camelcase}: #{bare_msg}" }
  else
    let(:msg) { bare_msg }
  end

  it "fails with a #{opts.keys[0]} status" do
    expect(response).to have_http_status err
    exp = JSONAPI_ERROR[err.to_sym].dup
    exp['detail'] = msg if msg
    expect(error).to include(exp)
  end

  if opts[:unauthorized]
    it 'requests authentication' do
      expect(response.headers['WWW-Authenticate'])
        .to eq('Token realm="bgumath"')
    end
  end

end

RSpec.shared_examples 'success with correct data' do
  it 'returns successfully with correct data' do
    expect(response).to be_successful
    expect(data).to include('type' => types)
    expect(attributes).to include(obj_attrs(
      pol_attributes - %w[created_at updated_at id web admin_users]
    ))
    expect(attributes.keys)
      .to match_array(pol_attributes + %w[writable] - %w[admin_users id])
  end
end

RSpec.shared_context 'request context' do

  include_examples 'general includes'

  # :model, :user, :objects and :object should be defined in examples

  let(:type) { model.model_name.singular_route_key }
  let(:types) { model.model_name.route_key }
  let(:path) { "/api/v1/#{types}" }
  let(:resource) { (model.name + 'Resource').constantize }
  let(:json) { JSON.parse(response.body) }
  let(:data) { json['data'] }
  let(:errors) { json['errors'] }
  let(:error) { binding.pry unless errors;errors[0] }
  let(:id) { binding.pry unless data;data['id'] }
  let(:attributes) { binding.pry unless data;data['attributes'] }
  let(:object_id) { object.id }
  let(:policy) { Pundit.policy!(user, object) rescue nil }
  let(:api_key) {
    user.present? ?
    ActionController::HttpAuthentication::Token.encode_credentials(
      user.create_api_key
    ) : nil
  }
  let(:default_payload) { to_jsonapi(object, user) }
  let(:payload) { default_payload }
  let(:headers) { api_key.present? ? { Authorization: api_key } : {} }



  let(:pol_attributes) {
    (policy&.allowed_attributes || []).reject{|x| x.to_s =~ /_id$/}
  }

  def obj_attrs(lattr = pol_attributes - %w[slug web])
    obj = object.decorate
    Hash[lattr.map{|att|
      val = obj.send(att)
      val = val.try(:xmlschema) || val
      [att, val]
    }]
  end

  def to_jsonapi(obj, usr = User.admin, **opts)
    params = obj.to_perm_params(usr, **opts)
    rels = {}
    params.keys.select{|x| x.to_s =~ /_id$/}.each do |irel|
      id = params.delete(irel)
      next unless id
      rel = irel.to_s.gsub(/_id$/, '')
      rrel = object.send(rel)
      #rels[rel.to_s.dasherize.to_sym] = { data: {
      rels[rel.to_sym] = { data: {
        type: rrel.class.name.underscore.pluralize, id: rrel.id
      } }
    end
    res = { data: {
      type: obj.class.table_name,
      attributes: params, relationships: rels
    }}
    res[:data][:id] = obj.id if obj.id.present?
    res[:debug] = true
    res
  end

end

RSpec.shared_examples 'get all objects with user' do

  it 'is successful' do
    expect(response).to be_successful, json
    expect(data.count).to eq(objects.count)
    data.each do |item|
      expect(item).to include('type' => types)
    end
  end
end

RSpec.shared_examples 'get object with user' do
  context 'when the object exists' do
    it 'returns a correct id' do
      expect(id.to_s).to eq(object_id.to_s)
    end

    include_examples 'success with correct data'
  end

  context 'when the object does not exist' do
    let(:object_id) {
      (object.id.is_a?(Integer) || object.id =~ /^[0-9]+$/) ? 1000
      : SecureRandom.uuid
    }
    include_examples 'reject with error', not_found: nil
  end
end

RSpec.shared_examples 'reject without user' do
  context 'when no user is given:' do
    let(:user) { nil }

    ## TODO this needs to change to unauthenticated, see issues_policy
    include_examples 'reject with error',
      forbidden: I18n.t(:must_login, locale: :en)

  end
end

RSpec.shared_examples 'destroy object' do
  context 'when the object exists' do
    it 'is successful' do
      expect(response).to be_successful
      expect(json).to eq('meta' => {})
      expect { object.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  context 'when the object does not exist' do
    let(:object_id) {
      (object.id.is_a?(Integer) || object.id =~ /^[0-9]+$/) ? 1000
      : SecureRandom.uuid
    }
    include_examples 'reject with error', not_found: nil
  end
end


