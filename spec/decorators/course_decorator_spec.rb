# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CourseDecorator do
  context 'stringification' do
    it 'should stringify to full course name if title is present' do
      gcourse = build(:generic_course, name: 'foo')
      course = build(:course, generic_course: gcourse, title: 'bar')
      expect("#{course.decorate}").to eq 'bar'
    end

    it 'should stringify to generic course name if title is not present' do
      gcourse = build(:generic_course, name: 'foo')
      course = build(:course, generic_course: gcourse, title: nil)
      expect("#{course.decorate}").to eq 'foo'
    end
  end
end
