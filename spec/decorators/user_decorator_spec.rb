require 'rails_helper'

RSpec.describe UserDecorator do
  context 'email' do
    it 'uses the specified mail domain for the address' do
      user = build(:user, email: 'ridculy', mail_domain: 'unseen.bgu.ac.il')
      expect(user.decorate.email_address).to eq 'ridculy@unseen.bgu.ac.il'
    end
  end
end

