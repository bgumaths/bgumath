# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MeetingDecorator do
  include_examples 'general includes'

  let(:seminar) { build(:seminar, :current) }
  let(:meeting) { build(:meeting, seminar: seminar) }
  before :context do
    Timecop.freeze(default_term_date)
  end

  describe '#online' do
    subject { meeting.decorate.online }
    context 'when seminar is online' do
      context 'and meeting has its own url' do
        it 'returns the meeting url' do
          expect(subject).to eq(meeting.online)
        end
      end
      context 'and meeting has no url' do
        context 'if meeting is in the future' do
          let(:meeting) {
            build(:meeting, online: nil, seminar: seminar, sdate: Date.today)
          }
          it 'returns the seminar url' do
            expect(subject).to eq(seminar.online)
          end
        end
        context 'if meeting is over' do
          let(:meeting) {
            build(:meeting, online: nil, seminar: seminar,
                  date: Date.today - 1.day )
          }
          it 'is false' do
            expect(subject).to be_falsy
          end
        end
      end
    end
    context 'when seminar is offline' do
      let(:seminar) { build(:seminar, :current, online: nil) }
      context 'and meeting has its own url' do
        it 'returns the meeting url' do
          expect(subject).to eq(meeting.online)
        end
      end
      context 'and meeting has no url' do
        let(:meeting) {
          build(:meeting, online: nil, seminar: seminar)
        }
        it 'is false' do
          expect(subject).to be_falsy
        end
      end
    end
  end
end
