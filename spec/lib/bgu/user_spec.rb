# frozen_string_literal: true

require 'bgu/user'
require 'rails_helper'

RSpec.describe Bgu::User do
  before(:context) do
    @login = Rails.application.credentials.bgu_test_user[:login]
    @id = Rails.application.credentials.bgu_test_user[:id]
    @pass = Rails.application.credentials.bgu_test_user[:password]
  end

  let(:login) { @login }
  let(:id) { @id }
  let(:pass) { @pass }
  let(:user) { Bgu::User.new(login, id, faraday: { ssl: { verify: false } }) }
  subject { user.auth?(pass) }

  describe '.auth?' do
    context 'when credentials are valid' do
      it 'is true and returns correct details' do
        expect(subject).to be_truthy
        expect(user).to have_attributes(
          first_name: 'מורי',
          last_name: 'המחלקה',
          user_name: 'stamt',
          email_address: 'stamt@bgu.ac.il',
          type: 'Students',
          error: nil,
        )
      end
    end

    context 'when login is wrong' do
      let(:login) { @login + 'foo' }
      it 'is false and contains an error' do
        expect(subject).to be_falsey
        expect(user.details).to be_nil
        expect(user.error).to eq 'Wrong user name or password'
      end
    end

    context 'when id is wrong' do
      let(:id) { @id.to_s + '123' }
      it 'is false and contains an error' do
        expect(subject).to be_falsey
        expect(user.details).to be_nil
        expect(user.error).to eq 'Wrong user name or password'
      end
    end

    context 'when password is wrong' do
      let(:pass) { @pass + 'foo' }
      it 'is false and contains an error' do
        expect(subject).to be_falsey
        expect(user.details).to be_nil
        expect(user.error).to eq 'Wrong user name or password'
      end
    end
  end
end

