FactoryBot.define do
  factory :hosts_postdoc do
    association :postdoc, :simple
    association :host, :simple
  end
end
