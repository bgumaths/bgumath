# frozen_string_literal: true

FactoryBot.define do
  factory :degree do
    name_i18n { '' }
    description_i18n { '' }
    slug { 'MyString' }
  end
end
