FactoryBot.define do
  factory :app_setup do
    depname_i18n { { en: '', he: '' } }
    fdepname_i18n { { en: '', he: '' } }
    department_i18n { { en: '', he: '' } }
    university_i18n { { en: '', he: '' } }
    building_i18n { { en: '', he: '' } }
    dept_snail_i18n { { en: '', he: '' } }
    dept_email { "MyString" }
    dept_fax { "MyString" }
    dept_phone { "MyString" }
    mail_domain { "MyString" }
    local_mail_domain { "MyString" }
    ml_base { "MyString" }
    ml_mail_domain { "MyString" }
    ml_domain { "MyString" }
    ml_admin { "MyString" }
    ml_admin_name { "MyString" }
    gmaps_url { "MyString" }
    moovit_url { "MyString" }
    waze_url { "MyString" }
    parent_url { "MyString" }
    gspreadsheet_key { "MyString" }
  end
end
