# frozen_string_literal: true

FactoryBot.define do
  factory :year_plan do
    name_i18n { { en: Faker::Company.catch_phrase, he: Faker::Company.catch_phrase } }
    degree { nil }
    remarks_i18n { { en: Faker::Company.catch_phrase, he: Faker::Company.catch_phrase } }
  end
end
