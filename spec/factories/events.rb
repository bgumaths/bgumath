# frozen_string_literal: true

FactoryBot.define do
  factory :event do
    title { Faker::Lorem.sentence }
    starts { Faker::Date.between(from: 5.years.ago, to: 5.years.from_now) }
    ends { Faker::Date.between(from: starts, to: starts + 6.months) }
    expires { Faker::Date.between(from: ends + 1.year, to: ends + 3.years) }
    location { Faker::Address.street_address }
    web { Faker::Internet.url }
    online { Faker::Internet.url }
    description { Faker::Lorem.paragraph }
    centre { [false, true].sample }

    trait :past do
      ends { Faker::Date.between(from: 5.years.ago, to: 7.months.ago) }
      starts { Faker::Date.between(from: ends - 6.months, to: ends) }
      expires {
        Faker::Date.between(from: 1.month.from_now, to: 5.years.from_now)
      }
    end

    trait :recent do
      ends { Faker::Date.between(from: 6.months.ago, to: 1.day.ago) }
      starts { Faker::Date.between(from: ends - 6.months, to: ends) }
    end

    trait :future do
      starts {
        Faker::Date.between(from: 1.day.from_now, to: 5.years.from_now)
      }
      expires {
        Faker::Date.between(from: 1.year.from_now, to: 5.years.from_now)
      }
    end

    trait :expired do
      starts { Faker::Date.between(from: 5.years.ago, to: 3.years.ago) }
      expires { Faker::Date.between(from: 2.years.ago, to: 1.month.ago) }
    end
  end
end
