# frozen_string_literal: true

FactoryBot.define do
  factory :old_user do
    first_i18n do
      { en: Faker::Name.first_name,
        he: %w[אברהם יצחק יעקב שרה רבקה רחל לאה].sample, }
    end
    last_i18n { { en: Faker::Name.last_name, he: %w[כהן לוי].sample } }
    sequence(:email) do |n|
      id = last[0..5] + first[0] + n.to_s
      [id, Faker::Internet.email(name: id)].sample
    end
    status { %w[Admin Regular Kamea External Virtual Adjunct Postdoc Msc Phd].sample }
    phd_year { Faker::Date.backward(days: 30.years).year }
    phd_from { Faker::University.name }
    interests { Faker::Lorem.paragraph }
    rank { %w[Prof Dr Mr Ms].sample }
    webpage { Faker::Internet.url }
    state { :_active }
  end
end
