# frozen_string_literal: true

FactoryBot.define do
  factory :model_table do
    name { 'MyString' }
  end
end
