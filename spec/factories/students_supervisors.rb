FactoryBot.define do
  factory :students_supervisor do
    association :supervisor, :simple
    association :student, :simple
  end
end
