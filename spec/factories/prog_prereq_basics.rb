# frozen_string_literal: true

FactoryBot.define do
  factory :prog_prereq_basic do
    minc { 1 }
    maxc { 3 }
    minp { 2.5 }
    maxp { 5 }
    course_list { nil }
  end
end
