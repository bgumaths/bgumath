# frozen_string_literal: true
#require 'support/term_methods'

FactoryBot.define do
  factory :course do
    association :generic_course, course_count: 0
    slug {}
    title_i18n { { en: Faker::Lorem.sentence, he: %w[עקומים הצגות].sample } }
    content_i18n { { en: Faker::Lorem.paragraph, he: Faker::Lorem.paragraph } }
    abstract_i18n { { en: Faker::Movies::Lebowski.quote,
                      he: Faker::Movies::Lebowski.quote }}

    association :lecturer, strategy: :build
    hours { Faker::Lorem.sentence }
    web { Faker::Internet.url }
    # we cannot create terms randomly, since they may overlap, and we get 
    # many validation errors. Instead, build one term in the test, and use 
    # it for all courses
    association :term, :simple, strategy: :build
    association :student_rep, factory: :issues_student_rep, strategy: :build

    after(:build) do |c,e|
      c.student_rep.courses.push(c) if c.student_rep
    end

    trait :current do
      transient do
        date { default_term_date }
      end

      term { default_term(date) } 
    end

    trait :simple do
      current
    end

    trait :grad do
      generic_course { create(:generic_course, :grad, course_count: 0) }
    end

    trait :undergrad do
      generic_course { create(:generic_course, :undergrad, course_count: 0) }
    end
  end
end

