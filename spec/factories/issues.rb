# frozen_string_literal: true

FactoryBot.define do
  factory :issue do
    association :course, :simple
    title { Faker::Lorem.sentence }
    content { Faker::Lorem.paragraph }
    correspondence { Faker::Lorem.paragraph }
    # this is not done if issue is not saved
    after(:build) do |i,e|
      i.reporter = i.course.student_rep
    end

    trait :replied do
      transient do
        reply { Faker::Lorem.paragraph }
        replier { build :regular, :responder, :simple }
      end

      after(:create) do |i,e|
        create(:issue_response, issue: i,
               content: e.reply, creator: e.replier)
      end
    end

    trait :published do
      transient do
        reply { Faker::Lorem.paragraph }
        replier { build :regular, :responder, :simple }
        published_at { Time.zone.now }
      end

      after(:create) do |i,e|
        create(:issue_response, :published, issue: i, content: e.reply,
               creator: e.replier, published_at: e.published_at)
      end
    end
      
  end
end

