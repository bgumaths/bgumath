# frozen_string_literal: true

#require 'support/term_methods'

FactoryBot.define do
  factory :term do
    transient do
      today { default_term_date }
    end

    # normally don't set `starts` and `ends` directly, instead set `today`
    starts {
      raise "No term can include #{today}" if [7, 8].include?(today.month)
      # approx mid-term date of today's term
      ago = 2.months.ago(today)
      max_start = Date.new(ago.year, ago.month < 7 ? 4 : 11, 10)
      min_start = 6.weeks.ago(max_start)
      # include today
      min_start = today if min_start > today
      max_start = today if max_start > today
      overlaps = Term.ends_no_exams_after(min_start)
        .starts_before(max_start).take
      if overlaps
        min_start = overlaps.ends + 1.day
      end
      min_start = max_start if min_start > max_start

      res = Faker::Date.between(from: min_start, to: max_start)
      binding.pry if res.month == 7
      res
    }
    # avoid overlapping terms, if possible
    transient do
      next_existing { Term.next(starts)&.starts || starts + 100.years }
      min_ends { [Term.min_len.since(starts), today].max }
      max_ends { [next_existing - 1.day, Term.max_len.since(starts)].min }
    end
    ends { Faker::Date.between(from: min_ends, to: max_ends) }
    name_i18n { { en: nil, he: nil } }

    transient do
      course_count { [*0..10].sample }
    end

    trait :simple do
      course_count { 0 }
    end

    after(:create) do |u, e|
      create_list(:course, e.course_count, term: u)
    end

    trait :current do
    end

    transient do
      prev { build(:term) }
    end

    trait :following do
      today { prev.ends + (prev.max_break + 20).days }
    end

    trait :past do
      today {
        res = prev.starts - (prev.max_break + 40).days
        res -= 2.months if [7,8].include?(res.month)
        res
      }
    end

    trait :old do
      today { prev.starts - 1.year }
    end

    # the _physical_ year where the term start, i.e.,
    # `create(:term, starts: 2000)` will create the term fall2001
    transient do
      year { Date.current.year }
    end

    trait :fall do
      today { Date.new(year, 12, 1) }
    end

    trait :spring do
      today { Date.new(year, 5, 1) }
    end
  end
end

