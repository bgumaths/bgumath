FactoryBot.define do
  factory :course_lists_prog_prereq do
    course_list
    association :prereq, factory: :prog_prereq
  end
end
