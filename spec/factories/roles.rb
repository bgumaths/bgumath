# frozen_string_literal: true

FactoryBot.define do
  factory :role do
    title_i18n {
      { en: Faker::Lorem.sentence, he: %w[עקומים הצגות].sample }
    }
    description_i18n {
      { en: Faker::Lorem.paragraph, he: Faker::Lorem.paragraph }
    }
    visibility { %i[hidden internal open].sample }

    trait :for_seminar do 
      association :resource, :current, factory: :seminar
      visibility { :auto }
    end

    trait :for_course do
      association :resource, :current, factory: :course
      visibility { :auto }
    end

    factory :vice_role do
      name { 'Department vice head' }
      visibility { :open }
      email { Faker::Internet.email }
    end
      
  end
end

