# frozen_string_literal: true

FactoryBot.define do
  factory :ac_year do
    sequence(:year, 2020)
  end
end
