# frozen_string_literal: true

FactoryBot.define do
  factory :free_plan_item do
    title_i18n { { en: Faker::Company.catch_phrase, he: Faker::Company.catch_phrase } }
    minp { 1.5 }
    maxp { 1.5 }
    term_plan { nil }
  end
end
