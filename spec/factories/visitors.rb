# frozen_string_literal: true

FactoryBot.define do
  factory :visitor do
    email { Faker::Internet.email }
    first_i18n do
      { en: Faker::Name.first_name,
        he: %w[אברהם יצחק יעקב שרה רבקה רחל לאה].sample, }
    end
    last_i18n { { en: Faker::Name.last_name, he: %w[כהן לוי].sample } }

    office { Faker::Address.building_number }
    phone { Faker::PhoneNumber.subscriber_number }
    rank { %w[Prof Dr Mr Ms].sample }
    webpage { Faker::Internet.url }
    origin { Faker::University.name }
    # ensure not a past visitor
    starts { Faker::Date.between(from: 6.months.ago, to: 6.months.from_now) }
    ends { Faker::Date.between(from: starts, to: starts + 7.months) }
  end
end
