# frozen_string_literal: true

FactoryBot.define do
  factory :generic_course do
    name_i18n { { en: Faker::Lorem.sentence, he: Faker::Lorem.sentence } }
    description_i18n do
      {
        en: Faker::Lorem.paragraphs(number: 2).map { |x| "<p>#{x}</p>" }.join("\n"),
        he: 'תיאור הקורס בעברית',
      }
    end
    graduate { [false, true].sample }
    sequence(:shid, 1000) { |n| "201.#{graduate ? 2 : 1}.#{n}" }
    prereq { [false, true].sample }
    core { [false, true].sample }
    advanced { graduate ? true : [false, true].sample }
    lectures { [*1..6].sample }
    exercises { [*1..6].sample }
    spring { [false, true].sample }
    fall { [false, true].sample }
    slug {}
    association :aguda_rep, factory: :issues_aguda_rep, strategy: :build

    transient do
      # can't create many courses, since creating more than one course for 
      # the same gc in the same term doesn't validate
      course_count { [*0..1].sample }
      department_count { [*1..2].sample }
    end

    after(:create) do |u, e|
      create_list(:course, e.course_count, generic_course: u)
      create_list(:department, e.department_count, courses: [u])
    end

    trait :grad do
      graduate { true }
    end

    trait :undergrad do
      graduate { false }
    end

    trait :basic do
      graduate { false }
      advanced { false }
    end

    trait :simple do
      course_count { 0 }
      department_count { 0 }
    end
  end
end

