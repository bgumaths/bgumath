# frozen_string_literal: true

FactoryBot.define do
  factory :plan_item do
    generic_course { nil }
  end
end
