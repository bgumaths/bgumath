# frozen_string_literal: true

FactoryBot.define do
  factory :precise_prog_prereq_info do
    prereq { nil }
    course_list { nil }
  end
end
