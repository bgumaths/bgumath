# frozen_string_literal: true

require 'authlogic/test_case'
FactoryBot.define do
  factory :issues_user do
    sequence(:login) {|n| Faker::Name.first_name + n.to_s }
    password { Faker::Internet.password }
    fullname { Faker::Movies::Lebowski.character }
    mail_domain { nil }
    after(:build) { |u| u.password_confirmation = u.password }

    password_salt { Authlogic::Random.hex_token }
    persistence_token { Authlogic::Random.hex_token }
    after(:build) { |u|
      u.crypted_password = u.send(:crypto_provider).encrypt(
        u.password + u.password_salt) if u.password.present?
    }
    active { true }
    approved { true }
    confirmed { true }
    pass_changed { true }
    type { %w[IssuesStaff IssuesStudentRep IssuesAgudaRep].sample }

    factory :issues_student_rep, class: 'IssuesStudentRep' do
      type  { %w[IssuesStudentRep].sample }

      transient do
        course_count { 0 }
      end

      after(:create) do |u, e|
        create_list(:course, e.course_count, student_rep: u)
      end
    end

    factory :issues_aguda_rep, class: 'IssuesAgudaRep' do
      type  { %w[IssuesAgudaRep].sample }

      transient do
        course_count { 0 }
      end

      after(:create) do |u, e|
        create_list(:generic_course, e.course_count, aguda_rep: u,
                    course_count: 0)
      end
    end

    factory :issues_staff, class: 'IssuesStaff' do
      department
      type  { %w[IssuesStaff].sample }
      after(:build) do |u,e|
        u.department.people.push(u)
      end
    end
  end
end

