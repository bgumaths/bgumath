FactoryBot.define do
  factory :course_lists_course do
    course_list
    association :course, :simple, factory: :generic_course
  end
end
