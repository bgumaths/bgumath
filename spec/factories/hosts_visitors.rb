FactoryBot.define do
  factory :hosts_visitor do
    visitor
    association :host, :simple
  end
end
