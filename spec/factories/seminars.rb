# frozen_string_literal: true

FactoryBot.define do
  factory :seminar do
    name_i18n { Hash[I18n.available_locales
      .map {|x| [x.to_s, Faker::Lorem.sentence] }] }
    starts { Faker::Time.forward(days: 1, period: :day) }
    ends { starts + rand(30..150) }
    day { rand(0..6) }
    description { Faker::Lorem.paragraph }
    room { Faker::Address.street_address }
    online { Faker::Internet.url }
    list_id { Faker::Internet.slug }
    association :term, :simple
    transient do
      meeting_count { 0 }
    end

    trait :current do
      transient do
        date { default_term_date }
      end

      term { default_term(date) }
    end

    trait :simple do
      current
    end

    after(:create) do |u, e|
      create_list(:meeting, e.meeting_count, seminar: u)
    end
  end
end
