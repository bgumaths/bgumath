# frozen_string_literal: true

FactoryBot.define do
  factory :shnaton do
    ac_year
    preamble_i18n { { en: Faker::Lorem.paragraph, he: Faker::Lorem.paragraph } }
  end
end
