# frozen_string_literal: true

FactoryBot.define do
  factory :term_plan do
    year { '' }
    term { '' }
    remarks_i18n { { en: Faker::Company.catch_phrase, he: Faker::Company.catch_phrase } }
    degree_plan
  end
end
