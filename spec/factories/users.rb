# frozen_string_literal: true

FactoryBot.define do
  factory :user_base, class: 'User' do
    first_i18n do
      { en: Faker::Name.first_name,
        he: %w[אברהם יצחק יעקב שרה רבקה רחל לאה].sample, }
    end
    last_i18n { { en: Faker::Name.last_name, he: %w[כהן לוי].sample } }
    sequence(:email) do |n|
      last_i18n[:en][0..5] + first_i18n[:en][0] + n.to_s
    end
    office { Faker::Address.building_number }
    phd_year { Faker::Date.backward(days: 11000).year }
    phd_from { Faker::University.name }
    interests { Faker::Lorem.paragraph }
    phone { Faker::PhoneNumber.subscriber_number }
    rank { %w[Prof Dr Mr Ms].sample }
    webpage { Faker::Internet.url }
    state { :_active }
    hours { nil }
    orcid { nil }
    gs { nil }
    scopus { nil }
    arxiv { nil }
    mail_domain { nil }
    pure { nil }
    #centre { false }

    trait :deceased do
      deceased { Faker::Date.backward(days: 1.year) }
    end

    trait :onleave do
      transient do
        leave_starts { Faker::Date.between(from: 5.years.ago, to: 5.years.from_now) }
        leave_ends do
          Faker::Date.between(from: leave_starts + 3.months, to: leave_starts + 2.years)
        end
      end
      after(:create) do |u, e|
        create_list(:leave, 1, user: u, starts: e.leave_starts, ends: e.leave_ends)
        u.reload
      end
    end

    trait :password do
      password { Faker::Internet.password }
      password_salt { Authlogic::Random.hex_token }
      persistence_token { Authlogic::Random.hex_token }
      #single_access_token { Authlogic::Random.friendly_token }
      #perishable_token { Authlogic::Random.friendly_token }
      after(:build) do |u|
        u.password_digest = u.send(:crypto_provider).encrypt(
          u.password + u.password_salt
        )
      end
    end

    factory :virtual, class: 'Virtual' do
      trait :admin do
        password
        after(:create) { |u| u.add_role :Admin }
      end
    end

    factory :admin, class: 'Admin' do
      trait :depadmin do
        after(:build) { |u| u.add_role 'Department Administrator' }
      end

      trait :secretary do
        after(:build) { |u| u.add_role 'Secretary' }
      end

      trait :student_coord do
        after(:build) { |u| u.add_role 'Student Coordinator' }
      end
    end

    factory :academic_base do
      hours { Faker::Lorem.sentence }

      transient do
        course_count { [*0..3].sample }
        research_group_count { [*0..3].sample }
      end
      after(:create) do |u, e|
        create_list(:course, e.course_count, lecturer: u,
                    term: Term.default || create(:term, :simple))
        create_list(:research_group, e.research_group_count, members: [u])
      end

      factory :adjunct, class: 'Adjunct' do
      end

      factory :external, class: 'External' do
      end

      factory :member_base, class: 'Member' do
        rank { %w[Dr Prof].sample }
        transient do
          student_count { [*0..4].sample }
          postdoc_count { [*0..4].sample }
        end

        trait :simple do
          student_count { 0 }
          postdoc_count { 0 }
          course_count { 0 }
        end

        after(:create) do |u, e|
          # don't create courses for students
          create_list(:student, e.student_count, :simple, supervisors: [u])
          create_list(:postdoc, e.postdoc_count, :simple, hosts: [u])
        end

        trait :emeritus do
          finish { Faker::Date.backward(days: 1.year) }
          hours { nil }
        end

        trait :future_retire do
          finish {
            Faker::Date.between(from: 1.year.from_now, to: 5.years.from_now)
          }
        end

        factory :regular, class: 'Regular' do
          trait :chair do
            after(:build) { |u| u.add_role :Chair }
          end

          trait :vice do
            after(:build) { |u| u.add_role 'Department vice head' }
          end

          trait :responder do
            after(:build) { |u| u.add_role 'Issues Responder' }
          end

          trait :center_dir do
            after(:build) { |u| u.add_role 'Center Director' }
          end

          trait :teaching_committee do
            after(:build) { |u| u.add_role 'Teaching Committee' }
          end
        end

        factory :kamea, class: 'Kamea' do
        end

        factory :member, aliases: %i[host supervisor coordinator],
                         class: 'Regular' do
          # create only regulars, so that members index test passes
          #initialize_with { [Regular, Kamea].sample.new }
        end
      end

      factory :student_base do
        transient do
          supervisor_count { [*1..2].sample }
        end

        trait :simple do
          supervisor_count { 0 }
          course_count { 0 }
        end

        after(:create) do |u, e|
          u.supervisors = create_list(:supervisor, e.supervisor_count, students: [u]) if u.supervisors.empty?
        end

        factory :msc, class: 'Msc' do
        end

        factory :phd, class: 'Phd' do
        end

        factory :student do
          initialize_with { [Msc, Phd].sample.new }
        end
      end

      factory :postdoc, class: 'Postdoc' do
        rank { 'Dr' }
        hours { nil }
        #centre { [false, true].sample }
        transient do
          host_count { [*1..3].sample }
        end

        trait :simple do
          host_count { 0 }
          course_count { 0 }
        end

        after(:create) do |u, e|
          create_list(:member, e.host_count, postdocs: [u]) if u.hosts.empty?
        end
      end

      factory :academic, aliases: [:lecturer] do
        trait :simple do
          course_count { 0 }
        end
        #External,
        initialize_with { [Regular, Kamea, Adjunct, Postdoc, Msc, Phd].sample.new }
      end
    end

    factory :user, aliases: [:creator] do
      trait :simple
      #External,
      initialize_with { [Admin, Regular, Kamea, Virtual, Adjunct, Postdoc, Msc, Phd].sample.new }
    end
  end
end
