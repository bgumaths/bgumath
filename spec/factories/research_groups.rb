# frozen_string_literal: true

FactoryBot.define do
  factory :research_group do
    name { Faker::ChuckNorris.fact }
    description { Faker::Lorem.paragraph }
    transient do
      members_num { 0 }
      seminars_num { 0 }
    end

    after(:create) do |group, e|
      create_list(:member, e.members_num, research_groups: [group])
      create_list(:seminar, e.seminars_num, research_groups: [group])
    end
  end
end
