FactoryBot.define do
  factory :research_groups_seminar do
    research_group
    association :seminar, :simple
  end
end
