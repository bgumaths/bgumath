# frozen_string_literal: true

FactoryBot.define do
  factory :course_list do
    name_i18n {  { en: Faker::Lorem.sentence, he: %w[עקומים הצגות].sample } }
    remarks_i18n {  { en: Faker::Lorem.paragraph, he: Faker::Lorem.paragraph } }
    shnaton
  end
end
