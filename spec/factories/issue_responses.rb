FactoryBot.define do
  factory :issue_response do
    issue
    association :creator, :responder, :simple, factory: :regular
    content { Faker::Lorem.paragraph }
    published_at { nil }

    trait :published do
      published_at { Time.zone.now }
    end
  end
end
