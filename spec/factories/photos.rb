FactoryBot.define do
  factory :photo do
    x { rand * 100 }
    y { rand * 100 }
    width { rand * 200 }
    height { rand * 200 }

    transient do
      photo_name { 'fruity.png' }
      photo_blob { ActiveStorage::Blob.create_and_upload!(
        io: File.open(
          Rails.root.join('spec', 'factories', 'images', photo_name)
        ),
        filename: photo_name,
      )}
    end

    initialize_with {
      new(image: photo_blob)
    }

  end
end

