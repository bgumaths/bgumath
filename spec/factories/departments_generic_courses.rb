FactoryBot.define do
  factory :departments_generic_course do
    association :course, :simple, factory: :generic_course
    department
  end
end
