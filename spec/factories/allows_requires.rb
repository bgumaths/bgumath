FactoryBot.define do
  factory :allows_require do
    association :require, :simple, factory: :generic_course
    association :allow, :simple, factory: :generic_course
  end
end
