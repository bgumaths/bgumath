FactoryBot.define do
  factory :replaced_by_replace do
    association :replaced_by, :simple, factory: :generic_course 
    association :replace, :simple, factory: :generic_course
  end
end
