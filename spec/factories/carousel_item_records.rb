FactoryBot.define do
  factory :carousel_item_record do
    association :item, factory: :resource_carousel_item, strategy: :build
    seminar_item

    trait :seminar_item do
      association :resource, :current, factory: :seminar, strategy: :build
    end

    trait :course_item do
      association :resource, :current, factory: :course, strategy: :build
    end
  end
end
