# frozen_string_literal: true

FactoryBot.define do
  factory :meeting do
    association :seminar, :current
    speaker { Faker::Name.name }
    title { Faker::ChuckNorris.fact }
    abstract { Faker::Lorem.paragraph }
    online { Faker::Internet.url }
    transient do
      sdate { seminar.try(:term).try(:starts) || Date.today }
    end
    date { Faker::Date.between(from: sdate, to: seminar.try(:term).try(:ends) || sdate + 4.months) }
    from { Faker::University.name }
  end
end
