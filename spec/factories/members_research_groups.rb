FactoryBot.define do
  factory :members_research_group do
    association :member, :simple
    research_group
  end
end
