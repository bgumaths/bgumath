FactoryBot.define do
  factory :carousel_item do
    activates { Date.current - 1.day }
    expires { Date.current + 5.days }
    interval { 5000 }
    order { 0 }

    factory :resource_carousel_item, class: 'ResourceCarouselItem' do
      transient do
        resource_count { [*1..3].sample }
      end
      for_seminars

      trait :for_seminars do
        after(:build) do |c, e|
          build_list(:carousel_item_record, e.resource_count,
                     :seminar_item, item: c)
        end
      end
    end

    factory :url_carousel_item, class: 'UrlCarouselItem' do
      url { Faker::Internet.url }
    end

    factory :image_carousel_item, class: 'ImageCarouselItem' do
      association :image, factory: :photo
    end

  end

end

