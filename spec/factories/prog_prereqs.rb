# frozen_string_literal: true

FactoryBot.define do
  factory :prog_prereq do
    name_i18n { { en: Faker::Lorem.sentence, he: Faker::Lorem.sentence } }
  end
end
