FactoryBot.define do
  factory :issues_issues_staff do
    issue
    association :staff, factory: :issues_staff
  end
end
