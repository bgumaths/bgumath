FactoryBot.define do
  factory :role_event_base, class: 'RoleEvent' do
    role
    user
    association :creator, factory: :user

    factory :role_added, class: 'RoleAdded' do
    end

    factory :role_removed, class: 'RoleRemoved' do
    end

    factory :role_event do
      initialize_with { [RoleAdded, RoleRemoved].sample.new }
    end

  end
end

