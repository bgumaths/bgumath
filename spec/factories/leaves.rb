# frozen_string_literal: true

FactoryBot.define do
  factory :leave, class: 'Leave' do
    user
    starts { Faker::Date.between(from: 5.years.ago, to: 5.years.from_now) }
    ends { Faker::Date.between(from: starts + 3.months, to: starts + 2.years) }
    note_i18n { { en: Faker::Lorem.sentence, he: 'בחופשה ללא תשלום' } }
  end
end
