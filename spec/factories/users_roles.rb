FactoryBot.define do
  factory :users_role do
    association :user, :simple
    role
  end
end
