# frozen_string_literal: true

FactoryBot.define do
  factory :department do
    name_i18n {
      { en: Faker::Company.catch_phrase, he: Faker::Company.catch_phrase }
    }
    bgu_name { Faker::Company.name }
    sequence :catalog_id, 100

    transient do
      course_count { 0 }
      people_count { 0 }
    end

    after(:create) do |d, e|
      create_list(:generic_course, e.course_count, :simple, departments: [d])
      create_list(:issues_staff, e.people_count, department: d)
    end
  end
end

