import Base from '@/models/Base'

export default class BaseEvent extends Base {
  static singular = 'base_event'
  static collection = 'base_events'
  static type = 'base_event'

  static {
    this.register()
  }

  /*
  online
  starts
  ends
*/

  get ostarts() {
    Object.defineProperty(this, 'ostarts', {
      value: new Date(this.starts),
    })
    return this.ostarts
  }

  get oends() {
    Object.defineProperty(this, 'oends', {
      value: new Date(this.ends),
    })
    return this.oends
  }

  after(date = new Date()) {
    return this.ostarts >= date
  }
  
  get upcoming() {
    return this.after()
  }

  endsAfter(date = new Date()) {
    return this.oends >= date
  }

  ongoing(date = new Date()) {
    return !this.after(date) && this.endsAfter(date)
  }
}
