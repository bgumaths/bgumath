import IssuesUser from '@/models/IssuesUser'

export default class IssuesAgudaRep extends IssuesUser {
  static singular = 'teaching_issues_user'
  static collection = 'teaching_issues_users'
  static type = 'issues_aguda_rep'

  static {
    this.register()
  }
}
