import Base from '@/models/Base'
import Term from '@/models/Term'
import Meeting from '@/models/Meeting'
import { hasMany } from '@/models/Base'

export default class Seminar extends Base {
  static singular = 'research_term_seminar'
  static collection = 'research_term_seminars'
  static type = 'seminar'

  static {
    this.register()
  }

  /*
  name_i18n
  description_i18n
  room
  day
  slug
  online
  term_id
  starts
  ends
*/

  static get relations() {
    return {
      term: Term,
      meetings: hasMany(Meeting),
    }
  }

  get name() {
    return this.name_i18n[this.$i18n.locale]
  }

  get description() {
    return this.description_i18n[this.$i18n.locale]
  }

  get ostarts() {
    Object.defineProperty(this, 'ostarts', {
      value: new Date(`1900-01-01T${this.starts}`),
    })
    return this.ostarts
  }

  get oends() {
    Object.defineProperty(this, 'oends', {
      value: new Date(`1900-01-01T${this.ends}`),
    })
    return this.oends
  }

  get weekday() {
    return new Date(2000, 1, this.day - 1).toLocaleString(this.$i18n.locale, {
      weekday: 'short',
    })
  }
}
