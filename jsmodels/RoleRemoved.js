import RoleEvent from '@/models/RoleEvent'

export default class RoleRemoved extends RoleEvent {
  static singular = 'admin_role_removed'
  static collection = 'admin_role_removeds'
  static type = 'role_removed'

  static {
    this.register()
  }
}
