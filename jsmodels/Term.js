import Base from '@/models/Base'

export default class Term extends Base {
  static singular = 'teaching_term'
  static collection = 'teaching_terms'
  static type = 'term'

  static {
    this.register()
  }
  static defaultId = 'current'
}
