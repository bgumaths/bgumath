import Base from '@/models/Base'

export default class AcYear extends Base {
  static singular = 'ac_year'
  static collection = 'ac_years'
  static type = 'ac_year'

  static {
    this.register()
  }
}
