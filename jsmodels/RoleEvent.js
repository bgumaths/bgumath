import Base from '@/models/Base'
import User from '@/models/User'
import Role from '@/models/Role'

export default class RoleEvent extends Base {
  static singular = 'admin_role_event'
  static collection = 'admin_role_events'
  static type = 'role_event'

  static {
    this.register()
  }

  /*
  created_at
*/

  static get relations() {
    return {
      creator: User,
      role: Role,
    }
  }
}
