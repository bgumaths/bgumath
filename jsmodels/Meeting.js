import BaseEvent from '@/models/BaseEvent'
import Seminar from '@/models/Seminar'

export default class Meeting extends BaseEvent {
  static singular = 'research_term_seminar_meeting'
  static collection = 'research_term_seminar_meetings'
  static type = 'meeting'

  static {
    this.register()
  }

  /*
  web
  abstract
  speaker
  from
  speaker_web
  location
  title
  announcement_i18n
  room
  date
  seminar_id
  term_id
*/

  static get relations() {
    return {
      seminar: Seminar,
    }
  }

  get announcement() {
    return this.announcement_i18n[this.$i18n.locale]
  }

  get odate() {
    Object.defineProperty(this, 'odate', {
      value: new Date(this.date),
    })
    return this.odate
  }

  get fullSpeaker() {
    let res = this.speaker_web
      ? `[${this.speaker}](${this.speaker_web})`
      : this.speaker
    if (this.from) res += ` (${this.from})`
    return res
  }
}
