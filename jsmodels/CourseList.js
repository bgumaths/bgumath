import Base from '@/models/Base'

export default class CourseList extends Base {
  static singular = 'teaching_ac_year_shnaton_course_list'
  static collection = 'teaching_ac_year_shnaton_course_lists'
  static type = 'course_list'

  static {
    this.register()
  }
}
