import User from '@/models/User'

export default class Admin extends User {
  static singular = 'people_user'
  static collection = 'people_users'
  static type = 'user'

  static {
    this.register()
  }
}
