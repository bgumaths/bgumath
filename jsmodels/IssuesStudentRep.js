import IssuesUser from '@/models/IssuesUser'

export default class IssuesStudentRep extends IssuesUser {
  static singular = 'teaching_issues_user'
  static collection = 'teaching_issues_users'
  static type = 'issues_student_rep'

  static {
    this.register()
  }
}
