import Academic from '@/models/Academic'
import Member from '@/models/Member'
import { hasMany } from '@/models/Base'

export default class Student extends Academic {
  static singular = 'people_user'
  static collection = 'people_users'
  static type = 'user'

  static {
    this.register()
  }

  static get relations() {
    return {
      supervisors: hasMany(Member),
    }
  }
}
