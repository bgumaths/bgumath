import Base from '@/models/Base'
import Academic from '@/models/Academic'
import Seminar from '@/models/Seminar'
import { hasMany } from '@/models/Base'

export default class ResearchGroup extends Base {
  static singular = 'research_research_group'
  static collection = 'research_research_groups'
  static type = 'research_group'

  static {
    this.register()
  }

  /*
  name
  description
  member_ids
  seminar_ids
*/

  static get relations() {
    return {
      members: hasMany(Academic),
      seminars: hasMany(Seminar),
    }
  }
}
