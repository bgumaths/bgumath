import Base from '@/models/Base'
import Department from '@/models/Department'
import { hasMany } from '@/models/Base'

export default class GenericCourse extends Base {
  static singular = 'teaching_generic_course'
  static collection = 'teaching_generic_courses'
  static type = 'generic_course'

  static {
    this.register()
  }

  /*
  shid
  description_i18n
  shnaton_url
  level
*/

  static get relations() {
    return {
      departments: hasMany(Department),
    }
  }

  get description() {
    return this.description_i18n[this.$i18n.locale]
  }
}
