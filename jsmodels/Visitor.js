import BaseEvent from '@/models/BaseEvent'
import Member from '@/models/Member'
import { hasMany } from '@/models/Base'

export default class Visitor extends BaseEvent {
  static singular = 'people_visitor'
  static collection = 'people_visitors'
  static type = 'visitor'

  static {
    this.register()
  }

  /*
  first_i18n
  last_i18n
  rank
  webpage
  origin
  starts
  ends
*/

  static get relations() {
    return {
      hosts: hasMany(Member),
    }
  }

  get first() {
    return this.first_i18n[this.$i18n.locale]
  }

  get last() {
    return this.last_i18n[this.$i18n.locale]
  }

  get ostarts() {
    Object.defineProperty(this, 'ostarts', {
      value: new Date(this.starts),
    })
    return this.ostarts
  }

  get oends() {
    Object.defineProperty(this, 'oends', {
      value: new Date(this.ends),
    })
    return this.oends
  }

  get duration() {
    const fmt = new Intl.DateTimeFormat(this.$i18n.locale, {
      year: '2-digit',
      month: 'short',
      day: 'numeric',
    })
    return fmt.formatRange(this.ostarts, this.oends)
  }
}
