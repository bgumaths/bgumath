import Academic from '@/models/Academic'
import Student from '@/models/Student'
import Postdoc from '@/models/Postdoc'
import Visitor from '@/models/Visitor'
import { hasMany } from '@/models/Base'

export default class Member extends Academic {
  static singular = 'people_user'
  static collection = 'people_users'
  static type = 'user'

  static {
    this.register()
  }

  /*
  kamea
  postdoc_ids
  student_ids
  visitor_ids
*/

  static get relations() {
    return {
      students: hasMany(Student),
      postdocs: hasMany(Postdoc),
      visitors: hasMany(Visitor),
    }
  }
}
