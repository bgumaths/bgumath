import Base from '@/models/Base'

export default class IssuesUser extends Base {
  static singular = 'teaching_issues_user'
  static collection = 'teaching_issues_users'
  static type = 'issues_user'

  static {
    this.register()
  }
}
