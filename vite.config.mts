import { defineConfig } from 'vite'
import RubyPlugin from 'vite-plugin-ruby'
import FullReload from 'vite-plugin-full-reload'
import vue from '@vitejs/plugin-vue'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'

export default defineConfig({
  build: {
    minify: true,
    sourcemap: false,
    //sourcemap: true,
    rollupOptions: {
      maxParallelFileOps: 1,
      output: {
        manualChunks: (id) => {
          if (id.includes("node_modules")) {
            return "vendor";
          }
        },
        sourcemapIgnoreList: (relativeSourcePath) => {
          return relativeSourcePath.includes("node_modules");
        },
      },
      input: {
        application: 'app/javascript/entrypoints/application.js',
      },
    },
  },
  server: {
    fs: {
      strict: false,
    },
  },
  optimizeDeps: {
    include: ['@inertiajs/vue3'],
  },
  plugins: [
    RubyPlugin(),
    vue({
      template: {
        transformAssetUrls,
        compilerOptions: {
          isCustomElement: (tag) => false,
        },
      }
    }),
    quasar({
      sassVariables: true, // 'app/javascript/style/quasar-variables.scss',
      autoImportComponentCase: 'combined',
    }),
    FullReload(['config/routes.rb', 'app/views/**/*']),
  ],
})
