#### About the center

The Center for Advanced Studies in Mathematics was established at Ben-Gurion 
University of the Negev (BGU) in March 2001, thanks to the generous donations 
of Mrs. Lis Gaines and the initiative and support of the late [Prof. Beno 
Eckmann](eckmann). Since then, we have been fortunate to receive generous 
support from several additional donors: _Mr. Martin Blackman and the Skirball 
Foundation_,
_Dr. Joseph Friedman_, _Mrs. Edy and Mr. Sol Freedman_, and the late
_Mrs. Noriko Sakurai_ and her husband _Prof. Daniel Sternheimer_.

The Center promotes research activity in diverse areas of mathematics and in 
various applications. The Center supports the whole "food chain" of 
mathematical activity, from a [special program for gifted highschool 
students](/community/school) to post-doctoral fellows and visiting 
researchers, and it sponsors conferences, workshops and lecture series.  The 
Center also awards the [Friedman Prize for outstanding graduate 
students](friedman).

The International Advisory Committee for the Center is composed of 
world-famous mathematicians from Israel and abroad, among them three winners 
of the Fields medal, the Mathematics equivalent of the Nobel Prize, two 
winners of the prestigious Wolf Prize in Mathematics, three winners of the 
Israel Prize in Mathematics and members of the Israel Academy of Science.

#### Center staff

##### Director

[Emeritus Prof Miriam Cohen](/~mia)

##### Advisory Committee

- V. Drinfeld, University of Chicago
- G. Faltings, MPI Bonn
- H. Furstenberg, The Hebrew University
- M. Kontsevich, IHES
- I. Kra, SUNY Stony Brook
- P. Lax, Courant Institute
- S. Shelah, The Hebrew University

##### In Memoriam

- [Prof. Beno Eckmann](eckmann)
- Prof. Joram Lindenstrauss

