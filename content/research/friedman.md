#### The Friedman Prize

The [Center for Advanced Studies in Mathematics](/research/center) and the 
[Department of Mathematics at BGU](/) award the _Friedman prize_ to graduate 
students with outstanding thesis work.

Dr. Yossi Friedman, son of Mina and the late Asher Friedman, was born and 
educated in Beer Sheva. He started his studies at the Department of 
Mathematics and Computer Science (at the time a single department) at Ben 
Gurion University as a young school boy (see below his description of the 
    experience) where he received his B.Sc. degree with high honours.  He 
then went on to receive his Ph.D. degree in Computer Science at Stanford 
University in California. Upon graduation he was approached by investment 
companies and since the mid 1990's he has been working in this capacity. 
He is now a partner in Millenium partners, a well-known New York 
investment company.

Dr. Friedman has contributed to the Center of Advanced Studies in Mathematics 
at Ben Gurion University, and part of his donation is directed towards an 
annual prize for outstanding graduate students in the Mathematics department.

Here are Dr. Friedman's words at the Friedman prize ceremony of April 30, 
2007:

<blockquote markdown="1">

The expression "Girsa Dyankuta" comes from Aramaic, and literally means 
the things that a person learned in his childhood.  The deeper meaning of 
the expression is that the knowledge, wisdom, and understanding that a 
person absorbs as a child are deeply impressed on his soul and accompany 
him for life. Ben-Gurion University is my "Girsa Dyankuta".  When I was 
born in Beer-Sheva, at the Soroka Medical Center, across from the 
hospital was a large sandy lot peppered with thorns. At that time, 
Ben-Gurion University was scattered around several locations, and only 
several years later were the first buildings erected on the site where 
the campus is today.

When I was in the fourth grade, about nine or ten years of age, I 
underwent achievement tests, along with many of the schoolchildren of 
Beer-Sheva, and my parents received a letter stating that, as an 
"above-average" pupil, I was invited to participate in special enrichment 
courses for schoolchildren at the university.  I knew these courses were 
worthwhile, because my sister, who is four years older than I, had been 
invited to participate in them several years earlier and had been very 
excited about them. And, like everything else that my sister did, I 
wanted to do the same. She was interested in the life sciences. I was 
afraid of blood, so I chose the computer course. At the time this course 
seemed futuristic and fascinating, like Jules Verne's science fiction.

When I arrived at the course, in the computation center, which 
contained only one computer that required special climactic 
conditions, it seemed even more fantastic than science fiction. We, 
the pupils in the course, ran around the center, mingling with the 
university students, not just on the days of the course, but every 
day. There was ten-year-old Dror, who came every day by himself from 
the town of Kiryat Gat on the bus, and later became a professor at 
Harvard. There was Victor, the new immigrant from the Soviet Union, and 
me, and other "computer freaks" like us.

The university loved us, the little "freaks", and allowed us to come 
and go as we pleased. It gave us computer time and access to 
lecturers and researchers, and helped us to develop intellectual 
curiosity and to look for solutions. So much so, that on several 
occasions my Dad had to come to the computation center to get me at 
midnight, wearing a sweater over his pajamas, because I'd lost track 
of time.

When I began high school, it was the Department of Mathematics and 
Computer Sciences that offered me a chance to take courses as a 
regular university student while I was still a high school pupil. 
There were teachers at my high school who were not pleased with this, 
and who saw it as a personal insult to them, and the university came 
through by allowing me to be absent from lectures that overlapped 
with these teachers' lessons.

And so it happened that shortly after I matriculated from high 
school, I received my bachelor's degree in Mathematics and Computer 
Sciences. With the recommendation of my lecturers at BGU, I was 
accepted into a PhD program at Stanford University, and with the "Girsa 
Dyankuta" from BGU, I became a partner in an investment firm in New York. 
I live in a beautiful apartment and enjoy a good life. But I always 
remember the "incubator" of my ambitions and their fulfillmen --- where 
they believed in me and treated me like an intelligent adult when I was 
still a small child. That is my "Girsa Dyankuta", and probably that of 
many others as well.

<footer class="blockquote-footer">
<cite>
Dr. Friedman, Friedman prize ceremony, Ben-Gurion math department,
April 30 2007
</cite>
</footer>

</blockquote>

