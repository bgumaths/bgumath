* Class request form ([pdf file](/files/class-request.pdf) and [MS word 
  file](/files/class-request.doc))
* [Statement regrading primary residence](/files/residence.pdf)
* [FAQ regarding guests](/files/guests.pdf)
* [Exam grades submission guidelines](/files/exams.pdf)
* [Course grades statistics](/files/grades_stat.pdf)
