* [Research and Development 
  Authority](http://in.bgu.ac.il/en/osr/Pages/default.aspx)
* [Personnel Department Service login](http://bguvm.bgu.ac.il/knisa.html)
* [Travel form](https://bgufin.bgu.ac.il/xTafnit/default.csp) (from within 
  the University or via a [secure connection](/internal/support))
* [University forms](http://in.bgu.ac.il/osh/Pages/UniForms.aspx)
* [University phone 
  book](http://w05.bgu.ac.il/nihulitweb/phone_books/view/index.asp?phone_book_id=1016)
* [Academic Calendars](http://in.bgu.ac.il/acadsec/Pages/acadcalendar.aspx)

