* [Class enrollment and mass mailing](http://gezer.bgu.ac.il/mass/login.php)
* [Exam Schedule for Lecturers and Teaching 
  Assistants](https://bgu4u.bgu.ac.il/pls/apex/f?p=114:101)
* [Tadpisim](http://www.bgu.ac.il/printouts/) - course pages and archive of 
  old exams
* [Academic Calendars](http://in.bgu.ac.il/acadsec/Pages/acadcalander.aspx)
* [Academic Video Services](http://karish.bgu.ac.il/video/)
* [High Learn](http://hl2.bgu.ac.il/NewLoginFrames.asp)
* [Exams safe](https://sdb.bgu.ac.il/Exams/frmLogin.aspx)
