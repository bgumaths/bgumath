* [Research and Development 
  Authority](http://in.bgu.ac.il/en/osr/Pages/default.aspx)
* [Israel Science Foundation](http://www.isf.org.il/) (ISF)
* United States-Israel [Binational Science Foundation](http://www.bsf.org.il) 
  (BSF)
* [German-Israel Foundation](http://www.gif.org.il/) (GIF)
* [UK Research Office](http://www.ukro.ac.uk), with emphasis on their 
  information service by subscription.

