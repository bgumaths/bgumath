* [MathSciNet](http://www.ams.org/mathscinet/search) (in case of access 
  problems, you may need to
  [reconfigure the proxy](/internal/support))
* [Search in the Zentralblatt fuer Mathematik](http://www.emis.de/ZMATH/)
* [Web of Science](http://scientific.thomson.com/products/wos/) electronic 
  [citation index](http://isiknowledge.com/wos).
* [SCOPUS](http://www.scopus.com/home.url) citation analysis service.
* [AMS combined membership list](http://www.ams.org/cml/)
* [List of journal webpages](https://www.cs.bgu.ac.il/~efrat/journals.html) 
  compiled by [Ido Efrat](/~efrat).

