*  [Aranne Library](http://www.bgu.ac.il/aranne/)
   - [Library Catalogue](//primo.bgu.ac.il/discovery/search?vid=972BGU_INST:972BGU)
   - [Inter-library loan](//primo.bgu.ac.il/discovery/blankIll?vid=972BGU_INST:972BGU) of books and articles ([Instructions](//in.bgu.ac.il/en/aranne/pages/events/interLibraryLoan.aspx)) 
   - [Guides](//libguides.bgu.ac.il/central/research), including
     [remote access](//in.bgu.ac.il/en/aranne/Pages/ezproxy.aspx)
   - For problems with journal access: [yaatz](mailto:yaatz@bgu.ac.il)

* [Israel Union list](http://uli.nli.org.il) - catalog of books in all Israeli institutions.
* [Israel Union Serials](http://libnet.ac.il/~libnet/uls/) - catalog of serials in all Israeli institutions.

