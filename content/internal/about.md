#### General usage

Hopefully everything is self evident. One less noticeable feature is that 
some information is available in alternative forms, as follows:

- A calendar symbol ({%- glyphicon calendar -%}) near a title or an item 
  means that the information is available as an 
  [ical](https://en.wikipedia.org/wiki/ICalendar) feed, which can be used by 
  calendar applications (e.g., Google calendars). For example, each seminar 
  has such a calendar.

- A feed symbol ({%- glyphicon rss -%}) similarly provides the information as 
  an [atom](https://en.wikipedia.org/wiki/Atom_(standard)) feed, usable by 
  news readers, such as Feedly.

- A printer icon ({%- glyphicon file-pdf -%}) provides a pdf version. This is 
  created from a LaTeX version, which is available as well, on the 
  corresponding html page.

- API: All resources are available via an API, following the 
  [json:api](https://jsonapi.org/) standard, under the `/api/v1` namespace. A 
  simpler `json` form and an `xml` version are available by adding the 
  `.json` or `.xml` extensions. This is useful for interaction with other 
  pages, and for shell scripts. For example, I have the following shell 
  function to find someone's contact details:

````.sh
  ph() {
    curl -s -g "https://www.math.bgu.ac.il/api/v1/users.json?fields[users]=email,first,last,phone,office&filter[email][match]=${1}&filter[|][first][match]=${1}&filter[|][last][match]=${1}" | json_xs
  }
````

  This will match the email, first name and last name against the given 
  argument (regexp) and print a simplified json of the matching entries 
  (which is pretified by `json_xs`)

The rest of this page is intended for members of the department.

#### Editing content

A central principle for the website is to make it easy for any member of the 
department to modify any content related to them, so that updates are quick 
and accurate.  To achieve this, one should first [Login](#loginbox), using 
the department username and password.

Once this is done, certain parts will slightly alter their appearance, to 
show that they are modifiable. For example, the member's name in the 
appropriate page (e.g., on the [Regular Faculty]({%- url_for members -%}) 
page, if the user belongs there) will have an `edit` button by it, which 
looks like this: {% glyphicon edit %} or like this: {% badge Edit %}. 
Clicking it will bring up an html form where various details (e.g., research 
interests) can be modified.

Additionally, some items will appear with a <span 
class="editable-click">dashed underline</span>, which means they can be 
edited directly, without a form. Examples of these include the office and 
phone numbers that appear in the table.

Following are the main details that can be modified.

* _Personal details_. These include contact details, research interests, 
  students/postdocs/supervisors, etc.

* _Events_. Click the `New Event` button at the bottom of the [Events]({%- 
  url_for events -%}) to add a new event (e.g., a conference).

* _Courses_. If you are teaching an advanced course, an entry for it should 
    be created on the [Courses page]({% url_for cterm %}) for that term.  See 
    [Courses](#courses) below for details.

* _Seminars_. If you wish to run a seminar, you may create one on the 
    [Seminars page]({% url_for seminars %}). You may then edit its details, 
    add meetings, etc. See [Seminars](#seminars) below for details.

* The [Visitors' Wiki]({% url_for wiki %}). This is not really an 
  integral part of the department website, but is a useful resource for 
  visitors.

What elements of the site can be modified by a particular person depends on 
that person's roles in the department. If permissions seem wrong, please 
write to the {% full_email admin %}

##### Courses

There are two notions of a course: a "generic" course, which essentially 
corresponds to an entry in the course catalogue ("Shnaton"), and a regular 
course, which is an instance of a generic course that is actually being 
taught by a particular person on a particular term.

The latter kind should normally be created by the person teaching the course. 
The easiest way to do this is by finding a similar course in a previous term, 
and clicking the `copy` icon next to it. The copy icon looks like this: {% 
glyphicon clone %}, or like this: {% badge Clone %}. Pressing it will create 
a new course form, with default values taken from the previously taught 
course. Another method is to visit the
[generic courses]({%- url_for generic_courses -%}) page, and click `new` on 
the relevant generic course. This will use the defaults of the generic 
course. It is also possible to start from scratch by clicking `new course` on 
the [Courses]({%- url_for cterm %}) page.

The edit page for a course features an `update` button attached to its 
generic course, which can be used to fill the blank entries from the 
corresponding entries in generic course. This will also fetch information 
from the course catalogue, if available. Likewise, the generic course has an 
`update` button, which fills the blank fields with entries from the course 
catalogue.

##### Seminars

To start a new seminar, use the `New Seminar` button on the bottom of the 
[Seminars page]({%- url_for seminars -%}). This will open a page to fill in 
the details. By default, the creator will be the only admin for the seminar, 
i.e., the only one allowed to add meetings. However, every admin can make 
additional users admins.

**Note:** If the seminar name is filled only in one language, it will always 
be displayed in that language, and will not be translated.

The page for each seminar contains the list of talks. If the user is an admin 
for this seminar, there will be a `New Meeting` button at the bottom to 
schedule meetings. As usual, each meeting can be modified by clicking the 
`edit` icon by its title.

Please note that seminars should be re-created each term. As with courses, 
the easiest way to do this is to find the seminar in a previous term, and 
click the `clone` button {%- badge clone -%}.

An efficient way to run the mailing list for the seminar is via the [mailing 
list server]({%- setting ml_domain -%}). If the seminar has a mailing list on 
this server, and the name of the list is detailed in the seminar parameters, 
there will be a link to the page, and the address will be advertised in the 
pdf announcements. The seminar admins will have a button on the meeting's 
page that allows them to (edit and) send an announcement to the mailing list.

##### Text formatting

Most of the text boxes that appear when editing components allow basic 
formatting, using a [variant of 
markdown](http://kramdown.gettalong.org/syntax.html). In a nutshell, this 
allows constructs such as `**bold**` for **bold**, `*italic*` for *italic*, 
`` `code` `` for `code`, etc. In addition, it is possible to typeset basic 
math using standard LaTeX notation, delimited, as usual, by dollar signs. 
Please note that general latex is not supported, only math. Also, please 
avoid using html markup, since most of the markup needs also to be translated 
to LaTeX.

##### Source code

The source code for the site is available with:

~~~.sh
git clone https://www.math.bgu.ac.il/git/wwwmath/bgumath.git
~~~

Contributions are welcome!

<div class="row-flex justify-content-center my-3" markdown="1">
<div class="bordered text-center" markdown="1">
**For additional information, questions and comments, please write to the
{%- full_email admin -%}.**

</div>
</div>

