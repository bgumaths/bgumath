Make sure your laptop's MAC address is registered with the CS lab or that 
your machine is registered with the university's wireless network (visit the 
[CS 
Lab](http://in.bgu.ac.il/en/natural_science/cs/Pages/Computing/Help.aspx)).

The following instructions are offered by the CS Lab. If they fail, please 
[file a report](http://help.cs.bgu.ac.il/helpdesk).

##### Linux
Make sure that `cups` is installed. There are two methods:

- Automatic printer discovery
   1. Add the following lines to `/etc/hosts`:
      
      ~~~~
      132.72.44.185 vmprtsrv vmprtsrv.local
      132.72.44.56 vmsmb vmsmb.local
      ~~~~
   2. Install `avahi-daemon` and `cups-browsed` (part of the `cups-filters` 
      package)
   3. Add the line
      
      ~~~~
      BrowsePoll vmprtsrv
      ~~~~
      to `/etc/cups/cups-browsed.conf`.
   4. Start `avahi-daemon`, and (re)start `cups` and `cups-browsed` services
   5. Use the printers `math{1,2}{s,d}` for 1st/2nd floor single/double sided 
      printing
- Manual configuration

   If the above fails, stop the `cups` service, and replace 
   `/etc/cups/printers.conf` with a file similar to the following:

       <DefaultPrinter lpz1d>
       AuthInfoRequired none
       Info HP-603 double side
       Location Math building 1st floor room 118
       MakeModel HP LaserJet 600 M601 M602 M603 Postscript (recommended)
       DeviceURI socket://132.72.145.99
       Type 8425684
       </DefaultPrinter>

       <Printer lpz2d>
       AuthInfoRequired none
       Info HP-603 double side
       Location Math building 2nd floor room 219
       MakeModel HP LaserJet 600 M601 M602 M603 Postscript (recommended)
       DeviceURI socket://132.72.145.73
       Type 8425684
       </Printer>

   (Modify which one is the default to your convenience). Restart `cups`

##### Windows
Create a "standard IP port printer".  Use the queue name from the previous 
item as the port name.

