The problem of using LaTeX in Hebrew is divided into two parts: typing the 
LaTeX source, and typesetting it using a LaTeX variant.

##### Typing #####

For typing, it is possible to use a standalone editor, or an integrated 
environment. Examples of the latter include 
[TeXworks](http://www.tug.org/texworks), which comes with the standard TeX 
distributions (e.g., `MikTeX` and `TeXLive`) and `TeXnicCenter`. The 
essential requirement is support for bidirectional (bidi) editing, preferably 
with some LaTeX syntax recognition. For standalone editors that run in a 
terminal (e.g. [vim](http://www.vim.org) or 
[emacs](https://www.gnu.org/s/emacs)), bidi support might be provided by the 
terminal (One such option on Unix-type systems is 
[urxvt](http://software.schmorp.de/pkg/rxvt-unicode.html), [contact 
me](/~kamenskm) for details).

In any case, it is essential that the text is saved using the `UTF-8` 
encoding (this should be the default in any post-2005 editor).

##### Processing #####

Documents should be processed with the [XeTeX](http://xetex.sf.net) variant 
of TeX. One advantage is that it uses the system fonts, rather than special 
fonts that come with TeX. XeTeX (and XeLaTeX) are provided with the standard 
distributions. Multilanguage support is provided by the 
[polyglossia](https://github.com/reutenauer/polyglossia) package (a 
replacement for `babel`), which should be loaded in the preamble. Polyglossia 
uses [fontspec](https://github.com/wspr/fontspec) to select the fonts, and 
should be configured to select the correct font. This depends on the OS and 
on the fonts installed, please see the `fontspec` documentation for details. 
One way of loading the Hebrew support is to input the file `hebrew.tex` 
available in the [bgu-utils](/git/wwwmath/bgu-utils/tree/master/tex/hebrew) 
repository, which also contains some examples (again, this file may need to 
be modified to use the correct fonts).

Once the document is ready, it should be processed with xelatex: `xelatex 
stuff.tex`. An integrated tex environment might have a button for that.

##### Alternatives #####

An alternative is to use [LyX](http://www.lyx.org/), a "wysiwyg" front-end to 
(xe)latex, which is rumoured to support bidi. Please check the web for 
[instructions](https://wiki.lyx.org/LyX/XeTeX).

