* [How to Access E-Journals](/internal/journalhelp)
* Accessing the University computers from outside is possible via 
  [vpn](https://in.bgu.ac.il/computing/Pages/vpn-service.aspx) ([version in 
  English](https://in.bgu.ac.il/en/computing/Pages/comm_from_home.aspx)). 
  [Alternative instructions for Ubuntu](/~amyekut/VPN-ubuntu.pdf) by [Amnon 
  Yekutieli](/~amyekut)

