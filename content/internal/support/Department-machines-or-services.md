* [Help FAQ](http://vmoracle3.cs.bgu.ac.il/wiki) (CS wiki, only from within 
  the University)
* Contact the CS Lab: [web form](http://help.cs.bgu.ac.il/helpdesk)  or [email](mailto:help@cs.bgu.ac.il).
* [CS Lab Staff](http://in.bgu.ac.il/en/natural_science/cs/Pages/People/CSStaffDBTech.aspx)
* Some files and utilities related to the department are available for 
  download as  an [archive](/git/wwwmath/bgu-utils/repository/archive.tar.gz) 
  or via git with
    `git clone 'https://www.math.bgu.ac.il/git/wwwmath/bgu-utils'`
  Currently this includes a LaTeX class file for the BGU cv format, along 
  with some templates.
* [Instructions](/files/copymachine.pdf) for operating the copying machine.
