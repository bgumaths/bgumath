Candidates should have completed recently (not more than 4 years ago), or be 
about to complete, a Ph.D. in mathematics. Postdoctoral fellows are expected 
to cooperate with [members of the department](/people/members) or with the 
[research groups](/research/research_groups) within the department.

- **Non-Teaching Positions:** the [Center for Advanced Studies in 
  Mathematics](/research/center), supported by the Skirball foundation, 
  sponsors several postdoctoral positions every year. Review of applications 
  begins usually at the *end of December*, but in exceptional cases the 
  applications can be considered all year round.

-  **Teaching Positions:** Candidates capable of teaching in Hebrew can apply 
   for teaching postdoctoral fellowships. The starting date for these 
   positions is usually *October 1*.

To apply for any of these fellowships, please send a formal _application
letter_, _resume_, _research statement_ and arrange for at least _three 
references_ to be sent to the address below.

**Furthermore**,

- Applicants are strongly encouraged to specify the
 [researchers](/people/members) closest to their research area and to contact 
 those researchers directly.

- **American applicants** are strongly encouraged to apply for a [Fullbright 
  Fellowship](http://www.fulbright.org.il/index.php?id=1317) which provides a 
  substantial stipend awarded in _addition_ to the customary postdoctoral 
  fellowship offered by the Mathematics department.

  Applications must be submitted early; for example the deadline was _August 
  1st, 2013_ for the fellowship corresponding to the academic year 2014/15.

- **Canadian applicants** are strongly encouraged to apply for a fellowship 
  in the [Azrieli Fellows Program](http://fellows.azrielifoundation.org/), 
  which provides a substantial stipend.

- **French applicants** are strongly encouraged to apply for a [Chateaubriand 
  Fellowship](http://www.france-israel-fellows.com/?q=chateaubriand-fellowships) 
  which provides a substantial stipend. Applications must be submitted until 
  the second half of December each year.

- **China/India applicants** are strongly encouraged to apply for the [special scholarship](/~kernerdm/h_files.html/LJ/pbc.pdf)

- **American/Canadian aplicants** (or applicants with documented status in US/Canada) can apply for the [Zuckerman scholarship](/~kernerdm/h_files.html/LJ/Zuckerman.Call.pdf)

Information on some [additional scholarships](http://in.bgu.ac.il/en/kreitman_school/Pages/Post-Doc.aspx) is available from the Kreitman school

##### Information for Prospective Postdocs

See the [Wiki knowledge base](/community/wiki) maintained informally by 
current postdocs.

