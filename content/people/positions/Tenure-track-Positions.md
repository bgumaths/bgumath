The [Department of Mathematics](/) at the [Ben-Gurion University of the 
Negev](https://bgu.ac.il) in Israel is inviting applications for tenure track 
positions at all levels.

Candidates must have a Ph.D. in mathematics, and demonstrate excellence in 
research and teaching. Candidates in all areas of mathematics will be 
considered. Mathematicians whose research lies in the area of Mathematical 
Education are encouraged to apply. All positions require teaching in Hebrew.

Applications will be reviewed on a continuing basis. Applicants are 
encouraged to apply before **November 1**, although late applications may be 
considered until the positions are filled.

The applicant should send a **resume**, a **research statement**, and a few 
**selected works** to the address below. In addition, the department will ask 
for reference letters directly from researchers, to be chosen by the 
department. Due to internal regulations, we are prevented from using 
reference letters requested directly by the applicant.  Nevertheless, 
applicants may **supply a list of 4-5 experts in their field as possible 
references**, although the department is not obligated to use this list.

