# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[6.1].define(version: 2019_04_27_044104) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "ac_years", force: :cascade do |t|
    t.integer "year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["year"], name: "index_ac_years_on_year", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "admins_seminars", id: false, force: :cascade do |t|
    t.integer "admin_id", null: false
    t.integer "seminar_id", null: false
    t.index ["admin_id"], name: "index_admins_seminars_on_admin_id"
    t.index ["seminar_id"], name: "index_admins_seminars_on_seminar_id"
  end

  create_table "allows_requires", id: false, force: :cascade do |t|
    t.bigint "allow_id", null: false
    t.bigint "require_id", null: false
    t.index ["allow_id", "require_id"], name: "index_allows_requires_on_allow_id_and_require_id", unique: true
    t.index ["require_id", "allow_id"], name: "index_allows_requires_on_require_id_and_allow_id", unique: true
  end

  create_table "basic_prog_prereq_infos", force: :cascade do |t|
    t.integer "minc"
    t.integer "maxc"
    t.float "minp"
    t.float "maxp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "prereq_id"
    t.index ["prereq_id"], name: "index_basic_prog_prereq_infos_on_prereq_id", unique: true
  end

  create_table "course_lists", force: :cascade do |t|
    t.jsonb "name_i18n", default: {}, null: false
    t.jsonb "remarks_i18n", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.bigint "shnaton_id", null: false
    t.integer "dcredits"
    t.index ["name_i18n"], name: "index_course_lists_on_name_i18n"
    t.index ["remarks_i18n"], name: "index_course_lists_on_remarks_i18n"
    t.index ["shnaton_id", "slug"], name: "index_course_lists_on_shnaton_id_and_slug", unique: true
    t.index ["shnaton_id"], name: "index_course_lists_on_shnaton_id"
    t.index ["slug"], name: "index_course_lists_on_slug"
  end

  create_table "course_lists_courses", id: false, force: :cascade do |t|
    t.bigint "course_list_id", null: false
    t.bigint "course_id", null: false
    t.index ["course_id", "course_list_id"], name: "index_course_lists_courses_on_course_id_and_course_list_id"
    t.index ["course_list_id", "course_id"], name: "index_course_lists_courses_on_course_list_id_and_course_id"
  end

  create_table "course_lists_prog_prereqs", id: false, force: :cascade do |t|
    t.bigint "course_list_id", null: false
    t.bigint "prereq_id", null: false
    t.index ["prereq_id", "course_list_id"], name: "index_course_lists_prog_prereqs_on_prereq_id_and_course_list_id", unique: true
  end

  create_table "courses", id: :serial, force: :cascade do |t|
    t.integer "term_id"
    t.integer "generic_course_id"
    t.integer "lecturer_id"
    t.text "hours"
    t.string "web", limit: 255
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "title_i18n", default: {}, null: false
    t.jsonb "content_i18n", default: {}, null: false
    t.string "slug"
    t.jsonb "descfiles"
    t.bigint "student_rep_id"
    t.bigint "aguda_rep_id"
    t.index ["aguda_rep_id"], name: "index_courses_on_aguda_rep_id"
    t.index ["content_i18n"], name: "index_courses_on_content_i18n"
    t.index ["generic_course_id", "term_id"], name: "index_courses_on_generic_course_id_and_term_id", unique: true
    t.index ["generic_course_id"], name: "index_courses_on_generic_course_id"
    t.index ["lecturer_id"], name: "index_courses_on_lecturer_id"
    t.index ["slug"], name: "index_courses_on_slug"
    t.index ["student_rep_id"], name: "index_courses_on_student_rep_id"
    t.index ["term_id", "slug"], name: "index_courses_on_term_id_and_slug", unique: true
    t.index ["term_id"], name: "index_courses_on_term_id"
    t.index ["title_i18n"], name: "index_courses_on_title_i18n"
  end

  create_table "courses_issues_users", id: false, force: :cascade do |t|
    t.bigint "issues_user_id", null: false
    t.bigint "course_id", null: false
    t.index ["course_id", "issues_user_id"], name: "index_courses_issues_users_on_course_id_and_issues_user_id", unique: true
    t.index ["issues_user_id", "course_id"], name: "index_courses_issues_users_on_issues_user_id_and_course_id", unique: true
  end

  create_table "degree_plans", force: :cascade do |t|
    t.jsonb "name_i18n", default: {"en"=>"recommended program", "he"=>"תכנית לימודים מומלצת"}, null: false
    t.bigint "degree_id"
    t.jsonb "remarks_i18n", default: {}, null: false
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["degree_id", "slug"], name: "index_degree_plans_on_degree_id_and_slug", unique: true
    t.index ["degree_id"], name: "index_degree_plans_on_degree_id"
    t.index ["slug"], name: "index_degree_plans_on_slug"
  end

# Could not dump table "degrees" because of following StandardError
#   Unknown type 'degree_type' for column 'type'

  create_table "departments", force: :cascade do |t|
    t.jsonb "name_i18n", default: {}, null: false
    t.integer "catalog_id"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "webpage"
    t.index ["catalog_id"], name: "index_departments_on_catalog_id", unique: true
    t.index ["slug"], name: "index_departments_on_slug", unique: true
  end

  create_table "departments_generic_courses", id: false, force: :cascade do |t|
    t.bigint "generic_course_id", null: false
    t.bigint "department_id", null: false
    t.index ["department_id", "generic_course_id"], name: "index_departments_generic_courses_on_dept_id_and_course_id"
    t.index ["generic_course_id", "department_id"], name: "index_departments_generic_courses_on_course_id_and_dept_id"
  end

  create_table "events", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255
    t.string "location", limit: 255
    t.string "web", limit: 255
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "centre"
    t.tstzrange "duration", null: false
    t.jsonb "descfiles"
    t.string "slug"
    t.date "expires"
    t.index ["duration"], name: "index_events_on_duration", using: :gist
    t.index ["slug"], name: "index_events_on_slug", unique: true
  end

  create_table "free_plan_items", force: :cascade do |t|
    t.jsonb "title_i18n", default: {"en"=>"Elective courses", "he"=>"קורסי בחירה"}, null: false
    t.float "minp"
    t.float "maxp"
    t.bigint "term_plan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["term_plan_id"], name: "index_free_plan_items_on_term_plan_id"
  end

  create_table "friendly_id_slugs", id: :serial, force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "generic_courses", id: :serial, force: :cascade do |t|
    t.string "shid", limit: 255
    t.boolean "graduate", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "prereq", default: false, null: false
    t.boolean "core", default: false, null: false
    t.jsonb "name_i18n", default: {}, null: false
    t.jsonb "description_i18n", default: {}, null: false
    t.string "slug"
    t.jsonb "descfiles"
    t.boolean "advanced", default: true
    t.boolean "fall"
    t.boolean "spring"
    t.integer "lectures"
    t.integer "exercises"
    t.integer "dcredits"
    t.bigint "year_added_id"
    t.bigint "year_removed_id"
    t.bigint "aguda_rep_id"
    t.index ["aguda_rep_id"], name: "index_generic_courses_on_aguda_rep_id"
    t.index ["description_i18n"], name: "index_generic_courses_on_description_i18n"
    t.index ["name_i18n"], name: "index_generic_courses_on_name_i18n"
    t.index ["shid"], name: "index_generic_courses_on_shid", unique: true, where: "(year_removed_id IS NULL)"
    t.index ["slug"], name: "index_generic_courses_on_slug", unique: true
    t.index ["year_added_id"], name: "index_generic_courses_on_year_added_id"
    t.index ["year_removed_id"], name: "index_generic_courses_on_year_removed_id"
  end

  create_table "hosts_postdocs", id: false, force: :cascade do |t|
    t.integer "postdoc_id", null: false
    t.integer "host_id", null: false
    t.index ["postdoc_id", "host_id"], name: "index_hosts_postdocs_on_postdoc_id_and_host_id"
  end

  create_table "hosts_visitors", id: false, force: :cascade do |t|
    t.integer "host_id"
    t.integer "visitor_id"
    t.index ["host_id", "visitor_id"], name: "hosts_visitors_index", unique: true
  end

  create_table "impressions", id: :serial, force: :cascade do |t|
    t.string "impressionable_type"
    t.string "impressionable_id"
    t.string "user_id"
    t.string "controller_name"
    t.string "action_name"
    t.string "view_name"
    t.string "request_hash"
    t.inet "ip_address"
    t.string "session_hash"
    t.text "message"
    t.text "referrer"
    t.jsonb "params"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index"
    t.index ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index"
    t.index ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index"
    t.index ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index"
    t.index ["impressionable_type", "impressionable_id", "params"], name: "poly_params_request_index"
    t.index ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index"
    t.index ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index"
    t.index ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index"
    t.index ["user_id"], name: "index_impressions_on_user_id"
  end

  create_table "issues", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.bigint "course_id"
    t.string "title", null: false
    t.text "content"
    t.text "reply"
    t.boolean "published", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.datetime "replied_at"
    t.datetime "published_at"
    t.text "correspondence", null: false
    t.bigint "reporter_id", null: false
    t.bigint "replier_id"
    t.bigint "aguda_rep_id"
    t.index ["aguda_rep_id"], name: "index_issues_on_aguda_rep_id"
    t.index ["course_id"], name: "index_issues_on_course_id"
    t.index ["replier_id"], name: "index_issues_on_replier_id"
    t.index ["reporter_id"], name: "index_issues_on_reporter_id"
    t.index ["slug"], name: "index_issues_on_slug", unique: true
  end

  create_table "issues_staffs", id: false, force: :cascade do |t|
    t.uuid "issue_id"
    t.integer "staff_id"
    t.index ["issue_id", "staff_id"], name: "index_issues_staffs_on_issue_id_and_staff_id"
    t.index ["staff_id", "issue_id"], name: "index_issues_staffs_on_staff_id_and_issue_id"
  end

  create_table "issues_users", force: :cascade do |t|
    t.string "login", null: false
    t.string "crypted_password", null: false
    t.string "password_salt", null: false
    t.string "persistence_token"
    t.integer "login_count", default: 0, null: false
    t.integer "failed_login_count", default: 0, null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.inet "current_login_ip"
    t.inet "last_login_ip"
    t.boolean "active", default: true
    t.boolean "approved", default: true
    t.boolean "confirmed", default: true
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "fullname"
    t.boolean "pass_changed", default: false
    t.bigint "department_id"
    t.index ["department_id"], name: "index_issues_users_on_department_id"
    t.index ["login"], name: "index_issues_users_on_login", unique: true
  end

  create_table "leaves", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.daterange "duration"
    t.jsonb "note_i18n", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["duration"], name: "index_leaves_on_duration"
    t.index ["user_id"], name: "index_leaves_on_user_id"
  end

  create_table "meetings", id: :serial, force: :cascade do |t|
    t.integer "seminar_id"
    t.string "speaker", limit: 255
    t.string "title", limit: 255
    t.text "abstract"
    t.string "room", limit: 255
    t.string "from", limit: 255
    t.tstzrange "duration", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.string "speaker_web"
    t.jsonb "descfiles"
    t.jsonb "announcement_i18n", default: {}, null: false
    t.index ["duration"], name: "index_meetings_on_duration", using: :gist
    t.index ["seminar_id"], name: "index_meetings_on_seminar_id"
    t.index ["slug"], name: "index_meetings_on_slug"
  end

  create_table "members_research_groups", id: false, force: :cascade do |t|
    t.integer "member_id", null: false
    t.integer "research_group_id", null: false
    t.index ["member_id", "research_group_id"], name: "members_research_groups_index", unique: true
    t.index ["member_id"], name: "index_members_research_groups_on_member_id"
    t.index ["research_group_id"], name: "index_members_research_groups_on_research_group_id"
  end

  create_table "old_users", id: :serial, force: :cascade do |t|
    t.string "email"
    t.string "status"
    t.string "phd_year"
    t.string "phd_from"
    t.text "interests"
    t.string "rank"
    t.string "webpage"
    t.integer "state", default: 0, null: false
    t.jsonb "first_i18n", default: {}, null: false
    t.jsonb "last_i18n", default: {}, null: false
    t.string "slug"
    t.jsonb "photos"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_old_users_on_email"
    t.index ["first_i18n"], name: "index_old_users_on_first_i18n"
    t.index ["last_i18n"], name: "index_old_users_on_last_i18n"
    t.index ["slug"], name: "index_old_users_on_slug", unique: true
  end

  create_table "plan_items", force: :cascade do |t|
    t.bigint "term_plan_id"
    t.bigint "generic_course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["generic_course_id"], name: "index_plan_items_on_generic_course_id"
    t.index ["term_plan_id"], name: "index_plan_items_on_term_plan_id"
  end

  create_table "precise_prog_prereq_infos", force: :cascade do |t|
    t.bigint "prereq_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["prereq_id"], name: "index_precise_prog_prereq_infos_on_prereq_id", unique: true
  end

  create_table "prog_prereqs", force: :cascade do |t|
    t.jsonb "name_i18n", default: {}, null: false
    t.jsonb "description_i18n", default: {}, null: false
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "parent_id"
    t.string "slug"
    t.bigint "course_lists_order", default: [], array: true
    t.bigint "degree_id"
    t.index ["degree_id"], name: "index_prog_prereqs_on_degree_id"
    t.index ["name_i18n"], name: "index_prog_prereqs_on_name_i18n"
    t.index ["parent_id"], name: "index_prog_prereqs_on_parent_id"
    t.index ["slug"], name: "index_prog_prereqs_on_slug"
    t.index ["type"], name: "index_prog_prereqs_on_type"
  end

  create_table "programs", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "name_i18n", default: {}, null: false
    t.jsonb "description_i18n", default: {}, null: false
    t.string "slug"
    t.index ["description_i18n"], name: "index_programs_on_description_i18n"
    t.index ["name_i18n"], name: "index_programs_on_name_i18n"
    t.index ["slug"], name: "index_programs_on_slug", unique: true
  end

  create_table "programs_coordinators", id: false, force: :cascade do |t|
    t.integer "program_id"
    t.integer "coordinator_id"
    t.index ["program_id", "coordinator_id"], name: "programs_coordinators_index", unique: true
  end

  create_table "replaced_by_replaces", id: false, force: :cascade do |t|
    t.bigint "replace_id", null: false
    t.bigint "replaced_by_id", null: false
    t.index ["replace_id", "replaced_by_id"], name: "index_replaced_by_replaces_on_replace_id_and_replaced_by_id"
    t.index ["replaced_by_id", "replace_id"], name: "index_replaced_by_replaces_on_replaced_by_id_and_replace_id"
  end

  create_table "research_groups", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.string "slug"
    t.index ["name"], name: "index_research_groups_on_name"
    t.index ["slug"], name: "index_research_groups_on_slug", unique: true
  end

  create_table "research_groups_seminars", id: false, force: :cascade do |t|
    t.integer "research_group_id", null: false
    t.integer "seminar_id", null: false
    t.index ["seminar_id", "research_group_id"], name: "index_seminar_id_research_group_id", unique: true
    t.index ["seminar_id", "research_group_id"], name: "research_groups_seminars_index", unique: true
  end

  create_table "roles", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.integer "resource_id"
    t.string "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "visibility", default: 2, null: false
    t.jsonb "title_i18n", default: {}, null: false
    t.jsonb "description_i18n", default: {}, null: false
    t.string "email"
    t.index ["description_i18n"], name: "index_roles_on_description_i18n"
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["title_i18n"], name: "index_roles_on_title_i18n"
  end

  create_table "seminars", id: :serial, force: :cascade do |t|
    t.integer "day"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "room", limit: 255
    t.tstzrange "duration", null: false
    t.jsonb "name_i18n", default: {}, null: false
    t.jsonb "description_i18n", default: {}, null: false
    t.string "slug"
    t.string "list_id"
    t.string "web"
    t.integer "term_id"
    t.index ["description_i18n"], name: "index_seminars_on_description_i18n"
    t.index ["duration"], name: "index_seminars_on_duration", using: :gist
    t.index ["list_id"], name: "index_seminars_on_list_id"
    t.index ["name_i18n"], name: "index_seminars_on_name_i18n"
    t.index ["slug"], name: "index_seminars_on_slug"
    t.index ["term_id", "slug"], name: "index_seminars_on_term_id_and_slug", unique: true
    t.index ["term_id"], name: "index_seminars_on_term_id"
  end

  create_table "sessions", id: :serial, force: :cascade do |t|
    t.string "session_id", null: false
    t.jsonb "data"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "settings", id: :serial, force: :cascade do |t|
    t.string "var", limit: 255, null: false
    t.text "value"
    t.integer "thing_id"
    t.string "thing_type", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true
  end

  create_table "shnatons", force: :cascade do |t|
    t.bigint "ac_year_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "degrees_order", default: [], array: true
    t.index ["ac_year_id"], name: "index_shnatons_on_ac_year_id", unique: true
  end

  create_table "students_supervisors", id: false, force: :cascade do |t|
    t.integer "student_id", null: false
    t.integer "supervisor_id", null: false
    t.index ["student_id", "supervisor_id"], name: "index_students_supervisors_on_student_id_and_supervisor_id"
    t.index ["supervisor_id", "student_id"], name: "index_students_supervisors_on_supervisor_id_and_student_id"
  end

  create_table "templates", id: :serial, force: :cascade do |t|
    t.text "body"
    t.string "path"
    t.string "locale"
    t.string "handler", default: "erb"
    t.boolean "partial", default: false
    t.string "format", default: "html"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.string "url"
    t.string "object_type"
    t.integer "object_id"
    t.index ["object_type", "object_id"], name: "index_templates_on_object_type_and_object_id"
    t.index ["path", "locale", "format", "handler", "partial"], name: "templates_index", unique: true
    t.index ["slug"], name: "index_templates_on_slug", unique: true
  end

# Could not dump table "term_plans" because of following StandardError
#   Unknown type 'term' for column 'term'

  create_table "terms", id: :serial, force: :cascade do |t|
    t.date "starts"
    t.date "ends"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "exams"
    t.string "slug"
    t.index ["slug"], name: "index_terms_on_slug", unique: true
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", limit: 255
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest", limit: 255
    t.string "office", limit: 255
    t.string "status", limit: 255
    t.string "phd_year", limit: 255
    t.string "phd_from", limit: 255
    t.text "interests"
    t.string "phone", limit: 255
    t.string "rank", limit: 255
    t.string "hours", limit: 255
    t.string "webpage", limit: 255, default: "1"
    t.string "persistence_token", limit: 255
    t.string "password_salt", limit: 255
    t.integer "failed_login_count", default: 0, null: false
    t.integer "state", default: 0, null: false
    t.jsonb "first_i18n", default: {}, null: false
    t.jsonb "last_i18n", default: {}, null: false
    t.string "slug"
    t.jsonb "photos"
    t.string "oh_location"
    t.date "start"
    t.date "finish"
    t.date "deceased"
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string "current_login_ip"
    t.string "last_login_ip"
    t.string "selected_photos"
    t.date "oh_expiry"
    t.jsonb "crop_data_photos", default: {}, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["first_i18n"], name: "index_users_on_first_i18n"
    t.index ["last_i18n"], name: "index_users_on_last_i18n"
    t.index ["slug"], name: "index_users_on_slug"
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", unique: true
  end

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string "foreign_key_name", null: false
    t.integer "foreign_key_id"
    t.string "foreign_type", null: false
    t.index ["foreign_key_name", "foreign_key_id", "foreign_type"], name: "index_version_associations_on_foreign_key"
    t.index ["version_id"], name: "index_version_associations_on_version_id"
  end

  create_table "versions", id: :serial, force: :cascade do |t|
    t.string "item_type", limit: 255, null: false
    t.string "item_id", null: false
    t.string "event", limit: 255, null: false
    t.string "whodunnit", limit: 255
    t.datetime "created_at"
    t.text "message"
    t.jsonb "object"
    t.datetime "undone_at"
    t.jsonb "object_changes"
    t.string "item_subtype"
    t.integer "transaction_id"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
    t.index ["transaction_id"], name: "index_versions_on_transaction_id"
  end

  create_table "visitors", id: :serial, force: :cascade do |t|
    t.string "email"
    t.jsonb "first_i18n", default: {}, null: false
    t.jsonb "last_i18n", default: {}, null: false
    t.string "office"
    t.string "phone"
    t.string "rank"
    t.string "webpage"
    t.string "origin"
    t.boolean "centre", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.daterange "duration", null: false
    t.index ["email"], name: "index_visitors_on_email"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "course_lists", "shnatons"
  add_foreign_key "courses", "issues_users", column: "aguda_rep_id"
  add_foreign_key "courses", "issues_users", column: "student_rep_id"
  add_foreign_key "degree_plans", "degrees"
  add_foreign_key "degree_plans", "degrees"
  add_foreign_key "degrees", "shnatons"
  add_foreign_key "free_plan_items", "term_plans"
  add_foreign_key "generic_courses", "ac_years", column: "year_added_id"
  add_foreign_key "generic_courses", "ac_years", column: "year_removed_id"
  add_foreign_key "generic_courses", "issues_users", column: "aguda_rep_id"
  add_foreign_key "issues", "courses"
  add_foreign_key "issues", "issues_users", column: "aguda_rep_id"
  add_foreign_key "issues", "issues_users", column: "reporter_id"
  add_foreign_key "issues", "users", column: "replier_id"
  add_foreign_key "issues_users", "departments"
  add_foreign_key "plan_items", "generic_courses"
  add_foreign_key "plan_items", "term_plans"
  add_foreign_key "prog_prereqs", "degrees"
  add_foreign_key "seminars", "terms"
  add_foreign_key "shnatons", "ac_years"
  add_foreign_key "term_plans", "degree_plans"
  add_foreign_key "term_plans", "degree_plans"
end
