SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

-- *not* creating schema, since initdb creates it


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: degree_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.degree_type AS ENUM (
    'single',
    'dual',
    'double',
    'special'
);


--
-- Name: term; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.term AS ENUM (
    'fall',
    'spring'
);


--
-- Name: que_validate_tags(jsonb); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.que_validate_tags(tags_array jsonb) RETURNS boolean
    LANGUAGE sql
    AS $$
  SELECT bool_and(
    jsonb_typeof(value) = 'string'
    AND
    char_length(value::text) <= 100
  )
  FROM jsonb_array_elements(tags_array)
$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: que_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.que_jobs (
    priority smallint DEFAULT 100 NOT NULL,
    run_at timestamp with time zone DEFAULT now() NOT NULL,
    id bigint NOT NULL,
    job_class text NOT NULL,
    error_count integer DEFAULT 0 NOT NULL,
    last_error_message text,
    queue text DEFAULT 'default'::text NOT NULL,
    last_error_backtrace text,
    finished_at timestamp with time zone,
    expired_at timestamp with time zone,
    args jsonb DEFAULT '[]'::jsonb NOT NULL,
    data jsonb DEFAULT '{}'::jsonb NOT NULL,
    job_schema_version integer,
    kwargs jsonb DEFAULT '{}'::jsonb NOT NULL,
    CONSTRAINT error_length CHECK (((char_length(last_error_message) <= 500) AND (char_length(last_error_backtrace) <= 10000))),
    CONSTRAINT job_class_length CHECK ((char_length(
CASE job_class
    WHEN 'ActiveJob::QueueAdapters::QueAdapter::JobWrapper'::text THEN ((args -> 0) ->> 'job_class'::text)
    ELSE job_class
END) <= 200)),
    CONSTRAINT queue_length CHECK ((char_length(queue) <= 100)),
    CONSTRAINT valid_args CHECK ((jsonb_typeof(args) = 'array'::text)),
    CONSTRAINT valid_data CHECK (((jsonb_typeof(data) = 'object'::text) AND ((NOT (data ? 'tags'::text)) OR ((jsonb_typeof((data -> 'tags'::text)) = 'array'::text) AND (jsonb_array_length((data -> 'tags'::text)) <= 5) AND public.que_validate_tags((data -> 'tags'::text))))))
)
WITH (fillfactor='90');


--
-- Name: TABLE que_jobs; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.que_jobs IS '6';


--
-- Name: que_determine_job_state(public.que_jobs); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.que_determine_job_state(job public.que_jobs) RETURNS text
    LANGUAGE sql
    AS $$
  SELECT
    CASE
    WHEN job.expired_at  IS NOT NULL    THEN 'expired'
    WHEN job.finished_at IS NOT NULL    THEN 'finished'
    WHEN job.error_count > 0            THEN 'errored'
    WHEN job.run_at > CURRENT_TIMESTAMP THEN 'scheduled'
    ELSE                                     'ready'
    END
$$;


--
-- Name: que_job_notify(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.que_job_notify() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    locker_pid integer;
    sort_key json;
  BEGIN
    -- Don't do anything if the job is scheduled for a future time.
    IF NEW.run_at IS NOT NULL AND NEW.run_at > now() THEN
      RETURN null;
    END IF;

    -- Pick a locker to notify of the job's insertion, weighted by their number
    -- of workers. Should bounce pseudorandomly between lockers on each
    -- invocation, hence the md5-ordering, but still touch each one equally,
    -- hence the modulo using the job_id.
    SELECT pid
    INTO locker_pid
    FROM (
      SELECT *, last_value(row_number) OVER () + 1 AS count
      FROM (
        SELECT *, row_number() OVER () - 1 AS row_number
        FROM (
          SELECT *
          FROM public.que_lockers ql, generate_series(1, ql.worker_count) AS id
          WHERE
            listening AND
            queues @> ARRAY[NEW.queue] AND
            ql.job_schema_version = NEW.job_schema_version
          ORDER BY md5(pid::text || id::text)
        ) t1
      ) t2
    ) t3
    WHERE NEW.id % count = row_number;

    IF locker_pid IS NOT NULL THEN
      -- There's a size limit to what can be broadcast via LISTEN/NOTIFY, so
      -- rather than throw errors when someone enqueues a big job, just
      -- broadcast the most pertinent information, and let the locker query for
      -- the record after it's taken the lock. The worker will have to hit the
      -- DB in order to make sure the job is still visible anyway.
      SELECT row_to_json(t)
      INTO sort_key
      FROM (
        SELECT
          'job_available' AS message_type,
          NEW.queue       AS queue,
          NEW.priority    AS priority,
          NEW.id          AS id,
          -- Make sure we output timestamps as UTC ISO 8601
          to_char(NEW.run_at AT TIME ZONE 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS.US"Z"') AS run_at
      ) t;

      PERFORM pg_notify('que_listener_' || locker_pid::text, sort_key::text);
    END IF;

    RETURN null;
  END
$$;


--
-- Name: que_state_notify(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.que_state_notify() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    row record;
    message json;
    previous_state text;
    current_state text;
  BEGIN
    IF TG_OP = 'INSERT' THEN
      previous_state := 'nonexistent';
      current_state  := public.que_determine_job_state(NEW);
      row            := NEW;
    ELSIF TG_OP = 'DELETE' THEN
      previous_state := public.que_determine_job_state(OLD);
      current_state  := 'nonexistent';
      row            := OLD;
    ELSIF TG_OP = 'UPDATE' THEN
      previous_state := public.que_determine_job_state(OLD);
      current_state  := public.que_determine_job_state(NEW);

      -- If the state didn't change, short-circuit.
      IF previous_state = current_state THEN
        RETURN null;
      END IF;

      row := NEW;
    ELSE
      RAISE EXCEPTION 'Unrecognized TG_OP: %', TG_OP;
    END IF;

    SELECT row_to_json(t)
    INTO message
    FROM (
      SELECT
        'job_change' AS message_type,
        row.id       AS id,
        row.queue    AS queue,

        coalesce(row.data->'tags', '[]'::jsonb) AS tags,

        to_char(row.run_at AT TIME ZONE 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS.US"Z"') AS run_at,
        to_char(now()      AT TIME ZONE 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS.US"Z"') AS time,

        CASE row.job_class
        WHEN 'ActiveJob::QueueAdapters::QueAdapter::JobWrapper' THEN
          coalesce(
            row.args->0->>'job_class',
            'ActiveJob::QueueAdapters::QueAdapter::JobWrapper'
          )
        ELSE
          row.job_class
        END AS job_class,

        previous_state AS previous_state,
        current_state  AS current_state
    ) t;

    PERFORM pg_notify('que_state', message::text);

    RETURN null;
  END
$$;


--
-- Name: ac_years; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ac_years (
    id bigint NOT NULL,
    year integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: ac_years_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.ac_years_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ac_years_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.ac_years_id_seq OWNED BY public.ac_years.id;


--
-- Name: active_storage_attachments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.active_storage_attachments (
    id bigint NOT NULL,
    name character varying NOT NULL,
    record_type character varying NOT NULL,
    record_id bigint NOT NULL,
    blob_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL
);


--
-- Name: active_storage_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.active_storage_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: active_storage_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.active_storage_attachments_id_seq OWNED BY public.active_storage_attachments.id;


--
-- Name: active_storage_blobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.active_storage_blobs (
    id bigint NOT NULL,
    key character varying NOT NULL,
    filename character varying NOT NULL,
    content_type character varying,
    metadata text,
    byte_size bigint NOT NULL,
    checksum character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    service_name character varying NOT NULL
);


--
-- Name: active_storage_blobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.active_storage_blobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: active_storage_blobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.active_storage_blobs_id_seq OWNED BY public.active_storage_blobs.id;


--
-- Name: active_storage_variant_records; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.active_storage_variant_records (
    id bigint NOT NULL,
    blob_id bigint NOT NULL,
    variation_digest character varying NOT NULL
);


--
-- Name: active_storage_variant_records_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.active_storage_variant_records_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: active_storage_variant_records_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.active_storage_variant_records_id_seq OWNED BY public.active_storage_variant_records.id;


--
-- Name: admins_seminars; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.admins_seminars (
    admin_id integer NOT NULL,
    seminar_id integer NOT NULL
);


--
-- Name: allows_requires; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.allows_requires (
    allow_id bigint NOT NULL,
    require_id bigint NOT NULL,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.763207'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.764245'::timestamp without time zone NOT NULL
);


--
-- Name: allows_requires_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.allows_requires_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: allows_requires_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.allows_requires_id_seq OWNED BY public.allows_requires.id;


--
-- Name: app_setups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.app_setups (
    id bigint NOT NULL,
    depname_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    fdepname_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    department_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    university_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    dept_email character varying,
    dept_fax character varying,
    dept_phone character varying,
    mail_domain character varying,
    local_mail_domain character varying,
    building_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    dept_snail_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    ml_base character varying,
    ml_mail_domain character varying,
    ml_domain character varying,
    ml_admin character varying,
    ml_admin_name character varying,
    gmaps_url character varying,
    moovit_url character varying,
    waze_url character varying,
    parent_url character varying,
    gspreadsheet_key character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: app_setups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.app_setups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: app_setups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.app_setups_id_seq OWNED BY public.app_setups.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: basic_prog_prereq_infos; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.basic_prog_prereq_infos (
    id bigint NOT NULL,
    minc integer,
    maxc integer,
    minp double precision,
    maxp double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    prereq_id bigint
);


--
-- Name: basic_prog_prereq_infos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.basic_prog_prereq_infos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: basic_prog_prereq_infos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.basic_prog_prereq_infos_id_seq OWNED BY public.basic_prog_prereq_infos.id;


--
-- Name: carousel_item_records; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.carousel_item_records (
    id bigint NOT NULL,
    item_id bigint,
    resource_type character varying,
    resource_id bigint,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: carousel_item_records_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.carousel_item_records_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: carousel_item_records_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.carousel_item_records_id_seq OWNED BY public.carousel_item_records.id;


--
-- Name: carousel_items; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.carousel_items (
    id bigint NOT NULL,
    activates timestamp without time zone,
    expires timestamp without time zone,
    "interval" integer,
    "order" integer,
    url character varying,
    type character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: carousel_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.carousel_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: carousel_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.carousel_items_id_seq OWNED BY public.carousel_items.id;


--
-- Name: chair_role_setups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.chair_role_setups (
    id bigint NOT NULL,
    app_setup_id bigint,
    role_id bigint,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: chair_role_setups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.chair_role_setups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: chair_role_setups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.chair_role_setups_id_seq OWNED BY public.chair_role_setups.id;


--
-- Name: course_lists; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.course_lists (
    id bigint NOT NULL,
    name_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    remarks_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    slug character varying,
    shnaton_id bigint NOT NULL,
    dcredits integer,
    type character varying
);


--
-- Name: course_lists_courses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.course_lists_courses (
    course_list_id bigint NOT NULL,
    course_id bigint NOT NULL,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.580797'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.581823'::timestamp without time zone NOT NULL
);


--
-- Name: course_lists_courses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.course_lists_courses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: course_lists_courses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.course_lists_courses_id_seq OWNED BY public.course_lists_courses.id;


--
-- Name: course_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.course_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: course_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.course_lists_id_seq OWNED BY public.course_lists.id;


--
-- Name: course_lists_prog_prereqs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.course_lists_prog_prereqs (
    course_list_id bigint NOT NULL,
    prereq_id bigint NOT NULL,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 15:58:26.686497'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 15:58:26.687688'::timestamp without time zone NOT NULL
);


--
-- Name: course_lists_prog_prereqs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.course_lists_prog_prereqs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: course_lists_prog_prereqs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.course_lists_prog_prereqs_id_seq OWNED BY public.course_lists_prog_prereqs.id;


--
-- Name: courses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.courses (
    id integer NOT NULL,
    term_id integer,
    generic_course_id integer,
    lecturer_id integer,
    hours text,
    web character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    title_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    content_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    slug character varying,
    student_rep_id bigint,
    aguda_rep_id bigint,
    abstract_i18n jsonb DEFAULT '{}'::jsonb NOT NULL
);


--
-- Name: courses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.courses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: courses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.courses_id_seq OWNED BY public.courses.id;


--
-- Name: courses_issues_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.courses_issues_users (
    issues_user_id bigint NOT NULL,
    course_id bigint NOT NULL
);


--
-- Name: degree_plans; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.degree_plans (
    id bigint NOT NULL,
    name_i18n jsonb DEFAULT '{"en": "recommended program", "he": "תכנית לימודים מומלצת"}'::jsonb NOT NULL,
    degree_id bigint,
    remarks_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    slug character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: degree_plans_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.degree_plans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: degree_plans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.degree_plans_id_seq OWNED BY public.degree_plans.id;


--
-- Name: degrees; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.degrees (
    id bigint NOT NULL,
    name_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    description_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    slug character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    credits double precision,
    shnaton_id bigint NOT NULL,
    prereqs_order bigint[] DEFAULT '{}'::bigint[],
    type public.degree_type DEFAULT 'single'::public.degree_type NOT NULL
);


--
-- Name: degrees_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.degrees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: degrees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.degrees_id_seq OWNED BY public.degrees.id;


--
-- Name: departments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.departments (
    id bigint NOT NULL,
    name_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    catalog_id integer,
    slug character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    webpage character varying,
    bgu_name character varying
);


--
-- Name: departments_generic_courses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.departments_generic_courses (
    generic_course_id bigint NOT NULL,
    department_id bigint NOT NULL,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.008671'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.009935'::timestamp without time zone NOT NULL
);


--
-- Name: departments_generic_courses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.departments_generic_courses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: departments_generic_courses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.departments_generic_courses_id_seq OWNED BY public.departments_generic_courses.id;


--
-- Name: departments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.departments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: departments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.departments_id_seq OWNED BY public.departments.id;


--
-- Name: events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.events (
    id integer NOT NULL,
    title character varying(255),
    location character varying(255),
    web character varying(255),
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    centre boolean,
    duration tstzrange NOT NULL,
    slug character varying,
    expires date,
    online character varying
);


--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.events_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.events_id_seq OWNED BY public.events.id;


--
-- Name: free_plan_items; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.free_plan_items (
    id bigint NOT NULL,
    title_i18n jsonb DEFAULT '{"en": "Elective courses", "he": "קורסי בחירה"}'::jsonb NOT NULL,
    minp double precision,
    maxp double precision,
    term_plan_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: free_plan_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.free_plan_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: free_plan_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.free_plan_items_id_seq OWNED BY public.free_plan_items.id;


--
-- Name: friendly_id_slugs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.friendly_id_slugs (
    id integer NOT NULL,
    slug character varying NOT NULL,
    sluggable_id integer NOT NULL,
    sluggable_type character varying(50),
    scope character varying,
    created_at timestamp without time zone
);


--
-- Name: friendly_id_slugs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.friendly_id_slugs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: friendly_id_slugs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.friendly_id_slugs_id_seq OWNED BY public.friendly_id_slugs.id;


--
-- Name: generic_courses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.generic_courses (
    id integer NOT NULL,
    shid character varying(255),
    graduate boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    prereq boolean DEFAULT false NOT NULL,
    core boolean DEFAULT false NOT NULL,
    name_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    description_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    slug character varying,
    advanced boolean DEFAULT true,
    fall boolean,
    spring boolean,
    lectures integer,
    exercises integer,
    dcredits integer,
    year_added_id bigint,
    year_removed_id bigint,
    aguda_rep_id bigint
);


--
-- Name: generic_courses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.generic_courses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: generic_courses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.generic_courses_id_seq OWNED BY public.generic_courses.id;


--
-- Name: hosts_postdocs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hosts_postdocs (
    postdoc_id integer NOT NULL,
    host_id integer NOT NULL,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.308292'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.311036'::timestamp without time zone NOT NULL
);


--
-- Name: hosts_postdocs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.hosts_postdocs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hosts_postdocs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.hosts_postdocs_id_seq OWNED BY public.hosts_postdocs.id;


--
-- Name: hosts_visitors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hosts_visitors (
    host_id integer,
    visitor_id integer,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.409054'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.41025'::timestamp without time zone NOT NULL
);


--
-- Name: hosts_visitors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.hosts_visitors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hosts_visitors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.hosts_visitors_id_seq OWNED BY public.hosts_visitors.id;


--
-- Name: impressions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.impressions (
    id integer NOT NULL,
    impressionable_type character varying,
    impressionable_id character varying,
    user_id character varying,
    controller_name character varying,
    action_name character varying,
    view_name character varying,
    request_hash character varying,
    ip_address inet,
    session_hash character varying,
    message text,
    referrer text,
    params jsonb,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: impressions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.impressions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: impressions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.impressions_id_seq OWNED BY public.impressions.id;


--
-- Name: issue_responses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.issue_responses (
    id bigint NOT NULL,
    issue_id uuid NOT NULL,
    creator_id bigint NOT NULL,
    content text NOT NULL,
    published_at timestamp without time zone,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: issue_responses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.issue_responses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: issue_responses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.issue_responses_id_seq OWNED BY public.issue_responses.id;


--
-- Name: issues; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.issues (
    id uuid DEFAULT public.gen_random_uuid() NOT NULL,
    course_id bigint,
    title character varying NOT NULL,
    content text,
    reply text,
    published boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    slug character varying,
    replied_at timestamp without time zone,
    published_at timestamp without time zone,
    correspondence text NOT NULL,
    reporter_id bigint NOT NULL,
    replier_id bigint,
    aguda_rep_id bigint
);


--
-- Name: issues_issues_staffs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.issues_issues_staffs (
    issue_id uuid,
    staff_id integer,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 15:58:26.822306'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 15:58:26.823058'::timestamp without time zone NOT NULL
);


--
-- Name: issues_issues_staffs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.issues_issues_staffs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: issues_issues_staffs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.issues_issues_staffs_id_seq OWNED BY public.issues_issues_staffs.id;


--
-- Name: issues_role_setups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.issues_role_setups (
    id bigint NOT NULL,
    app_setup_id bigint,
    role_id bigint,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: issues_role_setups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.issues_role_setups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: issues_role_setups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.issues_role_setups_id_seq OWNED BY public.issues_role_setups.id;


--
-- Name: issues_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.issues_users (
    id bigint NOT NULL,
    login character varying NOT NULL,
    crypted_password character varying,
    password_salt character varying,
    persistence_token character varying,
    login_count integer DEFAULT 0 NOT NULL,
    failed_login_count integer DEFAULT 0 NOT NULL,
    last_request_at timestamp without time zone,
    current_login_at timestamp without time zone,
    last_login_at timestamp without time zone,
    current_login_ip inet,
    last_login_ip inet,
    active boolean DEFAULT true,
    approved boolean DEFAULT true,
    confirmed boolean DEFAULT true,
    type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fullname character varying,
    pass_changed boolean DEFAULT true,
    department_id bigint,
    api_key character varying,
    mail_domain character varying
);


--
-- Name: issues_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.issues_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: issues_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.issues_users_id_seq OWNED BY public.issues_users.id;


--
-- Name: leaves; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.leaves (
    id integer NOT NULL,
    user_id integer,
    duration daterange,
    note_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: leaves_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.leaves_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: leaves_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.leaves_id_seq OWNED BY public.leaves.id;


--
-- Name: meetings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.meetings (
    id integer NOT NULL,
    seminar_id integer,
    speaker character varying(255),
    title character varying(255),
    abstract text,
    room character varying(255),
    "from" character varying(255),
    duration tstzrange NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    slug character varying,
    speaker_web character varying,
    announcement_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    online character varying
);


--
-- Name: meetings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.meetings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: meetings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.meetings_id_seq OWNED BY public.meetings.id;


--
-- Name: members_research_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.members_research_groups (
    member_id integer NOT NULL,
    research_group_id integer NOT NULL,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 14:39:02.331531'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 14:39:02.342786'::timestamp without time zone NOT NULL
);


--
-- Name: members_research_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.members_research_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: members_research_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.members_research_groups_id_seq OWNED BY public.members_research_groups.id;


--
-- Name: old_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.old_users (
    id integer NOT NULL,
    email character varying,
    status character varying,
    phd_year character varying,
    phd_from character varying,
    interests text,
    rank character varying,
    webpage character varying,
    state integer DEFAULT 0 NOT NULL,
    first_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    last_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    slug character varying,
    photos jsonb,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: old_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.old_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: old_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.old_users_id_seq OWNED BY public.old_users.id;


--
-- Name: photos; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.photos (
    imageable_type character varying,
    imageable_id bigint,
    x numeric,
    y numeric,
    width numeric,
    height numeric,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    rotate numeric,
    "scaleX" numeric,
    "scaleY" numeric,
    id bigint NOT NULL
);


--
-- Name: photos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: photos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.photos_id_seq OWNED BY public.photos.id;


--
-- Name: plan_items; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.plan_items (
    id bigint NOT NULL,
    term_plan_id bigint,
    generic_course_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: plan_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.plan_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: plan_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.plan_items_id_seq OWNED BY public.plan_items.id;


--
-- Name: precise_prog_prereq_infos; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.precise_prog_prereq_infos (
    id bigint NOT NULL,
    prereq_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: precise_prog_prereq_infos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.precise_prog_prereq_infos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: precise_prog_prereq_infos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.precise_prog_prereq_infos_id_seq OWNED BY public.precise_prog_prereq_infos.id;


--
-- Name: prog_prereqs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.prog_prereqs (
    id bigint NOT NULL,
    name_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    description_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    parent_id bigint,
    slug character varying,
    course_lists_order bigint[] DEFAULT '{}'::bigint[],
    degree_id bigint
);


--
-- Name: prog_prereqs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.prog_prereqs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prog_prereqs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.prog_prereqs_id_seq OWNED BY public.prog_prereqs.id;


--
-- Name: programs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.programs (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    description_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    slug character varying
);


--
-- Name: programs_coordinators; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.programs_coordinators (
    program_id integer,
    coordinator_id integer
);


--
-- Name: programs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.programs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: programs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.programs_id_seq OWNED BY public.programs.id;


--
-- Name: que_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.que_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: que_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.que_jobs_id_seq OWNED BY public.que_jobs.id;


--
-- Name: que_lockers; Type: TABLE; Schema: public; Owner: -
--

CREATE UNLOGGED TABLE public.que_lockers (
    pid integer NOT NULL,
    worker_count integer NOT NULL,
    worker_priorities integer[] NOT NULL,
    ruby_pid integer NOT NULL,
    ruby_hostname text NOT NULL,
    queues text[] NOT NULL,
    listening boolean NOT NULL,
    job_schema_version integer DEFAULT 1,
    CONSTRAINT valid_queues CHECK (((array_ndims(queues) = 1) AND (array_length(queues, 1) IS NOT NULL))),
    CONSTRAINT valid_worker_priorities CHECK (((array_ndims(worker_priorities) = 1) AND (array_length(worker_priorities, 1) IS NOT NULL)))
);


--
-- Name: que_values; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.que_values (
    key text NOT NULL,
    value jsonb DEFAULT '{}'::jsonb NOT NULL,
    CONSTRAINT valid_value CHECK ((jsonb_typeof(value) = 'object'::text))
)
WITH (fillfactor='90');


--
-- Name: replaced_by_replaces; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.replaced_by_replaces (
    replace_id bigint NOT NULL,
    replaced_by_id bigint NOT NULL,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.922456'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.923568'::timestamp without time zone NOT NULL
);


--
-- Name: replaced_by_replaces_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.replaced_by_replaces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: replaced_by_replaces_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.replaced_by_replaces_id_seq OWNED BY public.replaced_by_replaces.id;


--
-- Name: research_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.research_groups (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    description text,
    slug character varying
);


--
-- Name: research_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.research_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: research_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.research_groups_id_seq OWNED BY public.research_groups.id;


--
-- Name: research_groups_seminars; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.research_groups_seminars (
    research_group_id integer NOT NULL,
    seminar_id integer NOT NULL,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 12:49:05.636622'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 12:49:05.637469'::timestamp without time zone NOT NULL
);


--
-- Name: research_groups_seminars_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.research_groups_seminars_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: research_groups_seminars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.research_groups_seminars_id_seq OWNED BY public.research_groups_seminars.id;


--
-- Name: role_events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.role_events (
    id bigint NOT NULL,
    role_id bigint,
    user_id bigint,
    type character varying DEFAULT 'added'::character varying NOT NULL,
    creator_id bigint,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: role_events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.role_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: role_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.role_events_id_seq OWNED BY public.role_events.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(255),
    resource_id integer,
    resource_type character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    visibility integer DEFAULT 2 NOT NULL,
    title_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    description_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    email character varying
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: seminars; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seminars (
    id integer NOT NULL,
    day integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    room character varying(255),
    duration tstzrange NOT NULL,
    name_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    description_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    slug character varying,
    list_id character varying,
    web character varying,
    term_id integer,
    online character varying
);


--
-- Name: seminars_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.seminars_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: seminars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.seminars_id_seq OWNED BY public.seminars.id;


--
-- Name: sessions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sessions (
    id integer NOT NULL,
    session_id character varying NOT NULL,
    data jsonb,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sessions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sessions_id_seq OWNED BY public.sessions.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.settings (
    id integer NOT NULL,
    var character varying(255) NOT NULL,
    value text,
    thing_id integer,
    thing_type character varying(30),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    visibility integer DEFAULT 1 NOT NULL
);


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.settings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: shnatons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.shnatons (
    id bigint NOT NULL,
    ac_year_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    degrees_order bigint[] DEFAULT '{}'::bigint[],
    preamble_i18n jsonb DEFAULT '{}'::jsonb NOT NULL
);


--
-- Name: shnatons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.shnatons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: shnatons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.shnatons_id_seq OWNED BY public.shnatons.id;


--
-- Name: staff_role_setups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.staff_role_setups (
    id bigint NOT NULL,
    app_setup_id bigint,
    role_id bigint,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: staff_role_setups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.staff_role_setups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: staff_role_setups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.staff_role_setups_id_seq OWNED BY public.staff_role_setups.id;


--
-- Name: students_supervisors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.students_supervisors (
    student_id integer NOT NULL,
    supervisor_id integer NOT NULL,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.167282'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 15:58:27.168399'::timestamp without time zone NOT NULL
);


--
-- Name: students_supervisors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.students_supervisors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: students_supervisors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.students_supervisors_id_seq OWNED BY public.students_supervisors.id;


--
-- Name: templates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.templates (
    id integer NOT NULL,
    body text,
    path character varying,
    locale character varying,
    handler character varying DEFAULT 'erb'::character varying,
    partial boolean DEFAULT false,
    format character varying DEFAULT 'html'::character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    slug character varying,
    url character varying,
    object_type character varying,
    object_id integer
);


--
-- Name: templates_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.templates_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: templates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.templates_id_seq OWNED BY public.templates.id;


--
-- Name: term_plans; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.term_plans (
    id bigint NOT NULL,
    year integer,
    degree_plan_id bigint,
    remarks_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    term public.term
);


--
-- Name: term_plans_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.term_plans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: term_plans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.term_plans_id_seq OWNED BY public.term_plans.id;


--
-- Name: terms; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.terms (
    id integer NOT NULL,
    starts date,
    ends date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    slug character varying,
    name_i18n jsonb DEFAULT '{}'::jsonb NOT NULL
);


--
-- Name: terms_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.terms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: terms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.terms_id_seq OWNED BY public.terms.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    email character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    password_digest character varying(255),
    office character varying(255),
    status character varying(255),
    phd_year character varying(255),
    phd_from character varying(255),
    interests text,
    phone character varying(255),
    rank character varying(255),
    hours text,
    webpage character varying(255) DEFAULT '1'::character varying,
    persistence_token character varying(255),
    password_salt character varying(255),
    failed_login_count integer DEFAULT 0 NOT NULL,
    state integer DEFAULT 0 NOT NULL,
    first_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    last_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    slug character varying,
    oh_location character varying,
    start date,
    finish date,
    deceased date,
    last_request_at timestamp without time zone,
    current_login_at timestamp without time zone,
    last_login_at timestamp without time zone,
    current_login_ip character varying,
    last_login_ip character varying,
    oh_expiry date,
    photo_id bigint,
    orcid character varying,
    gs character varying,
    scopus character varying,
    arxiv character varying,
    wos character varying,
    api_key character varying,
    mathscinet character varying,
    mail_domain character varying,
    pure uuid
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: users_roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users_roles (
    user_id integer,
    role_id integer,
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2020-01-11 15:58:26.40419'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2020-01-11 15:58:26.434101'::timestamp without time zone NOT NULL
);


--
-- Name: users_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_roles_id_seq OWNED BY public.users_roles.id;


--
-- Name: version_associations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.version_associations (
    id bigint NOT NULL,
    version_id integer,
    foreign_key_name character varying NOT NULL,
    foreign_key_id integer,
    foreign_type character varying NOT NULL
);


--
-- Name: version_associations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.version_associations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: version_associations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.version_associations_id_seq OWNED BY public.version_associations.id;


--
-- Name: versions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.versions (
    id integer NOT NULL,
    item_type character varying(255) NOT NULL,
    item_id character varying NOT NULL,
    event character varying(255) NOT NULL,
    whodunnit character varying(255),
    created_at timestamp without time zone,
    message text,
    object jsonb,
    undone_at timestamp without time zone,
    object_changes jsonb,
    item_subtype character varying,
    transaction_id integer
);


--
-- Name: versions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.versions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.versions_id_seq OWNED BY public.versions.id;


--
-- Name: visitors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.visitors (
    id integer NOT NULL,
    email character varying,
    first_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    last_i18n jsonb DEFAULT '{}'::jsonb NOT NULL,
    office character varying,
    phone character varying,
    rank character varying,
    webpage character varying,
    origin character varying,
    centre boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    duration daterange NOT NULL
);


--
-- Name: visitors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.visitors_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: visitors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.visitors_id_seq OWNED BY public.visitors.id;


--
-- Name: ac_years id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ac_years ALTER COLUMN id SET DEFAULT nextval('public.ac_years_id_seq'::regclass);


--
-- Name: active_storage_attachments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_attachments ALTER COLUMN id SET DEFAULT nextval('public.active_storage_attachments_id_seq'::regclass);


--
-- Name: active_storage_blobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_blobs ALTER COLUMN id SET DEFAULT nextval('public.active_storage_blobs_id_seq'::regclass);


--
-- Name: active_storage_variant_records id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_variant_records ALTER COLUMN id SET DEFAULT nextval('public.active_storage_variant_records_id_seq'::regclass);


--
-- Name: allows_requires id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.allows_requires ALTER COLUMN id SET DEFAULT nextval('public.allows_requires_id_seq'::regclass);


--
-- Name: app_setups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.app_setups ALTER COLUMN id SET DEFAULT nextval('public.app_setups_id_seq'::regclass);


--
-- Name: basic_prog_prereq_infos id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.basic_prog_prereq_infos ALTER COLUMN id SET DEFAULT nextval('public.basic_prog_prereq_infos_id_seq'::regclass);


--
-- Name: carousel_item_records id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.carousel_item_records ALTER COLUMN id SET DEFAULT nextval('public.carousel_item_records_id_seq'::regclass);


--
-- Name: carousel_items id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.carousel_items ALTER COLUMN id SET DEFAULT nextval('public.carousel_items_id_seq'::regclass);


--
-- Name: chair_role_setups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.chair_role_setups ALTER COLUMN id SET DEFAULT nextval('public.chair_role_setups_id_seq'::regclass);


--
-- Name: course_lists id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.course_lists ALTER COLUMN id SET DEFAULT nextval('public.course_lists_id_seq'::regclass);


--
-- Name: course_lists_courses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.course_lists_courses ALTER COLUMN id SET DEFAULT nextval('public.course_lists_courses_id_seq'::regclass);


--
-- Name: course_lists_prog_prereqs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.course_lists_prog_prereqs ALTER COLUMN id SET DEFAULT nextval('public.course_lists_prog_prereqs_id_seq'::regclass);


--
-- Name: courses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.courses ALTER COLUMN id SET DEFAULT nextval('public.courses_id_seq'::regclass);


--
-- Name: degree_plans id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.degree_plans ALTER COLUMN id SET DEFAULT nextval('public.degree_plans_id_seq'::regclass);


--
-- Name: degrees id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.degrees ALTER COLUMN id SET DEFAULT nextval('public.degrees_id_seq'::regclass);


--
-- Name: departments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.departments ALTER COLUMN id SET DEFAULT nextval('public.departments_id_seq'::regclass);


--
-- Name: departments_generic_courses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.departments_generic_courses ALTER COLUMN id SET DEFAULT nextval('public.departments_generic_courses_id_seq'::regclass);


--
-- Name: events id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.events ALTER COLUMN id SET DEFAULT nextval('public.events_id_seq'::regclass);


--
-- Name: free_plan_items id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.free_plan_items ALTER COLUMN id SET DEFAULT nextval('public.free_plan_items_id_seq'::regclass);


--
-- Name: friendly_id_slugs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.friendly_id_slugs ALTER COLUMN id SET DEFAULT nextval('public.friendly_id_slugs_id_seq'::regclass);


--
-- Name: generic_courses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.generic_courses ALTER COLUMN id SET DEFAULT nextval('public.generic_courses_id_seq'::regclass);


--
-- Name: hosts_postdocs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hosts_postdocs ALTER COLUMN id SET DEFAULT nextval('public.hosts_postdocs_id_seq'::regclass);


--
-- Name: hosts_visitors id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hosts_visitors ALTER COLUMN id SET DEFAULT nextval('public.hosts_visitors_id_seq'::regclass);


--
-- Name: impressions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.impressions ALTER COLUMN id SET DEFAULT nextval('public.impressions_id_seq'::regclass);


--
-- Name: issue_responses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issue_responses ALTER COLUMN id SET DEFAULT nextval('public.issue_responses_id_seq'::regclass);


--
-- Name: issues_issues_staffs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_issues_staffs ALTER COLUMN id SET DEFAULT nextval('public.issues_issues_staffs_id_seq'::regclass);


--
-- Name: issues_role_setups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_role_setups ALTER COLUMN id SET DEFAULT nextval('public.issues_role_setups_id_seq'::regclass);


--
-- Name: issues_users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_users ALTER COLUMN id SET DEFAULT nextval('public.issues_users_id_seq'::regclass);


--
-- Name: leaves id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.leaves ALTER COLUMN id SET DEFAULT nextval('public.leaves_id_seq'::regclass);


--
-- Name: meetings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.meetings ALTER COLUMN id SET DEFAULT nextval('public.meetings_id_seq'::regclass);


--
-- Name: members_research_groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.members_research_groups ALTER COLUMN id SET DEFAULT nextval('public.members_research_groups_id_seq'::regclass);


--
-- Name: old_users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.old_users ALTER COLUMN id SET DEFAULT nextval('public.old_users_id_seq'::regclass);


--
-- Name: photos id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photos ALTER COLUMN id SET DEFAULT nextval('public.photos_id_seq'::regclass);


--
-- Name: plan_items id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plan_items ALTER COLUMN id SET DEFAULT nextval('public.plan_items_id_seq'::regclass);


--
-- Name: precise_prog_prereq_infos id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.precise_prog_prereq_infos ALTER COLUMN id SET DEFAULT nextval('public.precise_prog_prereq_infos_id_seq'::regclass);


--
-- Name: prog_prereqs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prog_prereqs ALTER COLUMN id SET DEFAULT nextval('public.prog_prereqs_id_seq'::regclass);


--
-- Name: programs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.programs ALTER COLUMN id SET DEFAULT nextval('public.programs_id_seq'::regclass);


--
-- Name: que_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.que_jobs ALTER COLUMN id SET DEFAULT nextval('public.que_jobs_id_seq'::regclass);


--
-- Name: replaced_by_replaces id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.replaced_by_replaces ALTER COLUMN id SET DEFAULT nextval('public.replaced_by_replaces_id_seq'::regclass);


--
-- Name: research_groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.research_groups ALTER COLUMN id SET DEFAULT nextval('public.research_groups_id_seq'::regclass);


--
-- Name: research_groups_seminars id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.research_groups_seminars ALTER COLUMN id SET DEFAULT nextval('public.research_groups_seminars_id_seq'::regclass);


--
-- Name: role_events id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_events ALTER COLUMN id SET DEFAULT nextval('public.role_events_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: seminars id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seminars ALTER COLUMN id SET DEFAULT nextval('public.seminars_id_seq'::regclass);


--
-- Name: sessions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sessions ALTER COLUMN id SET DEFAULT nextval('public.sessions_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: shnatons id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.shnatons ALTER COLUMN id SET DEFAULT nextval('public.shnatons_id_seq'::regclass);


--
-- Name: staff_role_setups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.staff_role_setups ALTER COLUMN id SET DEFAULT nextval('public.staff_role_setups_id_seq'::regclass);


--
-- Name: students_supervisors id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.students_supervisors ALTER COLUMN id SET DEFAULT nextval('public.students_supervisors_id_seq'::regclass);


--
-- Name: templates id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.templates ALTER COLUMN id SET DEFAULT nextval('public.templates_id_seq'::regclass);


--
-- Name: term_plans id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.term_plans ALTER COLUMN id SET DEFAULT nextval('public.term_plans_id_seq'::regclass);


--
-- Name: terms id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.terms ALTER COLUMN id SET DEFAULT nextval('public.terms_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: users_roles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_roles ALTER COLUMN id SET DEFAULT nextval('public.users_roles_id_seq'::regclass);


--
-- Name: version_associations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.version_associations ALTER COLUMN id SET DEFAULT nextval('public.version_associations_id_seq'::regclass);


--
-- Name: versions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.versions ALTER COLUMN id SET DEFAULT nextval('public.versions_id_seq'::regclass);


--
-- Name: visitors id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.visitors ALTER COLUMN id SET DEFAULT nextval('public.visitors_id_seq'::regclass);


--
-- Name: ac_years ac_years_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ac_years
    ADD CONSTRAINT ac_years_pkey PRIMARY KEY (id);


--
-- Name: active_storage_attachments active_storage_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_attachments
    ADD CONSTRAINT active_storage_attachments_pkey PRIMARY KEY (id);


--
-- Name: active_storage_blobs active_storage_blobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_blobs
    ADD CONSTRAINT active_storage_blobs_pkey PRIMARY KEY (id);


--
-- Name: active_storage_variant_records active_storage_variant_records_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_variant_records
    ADD CONSTRAINT active_storage_variant_records_pkey PRIMARY KEY (id);


--
-- Name: allows_requires allows_requires_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.allows_requires
    ADD CONSTRAINT allows_requires_pkey PRIMARY KEY (id);


--
-- Name: app_setups app_setups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.app_setups
    ADD CONSTRAINT app_setups_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: basic_prog_prereq_infos basic_prog_prereq_infos_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.basic_prog_prereq_infos
    ADD CONSTRAINT basic_prog_prereq_infos_pkey PRIMARY KEY (id);


--
-- Name: carousel_item_records carousel_item_records_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.carousel_item_records
    ADD CONSTRAINT carousel_item_records_pkey PRIMARY KEY (id);


--
-- Name: carousel_items carousel_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.carousel_items
    ADD CONSTRAINT carousel_items_pkey PRIMARY KEY (id);


--
-- Name: chair_role_setups chair_role_setups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.chair_role_setups
    ADD CONSTRAINT chair_role_setups_pkey PRIMARY KEY (id);


--
-- Name: course_lists_courses course_lists_courses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.course_lists_courses
    ADD CONSTRAINT course_lists_courses_pkey PRIMARY KEY (id);


--
-- Name: course_lists course_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.course_lists
    ADD CONSTRAINT course_lists_pkey PRIMARY KEY (id);


--
-- Name: course_lists_prog_prereqs course_lists_prog_prereqs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.course_lists_prog_prereqs
    ADD CONSTRAINT course_lists_prog_prereqs_pkey PRIMARY KEY (id);


--
-- Name: courses courses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (id);


--
-- Name: degree_plans degree_plans_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.degree_plans
    ADD CONSTRAINT degree_plans_pkey PRIMARY KEY (id);


--
-- Name: degrees degrees_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.degrees
    ADD CONSTRAINT degrees_pkey PRIMARY KEY (id);


--
-- Name: departments_generic_courses departments_generic_courses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.departments_generic_courses
    ADD CONSTRAINT departments_generic_courses_pkey PRIMARY KEY (id);


--
-- Name: departments departments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.departments
    ADD CONSTRAINT departments_pkey PRIMARY KEY (id);


--
-- Name: events events_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: free_plan_items free_plan_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.free_plan_items
    ADD CONSTRAINT free_plan_items_pkey PRIMARY KEY (id);


--
-- Name: friendly_id_slugs friendly_id_slugs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.friendly_id_slugs
    ADD CONSTRAINT friendly_id_slugs_pkey PRIMARY KEY (id);


--
-- Name: generic_courses generic_courses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.generic_courses
    ADD CONSTRAINT generic_courses_pkey PRIMARY KEY (id);


--
-- Name: hosts_postdocs hosts_postdocs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hosts_postdocs
    ADD CONSTRAINT hosts_postdocs_pkey PRIMARY KEY (id);


--
-- Name: hosts_visitors hosts_visitors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hosts_visitors
    ADD CONSTRAINT hosts_visitors_pkey PRIMARY KEY (id);


--
-- Name: impressions impressions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.impressions
    ADD CONSTRAINT impressions_pkey PRIMARY KEY (id);


--
-- Name: issue_responses issue_responses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issue_responses
    ADD CONSTRAINT issue_responses_pkey PRIMARY KEY (id);


--
-- Name: issues_issues_staffs issues_issues_staffs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_issues_staffs
    ADD CONSTRAINT issues_issues_staffs_pkey PRIMARY KEY (id);


--
-- Name: issues issues_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT issues_pkey PRIMARY KEY (id);


--
-- Name: issues_role_setups issues_role_setups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_role_setups
    ADD CONSTRAINT issues_role_setups_pkey PRIMARY KEY (id);


--
-- Name: issues_users issues_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_users
    ADD CONSTRAINT issues_users_pkey PRIMARY KEY (id);


--
-- Name: leaves leaves_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.leaves
    ADD CONSTRAINT leaves_pkey PRIMARY KEY (id);


--
-- Name: meetings meetings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.meetings
    ADD CONSTRAINT meetings_pkey PRIMARY KEY (id);


--
-- Name: members_research_groups members_research_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.members_research_groups
    ADD CONSTRAINT members_research_groups_pkey PRIMARY KEY (id);


--
-- Name: old_users old_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.old_users
    ADD CONSTRAINT old_users_pkey PRIMARY KEY (id);


--
-- Name: photos photos_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photos
    ADD CONSTRAINT photos_pkey PRIMARY KEY (id);


--
-- Name: plan_items plan_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plan_items
    ADD CONSTRAINT plan_items_pkey PRIMARY KEY (id);


--
-- Name: precise_prog_prereq_infos precise_prog_prereq_infos_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.precise_prog_prereq_infos
    ADD CONSTRAINT precise_prog_prereq_infos_pkey PRIMARY KEY (id);


--
-- Name: prog_prereqs prog_prereqs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prog_prereqs
    ADD CONSTRAINT prog_prereqs_pkey PRIMARY KEY (id);


--
-- Name: programs programs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.programs
    ADD CONSTRAINT programs_pkey PRIMARY KEY (id);


--
-- Name: que_jobs que_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.que_jobs
    ADD CONSTRAINT que_jobs_pkey PRIMARY KEY (id);


--
-- Name: que_lockers que_lockers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.que_lockers
    ADD CONSTRAINT que_lockers_pkey PRIMARY KEY (pid);


--
-- Name: que_values que_values_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.que_values
    ADD CONSTRAINT que_values_pkey PRIMARY KEY (key);


--
-- Name: replaced_by_replaces replaced_by_replaces_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.replaced_by_replaces
    ADD CONSTRAINT replaced_by_replaces_pkey PRIMARY KEY (id);


--
-- Name: research_groups research_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.research_groups
    ADD CONSTRAINT research_groups_pkey PRIMARY KEY (id);


--
-- Name: research_groups_seminars research_groups_seminars_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.research_groups_seminars
    ADD CONSTRAINT research_groups_seminars_pkey PRIMARY KEY (id);


--
-- Name: role_events role_events_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_events
    ADD CONSTRAINT role_events_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: seminars seminars_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seminars
    ADD CONSTRAINT seminars_pkey PRIMARY KEY (id);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: shnatons shnatons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.shnatons
    ADD CONSTRAINT shnatons_pkey PRIMARY KEY (id);


--
-- Name: staff_role_setups staff_role_setups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.staff_role_setups
    ADD CONSTRAINT staff_role_setups_pkey PRIMARY KEY (id);


--
-- Name: students_supervisors students_supervisors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.students_supervisors
    ADD CONSTRAINT students_supervisors_pkey PRIMARY KEY (id);


--
-- Name: templates templates_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.templates
    ADD CONSTRAINT templates_pkey PRIMARY KEY (id);


--
-- Name: term_plans term_plans_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.term_plans
    ADD CONSTRAINT term_plans_pkey PRIMARY KEY (id);


--
-- Name: terms terms_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.terms
    ADD CONSTRAINT terms_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_roles users_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_roles
    ADD CONSTRAINT users_roles_pkey PRIMARY KEY (id);


--
-- Name: version_associations version_associations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.version_associations
    ADD CONSTRAINT version_associations_pkey PRIMARY KEY (id);


--
-- Name: versions versions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.versions
    ADD CONSTRAINT versions_pkey PRIMARY KEY (id);


--
-- Name: visitors visitors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.visitors
    ADD CONSTRAINT visitors_pkey PRIMARY KEY (id);


--
-- Name: controlleraction_ip_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX controlleraction_ip_index ON public.impressions USING btree (controller_name, action_name, ip_address);


--
-- Name: controlleraction_request_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX controlleraction_request_index ON public.impressions USING btree (controller_name, action_name, request_hash);


--
-- Name: controlleraction_session_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX controlleraction_session_index ON public.impressions USING btree (controller_name, action_name, session_hash);


--
-- Name: hosts_visitors_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX hosts_visitors_index ON public.hosts_visitors USING btree (host_id, visitor_id);


--
-- Name: impressionable_type_message_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX impressionable_type_message_index ON public.impressions USING btree (impressionable_type, message, impressionable_id);


--
-- Name: index_ac_years_on_year; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_ac_years_on_year ON public.ac_years USING btree (year);


--
-- Name: index_active_storage_attachments_on_blob_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_active_storage_attachments_on_blob_id ON public.active_storage_attachments USING btree (blob_id);


--
-- Name: index_active_storage_attachments_uniqueness; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_active_storage_attachments_uniqueness ON public.active_storage_attachments USING btree (record_type, record_id, name, blob_id);


--
-- Name: index_active_storage_blobs_on_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_active_storage_blobs_on_key ON public.active_storage_blobs USING btree (key);


--
-- Name: index_active_storage_variant_records_uniqueness; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_active_storage_variant_records_uniqueness ON public.active_storage_variant_records USING btree (blob_id, variation_digest);


--
-- Name: index_admins_seminars_on_admin_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_admins_seminars_on_admin_id ON public.admins_seminars USING btree (admin_id);


--
-- Name: index_admins_seminars_on_seminar_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_admins_seminars_on_seminar_id ON public.admins_seminars USING btree (seminar_id);


--
-- Name: index_allows_requires_on_allow_id_and_require_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_allows_requires_on_allow_id_and_require_id ON public.allows_requires USING btree (allow_id, require_id);


--
-- Name: index_allows_requires_on_require_id_and_allow_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_allows_requires_on_require_id_and_allow_id ON public.allows_requires USING btree (require_id, allow_id);


--
-- Name: index_basic_prog_prereq_infos_on_prereq_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_basic_prog_prereq_infos_on_prereq_id ON public.basic_prog_prereq_infos USING btree (prereq_id);


--
-- Name: index_carousel_item_records_on_item_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_carousel_item_records_on_item_id ON public.carousel_item_records USING btree (item_id);


--
-- Name: index_carousel_item_records_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_carousel_item_records_on_resource_type_and_resource_id ON public.carousel_item_records USING btree (resource_type, resource_id);


--
-- Name: index_chair_role_setups_on_app_setup_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_chair_role_setups_on_app_setup_id ON public.chair_role_setups USING btree (app_setup_id);


--
-- Name: index_chair_role_setups_on_role_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_chair_role_setups_on_role_id ON public.chair_role_setups USING btree (role_id);


--
-- Name: index_course_lists_courses_on_course_id_and_course_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_course_lists_courses_on_course_id_and_course_list_id ON public.course_lists_courses USING btree (course_id, course_list_id);


--
-- Name: index_course_lists_courses_on_course_list_id_and_course_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_course_lists_courses_on_course_list_id_and_course_id ON public.course_lists_courses USING btree (course_list_id, course_id);


--
-- Name: index_course_lists_on_name_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_course_lists_on_name_i18n ON public.course_lists USING btree (name_i18n);


--
-- Name: index_course_lists_on_remarks_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_course_lists_on_remarks_i18n ON public.course_lists USING btree (remarks_i18n);


--
-- Name: index_course_lists_on_shnaton_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_course_lists_on_shnaton_id ON public.course_lists USING btree (shnaton_id);


--
-- Name: index_course_lists_on_shnaton_id_and_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_course_lists_on_shnaton_id_and_slug ON public.course_lists USING btree (shnaton_id, slug);


--
-- Name: index_course_lists_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_course_lists_on_slug ON public.course_lists USING btree (slug);


--
-- Name: index_course_lists_prog_prereqs_on_prereq_id_and_course_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_course_lists_prog_prereqs_on_prereq_id_and_course_list_id ON public.course_lists_prog_prereqs USING btree (prereq_id, course_list_id);


--
-- Name: index_courses_issues_users_on_course_id_and_issues_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_courses_issues_users_on_course_id_and_issues_user_id ON public.courses_issues_users USING btree (course_id, issues_user_id);


--
-- Name: index_courses_issues_users_on_issues_user_id_and_course_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_courses_issues_users_on_issues_user_id_and_course_id ON public.courses_issues_users USING btree (issues_user_id, course_id);


--
-- Name: index_courses_on_abstract_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_courses_on_abstract_i18n ON public.courses USING btree (abstract_i18n);


--
-- Name: index_courses_on_aguda_rep_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_courses_on_aguda_rep_id ON public.courses USING btree (aguda_rep_id);


--
-- Name: index_courses_on_content_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_courses_on_content_i18n ON public.courses USING btree (content_i18n);


--
-- Name: index_courses_on_generic_course_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_courses_on_generic_course_id ON public.courses USING btree (generic_course_id);


--
-- Name: index_courses_on_generic_course_id_and_term_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_courses_on_generic_course_id_and_term_id ON public.courses USING btree (generic_course_id, term_id);


--
-- Name: index_courses_on_lecturer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_courses_on_lecturer_id ON public.courses USING btree (lecturer_id);


--
-- Name: index_courses_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_courses_on_slug ON public.courses USING btree (slug);


--
-- Name: index_courses_on_student_rep_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_courses_on_student_rep_id ON public.courses USING btree (student_rep_id);


--
-- Name: index_courses_on_term_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_courses_on_term_id ON public.courses USING btree (term_id);


--
-- Name: index_courses_on_term_id_and_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_courses_on_term_id_and_slug ON public.courses USING btree (term_id, slug);


--
-- Name: index_courses_on_title_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_courses_on_title_i18n ON public.courses USING btree (title_i18n);


--
-- Name: index_degree_plans_on_degree_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_degree_plans_on_degree_id ON public.degree_plans USING btree (degree_id);


--
-- Name: index_degree_plans_on_degree_id_and_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_degree_plans_on_degree_id_and_slug ON public.degree_plans USING btree (degree_id, slug);


--
-- Name: index_degree_plans_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_degree_plans_on_slug ON public.degree_plans USING btree (slug);


--
-- Name: index_degrees_on_name_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_degrees_on_name_i18n ON public.degrees USING btree (name_i18n);


--
-- Name: index_degrees_on_shnaton_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_degrees_on_shnaton_id ON public.degrees USING btree (shnaton_id);


--
-- Name: index_degrees_on_shnaton_id_and_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_degrees_on_shnaton_id_and_slug ON public.degrees USING btree (shnaton_id, slug);


--
-- Name: index_degrees_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_degrees_on_slug ON public.degrees USING btree (slug);


--
-- Name: index_departments_generic_courses_on_course_id_and_dept_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_departments_generic_courses_on_course_id_and_dept_id ON public.departments_generic_courses USING btree (generic_course_id, department_id);


--
-- Name: index_departments_generic_courses_on_dept_id_and_course_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_departments_generic_courses_on_dept_id_and_course_id ON public.departments_generic_courses USING btree (department_id, generic_course_id);


--
-- Name: index_departments_on_bgu_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_departments_on_bgu_name ON public.departments USING btree (bgu_name);


--
-- Name: index_departments_on_catalog_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_departments_on_catalog_id ON public.departments USING btree (catalog_id);


--
-- Name: index_departments_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_departments_on_slug ON public.departments USING btree (slug);


--
-- Name: index_events_on_duration; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_events_on_duration ON public.events USING gist (duration);


--
-- Name: index_events_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_events_on_slug ON public.events USING btree (slug);


--
-- Name: index_free_plan_items_on_term_plan_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_free_plan_items_on_term_plan_id ON public.free_plan_items USING btree (term_plan_id);


--
-- Name: index_friendly_id_slugs_on_slug_and_sluggable_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_friendly_id_slugs_on_slug_and_sluggable_type ON public.friendly_id_slugs USING btree (slug, sluggable_type);


--
-- Name: index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope ON public.friendly_id_slugs USING btree (slug, sluggable_type, scope);


--
-- Name: index_friendly_id_slugs_on_sluggable_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_friendly_id_slugs_on_sluggable_id ON public.friendly_id_slugs USING btree (sluggable_id);


--
-- Name: index_friendly_id_slugs_on_sluggable_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_friendly_id_slugs_on_sluggable_type ON public.friendly_id_slugs USING btree (sluggable_type);


--
-- Name: index_generic_courses_on_aguda_rep_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_generic_courses_on_aguda_rep_id ON public.generic_courses USING btree (aguda_rep_id);


--
-- Name: index_generic_courses_on_name_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_generic_courses_on_name_i18n ON public.generic_courses USING btree (name_i18n);


--
-- Name: index_generic_courses_on_shid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_generic_courses_on_shid ON public.generic_courses USING btree (shid) WHERE (year_removed_id IS NULL);


--
-- Name: index_generic_courses_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_generic_courses_on_slug ON public.generic_courses USING btree (slug);


--
-- Name: index_generic_courses_on_year_added_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_generic_courses_on_year_added_id ON public.generic_courses USING btree (year_added_id);


--
-- Name: index_generic_courses_on_year_removed_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_generic_courses_on_year_removed_id ON public.generic_courses USING btree (year_removed_id);


--
-- Name: index_hosts_postdocs_on_postdoc_id_and_host_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_hosts_postdocs_on_postdoc_id_and_host_id ON public.hosts_postdocs USING btree (postdoc_id, host_id);


--
-- Name: index_impressions_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_impressions_on_user_id ON public.impressions USING btree (user_id);


--
-- Name: index_issue_responses_on_creator_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issue_responses_on_creator_id ON public.issue_responses USING btree (creator_id);


--
-- Name: index_issue_responses_on_issue_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_issue_responses_on_issue_id ON public.issue_responses USING btree (issue_id);


--
-- Name: index_issues_issues_staffs_on_issue_id_and_staff_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_issues_staffs_on_issue_id_and_staff_id ON public.issues_issues_staffs USING btree (issue_id, staff_id);


--
-- Name: index_issues_issues_staffs_on_staff_id_and_issue_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_issues_staffs_on_staff_id_and_issue_id ON public.issues_issues_staffs USING btree (staff_id, issue_id);


--
-- Name: index_issues_on_aguda_rep_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_on_aguda_rep_id ON public.issues USING btree (aguda_rep_id);


--
-- Name: index_issues_on_course_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_on_course_id ON public.issues USING btree (course_id);


--
-- Name: index_issues_on_replier_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_on_replier_id ON public.issues USING btree (replier_id);


--
-- Name: index_issues_on_reporter_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_on_reporter_id ON public.issues USING btree (reporter_id);


--
-- Name: index_issues_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_issues_on_slug ON public.issues USING btree (slug);


--
-- Name: index_issues_role_setups_on_app_setup_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_role_setups_on_app_setup_id ON public.issues_role_setups USING btree (app_setup_id);


--
-- Name: index_issues_role_setups_on_role_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_role_setups_on_role_id ON public.issues_role_setups USING btree (role_id);


--
-- Name: index_issues_users_on_api_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_issues_users_on_api_key ON public.issues_users USING btree (api_key);


--
-- Name: index_issues_users_on_department_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_users_on_department_id ON public.issues_users USING btree (department_id);


--
-- Name: index_issues_users_on_login; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_issues_users_on_login ON public.issues_users USING btree (login);


--
-- Name: index_leaves_on_duration; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_leaves_on_duration ON public.leaves USING btree (duration);


--
-- Name: index_leaves_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_leaves_on_user_id ON public.leaves USING btree (user_id);


--
-- Name: index_meetings_on_duration; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_meetings_on_duration ON public.meetings USING gist (duration);


--
-- Name: index_meetings_on_seminar_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_meetings_on_seminar_id ON public.meetings USING btree (seminar_id);


--
-- Name: index_meetings_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_meetings_on_slug ON public.meetings USING btree (slug);


--
-- Name: index_members_research_groups_on_member_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_members_research_groups_on_member_id ON public.members_research_groups USING btree (member_id);


--
-- Name: index_members_research_groups_on_research_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_members_research_groups_on_research_group_id ON public.members_research_groups USING btree (research_group_id);


--
-- Name: index_old_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_old_users_on_email ON public.old_users USING btree (email);


--
-- Name: index_old_users_on_first_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_old_users_on_first_i18n ON public.old_users USING btree (first_i18n);


--
-- Name: index_old_users_on_last_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_old_users_on_last_i18n ON public.old_users USING btree (last_i18n);


--
-- Name: index_old_users_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_old_users_on_slug ON public.old_users USING btree (slug);


--
-- Name: index_photos_on_imageable_type_and_imageable_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_photos_on_imageable_type_and_imageable_id ON public.photos USING btree (imageable_type, imageable_id);


--
-- Name: index_plan_items_on_generic_course_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_plan_items_on_generic_course_id ON public.plan_items USING btree (generic_course_id);


--
-- Name: index_plan_items_on_term_plan_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_plan_items_on_term_plan_id ON public.plan_items USING btree (term_plan_id);


--
-- Name: index_precise_prog_prereq_infos_on_prereq_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_precise_prog_prereq_infos_on_prereq_id ON public.precise_prog_prereq_infos USING btree (prereq_id);


--
-- Name: index_prog_prereqs_on_degree_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_prog_prereqs_on_degree_id ON public.prog_prereqs USING btree (degree_id);


--
-- Name: index_prog_prereqs_on_name_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_prog_prereqs_on_name_i18n ON public.prog_prereqs USING btree (name_i18n);


--
-- Name: index_prog_prereqs_on_parent_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_prog_prereqs_on_parent_id ON public.prog_prereqs USING btree (parent_id);


--
-- Name: index_prog_prereqs_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_prog_prereqs_on_slug ON public.prog_prereqs USING btree (slug);


--
-- Name: index_prog_prereqs_on_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_prog_prereqs_on_type ON public.prog_prereqs USING btree (type);


--
-- Name: index_programs_on_name_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_programs_on_name_i18n ON public.programs USING btree (name_i18n);


--
-- Name: index_programs_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_programs_on_slug ON public.programs USING btree (slug);


--
-- Name: index_replaced_by_replaces_on_replace_id_and_replaced_by_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_replaced_by_replaces_on_replace_id_and_replaced_by_id ON public.replaced_by_replaces USING btree (replace_id, replaced_by_id);


--
-- Name: index_replaced_by_replaces_on_replaced_by_id_and_replace_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_replaced_by_replaces_on_replaced_by_id_and_replace_id ON public.replaced_by_replaces USING btree (replaced_by_id, replace_id);


--
-- Name: index_research_groups_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_research_groups_on_name ON public.research_groups USING btree (name);


--
-- Name: index_research_groups_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_research_groups_on_slug ON public.research_groups USING btree (slug);


--
-- Name: index_role_events_on_creator_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_role_events_on_creator_id ON public.role_events USING btree (creator_id);


--
-- Name: index_role_events_on_role_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_role_events_on_role_id ON public.role_events USING btree (role_id);


--
-- Name: index_role_events_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_role_events_on_user_id ON public.role_events USING btree (user_id);


--
-- Name: index_roles_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_roles_on_name ON public.roles USING btree (name);


--
-- Name: index_roles_on_name_and_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_roles_on_name_and_resource_type_and_resource_id ON public.roles USING btree (name, resource_type, resource_id);


--
-- Name: index_roles_on_title_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_roles_on_title_i18n ON public.roles USING btree (title_i18n);


--
-- Name: index_seminar_id_research_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_seminar_id_research_group_id ON public.research_groups_seminars USING btree (seminar_id, research_group_id);


--
-- Name: index_seminars_on_duration; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seminars_on_duration ON public.seminars USING gist (duration);


--
-- Name: index_seminars_on_list_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seminars_on_list_id ON public.seminars USING btree (list_id);


--
-- Name: index_seminars_on_name_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seminars_on_name_i18n ON public.seminars USING btree (name_i18n);


--
-- Name: index_seminars_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seminars_on_slug ON public.seminars USING btree (slug);


--
-- Name: index_seminars_on_term_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_seminars_on_term_id ON public.seminars USING btree (term_id);


--
-- Name: index_seminars_on_term_id_and_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_seminars_on_term_id_and_slug ON public.seminars USING btree (term_id, slug);


--
-- Name: index_sessions_on_session_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_sessions_on_session_id ON public.sessions USING btree (session_id);


--
-- Name: index_sessions_on_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_sessions_on_updated_at ON public.sessions USING btree (updated_at);


--
-- Name: index_settings_on_thing_type_and_thing_id_and_var; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_settings_on_thing_type_and_thing_id_and_var ON public.settings USING btree (thing_type, thing_id, var);


--
-- Name: index_shnatons_on_ac_year_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_shnatons_on_ac_year_id ON public.shnatons USING btree (ac_year_id);


--
-- Name: index_shnatons_on_preamble_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_shnatons_on_preamble_i18n ON public.shnatons USING btree (preamble_i18n);


--
-- Name: index_staff_role_setups_on_app_setup_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_staff_role_setups_on_app_setup_id ON public.staff_role_setups USING btree (app_setup_id);


--
-- Name: index_staff_role_setups_on_role_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_staff_role_setups_on_role_id ON public.staff_role_setups USING btree (role_id);


--
-- Name: index_students_supervisors_on_student_id_and_supervisor_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_students_supervisors_on_student_id_and_supervisor_id ON public.students_supervisors USING btree (student_id, supervisor_id);


--
-- Name: index_templates_on_object_type_and_object_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_templates_on_object_type_and_object_id ON public.templates USING btree (object_type, object_id);


--
-- Name: index_templates_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_templates_on_slug ON public.templates USING btree (slug);


--
-- Name: index_term_plans_on_degree_plan_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_term_plans_on_degree_plan_id ON public.term_plans USING btree (degree_plan_id);


--
-- Name: index_terms_on_name_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_terms_on_name_i18n ON public.terms USING btree (name_i18n);


--
-- Name: index_terms_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_terms_on_slug ON public.terms USING btree (slug);


--
-- Name: index_users_on_api_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_api_key ON public.users USING btree (api_key);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_first_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_first_i18n ON public.users USING btree (first_i18n);


--
-- Name: index_users_on_last_i18n; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_last_i18n ON public.users USING btree (last_i18n);


--
-- Name: index_users_on_photo_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_photo_id ON public.users USING btree (photo_id);


--
-- Name: index_users_on_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_slug ON public.users USING btree (slug);


--
-- Name: index_users_roles_on_user_id_and_role_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_roles_on_user_id_and_role_id ON public.users_roles USING btree (user_id, role_id);


--
-- Name: index_version_associations_on_foreign_key; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_version_associations_on_foreign_key ON public.version_associations USING btree (foreign_key_name, foreign_key_id, foreign_type);


--
-- Name: index_version_associations_on_version_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_version_associations_on_version_id ON public.version_associations USING btree (version_id);


--
-- Name: index_versions_on_item_type_and_item_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_versions_on_item_type_and_item_id ON public.versions USING btree (item_type, item_id);


--
-- Name: index_versions_on_transaction_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_versions_on_transaction_id ON public.versions USING btree (transaction_id);


--
-- Name: index_visitors_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_visitors_on_email ON public.visitors USING btree (email);


--
-- Name: members_research_groups_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX members_research_groups_index ON public.members_research_groups USING btree (member_id, research_group_id);


--
-- Name: poly_ip_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX poly_ip_index ON public.impressions USING btree (impressionable_type, impressionable_id, ip_address);


--
-- Name: poly_params_request_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX poly_params_request_index ON public.impressions USING btree (impressionable_type, impressionable_id, params);


--
-- Name: poly_request_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX poly_request_index ON public.impressions USING btree (impressionable_type, impressionable_id, request_hash);


--
-- Name: poly_session_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX poly_session_index ON public.impressions USING btree (impressionable_type, impressionable_id, session_hash);


--
-- Name: programs_coordinators_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX programs_coordinators_index ON public.programs_coordinators USING btree (program_id, coordinator_id);


--
-- Name: que_jobs_args_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX que_jobs_args_gin_idx ON public.que_jobs USING gin (args jsonb_path_ops);


--
-- Name: que_jobs_data_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX que_jobs_data_gin_idx ON public.que_jobs USING gin (data jsonb_path_ops);


--
-- Name: que_jobs_kwargs_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX que_jobs_kwargs_gin_idx ON public.que_jobs USING gin (kwargs jsonb_path_ops);


--
-- Name: que_poll_idx_with_job_schema_version; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX que_poll_idx_with_job_schema_version ON public.que_jobs USING btree (job_schema_version, queue, priority, run_at, id) WHERE ((finished_at IS NULL) AND (expired_at IS NULL));


--
-- Name: research_groups_seminars_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX research_groups_seminars_index ON public.research_groups_seminars USING btree (seminar_id, research_group_id);


--
-- Name: templates_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX templates_index ON public.templates USING btree (path, locale, format, handler, partial);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_schema_migrations ON public.schema_migrations USING btree (version);


--
-- Name: que_jobs que_job_notify; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER que_job_notify AFTER INSERT ON public.que_jobs FOR EACH ROW EXECUTE FUNCTION public.que_job_notify();


--
-- Name: que_jobs que_state_notify; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER que_state_notify AFTER INSERT OR DELETE OR UPDATE ON public.que_jobs FOR EACH ROW EXECUTE FUNCTION public.que_state_notify();


--
-- Name: free_plan_items fk_rails_05b08c039d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.free_plan_items
    ADD CONSTRAINT fk_rails_05b08c039d FOREIGN KEY (term_plan_id) REFERENCES public.term_plans(id);


--
-- Name: users fk_rails_0c23dac8aa; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_rails_0c23dac8aa FOREIGN KEY (photo_id) REFERENCES public.photos(id);


--
-- Name: plan_items fk_rails_251e7a2ba1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plan_items
    ADD CONSTRAINT fk_rails_251e7a2ba1 FOREIGN KEY (term_plan_id) REFERENCES public.term_plans(id);


--
-- Name: issue_responses fk_rails_271194df3d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issue_responses
    ADD CONSTRAINT fk_rails_271194df3d FOREIGN KEY (issue_id) REFERENCES public.issues(id);


--
-- Name: issues fk_rails_30d24f8d9d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT fk_rails_30d24f8d9d FOREIGN KEY (aguda_rep_id) REFERENCES public.issues_users(id);


--
-- Name: chair_role_setups fk_rails_32155cdc2e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.chair_role_setups
    ADD CONSTRAINT fk_rails_32155cdc2e FOREIGN KEY (app_setup_id) REFERENCES public.app_setups(id);


--
-- Name: issues_role_setups fk_rails_379cd3cfcb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_role_setups
    ADD CONSTRAINT fk_rails_379cd3cfcb FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- Name: seminars fk_rails_3ce37ccc58; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seminars
    ADD CONSTRAINT fk_rails_3ce37ccc58 FOREIGN KEY (term_id) REFERENCES public.terms(id);


--
-- Name: chair_role_setups fk_rails_56392c7ce1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.chair_role_setups
    ADD CONSTRAINT fk_rails_56392c7ce1 FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- Name: role_events fk_rails_646f38e5bd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_events
    ADD CONSTRAINT fk_rails_646f38e5bd FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: term_plans fk_rails_6982dbd5fb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.term_plans
    ADD CONSTRAINT fk_rails_6982dbd5fb FOREIGN KEY (degree_plan_id) REFERENCES public.degree_plans(id);


--
-- Name: carousel_item_records fk_rails_6e977ca37c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.carousel_item_records
    ADD CONSTRAINT fk_rails_6e977ca37c FOREIGN KEY (item_id) REFERENCES public.carousel_items(id);


--
-- Name: issues_role_setups fk_rails_71814b6e69; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_role_setups
    ADD CONSTRAINT fk_rails_71814b6e69 FOREIGN KEY (app_setup_id) REFERENCES public.app_setups(id);


--
-- Name: role_events fk_rails_8efb01dc15; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_events
    ADD CONSTRAINT fk_rails_8efb01dc15 FOREIGN KEY (creator_id) REFERENCES public.users(id);


--
-- Name: course_lists fk_rails_98e93c1552; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.course_lists
    ADD CONSTRAINT fk_rails_98e93c1552 FOREIGN KEY (shnaton_id) REFERENCES public.shnatons(id);


--
-- Name: active_storage_variant_records fk_rails_993965df05; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_variant_records
    ADD CONSTRAINT fk_rails_993965df05 FOREIGN KEY (blob_id) REFERENCES public.active_storage_blobs(id);


--
-- Name: generic_courses fk_rails_99c68dfc9d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.generic_courses
    ADD CONSTRAINT fk_rails_99c68dfc9d FOREIGN KEY (year_added_id) REFERENCES public.ac_years(id);


--
-- Name: staff_role_setups fk_rails_a1fffecfb0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.staff_role_setups
    ADD CONSTRAINT fk_rails_a1fffecfb0 FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- Name: courses fk_rails_b2ba49a817; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.courses
    ADD CONSTRAINT fk_rails_b2ba49a817 FOREIGN KEY (student_rep_id) REFERENCES public.issues_users(id);


--
-- Name: degrees fk_rails_b4d598448e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.degrees
    ADD CONSTRAINT fk_rails_b4d598448e FOREIGN KEY (shnaton_id) REFERENCES public.shnatons(id);


--
-- Name: prog_prereqs fk_rails_b731d19a0e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prog_prereqs
    ADD CONSTRAINT fk_rails_b731d19a0e FOREIGN KEY (degree_id) REFERENCES public.degrees(id);


--
-- Name: issue_responses fk_rails_ba110d4a05; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issue_responses
    ADD CONSTRAINT fk_rails_ba110d4a05 FOREIGN KEY (creator_id) REFERENCES public.users(id);


--
-- Name: active_storage_attachments fk_rails_c3b3935057; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_storage_attachments
    ADD CONSTRAINT fk_rails_c3b3935057 FOREIGN KEY (blob_id) REFERENCES public.active_storage_blobs(id);


--
-- Name: degree_plans fk_rails_c47274d0d7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.degree_plans
    ADD CONSTRAINT fk_rails_c47274d0d7 FOREIGN KEY (degree_id) REFERENCES public.degrees(id);


--
-- Name: generic_courses fk_rails_d04816bb61; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.generic_courses
    ADD CONSTRAINT fk_rails_d04816bb61 FOREIGN KEY (aguda_rep_id) REFERENCES public.issues_users(id);


--
-- Name: staff_role_setups fk_rails_dc12112616; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.staff_role_setups
    ADD CONSTRAINT fk_rails_dc12112616 FOREIGN KEY (app_setup_id) REFERENCES public.app_setups(id);


--
-- Name: role_events fk_rails_dc875952c5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role_events
    ADD CONSTRAINT fk_rails_dc875952c5 FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- Name: shnatons fk_rails_e4b23798e9; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.shnatons
    ADD CONSTRAINT fk_rails_e4b23798e9 FOREIGN KEY (ac_year_id) REFERENCES public.ac_years(id);


--
-- Name: issues fk_rails_e760f5afbf; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT fk_rails_e760f5afbf FOREIGN KEY (reporter_id) REFERENCES public.issues_users(id);


--
-- Name: courses fk_rails_eb33ecb37f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.courses
    ADD CONSTRAINT fk_rails_eb33ecb37f FOREIGN KEY (aguda_rep_id) REFERENCES public.issues_users(id);


--
-- Name: issues fk_rails_f7a4353f5d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT fk_rails_f7a4353f5d FOREIGN KEY (replier_id) REFERENCES public.users(id);


--
-- Name: plan_items fk_rails_fc374d6dac; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.plan_items
    ADD CONSTRAINT fk_rails_fc374d6dac FOREIGN KEY (generic_course_id) REFERENCES public.generic_courses(id);


--
-- Name: generic_courses fk_rails_fec94f4fa4; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.generic_courses
    ADD CONSTRAINT fk_rails_fec94f4fa4 FOREIGN KEY (year_removed_id) REFERENCES public.ac_years(id);


--
-- Name: issues fk_rails_ff05c886db; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT fk_rails_ff05c886db FOREIGN KEY (course_id) REFERENCES public.courses(id);


--
-- Name: issues_users fk_rails_ff38260f6e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues_users
    ADD CONSTRAINT fk_rails_ff38260f6e FOREIGN KEY (department_id) REFERENCES public.departments(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20150205102854'),
('20150206143744'),
('20150207013726'),
('20150207015241'),
('20150207042817'),
('20150207043718'),
('20150207044439'),
('20150209150935'),
('20150209203948'),
('20150211093519'),
('20150211100831'),
('20150211151119'),
('20150211155714'),
('20150211195446'),
('20150211210651'),
('20150211211649'),
('20150212081806'),
('20150214174727'),
('20150219121909'),
('20150219171231'),
('20150219183538'),
('20150220071702'),
('20150223201759'),
('20150224063808'),
('20150226163216'),
('20150516161948'),
('20150523170702'),
('20150526190317'),
('20150527120102'),
('20150528091349'),
('20150528094743'),
('20150528154954'),
('20150528185403'),
('20150529103104'),
('20150529123755'),
('20150530131351'),
('20150601045148'),
('20150601072907'),
('20150604053414'),
('20150605182338'),
('20150610141658'),
('20150610143532'),
('20150610143804'),
('20150610173522'),
('20150610181558'),
('20150610181812'),
('20150721103200'),
('20150722114322'),
('20150722122433'),
('20150815071012'),
('20150820125320'),
('20150822171811'),
('20150823141114'),
('20150824055021'),
('20150824060619'),
('20150824081821'),
('20150824082816'),
('20150824122644'),
('20150826131615'),
('20150828074020'),
('20150831203946'),
('20150905200022'),
('20150905204449'),
('20150908122805'),
('20150912055445'),
('20150912082416'),
('20150921195214'),
('20150921200636'),
('20150921201612'),
('20150922195121'),
('20150922195625'),
('20150923131421'),
('20150923192831'),
('20150924090325'),
('20150924123151'),
('20150924123720'),
('20150924152954'),
('20150924153336'),
('20150924165520'),
('20150925070359'),
('20150926114303'),
('20150926114646'),
('20150926114731'),
('20150926120921'),
('20150926123001'),
('20150926134544'),
('20150926143502'),
('20150926144235'),
('20150927163038'),
('20151002103755'),
('20151002110214'),
('20151005104947'),
('20151005110515'),
('20151005151206'),
('20151005154910'),
('20151005155200'),
('20151016110838'),
('20151024162504'),
('20151024170609'),
('20151024183306'),
('20151024183730'),
('20151024185024'),
('20151024185839'),
('20151024190338'),
('20151024200632'),
('20151024201353'),
('20151025201044'),
('20160212155502'),
('20160225145844'),
('20160226190447'),
('20160306053540'),
('20160306174541'),
('20160306181617'),
('20160306184206'),
('20160311210005'),
('20160313065013'),
('20160314135215'),
('20160314202336'),
('20160314222914'),
('20160321205853'),
('20160327042357'),
('20160401090924'),
('20160401155119'),
('20160403043547'),
('20160417184213'),
('20160421061816'),
('20160421062813'),
('20160421073239'),
('20160421075602'),
('20160421172341'),
('20160421172507'),
('20160425201312'),
('20160427111707'),
('20160502185703'),
('20160502185858'),
('20160503085337'),
('20160503204448'),
('20160518053754'),
('20160602063416'),
('20160603143946'),
('20160809150732'),
('20160825083027'),
('20160908180250'),
('20160908190644'),
('20160913175519'),
('20160924181102'),
('20160925053928'),
('20160925085747'),
('20160928150856'),
('20161009144244'),
('20161012070208'),
('20161012083320'),
('20161013061208'),
('20161013061509'),
('20161130195713'),
('20170123093656'),
('20170311111954'),
('20170816115110'),
('20170816115619'),
('20170816124054'),
('20170816130712'),
('20170818132714'),
('20170818135659'),
('20170902191834'),
('20170903124733'),
('20170904103742'),
('20170904110402'),
('20170904111754'),
('20170904121216'),
('20170904143417'),
('20170907125047'),
('20170907125307'),
('20170907125615'),
('20170907135233'),
('20170909151245'),
('20170910112119'),
('20170910184615'),
('20170910184932'),
('20170911081849'),
('20170912205349'),
('20170913122816'),
('20170920043806'),
('20170920051632'),
('20170920063908'),
('20170922163542'),
('20170922164858'),
('20170922165518'),
('20170924114018'),
('20170924133459'),
('20170928122056'),
('20170930140954'),
('20170930151329'),
('20170930194630'),
('20171005193448'),
('20171008084825'),
('20171009212054'),
('20171011093824'),
('20171012065648'),
('20171012065658'),
('20171012065668'),
('20171012113031'),
('20171012113238'),
('20171012161113'),
('20171012165927'),
('20171012184036'),
('20171013204012'),
('20171014221416'),
('20171028165031'),
('20171028182531'),
('20171029150519'),
('20171029215459'),
('20171030183353'),
('20171031054709'),
('20171031072445'),
('20171031074343'),
('20171103175619'),
('20171110223035'),
('20171110223453'),
('20171120195550'),
('20171120200539'),
('20171120202841'),
('20171226144901'),
('20171226151643'),
('20171226152158'),
('20180108151357'),
('20180113201436'),
('20180326193059'),
('20180627054842'),
('20180627092756'),
('20180627201657'),
('20180827050902'),
('20181003122418'),
('20181006105733'),
('20181022185517'),
('20181118065706'),
('20181118184256'),
('20190103091921'),
('20190103091922'),
('20190108124854'),
('20190108143017'),
('20190120193919'),
('20190121061103'),
('20190128202846'),
('20190129205648'),
('20190130095026'),
('20190130124726'),
('20190203060447'),
('20190203221654'),
('20190218182640'),
('20190301220557'),
('20190427044104'),
('20190502093820'),
('20190502095632'),
('20190502131417'),
('20190506062614'),
('20190506071615'),
('20190506090423'),
('20190514083605'),
('20190514085014'),
('20190516041843'),
('20190527074131'),
('20190621012253'),
('20190824185500'),
('20190928213005'),
('20191101132543'),
('20191107042849'),
('20191118205500'),
('20191123074601'),
('20191202150648'),
('20191209062618'),
('20191209070120'),
('20191218161323'),
('20200111121605'),
('20200111135602'),
('20200111150641'),
('20200111152132'),
('20200111152145'),
('20200111152158'),
('20200111152211'),
('20200111152224'),
('20200111152237'),
('20200111152249'),
('20200111152302'),
('20200111152315'),
('20200111173257'),
('20200128181641'),
('20200128184319'),
('20200128190609'),
('20200215151132'),
('20200502204101'),
('20200502204755'),
('20200502210713'),
('20200504043835'),
('20200530075918'),
('20201221173252'),
('20201221173253'),
('20210419054832'),
('20210828084934'),
('20210907093916'),
('20211012110256'),
('20220113063813'),
('20220227123322'),
('20220302080655'),
('20220323135840'),
('20221229222313'),
('20230129204229'),
('20230130083756'),
('20230130083830'),
('20230130083850'),
('20230205151252'),
('20230504163149');


