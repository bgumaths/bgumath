# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

AppSetup.create(YAML.unsafe_load_file('config/app.yml')[Rails.env])
User.create(email: 'wwwmath', status: 'Virtual')
Role.create(name: 'Admin', :users => User.where(email: 'wwwmath'),
            visibility: 1)
