class AddFieldsToPhoto < ActiveRecord::Migration[5.2]
  def change
    add_column :photos, :rotate, :decimal
    add_column :photos, :scaleX, :decimal
    add_column :photos, :scaleY, :decimal
  end
end
