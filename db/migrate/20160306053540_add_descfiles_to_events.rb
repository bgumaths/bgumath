class AddDescfilesToEvents < ActiveRecord::Migration[4.2]
  def change
    add_column :events, :descfiles, :jsonb
  end
end
