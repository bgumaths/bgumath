class AddDepartmentIdToIssuesUser < ActiveRecord::Migration[5.1]
  def change
    add_reference :issues_users, :department, foreign_key: true
  end
end
