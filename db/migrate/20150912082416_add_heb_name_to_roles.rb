class AddHebNameToRoles < ActiveRecord::Migration[4.2]
  def change
    add_column :roles, :heb_name, :string
    add_column :roles, :heb_desc, :text
  end
end
