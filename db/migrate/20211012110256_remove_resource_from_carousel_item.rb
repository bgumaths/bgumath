class RemoveResourceFromCarouselItem < ActiveRecord::Migration[6.1]
  def change
    remove_reference :carousel_items, :resource, polymorphic: true, index: true
  end
end
