class RenameWhentzWhenInEvents < ActiveRecord::Migration[4.2]
  def change
    rename_column :events, :whentz, :when
  end
end
