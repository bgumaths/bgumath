class AddSlugToResearchGroups < ActiveRecord::Migration[4.2]
  def change
    add_column :research_groups, :slug, :string
    add_index :research_groups, :slug, unique: true
  end

  def data
    ResearchGroup.find_each(&:save)
  end
end
