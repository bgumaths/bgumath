class RoleEventUserNull < ActiveRecord::Migration[6.0]
  def change
    change_column_null :role_events, :user_id, true
  end
end
