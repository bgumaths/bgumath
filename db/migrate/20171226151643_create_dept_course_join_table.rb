class CreateDeptCourseJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_join_table :generic_courses, :departments do |t|
      t.index [:generic_course_id, :department_id], name: :index_departments_generic_courses_on_course_id_and_dept_id
      t.index [:department_id, :generic_course_id], name: :index_departments_generic_courses_on_dept_id_and_course_id
    end
  end
end
