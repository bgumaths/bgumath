class AddAdvancedToGenericCourse < ActiveRecord::Migration[5.1]
  def change
    add_column :generic_courses, :advanced, :boolean, default: true
  end
end
