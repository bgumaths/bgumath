class UsersRolesIndexUnique < ActiveRecord::Migration[5.0]
  def change
    remove_index :users_roles, [:user_id, :role_id]
    add_index :users_roles, [:user_id, :role_id], unique: true
  end
end
