class AddIndexToIssuesUserLogin < ActiveRecord::Migration[5.2]
  def change
    add_index :issues_users, :login, unique: true
  end
end
