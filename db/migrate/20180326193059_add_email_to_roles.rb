class AddEmailToRoles < ActiveRecord::Migration[5.1]
  def change
    add_column :roles, :email, :string
  end
end
