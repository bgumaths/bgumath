class AddIdTimestampsToCourseListsProgPrereq < ActiveRecord::Migration[6.0]
  def change
    add_column :course_lists_prog_prereqs, :id, :primary_key
    add_column :course_lists_prog_prereqs, :created_at, :datetime, null: false, default: Time.zone.now
    add_column :course_lists_prog_prereqs, :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
