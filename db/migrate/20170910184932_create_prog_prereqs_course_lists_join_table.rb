class CreateProgPrereqsCourseListsJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_join_table :course_lists, :prereqs,
      table_name: :course_lists_prog_prereqs do |t|
      t.index [:prereq_id, :course_list_id]
      t.index [:course_list_id, :prereq_id]
    end
  end
end
