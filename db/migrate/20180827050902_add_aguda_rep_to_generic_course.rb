class AddAgudaRepToGenericCourse < ActiveRecord::Migration[5.2]
  def change
    add_reference :generic_courses, :aguda_rep, 
      foreign_key: { to_table: :issues_users }, null: true, index: true
  end

  def data
    IssuesAgudaRep.find_each do |rr|
      rr.generic_courses = rr.courses.map(&:generic_course).uniq
      rr.courses = []
      rr.save!
    end
  end
end
