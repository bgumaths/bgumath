class RenamePrereqsProgramsToProgPrereqsPrograms < ActiveRecord::Migration[5.1]
  def change
    rename_table :prereqs_programs, :prog_prereqs_programs
  end
end
