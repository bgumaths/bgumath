require_relative '20171028182531_create_issues'
class CreateIssuesUuidTable < ActiveRecord::Migration[5.1]
  def change
    revert CreateIssues
    create_table :issues, id: :uuid do |t|
      t.belongs_to :course, foreign_key: true
      t.string :title, null: false
      t.text :content
      t.text :reply
      t.boolean :published, default: false


      t.timestamps
    end
  end
end
