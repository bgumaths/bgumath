class CreateYearPlans < ActiveRecord::Migration[5.1]
  def change
    create_table :year_plans do |t|
      t.jsonb :name_i18n, default: {en: 'recommended program', he: 'תכנית לימודים מומלצת'}, null: false
      t.belongs_to :degree, foreign_key: true
      t.jsonb :remarks_i18n, default: {}, null: false
      t.string :slug

      t.timestamps
    end
    add_index :year_plans, :slug, unique: true
  end
end
