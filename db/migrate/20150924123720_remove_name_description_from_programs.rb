class RemoveNameDescriptionFromPrograms < ActiveRecord::Migration[4.2]
  def change
    remove_column :programs, :name, :string
    remove_column :programs, :heb_name, :string
    remove_column :programs, :description, :text
    remove_column :programs, :heb_desc, :text
  end
end
