class CreateModelTables < ActiveRecord::Migration[4.2]
  def change
    create_table :model_tables do |t|
      t.string :name

      t.timestamps null: false
    end
    add_index :model_tables, :name
  end
end
