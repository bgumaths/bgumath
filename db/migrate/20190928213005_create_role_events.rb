class CreateRoleEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :role_events do |t|
      t.belongs_to :role, foreign_key: true, index: true
      t.belongs_to :user, foreign_key: true, index: true
      t.string :type, null: false, default: :added
      t.belongs_to :creator, foreign_key: { to_table: :users }, null: true, index: true

      t.timestamps
    end
  end
end
