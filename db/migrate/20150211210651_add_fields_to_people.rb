class AddFieldsToPeople < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :phd_year, :string
    add_column :users, :phd_from, :string
    add_column :users, :interests, :text
    add_column :users, :phone, :string
  end
end
