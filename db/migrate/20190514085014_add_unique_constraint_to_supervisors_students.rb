class AddUniqueConstraintToSupervisorsStudents < ActiveRecord::Migration[5.2]
  def change
    add_index :students_supervisors, [:student_id, :supervisor_id], unique: true
  end
end
