class CreateAppSetups < ActiveRecord::Migration[7.0]
  def change
    create_table :app_setups do |t|
      t.jsonb :depname_i18n, default: {}, null: false
      t.jsonb :fdepname_i18n, default: {}, null: false
      t.jsonb :department_i18n, default: {}, null: false
      t.jsonb :university_i18n, default: {}, null: false
      t.string :dept_email
      t.string :dept_fax
      t.string :dept_phone
      t.string :mail_domain
      t.string :local_mail_domain
      t.jsonb :building_i18n, default: {}, null: false
      t.jsonb :dept_snail_i18n, default: {}, null: false
      t.string :ml_base
      t.string :ml_mail_domain
      t.string :ml_domain
      t.string :ml_admin
      t.string :ml_admin_name
      t.string :gmaps_url
      t.string :moovit_url
      t.string :waze_url
      t.string :parent_url
      t.string :gspreadsheet_key

      t.timestamps
    end
  end
end
