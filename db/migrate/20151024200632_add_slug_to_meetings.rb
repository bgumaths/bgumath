class AddSlugToMeetings < ActiveRecord::Migration[4.2]
  def change
    add_column :meetings, :slug, :string
    add_index :meetings, :slug
  end
  def data
    Meeting.find_each(&:save)
  end
end
