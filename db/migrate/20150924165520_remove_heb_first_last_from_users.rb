class RemoveHebFirstLastFromUsers < ActiveRecord::Migration[4.2]
  def change
    remove_column :users, :hebfirst, :string
    remove_column :users, :heblast, :string
  end
end
