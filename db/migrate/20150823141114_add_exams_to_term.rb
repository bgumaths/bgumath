class AddExamsToTerm < ActiveRecord::Migration[4.2]
  def change
    add_column :terms, :exams, :date
  end
end
