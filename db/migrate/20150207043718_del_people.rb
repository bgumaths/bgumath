class DelPeople < ActiveRecord::Migration[4.2]
  def change
    drop_table :people
  end
end
