class CreateIssuesUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :issues_users do |t|
      t.string :login, null: false
      t.string :crypted_password, null: false
      t.string :password_salt, null: false
      t.string :persistence_token
      t.integer :login_count, default: 0, null: false
      t.integer :failed_login_count, default: 0, null: false
      t.datetime :last_request_at
      t.datetime :current_login_at
      t.datetime :last_login_at
      t.inet :current_login_ip
      t.inet :last_login_ip
      t.boolean :active, default: false
      t.boolean :approved, default: false
      t.boolean :confirmed, default: false
      t.string :type

      t.timestamps
    end
  end
end
