class AddUndoneAtToVersions < ActiveRecord::Migration[4.2]
  def change
    add_column :versions, :undone_at, :timestamp
  end
end
