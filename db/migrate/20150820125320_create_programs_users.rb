class CreateProgramsUsers < ActiveRecord::Migration[4.2]
  def change
    create_table :programs_coordinators, :id => false do |t|
      t.references :program, :coordinator
    end

    add_index :programs_coordinators, [:program_id, :coordinator_id],
      name: "programs_coordinators_index",
      unique: true
  end
end
