class ChangeMeetingAbsfilesToDescfiles < ActiveRecord::Migration[5.0]
  def change
    rename_column :meetings, :absfiles, :descfiles
  end
end
