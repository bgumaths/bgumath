class AddIndexForNameInGenericCourses < ActiveRecord::Migration[4.2]
  def change
    add_index :generic_courses, :name_i18n
    add_index :generic_courses, :description_i18n
  end
end
