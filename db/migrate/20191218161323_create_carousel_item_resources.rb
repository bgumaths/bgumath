class CreateCarouselItemResources < ActiveRecord::Migration[6.0]
  def change
    create_table :carousel_item_resources do |t|
      t.belongs_to :item, foreign_key: { to_table: :carousel_items },
        index: true
      t.belongs_to :resource, polymorphic: true, index: true

      t.timestamps
    end
  end

  def data
    ResourceCarouselItem.find_each do |it|
      next unless it.resource.present?
      it.resources.create!(resource: it.resource)
    end
  end
end
