class AddAbstractToCourses < ActiveRecord::Migration[6.0]
  def change
    add_column :courses, :abstract_i18n, :jsonb, default: {}, null: false
    add_index :courses, :abstract_i18n
  end
end
