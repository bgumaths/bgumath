class AddDurationToMeetings < ActiveRecord::Migration[4.2]
  def change
    add_column :meetings, :duration, :tstzrange, default: :empty, null: false
    add_index :meetings, :duration, using: :gist
  end

  def data
    Meeting.find_each do |m|
      st = m[:date].beginning_of_day
      en = m[:date].beginning_of_day
      st += m[:starts].seconds_since_midnight.seconds
      en += m[:ends].seconds_since_midnight.seconds
      m.duration = st..en
      m.save!
    end
  end

end
