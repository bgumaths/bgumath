class AddTimestampsToMeetings < ActiveRecord::Migration[4.2]
  def change
    add_timestamps :meetings
  end

  def data
    Meeting.find_each do |m|
      puts "Saving #{m.decorate}\n"
      m.created_at = m.updated_at = Time.now
      m.save!
    end
  end
end
