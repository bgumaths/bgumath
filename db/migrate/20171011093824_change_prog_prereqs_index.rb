class ChangeProgPrereqsIndex < ActiveRecord::Migration[5.1]
  def change
    remove_index :prog_prereqs, column: [:shnaton_id, :slug]
    add_index :prog_prereqs, [:degree_id, :slug], unique: true
  end

  def data
    ProgPrereq.transaction do
      ProgPrereq.find_each do |pp|
        pp.slug = nil
        pp.save!
      end
    end
  end
end
