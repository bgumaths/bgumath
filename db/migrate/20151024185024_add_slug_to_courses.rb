class AddSlugToCourses < ActiveRecord::Migration[4.2]
  def change
    add_column :courses, :slug, :string
    add_index :courses, :slug
  end
  def data
    Course.find_each(&:save)
  end
end
