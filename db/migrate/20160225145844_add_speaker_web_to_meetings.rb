class AddSpeakerWebToMeetings < ActiveRecord::Migration[4.2]
  def change
    add_column :meetings, :speaker_web, :string
  end
end
