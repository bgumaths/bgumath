class ModifyPaperTrailToJsonb < ActiveRecord::Migration[4.2]
  def change
    rename_column :versions, :object, :old_object
    add_column :versions, :object, :jsonb
  end

  def data
    PaperTrail::Version.where.not(old_object: nil).find_each do |version|
      old = YAML.load version.old_object
      if old.respond_to?(:keys)
        version.update_columns old_object: nil, object: old
      else
        throw 'failed to decode'
      end
    end
  end
end
