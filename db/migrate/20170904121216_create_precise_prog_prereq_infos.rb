class CreatePreciseProgPrereqInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :precise_prog_prereq_infos do |t|
      t.belongs_to :prereq, index: {unique: true}
      t.belongs_to :course_list, foreign_key: true

      t.timestamps
    end
  end
end
