class ModifyImpressionistIndex < ActiveRecord::Migration[5.0]
  def change
    remove_index :impressions, [:impressionable_type, :impressionable_id, :params]
    add_index :impressions, [:impressionable_type, :impressionable_id, :params], name: 'poly_params_request_index', unique: false, length: { params: 255 }

  end
end
