class AddDatesToUser < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :start, :date
    add_column :users, :finish, :date
    add_column :users, :deceased, :date
  end
end
