class ChangeIdTypeImpressions < ActiveRecord::Migration[5.0]
  def change
    change_column :impressions, :impressionable_id, :string
  end
end
