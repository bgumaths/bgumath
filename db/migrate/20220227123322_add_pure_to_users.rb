class AddPureToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :pure, :uuid, null: true
  end
end
