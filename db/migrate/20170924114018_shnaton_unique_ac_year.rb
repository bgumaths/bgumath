class ShnatonUniqueAcYear < ActiveRecord::Migration[5.1]
  def change
    remove_index :shnatons, :ac_year_id
    add_index :shnatons, :ac_year_id, unique: true
  end
end
