class AddShidIndexToGenericCourses < ActiveRecord::Migration[5.2]
  def change
    add_index :generic_courses, :shid, where: "year_removed_id IS NULL",
      unique: true
  end
end
