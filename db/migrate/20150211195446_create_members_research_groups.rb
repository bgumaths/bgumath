class CreateMembersResearchGroups < ActiveRecord::Migration[4.2]
  def change
    create_join_table :members, :research_groups do |t|
      t.index :member_id
      t.index :research_group_id
    end
  end
end
