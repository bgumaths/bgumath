class IssuesCorrespondenceNonNull < ActiveRecord::Migration[5.2]
  def change
    change_column_null :issues, :correspondence, false, '-- חסר --'
  end
end
