class RenameNameInRoles < ActiveRecord::Migration[4.2]
  def change
    rename_column :roles, :name_i18n, :title_i18n
  end
end
