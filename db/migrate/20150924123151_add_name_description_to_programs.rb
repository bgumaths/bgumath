class AddNameDescriptionToPrograms < ActiveRecord::Migration[4.2]
  def change
    add_column :programs, :name_i18n, :jsonb, default: {}, null: false
    add_index :programs, :name_i18n
    add_column :programs, :description_i18n, :jsonb, default: {}, null: false
    add_index :programs, :description_i18n
  end

  def data
    Program.find_each do |p|
      p.name_i18n = {en: p.name, he: p.heb_name}
      p.description_i18n = {en: p.description, he: p.heb_desc}
      p.save!
    end
  end
end
