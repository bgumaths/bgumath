class AddOnlineToMeetings < ActiveRecord::Migration[6.0]
  def change
    add_column :meetings, :online, :string
  end
end
