class DelTimeSlots < ActiveRecord::Migration[4.2]
  def change
    drop_table :time_slots
    drop_table :courses_time_slots
  end
end
