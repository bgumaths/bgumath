class AddObjectRefToTemplate < ActiveRecord::Migration[5.0]
  def change
    add_reference :templates, :object, polymorphic: true
  end
end
