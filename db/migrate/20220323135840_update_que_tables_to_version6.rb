class UpdateQueTablesToVersion6 < ActiveRecord::Migration[6.1]
  def up
    Que.migrate!(version: 6)
  end

  def down
    Que.migrate!(version: 5)
  end
end
