class AddSelectedPhotosToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :selected_photos, :string
  end

  def data
    User.find_each do |u|
      u.selected_photos=u.photos.last.try(:url)
      u.save!
    end
  end

end
