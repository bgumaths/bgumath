class AddApiKeyToIssuesUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :issues_users, :api_key, :string
    add_index :issues_users, :api_key, unique: true
  end
end
