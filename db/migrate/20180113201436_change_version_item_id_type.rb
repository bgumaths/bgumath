class ChangeVersionItemIdType < ActiveRecord::Migration[5.1]
  def change
    change_column :versions, :item_id, :string
  end
end
