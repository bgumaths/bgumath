class CreateAcYears < ActiveRecord::Migration[5.1]
  def change
    create_table :ac_years do |t|
      t.integer :year

      t.timestamps
    end
    add_index :ac_years, :year, unique: true
  end
end
