class AddOhLocationToUser < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :oh_location, :string
  end
end
