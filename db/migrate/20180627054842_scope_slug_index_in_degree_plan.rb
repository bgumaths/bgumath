class ScopeSlugIndexInDegreePlan < ActiveRecord::Migration[5.2]
  def change
    remove_index :degree_plans, :slug
    add_index :degree_plans, :slug, unique: false
    add_index :degree_plans, [:degree_id, :slug], unique: true
  end
end
