class AddDescfilesToCourses < ActiveRecord::Migration[4.2]
  def change
    add_column :courses, :descfiles, :jsonb
  end
end
