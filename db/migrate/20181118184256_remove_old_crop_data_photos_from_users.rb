class RemoveOldCropDataPhotosFromUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :old_crop_data_photos
  end
end
