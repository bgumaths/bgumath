class RemoveWhenFromEvents < ActiveRecord::Migration[4.2]
  def change
    remove_column :events, :when, :tsrange
  end
end
