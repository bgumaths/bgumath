class AllowsRequiresUniq < ActiveRecord::Migration[5.2]
  def change
    remove_index :allows_requires, [:allow_id, :require_id]
    add_index :allows_requires, [:allow_id, :require_id], unique: true
    remove_index :allows_requires, [:require_id, :allow_id]
    add_index :allows_requires, [:require_id, :allow_id], unique: true
  end
end
