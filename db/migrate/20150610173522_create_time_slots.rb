class CreateTimeSlots < ActiveRecord::Migration[4.2]
  def change
    create_table :time_slots do |t|
      t.integer :day
      t.time :starts
      t.time :ends

      t.timestamps null: false
    end
  end
end
