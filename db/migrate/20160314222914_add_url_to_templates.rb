class AddUrlToTemplates < ActiveRecord::Migration[4.2]
  def change
    add_column :templates, :url, :string
  end
end
