class AddOrcidToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :orcid, :string, null: true
    add_column :users, :gs, :string, null: true
    add_column :users, :scopus, :string, null: true
    add_column :users, :arxiv, :string, null: true
    add_column :users, :wos, :string, null: true
  end
end
