class ChangeDefaultWebUsers < ActiveRecord::Migration[4.2]
  def change
    change_column_default :users, :webpage, 1
  end
end
