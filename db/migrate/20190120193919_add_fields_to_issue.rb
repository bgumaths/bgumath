class AddFieldsToIssue < ActiveRecord::Migration[5.2]
  def change
    add_column :issues, :replied_at, :datetime
    add_column :issues, :published_at, :datetime
  end

  def data
    puts "Updating published and replied dates"
    Issue.reset_column_information
    Issue.find_each do |i|
      puts "  #{i.title}: #{i.updated_at}"
      i.update!(published_at: i.updated_at, replied_at: i.updated_at - 24.hours)
    end
  end
end
