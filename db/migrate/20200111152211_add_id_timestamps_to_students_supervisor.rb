class AddIdTimestampsToStudentsSupervisor < ActiveRecord::Migration[6.0]
  def change
    add_column :students_supervisors, :id, :primary_key
    add_column :students_supervisors, :created_at, :datetime, null: false, default: Time.zone.now
    add_column :students_supervisors, :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
