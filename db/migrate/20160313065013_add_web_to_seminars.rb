class AddWebToSeminars < ActiveRecord::Migration[4.2]
  def change
    add_column :seminars, :web, :string
  end
end
