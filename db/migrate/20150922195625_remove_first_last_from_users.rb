class RemoveFirstLastFromUsers < ActiveRecord::Migration[4.2]
  def change
    remove_column :users, :first, :string
    remove_column :users, :last, :string
  end
end
