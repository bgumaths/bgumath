class DropCourses < ActiveRecord::Migration[4.2]
  def change
    drop_table :courses
  end
end
