class CreateIssuesJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_join_table :issues_users, :courses do |t|
      t.index [:issues_user_id, :course_id], unique: true
      t.index [:course_id, :issues_user_id], unique: true
    end
  end
end
