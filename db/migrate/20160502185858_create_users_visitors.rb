class CreateUsersVisitors < ActiveRecord::Migration[4.2]
  def change
    create_table :hosts_visitors, :id => false do |t|
      t.references :host
      t.references :visitor
    end

    add_index :hosts_visitors, [:host_id, :visitor_id],
      name: "hosts_visitors_index",
      unique: false
  end
end
