class AddAuthlogicFieldsToUser < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :persistence_token, :string
    add_column :users, :password_salt, :string
    add_column :users, :failed_login_count, :integer, null: false, default: 0
  end
end
