class CreateFreePlanItems < ActiveRecord::Migration[5.1]
  def change
    create_table :free_plan_items do |t|
      t.jsonb :title_i18n, default: {en: 'Elective courses', he: 'קורסי בחירה'}, null: false
      t.float :minp
      t.float :maxp
      t.belongs_to :term_plan, foreign_key: true

      t.timestamps
    end
  end
end
