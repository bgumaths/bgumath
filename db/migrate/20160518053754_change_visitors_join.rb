class ChangeVisitorsJoin < ActiveRecord::Migration[4.2]
  def change
    remove_index :hosts_visitors, name: 'hosts_visitors_index'
    add_index :hosts_visitors, [:host_id, :visitor_id],
      name: "hosts_visitors_index",
      unique: true
  end
end
