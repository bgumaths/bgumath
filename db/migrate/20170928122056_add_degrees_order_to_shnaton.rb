class AddDegreesOrderToShnaton < ActiveRecord::Migration[5.1]
  def change
    add_column :shnatons, :degrees_order, :bigint, array: true, default: []
  end
end
