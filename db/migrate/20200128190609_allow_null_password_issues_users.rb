class AllowNullPasswordIssuesUsers < ActiveRecord::Migration[6.0]
  def change
    change_column_null(:issues_users, :crypted_password, true)
    change_column_null(:issues_users, :password_salt, true)
  end
end
