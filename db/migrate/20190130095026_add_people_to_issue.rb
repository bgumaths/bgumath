class AddPeopleToIssue < ActiveRecord::Migration[5.2]
  def change
    add_reference :issues, :reporter, foreign_key: { to_table: :issues_users }
    add_reference :issues, :replier, foreign_key: { to_table: :users }
  end

  def data
    puts "Updating reporters and repliers"
    Issue.reset_column_information
    Issue.find_each do |i|
      puts "  #{i.title}:"
      orterl = i.versions.first.try(:whodunnit)
      orter = IssuesStudentRep.find_by(login: orterl)
      if orter.present?
        puts "    reporter: #{orter.login}"
      elsif orterl.present?
        puts "    WARNING: unable to find reporter #{orterl}"
        orter = i.course.student_rep || IssuesStudentRep.take
      end
      lier = i.versions.select{|x| (x.changeset)['reply']}.last.try(:whodunnit)
      lier = User.find(lier) if lier
      puts "    replier: #{lier.email}" if lier.respond_to?(:email)
      i.update!(reporter: orter, replier: lier)
    end
  end

end
