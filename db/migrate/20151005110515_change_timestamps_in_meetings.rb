class ChangeTimestampsInMeetings < ActiveRecord::Migration[4.2]
  def change
    change_column_null :meetings, :created_at, false
    change_column_null :meetings, :updated_at, false
  end
end
