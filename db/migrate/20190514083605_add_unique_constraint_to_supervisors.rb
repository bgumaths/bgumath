class AddUniqueConstraintToSupervisors < ActiveRecord::Migration[5.2]
  def change
    remove_index :students_supervisors, [:student_id, :supervisor_id]
    remove_index :students_supervisors, [:supervisor_id, :student_id]
  end

  def data
    User.where(status: %w[Phd Msc]).find_each do |ss|
      puts "Removing duplicate supervisors for #{ss.slug}"
      ss.supervisors = ss.supervisors.uniq
      ss.save!
    end
  end
end

