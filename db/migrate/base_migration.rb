class BaseMigration < ActiveRecord::Migration[4.2]
  def change_column_hstore_jsonb(table, field)
    change_column_null table, field, true
    change_column_default table, field, nil
    change_column table, field, "jsonb USING CAST(#{field} AS jsonb)"
    change_column_default table, field, {}
    change_column_null table, field, false
  end
end


