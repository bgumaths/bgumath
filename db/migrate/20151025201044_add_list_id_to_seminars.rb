class AddListIdToSeminars < ActiveRecord::Migration[4.2]
  def change
    add_column :seminars, :list_id, :string
    add_index :seminars, :list_id, unique: true
  end
end
