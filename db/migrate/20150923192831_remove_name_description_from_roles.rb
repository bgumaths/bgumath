class RemoveNameDescriptionFromRoles < ActiveRecord::Migration[4.2]
  def change
    remove_column :roles, :heb_name, :string
    remove_column :roles, :description, :text
    remove_column :roles, :heb_desc, :text
  end
end
