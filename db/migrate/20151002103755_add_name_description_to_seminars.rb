class AddNameDescriptionToSeminars < ActiveRecord::Migration[4.2]
  def change
    add_column :seminars, :name_i18n, :jsonb, default: {}, null: false
    add_index :seminars, :name_i18n
    add_column :seminars, :description_i18n, :jsonb, default: {}, null: false
    add_index :seminars, :description_i18n
  end

  def data
    Seminar.find_each do |s|
      s.name_i18n[:en] = s[:name]
      s.description_i18n[:en] = s[:description]
      s.save!
    end
  end
end
