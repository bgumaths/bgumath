class RemoveRetiredFromUsers < ActiveRecord::Migration[4.2]
  def change
    remove_column :users, :retired, :boolean
  end
end
