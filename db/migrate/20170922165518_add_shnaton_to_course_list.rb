class AddShnatonToCourseList < ActiveRecord::Migration[5.1]
  def change
    add_reference :course_lists, :shnaton, foreign_key: true
    add_index :course_lists, :slug, unique: false
    add_index :course_lists, [:shnaton_id, :slug], unique: true

  end
end
