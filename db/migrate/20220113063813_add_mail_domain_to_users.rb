class AddMailDomainToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :mail_domain, :string, null: true, default: nil
  end
end
