class ResearchGroupNameNotNull < ActiveRecord::Migration[4.2]
  def change
    change_column_null :research_groups, :name, false
  end
end
