class AddRoomToSeminars < ActiveRecord::Migration[4.2]
  def change
    add_column :seminars, :room, :string
  end
end
