class RemoveCourseListFromPreciseProgPrereqInfo < ActiveRecord::Migration[5.1]
  def change
    remove_column :precise_prog_prereq_infos, :course_list_id, :references
    remove_column :basic_prog_prereq_infos, :course_list_id, :references
  end
end
