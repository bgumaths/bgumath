class AddHebNameToGenericCourse < ActiveRecord::Migration[4.2]
  def change
    add_column :generic_courses, :heb_name, :string
  end
end
