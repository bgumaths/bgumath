class AddSlugToPrograms < ActiveRecord::Migration[4.2]
  def change
    add_column :programs, :slug, :string
    add_index :programs, :slug, unique: true
  end
  def data
    Program.find_each(&:save)
  end
end
