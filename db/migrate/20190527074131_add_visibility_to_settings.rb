class AddVisibilityToSettings < ActiveRecord::Migration[5.2]
  def change
    add_column :settings, :visibility, :integer, null: false, default: 1
  end
end
