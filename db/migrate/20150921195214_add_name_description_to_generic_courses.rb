class AddNameDescriptionToGenericCourses < ActiveRecord::Migration[4.2]
  def change
    add_column :generic_courses, :name_i18n, :jsonb, default: {}, null: false
    add_column :generic_courses, :description_i18n, :jsonb, default: {}, null: false
  end

end
