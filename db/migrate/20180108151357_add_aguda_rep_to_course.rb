class AddAgudaRepToCourse < ActiveRecord::Migration[5.1]
  def change
    add_reference :courses, :aguda_rep, foreign_key: {to_table: :issues_users}
  end

  def data
    Course.find_each do |c|
      c.aguda_rep = c.student_rep
      c.student_rep = nil
    end
  end
end
