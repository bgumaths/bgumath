class AddPhotoToUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :photo, foreign_key: true, index: {unique: true}, 
      type: :uuid
  end
end
