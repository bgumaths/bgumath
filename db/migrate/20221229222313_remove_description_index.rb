class RemoveDescriptionIndex < ActiveRecord::Migration[7.0]
  def change
    remove_index :programs, :description_i18n, if_exists: true
    remove_index :seminars, :description_i18n, if_exists: true
    remove_index :roles, :description_i18n, if_exists: true
    remove_index :generic_courses, :description_i18n, if_exists: true

  end
end
