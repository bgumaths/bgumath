class RemoveStartsEndsFromSeminars < ActiveRecord::Migration[4.2]
  def change
    remove_column :seminars, :starts, :time
    remove_column :seminars, :ends, :time
  end
end
