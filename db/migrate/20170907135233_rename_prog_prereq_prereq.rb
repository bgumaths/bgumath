class RenameProgPrereqPrereq < ActiveRecord::Migration[5.1]
  def change
    rename_column :degrees_prog_prereqs, :prog_prereq_id, :prereq_id
  end
end
