class ChangePassChangedDefault < ActiveRecord::Migration[6.0]
  def change
    change_column_default :issues_users, :pass_changed, from: false, to: true
  end
end
