class AddWhenToEvents < ActiveRecord::Migration[4.2]
  def change
    add_column :events, :when, :tsrange, default: :empty, null: false
    add_index :events, :when, using: :gist
  end

  def data
    Event.find_each do |e|
      e.when = e[:starts]..e[:ends]
      e.save!
    end
  end
end

