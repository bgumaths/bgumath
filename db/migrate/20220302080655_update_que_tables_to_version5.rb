class UpdateQueTablesToVersion5 < ActiveRecord::Migration[6.1]
  def up
    Que.migrate!(version: 5)
  end
  def down
    Que.migrate!(version: 4)
  end
end
