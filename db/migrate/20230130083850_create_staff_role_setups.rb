class CreateStaffRoleSetups < ActiveRecord::Migration[7.0]
  def change
    create_table :staff_role_setups do |t|
      t.belongs_to :app_setup, foreign_key: true
      t.belongs_to :role, foreign_key: true

      t.timestamps
    end
  end
end
