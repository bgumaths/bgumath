class AddCropDataPhotosToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :crop_data_photos, :text, default: '{}', null: false
  end

  def data
    User.find_each do |u|
      u.crop_data_photos = '{}'
      u.save!
    end
  end
end
