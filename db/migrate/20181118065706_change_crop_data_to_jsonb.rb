class ChangeCropDataToJsonb < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :crop_data_photos, :old_crop_data_photos
    add_column :users, :crop_data_photos, :jsonb, default: {}, null: false
  end

#  def data
#    User.find_each do |uu|
#      val = {}
#      puts "Processing #{uu.slug}, old crop data 
  #      is:\n#{uu.old_crop_data_photos.inspect}\n"
#      JSON.parse(uu.old_crop_data_photos).each do |k,v|
#        puts "  #{k} => #{v.inspect}\n"
#        val[k] = JSON.parse(v)
#      end
#      uu.update_columns old_crop_data_photos: nil, crop_data_photos: val
#    end
#  end
end
