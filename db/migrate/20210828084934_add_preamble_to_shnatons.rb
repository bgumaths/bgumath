class AddPreambleToShnatons < ActiveRecord::Migration[6.1]
  def change
    add_column :shnatons, :preamble_i18n, :jsonb, default: {}, null: false
    add_index :shnatons, :preamble_i18n
  end
end
