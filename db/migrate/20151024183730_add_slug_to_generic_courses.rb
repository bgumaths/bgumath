class AddSlugToGenericCourses < ActiveRecord::Migration[4.2]
  def change
    add_column :generic_courses, :slug, :string
    add_index :generic_courses, :slug, unique: true
  end

  def data
    GenericCourse.find_each(&:save)
  end
end
