class CreateResearchGroupsSeminarsJoinTable < ActiveRecord::Migration[4.2]
  def change
    create_join_table :research_groups, :seminars do |t|
      # t.index [:research_group_id, :seminar_id]
      t.index [:seminar_id, :research_group_id], :unique => true, 
        :name => 'index_seminar_id_research_group_id'
    end
  end
end
