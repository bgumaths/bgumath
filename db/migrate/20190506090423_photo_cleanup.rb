class PhotoCleanup < ActiveRecord::Migration[5.2]
  def change
    change_table :photos do |t|
      t.remove :uuid
      t.remove :image_id
    end

    change_table :users do |t|
      t.remove :photo_uuid
      t.remove :selected_photos
      t.remove :crop_data_photos
      t.remove :photos
    end
  end
end
