class AddDescriptionToResearchGroup < ActiveRecord::Migration[4.2]
  def change
    add_column :research_groups, :description, :text
  end
end
