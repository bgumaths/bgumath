class ChangeBoolsInGenericCourse < ActiveRecord::Migration[4.2]
  def change
    change_column :generic_courses, :graduate, :boolean, default: false, null: false
  end
end
