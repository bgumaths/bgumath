class CreateCourseLists < ActiveRecord::Migration[5.1]
  def change
    create_table :course_lists do |t|
      t.jsonb :name_i18n, default: {}, null: false
      t.jsonb :remarks_i18n, default: {}, null: false

      t.timestamps
    end
    add_index :course_lists, :name_i18n
    add_index :course_lists, :remarks_i18n
  end
end
