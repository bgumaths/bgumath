class AddDescfilesToGenericCourses < ActiveRecord::Migration[4.2]
  def change
    add_column :generic_courses, :descfiles, :jsonb
  end
end
