class RemoveNameHebNameDescriptionHebDescFromGenericCourses < ActiveRecord::Migration[4.2]
  def change
    remove_column :generic_courses, :name, :string
    remove_column :generic_courses, :heb_name, :string
    remove_column :generic_courses, :description, :text
    remove_column :generic_courses, :heb_desc, :text
  end
end
