class AddFirstLastToUsers < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :first_i18n, :jsonb, default: {}, null: false
    add_column :users, :last_i18n, :jsonb, default: {}, null: false
    add_index :users, :first_i18n
    add_index :users, :last_i18n
  end

  def data
    User.find_each do |u|
      u.first_i18n = {en: u.first, he: u.hebfirst}
      u.last_i18n = {en: u.last, he: u.heblast}
      u.save!
    end
  end
end
