class CreateProgPrereqs < ActiveRecord::Migration[5.1]
  def change
    create_table :prog_prereqs do |t|
      t.jsonb :name_i18n, default: {}, null: false
      t.jsonb :description_i18n, default: {}, null: false
      t.string :type

      t.timestamps
    end
    add_index :prog_prereqs, :name_i18n
    add_index :prog_prereqs, :type
  end
end
