class RemoveUniqueFromEmailInOldUser < ActiveRecord::Migration[5.0]
  def change
    remove_index :old_users, :email
    add_index :old_users, :email
  end
end
