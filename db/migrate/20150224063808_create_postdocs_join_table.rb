class CreatePostdocsJoinTable < ActiveRecord::Migration[4.2]
  def change
    create_join_table :postdocs, :hosts do |t|
      t.index [:postdoc_id, :host_id]
      # t.index [:host_id, :postdoc_id]
    end
  end
end
