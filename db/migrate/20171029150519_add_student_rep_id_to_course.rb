class AddStudentRepIdToCourse < ActiveRecord::Migration[5.1]
  def change
    add_reference :courses, :student_rep, index: true
  end
end
