class AddShnatonToProgPrereq < ActiveRecord::Migration[5.1]
  def change
    add_reference :prog_prereqs, :shnaton, foreign_key: true
    remove_index :prog_prereqs, :slug
    add_index :prog_prereqs, :slug, unique: false
    add_index :prog_prereqs, [:shnaton_id, :slug], unique: true
  end
end
