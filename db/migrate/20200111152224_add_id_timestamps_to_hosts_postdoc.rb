class AddIdTimestampsToHostsPostdoc < ActiveRecord::Migration[6.0]
  def change
    add_column :hosts_postdocs, :id, :primary_key
    add_column :hosts_postdocs, :created_at, :datetime, null: false, default: Time.zone.now
    add_column :hosts_postdocs, :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
