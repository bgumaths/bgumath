class IssuesReporterNonNull < ActiveRecord::Migration[5.2]
  def change
    change_column_null :issues, :reporter_id, false
  end
end
