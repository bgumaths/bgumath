class AddTypeToCourseLists < ActiveRecord::Migration[7.0]
  def change
    add_column :course_lists, :type, :string
  end
end
