class AddIdTimestampsToUsersRole < ActiveRecord::Migration[6.0]
  def change
    add_column :users_roles, :id, :primary_key
    add_column :users_roles, :created_at, :datetime, null: false, default: Time.zone.now
    add_column :users_roles, :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
