class CreateUserSessions < ActiveRecord::Migration[4.2]
  def change
    create_table :user_sessions do |t|
      t.string :session_id, :null => false
      t.text :data
      t.timestamps null: false
    end

    add_index :user_sessions, :session_id
    add_index :user_sessions, :updated_at

  end
end
