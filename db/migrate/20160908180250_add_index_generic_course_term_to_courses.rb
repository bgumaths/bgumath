class AddIndexGenericCourseTermToCourses < ActiveRecord::Migration[5.0]
  def change
    add_index :courses, [:generic_course_id, :term_id], unique: true
  end
end
