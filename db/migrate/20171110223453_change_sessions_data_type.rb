class ChangeSessionsDataType < ActiveRecord::Migration[5.1]
  def change
    change_column :sessions, :data, :jsonb, using: 'data::jsonb'
  end
end
