class AddTermIdToSeminars < ActiveRecord::Migration[4.2]
  def change
    add_reference :seminars, :term, index: true, foreign_key: true
    remove_index :seminars, :slug
    add_index :seminars, :slug, unique: false
    add_index :seminars, [:term_id, :slug], unique: true
  end

  def data
    Seminar.find_each do |s|
      Term.find_each do |t|
        st = s.dup
        st.research_groups = s.research_groups
        st.term = t
        st.save!
        s.meetings.between(t.starts, t.next.try(:starts)).each do |m|
          m.seminar = st
          m.save!
        end
      end
    end
  end
end
