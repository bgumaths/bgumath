class CreateAdminsSeminars < ActiveRecord::Migration[4.2]
  def change
    create_join_table :admins, :seminars do |t|
      t.index :admin_id
      t.index :seminar_id
    end
  end
end
