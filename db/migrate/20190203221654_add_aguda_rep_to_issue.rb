class AddAgudaRepToIssue < ActiveRecord::Migration[5.2]
  def change
    add_reference :issues, :aguda_rep, foreign_key: { to_table: :issues_users }
  end
end
