class ChangeUserOfficeHoursToText < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :hours, :text, null: true
  end
end
