class AddFullnameToIssuesUser < ActiveRecord::Migration[5.1]
  def change
    add_column :issues_users, :fullname, :string
  end
end
