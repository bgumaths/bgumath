class DropStudentRepFromCourse < ActiveRecord::Migration[5.1]
  def change
    remove_column :courses, :student_rep_id
  end
end
