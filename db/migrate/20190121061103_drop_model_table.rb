class DropModelTable < ActiveRecord::Migration[5.2]
  def change
    drop_table :model_tables
  end
end
