class AddStudentRepIdToCourses < ActiveRecord::Migration[5.1]
  def change
    add_reference :courses, :student_rep, foreign_key: {to_table: :issues_users }
  end
end
