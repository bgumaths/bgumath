class CreateIssueResponses < ActiveRecord::Migration[6.0]
  def change
    create_table :issue_responses do |t|
      t.belongs_to :issue, foreign_key: true, type: :uuid,
        index: { unique: true }, null: false
      t.belongs_to :creator, foreign_key: { to_table: :users }, index: true,
        null: false
      t.text :content, null: false
      t.datetime :published_at, default: nil, null: true

      t.timestamps
    end
  end

  def data
    Issue.where.not(reply: nil).find_each do |is|
      puts "Creating response for #{is.slug}: #{is.title}"
      is.create_response!
    end
  end
end
