class AddNameDescriptionToRoles < ActiveRecord::Migration[4.2]
  def change
    add_column :roles, :name_i18n, :jsonb, default: {}, null: false
    add_index :roles, :name_i18n
    add_column :roles, :description_i18n, :jsonb, default: {}, null: false
    add_index :roles, :description_i18n
  end

  def data
    Role.find_each do |r|
      puts r.name
      r.name_i18n = { en: r.name, he: r.heb_name }
      r.description_i18n = { en: r.description, he: r.heb_desc }
      r.save(validate: false) or raise
    end
  end
end
