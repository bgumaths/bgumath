class CreateDegreeJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_join_table :degrees, :prog_prereqs do |t|
      t.index [:degree_id, :prog_prereq_id]
      t.index [:prog_prereq_id, :degree_id]
    end
  end
end
