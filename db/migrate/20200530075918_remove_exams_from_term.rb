class RemoveExamsFromTerm < ActiveRecord::Migration[6.0]
  def change
    remove_column :terms, :exams, :datetime
  end
end
