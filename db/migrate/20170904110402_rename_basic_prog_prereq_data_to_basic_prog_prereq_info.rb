class RenameBasicProgPrereqDataToBasicProgPrereqInfo < ActiveRecord::Migration[5.1]
  def change
    rename_table :basic_prog_prereq_datas, :basic_prog_prereq_infos
  end
end
