class AddIndexToDegree < ActiveRecord::Migration[5.1]
  def change
    remove_index :degrees, :slug
    add_index :degrees, :slug, unique: false
    add_index :degrees, [:shnaton_id, :slug], unique: true
  end
end
