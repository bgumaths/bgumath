class RemoveNameDescriptionFromSeminars < ActiveRecord::Migration[4.2]
  def change
    remove_column :seminars, :name, :string
    remove_column :seminars, :description, :text
  end
end
