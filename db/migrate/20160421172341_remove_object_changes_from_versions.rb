class RemoveObjectChangesFromVersions < ActiveRecord::Migration[4.2]
  def change
    remove_column :versions, :object_changes, :text
  end
end
