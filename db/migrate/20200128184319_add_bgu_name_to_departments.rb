class AddBguNameToDepartments < ActiveRecord::Migration[6.0]
  def change
    add_column :departments, :bgu_name, :string
    add_index :departments, :bgu_name
  end
end
