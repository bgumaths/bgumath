class CreateIssues < ActiveRecord::Migration[5.1]
  def change
    create_table :issues do |t|
      t.belongs_to :course, foreign_key: true
      t.string :title, null: false
      t.text :content
      t.text :reply

      t.timestamps
    end
  end
end
