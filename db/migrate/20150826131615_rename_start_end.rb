class RenameStartEnd < ActiveRecord::Migration[4.2]
  def change
    rename_column :meetings, :start, :starts
    rename_column :meetings, :end, :ends
    rename_column :seminars, :start, :starts
    rename_column :seminars, :end, :ends

  end
end
