class CreateEvents < ActiveRecord::Migration[4.2]
  def change
    create_table :events do |t|
      t.string :title
      t.datetime :starts
      t.datetime :ends
      t.string :location
      t.string :web
      t.text :description

      t.timestamps null: false
    end
  end
end
