class AddPhotosToUsers < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :photos, :jsonb
  end
end
