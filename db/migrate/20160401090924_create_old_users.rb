class CreateOldUsers < ActiveRecord::Migration[4.2]
  def change
    create_table :old_users do |t|
      t.string :email
      t.string :status
      t.string :phd_year
      t.string :phd_from
      t.text :interests
      t.string :rank
      t.string :webpage
      t.integer :state, default: 0, null: false
      t.jsonb :first_i18n, default: {}, null:false
      t.jsonb :last_i18n, default: {}, null:false
      t.string :slug
      t.jsonb :photos

      t.timestamps null: false
    end
    add_index :old_users, :email, unique: true, using: :btree
    add_index :old_users, :first_i18n, using: :btree
    add_index :old_users, :last_i18n, using: :btree
    add_index :old_users, :slug, unique: true, using: :btree
  end
end
