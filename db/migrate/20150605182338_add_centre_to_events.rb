class AddCentreToEvents < ActiveRecord::Migration[4.2]
  def change
    add_column :events, :centre, :boolean
  end
end
