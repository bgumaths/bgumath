class AddFieldsToGenericCourse < ActiveRecord::Migration[5.1]
  def change
    add_column :generic_courses, :fall, :boolean
    add_column :generic_courses, :spring, :boolean
    add_column :generic_courses, :lectures, :integer
    add_column :generic_courses, :exercises, :integer

    create_join_table :allows, :requires do |t|
      t.index [:allow_id, :require_id]
      t.index [:require_id, :allow_id]
    end
  end
end
