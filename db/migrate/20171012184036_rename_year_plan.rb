class RenameYearPlan < ActiveRecord::Migration[5.1]
  def change
    rename_table :year_plans, :degree_plans
    rename_column :term_plans, :year_plan_id, :degree_plan_id
  end
end
