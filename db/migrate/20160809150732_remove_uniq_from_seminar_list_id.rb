class RemoveUniqFromSeminarListId < ActiveRecord::Migration[5.0]
  def change
    remove_index :seminars, :list_id
    add_index :seminars, :list_id
  end
end
