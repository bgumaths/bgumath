class AddMailDomainToIssuesUser < ActiveRecord::Migration[7.0]
  def change
    add_column :issues_users, :mail_domain, :string, null: true, default: nil
  end
end
