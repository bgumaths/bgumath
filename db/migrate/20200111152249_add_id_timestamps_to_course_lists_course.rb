class AddIdTimestampsToCourseListsCourse < ActiveRecord::Migration[6.0]
  def change
    add_column :course_lists_courses, :id, :primary_key
    add_column :course_lists_courses, :created_at, :datetime, null: false, default: Time.zone.now
    add_column :course_lists_courses, :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
