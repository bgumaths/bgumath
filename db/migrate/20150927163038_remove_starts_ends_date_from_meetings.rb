class RemoveStartsEndsDateFromMeetings < ActiveRecord::Migration[4.2]
  def change
    remove_column :meetings, :starts, :time
    remove_column :meetings, :ends, :time
    remove_column :meetings, :date, :date
  end
end
