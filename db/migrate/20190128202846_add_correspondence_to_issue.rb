class AddCorrespondenceToIssue < ActiveRecord::Migration[5.2]
  def change
    add_column :issues, :correspondence, :text
  end
end
