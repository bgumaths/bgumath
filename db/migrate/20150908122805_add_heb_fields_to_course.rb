class AddHebFieldsToCourse < ActiveRecord::Migration[4.2]
  def change
    add_column :courses, :heb_title, :string
    add_column :courses, :heb_content, :text
  end
end
