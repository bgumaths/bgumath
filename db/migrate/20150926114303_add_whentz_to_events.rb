class AddWhentzToEvents < ActiveRecord::Migration[4.2]
  def change
    add_column :events, :whentz, :tstzrange, default: :empty, null: false
    add_index :events, :whentz, using: :gist
  end

  def data
    Event.find_each do |e|
      e.whentz = e.when.first.localtime..e.when.last.localtime
      e.save!
    end
  end
end
