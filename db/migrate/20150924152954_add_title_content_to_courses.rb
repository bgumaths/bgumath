class AddTitleContentToCourses < ActiveRecord::Migration[4.2]
  def change
    add_column :courses, :title_i18n, :jsonb, default: {}, null: false
    add_index :courses, :title_i18n
    add_column :courses, :content_i18n, :jsonb, default: {}, null: false
    add_index :courses, :content_i18n
  end

  def data
    Course.find_each do |c|
      c.title_i18n = {en: c.title, he: c.heb_title}
      c.content_i18n = {en: c.content, he: c.heb_content}
      c.save!
    end
  end
end
