class UniqueResearchGrroupsAssoc < ActiveRecord::Migration[5.2]
  def change
    add_index :members_research_groups, [:member_id, :research_group_id], unique: true, name: :members_research_groups_index
    add_index :research_groups_seminars, [:seminar_id, :research_group_id], unique: true, name: :research_groups_seminars_index
    drop_table :people_research_groups
  end
end
