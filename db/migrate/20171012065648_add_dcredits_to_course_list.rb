class AddDcreditsToCourseList < ActiveRecord::Migration[5.1]
  def change
    add_column :course_lists, :dcredits, :integer
  end
end
