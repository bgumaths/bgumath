class CreateVisitors < ActiveRecord::Migration[4.2]
  def change
    create_table :visitors do |t|
      t.string :email
      t.jsonb :first_i18n, default: {}, null: false
      t.jsonb :last_i18n, default: {}, null: false
      t.string :office
      t.string :phone
      t.string :rank
      t.string :webpage
      t.string :origin
      t.date :starts
      t.date :ends
      t.boolean :centre, default: true, null: false

      t.timestamps null: false
    end
    add_index :visitors, :email
  end
end

