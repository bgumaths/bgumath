class AddRetiredToPeople < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :retired, :boolean
  end
end
