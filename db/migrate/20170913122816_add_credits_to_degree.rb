class AddCreditsToDegree < ActiveRecord::Migration[5.1]
  def change
    add_column :degrees, :credits, :float
  end
end
