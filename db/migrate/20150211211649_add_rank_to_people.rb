class AddRankToPeople < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :rank, :string
  end
end
