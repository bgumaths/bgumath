class CreateTemplates < ActiveRecord::Migration[4.2]
  def change
    create_table :templates do |t|
      t.text :body
      t.string :path
      t.string :locale
      t.string :handler, default: 'erb'
      t.boolean :partial, default: false
      t.string :format, default: 'html'

      t.timestamps null: false
    end
  end
end
