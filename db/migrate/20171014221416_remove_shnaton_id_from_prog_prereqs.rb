class RemoveShnatonIdFromProgPrereqs < ActiveRecord::Migration[5.1]
  def change
    remove_column :prog_prereqs, :shnaton_id
  end
end
