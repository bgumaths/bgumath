class AddPrereqToBasicProgPrereqInfo < ActiveRecord::Migration[5.1]
  def change
    add_reference :basic_prog_prereq_infos, :prereq, index: {unique: true}
  end
end
