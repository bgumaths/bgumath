class AddHebToUsers < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :hebfirst, :string
    add_column :users, :heblast, :string
  end
end
