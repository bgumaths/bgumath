class AddOnlineToSeminars < ActiveRecord::Migration[6.0]
  def change
    add_column :seminars, :online, :string
  end
end
