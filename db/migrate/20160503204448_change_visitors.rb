class ChangeVisitors < ActiveRecord::Migration[4.2]
  def change
    add_column :visitors, :duration, :daterange, default: :empty, null: false
    remove_column :visitors, :starts, :date
    remove_column :visitors, :ends, :date
  end
end
