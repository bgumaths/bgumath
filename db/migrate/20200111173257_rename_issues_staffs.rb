class RenameIssuesStaffs < ActiveRecord::Migration[6.0]
  def change
    rename_table :issues_staffs, :issues_issues_staffs
  end
end
