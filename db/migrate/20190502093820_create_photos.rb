class CreatePhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :photos, id: :uuid do |t|
      t.references :imageable, polymorphic: true, index: true
      t.decimal :x
      t.decimal :y
      t.decimal :width
      t.decimal :height

      t.timestamps
    end
  end
end
