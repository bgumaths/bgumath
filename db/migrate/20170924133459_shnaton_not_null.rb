class ShnatonNotNull < ActiveRecord::Migration[5.1]
  def change
    change_column_null :degrees, :shnaton_id, false
    change_column_null :prog_prereqs, :shnaton_id, false
    change_column_null :course_lists, :shnaton_id, false
  end
end
