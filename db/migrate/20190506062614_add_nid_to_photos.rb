class AddNidToPhotos < ActiveRecord::Migration[5.2]
  def change
    change_table :photos do |t|
      t.uuid :uuid
      t.bigint :image_id
    end

    change_table :users do |t|
      t.uuid :photo_uuid
    end
  end

  def data
    Photo.find_each do |ph|
      ph.uuid=ph.id
      ph.image_id = ph.image_attachment.id
      ph.save!
    end

    User.find_each do |uu|
      next unless uu.photo.present?
      uu.photo_uuid = uu.photo.uuid
      uu.save!
    end
  end

end
