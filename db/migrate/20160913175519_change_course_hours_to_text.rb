class ChangeCourseHoursToText < ActiveRecord::Migration[5.0]
  def change
    change_column :courses, :hours, :text
  end
end
