require_relative '20190203060447_create_issue_staff_join_table'
class ChangeIssueIdUuid < ActiveRecord::Migration[5.2]
  def change
    revert CreateIssueStaffJoinTable
    create_table :issues_staffs, id: false do |t|
      t.uuid :issue_id
      t.integer :staff_id
      t.index [:issue_id, :staff_id]
      t.index [:staff_id, :issue_id]
    end
  end
end
