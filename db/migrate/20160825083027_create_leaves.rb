class CreateLeaves < ActiveRecord::Migration[5.0]
  def change
    create_table :leaves do |t|
      t.references :user, foreign_key: true
      t.daterange :duration
      t.jsonb :note_i18n, default: {}, null: false

      t.timestamps
    end
    add_index :leaves, :duration
  end
end
