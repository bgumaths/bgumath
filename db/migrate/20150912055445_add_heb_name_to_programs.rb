class AddHebNameToPrograms < ActiveRecord::Migration[4.2]
  def change
    add_column :programs, :heb_name, :string
    add_column :programs, :heb_desc, :text
  end
end
