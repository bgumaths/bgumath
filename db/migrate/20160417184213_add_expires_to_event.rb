class AddExpiresToEvent < ActiveRecord::Migration[4.2]
  def change
    add_column :events, :expires, :date
  end
end
