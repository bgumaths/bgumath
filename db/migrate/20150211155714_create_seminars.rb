class CreateSeminars < ActiveRecord::Migration[4.2]
  def change
    create_table :seminars do |t|
      t.string :name
      t.time :start
      t.time :end
      t.string :day
      t.text :description

      t.timestamps null: false
    end
    add_index :seminars, :name
  end
end
