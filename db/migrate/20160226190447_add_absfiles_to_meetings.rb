class AddAbsfilesToMeetings < ActiveRecord::Migration[4.2]
  def change
    add_column :meetings, :absfiles, :jsonb
  end
end
