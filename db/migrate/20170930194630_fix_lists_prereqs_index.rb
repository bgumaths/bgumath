class FixListsPrereqsIndex < ActiveRecord::Migration[5.1]
  def change
    remove_index :course_lists_prog_prereqs, [:course_list_id, :prereq_id]
    remove_index :course_lists_prog_prereqs, [:prereq_id, :course_list_id]
    add_index :course_lists_prog_prereqs, [:prereq_id, :course_list_id], unique: true
  end
end
