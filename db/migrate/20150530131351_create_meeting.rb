class CreateMeeting < ActiveRecord::Migration[4.2]
  def change
    create_table :meetings do |t|
      t.belongs_to :seminar, index: true
      t.string :speaker
      t.string :title
      t.text :abstract
      t.datetime :date
      t.time :start
      t.time :end
      t.string :room
    end
    add_foreign_key :meetings, :seminars
  end
end
