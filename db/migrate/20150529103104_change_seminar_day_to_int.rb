class ChangeSeminarDayToInt < ActiveRecord::Migration[4.2]
  def change
    change_column :seminars, :day, 'integer USING day::integer'
  end
end
