class AddTypeToDegree < ActiveRecord::Migration[5.1]
  def up
    execute <<-SQL
      DO $$ BEGIN
        CREATE TYPE degree_type AS ENUM ('single', 'dual', 'double', 'special');
      EXCEPTION
        WHEN duplicate_object THEN null;
      END $$;
    SQL

    add_column :degrees, :type, :degree_type, index: true, null: false, default: :single
  end

  def down
    remove_column :degrees, :type

    execute <<-SQL;
      DROP TYPE degree_type;
    SQL
  end
end
