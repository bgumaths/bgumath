class CreateTermPlans < ActiveRecord::Migration[5.1]
  def up
    execute <<-SQL
      CREATE TYPE term AS ENUM ('fall', 'spring');
    SQL

    create_table :term_plans do |t|
      t.integer :year
      t.belongs_to :year_plan, foreign_key: true
      t.jsonb :remarks_i18n, default: {}, null: false

      t.timestamps
    end

    add_column :term_plans, :term, :term
  end

  def down
    drop_table :term_plans

    execute <<-SQL;
      DROP TYPE term;
    SQL
  end
end
