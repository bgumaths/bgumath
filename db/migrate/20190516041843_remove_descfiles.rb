class RemoveDescfiles < ActiveRecord::Migration[5.2]
  def change
    %i[events courses generic_courses meetings].each do |tt|
      remove_column tt, :descfiles, :jsonb
    end
  end
end
