class AddIdTimestampsToDepartmentsGenericCourse < ActiveRecord::Migration[6.0]
  def change
    add_column :departments_generic_courses, :id, :primary_key
    add_column :departments_generic_courses, :created_at, :datetime, null: false, default: Time.zone.now
    add_column :departments_generic_courses, :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
