class AddOhExpiryToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :oh_expiry, :date
  end
end
