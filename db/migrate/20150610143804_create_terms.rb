class CreateTerms < ActiveRecord::Migration[4.2]
  def change
    create_table :terms do |t|
      t.date :starts
      t.date :ends

      t.timestamps null: false
    end
  end
end
