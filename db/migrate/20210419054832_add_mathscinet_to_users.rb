class AddMathscinetToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :mathscinet, :string, null: true
  end
end
