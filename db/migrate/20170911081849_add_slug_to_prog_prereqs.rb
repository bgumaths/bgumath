class AddSlugToProgPrereqs < ActiveRecord::Migration[5.1]
  def change
    add_column :prog_prereqs, :slug, :string
    add_index :prog_prereqs, :slug, unique: true
  end
end
