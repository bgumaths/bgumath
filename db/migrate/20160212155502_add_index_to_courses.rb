class AddIndexToCourses < ActiveRecord::Migration[4.2]
  def change
    remove_index :courses, :slug
    add_index :courses, :slug, unique: false
    add_index :courses, [:term_id, :slug], unique: true
  end
end
