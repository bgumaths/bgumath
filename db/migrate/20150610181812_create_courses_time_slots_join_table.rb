class CreateCoursesTimeSlotsJoinTable < ActiveRecord::Migration[4.2]
  def change
    create_join_table :courses, :time_slots do |t|
      t.index [:course_id, :time_slot_id]
      t.index [:time_slot_id, :course_id]
    end
  end
end
