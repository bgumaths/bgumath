class DropProgPrereqProgramsJoinTable < ActiveRecord::Migration[5.1]
  def change
    drop_table :prog_prereqs_programs
  end
end
