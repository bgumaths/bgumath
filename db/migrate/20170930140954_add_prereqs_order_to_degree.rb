class AddPrereqsOrderToDegree < ActiveRecord::Migration[5.1]
  def change
    add_column :degrees, :prereqs_order, :bigint, array: true, default: []
  end
end
