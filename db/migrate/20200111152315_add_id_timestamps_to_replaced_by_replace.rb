class AddIdTimestampsToReplacedByReplace < ActiveRecord::Migration[6.0]
  def change
    add_column :replaced_by_replaces, :id, :primary_key
    add_column :replaced_by_replaces, :created_at, :datetime, null: false, default: Time.zone.now
    add_column :replaced_by_replaces, :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
