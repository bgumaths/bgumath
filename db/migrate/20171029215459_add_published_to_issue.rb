class AddPublishedToIssue < ActiveRecord::Migration[5.1]
  def change
    add_column :issues, :published, :boolean, default: false
  end
end
