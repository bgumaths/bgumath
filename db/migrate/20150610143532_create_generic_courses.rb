class CreateGenericCourses < ActiveRecord::Migration[4.2]
  def change
    create_table :generic_courses do |t|
      t.string :name
      t.string :description
      t.string :shid
      t.boolean :graduate

      t.timestamps null: false
    end
  end
end
