class RenameWhenDurationInEvents < ActiveRecord::Migration[4.2]
  def change
    rename_column :events, :when, :duration
  end
end
