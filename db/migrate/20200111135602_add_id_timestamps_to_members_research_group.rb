class AddIdTimestampsToMembersResearchGroup < ActiveRecord::Migration[6.0]
  def change
    add_column :members_research_groups, :id, :primary_key
    add_column :members_research_groups, :created_at, :datetime, null: false, default: Time.zone.now
    add_column :members_research_groups, :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
