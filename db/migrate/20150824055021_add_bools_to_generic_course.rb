class AddBoolsToGenericCourse < ActiveRecord::Migration[4.2]
  def change
    add_column :generic_courses, :prereq, :boolean, null: false, default: false
    add_column :generic_courses, :core, :boolean, null: false, default: false
  end
end
