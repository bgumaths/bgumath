class AddIdTimestampsToAllowsRequire < ActiveRecord::Migration[6.0]
  def change
    add_column :allows_requires, :id, :primary_key
    add_column :allows_requires, :created_at, :datetime, null: false, default: Time.zone.now
    add_column :allows_requires, :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
