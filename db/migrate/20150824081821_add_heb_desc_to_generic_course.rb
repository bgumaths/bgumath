class AddHebDescToGenericCourse < ActiveRecord::Migration[4.2]
  def change
    add_column :generic_courses, :heb_desc, :text
    change_column :generic_courses, :description, :text
  end
end
