class RemoveStartsEndsFromEvents < ActiveRecord::Migration[4.2]
  def change
    remove_column :events, :starts, :datetime
    remove_column :events, :ends, :datetime
  end
end
