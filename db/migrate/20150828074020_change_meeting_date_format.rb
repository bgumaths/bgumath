class ChangeMeetingDateFormat < ActiveRecord::Migration[4.2]
  def change
    change_column :meetings, :date, :date
  end
end
