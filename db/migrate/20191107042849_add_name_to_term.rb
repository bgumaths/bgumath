class AddNameToTerm < ActiveRecord::Migration[6.0]
  def change
    add_column :terms, :name_i18n, :jsonb, default: {}, null: false
    add_index :terms, :name_i18n
  end
end
