class AddDurationToSeminars < ActiveRecord::Migration[4.2]
  def change
    add_column :seminars, :duration, :tstzrange, default: :empty, null: false
    add_index :seminars, :duration, using: :gist
  end

  def data
    Seminar.find_each do |s|
      s.duration = s[:starts].to_time.in_time_zone..s[:ends].to_time.in_time_zone
      s.save!
    end
  end
end
