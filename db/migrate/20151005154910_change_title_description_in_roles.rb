require File.join(File.dirname(__FILE__), 'base_migration')
class ChangeTitleDescriptionInRoles < BaseMigration
  def change
    change_column_hstore_jsonb :roles, :title_i18n
    change_column_hstore_jsonb :roles, :description_i18n
  end
end
