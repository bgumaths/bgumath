class RemoveForeignKeyFromLeaves < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :leaves, :users
  end
end
