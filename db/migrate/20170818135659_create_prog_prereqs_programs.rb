class CreateProgPrereqsPrograms < ActiveRecord::Migration[5.1]
  def change
    create_join_table :prereqs, :programs do |t|
      t.index [:prereq_id, :program_id]
      t.index [:program_id, :prereq_id]
    end
  end
end
