class AddSlugToTerms < ActiveRecord::Migration[4.2]
  def change
    add_column :terms, :slug, :string
    add_index :terms, :slug, unique: true
  end

  def data
    Term.find_each(&:save)
  end
end
