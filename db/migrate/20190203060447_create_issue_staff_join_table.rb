class CreateIssueStaffJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_join_table :issues, :staffs do |t|
      t.index :issue_id
      t.index :staff_id
    end
  end
end
