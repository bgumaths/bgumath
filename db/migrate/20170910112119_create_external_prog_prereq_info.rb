class CreateExternalProgPrereqInfo < ActiveRecord::Migration[5.1]
  def change
    create_table :external_prog_prereq_infos do |t|
      t.jsonb :location_i18n, default: {}, null: false
      t.references :course_list, foreign_key: true
      t.belongs_to :prereq, index: { unique: true }
      t.integer :minc
      t.integer :maxc
      t.float :minp
      t.float :maxp
    end
  end
end
