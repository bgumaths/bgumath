class CreateShnatons < ActiveRecord::Migration[5.1]
  def change
    create_table :shnatons do |t|
      t.belongs_to :ac_year, foreign_key: true

      t.timestamps
    end
  end
end
