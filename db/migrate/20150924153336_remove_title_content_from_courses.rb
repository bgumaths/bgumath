class RemoveTitleContentFromCourses < ActiveRecord::Migration[4.2]
  def change
    remove_column :courses, :title, :string
    remove_column :courses, :heb_title, :string
    remove_column :courses, :content, :text
    remove_column :courses, :heb_content, :text
  end
end
