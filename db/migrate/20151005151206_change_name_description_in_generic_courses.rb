require File.join(File.dirname(__FILE__), 'base_migration')
class ChangeNameDescriptionInGenericCourses < BaseMigration
  def change
    change_column_hstore_jsonb :generic_courses, :name_i18n
    change_column_hstore_jsonb :generic_courses, :description_i18n
  end
end
