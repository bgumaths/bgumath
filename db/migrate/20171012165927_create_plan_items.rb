class CreatePlanItems < ActiveRecord::Migration[5.1]
  def change
    create_table :plan_items do |t|
      t.belongs_to :term_plan, foreign_key: true
      t.belongs_to :generic_course, foreign_key: true

      t.timestamps
    end
  end
end
