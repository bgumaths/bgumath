class CreateDepartments < ActiveRecord::Migration[5.1]
  def change
    create_table :departments do |t|
      t.jsonb :name_i18n, default: {}, null: false
      t.integer :catalog_id
      t.string :slug

      t.timestamps
    end
    add_index :departments, :catalog_id, unique: true
    add_index :departments, :slug, unique: true
  end
end
