class AddHistoryToGenericCourse < ActiveRecord::Migration[5.2]
  def change
    add_reference :generic_courses, :year_added, 
      foreign_key: { to_table: :ac_years }, null: true, index: true
    add_reference :generic_courses, :year_removed, 
      foreign_key: { to_table: :ac_years }, null: true, index: true
    create_join_table :replaces, :replaced_by do |t|
      t.index [:replace_id, :replaced_by_id]
      t.index [:replaced_by_id, :replace_id]
    end

  end
end
