class ChangeCarouselItemResourcesToCarouselItemRecords < ActiveRecord::Migration[6.1]
  def change
    rename_table :carousel_item_resources, :carousel_item_records
  end
end
