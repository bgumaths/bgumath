class AddSlugToSeminars < ActiveRecord::Migration[4.2]
  def change
    add_column :seminars, :slug, :string
    add_index :seminars, :slug, unique: true
  end
  def data
    Seminar.find_each(&:save)
  end
end
