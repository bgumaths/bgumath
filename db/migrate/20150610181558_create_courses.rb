class CreateCourses < ActiveRecord::Migration[4.2]
  def change
    create_table :courses do |t|
      t.references :term, index: true
      t.references :generic_course, index: true
      t.string :title
      t.text :content
      t.references :lecturer, index: true
      t.string :hours
      t.string :web

      t.timestamps null: false
    end
    add_foreign_key :courses, :terms
    add_foreign_key :courses, :generic_courses
    add_foreign_key :courses, :lecturers
  end
end
