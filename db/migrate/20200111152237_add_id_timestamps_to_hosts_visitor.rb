class AddIdTimestampsToHostsVisitor < ActiveRecord::Migration[6.0]
  def change
    add_column :hosts_visitors, :id, :primary_key
    add_column :hosts_visitors, :created_at, :datetime, null: false, default: Time.zone.now
    add_column :hosts_visitors, :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
