class CreateCourseListsGenericCourses < ActiveRecord::Migration[5.1]
  def change
    create_join_table :course_lists, :courses do |t|
      t.index [:course_list_id, :course_id]
      t.index [:course_id, :course_list_id]
    end
  end
end
