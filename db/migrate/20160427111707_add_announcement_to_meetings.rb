class AddAnnouncementToMeetings < ActiveRecord::Migration[4.2]
  def change
    add_column :meetings, :announcement_i18n, :jsonb, default: {}, null: false
  end
end
