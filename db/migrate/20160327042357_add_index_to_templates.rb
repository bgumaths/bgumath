class AddIndexToTemplates < ActiveRecord::Migration[4.2]
  def change
    add_index "templates", %w[path locale format handler partial], name: 'templates_index', using: :btree, unique: true
  end
end
