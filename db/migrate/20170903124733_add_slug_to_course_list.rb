class AddSlugToCourseList < ActiveRecord::Migration[5.1]
  def change
    add_column :course_lists, :slug, :string, unique: true
  end
end
