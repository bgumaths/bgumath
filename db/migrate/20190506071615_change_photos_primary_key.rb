class ChangePhotosPrimaryKey < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |t|
      t.remove :photo_id
    end

    change_table :photos do |t|
      t.remove :id
      t.primary_key :id, :bigint
    end

    add_reference :users, :photo, foreign_key: true, index: {unique: true}
  end

  #def data
  #  Photo.all.each do |ph|
  #    att = ActiveStorage::Attachment.find(ph.image_id) rescue next
  #    ph.image_attachment = att
  #    ph.save!
  #    if ph.imageable&.photo_uuid == ph.uuid
  #      ph.imageable.photo = ph
  #      ph.imageable.save!
  #    end
  #  end
  #end
end
