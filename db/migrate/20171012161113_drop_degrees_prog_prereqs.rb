class DropDegreesProgPrereqs < ActiveRecord::Migration[5.1]
  def change
    drop_table :degrees_prog_prereqs
  end
end
