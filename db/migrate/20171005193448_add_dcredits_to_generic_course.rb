class AddDcreditsToGenericCourse < ActiveRecord::Migration[5.1]
  def change
    add_column :generic_courses, :dcredits, :integer
  end
end
