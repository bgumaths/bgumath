class AddIdTimestampsToResearchGroupsSeminar < ActiveRecord::Migration[6.0]
  def change
    add_column :research_groups_seminars, :id, :primary_key, null: false
    add_column :research_groups_seminars,
      :created_at, :datetime, null: false, default: Time.zone.now
    add_column :research_groups_seminars,
      :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
