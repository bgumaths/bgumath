class CreateDegrees < ActiveRecord::Migration[5.1]
  def change
    create_table :degrees do |t|
      t.jsonb :name_i18n, default: {}, null: false
      t.jsonb :description_i18n, default: {}, null: false
      t.string :slug

      t.timestamps
    end
    add_index :degrees, :name_i18n
    add_index :degrees, :slug, unique: true
  end

end
