class AddParentIdToProgPrereq < ActiveRecord::Migration[5.1]
  def change
    add_reference :prog_prereqs, :parent, index: true
  end
end
