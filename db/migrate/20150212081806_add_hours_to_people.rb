class AddHoursToPeople < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :hours, :string
  end
end
