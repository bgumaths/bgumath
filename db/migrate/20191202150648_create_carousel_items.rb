class CreateCarouselItems < ActiveRecord::Migration[6.0]
  def change
    create_table :carousel_items do |t|
      t.datetime :activates
      t.datetime :expires
      t.integer :interval
      t.integer :order
      t.string :url
      t.string :type, null: false
      t.references :resource, polymorphic: true

      t.timestamps
    end
  end
end
