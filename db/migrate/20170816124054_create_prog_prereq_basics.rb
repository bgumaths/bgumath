class CreateProgPrereqBasics < ActiveRecord::Migration[5.1]
  def change
    create_table :prog_prereq_basics do |t|
      t.integer :minc
      t.integer :maxc
      t.float :minp
      t.float :maxp
      t.references :course_list, foreign_key: true

      t.timestamps
    end
  end
end

