class AddWebpageToDepartment < ActiveRecord::Migration[5.2]
  def change
    add_column :departments, :webpage, :string
  end
end
