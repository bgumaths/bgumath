require File.join(File.dirname(__FILE__), 'base_migration')
class ChangeFirstLastInUsers < BaseMigration
  def change
    change_column_hstore_jsonb :users, :first_i18n
    change_column_hstore_jsonb :users, :last_i18n
  end
end
