class AddIdTimestampsToIssuesStaff < ActiveRecord::Migration[6.0]
  def change
    add_column :issues_staffs, :id, :primary_key
    add_column :issues_staffs, :created_at, :datetime, null: false, default: Time.zone.now
    add_column :issues_staffs, :updated_at, :datetime, null: false, default: Time.zone.now
  end
end
