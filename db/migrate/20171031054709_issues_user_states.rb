class IssuesUserStates < ActiveRecord::Migration[5.1]
  def change
    change_column_default :issues_users, :active, true
    change_column_default :issues_users, :approved, true
    change_column_default :issues_users, :confirmed, true
    add_column :issues_users, :pass_changed, :boolean, default: false
  end
end
