class ChangeVisibilityOfRoles < ActiveRecord::Migration[4.2]
  def change
    change_column :roles, :visibility, :integer, default: 2, null: false
  end
end
