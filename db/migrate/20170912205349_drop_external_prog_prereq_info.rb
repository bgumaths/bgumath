class DropExternalProgPrereqInfo < ActiveRecord::Migration[5.1]
  def change
    drop_table :external_prog_prereq_infos
  end
end
