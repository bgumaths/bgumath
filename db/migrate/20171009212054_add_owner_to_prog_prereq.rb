class AddOwnerToProgPrereq < ActiveRecord::Migration[5.1]
  def change
    add_reference :prog_prereqs, :degree, foreign_key: true, index: true
  end

  def data
    ProgPrereq.all.each do |pp|
      if pp.parent_id.present?
        next
      else
        if pp.degree_ids.count == 1
          pp.degree_id = pp.degree_ids.first
        else
          raise "#{pp.name} has #{pp.degree_ids.count} degrees"
        end
      end
      pp.save!
    end
  end
end
