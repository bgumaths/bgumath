class CreateResearchGroups < ActiveRecord::Migration[4.2]
  def change
    create_table :research_groups do |t|
      t.string :name

      t.timestamps null: false
    end
    add_index :research_groups, :name
  end
end
