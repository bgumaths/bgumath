class DropAdminsSeminars < ActiveRecord::Migration[4.2]
  def change
    drop_table :admins_seminars
  end
end
