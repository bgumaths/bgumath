class AddCourseListsOrderToProgPrereqs < ActiveRecord::Migration[5.1]
  def change
    add_column :prog_prereqs, :course_lists_order, :bigint, array: true, default: []
  end
end
