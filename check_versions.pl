#!/usr/bin/env perl

# $Id$

use 5.10.0;
use warnings;
use integer;
use English qw(-no_match_vars);
use IPC::System::Simple qw(system);
use autodie qw(:all);

use Getopt::Long qw(:config gnu_getopt auto_help auto_version);
our %Opts = ();
GetOptions(\%Opts);

our $FILE = 'config/initializers/versions.rb';
open my $fh, '<', $FILE;

while(<$fh>) {
    if ( /(.*)_VERSION=(.*)/ ) {
        my $what = $1;
        my $cur = $2;
        say $what;
        say "  Current: $cur";
    } else {
        my $cmd;
        if ( /^#% (.*)/ ) {
            $cmd = "lynx -dump 'https://api.cdnjs.com/libraries/$1?fields=version'";
        } elsif (/^#\* (.*)/) {
            $cmd = $1;
        }
        if ( $cmd ) {
            $up = `$cmd`;
            say "  Upstream: $up";
        }
    }
}


__DATA__

# start of POD

=head1 NAME

check_versions -

=head1 SYNOPSIS

    check_versions

=head1 OPTIONS

=over

=item --help,-?

Give a short usage message and exit with status 1

=item --version

Print a line with the program name and exit with status 0

=back

=head1 ARGUMENTS

=head1 DESCRIPTION

=head1 FILES

=head1 BUGS

=head1 SEE ALSO

=head1 AUTHOR

Moshe Kamensky  (E<lt>moshe.kamensky@gmail.comE<gt>) - Copyright (c) 2017

=head1 LICENSE

This program is free software. You may copy or 
redistribute it under the same terms as Perl itself.

=cut

