# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

#role :app, %w{deploy@example.com}
role :web, %w{wwwmath@se-web.cs.bgu.ac.il}
role :db, %w{wwwmath@se-web.cs.bgu.ac.il}
role :app, %w{wwwmath@se-web.cs.bgu.ac.il}
#role :db, %w{wwwmath@localhost}, :primary => true
#role :db,  %w{deploy@example.com}

set :rbenv_custom_path, '/var/lib/gitlab-runner/.rbenv'
set :deploy_to, '/var/www/math'
set :repo_url, 'http://se-web.cs.bgu.ac.il/git/wwwmath/bgumath.git'

current_branch = `git rev-parse --abbrev-ref HEAD`.strip

# use the branch specified as a param, then use the current branch. If all 
# fails use master branch
set :branch, ENV['branch'] || current_branch || 'master' # you can use the 
# 'branch' parameter on deployment to specify the branch you wish to deploy
#set :branch, 'master'
#set :branch, 'ruby3'

after 'deploy:restart', 'sitemap:refresh'

set :passenger_restart_options,
  -> { "#{deploy_to} --ignore-app-not-running --ignore-passenger-not-running" }
# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

#server 'example.com', user: 'deploy', roles: %w{web app}, my_property: 
#:my_value


# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# And/or per server (overrides global)
# ------------------------------------
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }
