# config valid only for current version of Capistrano
#lock '3.3.5'

set :application, 'bgumath'
#set :repo_url, 'ssh://git@altssh.bitbucket.org:443/kamensky/bgumath.git'
# DON'T replace cicero1 with lvs
#set :repo_url, 
#'ssh://wwwmath@se-web:/~/mnt/lvs/git-data/repositories/wwwmath/bgumath.git'
set :repo_url, 'https://www.math.bgu.ac.il/git/wwwmath/bgumath.git'
#set :repo_url, 'http://se-web.cs.bgu.ac.il/git/wwwmath/bgumath.git'
# there seems to be an issue with expanding $HOME, set path explicitly (TODO)
set :rbenv_type, :user
set :rbenv_ruby, `rbenv local`.chomp
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all
set :use_sudo, false
set :systemd_unit_file, 'que.service'
set :assets_prefix, 'vite'


# Default branch is :master
set :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
append :linked_files, *%w[
config/database.yml
config/master.key
config/server.crt
config/server.key
config/gdrive.json
config/app.yml
public/robots.txt
public/BingSiteAuth.xml
public/googlec3a28d628f282445.html
]

# Default value for linked_dirs is []
append :linked_dirs, *%w[
  storage
  lists
  log
  public
  cache/rails-latex
  issues
  .bundle
]

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  namespace :check do
    before :linked_files, :set_master_key do
      on roles(:app) do
        unless test("[ -f #{shared_path}/config/master.key ]")
          upload! 'config/master.key', "#{shared_path}/config/master.key"
        end
      end
    end
  end

  after :starting, 'que:reload'

  after :updated, 'que:stop'

  after :published, 'que:start'

  after :failed, 'que:restart'

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  desc "Symlink shared config files"
  task :symlink_config_files do
    on roles(:web) do
      execute :ln, " -s #{ deploy_to }/shared/config/database.yml #{ current_path }/config/database.yml"
    end
  end

  desc "Restart Passenger app"
  task :restart do
    on roles(:web) do
      execute :touch, "#{ File.join(current_path, 'tmp', 'restart.txt') }"
    end
  end

  namespace :assets do
    namespace :webp do

      desc 'Updates mtime for webp images'
      task :touch => [:set_rails_env] do
        on roles(:web) do
          execute <<-CMD.gsub(/[\r\n\t]?/, '').squeeze(' ').strip
          cd #{release_path.join('public/assets')};
          for asset in $(
            find . -regex ".*\.webp$" -type f | LC_COLLATE=C sort
          ); do
            echo "Update webp asset: $asset";
            touch -c -- "$asset";
          done
          CMD
        end
      end
    end
  end
end

#after 'deploy:updated', 'deploy:assets:webp:touch'

# Already done via linked_files
#after :deploy, 'deploy:symlink_config_files'
after :deploy, 'deploy:restart'

