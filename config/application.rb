require_relative 'boot'

 require "rails"
 # Pick the frameworks you want:
 require "active_model/railtie"
 require "active_job/railtie"
 require "active_record/railtie"
 require "active_storage/engine"
 require "action_controller/railtie"
 require "action_mailer/railtie"
 #require "action_mailbox/engine"
 #require "action_text/engine"
 require "action_view/railtie"
 #require "action_cable/engine"
 # require "sprockets/railtie"
 #require "rails/test_unit/railtie"

module Gollum
  GIT_ADAPTER = 'rugged'
end

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Bgumath
  class Application < Rails::Application
    # In order for Graphiti to generate links, you need to set the routes 
    # host.
    # When not explicitly set, via the HOST env var, this will fall back to
    # the rails server settings.
    # Rails::Server is not defined in console or rake tasks, so this will only
    # use those defaults when they are available.
    routes.default_url_options[:host] = ENV.fetch('HOST') do
      if defined?(Rails::Server)
        argv_options = Rails::Server::Options.new.parse!(ARGV)
        "http://#{argv_options[:Host]}:#{argv_options[:Port]}"
      end
    end
    config.load_defaults '7.0'
    # Settings in config/environments/* take precedence over those specified  here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Jerusalem'
    config.beginning_of_week = :sunday

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.i18n.available_locales = [:en, :he]
    # allow default interpolation argument
    I18n.config.missing_interpolation_argument_handler = ->(key, args, str) {
      if args.key?(:interpolation_default)
        I18n.interpolate(str, {key => args[:interpolation_default]}.merge(args))
      else
        raise I18n::MissingInterpolationArgument.new(key, args, str)
      end
    }

    config.action_view.form_with_generates_remote_forms = true
    config.action_view.prefix_partial_path_with_controller_namespace = false
    config.eager_load_paths += Dir[Rails.root.join(*%w[spec mailers previews])]
    config.add_autoload_paths_to_load_path = false
    config.autoload_paths += Dir[Rails.root.join(*%w[lib auto **])]
    config.eager_load_paths += Dir[Rails.root.join(*%w[lib auto **])]

    config.generators do |g|
      g.test_framework :rspec,
        :fixtures => true,
        :view_specs => false,
        :helper_specs => false,
        :routing_specs => true,
        :controller_specs => true,
        :request_specs => true
      g.fixture_replacement :factory_bot, :dir => "spec/factories"
      g.template_engine :erb
      g.helper false
      g.jbuilder false
      g.stylesheets false
      g.javascripts false
      g.assets false
      #g.orm :active_record, primary_key_type: :uuid
    end

    #config.middleware.insert 0, Rack::UTF8Sanitizer, 
    #additional_content_types: ['application/vnd.api+json'],
    #  strategy: :exception

    # vips currently buggy, 
    # https://github.com/janko/image_processing/issues/84
    #config.active_storage.variant_processor = :vips
    config.active_storage.replace_on_assign_to_many = true

    require './app/middleware/validate_request_params'
    config.middleware.insert_before Rack::Head, ::ValidateRequestParams

    #config.rails_semantic_logger.quiet_assets = true
    config.rails_semantic_logger.quiet_assets = false
    # profiling
    #config.middleware.use(StackProf::Middleware, enabled:true, mode: :cpu, interval: 1000, save_every: 5)
    

    #ActiveRecord::SessionStore::Session.serializer = :hybrid
    ActiveRecord::SessionStore::Session.serializer = :null

    # following seems to happen automatically
    #config.middleware.use(Rack::Attack)

    I18n.backend.class.send(:include, I18n::Backend::Cascade)

    # get proper jsonapi response for exceptions
    config.debug_exception_response_format = :api
    #config.middleware.delete ActionDispatch::DebugExceptions
  end
end

module ActiveModel
  class Railtie < Rails::Railtie
    generators do |app|
      Rails::Generators.configure! app.config.generators
      require_relative '../lib/rails/generators/extensions'
    end
  end
end

# https://stackoverflow.com/questions/34103892/trace-error-in-rack-middleware
# Trace each middleware module entry/exit. Freely adapted from
# https://github.com/DataDog/dd-trace-rb/issues/368 "Trace each middleware in the Rails stack"
module MiddlewareTracer
  def call(env)
    #Rails.logger.debug { "MiddleWare: entry #{self.class.name}" }
    status, headers, response = super(env)
    #Rails.logger.debug { "MiddleWare: #{self.class.name}, returning with 
    #status #{status}" }
    [status, headers, response]
  end
end

# Instrument the middleware stack after initialization so that we
# know the stack won't be changed afterwards.
Rails.configuration.after_initialize do
  Rails.application.middleware.each do |middleware|
    klass = middleware.klass

    # There are a few odd middlewares that aren't classes.
    #klass.prepend(MiddlewareTracer) if klass.respond_to?(:prepend)
  end
end
