SimpleForm.setup do |config|
  config.wrappers :ranged_datetime, tag: 'div', class: 'form-group col', 
    error_class: 'has-error' do |b|
    b.use :html5
    b.use :placeholder
    b.optional :readonly

    b.use :label
    b.use :input, class: 'form-control'

    b.use :error, wrap_with: { tag: 'span', class: 'form-text' }
    b.use :hint,  wrap_with: { tag: 'p', class: %w[form-text form-hint] }
  end
end
