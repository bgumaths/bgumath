# frozen_string_literal: true

module Mail
  module Encodings
    module EncodeQuotedPrintableWithoutCR
      def encode(str)
        ::Mail::Utilities.to_crlf(super(::Mail::Utilities.to_lf(str)))
      end
    end
  end
end

Mail::Encodings::QuotedPrintable.singleton_class.prepend(Mail::Encodings::EncodeQuotedPrintableWithoutCR)

