NAME_MAX_LEN = 400
TEXT_MAX_LEN = 2000
EMAIL_MAX_LEN = 250
PASSWD_MIN_LEN = 6
VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
PAM_SERVICE = 'other'
ML_BASE = '.'
PHOTO_HEIGHT = 128
PHOTO_WIDTH = 108
TZID = Time.zone.tzinfo.name

IMG_TRANS = {
  sm: { resize_to_fit: [PHOTO_WIDTH / 2, PHOTO_HEIGHT / 2] },
  normal: { resize_to_fit: [PHOTO_WIDTH, PHOTO_HEIGHT] },
  large: { resize_to_fit: [PHOTO_WIDTH * 2, PHOTO_HEIGHT * 2] },
}.freeze

