InertiaRails.configure do | config |
  config.version = ViteRuby.digest # used for asset verioning
  # config.layout = 'some_other_file' # use this to change the default layout file that inertia will use. Default it uses application.html.erb.
end
