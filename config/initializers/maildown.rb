# frozen_string_literal: true

#Maildown::MarkdownEngine.set_text do |text|
  #Kramdown::Document.new(text).tap(&:to_remove_html_tags).to_kramdown
#end

Maildown::MarkdownEngine.set_html do |text|
  Kramdown::Document.new(text, input: 'GFM', hard_wrap: false).to_html
end

