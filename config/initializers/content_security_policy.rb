# Be sure to restart your server when you modify this file.

# Define an application-wide content security policy
# For further information see the following documentation
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy

default = %i[self https blob]
default += %i[http] if Rails.env.development?

Rails.application.config.content_security_policy do |policy|
  policy.default_src *default
  policy.font_src    *default, :data, :blob
  policy.img_src     *default, :data, :blob
  #policy.object_src  :none
  if Rails.env.development?
    policy.connect_src *default,
      "http://#{ViteRuby.config.host_with_port}",
      "ws://#{ViteRuby.config.host_with_port}"
  end
  # include unsafe_, since we use a nonce
  policy.script_src  *default, :unsafe_eval, :unsafe_inline, :strict_dynamic
    # Allow @vite/client to hot reload javascript changes in development
    policy.script_src *policy.script_src, :unsafe_eval, "http://#{ ViteRuby.config.host_with_port }" if Rails.env.development?

    # You may need to enable this in production as well depending on your setup.
    policy.script_src *policy.script_src, :blob if Rails.env.test?

  policy.style_src   *default, :unsafe_inline
    # Allow @vite/client to hot reload style changes in development
    policy.style_src *policy.style_src, :unsafe_inline if Rails.env.development?

#   # Specify URI for violation reports
#   # policy.report_uri "/csp-violation-report-endpoint"
end

# If you are using UJS then enable automatic nonce generation
# causes server errors
Rails.application.config.content_security_policy_nonce_generator = -> request { SecureRandom.base64(16) }
# apply nonce only to scripts and not to styles, since mathjax inserts 
# styles into the doc
# https://github.com/mathjax/MathJax/issues/256
Rails.application.config.content_security_policy_nonce_directives = %w[script-src].freeze

# Report CSP violations to a specified URI
# For further information see the following documentation:
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy-Report-Only
# Rails.application.config.content_security_policy_report_only = true

