#LatexToPdf.config.merge! command: 'xelatex', parse_runs: 2
LatexToPdf.config.merge! command: 'latexmk', 
  arguments: ['-r',  Rails.root.join('config', 'latexmkrc').to_s], 
  default_arguments: [], 
  preservework: true,
  # following doesn't work in production, since Rails.root resolves 
  # symlinks, and may become invalid with older releases. So we use relative 
  # paths
  #basedir: Rails.root.join('cache', 'rails-latex'),
  basedir: File.join('cache', 'rails-latex'),
  parse_runs: 1

