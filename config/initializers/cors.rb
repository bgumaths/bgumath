# frozen_string_literal: true
Rails.application.config.middleware.insert_before 0, Rack::Cors,
      debug: true, logger: -> { Rails.logger } do
      allow do
        origins 'localhost:3000', '127.0.0.1:3000', 'localhost:3333',
          'localhost:5000', 
                /\Ahttp:\/\/10\.0\.0\.\d{1,3}(:\d+)?\z/,
                /\Ahttp:\/\/192\.168\.1\.\d{1,3}(:\d+)?\z/,
                /\Ahttps:\/\/bgumath\.github\.io\z/
        resource '*', headers: :any,
          methods: %i[get post put patch delete options head],
          credentials: true
      end
    end

