ActionController::Renderers.add :yaml do |obj, options|
  str = obj.respond_to?(:to_yaml) ? obj.to_yaml(**(options[:to_yaml] || {})) : obj.to_s
  render text: str, type: :yaml
end

ActionController::Renderers.add :csv do |obj, options|
  str = obj.respond_to?(:to_csv) ? obj.to_csv(**(options[:to_csv] || {})) : obj.to_s
  send_data str, type: :csv
end
