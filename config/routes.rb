Rails.application.routes.draw do
  # the following should go first, for helpers to pick it and not the api
  resources :ac_years

  scope path: ApplicationResource.endpoint_namespace, defaults: { format: :jsonapi } do
    resource :user_session, controller: 'user_session_api',
      only: %i[show create update destroy]
    resources :issues_issues_staffs, controller: 'api'
    resources :replaced_by_replaces, controller: 'api'
    resources :allows_requires, controller: 'api'
    resources :course_lists_courses, controller: 'api'
    resources :hosts_visitors, controller: 'api'
    resources :hosts_postdocs, controller: 'api'
    resources :students_supervisors, controller: 'api'
    resources :departments_generic_courses, controller: 'api'
    resources :course_lists_prog_prereqs, controller: 'api'
    resources :users_roles, controller: 'api'
    resources :members_research_groups, controller: 'api'
    resources :research_groups_seminars, controller: 'api'
    resources :seminars, controller: 'api'
    resources :basic_prog_prereqs, controller: 'api'
    resources :ac_years, controller: 'api'
    resources :course_lists, controller: 'api'
    resources :courses, controller: 'api'
    resources :degree_plans, controller: 'api'
    resources :degrees, controller: 'api'
    resources :events, controller: 'api'
    resources :free_plan_items, controller: 'api'
    resources :generic_courses, controller: 'api'
    resources :issues, controller: 'issues_api'
    resources :issue_responses, controller: 'issues_api'
    resources :issues_users, controller: 'issues_api'
    resources :issues_student_reps, controller: 'issues_api'
    resources :issues_aguda_reps, controller: 'issues_api'
    resources :issues_staffs, controller: 'api'
    resources :meetings, controller: 'api'
    resources :plan_items, controller: 'api'
    resources :prog_prereqs, controller: 'api'
    resources :precise_prog_prereqs, controller: 'api'
    resources :research_groups, controller: 'api'
    resources :research_groups_seminars, controller: 'api'
    resources :roles, controller: 'api'
    resources :shnatons, controller: 'api'
    resources :term_plans, controller: 'api'
    resources :terms, controller: 'terms_api'
    resources :users, controller: 'users_api'
    resources :academics, controller: 'api'
    resources :members, controller: 'api'
    resources :regulars, controller: 'api'
    resources :kameas, controller: 'api'
    resources :students, controller: 'api'
    resources :phds, controller: 'api'
    resources :mscs, controller: 'api'
    resources :postdocs, controller: 'api'
    resources :adjuncts, controller: 'api'
    resources :admins, controller: 'api'
    resources :externals, controller: 'api'
    resources :virtuals, controller: 'api'
    resources :visitors, controller: 'api'
    resources :departments, controller: 'api'
    resources :photos, controller: 'api'
    resources :settings, controller: 'api'
    resources :carousel_items, controller: 'api'
    resources :carousel_item_records, controller: 'api'
  end

  ### regular routes

  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/  do

    #root to: 'pages#show', id: 'home', defaults: { format: 'html' }
    root to: 'ipages#home'
    resources :user_sessions, only: %i[create destroy]
    post 'login'   => 'user_sessions#create'
    get 'logout'  => 'user_sessions#destroy'
    delete 'logout'  => 'user_sessions#destroy'
    get 'layout' => 'pages#layout', id: 'layout'
    get 'carousel' => 'admin/carousel_items'

    # saml
    # we never use the saml_idp login form
    get '/saml/auth' => 'saml_idp#create'
    get '/saml/metadata' => 'saml_idp#show'
    post '/saml/auth' => 'saml_idp#create'
    match '/saml/logout' => 'saml_idp#logout', via: [:get, :post, :delete]

    # (semi-)static and inertia
    inertia 'people/positions' => 'People/Positions'

    inertia 'research/friedman' => 'research/friedman'
    get 'research/eckmann' => 'pages#show', id: 'eckmann'
    get 'research/center' => 'ipages#center'

    get 'teaching/grad' => 'pages#show', id: 'grad'

    inertia 'community/visitors' => 'community/visitors'
    get 'community/history' => 'pages#show', id: 'history'
    get 'community/school' => 'pages#show', id: 'school'

    inertia 'internal/resources' => 'StaticCards'
    inertia 'internal/support' => 'StaticCards'
    inertia 'internal/about' => 'Static'

    namespace :people do
      resources :members, :admins, :students, :postdocs, :adjuncts, :virtuals #, only: [:index, :new, :show]
      # aliases with additional helper methods.
      # XXX May want to reconsider this, as it sends /people/students to 
      # People::PhdsControllers, which does not exist
      resources :regulars, path: :members
      resources :kameas, path: :members
      #resources :phds#, path: :students
      #resources :mscs#, path: :students

      resources :users do
        collection do
          post 'delete'
          post 'upload'
          post 'generate_api_key'
        end
        member do
          post 'photo'
          patch 'photo'
          patch 'retire'
        end
      end

      resources :visitors, only: [:new, :edit, :create, :update, :destroy]
      get 'visitors' => 'postdocs#index'


    end

    namespace :research do
      # seminars and events now displayed
      get 'weekly', to: redirect('/')
      get 'faculty' => '/people/members#research'
      resources :research_groups do
        collection do
          get 'faculty'
        end
      end
      concern :seminar_c do
        resources :seminars do
          member do
            post 'create_ml'
          end
          resources :meetings, controller: 'seminar/meetings' do
            member do
              get 'email'
            end
          end
        end
      end
      concerns :seminar_c
      resources :terms, only: [], path: '' do
        concerns :seminar_c
      end
      resources :events do
        member do
          get 'email'
        end
      end
    end

    namespace :teaching do
      get 'hours' => '/people/users#hours'
      post 'hours' => '/people/users#reset_hours'
      #get 'undergrad', to: 
      #redirect('//in.bgu.ac.il/teva/math/Pages/ToarRishon.aspx')
      get 'catalog',
        to: redirect('https://bgu4u.bgu.ac.il/pls/scwp/!app.gate?app=ann')
      #to:redirect('//bgu4u.bgu.ac.il/pls/scwp/%21sc.AnnualSearchResults?on_course_department=201')
      resources :generic_courses
      resources :generic_courses, path: 'generic-courses'

      concern :course_c do
        resources :courses do
          collection do
            get 'gsheets_update'
          end
        end
      end
      concerns :course_c

      #resource :term, controller: 'terms', only: [:edit, :show, :update], 
      #  as: :cterm
      get '/cterm', to: redirect(path: '/teaching/courses')
      concern :shnaton_c do
        resource :shnaton, path: :handbook do
          resources :prog_prereqs
          resources :degrees, except: :index do
            collection do
              get  '', action: :show, controller: 'shnatons'
            end
            resources :plan_items, only: [:index, :destroy]
            resources :free_plan_items, only: [:index, :destroy]
            resources :prog_prereqs
            resources :degree_plans do
              resources :term_plans
            end
          end
          resources :course_lists
        end
      end
      concerns :shnaton_c
      resources :ac_years, only: [], path: '' do
        concerns :shnaton_c
      end

      resources :issues do
        collection do
          get 'login' => 'issues_user_sessions#new'
          post 'login'   => 'issues_user_sessions#create'
          get 'logout'  => 'issues_user_sessions#destroy'
          delete 'logout'  => 'issues_user_sessions#destroy'
        end
        member do
          post 'send_created'
          post 'send_update'
        end
      end
      resources :issue_responses, only: %i[create update destroy]
      resources :issues_users do
        member do
          delete 'reset_password'
        end
        collection do
          get 'gsheets_update'
        end
      end
      resources :departments

      # terms should be last
      resources :terms, path: '' do
        concerns :course_c
        member do
          get 'populate'
        end
      end

    end

    namespace :community do
      get 'map', 
        to: redirect('http://in.bgu.ac.il/maps/marcus_map.pdf', status: 302)
      get 'wiki', 
        to: redirect('//www.cs.bgu.ac.il/~wwwmath/postdoc/Main', status: 302)
      get 'school',
        to: redirect('https://sites.google.com/view/innaentova/math-for-highschoolers/%D7%9E%D7%93-%D7%9E%D7%AA%D7%9E%D7%98%D7%99%D7%A7%D7%94-%D7%91%D7%93%D7%A8%D7%95%D7%9D', status: 302)
      get 'imu', to: redirect('http://imu.org.il')
      get 'facebook', to: redirect('//www.facebook.com/math.bgu/')
    end

    namespace :internal do
      #get 'mattermost', to: redirect('http://se-web.cs.bgu.ac.il')
      get 'xmail', to: redirect('https://xmail.bgu.ac.il/')
    end

    namespace :admin do
      resource :app_setup, controller: :app_setup, only: [:edit, :show, :update]
      resources :roles
      resources :role_events
      resources :versions, only: [:index, :show, :destroy]
      resources :jobs, only: [:index, :destroy]
      resources :carousel_items
      get :mir, action: :index, controller: :mir
    end
  end

end

