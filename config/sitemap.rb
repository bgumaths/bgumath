# Set the host name for URL creation
SitemapGenerator::Interpreter.send :include, CommonHelper

SitemapGenerator::Sitemap.default_host = "https://www.math.bgu.ac.il"

#SitemapGenerator::Sitemap.sitemaps_path = "shared/"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
  add people_members_path, changefreq: 'monthly', lastmod: lastmod(User), priority: 0.7
  add people_admins_path, changefreq: 'monthly', lastmod: lastmod(User), priority: 1
  add people_students_path, changefreq: 'monthly', lastmod: lastmod(User)
  add people_postdocs_path, changefreq: 'monthly', lastmod: lastmod(User)
  add people_adjuncts_path, changefreq: 'monthly', lastmod: lastmod(User)
  #add people_externals_path, changefreq: 'monthly', lastmod: lastmod(User), 
  #priority: 0.1
  add people_users_path, changefreq: 'monthly', lastmod: lastmod(User)


  add people_positions_path, changefreq: 'yearly', priority: 0.7


  add research_weekly_path, changefreq: 'weekly', lastmod: [Seminar,Event].map{|x| lastmod(x)}.max, priority: 0.8
  add research_faculty_path, changefreq: 'monthly', lastmod: lastmod(User), priority: 0.9
  add research_research_groups_path, changefreq: 'yearly', lastmod: lastmod(ResearchGroup)

  add research_seminars_path, changefreq: 'monthly', lastmod: lastmod(Seminar), priority: 0.8
  Term.default.seminars.find_each do |item|
    next unless item.slug.present?
    add research_seminar_path(item.slug, locale: ''), changefreq: 'daily', lastmod: item.updated_at, priority: 0.7
  end

  add research_events_path, changefreq: 'daily', lastmod: lastmod(Event), priority: 0.8
  Event.from.find_each do |item|
    add research_event_path(item.id, locale: ''), lastmod: item.updated_at, priority: 0.7
  end

  add research_center_path, changefreq: 'monthly', priority: 0.8


  add teaching_hours_path, changefreq: 'weekly', lastmod: lastmod(User), priority: 0.6
  add teaching_grad_path, changefreq: 'yearly', priority: 0.4

  add teaching_cterm_path, changefreq: 'monthly', lastmod: lastmod(Course), priority: 0.7
  Term.default.courses.find_each do |item|
    add teaching_term_course_path(Term.default, item, locale: ''), changefreq: 'weekly', lastmod: item.updated_at
  end



  add community_history_path, changefreq: 'never', priority: 0.3
  add community_school_path, changefreq: 'yearly'
  add community_wiki_path, changefreq: 'daily'

end
