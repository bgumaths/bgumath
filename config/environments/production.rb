Rails.application.routes.default_url_options.merge!(
  { host: 'www.math.bgu.ac.il' }
)

Rails.application.configure do
  #config.action_controller.relative_url_root = '/home'
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true
  # IMPORTANT! do not cache templates, since they are dynamic (depend on 
  # additional params
  config.action_view.cache_template_loading = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  # eager loading causes issues with graphiti
  config.eager_load = false
  #config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?

  # Compress CSS using a preprocessor.
  #config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  #config.assets.compile = false

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  #config.assets.digest = true

  # Enable serving of images, stylesheets, and JavaScripts from an asset 
  # server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Store uploaded files on the local file system (see config/storage.yml for options).
  config.active_storage.service = :local

  # Mount Action Cable outside main process or domain
  # config.action_cable.mount_path = nil
  # config.action_cable.url = 'wss://example.com/cable'
  # config.action_cable.allowed_request_origins = [ 'http://example.com', /http:\/\/example.*/ ]

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  config.force_ssl = true

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :info

  # Prepend all log lines with the following tags.
  config.log_tags = [ :request_id ]

  # Use a different cache store in production.
  config.cache_store = :mem_cache_store

  # Use a real queuing backend for Active Job (and separate queues per  environment).
  # config.active_job.queue_adapter     = :resque
  #config.active_job.queue_adapter     = :async
  config.active_job.queue_adapter     = :que
  # config.active_job.queue_name_prefix = "bgumath_#{Rails.env}"
  config.action_mailer.perform_caching = false

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.delivery_method = :sendmail
  #config.action_mailer.sendmail_settings = {
  #  arguments: %w[-i],
  #}
  config.action_mailer.perform_deliveries = true
  config.action_mailer.logger = config.logger

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  # bgumath: Keep this false, since missing translation signals exclusion of 
  # locale
  config.i18n.fallbacks = false

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Use a different logger for distributed setups.
  # require 'syslog/logger'
  # config.logger = ActiveSupport::TaggedLogging.new(Syslog::Logger.new 'app-name')

  if ENV["RAILS_LOG_TO_STDOUT"].present?
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger = ActiveSupport::TaggedLogging.new(logger)
  end

  config.rails_semantic_logger.rendered = false

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false
  # dump as sql, since we have psql specific stuff
  config.active_record.schema_format = :sql

  config.x.saml_idp_base = 'https://www.math.bgu.ac.il'
  config.x.saml_clients = {
    "https://www.math.bgu.ac.il/git" => {
      #fingerprint: "9E:65:2E:03:06:8D:80:F2:86:C7:6C:77:A1:D9:14:97:0A:4D:F4:4D",
      metadata_url: "https://www.math.bgu.ac.il/git/users/auth/saml/metadata",
      #acs_url: "https://www.math.bgu.ac.il/git/users/auth/saml/acs",
    },
  }
  # allow web console in production
  #config.web_console.development_only = false
  
  # require config/master.key
  config.require_master_key = true
end
