Rails.application.routes.default_url_options.merge!(
  { host: 'localhost', port: ENV.fetch('BGUMATH_PORT') { 5000 } }
)

Rails.application.configure do
  # Settings specified here will take precedence over those in 
  # config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
	# eager loading causes issues with graphiti
  #config.eager_load = false
  config.eager_load = true

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true
    config.action_controller.enable_fragment_cache_logging = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Store uploaded files on the local file system (see config/storage.yml for options).
  config.active_storage.service = :local

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.delivery_method = :file
  config.action_mailer.file_settings = {
    location: Rails.root.join('tmp', 'mails')
  }
  #config.action_mailer.delivery_method = :letter_opener
  config.action_mailer.perform_deliveries = true
  config.action_mailer.default_options = {from: 'wwwmath@math.bgu.ac.il'}
  #config.active_job.queue_adapter = :inline
  config.active_job.queue_adapter = :que
  config.action_mailer.perform_caching = false
  

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load
  # dump as sql, since we have psql specific stuff
  config.active_record.schema_format = :sql
  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  #config.assets.debug = true
  #config.assets.js_compressor = :uglifier

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  #config.assets.digest = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  #config.assets.raise_runtime_errors = true

  # Suppress logger output for asset requests.
  #config.assets.quiet = true

  # Raises error for missing translations.
  # Don't do this, since we check for blank values
  #config.action_view.raise_on_missing_translations = true

  config.action_view.annotate_rendered_view_with_filenames = true


  config.web_console.allowed_ips = '192.168.0.0/16'
  # betterErrors overrides api exception handling
  if false #defined?(BetterErrors)
    BetterErrors::Middleware.allow_ip! '10.0.0.215'
    BetterErrors::Middleware.allow_ip! '192.168.1.215'
    BetterErrors::Middleware.allow_ip! '132.72.44.143'
    BetterErrors.use_pry!
  end

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  config.x.saml_idp_base =
    "http://10.0.0.215:#{Rails.application.routes.default_url_options[:port]}"
  config.x.saml_clients = {
    "http://10.0.0.215:4000/saml/metadata" => {
      #fingerprint: "9E:65:2E:03:06:8D:80:F2:86:C7:6C:77:A1:D9:14:97:0A:4D:F4:4D",
      metadata_url: "http://10.0.0.215:4000/saml/metadata",
      acs_url: "http://10.0.0.215:4000/saml/acs",
    },
  }

  config.require_master_key = true

  config.hosts << 'detritus'

end

