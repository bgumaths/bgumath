ORD_HE = %w[ראשון שני שלישי רביעי חמישי שישי שביעי שמיני תשיעי עשירי].freeze

{
  he: {
    number: {
      nth: {
        ordinalized: lambda do |_key, number:, **_opts|
          num = number.to_i
          ORD_HE[num - 1] || num
        end
      },
    },
  }
}
