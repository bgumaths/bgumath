# frozen_string_literal: true

class MultiselectInput < SimpleForm::Inputs::CollectionInput
  include ApplicationHelper
  def input(wrapper_options = nil)
    @label_method, @value_method = detect_collection_methods
    opts = merge_wrapper_options(input_html_options, wrapper_options)
    selected = object.public_send(attribute_name)
    msid = "#{object_name}_#{attribute_name}"
    template.tag.div(class: %w[row multiselect-row]) do
      template.tag.div(class: %w[col-5]) do
        select_card(tt(:available), "#{attribute_name}_avail",
                    collection,
                    **opts.deep_merge(multiple: true,
                                      data: {right: "##{msid}"},
                                      class: %w[multiselect multiselecta]))
      end <<
        template.tag.div(class: %w[col-2 btn-group-vertical]) do
          ar_button("#{msid}_avail_move_up", :up) <<
            ar_button("#{msid}_avail_rightAll", :plus) <<
            ar_button("#{msid}_avail_rightSelected", is_rtl? ? :previous : :next) <<
            ar_button("#{msid}_avail_leftSelected", is_rtl? ? :next : :previous) <<
            ar_button("#{msid}_avail_leftAll", :minus) <<
            ar_button("#{msid}_avail_move_down", :down)
        end <<
        template.tag.div(class: %w[col-5]) do
          select_card(tt(:selected), attribute_name,
                      collection.select do |el|
                        selected.include?(el.is_a?(Array) ? el[0].id : el.id)
                      end,
                      **opts.deep_merge(multiple: true, class: %w[multiselect]))
        end
    end.html_safe
  end

  protected

  def ar_button(id, icon)
    template.button_tag(type: :button, id:, class: %w[btn btn-sm multiselect]) do
      glyphicon(icon)
    end
  end

  def select_card(title, attri, coll, **opts)
    newcoll = coll.map do |x|
      if x.is_a?(Array)
        x
      elsif x.respond_to?(:to_tooltip)
        [x, { title: x.to_tooltip, 'data-toggle' => 'tooltip' }]
      else
        [x, {}]
      end
    end
    template.tag.div(class: %w[card multiselect]) do
      template.tag.h6(title, class: 'card-header text-center') <<
        @builder.collection_select(attri, newcoll,
                                   ->(it) { it[0].public_send(@value_method) },
                                   ->(it) { it[0].public_send(@label_method) },
                                   opts.deep_merge(selected: []), opts) <<
        (block_given? ? template.tag.div(yield, class: 'card-footer') : '')
    end
  end
end

