# frozen_string_literal: true

###### File upload #######
# Uploading files looks as follows: The current files appear first, produced 
# by the #current_items method, implemented in sub-classes. Then there is a 
# file-drop area, and an input to upload files.
# When a new file is uploaded, it is created on the server by 
# ApplicationContrller#create_upload, but not yet attached to the object 
# (which could be new).  The html for the new attached file is returned via 
# ajax by ApplicationContrller#update_attachments, along with the array of 
# signed ids for the created blobs. The `fileupload` javascript then appends 
# the html to the containing `.files` element. It also insert the signed ids 
# as the value of the `input.file-signed-ids`, if it exists (some uploaders 
# may store the signed id by different means, see the photos uploader for 
# example).
#
# Deleting attachments: the html of each element may contain a delete button.  
# Such a button should have `.destroy-attach` class. Clicking that will 
# trigger removal of the containing element with class `.attach-container`, 
# and if an element with `.destroy-button` exists, the id stored in its 
# `data-signed-id` will be removed from `input.file-signed-ids`.
#
# Hence, the whole input has the following structure:
#
#     <div ... class="files">
#
#       <div class="attach-container">        --+
#              .                                |
#              .                                |
#              .                                |
#         <tag ... class="destroy-attach">      | One element, as produced
#           <tag ... class="destroy-button"     | by `item_html`
#                data-signed-id="a">            |
#           </tag>                              |
#         </tag>                                |
#       </div>                                --+
#                   .
#                   .
#                   .
#     </div>
#
#     <input ... class="file-signed-ids" value="[a,b,c]">
#     <div ... class="file-drop"></div>
#     <input type="file" ...>
#
class AttachmentInput < SimpleForm::Inputs::FileInput
  def input(wrapper_options = nil)
    opts = merge_wrapper_options(input_html_options, wrapper_options)

    (current_items(**opts) + file_field(**opts)).html_safe
  end

  protected

  def file_field(name = attribute_name, **opts)
    # we need to override readonly and disabled attributes set in 
    # data_form_builder, since this is normally a fake field, so will not 
    # appear as permitted in the policy. If these are actually required, 
    # pass a (true) value which is different from `true`
    oopts = if opts[:readonly] == true
              { readonly: false, disabled: false }
            else
              {}
            end
    template.tag.div(class: %w[file-drop text-center]) do
      template.tag.p(template.tt('file-drop'), class: %w[lead]) +
        template.tag.p(template.tt('or', what: ''))
    end + 
      @builder.file_field(name, multiple: name.to_s == name.to_s.pluralize,
                                class: %w[form-control-file], **opts, **oopts)
  end
end

