# frozen_string_literal: true

# copied from simple_form_fancy_uploads, modified to support multiple files
class AttachmentPreviewInput < AttachmentInput
  protected

  def current_items(name = attribute_name, **_opts)
    template.tag.div(class: %w[row files]) do
      template.render(object.decorate.send(name) || [], field: name) || ''
    end +
      @builder.input(
        "#{name}_signed_ids", as: :hidden, wrapper: false,
                              input_html: { class: %w[file-signed-ids] }
      )
  end
end

