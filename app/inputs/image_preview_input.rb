# frozen_string_literal: true

# copied from simple_form_fancy_uploads, modified to support multiple files,
# jquery crop, etc
class ImagePreviewInput < AttachmentInput
  protected

  def current_items(field = attribute_name, **_opts)
    @builder.association(
      field.to_s.singularize,
      collection: object.public_send(field),
      label_method: ->(fd) {
        fd&.decorate&.to_label(field, builder: @builder) || I18n.t(:none)
      },
      as: :custom_radio_buttons,
      legend_tag: false,
      wrapper_html: { class: %w[files] },
      include_blank: I18n.t(:none),
    )
  end
end

