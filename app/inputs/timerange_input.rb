# frozen_string_literal: true

class TimerangeInput < SimpleForm::Inputs::Base
  def input(wrapper_options = nil)
    opts = merge_wrapper_options(input_html_options, wrapper_options)
    given_opts = options.clone
    given_opts.delete(:as)
    full_opts = { as: :time_picker, html5: true, input_html: opts,
                  required: required_field?, wrapper: :ranged_datetime, }.deep_merge(given_opts)
    template.tag.div(class: %w[row datetimerange]) do
      @builder.simple_fields_for attribute_name,
                                 @builder.object.public_send(attribute_name) do |f|
        (f.input(:beginning, full_opts) <<
        f.input(:ending, full_opts)).html_safe
      end
    end
  end

  def label(wrapper_options = nil)
    options[:label].present? ? super : ''
  end
end

