# frozen_string_literal: true

class TstzrangeInput < SimpleForm::Inputs::Base
  def input(wrapper_options = nil)
    opts = merge_wrapper_options(input_html_options, wrapper_options)
    input_html = {
      data: { date_options: { showTodayButton: true } },
    }.deep_merge(opts)
    template.tag.div(class: %w[row datetimerange]) do
      @builder.simple_fields_for attribute_name,
                                 @builder.object.public_send(attribute_name) do |f|
        #type = has_time(f.object.first) ? :datetime_picker : :date_picker
        # we always wish to allow setting time, even if currently it's 0. We
        # do not display it if it's 0 (in datetime_picker)
        type = options.delete(:type) || :datetime_picker
        (
          f.input(:first, **{ label: I18n.t('starts'), as: type, html5: true,
                              input_html:,
                              wrapper: :ranged_datetime, }.deep_merge(opts)) <<
          f.input(:last, **{ label: I18n.t('ends'), as: type, html5: true,
                             input_html:,
                             wrapper: :ranged_datetime, }.deep_merge(opts))
        ).html_safe
      end
    end
  end

  def label(wrapper_options = nil)
    options[:label].present? ? super : ''
  end
end

