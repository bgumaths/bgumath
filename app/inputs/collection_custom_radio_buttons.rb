# frozen_string_literal: true

require 'simple_form/tags'

class CollectionCustomRadioButtons < SimpleForm::Tags::CollectionRadioButtons
  def render_component(builder)
    builder.radio_button +
      builder.label(class: 'custom-control-label collection_radio_buttons')
  end
end

