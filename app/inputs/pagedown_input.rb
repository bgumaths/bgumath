# frozen_string_literal: true

class PagedownInput < SimpleForm::Inputs::TextInput
  def input(_wrapper_options = nil)
    prev = input_html_options.delete(:preview)
    @nsprefix = input_html_options[:namespace] || ''
    @nsprefix += '-' if @nsprefix.present?
    #out = ''.html_safe
    out = template.render('pagedown_help')
    out << tag.div(nil, id: "wmd-button-bar-#{common_id}")

    out << wmd_input

    if  prev
      out << tag.div(nil, id: "wmd-preview-#{common_id}",
                          class: %w[mathjax wmd-preview], dir: 'auto')
    end

    out.html_safe
  end

  protected

  def wmd_input
    @builder.text_area(
      attribute_name,
      input_html_options.merge(
        class: 'wmd-input form-control', id: common_id,
        dir: :auto, dirname: "#{common_id}.dir", namespace: nil
      ),
    )
  end

  def mathjax_update
    %<MathJax.typeset("wmd-preview-#{common_id}")>
  end

  def common_id
    ns = @builder.options[:namespace]
    res = @nsprefix + input_class
    ns.present? ? "#{ns}_#{res}" : res
  rescue StandardError
    @nsprefix + input_class
  end
end

