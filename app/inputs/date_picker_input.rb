# frozen_string_literal: true

class DatePickerInput < SimpleForm::Inputs::StringInput
  def input(wrapper_options)
    @value = wrapper_options[:value] || options[:value]
    set_html_options
    set_value_html_option

    template.tag.div class: 'input-group date datepicker datetimepicker',
                     id: "#{base_id}-div", data: { target_input: :nearest }, dir: :ltr do
      input = super(wrapper_options.deep_merge(
        data: { target: "##{base_id}-div" },
      )) # leave StringInput do the real rendering
      input + input_button
    end
  end

  def input_html_classes
    super.push '' # 'form-control'
  end

  private

  def base_id
    @base_id ||= "#{object_name.to_s.gsub(/\]\[|[^-a-zA-Z0-9:.]/, '_').sub(/_$/, 
                                                                           '')}-#{attribute_name.to_s.sub(/\?$/, '')}"
  end

  def input_button
    # need datepickerbutton for the js to find the button
    template.tag.div class: 'input-group-append',
                     data: { target: "##{base_id}-div", toggle: :datetimepicker } do
      template.tag.div class: 'input-group-text' do
        template.tag.span '', class: %w[bi-calendar-date]
      end
    end
  end

  def set_html_options
    input_html_options[:type] = 'text'
    input_html_options[:data] ||= {}
    input_html_options[:data].deep_merge!(date_options:)
  end

  def set_value_html_option
    return if value.blank?

    input_html_options[:value] ||= begin
      I18n.l(value, format: display_pattern)
    rescue StandardError
      nil
    end
    #input_html_options[:data][:date_options][:defaultDate] = input_html_options[:value]
  end

  def value
    @value || object.try(attribute_name)
  end

  def display_pattern
    I18n.t('datepicker.dformat', default: '%d/%m/%Y')
  end

  def picker_pattern
    I18n.t('datepicker.pformat', default: 'DD/MM/YYYY')
  end

  def date_view_header_format
    I18n.t('dayViewHeaderFormat', default: 'MMMM YYYY')
  end

  def date_options_base
    {
      #locale: I18n.locale.to_s,
      locale: :en,
      format: picker_pattern || '',
      dayViewHeaderFormat: date_view_header_format,
      collapse: true,
      icons: {
        time: 'bi-clock',
        date: 'bi-calendar-date',
        up: 'bi-arrow-up-circle',
        down: 'bi-arrow-down-circle',
        previous: 'bi-arrow-left-circle',
        next: 'bi-arrow-right-circle',
        today: 'bi-calendar-check',
        clear: 'bi-trash',
        close: 'bi-x-octagon',
      },
    }
  end

  def date_options
    custom_options = input_html_options[:data][:date_options] || {}
    date_options_base.merge!(custom_options)
  end
end

