# frozen_string_literal: true

require 'simple_form/tags'

class CustomCheckBoxesInput < SimpleForm::Inputs::CollectionCheckBoxesInput
  def item_wrapper_class
    'custom-checkbox'
  end
end

class CollectionCustomCheckBoxes < SimpleForm::Tags::CollectionCheckBoxes
  def render_component(builder)
    builder.check_box +
      builder.label(class: 'custom-control-label collection_check_boxes')
  end
end

