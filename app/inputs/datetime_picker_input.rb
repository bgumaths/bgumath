# frozen_string_literal: true

class DatetimePickerInput < DatePickerInput
  protected

  def display_pattern
    super +
      (has_time ? " #{I18n.t('timepicker.dformat', default: '%R')}" : '')
  end

  def picker_pattern
    super +
      (has_time ? " #{I18n.t('timepicker.pformat', default: 'HH:mm')}" : '')
  end

  def has_time(val = value)
    val.localtime.seconds_since_midnight.positive?
  end
end

