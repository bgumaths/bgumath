# frozen_string_literal: true

class DataFormBuilder < SimpleForm::FormBuilder
  attr_accessor :data

  def initialize(...)
    @data = {}
    super
  end

  def find_input(attr, opts = {}, &)
    opts[:input_html].try(:delete, :include_hidden)
    super
  end

  # make forbidden fields disabled
  # The 'options' field below is defined as an optional hash, not a double 
  # splat, for compatibility with SimpleForm
  def input(attribute_name, options = {}, &)
    attr_name = options[:attribute_name]
    if attr_name.blank?
      # check if nested, like a trasto field
      attr_name = (object_name =~ /\[(.*)\]$/) ? $1 : attribute_name
    end
    if !template.respond_to?(:permitted?) ||
       template.permitted?(attr_name, object)
      super
    else
      super(attribute_name, readonly: true, disabled: true, **options, &)
    end
  end

  def input_admins(**opts)
    input(:admins, **{input_html: {multiple: true},
                      collection: object.can_admin.map(&:decorate),
                      readonly: false, disabled: false, }.deep_merge(opts))
  end

  def collection_custom_radio_buttons(method, collection, value_method,
                                      text_method, options = {},
                                      html_options = {}, &)
    CollectionCustomRadioButtons.new(
      @object_name, method, @template, collection, value_method, text_method, 
      objectify_options(options), @default_options.merge(html_options)
    ).render(&)
  end

  def collection_custom_check_boxes(method, collection, value_method,
                                    text_method, options = {},
                                    html_options = {}, &)
    CollectionCustomCheckBoxes.new(
      @object_name, method, @template, collection, value_method, text_method,
      objectify_options(options), @default_options.merge(html_options)
    ).render(&)
  end
end

class SimpleForm::Inputs::Base
  # copied from SimpleForm, to include namespace
  def html_options_for(namespace, css_classes)
    html_options = options[:"#{namespace}_html"]
    html_options = html_options ? html_options.dup : {}
    css_classes << html_options[:class] if html_options.key?(:class)
    html_options[:class] = css_classes unless css_classes.empty?
    ns = (@builder.try(:data) || {})[:namespace]
    html_options[:namespace] = ns if ns.present? && !(
        respond_to?(:generate_label_for_attribute?) &&
        !generate_label_for_attribute?
      )
    html_options
  end
end

