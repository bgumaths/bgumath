# frozen_string_literal: true

class CustomRadioButtonsInput < SimpleForm::Inputs::CollectionRadioButtonsInput
  def item_wrapper_class
    'custom-radio attach-container'
  end
end

