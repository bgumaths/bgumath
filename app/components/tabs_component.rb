# frozen_string_literal: true

class TabsComponent < BaseComponent
  attr_accessor :list, :opts, :force, :id_prefix

  # @param list - list of tabs. If an item is an array, its first element
  # will be the name, and the second the id of the tab. Otherwise, the
  # methods give by to_name and by to_id are used to convert it
  # @param to_name - method to convert item to a tab name
  # @param to_id - method to convert a list item to an id
  # @param opts - may contain the following:
  # - class: the class for each tab
  # - color: a color class, by default 'bg-info-lighter'
  # - cls: additional class elements, by default 'mb-3'
  # - force: if true, show tabs when there is only one tab
  # - id_prefix: prefix to add to all tab ids and elements
  def initialize(list, to_name = ->(obj) { obj&.decorate },
                 to_id = ->(obj) { obj&.decorate&.to_id }, **opts)
    @force = opts.delete(:force)
    @id_prefix = opts.delete(:id_prefix) || ''
    @opts = { id: "#{@id_prefix}tabs-nav",
              class: %w[navbar text-center navbar-light]
            .push(opts.delete(:color) || 'bg-info-lighter')
            .push(opts[:cls] || %w[mb-3]), }.deep_merge(opts) do |_k, v, w|
      v + w
    end
    @list = list
    @to_name = if to_name.respond_to?(:call)
                 to_name
               else
                 ->(*args, **mopts) { public_send(to_name, *args, **mopts) }
               end
    @to_id = if to_id.respond_to?(:call)
               to_id
             else
               ->(*args, **mopts) { public_send(to_id, *args, **mopts) }
             end
    super(**opts)
  end

  def each_item(**ops)
    list.each do |x|
      yield(to_name(x), to_id(x), x, ops[:first])
      ops[:first] = ''
    end
  end

  def display?
    force || list.count > 1
  end

  def to_name(item)
    if item.respond_to?(:pop)
      item[0]
    elsif @to_name.respond_to?(:call)
      @to_name.(item)
    end
  end

  def to_id(item)
    id_prefix +
      (item.respond_to?(:pop) ? item[1] : @to_id.try(:call, item)).to_s
  end
end

