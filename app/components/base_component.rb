# frozen_string_literal: true

class BaseComponent
  include ActiveModel::Model
  delegate_missing_to :context

  def initialize(**opts)
    @context = opts[:context]
    @block = opts[:block]
  end

  def self.component_name
    to_s.underscore.delete_suffix('_component')
  end

  def to_partial_path
    "components/#{self.class.component_name}"
  end

  def to_s
    render(self, &block).html_safe
  end

  protected

  attr_accessor :context, :block
end

