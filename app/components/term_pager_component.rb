# frozen_string_literal: true

class TermPagerComponent < PagerComponent
  def initialize(term, **opts)
    super((opts.delete(:list) || Term.reorder(starts: :asc)).decorate,
          term&.decorate, default: Term.default, **opts)
  end

  def to_partial_path
    'components/pager'
  end
end

