# frozen_string_literal: true

class TreeButtonComponent < BaseComponent
  attr_accessor :tree, :type,
                :content, :chart, :modal, :modal_title, :modal_opts

  def initialize(tree, type = 'sigma_graph', **opts)
    @tree = tree
    @type = type
    @content = opts[:content]
    #@id = opts[:id] || 'tree-modal'
    @chart = opts[:chart] || {}
    @modal = nil
    @modal_title = opts[:modal_title]
    @modal_opts = opts[:modal_opts] || {}
    super(**opts)
  end

  def id
    "#{type}-modal"
  end

  def title
    content || glyphicon(type, modal_title)
  end

  def container_id
    "#{id}-container"
  end

  def modal_done?
    !!modal
  end

  def modal!(**opts)
    @@modal = ModalComponent.new(
      modal_title, nil,
      **modal_opts.deep_merge(id:, button: false).deep_merge(opts)
    )
  end

  def data
    try(type)
  end

  def treant_tree
    # https://fperucic.github.io/treant-js/#orgchart-api
    {
      chart: {
        levelSeparation: 175,
        siblingSeparation: 270,
        subTeeSeparation: 270,
        padding: 35,
        #animateOnInit: true,
        node: { HTMLclass: 'tree-node' },
        connectors: {
          type: 'curve',
          style: {
            'stroke-width' => 2,
            'stroke-linecap' => 'round',
            stroke: '#ccc',
          },
        },
        container: "##{container_id}",
      }.deep_merge(chart),
      nodeStructure: tree,
    }
  end

  def sigma_graph(cont = container_id)
    GraphComponent.new(tree, cont).sigma_graph
  end
end

