# frozen_string_literal: true

class AccordionComponent < BaseComponent
  class Item < BaseComponent
    attr_accessor :id, :accordion

    def initialize(accordion, id, **)
      @accordion = accordion
      @id = id
      super(**)
    end
  end

  @@id = 0

  attr_accessor :opts

  def initialize(**opts)
    @opts = opts
    @opts[:id] ||= "accordion_#{@@id += 1}"
    super
    @opts.delete(:context)
  end

  def id
    opts[:id]
  end

  def item(id, &)
    render(Item.new(self, id), &)
  end

  def item_title(&)
    content_for(:title, flush: true, &)
  end
end

