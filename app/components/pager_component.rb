# frozen_string_literal: true

class PagerComponent < BaseComponent
  attr_accessor :list, :cur

  def initialize(list, cur, **opts)
    @list = list
    @cur = cur
    @to_path = opts[:to_path] || ->(x) { x.try(:decorate).try(:show_path) }
    @to_class = opts[:to_class]
    @default = opts[:default]
    super(**opts)
  end

  def to_path(x, ...)
    return nil unless x

    @to_path.(x, ...)
  end

  def to_class(x, url = to_path(x), **)
    return @to_class.(x, url, **) if @to_class

    res = []
    res.push('disabled') unless url
    res.push('pager-default') if x == @default
    res.push('active') if x == cur
    res.join(' ')
  end

  def cur_index
    @cur_index ||= list.index(cur)
  end

  def item(x, url = to_path(x), cls = nil, **opts)
    cls ||= to_class(x, url, **opts)
    link_opts = { class: ['page-link'] }
    link_opts[:tabindex] = -1 unless x
    link_opts = link_opts.deep_merge(opts[:link_opts] || {})
    tag.li(class: ['page-item', cls]) do
      link_to(url, **link_opts) do
        if block_given?
          yield(x)
        else
          x.to_html +
            (if x == cur
               tag.span(" (#{tt :current, x})", class: 'sr-only')
             else
               ''
             end)
        end
      end
    end
  end

  def previ
    cur_index&.positive? ? list[cur_index - 1] : nil
  end

  def nexti
    cur_index ? list[cur_index + 1] : nil
  end

  def prev_item
    item(previ, link_opts: { aria: { label: 'Previous' } }) do
      tag.span('&laquo;'.html_safe, aria: { hidden: true }) +
        tag.span('Previous', class: 'sr-only')
    end
  end

  def next_item
    item(nexti, link_opts: { aria: { label: 'Next' } }) do
      tag.span('&raquo;'.html_safe, aria: { hidden: true }) +
        tag.span('Next', class: 'sr-only')
    end
  end
end

