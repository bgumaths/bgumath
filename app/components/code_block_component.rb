# frozen_string_literal: true

class CodeBlockComponent < BaseComponent
  @@id = 0
  attr_accessor :id, :code, :pre_classes, :lang, :code_opts

  def initialize(**opts)
    @id = opts.delete(:id) || "code-#{@@id += 1}"
    @code = opts.delete(:code)
    @lang = opts.delete(:lang)
    @pre_classes = opts.delete(:pre_classes) || ''
    @code_opts = opts.delete(:code_opts) || {}
    super
  end

  def code_tag(*, **opts)
    tag.code(*, **{ id: "#{id}-code" }.deep_merge(
      lang.present? ? { class: lang } : {}
    ).deep_merge(code_opts).deep_merge(opts))
  end

  def copy_button(**bopts)
    tag.button(t(:copy),
               **{ class: %w[btn btn-sm btn-secondary copy-button],
                   type: :button, title: t(:copy_tip),
                   data: {
                     toggle: :tooltip,
                     'clipboard-target' => "##{id}-code",
                   }, }.deep_merge(bopts))
  end
end

