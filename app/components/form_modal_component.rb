# frozen_string_literal: true

class FormModalComponent < BaseComponent
  attr_accessor :model, :title, :form_opts, :form_path, :index_id, :modal_id,
                :partial, :partial_prefix, :index_type, :modal_opts,
                :modal, :button, :id_prefix

  # @param model a new decorated model for the form
  # @option opts [String] :title title of the modal and the button
  # @option opts [Hash] :form_opts options for simple_form_for
  # @option opts [String, Array] :form_path path for simple_form_for.  
  # Defaults to model's form_path
  # @option opts [String] :id_prefix prefix to add to the id of various 
  # elements, including the modal. Also used as a namespace for the form 
  # elements
  # @option opts [String] :index_id id of the element into which the 
  # resulting new object should be included. If non-empty overrides the 
  # index-id set by the server in the response
  # @option opts [String] :index_type the type of element into which the new 
  # object is inserted.  Can be 'dt' for a datatables table, 'tabs' for a 
  # new tab element or empty for simple insertion
  # @option opts [String] :prefix where to look for the partial to render 
  # the element.  Useful if the element is not the current type (e.g., form 
  # for a new Meeting on the page of a seminar)
  # @option opts [Boolean] :button whether to insert the form button into the 
  # buttons toolbar. If false, a button can be inserted with 
  # `@form.button_tag`
  # @option opts [Hash] :modal_opts options for the modal
  # @option opts [String] :modal_id the id for the modal, should usually not 
  # be given
  # @option opts [String] :partial the partial to render as the form.  
  # Usually not needed
  def initialize(model = nil, **opts)
    super(**opts)
    @model = model || context.model
    @title = opts.delete(:title) || context.tt('actions.new', @model)
    # We are often in a controller different from the one corresponding to 
    # the object we wish to create. so the following leads to errors
    #@form_opts = context.form_opts.deep_merge(opts[:form_opts] || {})
    #@form_path = opts[:form_path] || context.form_path
    @form_opts = {builder: DataFormBuilder}.deep_merge(
      opts.delete(:form_opts) || {},
    )
    @form_path = opts.delete(:form_path) || model.form_path
    @id_prefix = opts.delete(:id_prefix) || ''
    @index_id = opts.delete(:index_id)
    @id = @index_id || 'item-list'
    @modal_id = opts.delete(:modal_id) || "#{@id_prefix}#{@id}-modal"
    @partial = opts.delete(:partial) ||
               "#{@model.class.model_controller_path}/form"
    @index_type = opts.delete(:index_type) || ''
    @partial_prefix = if opts.key?(:prefix)
                        opts.delete(:prefix)
                      else
                        controller.lookup_context.prefixes[0] || params[:controller]
                      end
    @button = opts.key?(:button) ? opts.delete(:button) : true
    @modal_opts = {
      class: 'modal form-modal', button_opts: {
                                   class: %w[btn btn-primary btn-new-item],
                                 },
      dialog_opts: { class: 'modal-xl' },
    }.deep_merge(opts.delete(:modal_opts) || {})
  end

  delegate :button_tag, to: :modal

  def simple_form_opts
    form_opts.deep_merge({
      namespace: id_prefix,
      html: { id: "#{modal_id}-form" },
      data: {
        remote: true, type: :text, index_id:, index_type:, 
        id_prefix:, 
      }.compact!,
    }.compact_blank)
  end
end

