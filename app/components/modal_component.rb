# frozen_string_literal: true

class ModalComponent < BaseComponent
  @@id = 0

  attr_accessor :id, :title, :button_text, :button_opts, :button, :nobody,
                :cssclass, :dialog_opts

  def initialize(title, button_text = title, **opts)
    @id = opts[:id] || "modal_#{@@id += 1}"
    @title = title
    @button_text = button_text
    @button_opts = opts[:button_opts] || {}
    @button = opts.key?(:button) ? opts[:button] : true
    @nobody = opts.key?(:nobody) ? opts[:nobody] : false
    @cssclass = opts[:class] || 'modal'
    @dialog_opts = opts[:dialog_opts] || {}
    super(**opts)
  end

  def button?
    !!button
  end

  def nobody?
    !!nobody
  end

  def header_content
    :"#{id}_header"
  end

  def footer_content
    :"#{id}_footer"
  end

  def body_content
    :"#{id}_body"
  end

  def close_button_content
    :"#{id}_close_button"
  end

  def header_title_content
    :"#{id}_header_title"
  end

  def header(**opts)
    tag.div(class: %w[modal-header], **opts) do
      if block_given?
        yield(self).html_safe
      else
        content_for header_content
      end
    end
  end

  def close_button
    content_for close_button_content
  end

  def header_title
    content_for header_title_content
  end

  def body(**opts)
    tag.div(class: %w[modal-body], **opts) do
      if block_given?
        yield(self).html_safe
      else
        content_for body_content
      end
    end
  end

  def footer(**opts)
    tag.div(class: 'modal-footer', **opts) do
      if block_given?
        yield(self).html_safe
      else
        content_for footer_content
      end
    end
  end

  def button_class
    %w[btn btn-primary]
  end

  def button_tag(text = button_text, **opts)
    tag.button text, **{
      type: :button, class: button_class,
      data: { toggle: :modal, target: "##{id}" },
    }.deep_merge(button_opts).deep_merge(opts)
  end
end

