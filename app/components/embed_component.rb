# frozen_string_literal: true

class EmbedComponent < BaseComponent
  attr_accessor :files, :link_opts

  def initialize(files, **opts)
    @files = files
    @link_opts = opts[:link_opts] || { class: %w[btn btn-primary] }
    super(**opts)
  end

  def tex_include_files(latex_config)
    return unless files

    workdir = Pathname.new(File.join(LatexToPdf.config[:basedir],
                                     latex_config[:workdir].()))
    FileUtils.mkdir_p workdir
    files.each do |f|
      blob = f.blob
      blob = blob.try(:object) || blob
      blob.open do |bb|
        FileUtils.cp(bb.path, workdir / f.to_s)
      end
      yield f
    end
  end
end

