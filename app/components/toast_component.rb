# frozen_string_literal: true

class ToastComponent < BaseComponent
  @@id = 0

  attr_accessor :title, :subtitle, :id, :cssclass

  def initialize(title, subtitle = nil, **opts)
    @id = opts[:id] || "toast_#{@@id += 1}"
    @title = title
    @subtitle = subtitle
    @cssclass = opts[:cssclass] || ''
    super(**opts)
  end

  def header
    tag.div(class: %w[toast-header]) do
      if block_given?
        yield(self).html_safe
      else
        content_for header_content
      end
    end
  end

  def close_button
    content_for close_button_content
  end

  def header_title
    content_for header_title_content
  end

  def close_button_content
    :"#{id}_close_button"
  end

  def header_title_content
    :"#{id}_header_title"
  end

  def header_content
    :"#{id}_header"
  end

  def body_content
    :"#{id}_body"
  end

  def body(**opts)
    tag.div(class: %w[toast-body], **opts) do
      if block_given?
        yield(self).html_safe
      else
        content_for body_content
      end
    end
  end
end

