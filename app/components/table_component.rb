# frozen_string_literal: true

class TableComponent < BaseComponent
  attr_accessor :cls, :data, :title, :html_options, :id

  def initialize(**opts)
    super
    @data = opts[:data]
    @cls = %w[table table-striped table-hover]
    # explicitly pass data: false if not interested in datatables
    @cls.push('datatable', 'display', 'dt-responsive', 'no-wrap') if
      @data || @data.nil?
    # pass class: [foo] to add to the default, pass class inside html_options
    # to replace
    @cls.push(*opts[:class]) if opts[:class]
    @title = opts[:title] || params[:controller].parameterize
    @html_options = opts[:html_options] || {}
    @id = opts.key?(:id) ? opts[:id] : "#{@title}.table"
  end

  def table_opts(**opts)
    {
      class: cls, dir: loc2dir, id:, width: '100%', data: @data,
    }.deep_merge(opts)
  end

  def th(*args, **opts, &)
    scope = opts[:scope] || :col
    opts = {
      role: (scope.to_sym == :col) ? :columnheader : :rowheader,
      scope:,
    }.merge(opts)
    tag.th(role: (scope.to_sym == :col) ? :columnheader : :rowheader,
           scope:, **opts) do
      tag.div(*args, &)
    end
  end
end

