# frozen_string_literal: true

class SelectorComponent < BaseComponent
  attr_accessor :form, :field, :col, :cast, :hide, :input, :class_prefix

  # @param form [SimpleForm] the simple_form object
  # @param field [Symbol] the field
  # @param col [Array] collection of possible values
  # @param opts [Hash]
  #   @param class_prefix prefix to fieldset selector_id
  #   @param cast whether to cast the object into the given value (if col
  #     consists of classes). If nil, try casting if possible
  #   @param hide [Boolean] whether to hide inactive elements or just disable
  #   @param input [Hash] options to the selector input
  def initialize(form, field, col, **opts)
    @form = form
    @field = field
    @col = col
    @cast = opts.delete(:cast)
    @hide = opts.delete(:hide)
    @class_prefix = opts.delete(:class_prefix) || ''
    @input = opts.delete(:input) || {}
    @classes = [selector_cls] << (@input.delete(:class) || [])
    (@input[:input_html] ||= {}).deep_merge!(class: @classes)
    @input[:input_html].deep_merge!(data: { hide: true }) if @hide
    super(**opts)
  end

  def for(val, **opts)
    casti = opts.key?(:cast) ? opts[:cast] : cast
    casti = true if casti.nil? && val.is_a?(Class)
    if casti
      old = form.object
      form.object = begin
        form.object.becomes(val)
      rescue StandardError
        old
      end
    end
    if form.respond_to?(:data)
      form.data[:namespace] = val.to_s
    end
    res = tag.fieldset(
      **{
        class: ['', selector_id, "#{selector_id}-#{val}"].map do |x|
          selected_cls(x)
        end << (opts[:class] || []),
      }.deep_merge(opts[:html] || {}),
    ) do
      yield(val)
    end
    if casti
      form.object = old
    end
    if form.respond_to?(:data)
      form.data.delete(:namespace)
    end
    res
  end

  def each(...)
    col.map do |val|
      self.for(val, ...)
    end.join.html_safe
  end

  def selected_cls(type = nil)
    type.present? ? "#{selected_cls}-#{type}" : 'vis-selected'
  end

  def selector_cls
    'vis-selector'
  end

  def selector_id
    "#{class_prefix}#{form.object_name.to_s.tr('[]', '_')}_#{field}".gsub(
      /__+/, '_'
    )
  end
end

