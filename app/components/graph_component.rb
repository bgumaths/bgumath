# frozen_string_literal: true

class GraphComponent < BaseComponent
  attr_accessor :graph, :id, :rtl, :bare

  def initialize(graph, id = :sigma_graph_container, **opts)
    @graph = graph
    @id = id
    # TODO
    @rtl = opts[:rtl].nil? ? I18n.locale == :he : opts[:rtl]
    @bare = opts[:bare]
    super(**opts)
  end

  # graph field of data is used in sigma.js to build the graph according to 
  # sigma. The class sigma_graph_container is used as an identifier.
  def div_opts(**opts)
    { id:, class: :sigma_graph_container,
      data: { graph: sigma_graph.deep_merge(opts) }, }
  end

  def sigma_graph
    {
      renderers: [
        {
          container: id,
          type: 'canvas',
        }
      ],
      graph:,
      settings: {
        clone: false,
        defaultEdgeType: 'arrow',
        defaultNodeColor: '#ec5148',
        defaultNodeHoverColor: '#ff60f0',
        defaultNodeActiveColor: '#333333',
        defaultNodeBorderColor: 'blue',
        defaultLabelHoverColor: 'darkblue',
        hoverFontStyle: 'bold',
        labelHoverShadowColor: 'darkcyan',
        nodeHoverColor: 'default',
        defaultEdgeColor: '#51bc48',
        defaultLabelColor: '#5148ec',
        defaultLabelSize: 12,
        edgeColor: 'default',
        edgeType: 'default',
        minArrowSize: 8,
        labelSize: 'proportional',
        labelSizeRatio: 2,
        minEdgeSize: 1,
        #enableHovering: false,
        enableEdgeHovering: false,
        edgeHoverSizeRatio: 2,
        activeFontStyle: 'bold',
        nodeActiveColor: 'node',
        labelThreshold: 0,
        maxEdgeSize: 2,
        drawEdgeLabels: false,
        edgeHoverExtremeties: true,
        doubleClickEnabled: false,
        nodesPowRatio: 0.8,
        edgesPowRatio: 0.8,
        #autoRescale: ['nodeSize', 'edgeSize'],
        ## disable mouse wheel, so we may scroll the page
        mouseWheelEnabled: true,
        zoomingRatio: 1.1,
        # set a margin, so that labels are visible
        sideMargin: 120,
        labelAlignment: 'bottom',
        maxNodeLabelLineLength: 21,
        scalingMode: 'inside',
      },
    }
  end
end

