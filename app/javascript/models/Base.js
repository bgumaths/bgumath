import * as routes from '@/utils/routes.js'

export function bless(obj, rel = null, extra = {}, created = {}) {
  if (Array.isArray(obj)) {
    return rel instanceof _HasMany
      ? rel.create(obj, extra, created)
      : obj.map((it) => bless(it, rel, extra, created))
  }
  const klass = Base.models[obj?.meta?.klass] || rel
  return klass && !(obj instanceof klass)
    ? klass.create(obj, extra, created)
    : obj
}

class _HasMany {
  static canonical = {}
  static unique(cls) {
    return (this.canonical[cls] ||= new this(cls))
  }
  #type
  constructor(cls) {
    this.#type = cls
  }
  create(arr, extra = {}, created = {}) {
    return bless(arr, this.#type, extra, created)
  }
}

export function hasMany(cls) {
  return _HasMany.unique(cls)
}

export default class Base {
  static type = 'base'
  static models = {}
  static register() {
    Base.models[this.name] ||= this
  }

  static get relations() {
    return {}
  }

  static {
    this.register()
  }

  constructor(obj, extra = {}) {
    Object.assign(this, obj ? this.constructor.create(obj, extra) : extra)
  }

  // create an instance of this, from `obj` which can be an object or
  // another instance, recursively blessing all relations
  static create(obj, extra = {}, created = {}) {
    created[this] ||= {}
    //if (created[this][obj?.id]) return created[this][obj?.id]
    const res = created[this][obj?.id] || new this(null, extra)
    if (obj?.id) created[this][obj.id] ||= res
    Object.keys(obj).forEach((k) => {
      res[k] ||= bless(obj[k], this.relations[k], extra, created)
    })
    return res
  }

  get oid() {
    return this.to_param || this.id
  }

  static index_path(opts = {}) {
    const fn = routes[this.collection]
    if (!fn) throw `Unknown route ${this.collection}`
    opts._options ??= true
    return fn.call(null, opts)
  }

  static attrKey(attr) {
    return `attributes.${this.type}.${attr}`
  }

  attrKey(attr) {
    return this.constructor.attrKey(attr)
  }

  path(opts = {}) {
    const id = opts.id ?? this.oid
    const ns = id ? this.constructor.singular : this.constructor.collection
    const fn = routes[ns]
    if (!fn) {
      console.warn(`Unknown route ${ns}`)
      return undefined
    }
    opts._options = true
    if (id) {
      opts.id ??= id
    } else delete opts.id
    fn.requiredParams().forEach((prm) => {
      if (prm !== 'id') {
        if (this[prm]) {
          opts[prm] ??= this[prm]
        } else {
          const rel = prm.match(/^(.*)_id/)[1]
          if (rel) {
            if (this[rel]) {
              opts[prm] ??= this[rel]?.oid
            } else {
              const relf = this.constructor.relations[rel]
              if (relf?.defaultId) {
                opts[prm] = relf.defaultId
              }
            }
          }
        }
      }
    })
    //console.log(opts)
    return fn.call(null, opts)
  }

  get index_path() {
    return this.path({ id: false })
  }

  get show_path() {
    return this.oid && this.path()
  }

  get edit_path() {
    return this.oid && this.show_path + '/edit'
  }

  get submit_path() {
    return this.path()
  }

  toLocaleString(loc) {
    return this.to_fs_i18n[loc]
  }

  get to_s() {
    return this.toLocaleString(this.$i18n.locale)
  }

  toString() {
    return this.to_s
  }

  get linked() {
    return `[${this.to_s}](${this.show_path})`
  }

  labelI18n(loc) {
    return this.to_label_i18n[loc]
  }

  get label() {
    return this.labelI18n(this.$i18n.locale)
  }

  hintI18n(loc) {
    return this.to_hints_i18n[loc]
  }

  get hint() {
    return this.hintI18n(this.$i18n.locale)
  }

  get asItem() {
    return {
      value: this.id,
      label: this.to_s,
      hint: this.hint,
    }
  }

  fullnameOptsI18n(loc) {
    if (!this.to_fullname_opts_i18n) return null

    return this.to_fullname_opts_i18n[loc]
  }

  get fullnameOpts() {
    return this.fullnameOptsI18n(this.$i18n.locale)
  }

  createdAt(format = 'long') {
    const date = new Date(this.created_at)
    return format ? this.$i18n.d(date, format) : date
  }

  updatedAt(format = 'long') {
    const date = new Date(this.updated_at)
    return format ? this.$i18n.d(date, format) : date
  }
}
