import BaseEvent from '@/models/BaseEvent'
import Member from '@/models/Member'
import { hasMany } from '@/models/Base'

export default class Visitor extends BaseEvent {
  static singular = 'people_visitor'
  static collection = 'people_visitors'
  static type = 'visitor'

  static {
    this.register()
  }

  /*
  first_i18n
  last_i18n
  rank
  webpage
  origin
  starts
  ends
*/

  static get relations() {
    return {
      ...super.relations,
      hosts: hasMany(Member),
    }
  }

  get first() {
    return this.first_i18n[this.$i18n.locale]
  }

  get last() {
    return this.last_i18n[this.$i18n.locale]
  }
}
