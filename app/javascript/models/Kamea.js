import Member from '@/models/Member'

export default class Kamea extends Member {
  static singular = 'people_user'
  static collection = 'people_users'
  static type = 'user'

  static {
    this.register()
  }
}
