import Base from '@/models/Base'

export default class Department extends Base {
  static singular = 'teaching_department'
  static collection = 'teaching_departments'
  static type = 'department'

  static {
    this.register()
  }
}
