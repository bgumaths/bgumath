import BaseEvent from '@/models/BaseEvent'

export default class Event extends BaseEvent {
  static singular = 'research_event'
  static collection = 'research_events'
  static type = 'event'

  static {
    this.register()
  }

  /*
  web
  location
  description
  centre
  title
  expires
*/

  get oexpires() {
    Object.defineProperty(this, 'oexpires', {
      value: new Date(this.expires),
    })
    return this.oexpires
  }
}
