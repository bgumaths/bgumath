import RoleEvent from '@/models/RoleEvent'

export default class RoleAdded extends RoleEvent {
  static singular = 'admin_role_added'
  static collection = 'admin_role_addeds'
  static type = 'role_added'

  static {
    this.register()
  }
}
