import Academic from '@/models/Academic'
import Student from '@/models/Student'
import Postdoc from '@/models/Postdoc'
import Visitor from '@/models/Visitor'
import ResearchGroup from '@/models/ResearchGroup'
import { hasMany } from '@/models/Base'

export default class Member extends Academic {
  static singular = 'people_user'
  static collection = 'people_users'
  static type = 'user'

  static {
    this.register()
  }

  /*
  kamea
  postdoc_ids
  student_ids
  visitor_ids
*/

  static get relations() {
    return {
      ...super.relations,
      students: hasMany(Student),
      postdocs: hasMany(Postdoc),
      visitors: hasMany(Visitor),
      past_students: hasMany(Student),
      cur_students: hasMany(Student),
      research_groups: hasMany(ResearchGroup),
    }
  }
}
