import Base from '@/models/Base'
import { hasMany } from '@/models/Base'
import Role from '@/models/Role'
import RoleEvent from '@/models/RoleEvent'
// import these to initialize them
// eslint-disable-next-line
import RoleAdded from '@/models/RoleAdded'
// eslint-disable-next-line
import RoleRemoved from '@/models/RoleRemoved'

export default class User extends Base {
  static singular = 'people_user'
  static collection = 'people_users'
  static type = 'user'

  static {
    this.register()
  }

  /*
  status
  web
  all_notes
  first_i18n
  last_i18n
  rank
  webpage
  start
  finish
  deceased
  hours
  oh_location
  pure
  orcid
  gs
  scopus
  wos
  arxiv
  mathscinet
  photo_url
  webids
  actions
  estate
  photo_signed_id
  menu_item_title
  photos_attributes
  admins
*/

  static get relations() {
    return {
      ...super.relations,
      relevant_roles: hasMany(Role),
      relevant_role_events: hasMany(RoleEvent),
    }
  }

  get first() {
    return this.first_i18n[this.$i18n.locale]
  }

  get last() {
    return this.last_i18n[this.$i18n.locale]
  }

  get isActive() {
    return this.estate === 'active'
  }

  get isRetired() {
    return this.estate === 'retired'
  }

  get isDeceased() {
    return this.estate === 'deceased'
  }
}
