import Base from '@/models/Base'
import Role from '@/models/Role'
import { hasMany } from '@/models/Base'

export default class AppSetup extends Base {
  static singular = 'admin_app_setup'
  static collection = 'admin_app_setup'
  static type = 'app_setup'

  static {
    this.register()
  }

  /*
  depname_i18n
  fdepname_i18n
  department_i18n
  university_i18n
  dept_email
  dept_fax
  dept_phone
  mail_domain
  local_mail_domain
  building_i18n
  dept_snail_i18n
  ml_base
  ml_mail_domain
  ml_domain
  ml_admin
  ml_admin_name
  gmaps_url
  moovit_url
  waze_url
  parent_url
  gspreadsheet_key
*/

  static get relations() {
    return {
      ...super.relations,
      chair_roles: hasMany(Role),
      staff_roles: hasMany(Role),
      issues_roles: hasMany(Role),
    }
  }

  get depname() {
    return this.depname_i18n[this.$i18n.locale]
  }

  get fdepname() {
    return this.fdepname_i18n[this.$i18n.locale]
  }

  get department() {
    return this.department_i18n[this.$i18n.locale]
  }

  get university() {
    return this.university_i18n[this.$i18n.locale]
  }

  get building() {
    return this.building_i18n[this.$i18n.locale]
  }

  get dept_snail() {
    return this.dept_snail_i18n[this.$i18n.locale]
  }

  get submit_path() {
    return this.index_path
  }
}
