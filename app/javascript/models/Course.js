import Base from '@/models/Base'
import Academic from '@/models/Academic'
import GenericCourse from '@/models/GenericCourse'
import IssuesStudentRep from '@/models/IssuesStudentRep'
import Term from '@/models/Term'

export default class Course extends Base {
  static singular = 'teaching_term_course'
  static collection = 'teaching_term_courses'
  static type = 'course'

  static {
    this.register()
  }

  /*
  web
  graduate
  some_info_i18n
  abstract_i18n
*/

  static get relations() {
    return {
      ...super.relations,
      lecturer: Academic,
      generic_course: GenericCourse,
      term: Term,
      student_rep: IssuesStudentRep,
    }
  }

  get some_info() {
    return this.some_info_i18n[this.$i18n.locale]
  }

  get abstract() {
    return this.abstract_i18n[this.$i18n.locale]
  }

  get shid() {
    return this.generic_course.shid
  }

  get shnaton_url() {
    return this.generic_course.shnaton_url
  }

  get description() {
    return this.generic_course.description
  }

  get level() {
    return this.generic_course.level
  }
}
