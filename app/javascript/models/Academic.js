import User from '@/models/User'
import ResearchGroup from '@/models/ResearchGroup'
import { hasMany } from '@/models/Base'

export default class Academic extends User {
  static singular = 'people_user'
  static collection = 'people_users'
  static type = 'user'

  static {
    this.register()
  }

  /*
  interests
  phd_year
  phd_from
  research_group_ids
  course_ids
*/

  static get relations() {
    return {
      ...super.relations,
      research_groups: hasMany(ResearchGroup),
    }
  }
}
