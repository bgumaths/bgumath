import Base from '@/models/Base'
import User from '@/models/User'

export default class UserSession extends Base {
  static type = 'user_session'
  static singular = 'user_session'
  static collection = 'user_sessions'

  static {
    this.register()
  }

  static get relations() {
    return {
      ...super.relations,
      user: User,
    }
  }
}
