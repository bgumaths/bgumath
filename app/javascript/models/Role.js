import Base from '@/models/Base'

export default class Role extends Base {
  static singular = 'admin_role'
  static collection = 'admin_roles'
  static type = 'role'

  static {
    this.register()
  }

  static get relations() {
    return {
      ...super.relations,
      resource: Base,
    }
  }
}
