import Academic from '@/models/Academic'

export default class Adjunct extends Academic {
  static singular = 'people_user'
  static collection = 'people_users'
  static type = 'user'

  static {
    this.register()
  }
}
