import Base from '@/models/Base'
import Department from '@/models/Department'
import IssuesStaff from '@/models/IssuesStaff'
import AcYear from '@/models/AcYear'
import { hasMany } from '@/models/Base'

export default class GenericCourse extends Base {
  static singular = 'teaching_generic_course'
  static collection = 'teaching_generic_courses'
  static type = 'generic_course'

  static {
    this.register()
  }

  /*
  shid
  description_i18n
  shnaton_url
  level
*/

  static get relations() {
    return {
      ...super.relations,
      departments: hasMany(Department),
      issues_staffs: hasMany(IssuesStaff),
      year_added: AcYear,
      year_removed: AcYear,
      replaces: hasMany(GenericCourse),
      replaced_by: hasMany(GenericCourse),
    }
  }

  get description() {
    return this.description_i18n[this.$i18n.locale]
  }

  get credits() {
    return this.lectures + this.exercises / 2
  }
}
