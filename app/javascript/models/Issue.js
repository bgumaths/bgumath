import Base from '@/models/Base'

export default class Issue extends Base {
  static singular = 'teaching_issue'
  static collection = 'teaching_issues'
  static type = 'issue'

  static {
    this.register()
  }
}
