import IssuesUser from '@/models/IssuesUser'

export default class IssuesStaff extends IssuesUser {
  static singular = 'teaching_issues_user'
  static collection = 'teaching_issues_users'
  static type = 'issues_staff'

  static {
    this.register()
  }
}
