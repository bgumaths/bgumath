import Student from '@/models/Student'

export default class Msc extends Student {
  static singular = 'people_user'
  static collection = 'people_users'
  static type = 'user'

  static {
    this.register()
  }
}
