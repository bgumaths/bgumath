import { objectKey } from './keys'
import { computed } from 'vue'

export default {
  provide() {
    return {
      [objectKey]: computed(() => this.object),
    }
  },
  computed: {
    editable() {
      return this.object.editable && this.$state.editing
    },
  },
  methods: {
    tattr(attr) {
      return this.ttC(this.object.attrKey(attr))
    },
  },
}
