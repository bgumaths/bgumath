import { objectKey } from './keys'
import { inject } from 'vue'

export default {
  props: {
    object: {
      type: Object,
      default() {
        return inject(objectKey).value
      },
    },
  },
  methods: {
    tattr(attr) {
      return this.ttC(this.object.attrKey(attr))
    },
  },
}
