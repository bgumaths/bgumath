/* eslint no-console:0 */
/* eslint vue/one-component-per-file: 0 */
// This file is automatically compiled by Vite, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

import { createApp, h, reactive } from 'vue'
import mitt from 'mitt'
const emitter = mitt()

import { Quasar, Notify } from 'quasar'
import quasarLang from 'quasar/lang/he'
import iconSet from 'quasar/icon-set/mdi-v6'
// Import icon libraries
import '@quasar/extras/roboto-font-latin-ext/roboto-font-latin-ext.css'
import '@quasar/extras/mdi-v6/mdi-v6.css'
// Import Quasar css
import 'quasar/src/css/index.sass'

import QMarkdown from '@quasar/quasar-ui-qmarkdown'

import { createInertiaApp, Link } from '@inertiajs/vue3'

import * as Routes from '@/utils/routes.js'

function getRoute(rt, ...args) {
  const rfn = Routes[rt]
  if (rfn) {
    return rfn.apply(null, args)
  } else {
    console.log(`Tried to get non-existent route ${rt}`)
    return undefined
  }
}

import { createI18n } from 'vue-i18n'

import en from '@/lang/en'
import he from '@/lang/he'
import dateTime from '@/lang/datetime'

const messages = { en, he }

const defLocale = 'en-US'

const i18n = createI18n({
  locale: defLocale,
  fallbackLocale: defLocale,
  allowComposition: true,
  messages,
  datetimeFormats: {
    en: dateTime,
    he: dateTime,
  },
})

window.$i18n = i18n

const photoWidth = 108.0
const photoHeight = 128.0

import MarkdownIt from 'markdown-it'
import mk from '@traptitech/markdown-it-katex'
const markdown = new MarkdownIt().set({ linkify: true }).use(mk)

//import PageHeader from '@/Components/PageHeader'

const capitalize = (text) =>
  text?.replace(
    /\w\S*/g,
    (word) => word.charAt(0).toUpperCase() + word.substr(1).toLowerCase()
  )
const state = reactive({
  editing: false,
  toggleEdit() {
    this.editing = !this.editing
  },
})

// Not working...
//const pages = import.meta.glob('../Pages/**/*.vue', { eager: true })

import pages from './pages.js'

import BaseLayout from '@/Layouts/BaseLayout.vue'
import ApplicationLayout from '@/Layouts/ApplicationLayout.vue'

import '@/models'

import { bless } from '@/models/Base'

createInertiaApp({
  resolve: (name) => {
    const page = pages[name]
    if (!page)
      throw new Error(
        `Unknown page ${name}. Is it located under Pages with a .vue extension?`
      )
    page.layout = page.layout || [BaseLayout, ApplicationLayout]
    return page
  },
  setup({ el, App, props, plugin }) {
    const app = createApp({
      render: () => h(App, props),
    })
    app.use(plugin)
    app.use(Quasar, {
      config: {
        notify: {},
        brand: {
          primary: '#027be3',
          secondary: '#26a69a',
          accent: '#9c27b0',
          dark: '#1d1d1d',
          positive: '#21ba45',
          negative: '#c10015',
          info: '#31ccec',
          warning: '#f2c037',
        },
      },
      plugins: {
        Notify,
      },
      iconSet: iconSet,
      lang: quasarLang,
    })
    app.use(i18n)
    app.use(QMarkdown)
    /* eslint-disable-next-line vue/multi-word-component-names,  vue/no-reserved-component-names */
    app.component('Link', Link)
    /* https://github.com/vuetifyjs/vuetify/issues/11573 */
    app.component('RouterLink', {
      functional: true,
      render(h, context) {
        const data = { ...context.data }
        delete data.nativeOn
        const props = data.props || {}
        props.href = props.to
        return h('Link', data, context.children)
      },
    })
    //app.component('PageHeader', PageHeader)

    //app.config.unwrapInjectedRef = true
    app.config.globalProperties.$photoWidth = photoWidth
    app.config.globalProperties.$photoHeight = photoHeight
    app.config.globalProperties.$route = Routes
    app.config.globalProperties.$getRoute = getRoute
    app.config.globalProperties.$state = state
    app.config.globalProperties.$emitter = emitter

    app.config.globalProperties.$bless = function (obj, cls = null) {
      return obj ? bless(obj, cls, { $i18n: this.$i18n }) : obj
    }

    // default translation key for the page
    app.config.globalProperties.$page_key = function (
      path = window.location.pathname
    ) {
      return (
        'dep.title' +
        path
          .split('/')
          .filter((x) => !x.match(/^(spring|fall)\d+$/))
          .join('.')
      )
    }

    // default title for the page
    app.config.globalProperties.$page_title = function (
      path = window.location.pathname
    ) {
      return this.ttC(this.$page_key(path))
    }

    const locales = Object.keys(messages)
    const days = [...Array(7).keys()].map((x) => new Date(2000, 1, x - 1))
    const weekdays = locales.reduce(
      (res, loc) => ({
        ...res,
        [loc]: days.map((d) => d.toLocaleString(loc, { weekday: 'short' })),
      }),
      {}
    )
    app.config.globalProperties.$weekdays = weekdays

    const dayOptions = locales.reduce(
      (res, loc) => ({
        ...res,
        [loc]: [...Array(7).keys()].map((i) => ({
          label: weekdays[loc][i],
          value: i,
        })),
      }),
      {}
    )
    app.config.globalProperties.$dayOptions = dayOptions

    app.config.globalProperties.$tod = (tim) =>
      `${String(tim.hour).padStart(2, 0)}:${String(tim.minute).padStart(2, 0)}`

    app.config.globalProperties.$dateString = function (date, time = false) {
      const d = new Date(typeof date === 'string' ? Date.parse(date) : date)
      const opts = { dateStyle: 'medium' }
      if (time) opts.timeStyle = 'short'
      return d.toLocaleString(this.$i18n.locale, opts)
    }

    app.config.globalProperties.$dir = function (locale = this.$i18n.locale) {
      return locale == 'he' ? 'rtl' : 'ltr'
    }

    app.config.globalProperties.$bullet = function () {
      return this.$q.lang.rtl ? '$bulletLeft' : '$bulletRight'
    }

    app.config.globalProperties.$setting = function (
      which,
      locale = this.$i18n.locale
    ) {
      const val =
        this.$page.props.settings[which] ??
        this.$page.props.settings[`${which}_i18n`]
      return typeof val === 'object' ? val[locale] : val
    }

    // convert field name to ids
    app.config.globalProperties.$iattr = function (attr) {
      return attr.endsWith('s') ? attr.slice(0, -1) + '_ids' : attr + '_id'
    }

    // capitalized translation
    app.config.globalProperties.ttC = function (...args) {
      const res = this.$t(...args)
      return /[A-Z]/.test(res) ? res : capitalize(res)
    }
    const premkdn = (it) => (it || '').toString().replace(/--/g, '\u2013')
    app.config.globalProperties.$md = (mkdn) => markdown.render(premkdn(mkdn))
    app.config.globalProperties.$mdi = (mkdn) =>
      markdown.renderInline(premkdn(mkdn))

    try {
      app.mount(el)
    } catch (e) {
      console.error(e)
    }
    return app
  },
})
