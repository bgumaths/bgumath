import objectProvider from '@/mixins/objectProvider'

export default {
  mixins: [objectProvider],
  props: {
    record: {
      type: Object,
      required: true,
    },
  },
  computed: {
    object() {
      return this.$bless(this.record)
    },
  },
}
