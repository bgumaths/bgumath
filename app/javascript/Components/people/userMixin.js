export default {
  webidUrls: {
    pure: 'https://cris.bgu.ac.il/en/persons/',
    arxiv: 'https://arxiv.org/a/',
    gs: 'https://scholar.google.com/citations?user=',
    mathscinet: 'https://mathscinet.ams.org/mathscinet/MRAuthorID/',
    orcid: 'https://orcid.org/',
    scopus: 'https://scopus.com/authid/detail.uri?authorId=',
    wos: 'https://publons.com/researcher/',
  },
  ranks: ['Prof', 'Dr', 'Mr', 'Ms', 'Emeritus'],
  statuses: [
    'Admin',
    'Regular',
    'Kamea',
    'Virtual',
    'Adjunct',
    'Postdoc',
    'Msc',
    'Phd',
    'External',
  ],
  researchers: new Set([
    'Regular',
    'Kamea',
    'Postdoc',
    'Msc',
    'Phd',
    'External',
    'Adjunct',
  ]),
  seniors: new Set(['Regular', 'Kamea', 'Postdoc', 'External', 'Adjunct']),
  students: new Set(['Msc', 'Phd']),
  visitors: new Set(['Postdoc']),
  hosts: new Set(['Regular', 'Kamea']),
  computed: {
    fullname() {
      return this.user.fullname[this.$i18n.locale]
    },
    isResearcher() {
      return this.$options.researchers.has(this.user.status)
    },
    isTeacher() {
      return this.isResearcher
    },
    professionalContent() {
      return this.isResearcher || this.isTeacher
    },
    isSenior() {
      return this.$options.seniors.has(this.user.status)
    },
    canHost() {
      return this.$options.hosts.has(this.user.status)
    },
    isHosted() {
      return this.$options.visitors.has(this.user.status)
    },
    canSupervise() {
      return this.canHost
    },
    isSupervised() {
      return this.$options.students.has(this.user.status)
    },
    collaborates() {
      return (
        this.canHost || this.isHosted || this.canSupervise || this.isSupervised
      )
    },
  },
  methods: {
    webids(user = this.user) {
      if (!user.webids) {
        return []
      }
      // not taking keys of user.webids to have a fixed order
      return Object.keys(this.$options.webidUrls).filter(
        (key) => user.webids[key]
      )
    },
    webidUrl(type, user = this.user) {
      const val = user.webids[type]
      return val ? this.$options.webidUrls[type] + val : undefined
    },
    user_path(user = this.user) {
      return this.$getRoute('people_user', user.to_param || user.id)
    },
    phd(user = this.user, meth = (it) => it) {
      return [user.phd_from, user.phd_year]
        .filter((x) => x)
        .map(meth)
        .join(', ')
    },
  },
}
