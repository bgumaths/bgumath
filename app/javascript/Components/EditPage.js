import objectProvider from '@/mixins/objectProvider'

export default {
  mixins: [objectProvider],
  props: {
    record: {
      type: Object,
      required: true,
    },
  },
  data() {
    return {
      object: this.$bless(this.record),
    }
  },
}
