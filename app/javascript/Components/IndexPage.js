import objectProvider from '@/mixins/objectProvider'

export default {
  mixins: [objectProvider],
  props: {
    record: {
      type: Object,
    },
    records: {
      type: Array,
      required: true,
    },
  },
  data() {
    return {
      object: this.$bless(this.record),
      items: this.$bless(this.records),
    }
  },
}
