// AsQUploader.js
import { createUploaderComponent } from 'quasar'
import { computed } from 'vue'
import { DirectUpload } from '@rails/activestorage'

// export a Vue component
export default createUploaderComponent({
  name: 'AsQUploader', // your component's name
  props: {
    url: String,
    fileTranform: {
      type: Function,
      default: (file) => file,
    },
  },
  emits: ['uploaded', 'failed', 'uploading'],

  injectPlugin({ props, emit, helpers }) {
    let ashelper = {
      xhr: null,
      file: null,
      directUploadWillCreateBlobWithXHR() {},
      directUploadWillStoreFileWithXHR(request) {
        this.xhr = request
        emit('uploading', { request })
        request.upload.addEventListener(
          'progress',
          (evt) => {
            helpers.uploadedSize.value += evt.loaded
            if (this.file)
              helpers.updateFileStatus(this.file, 'uploading', evt.loaded)
          },
          false
        )
      },
    }

    // [ REQUIRED! ]
    // We're working on uploading files
    const isUploading = computed(() => !!ashelper.xhr)

    // [ REQUIRED! ]
    // Abort and clean up any process
    // that is in progress
    function abort() {
      ashelper.xhr?.abort()
    }

    // [ REQUIRED! ]
    // Start the uploading process
    function upload() {
      const queue = helpers.queuedFiles.value.slice(0)
      helpers.queuedFiles.value = []
      queue.forEach(asUpload)
    }

    async function asUpload(file) {
      const tfile = file.___uploadTransform
        ? await file.___uploadTransform()
        : file
      ashelper.file = file
      const asdu = new DirectUpload(tfile, props.url, ashelper)
      helpers.updateFileStatus(file, 'uploading', 0)
      asdu.create((error, blob) => {
        if (error) {
          helpers.queuedFiles.value.push(file)
          helpers.updateFileStatus(file, 'failed')
          emit('failed', { tfile, error })
        } else {
          helpers.uploadedFiles.value.push(file)
          helpers.updateFileStatus(file, 'uploaded')
          emit('uploaded', { tfile, blob })
        }
        ashelper.xhr = null
        ashelper.file = null
      })
    }

    return {
      isUploading,
      abort,
      upload,
    }
  },
})
