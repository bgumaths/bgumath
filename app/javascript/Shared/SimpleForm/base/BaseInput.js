import { formKey, typeKey, objectKey } from './keys'

export default {
  inject: {
    form: { from: formKey },
    type: { from: typeKey },
    object: { from: objectKey },
  },
  props: {
    // attribute to input
    attr: {
      type: String,
      required: true,
    },
    // attribute key for translation
    tattr: {
      type: String,
      default: (props) => props.attr,
    },
    label: {
      type: String,
    },
    hint: {
      type: String,
    },
    // if locale is given, model[attr] is an object with locales, otherwise
    // it is the usual value
    locale: {
      type: String,
    },
  },
  computed: {
    clabel() {
      return (
        this.label ||
        this.message('labels') +
          (this.locale ? ` (${this.ttC('langname.' + this.locale)})` : '')
      )
    },
    chint() {
      return this.hint || this.message('hints', false)
    },
    // model of submitted
    model() {
      return this.form[this.type]
    },
    value: {
      get() {
        return this.model
          ? this.locale
            ? this.model[this.attr][this.locale]
            : this.model[this.attr]
          : null
      },
      set(val) {
        if (!this.model) return
        if (this.locale) {
          this.model[this.attr] ||= {}
          this.model[this.attr][this.locale] = val
        } else this.model[this.attr] = val
      },
    },
    errors() {
      return this.form.errors
        ? (this.form.errors[this.tattr] || []).join('')
        : null
    },
    objval() {
      if (!this.object) {
        alert(`Object is missing in input (attr ${this.attr})`)
        return null
      }

      if (!(this.attr in this.object)) {
        alert(
          `Attribute ${this.attr} is missing in object, please consider adding it to inertia_edit_params`
        )
        return null
      }

      return this.locale
        ? this.object[this.attr][this.locale]
        : this.object[this.attr]
    },
  },
  created() {
    this.updateValue()
  },
  watch: {
    objval() {
      this.updateValue()
    },
  },
  methods: {
    updateValue() {
      this.value = this.objval
    },
    message(kind, fallback = true, loc = this.$i18n.locale, tkey = this.tattr) {
      const keys = [`forms.${kind}.${this.type}.${tkey}`]
      if (kind === 'labels') keys.push(`attributes.${this.type}.${tkey}`)
      for (const key of keys) {
        if (this.$te(key, loc)) {
          return this.$mdi(this.ttC(key, loc))
        }
      }
      return fallback ? this.$mdi(this.ttC(tkey, loc)) : null
    },
  },
}
