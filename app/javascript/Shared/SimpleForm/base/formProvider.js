import objectMixin from '@/mixins/objectMixin'

export default {
  mixins: [objectMixin],
  computed: {
    model() {
      return this.form[this.type]
    },
    create() {
      return !this.object.id
    },
  },
  methods: {
    createForm() {
      return this.$inertia.form(this.formId(), { [this.type]: {} })
    },
    formId() {
      return this.create
        ? `create-${this.type}`
        : `edit-${this.type}-${this.object.id}`
    },
  },
}
