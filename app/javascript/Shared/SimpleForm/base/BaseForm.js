import { formKey, typeKey, objectKey } from './keys'
import { computed } from 'vue'
import objectMixin from '@/mixins/objectMixin'

export default {
  provide() {
    return {
      [formKey]: this.form,
      [typeKey]: this.type,
      [objectKey]: computed(() => this.object),
    }
  },
  mixins: [objectMixin],
  props: {
    formObj: {
      type: Object,
      //default: computed(() =>
      //  this.$inertia.form(this.formId(), { [this.type]: {} })
      //),
    },
    type: {
      type: String,
      default: (props) => props.object.constructor.type,
    },
    success: {
      type: Function,
      default: () => {},
    },
    fail: {
      type: Function,
      default: () => {},
    },
    method: {
      type: String,
      default: null,
    },
    beforeSave: {
      type: Function,
      default: () => {},
    },
    transform: {
      type: Function,
      default: (data) => data,
    },
  },
  data() {
    return {
      form:
        this.formObj ?? this.$inertia.form(this.formId(), { [this.type]: {} }),
    }
  },
  computed: {
    model() {
      return this.form[this.type]
    },
    modelKeys() {
      return Object.keys(this.model)
    },
    modified() {
      return this.modelKeys.some((k) => this.model[k] !== this.object[k])
    },
    verb() {
      return this.method ?? (this.create ? 'post' : 'put')
    },
    create() {
      return !this.object.id
    },
    submitUrl() {
      return this.object.submit_path
    },
  },
  methods: {
    modelReset() {
      this.modelKeys.forEach((k) => (this.model[k] = this.object[k]))
    },
    formId() {
      return this.create
        ? `create${this.type}`
        : `edit${this.type}${this.object.id}`
    },
    isValid(att) {
      return !(att in (this.form.errors || {}))
    },
    reset(evt) {
      evt.preventDefault()
      this.form.reset()
    },
    _success(page) {
      return this.success(page)
    },
    _fail(errors) {
      return this.fail(errors)
    },
    submit(evt) {
      if (evt) evt.preventDefault()
      this.beforeSave()
      this.form.transform(this.transform).submit(this.verb, this.submitUrl, {
        preserveScroll: true,
        preserveState: true,
        onSuccess: this._success,
        onError: this._fail,
      })
    },
  },
}
