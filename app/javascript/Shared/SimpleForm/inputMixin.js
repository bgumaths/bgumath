export default {
  props: {
    attr: {
      type: String,
      required: true,
    },
    tattr: {
      type: String,
      default: null,
    },
    label: {
      type: String,
    },
    hint: {
      type: String,
    },
  },
  computed: {
    tkey() {
      return this.tattr || this.attr
    },
    clabel() {
      return this.label || this.message('labels')
    },
    chint() {
      return this.hint || this.message('hints', false)
    },
    value() {
      return this.model[this.attr]
    },
  },
  methods: {
    message(kind, fallback = true, loc = this.$i18n.locale, tkey = this.tkey) {
      const keys = [
        `forms.${kind}.${this.type}.${tkey}`,
        `attributes.${this.type}.${tkey}`,
      ]
      for (const key of keys) {
        if (this.$te(key, loc)) {
          return this.$mdi(this.ttC(key, loc))
        }
      }
      return fallback ? this.$mdi(this.ttC(tkey, loc)) : ''
    },
  },
}
