import PeInput from './PeInput.vue'
import objectMixin from '@/mixins/objectMixin'
import objectProvider from '@/mixins/objectProvider'

export default {
  components: {
    PeInput,
  },
  mixins: [objectMixin, objectProvider],
}
