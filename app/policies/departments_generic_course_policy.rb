# frozen_string_literal: true

class DepartmentsGenericCoursePolicy < ApplicationPolicy
  def permitted_attributes
    %i[department_id course_id]
  end
end

