# frozen_string_literal: true

class ResearchGroupPolicy < ApplicationPolicy
  def update?
    (member? and record.members.exists?(id: user.id)) or super
  end

  def create?
    member? or super
  end

  alias_method :faculty?, :index?

  def permitted_attributes
    [:name, :description, :slug, {member_ids: [], member_emails: [],
                                  seminar_ids: [], }]
  end
end

