# frozen_string_literal: true

class IssuesStudentRepPolicy < IssuesUserPolicy
  def index?
    true
  end

  def show?
    true
  end
end

