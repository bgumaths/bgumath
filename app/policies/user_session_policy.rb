# frozen_string_literal: true

class UserSessionPolicy < ApplicationPolicy
  def permitted_attributes
    %i[email pam_login password pam_password]
  end
end

