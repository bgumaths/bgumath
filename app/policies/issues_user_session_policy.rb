# frozen_string_literal: true

class IssuesUserSessionPolicy < ApplicationPolicy
  def permitted_attributes
    %i[login password]
  end
end

