# frozen_string_literal: true

class PreciseProgPrereqPolicy < ProgPrereqPolicy
  def permitted_attributes
    super.push(:course_list_id)
  end
end

