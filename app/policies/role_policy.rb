# frozen_string_literal: true

class RolePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user
        if user.is_Admin? || user.is_staff?
          scope.manual
        else
          # XXX enum values don't seem to work...
          scope.where(visibility: [2, 3, 4, 5])
        end
      else
        scope.where(visibility: [3, 4, 5])
      end
    end
  end

  #def index?
  #  admin?
  #end

  def permitted_attributes
    # TODO: deal with resource_type, resource_id
    [:name, :title, :description, :visibility, :email,
     :resource_type, :resource_id,
     {title_i18n: I18n.available_locales,
      description_i18n: I18n.available_locales,
      user_ids: [], }]
  end
end

