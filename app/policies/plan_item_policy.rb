# frozen_string_literal: true

class PlanItemPolicy < HandbookBasePolicy
  def edit?
    false
  end

  def new?
    false
  end

  def permitted_attributes
    %i[term_plan_id generic_course_id]
  end
end

