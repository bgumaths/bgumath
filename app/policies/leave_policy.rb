# frozen_string_literal: true

class LeavePolicy < ApplicationPolicy
  def create?
    (user and record.user == user) or super
  end

  alias_method :update?, :create?

  def self.permitted_attributes
    [
      :note, :starts, :ends, :user_id, {duration: %i[first last],
                                        note_i18n: I18n.available_locales, }
    ]
  end

  def permitted_attributes
    self.class.permitted_attributes
  end
end

