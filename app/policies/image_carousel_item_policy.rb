# frozen_string_literal: true

class ImageCarouselItemPolicy < CarouselItemPolicy
  def permitted_attributes
    super + %i[image image_signed_ids] +
      [image_signed_ids: [], 
       image_attributes: PhotoPolicy.permitted_attributes]
  end
end

