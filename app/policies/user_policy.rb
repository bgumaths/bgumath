# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  # we allow editting for the admin, or for the current user
  def update?
    (user and (user.becomes(User) == record.try(:becomes, User))) or super
  end

  alias_method :photo?, :show?

  alias_method :hours?, :index?

  alias_method :me?, :index?
  
  alias_method :special?, :index?

  def reset_hours?
    admin?
  end

  alias_method :retire?, :reset_hours?

  def generate_api_key?
    user
  end

  # remove orcid and scopus, since these should normally be updated from pure
  def readonly_webids
    @readonly_webids ||= if admin?
                           []
                         else
                           User::WEB_IDS.select { |ww| BaseWebid.service_class(ww).readonly? }
                         end
  end

  def permitted_attributes
    @permitted_attributes ||=
      User::WEB_IDS - readonly_webids +
      [:state, :status, :first, :last, :email, :office, :rank, :phone, 
       :password, :password_confirmation, :syspass, :webpage, :oh_location,
       :hours, :start, :finish, :deceased, :onleave, :photo_id,
       :photos_signed_ids, :photo, :photos, :mail_domain, :photo_signed_id, 
       {
         first_i18n: I18n.available_locales,
         last_i18n: I18n.available_locales,
         photos_signed_ids: [],
         leaves_attributes: LeavePolicy.permitted_attributes + %i[id _destroy],
         photos_attributes: PhotoPolicy.permitted_attributes + %i[id _destroy],
       }]
  end

  def allowed_attributes
    @allowed_attributes ||= 
      super -
      %w[password password_confirmation syspass photos_signed_ids leaves_attributes photos_attributes] +
      %w[seminars] + readonly_webids.map(&:to_s)
  end

  def role_history?
    admin?
  end
end

