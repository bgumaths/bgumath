# frozen_string_literal: true

class DegreePlanPolicy < HandbookBasePolicy
  alias_method :duplicate?, :new?

  def permitted_attributes
    [:name, :slug, :degree_id, :remarks, {term_plan_ids: [],
                                          name_i18n: I18n.available_locales, remarks_i18n: I18n.available_locales, }]
  end
end

