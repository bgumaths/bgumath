# frozen_string_literal: true

class SeminarPolicy < ApplicationPolicy
  def create?
    user.is_a?(User) or super
  end

  alias_method :weekly?, :index?
  alias_method :duplicate?, :new?
  alias_method :create_ml?, :edit?

  def permitted_attributes
    [:name, :description, :starts, :ends, :day, :room, :web, :online,
     :slug, :term_id, :list_id,
     {research_group_ids: [],
      name_i18n: I18n.available_locales,
      description_i18n: I18n.available_locales,
      shift: %i[beginning ending], }]
  end

  def allowed_attributes
    super.push('meeting_ids')
  end
end

