# frozen_string_literal: true

class TemplatePolicy < ApplicationPolicy
  def index?
    user
  end

  def show?
    user
  end

  def create?
    if (pp = Pundit.policy(user, record.try(:object)))
      # use the permissions of the object associated to this template
      pp.create?
    else
      super
    end
  end

  def update?
    if record.path.start_with?('pages/')
      # the template for the static page is the page itself, so use its
      # policy
      StringPolicy.new(user, record.path).update?
    elsif (pp = Pundit.policy(user, record.object))
      # use the permissions of the object associated to this template
      pp.update?
    else
      super
    end
  end

  def download?
    show?
  end

  def download_all?
    index?
  end

  def permitted_attributes
    %i[body path locale handler partial format url]
  end
end

