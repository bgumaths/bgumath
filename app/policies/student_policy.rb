# frozen_string_literal: true

class StudentPolicy < UserPolicy
  def permitted_attributes
    super.push(
      :interests, :supervisor_ids, # for x-editable
      research_group_ids: [], supervisor_ids: [], supervisor_emails: [],
      generic_course_ids: []
    )
  end
end

