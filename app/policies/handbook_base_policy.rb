# frozen_string_literal: true

class HandbookBasePolicy < ApplicationPolicy
  def create?
    handbook_editor? or super
  end

  def update?
    handbook_editor? or super
  end

  protected

  def handbook_editor?
    user.has_role?('Handbook Editor')
  rescue NoMethodError
    false
  end
end

