# frozen_string_literal: true

class PaperTrail::VersionPolicy < ApplicationPolicy
  class Scope < Scope
    # do it here, since we can't access the model
    def resolve
      scope.order(created_at: :desc).limit(300)
    end
  end

  def index?
    admin?
  end

  def show?
    admin?
  end

  def undo?
    admin?
  end

  def permitted_attributes
    []
  end
end

