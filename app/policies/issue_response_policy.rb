# frozen_string_literal: true

class IssueResponsePolicy < IssuesBasePolicy
  def index?
    false
  end

  def show?
    issue_policy.show? &&
      (record.published? || privileged? || lecturer? || staff?)
  end

  def update?
    replier? && !record.published?
  end
  
  def create?
    issue_policy.respond?
  end

  def destroy?
    update?
  end

  def permitted_attributes
    %i[issue_id creator_id content published_at]
  end

  delegate :lecturer?, :student?, :staff?, :extinvolved?, to: :issue_policy

  protected

  def issue_policy
    @issue_policy ||= Pundit.policy(user, record.issue)
  end
end

