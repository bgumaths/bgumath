# frozen_string_literal: true

class DepartmentPolicy < IssuesBasePolicy
  def permitted_attributes
    [:name, :slug, :webpage, :catalog_id, :slug, :bgu_name,
     {person_ids: [], course_ids: [], name_i18n: I18n.available_locales}]
  end
end

