# frozen_string_literal: true

class IssuesUserPolicy < IssuesBasePolicy
  class Scope < Scope
    # not sure what's the right thing to do here, probably shouldn't be a 
    # big secret
    def resolve
      if user.is_a?(User) || (user.is_a?(IssuesStaff) && user.pass_changed?)
        scope
      elsif user.is_a?(IssuesUser) && user.pass_changed?
        scope.where(login: user.login)
      else
        scope.none
      end
    end
  end

  def index?
    active_user && !issues_student
  end

  def show?
    active_user && (!issues_student || issues_student == record)
  end

  def create?
    super || privileged?
  end

  def update?
    super || privileged? || (issues_user(true) == record)
  end

  def gsheets_update?
    admin?
  end

  alias_method :reset_password?, :update?

  def permitted_attributes
    if issues_user
      %i[fullname password password_confirmation terms_of_usage]
    elsif issues_user(true)
      # new issues user
      %i[password password_confirmation terms_of_usage]
    else
      [:fullname, :login, :password, :password_confirmation, :type, :active,
       :department_id, {course_ids: [], generic_course_ids: []}]
    end
  end

  def allowed_attributes
    %i[fullname login type department_id course_ids]
  end

  def allowed_courses
    Course.current
  end
end

