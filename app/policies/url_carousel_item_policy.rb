# frozen_string_literal: true

class UrlCarouselItemPolicy < CarouselItemPolicy
  def permitted_attributes
    super + %i[url]
  end
end

