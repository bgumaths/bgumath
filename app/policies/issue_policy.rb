# frozen_string_literal: true

class IssuePolicy < IssuesBasePolicy
  class Scope < Scope
    def resolve
      if user.is_a?(IssuesUser) && user.pass_changed?
        scope.where(course: user.courses)
      elsif privileged?
        scope
      elsif !user.is_a?(IssuesUser)
        scope.where(course: user.try(:courses))
      else
        scope.none
      end
    end

    protected

    def user_str
      user&.to_slug || user
    end

    def replier?
      return @is_replier unless @is_replier.nil?
      return false unless user

      logger.debug("  Checking if #{user_str} is a replier")
      @is_replier = user.try(:is_issues_responder?)
    end

    # TODO!
    def head?
      return false unless user

      logger.debug { "  Checking if #{user_str} is the dept head or vice head" }
      user.try(:has_role?, 'Department Head') or
        user.try(:has_role?, 'Department vice head')
    end

    def privileged?
      replier? || head? || user.try(:is_Admin?)
    end

    def logger
      Rails.logger
    end
  end

  def index?
    user
  end

  def show?
    extinvolved? || privileged? || lecturer?
  end

  def create?
    issues_student || admin?
  end

  def update?
    (head? || admin?) && !record.published?
  end

  def destroy?
    privileged?
  end

  # are we allowed to (re)send the creation email?
  def send_created?(_who = nil)
    privileged?
  end

  alias_method :send_update?, :send_created?

  # should the staff dealing with the issue be shown?
  def show_staff?
    privileged?
  end

  def show_aguda?
    show?
  end

  def respond?
    replier?
  end

  def permitted_attributes
    if issues_student
      %i[course_id title content correspondence]
    else
      []
    end
  end

  def allowed_attributes
    return [] unless show?

    res = %w[course_id title content slug aguda_rep_id correspondence web allowed_locales]
    if record.published? || privileged? || lecturer? || staff?
      res.push('response_id')
    end
    res
  end

  # courses allowed for this user, used in the form
  def courses
    privileged? ? Term.default.courses.all : issues_user.try(:courses) || []
  end

  # is the given user the lecturer in the issue course?
  def lecturer?
    !user.is_a?(IssuesUser) &&
      record.try(:course) && user.try(:courses).try(:include?, record.course)
  end

  # given user the student responsible for the issue course
  def student?
    extinvolved? && user.is_a?(IssuesStudentRep)
  end

  # given user staff responsible for the issue course
  def staff?
    extinvolved? && user.is_a?(IssuesStaff)
  end

  # given user is external and is involved in this course
  def extinvolved?
    issues_user && user.courses.include?(record.course)
  end
end

