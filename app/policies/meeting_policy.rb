# frozen_string_literal: true

class MeetingPolicy < BaseEventPolicy
  # if we may _update_ the seminar, we may create (or update) meetings
  def create?
    spolicy = Pundit.policy(user, record.try(:seminar))
    spolicy&.update? || super
  end

  def update?
    spolicy = Pundit.policy(user, record.try(:seminar))
    spolicy&.update? || super
  end

  # who is allowed to email the ML?
  alias_method :email_ml?, :create?

  def permitted_attributes
    merge_attrs(
      super,
      %i[date room speaker from abstract speaker_web announcement slug],
    )
  end
end

