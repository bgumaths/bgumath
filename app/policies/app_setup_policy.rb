# frozen_string_literal: true

class AppSetupPolicy < ApplicationPolicy
  def show?
    # TODO: temporarily make this true, to obtain proper serialization of 
    # attributes
    true
    #admin?
  end

  def create?
    admin?
  end

  def permitted_attributes
    [
      :dept_email,
      :dept_fax,
      :dept_phone,
      :mail_domain,
      :local_mail_domain,
      :ml_base,
      :ml_mail_domain,
      :ml_domain,
      :ml_admin,
      :ml_admin_name,
      :gmaps_url,
      :moovit_url,
      :waze_url,
      :parent_url,
      :gspreadsheet_key,
      {
        depname_i18n: I18n.available_locales,
        fdepname_i18n: I18n.available_locales,
        department_i18n: I18n.available_locales,
        university_i18n: I18n.available_locales,
        building_i18n: I18n.available_locales,
        dept_snail_i18n: I18n.available_locales,
        chair_role_ids: [],
        staff_role_ids: [],
        issues_role_ids: [],
      }
    ]
  end
end

