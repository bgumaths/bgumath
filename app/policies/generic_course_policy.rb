# frozen_string_literal: true

class GenericCoursePolicy < ApplicationPolicy
  def create?
    user&.has_role?('Teaching Committee') or head? or super
  end

  def update?
    user&.has_role?('Teaching Committee') or head? or super
    #member? or super
  end

  alias_method :duplicate?, :new?

  def permitted_attributes
    [:name, :description, :shid, :graduate, :prereq, :core, :advanced,
     :spring, :fall, :lectures, :exercises, :credits, :dcredits, :slug,
     :year_added_id, :year_removed_id, :descfiles_signed_ids, :aguda_rep_id,
     {department_ids: [],
      course_list_ids: [], require_ids: [], replace_ids: [],
      name_i18n: I18n.available_locales,
      description_i18n: I18n.available_locales,
      descfiles: %i[signed_id url to_s filename io],
      descfiles_signed_ids: [], }]
  end

  def allowed_attributes
    super + %w[shnaton_url]
  end
end

