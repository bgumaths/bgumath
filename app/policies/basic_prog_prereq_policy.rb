# frozen_string_literal: true

class BasicProgPrereqPolicy < ProgPrereqPolicy
  def permitted_attributes
    super.push(:course_list_id, :minp, :maxp, :minc, :maxc)
  end
end

