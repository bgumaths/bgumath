# frozen_string_literal: true

class BaseEventPolicy < ApplicationPolicy
  def destroy?
    update?
  end

  def email?
    user
  end

  def permitted_attributes
    merge_attrs(
      super, 
      [:starts, :ends, :title, :descfiles_signed_ids, :online,
       {
         shift: %i[beginning ending], descfiles_signed_ids: [],
         announcement_i18n: I18n.available_locales, duration: %i[first last],
         descfiles: %i[signed_id url to_s filename io],
       }],
    )
  end
end

