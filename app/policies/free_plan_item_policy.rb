# frozen_string_literal: true

class FreePlanItemPolicy < HandbookBasePolicy
  def edit?
    false
  end

  def new?
    false
  end

  def permitted_attributes
    []
  end
end

