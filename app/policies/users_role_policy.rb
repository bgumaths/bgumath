# frozen_string_literal: true

class UsersRolePolicy < ApplicationPolicy
  def permitted_attributes
    %i[user_id role_id]
  end
end

