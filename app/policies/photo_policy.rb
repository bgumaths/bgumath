# frozen_string_literal: true

class PhotoPolicy < ApplicationPolicy
  def create?
    (user and record.imageable == user) or super
  end

  alias_method :update?, :create?

  def self.permitted_attributes
    %i[x y width height signed_id]
  end

  def permitted_attributes
    self.class.permitted_attributes
  end
end

