# frozen_string_literal: true

class CarouselItemPolicy < ApplicationPolicy
  def carousel?
    true
  end

  def index?
    create?
  end

  def show?
    create?
  end

  def permitted_attributes
    %i[activates expires order interval type]
  end
end

