# frozen_string_literal: true

class CourseListPolicy < HandbookBasePolicy
  def permitted_attributes
    [:shnaton_id, :name, :remarks, :credits, :dcredits,
     {course_ids: [], prereq_ids: [],
      name_i18n: I18n.available_locales,
      remarks_i18n: I18n.available_locales, }]
  end
end

