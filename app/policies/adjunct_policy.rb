# frozen_string_literal: true

class AdjunctPolicy < UserPolicy
  def permitted_attributes
    super.push(:interests, :phd_year, :phd_from,
               research_group_ids: [], generic_course_ids: [])
  end
end

