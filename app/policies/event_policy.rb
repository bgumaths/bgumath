# frozen_string_literal: true

class EventPolicy < BaseEventPolicy
  def create?
    member? or super
  end

  def update?
    user.try(:has_role?, 'Center Director') || super
  end

  def permitted_attributes
    merge_attrs(
      super,
      %i[web description location centre slug expires],
    )
  end
end

