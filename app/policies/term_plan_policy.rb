# frozen_string_literal: true

class TermPlanPolicy < HandbookBasePolicy
  def permitted_attributes
    [:year, :term, :degree_plan_id, :remarks,
     {plan_item_ids: [],
      generic_course_ids: [],
      remarks_i18n: I18n.available_locales,
      free_plan_items_attributes: [:id, :minp, :maxp, :title,
                                   {title_i18n: I18n.available_locales}], }]
  end
end

