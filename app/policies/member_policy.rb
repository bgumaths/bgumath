# frozen_string_literal: true

class MemberPolicy < UserPolicy
  alias_method :research?, :index?

  def permitted_attributes
    res = super
    res.push(:interests, :phd_year, :phd_from,
             :student_ids, :postdoc_ids, # for x-editable
             {
               research_group_ids: [], student_ids: [], student_emails: [],
               postdoc_ids: [], postdoc_emails: [], generic_course_ids: [],
             })
  end

  def allowed_attributes
    super - %w[student_emails postdoc_emails generic_course_ids]
  end
end

