# frozen_string_literal: true

class IssuesStaffPolicy < IssuesUserPolicy
  def permitted_attributes
    issues_user(true) ? super : super.push(:department_id, :department)
  end
end

