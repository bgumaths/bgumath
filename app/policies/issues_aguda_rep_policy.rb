# frozen_string_literal: true

class IssuesAgudaRepPolicy < IssuesUserPolicy
  def index?
    true
  end

  def show?
    true
  end
end

