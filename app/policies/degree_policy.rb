# frozen_string_literal: true

class DegreePolicy < HandbookBasePolicy
  def permitted_attributes
    [:name, :description, :slug, :type, :credits, {plan_ids: [], prereq_ids: [],
                                                   name_i18n: I18n.available_locales,
                                                   description_i18n: I18n.available_locales, }]
  end
end

