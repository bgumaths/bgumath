# frozen_string_literal: true

class ResourceCarouselItemPolicy < CarouselItemPolicy
  def permitted_attributes
    super + [
      resources_attributes: %i[resource_type resource_id _destroy id]
    ]
  end
end

