# frozen_string_literal: true

class JobPolicy < ApplicationPolicy
  class Scope < Scope
    # do it here, since we can't access the model
    def resolve
      scope
    end
  end

  def index?
    admin?
  end

  def show?
    admin?
  end

  def undo?
    admin?
  end

  def permitted_attributes
    []
  end
end

