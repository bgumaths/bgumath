# frozen_string_literal: true

class VirtualPolicy < UserPolicy
  def show?
    admin?
  end
end

