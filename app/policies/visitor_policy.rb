# frozen_string_literal: true

class VisitorPolicy < ApplicationPolicy
  def create?
    member? or super
  end

  def update?
    (user and (
      user.has_role?('Center Director') or
      record.hosts.exists?(user.id)
    )) or super
  end

  def email?
    user
  end

  def permitted_attributes
    [
      :email, :first, :last, :office, :phone, :rank, :webpage, :origin,
      :centre, :starts, :ends, :hosts_ids, # allow string for x-editable
      {duration: %i[first last], first_i18n: I18n.available_locales,
       last_i18n: I18n.available_locales, host_ids: [], host_emails: [], }
    ]
  end
end

