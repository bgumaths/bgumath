# frozen_string_literal: true

class IssuesBasePolicy < ApplicationPolicy
  def initialize(user, record)
    # TODO: change this to Errors::InvalidAuth...
    raise Pundit::NotAuthorizedError, I18n.t(:must_login) unless user

    super
  end

  protected

  def issues_user(accept_new = false)
    (user.is_a?(IssuesUser) && (accept_new || user.pass_changed?)) ? user : nil
  end

  def issues_student(...)
    logger.debug("  Checking if #{user_str} is a student rep")
    user.is_a?(IssuesStudentRep) && issues_user(...)
  end

  def issues_aguda(...)
    user.is_a?(IssuesAgudaRep) && issues_user(...)
  end

  def issues_staff(...)
    user.is_a?(IssuesStaff) && issues_user(...)
  end

  def active_user
    return @active_user unless @active_user.nil?

    logger.debug('  Checking if there is an active user')
    @active_user ||= user.is_a?(IssuesUser) ? issues_user : user
  end

  def privileged?
    super || replier?
  end
    
end

