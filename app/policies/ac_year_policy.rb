# frozen_string_literal: true

class AcYearPolicy < ApplicationPolicy
  def index?
    user
  end

  def show?
    user
  end

  def permitted_attributes
    [:year]
  end

  def allowed_attributes
    super.push(:shnaton, :shnaton_id)
  end
end

