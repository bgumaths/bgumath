# frozen_string_literal: true

class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record.is_a?(Draper::Decorator) ? record.object : record
  end

  def index?
    true
  end

  def show?
    true
    #scope.where(id: record.id).exists?
  end

  # generally, override create? and update? in subclasses

  def create?
    #logger.debug("Checking if #{user.try(:to_slug) || 'ANON'} may create #{record || '...'}")
    assist? || admin?
  end

  def new?
    create?
  end

  def update?
    assist? || admin? || user.try(:is_Admin_of?, record)
  end

  def edit?
    update?
  end

  def modify?
    record&.persisted? ? update? : create?
  end

  def destroy?
    user.try(:is_Admin?)
  end

  # update from Google sheets. Disable by default
  def gsheets_update?
    false
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  def merge_attrs(*args)
    args.reduce([{}]) do |res, v|
      h = res.pop
      hh = v.last.is_a?(Hash) ? v.pop : {}
      (res | v) + [h.vdeep_merge(hh)]
    end
  end

  def permitted_keys(action = record.persisted? ? :update? : :create?)
    @permitted_keys ||= {}
    @permitted_keys[action] ||= if !respond_to?(action) || try(action)
                                  permitted_attributes.map { |x| x.try(:keys) || x }.flatten.map(&:to_s)
                                else
                                  []
                                end
  end

  # displayed attributes in api. By default, allow the permitted attributes,
  # and the id
  def allowed_attributes
    @allowed_attributes ||= (permitted_keys(:show?) +
     %w[id created_at updated_at admin_users web allowed_locales]).uniq
  end

  def visible?(field)
    allowed_attributes.include?(field.to_s)
  end

  def writable?(field, ...)
    permitted_keys(...).include?(field.to_s)
  end

  # attributes that appear in the serialization
  def serialize_attributes
    #allowed_attributes.select { |x| record.respond_to?(x) }
    allowed_attributes
  end

  def permitted_attributes
    []
  end

  def serialize_associations
    allowed_attributes.select { |x| x.match(/_ids?$/) }.map do |x|
      x.gsub(/_id(s)?$/, '\1').to_s
    end.select { |x| record.respond_to?(x) }
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end

    protected

    def logger
      Rails.logger
    end

    def user_str
      user&.to_slug || user
    end

    def admin?
      # allow the staff to modify everything
      return false unless user
      return @isAdmin unless @isAdmin.nil?

      logger.debug("  Checking if #{user_str} is a webadmin")
      @is_admin = user.is_staff? || user.is_Admin?
    rescue NoMethodError
      false
    end

    def assist?
      return false unless user

      logger.debug("  Checking if #{user_str} is the math assistant")
      user.try(:has_role?, 'Math Assistant')
    rescue NoMethodError
      false
    end
  end

  protected

  def user_str
    user&.to_slug || user
  end

  def admin?
    # allow the staff to modify everything
    return false unless user
    return @isAdmin unless @isAdmin.nil?

    logger.debug("  Checking if #{user_str} is a webadmin")
    @is_admin = user.is_staff? || user.is_Admin?
  rescue NoMethodError
    false
  end

  # TODO!
  def head?
    return false unless user

    logger.debug("  Checking if #{user_str} is the dept head or vice head")
    user.try(:has_role?, 'Department Head') or
      user.try(:has_role?, 'Department vice head')
  end

  def replier?
    return @is_replier unless @is_replier.nil?
    return false unless user

    logger.debug("  Checking if #{user_str} is a replier")
    @is_replier = user.try(:is_issues_responder?)
  end

  def privileged?
    return false unless user

    logger.debug("  Checking if #{user_str} is privileged")
    head? || admin?
  end

  def member?
    user&.is_a?(Member)
  end

  def regular?
    user&.is_a?(Regular)
  end

  def assist?
    return false unless user

    logger.debug("  Checking if #{user_str} is the math assistant")
    user.try(:has_role?, 'Math Assistant')
  rescue NoMethodError
    false
  end

  def logger
    Rails.logger
  end
end

