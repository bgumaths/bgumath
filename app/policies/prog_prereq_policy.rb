# frozen_string_literal: true

class ProgPrereqPolicy < HandbookBasePolicy
  def subreq?
    update?
  end

  alias_method :duplicate?, :new?

  def permitted_attributes
    [:shnaton_id, :name, :description, :type, :parent_id, :slug,
     :degree_id, {name_i18n: I18n.available_locales,
                  description_i18n: I18n.available_locales, degree_ids: [],
                  course_list_ids: [], }]
  end
end

