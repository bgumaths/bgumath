# frozen_string_literal: true

class PostdocPolicy < UserPolicy
  # allow host_ids also as a string, for x-editable
  def permitted_attributes
    super.push(:interests, :phd_year, :phd_from, :host_ids,
               research_group_ids: [], host_ids: [], host_emails: [])
  end
end

