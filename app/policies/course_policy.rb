# frozen_string_literal: true

class CoursePolicy < ApplicationPolicy
  def create?
    member? or super
  end

  def update?
    (user and
     (record.lecturer == user or
      user.try(:has_role?, 'Teaching Committee') or
      user.try(:is_issues_responder?))) or super
  end

  def gsheets_update?
    admin?
  end

  alias_method :duplicate?, :new?

  def permitted_attributes
    [:term_id, :title, :generic_course_id, :content, :lecturer_id,
     :student_rep_id, :student_rep_email, :shid, 
     :lecturer_email, :hours, :web, :descfiles_signed_ids, :abstract,
     {abstract_i18n: I18n.available_locales,
      title_i18n: I18n.available_locales,
      content_i18n: I18n.available_locales,
      descfiles: %i[signed_id url to_s filename io],
      descfiles_signed_ids: [], issues_staff_ids: [], }]
  end

  def allowed_attributes
    (super + %w[graduate name_i18n description_i18n advanced
                aguda_rep_email lectures exercises credits]).uniq
  end
end

