# frozen_string_literal: true

class ShnatonPolicy < HandbookBasePolicy
  alias_method :duplicate?, :new?

  def permitted_attributes
    [:ac_year_id, :ac_year, {
      degree_ids: [],
      preamble_i18n: I18n.available_locales,
    }]
  end
end

