# frozen_string_literal: true

class MirPolicy < ApplicationPolicy
  def index?
    head? || assist? || admin?
  end
end

