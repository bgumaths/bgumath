# frozen_string_literal: true

class AdminPolicy < ApplicationPolicy
  # allow only admin
  def any?
    admin?
  end
end

