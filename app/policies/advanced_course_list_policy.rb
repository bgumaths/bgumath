# frozen_string_literal: true

class AdvancedCourseListPolicy < CourseListPolicy
  def permitted_attributes
    res = super
    res.last.delete(:course_ids)
    res
  end
end

