# frozen_string_literal: true

class StringPolicy < ApplicationPolicy
  @@restrict = {
    'settings' => proc { privileged? || assist? },
    'mir' => proc { privileged? || assist? },
    '' => true, # important!
  }

  def show?
    # if it's not there, it's allowed
    return true unless @@restrict.key?(record)

    auth = @@restrict[record]
    auth.respond_to?(:call) ? instance_eval(&auth) : auth
  end

  def index?
    show?
  end

  def create?
    (user and show?) or super
  end

  def destroy?
    (user and show?) or super
  end

  def self.permitted_attributes
    [:id]
  end

  def permitted_attributes
    self.class.permitted_attributes
  end
end

