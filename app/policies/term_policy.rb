# frozen_string_literal: true

class TermPolicy < ApplicationPolicy
  def create?
    Pundit.policy(user, Course).create?
  end

  def populate?
    user and (admin? or user.has_role?('Teaching Committee'))
  end

  def permitted_attributes
    [:starts, :ends,
     {seminar_ids: [], course_ids: [], name_i18n: I18n.available_locales}]
  end

  def allowed_attributes
    super.push('course_ids', 'seminar_ids', 'to_slug')
  end

  def serialize_attributes
    super.push('part', 'year')
  end
end

