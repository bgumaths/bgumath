# frozen_string_literal: true

class ScholarsWebid < BaseWebid
  class << self
    def base_url
      URI('https://scholars.bgu.ac.il/display/').freeze
    end

    def generic_url(id = '')
      base_url + id
    end

    def readonly?
      true
    end
  end

  delegate :base_url, to: :class

  def id_url
    @id_url ||= self.class.generic_url(id)
  end

  def img_url
    #'https://scholar.google.com/intl/en/scholar/images/1x/sprite_20161020.png'
    nil
  end

  def service_name
    'Scholars@BGU'
  end
end

