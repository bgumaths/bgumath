# frozen_string_literal: true

class ScopusWebid < BaseWebid
  class << self
    def base_url
      URI('https://scopus.com/').freeze
    end

    def generic_url(id = 'dashboard.uri')
      base_url + id
    end

    def readonly?
      true
    end
  end

  delegate :base_url, to: :class

  def id_url
    @id_url ||= base_url + "authid/detail.uri?authorId=#{id}"
  end
end

