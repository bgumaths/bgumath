# frozen_string_literal: true

## ## Adding a new Web ID
## 1. Add a derived class of this
## 2. Add translations
## 3. Add db field to user: rails g migration add_foo_to_users foo:string:null
## 4. Add name to WEB_IDS in User model

class BaseWebid
  include ActiveModel::Model

  attr_accessor :id

  delegate_missing_to :@helper

  def h
    @helper
  end

  def initialize(id, **opts)
    @id = id
    @helper = opts[:helper]
  end

  def to_html
    render(self).html_safe
  end

  class << self
    def service_name
      name.delete_suffix('Webid').humanize
    end

    def service
      name.delete_suffix('Webid').parameterize
    end

    def service_class(srv)
      "#{srv}_webid".classify.constantize
    end

    def service_icon
      service
    end

    def readonly?
      false
    end

    def tt(**)
      I18n.t(service, scope: %i[research webid], **)
    end
  end

  delegate :service, :service_name, :service_icon, :tt, to: :class

  def partial_name
    'webid'
  end

  def to_partial_path
    "webids/#{partial_name}"
  end

  # XXX following doesn't work, can't add content to :extracss
  def icon
    h.academicon(service_icon)
  end

  def icon_link(ico = icon)
    h.link_to(ico, id_url.to_s)
  end
end

