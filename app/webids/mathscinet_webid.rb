# frozen_string_literal: true

class MathscinetWebid < BaseWebid
  class << self
    def base_url
      URI('https://mathscinet.ams.org/').freeze
    end

    def generic_url(id = 'mathscinet/search/publications.html')
      base_url + id
    end
  end

  delegate :base_url, to: :class

  def id_url
    @id_url ||= base_url + "mathscinet/MRAuthorID/#{id}"
  end
end

