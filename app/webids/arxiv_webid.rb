# frozen_string_literal: true

class ArxivWebid < BaseWebid
  class << self
    def base_url
      URI('https://arxiv.org/').freeze
    end

    def generic_url
      "#{base_url}/user/"
    end
  end

  delegate :base_url, to: :class

  def id_url
    @id_url ||= "#{base_url}a/#{id}.html"
  end

  def service_name
    'arXiv'
  end
end

