# frozen_string_literal: true

class WosWebid < BaseWebid
  class << self
    def base_url
      URI('https://publons.com/').freeze
    end

    def generic_url(id = 'dashboard/summary')
      base_url + id
    end
  end

  delegate :base_url, to: :class

  def id_url
    @id_url ||= base_url + "researcher/#{id}"
  end

  def service_name
    'Web of Science'
  end
end

