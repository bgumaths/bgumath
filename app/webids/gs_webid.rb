# frozen_string_literal: true

class GsWebid < BaseWebid
  class << self
    def base_url
      URI('https://scholar.google.com/citations').freeze
    end

    def generic_url(id = '')
      base_url + id
    end
  end

  delegate :base_url, to: :class

  def id_url
    @id_url ||= base_url + "?user=#{id}"
  end

  def service_name
    'Google Scholar'
  end
end

