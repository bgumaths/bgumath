# frozen_string_literal: true

class OrcidWebid < BaseWebid
  class << self
    def base_url
      URI('https://orcid.org/').freeze
    end

    def generic_url(id = 'my-orcid')
      base_url + id
    rescue ArgumentError
      nil
    end

    def readonly?
      true
    end
  end

  delegate :base_url, to: :class

  def id_url
    @id_url ||= self.class.generic_url(id)
  end

  def img_url
    'https://orcid.org/sites/default/files/images/orcid_16x16.png'
  end
end

