# frozen_string_literal: true

class People::MembersController < PeopleController
  def research
    members = policy_scope(User).members.by_last.decorate.as_json(
      MemberDecorator.inertia_index_params.vdeep_merge(
        include: {
          cur_students: AcademicDecorator.inertia_index_params,
          past_students: AcademicDecorator.inertia_index_params,
          postdocs: AcademicDecorator.inertia_index_params,
          research_groups: ResearchGroupDecorator.inertia_index_params,
        },
      ),
    )
    render inertia: 'People/Members/Research', props: {
      records: members,
      allGroups: policy_scope(ResearchGroup).decorate.as_json(
        ResearchGroupDecorator.inertia_index_params,
      ),
      record: object.inertia_json,
    }
  end

  def self.model
    Member
  end

  protected

  def all
    @all ||= policy_scope(User).by_last
  end

  def members
    @members ||= all.members.decorate
  end

  def people
    @people ||= all.regulars.decorate
  end

  alias_method :regulars, :people

  def kamea
    @kamea ||= all.kameas.decorate
  end

  def emeriti
    @emeriti ||= all.emeriti.decorate
  end

  def deceased
    @deceased ||= all.deceased.decorate
  end

  def serialize_objects
    [@people, @kamea, @emeriti, @deceased].map do |l|
      l.map { |x| x.to_h(current_user) }
    end
  end
end

