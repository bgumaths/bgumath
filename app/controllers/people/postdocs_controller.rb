# frozen_string_literal: true

class People::PostdocsController < PeopleController
  protected

  def visitors
    @visitors ||= policy_scope(Visitor).end_from.order(duration: :asc)&.decorate
  end

  def visitor
    @visitor ||= Visitor.new.decorate
  end

  def past_postdocs
    @past_postdocs ||= policy_scope(Postdoc).retired&.decorate
  end

  def massage_params
    super
    # host_ids may come as a string from x-editable
    fix_list_param(:host_ids)
  end

  def serialize_objects
    [@people, @visitors].map do |l|
      l.map { |x| x.to_h(current_user) }
    end
  end

  def inertia_index_props
    {
      postdocs: -> { objects.as_json(PostdocDecorator.inertia_index_params) },
      pastPostdocs:
        -> { past_postdocs.as_json(PostdocDecorator.inertia_index_params) },
      visitors: -> { visitors.as_json(VisitorDecorator.inertia_index_params) },
      postdoc: -> { form_object.inertia_json },
      visitor: -> { visitor.inertia_json },
      formInfo: -> { inertia_form_info },
    }.freeze
  end
end

