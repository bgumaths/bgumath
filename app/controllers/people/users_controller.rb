# frozen_string_literal: true

class People::UsersController < PeopleController
  # the selected photo cannot be updated with the rest of the params, since 
  # the photo may need to be created first
  after_action :update_photo, only: %i[create update]
  #before_action :cast_object, only: [:new, :edit, :create, :update]
  def index
    @people = expose_scope.by_last.all.map { |x| x.becomes(User).decorate }
    super
  end

  protected

  def item_html_object
    object.becomes(object.status.safe_constantize).decorate
  rescue NoMethodError
    object
  end

  def ajax_prerender
    obj = object.decorate
    set_admins
    headers['X-index-id'] = obj.list_id
    headers['X-index-type'] = obj.list_type
    super
  end

  def update_photo
    bare_object.update(photo_signed_id: perm_params[:photo_signed_id])
  end

  def expose_scope
    current_user ? policy_scope(collection).alive : super
  rescue Pundit::NotDefinedError
    nil
  end
end

