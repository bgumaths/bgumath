# frozen_string_literal: true

class People::VisitorsController < ApplicationController
  protected

  def extra_index_url_options
    { controller: 'people/postdocs' }
  end

  def expose_build(...)
    res = super
    res.hosts << current_user if res.hosts.blank? && current_user&.can_host?
    res
  end

  def massage_params
    super
    # host_ids may come as a string from x-editable
    fix_list_param(:host_ids)
  end
end

