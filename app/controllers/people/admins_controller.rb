# frozen_string_literal: true

class People::AdminsController < PeopleController
  helper_method :staff, :chairs

  protected

  def staff
    @staff ||= UserDecorator.with_roles(Role.staff, policy_scope(User).active)
  end
  
  def chairs
    @chairs ||= UserDecorator.with_roles(Role.head, policy_scope(User).active)
  end

  def serialize_objects
    [staff, chairs].map do |l|
      l.map { |r, u| [r.to_h(current_user), u.to_h(current_user)] }
    end
  end
end

