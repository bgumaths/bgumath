# frozen_string_literal: true

class People::Members::ResearchView < BaseView
  def has_object?
    false
  end
end

