# frozen_string_literal: true

class People::Users::HoursView < BaseView
  def allowed_locales
    I18n.available_locales.select do |loc|
      I18n.t(action_name, scope: [:actions], locale: loc, default: nil)
    end.map(&:to_s)
  end

  def has_object?
    false
  end
end

