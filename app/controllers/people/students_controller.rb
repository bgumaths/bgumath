# frozen_string_literal: true

class People::StudentsController < PeopleController
  protected

  def students
    return @students if @students.present?

    @students = {}
    %w[Phd Msc].each do |k|
      @students[k] = policy_scope(User).active.where(status: k).by_last
    end
    @students
  end

	def past_students
		@past_students ||= policy_scope(Student).retired.by_last&.decorate
	end

  def massage_params
    super
    # host_ids may come as a string from x-editable
    fix_list_param(:supervisor_ids)
  end

  def serialize_objects
    @students.values.map do |l|
      l.map { |x| x.to_h(current_user) }
    end
  end

  def inertia_index_props
    {
      phds: -> {
        students['Phd'].decorate.as_json(PhdDecorator.inertia_index_params)
      },
      mscs: -> {
        students['Msc'].decorate.as_json(MscDecorator.inertia_index_params)
      },
      pastStudents: -> {
        past_students.as_json(StudentDecorator.inertia_index_params)
      },
      phd: -> { Phd.new.decorate.inertia_json },
      msc: -> { Msc.new.decorate.inertia_json },
      formInfo: -> { inertia_form_info },
    }.freeze
  end
end

