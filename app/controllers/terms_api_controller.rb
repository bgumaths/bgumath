# frozen_string_literal: true

class TermsApiController < ApiController
  def show
    case tid = params_id(params)
    when *special
      redir_to_term Term.public_send(tid)
    else
      super
    end
  end                  

  protected

  def special
    %w[current default].freeze
  end

  def redir_to_term(term)
    params.permit!
    return unless term.respond_to?(:id)

    redirect_to params.merge(action: :show, id: term.id),
                status: :see_other
    
  end

  def find_resource
    super unless special.include?(params_id)
  end
end

