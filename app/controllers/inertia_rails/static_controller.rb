# frozen_string_literal: true

module InertiaRails
  class StaticController < ::BaseController
    inertia_share users: -> {
      %w[administrator chair coordinator director admin secretaries].index_with do |x|
        [*User.try(x)].map(&:decorate)
      end.to_h
    }

    def static
      render inertia: params[:component], props: { route: request.path }
    end

    protected

    def bare_object
      nil
    end

    def get_title(...)
      nil
    end
  end
end

