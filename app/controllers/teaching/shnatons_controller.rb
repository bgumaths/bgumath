# frozen_string_literal: true

class Teaching::ShnatonsController < ApplicationController
  include Yearwise

  after_action :clone_relations, only: [:create]
  helper_method :year, :shnaton, :preamble, :degrees, :deg_list, :by_prereq,
                :by_degree
  objectify

  protected

  def expose_id
    super || %w[index new create].exclude?(action_name)
  end

  def expose_find(id, scope)
    (id == true) ? default_object : super
  end

  def clone_relations
    object.clone_shnaton_relations!(cloned_shnaton)
    return if object.save

    flash.now[:alert] = [object.errors.full_messages.to_sentence, :en]
    
  end

  def preamble
    return @preamble if @preamble

    @preamble = if object.preamble.present?
                  markdown(object.preamble, wrapper: { class: %w[mathjax lead] })
                else
                  begin
                    Rails.public_path.join(
                      I18n.locale.to_s, 'private', 'teaching', 'shnaton', 
                      shnaton.ac_year.year.to_s, 'undergrad.html'
                    ).read.html_safe
                  rescue Errno::ENOENT
                    ''
                  end
                end
  end

  def degrees
    return @degrees if @degrees

    @degrees = {}
    degree_list = bare_object.degrees
    Degree.types.each_key do |type|
      @degrees[type] = degree_list.public_send(type)&.decorate
    end
    @degrees
  end

  def deg_list
    return @deg_list if @deg_list

    @deg_list = bare_object.degrees & by_degree.keys
  end

  def by_prereq
    return @by_prereq if @by_prereq

    @by_prereq, @by_degree = shnaton.summary(:decorate)
    @by_prereq
  end

  def by_degree
    return @by_degree if @by_degree

    @by_prereq, @by_degree = shnaton.summary(:decorate)
    @by_degree
  end

  def cloned_shnaton
    Shnaton.for_year(params[:shnaton].try(:[], :clone_year))
  end

  def expose_build(...)
    if (old = cloned_shnaton)
      res = old.am_dup
      res.ac_year = ac_year.object
      res
    else
      super
    end
  end

  # allow duplicate
  def dup_object
    default_object if ac_year_id
  end

  # override from yearwise
  def collection
    model
  end

  def default_object
    ensure_year
    ac_year.shnaton
  end

  def form_opts
    super.deep_merge(url: object.show_path)
  end

  # our default year is the last one that has a handbook
  def default_year
    AcYear.where('year <= :cur', cur: CommonUtil.cur_year)
          .joins(:shnaton).order(year: :desc).limit(1).take
  end

  def year
    ac_year.try(:year)
  end
end

