# frozen_string_literal: true

class Teaching::IssuesUserSessionsController < BaseSessionsController
  helper_method :form_path, :form_opts, :issues_user_sessions_path
  prepend_before_action :load_authlogic
  objectify

  def create
    if issues_user_session.save
      session_reset
      user = issues_user_session.issues_user.decorate
      if user.pass_changed?
        flash[:notice] = I18n.t 'session.loggedin',
                                user:,
                                last_login_at: l(user.last_login_at,
                                                 format: :long),
                                last_login_ip: user.last_login_ip
        redirect_to teaching_issues_path(show_flowchart: true)
      else
        flash[:notice] = I18n.t 'session.pass_change'
        redirect_to user.edit_path
      end
    else
      flash[:alert] =
        I18n.t 'session.invalid',
               message: issues_user_session.errors.full_messages.to_sentence
      redirect_to(in_outer_locale { login_teaching_issues_path })
    end
  end

  def destroy
    IssuesUserSession.find.try(:destroy)
    redirect_to teaching_issues_path
  end

  protected

  def form_path
    bare_object
  end

  def issues_user_sessions_path(...)
    login_teaching_issues_path(...)
  end

  def form_opts
    {}
  end

  def expose_build(param = perm_params(nil), scope = collection)
    scope.new(param)
  end

  def load_authlogic
    Authlogic::Session::Base.controller ||=
      Authlogic::ControllerAdapters::RailsAdapter.new(self)
  end
end

