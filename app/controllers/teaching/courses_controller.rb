# frozen_string_literal: true

class Teaching::CoursesController < ApplicationController
  include Termwise
  before_action :redir_generic, if: :update_from_generic?,
                                only: %i[create update]
  helper_method :issues, :courses_for
  objectify

  expose :year, -> { params[:year] || CommonUtil.cur_year }
  expose :fterm, -> { Term.fall(year)&.decorate }
  expose :sterm, -> { Term.spring(year)&.decorate }
  expose :fall, -> { policy_scope(fterm.courses) }
  expose :spring, -> { policy_scope(sterm.courses) }
  expose :lecturers, -> { policy_scope(User.lecturers).decorate }

  # call bare_objects if no term exists, so that we don't fail for 
  # policy_scope
  def index
    bare_objects unless fterm || sterm
    super
  end

  def new
    super

    update_from_generic

    # set the most likely term
    course.term ||= Term.default_no_exams

    # initialise from shnaton, if needed
    update_from_shnaton if params[:shnaton]

    # This returns, via ajax, the values to update when the 'update from 
    # generic' button is pressed in the modal form for a new course
    respond_to do |format|
      format.text do
        render json: {
          item: false,
          form: object.to_form_content(
            *%i[generic_course_id lecturer_id web hours 
                title_i18n content_i18n], prefix: id_prefix
          ),
        }
        return
      end
      format.any
    end
  end

  def edit
    super

    # we do this conditionally, since we may want to leave fields blank on
    # purpose
    if params[:generic_course_id].present?
      update_from_generic
    end

    # set the most likely term
    course.term ||= Term.default_no_exams

    # initialise from shnaton, if needed
    update_from_shnaton if params[:shnaton]

    # save the course, so that attached files don't disappear
    #course.save
  end

  protected

  def dup_object
    Course.find(params[:course_id]) if params[:course_id]
  end

  def redir_generic
    opts = if (slug = object.try(:slug))
             { action: :edit, id: slug }
           else
             { action: :new }
           end
    redirect_to(opts.merge(
                  generic_course_id: perm_params[:generic_course_id],
                  id_prefix: params[:id_prefix],
                  shnaton: 1, model_param => redir_params
                ))
  end

  def form_opts
    super.deep_merge(html: { multipart: true })
  end

  def orig
    Course.find(params[:course_id])
  end

  def update_from_generic?
    params.key?(:commit) && params[:commit].casecmp('generic_course').zero?
  end

  def update_from_generic(gid = params[:generic_course_id])
    return if gid.blank?

    gc = GenericCourse.find_by(id: gid)
    course.generic_course = gc if gc.present?
    
  end

  def update_from_shnaton
    course.init_from_shnaton!
  rescue RestClient::NotFound
    flash[:alert] = tt('errors.shnaton_failed')
  end

  def redir_params
    perm_params
  end

  def issues
    @issues ||= policy_scope(Issue).where(course: bare_object)
  end

  def serialize_object
    res = super
    res[:generic_course] = course.generic_course.to_h(current_user)
    res[:term] = course.term.to_h(current_user)
    res[:lecturer] = course.lecturer.to_h(current_user)
    res
  end

  def ajax_prerender
    headers['X-index-id'] = "item-list-#{bare_object.level}"
  end

  def id_prefix
    # actionview adds underscore, hence so must we
    params[:id_prefix].present? ? "#{params[:id_prefix]}_" : ''
  end

  def inertia_form_info
    super.vdeep_merge(
      terms: inertia_form_fields(Term),
      student_reps: inertia_form_fields(IssuesStudentRep),
      generic_courses: inertia_form_fields(GenericCourse),
      lecturers: inertia_form_fields(User.lecturers),
    )
  end

  def inertia_show_props
    super.vdeep_merge(
      genericCourse: -> { inertia_object.generic_course.inertia_json },
    )
  end
end

