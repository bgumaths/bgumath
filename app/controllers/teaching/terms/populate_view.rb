# frozen_string_literal: true

class Teaching::Terms::PopulateView < BaseView
  def has_object?
    false
  end
end

