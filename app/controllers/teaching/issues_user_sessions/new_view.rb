# frozen_string_literal: true

class Teaching::IssuesUserSessions::NewView < NewView
  def has_object?
    false
  end

  def allowed_locales
    %w[he]
  end
end

