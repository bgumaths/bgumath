# frozen_string_literal: true

class Teaching::PlanItemsController < ApplicationController
  helper_method :degree_plan
  objectify

  delegate :degree_plan, to: :term_plan

  include DegreePart

  def index
    redirect_to degree.decorate.show_path, status: :moved_permanently
  end

  def show
    redirect_to degree.decorate.show_path, status: :moved_permanently
  end

  protected

  def term_plan_id
    params[:term_plan] || params[:term_plan_id]
  end

  def term_plan
    @term_plan ||= object&.plan
  end

  def collection
    term_plan ? term_plan.plan_items : PlanItem
  end
end

