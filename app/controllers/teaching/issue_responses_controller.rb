# frozen_string_literal: true

class Teaching::IssueResponsesController < Teaching::IssuesBaseController
  objectify

  before_action :set_replier, only: %i[create update]

  after_action :schedule_reminder, if: :updated?
  after_action :send_update_email, unless: :no_email?

  def create
    perform(bare_object.save)
  end

  def update
    return publish! if publish?

    perform bare_object.update(perm_params)
  end

  def destroy
    perform bare_object.destroy
  end

  protected

  def mailer
    Teaching::IssueMailer
  end

  def perform(res)
    if res
      success_redirect
    else
      fail_redirect
    end
  end

  def issue
    object&.issue
  end

  def show_path
    issue&.show_path || super
  end

  def set_replier
    bare_object.creator ||= current_user
  end

  def schedule_reminder
    SendPublishReminderJob.set(wait: bare_object.publish_delay.hours)
                          .perform_later(issue.object)
  end

  def fail_redirect
    errors = object.errors.full_messages.to_sentence
    logger.warn(errors)
    redirect_to show_path, flash: { alert: errors }
  end

  def publish?
    params[:button] == 'publish'
  end

  def no_email?
    params[:button] == 'commit_no_email'
  end

  def publish!
    if params[:issue_response][:content] == object.content
      perform object.publish!
    else
      redirect_to show_path, flash: { alert: t('.publish_modified') }
    end
  end
end

