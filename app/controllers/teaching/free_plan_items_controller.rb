# frozen_string_literal: true

class Teaching::FreePlanItemsController < ApplicationController
  helper_method :degree_plan, :term_plan
  objectify

  delegate :degree_plan, to: :term_plan

  include DegreePart

  def index
    redirect_to degree.decorate.show_path, status: :moved_permanently
  end

  def show
    redirect_to degree.decorate.show_path, status: :moved_permanently
  end

  protected

  def term_plan
    @term_plan ||= object&.term_plan
  end

  def collection
    policy_scope(@term_plan.present? ? @term_plan.free_plan_items : model)
  end
end

