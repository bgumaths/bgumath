# frozen_string_literal: true

class Teaching::GenericCoursesController < ApplicationController
  before_action :redir_shnaton, if: :update_from_shnaton?,
                                only: %i[create update]
  before_action :update_from_shnaton, only: %i[new edit]
  helper_method :all
  objectify

  expose :undergrad, -> { all.active.where(graduate: false).decorate }
  expose :grad, -> { all.active.where(graduate: true).decorate }
  expose :dep, -> { all.deprecated }
  expose :undergrad_dep, -> { dep.where(graduate: false).decorate }
  expose :grad_dep, -> { dep.where(graduate: true).decorate }

  protected

  # allow finding by shid
  def expose_find(id, scope)
    /^\d{3}-\d-\d{4}$/.match?(id) ? scope.find_by(shid: id.tr('-', '.')) : super
  end

  def dup_object
    GenericCourse.find(params[:generic_course_id]) if params[:generic_course_id]
  end

  def redir_shnaton
    opts = if (slug = object.try(:slug).presence)
             { action: :edit, id: slug }
           else
             { action: :new }
           end
    redirect_to(opts.merge(generic_course: perm_params))
  end

  # fix values for editable
  def massage_params
    super
    return unless params[:generic_course]

    %i[prereq core graduate advanced spring fall].each do |f|
      if params[:generic_course].key?(f) &&
         params[:generic_course][f].respond_to?(:push)
        params[:generic_course][f] = !params[:generic_course][f][0].empty?
      end
    end
    fix_list_param(:require_ids)
  end

  def update_from_shnaton?
    params.key?(:commit) && params[:commit].casecmp('shnaton').zero?
  end

  def update_from_shnaton
    object.init_from_shnaton!(params[:shid]) if params[:shid].present?
  rescue GenericCourse::ShnatonError => e
    flash[:alert] = current_user.try(:is_Admin?) ? e.to_s : t('errors.messages.update_from_shnaton')
  end

  def inertia_form_info
    super.vdeep_merge(
      departments: inertia_form_fields(Department),
      aguda_reps: inertia_form_fields(IssuesAgudaRep),
      generic_courses: inertia_form_fields(GenericCourse),
      course_lists: inertia_form_fields(CourseList),
      ac_years: inertia_form_fields(AcYear),
    )
  end

  def form_opts
    super.deep_merge(html: { multipart: true })
  end

  def all
    @all ||= policy_scope(params[:advanced] ? GenericCourse.advanced : GenericCourse)
  end

  def inertia_show_props
    super.vdeep_merge(
      genericCourse: -> { inertia_object.inertia_json },
      avail_terms:
        -> { inertia_object.courses.map(&:term).map(&:inertia_json) },
    )
  end
end

