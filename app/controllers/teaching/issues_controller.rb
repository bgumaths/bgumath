# frozen_string_literal: true

class Teaching::IssuesController < Teaching::IssuesBaseController
  objectify

  helper_method :staff, :courses, :lecturer, :aguda_rep,
                :issue_response, :response_policy, :response_form_path,
                :response_form_opts, :replier, :show_response

  before_action :system_health, only: %i[new create]
  before_action :set_reporter, only: [:create]

  after_action :send_created_email, only: [:create], if: :created?
  after_action :send_missing_lecturer, only: [:create], if: :missing_lecturer?

  %i[reporter student_rep departments course].each do |rol|
    expose rol, from: :issue
  end

  rescue_from Bgumath::NoIssuesRespondersError, with: :no_issues_responders

  def send_created
    send_email(:created)
  end

  def send_update
    send_email(:update)
  end

  protected

  # redirect the issues user to change the password if it was reset
  def non_authorized_redirect
    if current_issues_user && !current_issues_user.pass_changed?
      flash[:alert] = I18n.t 'session.pass_change'
      current_issues_user.decorate.edit_path
    else
      in_outer_locale { login_teaching_issues_path }
    end
  end

  ### helper methods ###
  def aguda_rep
    policy.show_aguda? ? issue.aguda_rep : nil
  end

  def staff(...)
    policy.show_staff? ? issue.staff(...) : nil
  end

  def courses
    return @courses if @courses.present?

    @courses = policy.courses.map(&:decorate)
    object.course = @courses[0] if @courses.length == 1
    @courses
  end

  delegate :lecturer, to: :course

  def replier
    issue_response&.creator
  end

  def set_reporter
    bare_object.reporter ||= current_issues_user
  end

  def issue_response
    @issue_response ||=
      issue.response || (if policy.respond?
                           issue.build_response(creator: current_user).decorate
                         end)
  end

  def response_policy
    @response_policy ||= policy(issue_response)
  end

  def show_response
    response_policy&.modify? ||
      (response_policy&.show? && issue_response&.has_content?)
  end

  def response_form_path
    issue_response.form_path
  end

  def response_form_opts
    # autocomplete off to work around 
    # https://github.com/rails/rails/issues/37864
    form_opts.deep_merge(html: {autocomplete: :off})
  end

  def missing_lecturer?
    created? && issue&.course&.lecturer.blank?
  end

  def send_missing_lecturer
    mailer.missing_lecturer(issue)&.deliver_later
  end

  def no_issues_responders(_exc)
    err_text = I18n.t('errors.messages.issues_noresponders',
                      locale: :en)
    respond_to do |format|
      format.text do
        headers['X-validation-errors'] = err_text
        render partial: 'flash',
               locals: {
                 type: :alert,
                 message: render_to_string(
                   'sysdeny', view_source_map: false, formats: :html,
                              layout: false
                 ),
               },
               formats: :html, status: :unprocessable_entity
      end
      format.html do
        render 'sysdeny'
      end
    end
  end
end

