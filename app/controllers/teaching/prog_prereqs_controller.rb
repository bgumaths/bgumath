# frozen_string_literal: true

class Teaching::ProgPrereqsController < ApplicationController
  helper_method :types, :parent, :course_list
  objectify

  expose :unattached, -> { ProgPrereq.unattached.decorate }

  include DegreePart

  def self.types
    @@types ||= %i[PreciseProgPrereq BasicProgPrereq]
    #@@types ||= model.subclasses.map(&:to_s).select(&:presence)
  end

  def policy_model
    params[model_param].try(:[], :type).try(:constantize) || super
  end

  protected

  def course_list
    list = bare_object.course_lists.new
    list.prereqs << bare_object
    list.decorate
  end

  def dup_object
    ProgPrereq.find(params[:dup_obj]) if params[:dup_obj]
  end

  def types
    self.class.types
  end

  def base_model_name
    'ProgPrereq'
  end

  def form_opts
    super.deep_merge(as: :prog_prereq, html: { id: :prog_prereq })
  end

  types.each do |type|
    helper_method :"teaching_shnaton_degree_#{type.to_s.underscore}_path"
    helper_method :"teaching_shnaton_degree_#{type.to_s.underscore}s_path"
    helper_method :"teaching_ac_year_shnaton_degree_#{type.to_s.underscore}_path"
    helper_method :"teaching_ac_year_shnaton_degree_#{type.to_s.underscore}s_path"
  end

  def respond_to_missing?(name, *)
    types.each do |type|
      # named captures don't work with interpolation!
      return true if /^(teaching_(ac_year_)?shnaton_degree)_#{type.to_s.underscore}(s?_path)$/.match?(name.to_s)
    end
    super
  end

  # do it like this since in production the helper methods are not yet loaded
  def method_missing(name, ...)
    types.each do |type|
      # named captures don't work with interpolation!
      if /^(teaching_(ac_year_)?shnaton_degree)_#{type.to_s.underscore}(s?_path)$/ =~ name.to_s
        return public_send(:"#{$1}_prog_prereq#{$3}", ...)
      end
    end
    super
  end

  def collection
    policy_scope(degree_id ? param_degree.all_prereqs : model)
  rescue ActiveRecord::RecordNotFound
    model.none
  end

  # need this, since above collection does not produce well-initialized
  # objects
  def expose_build(param = perm_params(nil), scope = collection)
    res = nil
    if (dup_obj = dup_object)
      res = dup_obj.am_dup(param)
      res.course_lists = dup_obj.course_lists
    else
      # do this in steps, since course_lists expects shnaton to be
      # initialized
      res = scope.new
      res = res.becomes(param[:type].constantize) if param[:type].present?
      res.attributes = param
    end
    res.parent = param_parent
    res.degree = param_degree
    res
  end

  def parent_id
    params[:parent] || params[:parent_id]
  end

  def param_parent
    parent_id ? collection.find(parent_id) : nil
  end

  def parent
    @parent ||= bare_object.try(:parent) || param_parent
  end
end

