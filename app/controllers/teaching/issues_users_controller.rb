# frozen_string_literal: true

class Teaching::IssuesUsersController < Teaching::IssuesBaseController
  after_action :update_pass_changed, only: %i[create update]
  after_action :send_creation_email, only: %i[create], if: :created?
  helper_method :issues_user_types, :recent_courses, :courses, :terms,
                :department, :type, :issues_staff, :issues_aguda_rep,
                :issues_student_rep, :recent_terms

  objectify

  def reset_password
    if bare_object == current_issues_user
      bare_object.crypted_password = bare_object.password_salt = nil
    else
      bare_object.generate_pass
    end
    respond_to do |format|
      if bare_object.save
        format.html do
          success_redirect
        end
        format.any(*formats) do
          render_object status: :ok, location: show_path
        end
        begin
          email = mailer.reset_password(object, cc: [User.admin, current_user])
          email.deliver_now
        rescue StandardError => e
          # There might an exception due to gpg error, which is not 
          # meaningful (eg NULL pointer), 
          # https://github.com/jkraemer/mail-gpg/issues/66
          logger.warn "Reset password: #{e.message}, Email: #{email}, Object: #{object}"
        end
      else
        format.html do
          decorate
          redirect_to(
            {action: :show},
            flash: {
              alert: [bare_object.errors.full_messages.to_sentence, :en],
            },
          )
        end
        format.any(*formats) do
          render_format bare_object.errors, status: :unprocessable_entity
        end
      end
    end
  end

  def self.issues_user_types
    IssuesUser.types
  end

  def self.stypes
    @stypes ||= issues_user_types.map { |x| x.model_name.singular }
  end

  protected

  def params_type
    (params[:issues_user] || {})[:type]
  end

  def type(obj = bare_object)
    res = params_type&.underscore || obj&.model_name&.singular
    safe_redir_back alert: tt('errors.issues_users.type_missing') unless
      self.class.stypes.include?(res)
    res
  end

  def collection
    (params_type || 'IssuesUser').constantize
  end

  def expose_build(...)
    obj = super
    return obj unless params_type != obj.type && 
                      (const = params_type.try(:constantize))

    res = obj.becomes!(const)
    # password not copied, since it is not an attribute
    res.password = obj.password
    res.password_confirmation = obj.password_confirmation
    res
  end

  def issues_user_types
    self.class.issues_user_types
  end

  issues_user_types.each do |type|
    helper_method :"teaching_#{type.to_s.underscore}s_path"
    helper_method :"teaching_#{type.to_s.underscore}_path"
  end

  def respond_to_missing?(name, ...)
    nn = name.to_s
    issues_user_types.each do |type|
      return true if nn == "teaching_#{type.to_s.underscore}s_path"
      return true if nn == "teaching_#{type.to_s.underscore}_path"
    end
    super
  end

  def method_missing(name, ...)
    issues_user_types.each do |type|
      if name.to_s == "teaching_#{type.to_s.underscore}s_path"
        return teaching_issues_users_path(...)
      end
      if name.to_s == "teaching_#{type.to_s.underscore}_path"
        return teaching_issues_user_path(...)
      end
    end
    logger.warn "Method missing?: #{name}"
    super
  end

  def update_pass_changed
    return unless perm_params[:password].present? &&
                  object.errors.messages.blank?

    object.pass_changed = true
    object.save!
    mailer.changed_password(object).deliver_later unless action_name == 'create'
  end

  def form_opts
    super.deep_merge(as: :issues_user, html: { id: :issues_user })
  end

  def send_creation_email
    # do not use deliver_later here, since the password in object will not 
    # be saved when (de)serializing
    mailer.account(object).deliver_now
  rescue StandardError => e
    flash[:alert] = e.message
  end

  def terms
    return @terms if @terms

    cur = Term.current
    @terms = [cur, cur&.other].map(&:presence)
  end

  # courses in the given term belonging to this user
  def courses(trm)
    @courses ||= {}
    @courses[trm] ||= policy_scope(bare_object.courses).this_term(trm).presence
  end

  def recent_courses(terms = 4)
    policy_scope(Course.this_term(Term.last(terms))).by_term.by_shid.decorate.map do |cc|
      [cc.to_label(false), cc.id,
       {class: ['list-select-item-term-select mx-2', cc.term.to_id]}]
    end
  end

  def recent_terms(count = 4, **opts)
    policy_scope((opts[:scope] || Term).reorder(starts: :desc).limit(count)).decorate
  end

  def department
    @department ||= object.try(:department)
  end

  def massage_params
    super
    return unless params[:issues_user] &&
                  (active = params[:issues_user][:active])

    params[:issues_user][:active] = active.present? && active != '0'
  end

  def item_changes(item)
    mailer.account(item.decorate).deliver_later if item.saved_change_to_id?
    super
  end

  def issues_staff(dept)
    IssuesStaff.new(department: dept).decorate
  end

  def issues_aguda_rep
    IssuesAgudaRep.new.decorate
  end

  def issues_student_rep
    IssuesStudentRep.new.decorate
  end
end

