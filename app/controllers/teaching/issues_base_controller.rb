# frozen_string_literal: true

class Teaching::IssuesBaseController < ApplicationController
  helper_method :current_issues_user_session, :current_issues_user

  protected

  # issues user takes precedence, so we can override our dept login
  def pundit_user
    current_issues_user || current_user
  end

  # store slugs for issues users, ids for regular users, so we may
  # distinguish
  def user_for_paper_trail
    if current_issues_user
      current_issues_user.try(:to_slug)
    else
      current_user.try(:id)
    end
  end

  def current_issues_user_session
    current_user_session(IssuesUser)
  end

  def current_issues_user
    current_user(IssuesUser)
  end

  def send_email(type)
    email = send(:"send_#{type}_email")
    header = email.header.fields.map { |x| "#{x.name}: #{x.decoded}" }
                  .join("\n")
    body = (email.parts.first || email.body).decoded
    render json: if email.present?
                   {
                     header:, 
                     body:,
                     fulltext: "#{header}\n\n#{body}",
                   }
                 else
                   { status: :unprocessable_entity }
                 end
  end

  def recepient
    return @recepient if @recepient.present?

    return unless params[:to]

    @recepient = (User.find_by(email: params[:to]) ||
                  IssuesUser.find_by(login: params[:to]))&.decorate
  end

  def send_created_email
    msg = if User.repliers.present?
            mailer.created(issue, **(recepient ? { to: recepient } : {}))
          else
            logger.warn('Issue created but no repliers present!')
            mailer.missing_repliers(issue)
          end

    msg&.deliver_later
    msg
  end

  def send_update_email
    msg = mailer.public_send(object.published? ? :published : :replied,
                             issue, **(recepient ? { to: recepient } : {}))
    msg&.deliver_later
    msg
  end

  # deny creating a new issue if system is unhealthy (no responders)
  def system_health
    return if User.repliers.present?

    mailer.missing_repliers(current_issues_user).deliver_later
    raise Bgumath::NoIssuesRespondersError
  end
end

