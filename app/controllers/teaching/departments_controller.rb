# frozen_string_literal: true

class Teaching::DepartmentsController < ApplicationController
  helper_method :teaching_issues_staffs_path
  objectify

  expose :issues_staff, -> { IssuesStaff.new(department: object).decorate }

  protected

  def teaching_issues_staffs_path(...)
    teaching_issues_users_path(...)
  end
end

