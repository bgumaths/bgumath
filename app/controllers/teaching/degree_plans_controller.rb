# frozen_string_literal: true

class Teaching::DegreePlansController < ApplicationController
  include DegreePart
  objectify

  def index
    redirect_to degree.decorate.show_path, status: :moved_permanently
  end

  def show
    redirect_to object.degree.show_path, status: :moved_permanently
  end

  protected

  def dup_object
    if (orig = params[:degree_plan_id] ||
               params[:degree_plan].try(:[], :degree_plan_id))
      collection.find(orig)
    end
  end

  def collection
    degree_id.present? ? param_degree.plans : model
  rescue ActiveRecord::RecordNotFound
    model.none
  end
end

