# frozen_string_literal: true

class Teaching::TermPlansController < ApplicationController
  before_action :build_free_item, only: %i[new edit]
  helper_method :degree_plan, :degree_plan_id
  objectify

  include DegreePart

  def index
    redirect_to degree.show_path, status: :moved_permanently
  end

  def show
    redirect_to degree.show_path, status: :moved_permanently
  end

  protected

  def build_free_item
    object.free_plan_items.build #if object.free_plan_items.blank?
  end

  def massage_params
    return if params[:term_plan][:free_plan_items_attributes].blank?

    params[:term_plan][:free_plan_items_attributes] =
      params[:term_plan][:free_plan_items_attributes].select do |_k, v|
        v[:minp].present? && v[:maxp].present?
      end
    
  end

  def degree_plan_id
    params[:degree_plan] || params[:degree_plan_id]
  end

  def param_degree_plan
    @param_degree_plan ||= param_degree.plans.find(degree_plan_id)
  end

  def degree_plan
    @degree_plan ||= object&.degree_plan || param_degree_plan
  end

  def collection
    param_degree_plan.term_plans
  rescue ActiveRecord::RecordNotFound
    TermPlan.none
  end
end

