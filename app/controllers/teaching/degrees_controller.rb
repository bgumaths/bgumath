# frozen_string_literal: true

class Teaching::DegreesController < ApplicationController
  include ShnatonPart
  helper_method :prereqs, :degtypes
  objectify

  protected

  def degtypes
    return @degtypes if @degtypes

    @degtypes = {}
    Degree.types.each_key do |type|
      deg = collection.public_send(type)
      @degtypes[type] = deg.decorate if deg.present?
    end
    @degtypes
  end

  def prereqs
    @prereqs ||= bare_object.prereqs.top_level.decorate
  end
end

