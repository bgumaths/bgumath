# frozen_string_literal: true

class Teaching::TermsController < ApplicationController
  prepend_before_action :save_seminars, only: [:create]
  after_action :dup_seminars, only: [:create]
  helper_method :prev, :next, :course, :courses, :undergard, :grad,
                :lecturers
  objectify

  def new
    super
    term.seminars = Term.default.seminars.dup if Term.default.respond_to? :seminars
  end

  # populate from shnaton
  def populate
    active = begin
      GenericCourse.shnaton_active(term)
    rescue RestClient::NotFound, RestClient::ServiceUnavailable => e
      safe_redir_back(alert: "Failed to connect to BGU catalogue: #{e.message} (trying '#{e.response&.request&.uri}')")
      return
    end
    @result = []
    active.each do |gc, info|
      if gc.in_term(term)
        @result << gc.decorate
        next
      end

      course = gc.instance_from_shnaton(term, info)
      course&.save
      @result << course.decorate
    end
  end

  protected

  def prev
    @prev ||= term&.prev&.decorate
  end

  def next
    @next ||= term&.next&.decorate
  end

  def courses
    @courses ||= policy_scope(term.courses)
  end

  def undergrad
    @undergrad ||= courses&.undergrad&.decorate
  end

  def grad
    @grad ||= courses&.grad&.decorate
  end

  def course
    @course ||= Course.new(term:).decorate
  end
  
  def lecturers
    @lecturers ||= User.lecturers.decorate
  end

  def expose_id
    res = super
    # switched notation for fall terms...
    res = 'fall2017' if res.to_s == 'fall2018'
    res ||= true unless %w[index new create].include?(action_name)
    res
  end

  def expose_find(id, scope)
    (id == true) ? Term.default : super
  end

  # remove the ids from the params, so that term validates, and save it for
  # updating later
  def save_seminars
    @semids = params[model_param].delete(:seminar_ids).compact_blank
  end

  def dup_seminars
    return unless created?

    success = []
    danger = []
    @semids.each do |id|
      # following is wrong, since the default term is now the new one
      #s = Term.default.seminars.find(id)
      # just search among all seminars
      s = Seminar.find_by(id:)
      next unless s

      sn = s.dup
      sn.id = nil
      sn.slug = s.slug
      sn.term = term
      if sn.save
        set_admins(sn, s.admins)
        success << I18n.t('success.create', what: sn.decorate.to_s)
      else
        danger << I18n.t([sn.errors.full_messages.to_sentence, :en])
      end
      flash[:notice] = success.join(', ') if success.present?
      flash[:alert] = danger.join(', ') if danger.present?
    end
  end
  
end

