# frozen_string_literal: true

class BaseSessionsController < BaseController
  protected

  # reset session as per rails security guide
  def session_reset
    temp_session = session.to_hash
    reset_session
    session.update temp_session.except 'session_id'
  end
end

