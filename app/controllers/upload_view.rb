# frozen_string_literal: true

class UploadView < BaseView
  def has_object?
    false
  end
end

