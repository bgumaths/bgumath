# frozen_string_literal: true

class UsersApiController < ApiController
  def show
    case params_id
    when 'special'
      render json: 
        special_users.index_with { |who| special_user(who) }
    when *special_users
      redir_to_user resource_data
    else
      super
    end
  end                  

  protected

  def special_users
    %w[me administrator chair coordinator director admin secretaries].freeze
  end

  def special?(who = params_id)
    who == 'special' || special_users.include?(who.to_s)
  end

  def special_user(who = params_id)
    (who == 'me') ? [current_user] : [*User.try(who)]
  end

  def redir_to_user(user)
    params.permit!
    return unless user.respond_to?(:id)

    redirect_to params.merge(action: :show, id: user.id),
                status: :see_other
    
  end

  def resource_data
    @resource_data ||= special? ? special_user.first : super
  end
end

