# frozen_string_literal: true

class GsheetsUpdateView < BaseView
  def has_object?
    false
  end
end

