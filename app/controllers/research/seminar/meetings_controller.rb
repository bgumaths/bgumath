# frozen_string_literal: true

class Research::Seminar::MeetingsController < ApplicationController
  include Datewise
  helper_method :msg
  objectify

  def index
    respond_to do |format|
      format.any(:html, :tex, :pdf) do
        # We don't use policy_scope, since we redirect
        skip_policy_scope
        redirect_to seminar_any.decorate.show_path
      end
      format.any { super }
    end
  end

  def new
    super
    meeting.date = seminar.default_date
  end

  def email
    addr = current_user.decorate.email_address(true)
    Research::Seminar::MeetingMailer.announce(
      object, addr, subject: seminar.decorate.to_s
    ).deliver_now
    safe_redir_back notice: "Email sent to '#{addr}'"
  end

  def inertia_show_path
    'Research/Seminars/Meetings/Show'
  end

  def inertia_edit_path
    'Research/Seminars/Meetings/Edit'
  end

  protected

  def msg
    to = seminar.list_id || ''
    to += "@#{AppSetup.ml_mail_domain}" unless to.blank? || to.include?('@')

    @msg ||= Research::Seminar::MeetingMailer.announce(
      meeting, to, subject: seminar.decorate.to_s
    ).message
  end

  def term_id
    return @term_id if @term_id

    @term_id = params[:term] || params[:term_id]
    @term_id = 'fall2017' if @term_id.to_s == 'fall2018'
    @term_id
  end

  def seminar_id
    params[:seminar] || params[:seminar_id]
  end

  def term
    @term ||=
      begin
        Term.find(term_id)
      rescue ActiveRecord::RecordNotFound
        Term.current(perm_params(nil)[:date] || Date.current)
      end
  end

  def seminar
    @seminar ||= term&.seminars&.find(seminar_id)
  end

  def seminar_any
    seminar
  rescue ActiveRecord::RecordNotFound
    Seminar.find(seminar_id)
  end

  def title_opts
    sem = seminar_any.try(:decorate)
    super.merge(
      { seminar: sem }.merge(
        object ? { date: l(object.try(:date)) } : {},
      ),
    )
  end

  def collection
    policy_scope(seminar_any.meetings)
    #term_id ? seminar.meetings : Meeting.of_seminar(seminar_id)
  rescue ActiveRecord::RecordNotFound
    Meeting.none
  end

  # include all meetings of the seminar in the calendar, not just per term
  def ical_events
    policy_scope(Meeting.of_seminar(seminar_id))
  end

  def ical_title
    tt(:of_uni, what: seminar_any.decorate.to_s)
  end

  def ics_path(**)
    seminar.decorate.meetings_path(format: :ics, **)
  end

  def atom_author
    @atom_author ||= seminar_any.admin_users.first&.decorate
  end

  def atom_author_name
    atom_author&.to_s
  end

  def atom_author_email
    atom_author&.email_address
  end

  def atom_author_uri
    atom_author&.url
  end

  def massage_params
    super
    mparams = params[:meeting]
    return if mparams.blank?

    if mparams[:date].present?
      mparams[:date] = Date.strptime(mparams[:date], '%Y-%m-%d')
    end

    return unless mparams[:descfiles].respond_to?(:map)

    vf = mparams[:descfiles].reject { |d| d[:_destroy] }
    mparams[:descfiles] = vf.map { |d| d[:signed_id] || d }
    
  end

  def form_opts
    super.deep_merge(html: { multipart: true })
  end

  def form_resent
    super.merge(meeting_date: seminar.default_date)
  end
end

