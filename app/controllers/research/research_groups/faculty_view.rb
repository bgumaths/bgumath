# frozen_string_literal: true

class Research::ResearchGroups::FacultyView < BaseView
  def has_object?
    false
  end
end

