# frozen_string_literal: true

class Research::Seminars::WeeklyView < BaseView
  def has_object?
    false
  end
end

