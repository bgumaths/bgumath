# frozen_string_literal: true

class Research::EventsController < ApplicationController
  include Datewise

  helper_method :cur, :recent, :past
  objectify

  def email
    addr = current_user.decorate.email_address(true)
    Research::EventMailer.announce(object, addr).deliver_now
    safe_redir_back notice: "Email sent to '#{addr}'"
  end

  protected

  def cur
    @cur ||= expose_scope.end_from.asc.decorate
  end
  
  def recent
    @recent ||= expose_scope.recent.desc.decorate
  end
  
  def past
    @past ||= expose_scope.past.desc.decorate
  end

  def inertia_objects
    @inertia_objects ||= expose_scope.fresh.desc.decorate
  end

  def ical_title
    tt(:of_uni, what: tt('research.events'))
  end

  def serialize_objects
    [cur, recent, past].each do |l|
      l.map { |x| x.to_h(current_user) }
    end
  end

  def form_opts
    super.deep_merge(html: { multipart: true })
  end
end

