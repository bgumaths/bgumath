# frozen_string_literal: true

class Research::ResearchGroupsController < ApplicationController
  helper_method :tabs_to_name, :tabs_to_id, :groupless
  objectify

  def index
    # we may have a partial reload, so we don't load groups again, and don't 
    # run policy_scope
    skip_policy_scope
    inertia_index
  end

  def show
    redirect_to({action: :index, params: { research_group: object.to_param }})
    #inertia_show
  end

  protected

  def inertia_show_path
    inertia_index_path
  end

  def inertia_form_info
    super.vdeep_merge(
      seminars: inertia_form_fields(Seminar),
      members: inertia_form_fields(User.academic),
    )
  end

  def inertia_index_props
    res = super
    if params[:researchGroup]
      group = collection.find(params[:researchGroup]).try(:decorate)
      authorize group, :show?
      # preloading issues
      Regular # rubocop:disable Lint/Void
      Kamea # rubocop:disable Lint/Void
      if group
        res = res.vdeep_merge(
          researchGroup: -> { group.inertia_json },
        )
      end
    end
    res
  end

  def inertia_show_props
    super.vdeep_merge(inertia_index_props)
  end

  def groupless
    @groupless ||= policy_scope(Member.active.groupless).decorate
  end

  def tabs_to_name(obj = object)
    obj.to_s
  end

  def tabs_to_id(obj = object)
    obj.to_id
  end

  def ajax_prerender
    headers['X-tab-item'] = tabs_to_id
    headers['X-tab-name'] = tabs_to_name
  end
end

