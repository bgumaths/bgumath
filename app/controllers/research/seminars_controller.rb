# frozen_string_literal: true

class Research::SeminarsController < ApplicationController
  helper_method :get_meeting_examples, :coming, :meetings, :following, :past,
                :meeting, :events

  include Termwise
  objectify

  def weekly
    @from = nil
    @default = false
    begin
      if params[:commit]
        if params[:commit].try(:downcase) == t('research.weekly!last')
          params[:dates][:from] = (Date.current - 1.week).beginning_of_week
        end
        if params[:commit].downcase == t('research.weekly!next')
          params[:dates][:from] = (Date.current + 1.week).beginning_of_week
        end
      end
      @from = from_date
      if to_date
        @to = to_date
        @from ||= @to - 1.week
      elsif @from
        @to = @from + 1.week
      else
        @from = CommonUtil.week_start
        @to = CommonUtil.week_end
        @default = true
      end
      @params = params.permit(:dates)
    rescue ArgumentError, NoMethodError => e
      redirect_to({ action: 'weekly', dates: nil }, flash: { alert: [e, :en] })
      return
    end
    events = Event.between(@from, @to)
    meetings = Meeting.between(@from, @to)
    @days = {}
    events.each do |ee|
      next if ee.cancelled?

      dd = ee.starts.to_date
      @days[dd] ||= []
      @days[dd].push(ee.decorate)
    end
    meetings.each do |m|
      next if m.cancelled?

      dd = m.date
      @days[dd] ||= []
      @days[dd].push(m.decorate)
    end
    respond do
      render_format([events, @colloq, @regular].map do |l|
        l&.map { |x| x.to_h(current_user) } || []
      end)
    end
  end

  def create_ml
    object.create_ml_email.deliver_later
    render json: object
  end

  protected

  def h1(hh = get_title)
    if action_name == 'weekly' && !@default
      super(tt('research.ev_range', range: date_range(@from, @to)))
    else
      super
    end
  end

  def events
    @events ||=
      [EventDecorator, *Seminar.all.decorate].map(&:to_fullcal).select do |x|
        x[:events].present?
      end
  end

  def cur
    @cur ||= bare_object.meetings
  end

  def coming
    @coming ||= cur.this_week.decorate.reject(&:cancelled?)
  end

  def meetings
    @meetings ||= cur.decorate
  end

  def following
    @following ||= cur.from_time.decorate
  end

  def past
    @past ||= cur.before.decorate
  end

  def meeting
    @meeting = cur.new.decorate
    @meeting.date = bare_object.default_date
    @meeting
  end

  def dup_object
    return unless params[:dup_obj]

    (term_id ? term.object.seminars : Seminar).find(params[:dup_obj])
  end

  def from_date
    params[:dates].try(:[], :from).try(:to_date).try(:beginning_of_day)
  end

  def to_date
    params[:dates].try(:[], :to).try(:to_date).try(:beginning_of_day)
  end

  def inertia_index_path
    'Research/Seminars/Index'
  end

  def inertia_show_props
    super.vdeep_merge(
      meeting: -> { policy(meeting)&.new? ? meeting.inertia_json : nil },
    )
  end
end

