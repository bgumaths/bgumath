# frozen_string_literal: true

class EditView < BaseView
  def has_object?
    true
  end
end

