# frozen_string_literal: true

class AcYearsController < ApplicationController
  helper_method :shnaton_years
  objectify

  def new
    super
    object.year = AcYear.next_avail
  end

  protected

  def shnaton_years
    @shnaton_years ||= policy_scope(collection).all.decorate.select(&:shnaton)
  end
end

