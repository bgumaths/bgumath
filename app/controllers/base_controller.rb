# frozen_string_literal: true

require 'errors'

class BaseController < ActionController::Base
  include ActiveStorage::SetCurrent
  include Pundit::Authorization
  # remove effects of Graphiti::Rails when not in API
  def debug_graphiti
    yield
  end

  def wrap_graphiti_context
    yield
  end

  include CommonHelper

  # How locales work: The main locale for the page is determined by the
  # request. However, some pages are only available in
  # a particular locale. In this case we still display the navbars in the 
  # requested locale, but the page content is displayed in its available 
  # locale, which we call the *inner locale*.
  #
  # The inner locale is determined in {BaseView#locales} as follows: First, 
  # we determine the list of allowed locales for that page. This is done in 
  # {BaseView#allowed_locales}: On pages that are associated to an object 
  # (e.g., show), this is given by its decorator's method 
  # {BaseDecorator#allowed_locales} (and usually deduces from the presence 
  # of a field in the given locale). For pages not associated with an object 
  # (such as index), the decorator's class method 
  # {BaseDecorator::allowed_locales} is used. Once the list of allowed 
  # locales is determined, the inner locale is the requested locale if it is 
  # in the list, and otherwise it is the first allowed locale.
  #
  # The inner and outer locales are returned by {#inner_locale} and 
  # {#outer_locale}, respectively. The default locale (I18n.locale) is set 
  # to the inner locale by {#set_locale}, via an around_action. There is a 
  # helper method {CommonHelper#in_locale} to perform a block in a given 
  # locale.  This is useful since certain utilities (such as translations) 
  # use the value of I18n.locale as the default locale.  
  # {CommonHelper#in_outer_locale} is a shortcut that runs a block in the 
  # outer locale. The url helpers in the decorator classes (such as 
  # {BaseDecorator#show_path}) run in such a block, so as to create links 
  # that request the outer locale.
  around_action :set_locale

  helper_method :model, :model_name, :model_param, :cache_key, :cacheable?,
                :object_cache_key, :page_cache_key, :perm_params, :title_opts,
                :index_path, :to_schema, :get_title, :policy_model,
                :current_url, :cur_url, :object, :objects, :form_object,
                :inner_locale, :outer_locale, :view, :set_model, :legacy

  # this is required, since rails memoizes the result of this method, while
  # we may change the locale in the process. Must be public!
  # We also use it to set the locale to the outer locale, so that links and 
  # forms generate correct urls
  def url_options
    super.merge(default_url_options, locale: outer_locale)
  end

  def object
    @object ||= bare_object.decorate
  rescue NoMethodError
    bare_object
  end

  def objects
    @objects ||= bare_objects.decorate
  end

  # delegated methods are public
  delegate :model, :decorator, :model_name, :base_model_name, :set_model,
           :policy_model, :mailer, to: :class

  protected

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  class << self
    ### The model
    def model
      @model ||=
        begin
          controller_path.classify.demodulize.constantize
        rescue NameError
          nil
        end
    end

    def decorator
      @decorator ||= model.try(:decorator_class)
    end

    def model_name
      @model_name ||= model ? model.model_name.name : ''
    end

    # model for determining the policy, overide in controllers for STI
    def policy_model
      model
    end

    # overriden for STI to return the base model
    def base_model_name
      model_name
    end

    def mailer
      "#{name.delete_suffix('Controller').singularize}Mailer".constantize
    end
  end

  ### urls
  def path_opts(...)
    {}
  end

  def index_path(*args, **opts)
    decorator&.index_path(*args, **path_opts(:index), **opts) ||
      url_for(action: :index)
  end

  def index_url(*args, **opts)
    decorator&.index_url(*args, **path_opts(:index), **opts) ||
      url_for(action: :index, only_path: false)
  end

  def show_path
    object&.show_path || url_for(action: :show) || root_path
  end

  def current_url
    request.original_url
  end

  def cur_url(...)
    url_for(only_path: true, **same_page(...))
  end
    
  ### The object
  def self.obj_var
    model&.model_name&.singular
  end

  def self.obj_vars
    model&.model_name&.plural
  end

  delegate :obj_var, :obj_vars, to: :class

  def self.objectify
    helper_method obj_var, obj_vars
    
    alias_method obj_var, :object
    alias_method obj_vars, :objects
  end

  # override this in scoped classes, e.g., meeting
  def collection
    model
  end

  # object to duplicate
  def dup_object
    nil
  end

  # the default admins for a new user
  def default_admins
    current_user&.is_corporeal? ? [current_user.id] : []
  end

  # decent_exposure
  def expose_build(param = perm_params(nil), scope = collection)
    if (dup_obj = dup_object)
      dup_obj.am_dup(param)
    else
      # Do this in two steps, since the collection could be something like a
      # term.courses, and then the new object res will have res.term == term,
      # even if we set term_id in param
      res = scope.new
      begin
        res.assign_attributes(param) if param.is_a?(Hash)
      rescue ActiveRecord::AssociationTypeMismatch
        # because of intermediate file upload, we might get mismatched fields 
        # here, just ignore it and go on ... (but maybe TODO)
      end
      
      res.default_admins ||= default_admins if res.respond_to?(:default_admins=)
      res
    end
  end

  def expose_id
    params["#{model_param}_id"].presence || params[:id].presence
  end

  def expose_find(id, scope)
    scope.find(id)
  end

  def expose_scope
    policy_scope(collection)
  rescue Pundit::NotDefinedError
    nil
  end

  def expose_collection
    collection
  end

  def expose_model
    model
  end

  def expose_build_params
    perm_params(nil)
  end

  expose :bare_object,
         scope: -> { expose_collection },
         model: -> { expose_model },
         build_params: -> { expose_build_params },
         id: -> { expose_id },
         find: ->(id, scope) { expose_find(id, scope) },
         build: -> { expose_build }

  # new object for modal forms
  def form_object(...)
    expose_build(...)&.decorate
  end

  expose :bare_objects, -> { expose_scope }

  # was the object just created?
  def created?
    bare_object.persisted?
  end

  # was the object just updated?
  def updated?
    bare_object.previous_changes.present?
  end

  ### locale
  def default_url_options
    { locale: I18n.locale }
  end

  def inner_locale(loc = outer_locale)
    view&.locale(loc) || loc
    #locales.include?(loc.to_sym) ? loc : locales.first
  end

  # the requested locale
  def outer_locale
    ploc = (params[:lang] || params[:locale] || '').to_sym
    (I18n.available_locales.include?(ploc) && ploc) || 
      http_accept_language.compatible_language_from(I18n.available_locales) || 
      I18n.available_locales.first
  end

  def set_locale(&)
    in_locale(inner_locale, &)
  end

  ### formats
  def self.formats
    @formats ||=
      ActionController::Renderers::RENDERERS.select { |x| Mime[x].present? }
    #.select do |x|
    #  (model || Base).method_defined? "to_#{x}"
    #end
  end

  delegate :formats, to: :class

  def render_format(what, **)
    render(request.format.to_sym => what, **)
  end

  ### params
  def self.model_param
    @model_param ||= model ? model.model_name.param_key.to_sym : nil
  end

  delegate :model_param, to: :class

  def model_params
    params[model_param]
  end

  # strong parameters using pundit. We allow optional params hash so that we
  # can duplicate for new
  def perm_params(obj = bare_object, strict = nil, req = nil)
    return {} unless model

    strict = %w[update create].include?(action_name) if strict.nil?
    req ||= strict ? params.require(model_param) : params[model_param]
    obj ||= policy_model.new
    (req.try(:permit, policy(obj).permitted_attributes) || {}).to_h
  rescue ActionController::ParameterMissing
    nil
  end

  def add_password
    return if model_params[:password].present?

    model_params[:password_confirmation] =
      model_params[:'password-confirmation'] =
        model_params[:password] = SecureRandom.urlsafe_base64(8)
  end

  ### authorization
  include CurrentUser

  def auth
    rec = view.policy_thing
    if rec
      authorize(rec)
    else
      skip_authorization
    end
  end

  # pundit authorisation failure exception handling
  def non_authorized_message(exc)
    pol = exc.policy
    return exc.message unless pol

    policy_name = pol.class.to_s.underscore
    rec = exc.record
    user = pol.try(:user) || I18n.t('pundit.unauthorized_user')
    if rec
      defrect = begin
        tt(rec.class)
      rescue I18n::MissingInterpolationArgument
        tt(:object, default: 'object')
      end
    end
    rect = begin
      I18n.t(rec, scope: %i[pundit rec_names], default: defrect)
    rescue StandardError
      defrect
    end
    trans = rec.try(:to_trans) || rec.try(:decorator_class).try(:to_trans) || {}
    rec = rec.decorate if rec.respond_to?(:decorate) && !rec.is_a?(Class)
    action = exc.query.to_s.delete('?')
    I18n.t exc.query, action:, user:, policy: policy_name.humanize,
                      record: rec, recordt: rect,
                      scope: ['pundit', policy_name],
                      default: :default, cascade: true, **trans
  end

  def non_authorized_redirect
    # prevent redirection loop
    if request.referer &&
       (URI(request.referer).path.gsub(%r{/*$}, '') !=
        request.path.gsub(%r{/*$}, ''))
      request.referer
    else
      root_path
    end
  end
  
  def inertia_request?
    !!request.headers['X-Inertia']
  end

  def user_not_authorized(exc)
    msg = non_authorized_message(exc)
    logger.info(msg)
    respond_to do |format|
      format.text do
        headers['X-validation-errors'] = msg
        render partial: 'flash', locals: {type: :alert, message: msg},
               formats: :html, status: :unprocessable_entity
      end
      format.html do
        if inertia_request?
          redirect_to non_authorized_redirect,
                      inertia: { errors: { base: msg } }
        else
          flash[:alert] = msg
          redirect_to non_authorized_redirect
        end
      end
      format.any(*formats) do
        head :unauthorized
        return
      end
    end
  end

  def record_not_found(exc)
    respond_to do |format|
      format.html do
        safe_redir_back alert: exc.message
      end
    end
  end

  ### cache
  # define it here and not in application_controller so that static pages
  # also use it
  def cache_key
    meth = "cache_key_#{action_name}"
    @cache_key ||= respond_to?(meth) ? public_send(meth) : nil
  end

  def cacheable?
    cache_key
  end

  def page_cache_key
    [locale, pundit_user.try(:id), params.permit!]
  end

  def object_cache_key(obj)
    page_cache_key.push(obj.try(:cache_key))
  end

  def cache_key_show(obj = object)
    object_cache_key(obj)
  end

  def cache_key_index
    page_cache_key.push(objects)
  end

  ### misc
  # set the admins of obj to be the given user_ids (using rolify)
  def set_admins(obj = bare_object,
                 user_ids = if params[model_param]
                              params[model_param][:admins]
                            end,
                 role = :Admin)
    return unless obj.persisted?
    return if user_ids.blank?

    # obj.admins is _not_ the right thing here, we need the actual persisted 
    # role assignments
    prev = (obj.admin_users(role) || []).map { |x| x.id.to_s }
    (prev - user_ids)&.each do |id|
      user = User.find_by(id:)
      begin
        user&.remove_role role.to_s, obj, modified_by: current_user
      rescue NoMethodError
        true
        # strange bug in rolify
        # (https://github.com/RolifyCommunity/rolify/issues/190), seems to
        # remove the role
      end
    end

    (user_ids - prev)&.each do |id|
      user = User.find_by(id:)
      user&.add_role role.to_s, obj, modified_by: current_user
    end
  end

  # turn a list param that was passed as a csv string instead into a list.
  # Useful for select fields that can be edited with x-editable.
  def fix_list_param(param)
    return unless params[model_param]

    return unless params[model_param][param].is_a?(String)

    params[model_param][param] = params[model_param][param].split(/\s*,\s*/)
    
  end

  def to_schema
    object.try(:to_schema)
  rescue NameError
    nil
  end

  def title_opts
    obj = view.thing
    return {} unless obj

    res = { object: obj }
    res[model_param] = obj.to_s if model_param
    res
  end

  def get_title(**opts)
    format = opts.delete(:format) || 'html'
    opts = title_opts.merge(opts)
    obj = opts.delete(:object)
    # first try an exceptional title
    eid = opts[:eid] || (controller_path.tr('/', '.') + "##{action_name}")
    id = opts.delete(:id) || "actions.#{action_name}"
    res = tt(eid.to_sym, obj, default: id.to_sym, **opts)
    markdown(res, inline: true, format:)
  end

  def safe_redir_back(**opts)
    opts.each do |k, v|
      logger.info("#{k}: #{v}")
    end
    # don't redirect back to the same url, to avoid loops
    if request.referer == request.original_url
      redirect_to root_path, flash: opts
    else
      redirect_back(fallback_location: root_path, flash: opts)
    end
  end

  def view_class
    "#{controller_path}/#{action_name}_view".classify.safe_constantize ||
      "#{action_name}_view".classify.safe_constantize || ShowView
  end

  def view
    @view ||= view_class&.new(self)
  end

  def extrajs(str = nil)
    @_extrajs ||= ''.html_safe
    @_extrajs += str.html_safe if str
    @_extrajs
  end

  def extracss(str = nil)
    @_extracss ||= ''.html_safe
    @_extracss += str.html_safe if str
    @_extracss
  end

  attr_reader :legacy

  # https://pragmaticstudio.com/tutorials/rails-session-cookies-for-api-authentication
  def set_csrf_cookie
    val = form_authenticity_token
    #cookies['CSRF-TOKEN'] = {value: val, secure: true}
    cookies['XSRF-TOKEN'] = {
      value: val,
      #domain: request.host,
      secure: Rails.env.production?,
    }
  end
  
  inertia_share settings: -> { AppSetup._instance.decorate.inertia_json }
  inertia_share auth: -> {
    user = current_session_user&.decorate
    res = {
      policy: {
        internal: policy(:internal).show?,
        admin: policy('settings').show?,
      },
    }
    return res unless user

    res.deep_merge(
      user: user.inertia_json,
      seminars: user.menu_items(:seminars).reject { |x| x == '|' },
      courses: user.menu_items(:courses).reject { |x| x == '|' },
      groups: user.try(:research_groups)&.decorate&.map(&:as_menu_item),
    )
  }

  def inertia_form_fields(mod, **fields)
    fields = fields.presence || mod.decorator_class.inertia_form_params
    policy_scope(mod).decorate.as_json(fields)
  end

  inertia_share i18n: -> {
    {
      locale: outer_locale,
    }
  }

  inertia_share csrf: -> { form_authenticity_token }

  inertia_share head: -> {
    {
      title: get_title(locale: inner_locale, format: :kramdown),
    }
  }

  # in termed situations (e.g., seminar list), inertia_term will be the 
  # currently selected term, while inertia_current_term is always the term 
  # for today
  def inertia_term
    inertia_current_term
  end

  def inertia_current_term
    Term.default_no_exams
  end

  inertia_share current_term:
    -> { inertia_current_term&.decorate&.inertia_json }
end

