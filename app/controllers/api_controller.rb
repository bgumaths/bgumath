# frozen_string_literal: true

require 'rescue_registry'
require 'errors'

class ApiController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods
  include Graphiti::Rails::Responders
  include ActionController::Cookies
  include ActionController::RequestForgeryProtection
  include Pundit::Authorization
  include TimeTravel if Rails.env.development?

  prepend_before_action :set_csrf_cookie
  # authorization for persisting is in application_resource
  before_action :auth, only: [:show]

  prepend_around_action :time_travel if Rails.env.development?

  after_action :notify_current_user

  # only numeric status is allowed
  register_exception Pundit::NotAuthorizedError,
                     status: 403,
                     message: true,
                     handler: Graphiti::Rails::ExceptionHandler,
                     detail: :exception
  register_exception Errors::UnwritableAttribute,
                     status: 403,
                     message: true,
                     handler: Graphiti::Rails::ExceptionHandler,
                     detail: :exception
  register_exception Errors::UnpermittedParameters,
                     status: 403,
                     message: true,
                     detail: :exception
  register_exception Graphiti::Errors::UnknownAttribute,
                     status: 422,
                     message: true,
                     handler: Graphiti::Rails::ExceptionHandler,
                     detail: :exception
  register_exception Graphiti::Errors::TypecastFailed,
                     status: 422,
                     message: true,
                     handler: Graphiti::Rails::ExceptionHandler,
                     detail: :exception
  register_exception Graphiti::Errors::InvalidInclude,
                     status: 400,
                     message: true,
                     handler: Graphiti::Rails::ExceptionHandler,
                     detail: :exception
  rescue_from Errors::InvalidAuthorizationToken, with: :respond_unauth

  def index
    authorize(model)
    respond_with(resources)
  end

  def show
    respond_with(resource)
  end

  def create
    if resource.save
      render jsonapi: resource, status: :created
    else
      render jsonapi_errors: resource
    end
  end

  def update
    if resource.update_attributes
      render jsonapi: resource
    else
      render jsonapi_errors: resource
    end
  end

  def destroy
    if resource.destroy
      render jsonapi: { meta: {} }, status: :ok
    else
      render jsonapi_errors: resource
    end
  end

  #####

  include CurrentUser

  def current_user(kls = User)
    authenticate_with_http_token do |token|
      return nil if token.blank?

      uu = kls.find_by_api_key(token)
      raise Errors::InvalidAuthorizationToken unless uu

      logger.debug("  http token user: '#{uu.try(:login)}'")
      uu
    end || current_session_user(kls)
  end

  def allow_graphiti_debug_json?
    Rails.env.local?
  end

  def show_detailed_exceptions?
    allow_graphiti_debug_json?
  end

  def pundit_user
    current_user
  end

  protected

  def model_name
    @model_name ||= request.path.split(%r{/}).select(&:present?)[2]
                           .gsub(/\..*/, '').classify
  end

  def model
    @model ||= model_name.constantize
  end

  def resource_class
    @resource_class ||= "#{model_name}Resource".constantize
  end
  
  def mailer
    "#{model_name}Mailer".constantize
  end

  def resources
    @resources ||= resource_class.all(params, policy_scope(model))
  end

  def params_id(par = params)
    par['id'] || par['data'].try(:[], 'id') || par['filter'].try(:[], 'id')
  rescue NoMethodError
    nil
  end

  def resource
    @resource ||= params_id ? find_resource : build_resource
  end

  def find_resource
    @find_resource ||= resource_class.find(params)
  end
  
  def build_resource
    @build_resource ||= resource_class.build(params)
  end

  def respond_unauth(exc)
    request_http_token_authentication(
      'bgumath', { errors: [{ status: '401', title: exc.message }]}.to_json
    )
  end

  def auth
    authorize(resource_data) if resource_data.present?
  rescue Graphiti::Errors::InvalidRequest => e
    if e.errors.details.any? do |_k, v| 
         v.any? do |it|
           it[:error] == :unwritable_attribute
         end
       end
      raise Errors::UnwritableAttribute, e.errors
    else
      raise
    end
  end

  # allow getting data without actual resource in subclasses
  def resource_data
    resource.try(:data)
  end

  # https://pragmaticstudio.com/tutorials/rails-session-cookies-for-api-authentication
  def set_csrf_cookie
    val = form_authenticity_token
    #cookies['CSRF-TOKEN'] = {value: val, secure: true}
    cookies['CSRF-TOKEN'] = {
      value: val,
      #domain: request.host,
      secure: Rails.env.production?,
    }
  end
  
  def notify_current_user(types = [User, IssuesUser], **opts)
    opts = { domain: request.host, **opts }
    types.each do |type|
      key = "#{type.name.underscore}_id"
      usession = current_user_session(type)
      if usession
        cookies.permanent[key] = usession.user_cookie.merge(opts)
      else
        cookies.delete key, **opts
      end
    end
  end
end

