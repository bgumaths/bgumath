# frozen_string_literal: true

class IpagesController < BaseController
  include Current

  def home
    render inertia: 'Home', props: {
      courses: current_courses,
      events: current_events,
      meetings: current_meetings,
      heads: roles(Role.head),
      admins: roles(Role.staff),
    }
  end

  def center
    render inertia: 'research/center', props: {
      events: center_events,
      visitors: center_visitors,
    }
  end

  protected

  def current_courses
    course_list.as_json(CourseDecorator.inertia_index_params)
  end

  def current_events
    event_list.as_json(EventDecorator.inertia_index_params)
  end

  def meeting_list
    Meeting.between(CommonUtil.week_start, CommonUtil.week_end).decorate
  end

  def current_meetings
    meeting_list.as_json(MeetingDecorator.inertia_params)
  end

  def roles(base)
    # don't add to RoleDecorator, to avoid endless recursion
    base.decorate.as_json(
      RoleDecorator.inertia_index_params.vdeep_merge(
        include: {
          users: UserDecorator.inertia_index_params,
        },
      ),
    )
  end

  def center_visitors
    params = VisitorDecorator.inertia_params
    (2013..Time.zone.today.year).map do |yy|
      Visitor.year(yy).where(centre: true).map do |vv|
        vv.decorate.as_json(params)
      end
    end
  end

  def center_events
    params = EventDecorator.inertia_index_params
    (2013..Time.zone.today.year).map do |yy|
      Event.year(yy).where(centre: true).map do |vv|
        vv.decorate.as_json(params)
      end
    end
  end

  def bare_object
    nil
  end
end

