# frozen_string_literal: true

ML_STATUSES = %i[_active _retired].freeze

class PeopleController < ApplicationController
  after_action :generate_mls, only: %i[create update destroy]
  before_action :update_webids, only: %i[create update]
  helper_method :form_roles, :research_groups, :courses, :roles

  def index
    inertia_index
  end

  def show
    inertia_show
  end

  def edit
    inertia_edit
  end

  def retire
    bare_object.finish = Time.zone.today - 1.day
    render json: bare_object.save ? {} : { status: :unprocessable_entity }
  end

  def generate_api_key
    if (@key = current_user&.create_api_key)
      render
    else
      safe_redir_back alert: current_user&.errors&.full_messages&.to_sentence
    end
  end

  def hours
    # if office hours are modifiable, we list all possible teachers, not only
    # those who have office hours, so that this can be updated
    @teachers ||= policy_scope(
      policy(User).edit? ? User.teaching : User.teachers,
    ).decorate
    @reset_date = if Term.default.respond_to?(:starts)
                    Term.default.starts - 3.months
                  end
    respond { render_format(@teachers.map { |x| x.to_h(current_user) }) }
  end

  def reset_hours
    date = Date.strptime(params['reset_hours'][:since],
                         I18n.t(:'datepicker.dformat'))
    if date
      teach = User.teaching.decorate.reject { |x| x.hours_modified_since(date) }
      teach.each do |tt|
        tt.hours = nil
        tt.save!
      end
      flash[:notice] = "Successfully reset hours for #{teach.count} people"
    else
      flash[:alert] =
        "Failed to parse date '#{params['reset_hours'][:since]}'"
    end
    redirect_to action: :hours
  end

  protected

  def update_webids
    object&.update_webids
  rescue RestClient::Exceptions::OpenTimeout => e
    logger.debug("Failed to update web ids: #{e}")
    nil
  end

  def self.generate_mls(lists = [Member, Phd, Msc, Postdoc])
    lists.each do |x|
      ML_STATUSES.each { |who| x.write_ml(who) }
    end
  end

  delegate :generate_mls, to: :class

  def courses
    @courses ||= policy_scope(bare_object&.courses)&.this_term&.decorate
  rescue NoMethodError
    nil
  end

  def research_groups
    @research_groups ||= policy_scope(bare_object&.research_groups)&.decorate
  rescue NoMethodError
    nil
  end

  def roles
    @roles ||= policy_scope(object.roles)&.decorate
  end

  def form_opts
    super.deep_merge(as: :user, html: { multipart: true })
  end

  # The following doesn't work: say we are updating the status of a user.  
  # Then we try to find them with the new status, and fail.
  #def collection
  #(params[model_param] || {})[:status]&.safe_constantize || super
  #end

  # have the same object names people for all subclasses
  def self.obj_var
    'person'
  end

  def self.obj_vars
    'people'
  end

  objectify

  def saved_params
    p = super
    p.delete(:syspass)
    p.delete(:password)
    p.delete(:password_confirmation)
    p[:status] = model_name
    p
  end

  # abuse the :admins input for general controllers: we use it to select the 
  # roles for this users. This hack should be removed TODO
  def set_admins(user = object, roles = params[model_param].try(:[], :admins))
    return unless roles.respond_to?(:include?)

    form_roles.each do |role|
      if roles.include?(role.id.to_s) || roles.include?(role.id.to_i)
        user.add_role(role.name, modified_by: current_user)
      else
        user.remove_role(role.name, modified_by: current_user)
      end
    end
  end

  def massage_params
    super
    return unless (prm = params[model_param])

    return if prm[:onleave].to_s == '1'

    prm.delete(:leaves_attributes)
    
  end

  def form_object(...)
    res = super
    res.leaves.build if res.leaves.empty?
    res.photos.build if res.photos.empty?
    res
  end

  def obj_render_opts
    super.deep_merge(layout: object.decorate.list_layout, as: 'person')
  end

  def default_attachment_field
    'photos'
  end

  def create_upload(file)
    blob = super
    Photo.create!(image: blob)
  end

  # show only active users by default
  #def expose_scope
  #super.active
  #end

  ### inertia
  
  # roles that can be updated through the user form
  def form_roles
    @form_roles ||= policy_scope(Role).global
  end

  def inertia_form_info
    super.deep_merge(
      roles: form_roles.as_json(only: %i[id title_i18n description_i18n]),
      research_groups: inertia_form_fields(ResearchGroup),
      postdocs: inertia_form_fields(Postdoc.active),
      students: inertia_form_fields(User.students),
      supervisors: inertia_form_fields(User.can_supervise),
      hosts: inertia_form_fields(User.can_host),
    )
  end

  def inertia_key
    'user'
  end
end

