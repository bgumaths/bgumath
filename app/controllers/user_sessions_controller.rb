# frozen_string_literal: true

class UserSessionsController < BaseSessionsController
  def create
    if object.save
      session_reset
      user = object.user.decorate
      flash[:notice] = I18n.t 'session.loggedin',
                              user:, last_login_at: l(user.last_login_at, format: :long),
                              last_login_ip: user.last_login_ip
    else
      flash[:alert] = I18n.t 'session.invalid',
                             message: object.errors.full_messages.to_sentence
    end
    redirect_back(fallback_location: root_path, inertia: { errors: object.errors })
  end

  def destroy
    current_user_session&.destroy
    redirect_back(fallback_location: root_path)
  end

  protected

  def expose_build(param = perm_params(nil), scope = collection)
    scope.new(param)
  end
end

