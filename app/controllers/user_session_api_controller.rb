# frozen_string_literal: true

class UserSessionApiController < ApiController
  skip_before_action :auth

  def update
    create
  end

  protected

  def resource(_par = params)
    @resource ||= if %w[destroy show].include?(action_name)
                    find_resource
                  else
                    build_resource
                  end
  end

  def bare_object
    UserSession.find
  end
end

