# frozen_string_literal: true

class Pages::ShowView < ShowView
  def allowed_locales
    I18n.available_locales.select do |loc|
      I18n.t(params[:id], default: nil, scope: ['pages#show'], locale: loc,
                          cascade: false)
    end.map(&:to_s)
  end
end

