# frozen_string_literal: true

class Pages::LayoutView < BaseView
  def has_object?
    false
  end
end

