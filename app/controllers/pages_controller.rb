# frozen_string_literal: true

class PagesController < BaseController
  include HighVoltage::StaticPage
  # authorize
  before_action :auth, except: %i[layout]
  after_action :verify_authorized, except: %i[layout]
  after_action :verify_policy_scoped, only: :index
  helper_method :object

  def layout
    @minimal_head = true
    html = render_to_string(layout: 'embed', formats: [:html])
    @legacy = true
    render inertia: 'Generic', props: { content: html }
  rescue ActionView::Template::Error, URI::InvalidURIError
    head :bad_request
  end

  def show
    respond_to do |format|
      format.jpg
      format.html do
        @templates = ["#{current_page}.#{I18n.locale}", current_page]
        html = begin
          render_to_string(template: @templates.shift, layout: 'embed',
                           formats: [:html])
        rescue ActionView::MissingTemplate
          retry unless @templates.empty?
        end
        return unless html

        @legacy = true
        render inertia: 'Generic', props: { content: html }
      end
      format.pdf do
        send_file(
          Rails.public_path.join(
            "#{page_path(locale: nil).sub(%r{^/*}, '')}.pdf",
          ), type: :pdf, disposition: 'inline'
        )
      end
    end
  end

  protected

  def get_title(**opts)
    if opts[:eid].blank?
      tt params[:id], scope: 'pages#show', default: nil, **opts
    else
      super
    end
  end

  def bare_object
    current_page
  end

  def commit_data(msg, **opts)
    user = current_user.try(:decorate) or return nil
    { name: user.to_s, email: user.email_address, message: msg }.merge(opts)
  end

  def fullname
    @fullname ||= params[:pageid] # || "#{params[:id]}/#{params[:title].presence || params[:snippet][:title]}"
  end

  alias_method :home_schema, :dept_schema

  def positions_schema
    {
      :@type => 'JobPosting',
      hiringOrganization: dept_schema,
    }
  end

  include ActionView::Helpers::AssetUrlHelper

  def to_schema
    try("#{object.sub(%r{^pages/}, '')}_schema")
  end

  def perm_params(*_args)
    return params.permit(:action, :controller, :host, :port, :id) if model.blank?

    super
  end

  def current_url
    (action_name == 'layout') ? request.referer : super
  end

  def cur_url(**opts)
    return super unless action_name == 'layout'

    orig = URI.parse(current_url || '')
    query = URI.decode_www_form(orig.query || '').to_h.symbolize_keys.deep_merge(opts)
    return orig if query.blank?

    orig.query = URI.encode_www_form(**query)
    orig.to_s
  end
end

