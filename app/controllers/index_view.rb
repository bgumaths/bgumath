# frozen_string_literal: true

class IndexView < BaseView
  def has_object?
    false
  end
end

