# frozen_string_literal: true

BLACKLIST_PARAMS = %w[password password_confirmation].freeze

class ApplicationController < BaseController
  include TimeTravel if Rails.env.development?

  #prepend_before_action :set_csrf_cookie
  # authorize. Do this before {BaseController#set_locale} since the locale 
  # requires the object, which we may not be authorized to view
  prepend_before_action :auth
  after_action :verify_authorized
  after_action :verify_policy_scoped, only: :index

  # alter params before setting objects
  prepend_before_action :massage_params, only: %i[new create update]

  prepend_around_action :time_travel if Rails.env.development?

  # should be first
  prepend_before_action :load_authlogic

  # deal with attachments ajax
  #before_action :update_attachments, only: %i[create update]

  # leave a paper trail
  before_action :set_paper_trail_whodunnit,
                except: %i[index show new edit]

  before_action :destroy_attachments, only: [:destroy]

  # redirect to editing the record if it exists, upon creation
  before_action :redirect_exists, only: %i[create]

  after_action :set_admins, only: %i[create update]
  after_action :set_creator, only: [:create]

  helper_method :form_path, :form_opts, :policy, :permitted?, :show_path,
                :path_opts, :legacy

  #### Default actions ######

  def legacy_index
    respond { render_objects }
  end

  def inertia_index
    render inertia: inertia_index_path, props: inertia_index_props
  end

  alias_method :index, :inertia_index

  def legacy_show
    respond { render_object }
  end

  def inertia_show
    render inertia: inertia_show_path, props: inertia_show_props
  end

  alias_method :show, :inertia_show

  def new
    respond
  end

  def inertia_edit
    render inertia: inertia_edit_path, props: inertia_edit_props
  end

  def legacy_edit
    respond
  end

  alias_method :edit, :inertia_edit

  def legacy_create
    respond_to do |format|
      if bare_object.save
        format.text do
          ajax_prerender
          render json: {
            item: item_html,
            form: form_reset,
          }
        end
        format.html do
          success_redirect
        end
      else
        errors = object.errors.full_messages.to_sentence
        logger.warn(errors)
        format.text do
          headers['X-validation-errors'] = errors
          # if there is a uniqueness validation failure, we add a link to 
          # edit the existing record
          object.errors.details.each do |attr, errs|
            errs.each do |err|
              next unless err[:error] == :taken

              existing = object.model&.class&.find_by(attr => err[:value])
              next if existing.blank?

              errors += (
                ' ' + existing.decorate.try(:action_list, :edit, bare: true)
                .try(:first)) || ''
            end
          end
          render partial: 'flash', locals: {type: :alert, message: errors},
                 formats: :html, status: :unprocessable_entity
        end
        format.html do
          flash[:alert] = [errors, :en]
          render action: :new, params: { model_param => form_params }
        end
      end
    end
  end

  def inertia_create
    if bare_object.save
      redirect_back_or_to index_path
    else
      redirect_back_or_to index_path, inertia: { errors: bare_object.errors }
    end
  end

  alias_method :create, :inertia_create

  def update
    if bare_object.update(perm_params)
      if inertia_request?
        if /edit$/.match?(request.referer)
          redirect_to show_path
        else
          redirect_back_or_to index_path
        end
      else
        success_redirect
      end
    else
      msg = bare_object.errors.full_messages.to_sentence
      if inertia_request?
        redirect_back_or_to index_path,
                            inertia: { errors: bare_object.errors }
      else
        flash[:alert] = [msg, :en]
        render action: :edit, params: { model_param => form_params }
      end
    end
  end

  def gsheets_update
    logger.info('Starting update from gsheets')
    @items = model.from_gsheets!(policy, *gsheets_args, gsheets_opts)
    if @items
      @pchanges = {}
      @items.each do |item|
        di = item.decorate
        logger.info("Working on #{di}")
        newv = item_changes(di)
        @pchanges[item] = newv if newv.present? || item.errors.present?
        logger.info("  changes: #{newv.to_json}")
        logger.info("  errors: #{item.errors.to_json}")
      end

      respond_to do |format|
        format.html
        format.any(*formats) do
          render_format @pchanges, status: :ok
        end
      end
    else
      respond_to do |format|
        msg = I18n.t('errors.messages.google_read', model:)
        format.html do
          safe_redir_back alert: msg
        end
        format.any(*formats) do
          logger.warn(msg)
          render_format msg, status: :unprocessable_entity
        end
      end
    end
  end

  def destroy
    bare_object.destroy
    respond_to do |format|
      format.html do
        
        success_redirect({
          action: :index, status: :see_other, locale: outer_locale,
        }.merge(extra_index_url_options))
      rescue StandardError
        redirect_to root_path
        
      end
      format.any(*formats) { head :no_content }
    end
  end

  def upload
    uploaded = params[model_param][:upload]
    unless uploaded
      safe_redir_back alert: 'No file chosen'
    end
    model.from_csv(uploaded.tempfile)
    safe_redir_back
  end

  protected

  def info_for_paper_trail
    { message: params&.delete(:message) }
  end

  ### authorization ###
  def policy(obj = bare_object || model)
    return nil if obj.blank?

    @policy ||= {}
    @policy[obj] ||= super
  end

  # may current_user edit the given field?
  def permitted?(field, obj = bare_object || model)
    # if obj is blank, we have a subfield (e.g., of trasto field)
    return true if obj.blank?

    pol = begin
      policy(obj)
    rescue Pundit::NotDefinedError, NoMethodError
      # if there is no policy, assume permitted, issues will be 
      # detected when creating/updating
      return true
    end
    pol.writable?(field)
  end

  def xeditable?(eobj = nil)
    p = policy(eobj)
    p ? p.edit? : false
  end

  def extra_index_url_options
    {}
  end

  def form_path
    object.try(:form_path)
  end

  # options for simple_form_for, e.g., { html: { multipart: true } } when the
  # form contains files
  def form_opts
    { builder: DataFormBuilder }
  end

  def gsheets_args
    []
  end

  def gsheets_opts
    {}
  end

  ### helpers for standard actions ###
  def massage_params
    # make slug nil if empty, so that it is created
    return unless params[model_param].respond_to?(:[])

    params[model_param][:slug] = nil if params[model_param].key?(:slug) && params[model_param][:slug].blank?
  end

  def set_creator
    return unless current_user && bare_object.persisted?

    bare_object.reload
    role = current_user.add_role(:creator, bare_object,
                                 modified_by: current_user)
    role.visibility = :auto
    role.save!
  end

  def render_objects(**)
    render_format(objects, **)
  end

  def render_object(**)
    render_format(object, **)
  end

  def success_redirect(where = show_path)
    redirect_to(where, flash: {
                  notice: tt("success.#{action_name}", model),
                })
  end

  def respond
    respond_to do |format|
      format.html do
        html = render_to_string(layout: 'embed', formats: [:html])
        @legacy = true
        render inertia: 'Generic', props: { content: html }
      end
      format.pdf do
        # catch latex errors

        @latex_config = {
          workdir: -> { object.show_path },
        }
        render
      rescue ActionView::Template::Error => e
        safe_redir_back alert: if e.message.include?('latex failed')
                                 'Failed to produce pdf, please try the latex source'
                               else
                                 e.message
                               end
      end
      format.tex do
        @latex_config = {
          workdir: -> { object.show_path },
        }
        st = render_to_string formats: [:tex], layout: 'application'
        send_data st, type: 'text/tex'
      end
      format.any(*formats) do
        render
      rescue ActionView::MissingTemplate
        yield
      end
    end
  end

  def load_authlogic
    Authlogic::Session::Base.controller ||=
      Authlogic::ControllerAdapters::RailsAdapter.new(self)
  end

  # params to pass back to the form in case of errors
  def form_params
    perm_params.reject do |k, _v|
      BLACKLIST_PARAMS.include?(k.to_s)
    end
  end

  # return the changes in the last version of the decorated object item.
  def item_changes(item)
    item.changes
  end

  def ajax_prerender
  end

  def obj_render_opts
    {}
  end

  def default_attachment_field
    'descfiles'
  end

  # intermediate file upload
  def update_attachments
    field = params[:attachment_field] || default_attachment_field
    obj = params[model_param].try(:[], field)
    return if obj.blank? || obj.is_a?(ActionController::Parameters) ||
              obj == ['']

    blobs = [*obj].compact_blank.map { |x| create_upload(x).decorate }
    render json: {
      html: render_to_string(blobs, 
                             locals: { array: obj.is_a?(Array), field: }),
      signed: blobs.map(&:signed_id),
    }
  end

  def create_upload(file)
    ActiveStorage::Blob.create_and_upload!(
      io: file.open, filename: file.original_filename,
      content_type: file.content_type
    )
  end

  def destroy_upload
    return unless (id = params.key?(:signed_id))

    ActiveStorage::Blob.find_signed!(id)&.destroy

    render json: {
      destroyed: id,
    }
  end

  # destroy attachments
  def destroy_attachments
    return unless params.key?(:attachment_id)

    field = params[:attachment_field] || default_attachment_field
    if object.public_send(:"delete_#{field}", params[:attachment_id])
      head :no_content
    else
      render_format object.errors, status: :unprocessable_entity
    end
  end

  # html for a new item returned by ajax

  def item_html_object
    object
  end

  def item_html(prefix = params[:partial_prefix], **opts)
    lookup_context.prefixes.unshift(prefix) if prefix.present?
    @noedit = true
    res = render_to_string(item_html_object, {
      formats: :html, view_source_map: false, **opts,
    }.deep_merge(obj_render_opts))
    lookup_context.prefixes.shift if prefix.present?
    res
  end

  # new values for form fields after creating a new object
  def form_reset
    {}
  end

  def redirect_exists
    # if the object already exists, switch to editing it
    return unless (slug = object.try(
      :normalize_friendly_id, object.try(:to_slug)
    ).presence)

    ex = begin
      expose_scope.find(slug)
    rescue ActiveRecord::RecordNotFound
      nil
    end
    # we could find an object with a different slug, but with historical 
    # slug the one we looked for. In this case, we create a new object as 
    # usual
    return unless ex&.slug == slug && policy(ex)&.edit?

    ex = ex.decorate
    msg = I18n.t 'errors.messages.exists_edit', what: ex
    flash.now[:alert] = msg
    logger.warn(msg)
    respond_to do |format|
      format.text do
        render json: {redirect: ex.edit_path}, status: :conflict
      end
      format.any do
        redirect_to ex.edit_path
      end
    end
    
  end

  def inertia_index_path(**_opts)
    #inertia_path(index_path(locale: nil, **opts).gsub(%r{^/*}, '') + '/index')
    inertia_path
  end

  def inertia_path(path = "#{controller_path}/#{action_name}")
    path.camelize.gsub('::', '/')
  end

  attr_reader :legacy

  def inertia_objects
    objects
  end

  def inertia_object
    object
  end

  def inertia_form_object
    form_object
  end
    
  def inertia_index_props
    {
      records: -> {
        inertia_objects.map do |rec|
          policy(rec).edit? ?  rec.inertia_edit_json : rec.inertia_index_json
        end
      },
      # TODO: only pass formInfo if can create new
      #formInfo: policy&.new? ? inertia_form_info : {},
      formInfo: -> {
        (policy&.new? || policy&.edit?) ? inertia_form_info : nil
      },
      # object for new form
      # TODO: lazy doesn't seem to work
      #object: InertiaRails.lazy(-> { form_object.inertia_json }),
      record: -> { policy&.new? ? inertia_form_object.inertia_json : nil },
    }.freeze
  end

  def inertia_show_props
    {
      record: -> { inertia_object.inertia_json },
    }
  end

  def inertia_show_path
    inertia_path
  end

  def inertia_form_info
    {}
  end

  def inertia_edit_props
    {
      record: -> { inertia_object.inertia_edit_json },
      formInfo: -> { inertia_form_info },
    }
  end

  def inertia_edit_path
    inertia_path
  end
end

