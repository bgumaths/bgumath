# frozen_string_literal: true

class IssuesApiController < ApiController
  register_exception Bgumath::NoIssuesRespondersError,
                     status: 503,
                     handler: Graphiti::Rails::ExceptionHandler,
                     detail: :exception

  before_action :system_health, only: %i[create]

  def current_issues_user
    current_user(IssuesUser)
  end

  def pundit_user
    current_issues_user
  rescue Errors::InvalidAuthorizationToken
    super
  end

  protected

  def mailer
    Teaching::IssueMailer
  end

  # deny creating a new issue if system is unhealthy (no responders)
  def system_health
    return if User.repliers.present?

    mailer.missing_repliers(current_issues_user).deliver_later
    raise Bgumath::NoIssuesRespondersError
  end
end

