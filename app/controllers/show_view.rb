# frozen_string_literal: true

class ShowView < BaseView
  def has_object?
    true
  end
end

