# frozen_string_literal: true

class Admin::CarouselItemsController < ApplicationController
  helper_method :current_objects, :reload_secs, :interval, :resource_types

  include Current

  def carousel
    render layout: 'carousel'
  end

  def policy_model
    (params[model_param] || {})[:type]&.safe_constantize || super
  end

  protected

  def current_objects(_trm = nil)
    @current_objects ||= (
      bare_objects.current.order(order: :asc).order('random()').decorate.all << 
      ResourceCarouselItem.new(
        small: true,
        title: tt('actions.weekly', locale: :en),
        seminars: seminar_list.select do |x|
          x.meetings.between(Time.zone.now, 7.days.from_now).present?
        end,
      ).decorate <<
      ResourceCarouselItem.new(
        courses: term ? course_list(term) : [],
        small: true,
        title: tt(Course, count: 2, locale: :he,
                          type: tt('teaching.generic_course.type.advanced',
                                   count: 2, locale: :he)).html_safe + 
                 ", #{markdown(term&.to_s(:he), inline: true)}".html_safe,
      ).decorate
    ).select(&:has_content?)
  end

  def reload_secs
    future = bare_objects.ends_after
    change = (future.map(&:activates) + future.map(&:expires)).select do |x|
      x.present? && x >= Time.zone.now
    end.min || Time.zone.now
    (change - Time.zone.now).clamp(1.hour, 1.day).to_i
  end

  def interval(item = nil)
    (item&.interval.presence || AppSetup.try(:tv_interval) || 15) * 1000
  end

  def collection
    (params[model_param] || {})[:type]&.safe_constantize || super
  end

  def default_attachment_field
    'image'
  end

  def create_upload(file)
    blob = super
    Photo.create!(image: blob)
  end

  def self.resource_types
    ResourceCarouselItem.resource_types
  end

  delegate :resource_types, to: :class

  resource_types.each do |typ|
    helper_method "#{typ.name.underscore}_list"
  end

  def seminar_list(term = Term.default)
    term.seminars.decorate.all
  end

  def course_list(term = Term.default)
    term.courses.advanced.decorate.all
  end

  def event_list
    Event.decorate.all
  end

  def role_list
    Role.global.where(visibility: [3, 4, 5]).decorate.all
  end

  def role_users(role)
    @role_users ||= {}
    @role_users[role] ||= User.active.with_rol(role).decorate
  end
end

