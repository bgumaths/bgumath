# frozen_string_literal: true

class Admin::JobsController < ApplicationController
  objectify

  def index
    @qs = {}
    objects.order(id: :desc).each do |job|
      @qs[job.queue] ||= []
      @qs[job.queue] << job
    end
  end

  def destroy
  end
end

