# frozen_string_literal: true

class Admin::VersionsController < ApplicationController
  def self.model
    PaperTrail::Version
  end

  objectify

  def destroy
    orig = object.undone_at.blank? ? object.reify : nil
    respond_to do |format|
      if orig&.save && object.update(undone_at: orig.updated_at)
        format.html do
          safe_redir_back(
            notice: ['Action undone', :en],
          )
        end
        format.any(*formats) do
          render_object status: :ok, location: object
        end
      else
        format.html do
          flash[:alert] = if orig
                            [(orig.errors.full_messages +
                                object.errors.full_messages).to_sentence, :en]
                          elsif object.undone_at
                            ["Change already undone on #{object.undone_at}", :en]
                          else
                            ['Failed to retrieve original object', :en]
                          end
          redirect_to object.decorate.show_path
        end
        format.any(*formats) do
          render_format object.errors, status: :unprocessable_entity
        end
      end
    end
  end
end

