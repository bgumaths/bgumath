# frozen_string_literal: true

class Admin::MirController < ApplicationController
  attr_accessor :generic, :courses, :tcourses, :ocourses, 
                :depts, :responders, :term

  helper_method :generic, :courses, :tcourses, :ocourses, 
                :depts, :responders, :term

  def index
    fmt = ->(gc) {
      c = gc.decorate
      "- [#{c.with_shid}](#{c.edit_path})"
    }
    @term = Term.default.decorate

    btn = ' <button class="btn btn-primary" type="button" data-toggle="collapse" data-target=".collapsible-%s" aria-expanded="false">+</button>'
    @generic = GenericCourse.mir_health(
      policy_scope(GenericCourse.dept.active.issuable),
      format: fmt,
      examined: btn % 'generic',
    ).html_safe
    @courses = Course.mir_health(
      policy_scope(Course.this_year.issuable),
      format: fmt,
      examined: btn % 'courses',
    ).html_safe
    @tcourses = Course.mir_health(
      policy_scope(Course.this_term.issuable),
      format: fmt,
      examined: btn % 'courses',
    ).html_safe
    @ocourses = Course.mir_health(
      policy_scope(Course.other_term.issuable),
      format: fmt,
      examined: btn % 'courses',
    ).html_safe
    @depts = Department.mir_health(
      policy_scope(Department),
      format: ->(dd) {
        d = dd.decorate
        "- [#{d}](#{teaching_issues_users_path}#!IssuesStaff-tab_#{d.to_id}-tab)"
      },
      examined: btn % 'departments',
    ).html_safe
    @responders = User.repliers.map(&:decorate)
  end

  def self.policy_model
    :mir
  end

  # following is needed because 'mir' is not plural
  def title_opts
    super.merge(eid: 'admin.mir#index')
  end
end

