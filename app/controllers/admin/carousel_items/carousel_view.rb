# frozen_string_literal: true

class Admin::CarouselItems::CarouselView < IndexView
  def has_object?
    false
  end
end

