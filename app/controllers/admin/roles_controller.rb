# frozen_string_literal: true

class Admin::RolesController < ApplicationController
  helper_method :troles, :users
  objectify

  protected

  def troles
    return @troles if @troles

    manual = policy_scope(
      Role.order(Arel.sql("title_i18n->'#{I18n.locale}'")).manual,
    )
    @troles = {
      global: manual.global,
      local: manual.local,
      #auto: Role.order("roles.title_i18n->'#{I18n.locale}'").auto.decorate
    }
  end

  def users(role = object)
    @users ||= {}
    return @users[role] if @users[role]

    @users[role] = User.with_rol(role).decorate
  end
end

