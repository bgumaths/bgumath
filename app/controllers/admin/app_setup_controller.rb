# frozen_string_literal: true

class Admin::AppSetupController < ApplicationController
  protected

  def index_path
    show_path
  end

  def expose_id
    super || true
  end

  def inertia_form_info
    super.vdeep_merge(roles: inertia_form_fields(Role.global.manual))
  end

  def inertia_show_props
    super.vdeep_merge(
      formInfo: -> {
        (policy&.new? || policy&.edit?) ? inertia_form_info : nil
      },
    )
  end

  def expose_find(id, *)
    (id == true) ? AppSetup._instance : super
  end
end

