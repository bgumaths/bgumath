# frozen_string_literal: true

class NewView < BaseView
  def has_object?
    true
  end
end

