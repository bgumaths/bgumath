# frozen_string_literal: true

class SamlIdpController < BaseController
  include SamlIdp::Controller
  protect_from_forgery

  skip_around_action :set_locale
  before_action :validate_saml_request, only: %i[new create logout]

  include CurrentUser
  # the controller redirects to a form if the email is empty, we wish to use
  # the session instead
  before_action :set_email, only: [:create]

  content_security_policy do |p|
    p.script_src :self, :https, :unsafe_inline
  end

  def show
    render xml: SamlIdp.metadata.signed
  end

  def new
    redirect_to root_path, flash: { alert: I18n.t(:must_login) }
  end

  def create
    unless params[:email].blank? && params[:password].blank?
      person = idp_authenticate(params[:email], params[:password])
      if person.present?
        @saml_response = idp_make_saml_response(person)
        render template: 'saml_idp/idp/saml_post', layout: false
        return
      end
    end
    redirect_to root_path, flash: { alert: I18n.t(:must_login) }
  end

  def logout
    idp_logout
    @saml_response = idp_make_saml_response(nil)
    render template: 'saml_idp/idp/saml_post', layout: false
  end
    
  protected

  def set_email
    params[:email] = '_session' if params[:email].blank?
  end

  def idp_authenticate(email, password)
    if email.present? && email != '_session'
      user = User.find_by(email:)
      return user&.authen?(password) ? user : nil
    end
    current_user
  end

  def idp_make_saml_response(found_user)
    encode_response found_user
  end

  def idp_logout
    #decode_request(params[:SAMLRequest])
    #current_user_session.destroy if current_user_session
  end
end

