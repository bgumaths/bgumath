# frozen_string_literal: true

class BaseView
  attr_accessor :controller

  delegate_missing_to :controller

  def initialize(controller, *_args)
    @controller = controller
  end

  def model
    controller.model.try(:decorator_class) || controller.model
  end

  def policy_model
    controller.policy_model.try(:decorator_class) || controller.policy_model
  end

  def locale(loc = I18n.locale)
    allowed_locales.include?(loc.to_s) ? loc : allowed_locales.first
  end

  def thing
    has_object? ? object : model
  end

  def policy_thing
    has_object? ? object : policy_model
  end

  def allowed_locales
    (thing.try(:allowed_locales) || I18n.available_locales).map(&:to_s)
  end
end

