# frozen_string_literal: true

module ShnatonPart
  extend ActiveSupport::Concern
  include Yearwise
  included do
    before_action :ensure_shnaton
    helper_method :shnaton
  end

  def shnaton
    return @shnaton if @shnaton

    ensure_year
    @shnaton = ac_year.shnaton&.decorate&.object
  end

  def ensure_shnaton
    raise ActiveRecord::RecordNotFound, 'No handbook exists' if shnaton.blank?
  end

  def collection
    ensure_shnaton
    shnaton.try(model.model_name.plural)
  end
end

