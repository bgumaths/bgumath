# frozen_string_literal: true

module Current
  extend ActiveSupport::Concern
  included do
    helper_method :term, :role_users
  end

  protected

  def term(trm = nil)
    trm ||= begin
      Date.parse(params[:date])
    rescue StandardError
      11.weeks.from_now
    end
    @term ||= {}
    @term[trm] ||= Term.current(trm)&.decorate
  end

  def seminar_list(trm = term)
    trm&.seminars&.decorate&.all
  end

  def course_list(trm = term)
    trm&.courses&.advanced&.decorate&.all
  end

  def event_list
    Event.end_from.decorate.all
  end

  def role_list
    Role.global.where(visibility: [3, 4, 5]).decorate.all
  end

  def role_users(role)
    @role_users ||= {}
    @role_users[role] ||= User.with_rol(role).decorate
  end
end

