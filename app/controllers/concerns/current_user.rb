# frozen_string_literal: true

# add current_user methods using sessions
module CurrentUser
  extend ActiveSupport::Concern

  included do
    helper_method :current_user_session, :current_user, :current_session_user,
                  :cur_user
    protect_from_forgery
  end

  protected

  def handle_unverified_request
    @failed_csrf = true
  end

  def current_user_session(kls = User)
    # if csrf protection failed, no session
    return nil if @failed_csrf

    @current_user_session ||= {}
    @current_user_session[kls] ||= "#{kls.name}Session".constantize.find
  end

  # the user in the current session. May be different from current_user if
  # used basic auth
  def current_session_user(kls = User)
    @current_session_user ||= {}
    @current_session_user[kls] ||= current_user_session(kls)
                                   .try(kls.model_name.singular)
  end

  # HTTP basic authentication takes presedence over session. We use HTTP
  # basic only for single access, i.e., don't store it in a session
  # Use `try` since we may be in a context where this is not available (e.g., 
  # mailer)
  def current_user(kls = User)
    #logger.debug('Current user:')
    @current_user ||= {}
    @current_user[kls] ||=
      try(:authenticate_with_http_basic) do |u, p|
        uu = kls.find_by_login(u)
        logger.debug("  http basic user: '#{uu.try(:login)}'")
        res = uu&.authen?(p) ? uu : nil
        res
      end || current_session_user(kls)
  end

  def cur_user(kls = User)
    @cur_user ||= {}
    @cur_user[kls] ||= current_user(kls)&.decorate
  end
end

