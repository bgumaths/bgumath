# frozen_string_literal: true

# controllers that require a term
module Termwise
  extend ActiveSupport::Concern
  included do
    helper_method :term
  end

  protected

  def term_id
    return @term_id if @term_id

    glob = params[:term] || params[:term_id]
    # Do not use the term from model_param: if we are updating the term, we
    # will not be able to find the original object
    #@term_id = params[model_param] ? params[model_param][:term_id] || glob :
    #glob
    @term_id = glob

    # transition to correct term names
    @term_id = 'fall2017' if @term_id.to_s == 'fall2018'
    @term_id
  end

  def term
    @term ||= (begin
      Term.find(term_id)
    rescue StandardError
      Term.default_no_exams
    end).try(:decorate)
  end

  def pterm
    @pterm ||= term.prev.try(:decorate)
  end

  def nterm
    @nterm ||= term.next.try(:decorate)
  end

  def ensure_term
    raise ActiveRecord::RecordNotFound, 'No term found' unless term
  end

  def collection
    ensure_term
    (term.try(:object) || term).public_send(model.model_name.plural)
  end

  def path_opts(action = nil)
    term.respond_to?(:slug) ? super.merge(term_id: term.slug) : super
  end

  def gsheets_args
    super.push(term_id)
  end

  def gsheets_opts
    super.merge(term_id: term.id)
  end

  def inertia_terms
    @inertia_terms ||=
      inertia_form_fields(policy_scope(Term).order(starts: :asc)) 
  end

  def inertia_form_info
    super.vdeep_merge(terms: inertia_terms)
  end

  def inertia_index_props
    super.vdeep_merge(
      avail_terms: -> { inertia_terms },
      term: inertia_term.decorate.inertia_json,
    ).freeze
  end

  def inertia_term
    term
  end
end

