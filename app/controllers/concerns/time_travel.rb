# frozen_string_literal: true

module TimeTravel
  extend ActiveSupport::Concern

  protected

  def time_travel
    @travel_to = params[:today] || ENV.fetch('BGUMATH_DATE', nil)
    if @travel_to.present?
      logger.info "Travelling to #{@travel_to}"
      Timecop.travel(@travel_to)
    end
    yield
    Timecop.return
  end
end

