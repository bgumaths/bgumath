# frozen_string_literal: true

# controllers that require a year
module Yearwise
  extend ActiveSupport::Concern
  included do
    helper_method :ac_year, :ac_year_id
  end

  def ac_year_id
    return @ac_year_id if @ac_year_id

    glob = params[:ac_year] || params[:ac_year_id]
    @ac_year_id ||= params[model_param] ? params[model_param][:ac_year_id] || glob : glob
  end

  def default_year
    @default_year ||= AcYear.current
  end

  def ac_year
    @ac_year ||= (begin
      AcYear.find(ac_year_id)
    rescue StandardError
      default_year
    end).try(:decorate)
  end

  def ensure_year
    raise ActiveRecord::RecordNotFound, 'No year found' unless ac_year
  end

  def collection
    ensure_year
    ac_year.object.public_send(model.model_name.plural)
  end
end

