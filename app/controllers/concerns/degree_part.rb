# frozen_string_literal: true

module DegreePart
  extend ActiveSupport::Concern
  include ShnatonPart
  included do
    helper_method :degree, :degree_id, :param_degree
  end

  protected

  def degree_id
    params[:degree] || params[:degree_id]
  end

  def param_degree
    @param_degree ||= shnaton.degrees.find(degree_id)
  end

  def degree
    @degree ||= (object&.degree || param_degree)&.decorate
  end

  def collection
    param_degree.try(model.model_name.plural)
  rescue ActiveRecord::RecordNotFound
    super
  end
end

