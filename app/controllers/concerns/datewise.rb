# frozen_string_literal: true

# events with a date that publish a calendar and a feed
module Datewise
  extend ActiveSupport::Concern

  included do
    helper_method :atom_title, :atom_url, :atom_events, :atom_locale, :atom_author_name, :atom_author_email, 
                  :atom_author_uri
    if Rails.env.production?
      after_action :validate_ical, only: %i[create update delete]
    end
  end

  protected

  def respond
    return super unless action_name == 'index'

    respond_to do |format|
      format.ics do
        cal = to_ical
        cal.publish
        # do not cache ical
        expires_now
        render body: cal.to_ical, content_type: 'text/calendar'
      end
      format.any(:rss, :atom)
      format.any { super }
    end
  end

  def to_ical(events = ical_events || [], title = ical_title, url = ical_url)
    require 'icalendar'
    require 'icalendar/tzinfo'
    cal = Icalendar::Calendar.new
    cal.append_custom_property('x-wr-calname', title) if title
    cal.append_custom_property('x-wr-caldesc', url) if url
    cal.append_custom_property('x-wr-timezone', TZID)
    tz = TZInfo::Timezone.get(TZID)
    tzones = {}
    events.each do |ev|
      eve = ev.decorate.to_ical
      cal.add_event(eve)
      # arg to ical_timezone must be DateTime, not Date
      tzone = tz.ical_timezone Icalendar::Values::DateTime.new(eve.dtstart)
      # keep in a hash, so that we add each timezone only once
      cal.add_timezone(tzone) unless tzones[tzone.to_ical]
      tzones[tzone.to_ical] = tzone
    end
    cal
  end

  def ical_events
    objects
  end

  def ical_title
    nil
  end

  def ical_url
    request.url.gsub(/\.[^.]*$/, '')
  end

  def atom_events
    ical_events.order(created_at: :desc)
  end

  def atom_title
    ical_title
  end

  def atom_url
    ical_url
  end

  def atom_locale
    I18n.locale
  end

  def atom_author_name
    tt(:depname)
  end

  def atom_author_email
    nil
  end

  def atom_author_uri
    nil
  end

  def ics_path(**)
    model.decorator_class.index_path(format: :ics, **)
  end

  def ics_url(**)
    model.decorator_class.index_url(format: :ics, **)
  end

  def ical_valid
    # icalendar.org expects _unencoded_ url. We always use the production
    # version
    resp = RestClient.get(
      "https://icalendar.org/validator.html?url=#{ics_url}&json=1",
    )
    JSON.parse(resp.body)
  rescue Errno::ENETUNREACH => e
    { warnings: [e.to_s] }
  end

  def validate_ical
    return unless Rails.env.production?

    val = ical_valid
    return unless val[:errors].present? || val[:warnings].present?

    addr = User.admin.decorate.email_address(true)
    ApplicationMailer.new.mail(
      to: addr, from: addr, subject: tt(:ical_errors),
      content_type: 'text/plain', body: val.to_json
    )
      
    
  end
end

