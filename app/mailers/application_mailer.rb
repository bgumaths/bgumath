# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  include CommonHelper
  include Pundit::Authorization
  # compat with CurrentUser
  def self.protect_from_forgery
  end
  include CurrentUser
  def self.user_email(user)
    # if the user exists but empty, it is optional, on nil we raise
    return nil if user.blank?

    user.try(:decorate).try(:email_address, true) || user
  end

  def self.admin
    @@admin ||= user_email(User.admin)
  end

  delegate :admin, to: :class

  delegate :user_email, to: :class

  default from: -> { admin }
  layout 'mailer'
  helper :application
  helper_method :outer_locale

  # TODO: the following contains two opts bc of some weirdness in kwargs in 
  # method_missing (called in ActionMailer::Base), starting with ruby3.2.0
  def announce(what, email, oopts = {}, **opts)
    @what = what
    @opts = oopts.deep_merge(opts)
    set_headers
    mail(to: email, **@opts)
  end

  protected

  def outer_locale
    I18n.locale
  end

  def self.object_name
    @object_name ||= name.demodulize.sub(/Mailer$/, '').downcase
  end

  def header_prefix(prefix = self.class.object_name)
    "X-bgumath-#{prefix}"
  end

  def set_headers(**opts)
    opts.each do |k, v|
      headers["#{header_prefix}-#{k.to_s.tr('_', '-')}"] = v if v.present?
    end
    headers[header_prefix('msg-class')] = self.class.object_name
    headers[header_prefix('msg-type')] = action_name
  end

  def default_url_options
    super.merge(locale: nil, protocol: :https)
  end
end

