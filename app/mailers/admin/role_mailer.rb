# frozen_string_literal: true

class Admin::RoleMailer < ApplicationMailer
  def role_changed(ev, **)
    @ev = ev.decorate
    set_headers(name: @ev.role.to_s)
    mail(to: [admin], subject: "Role #{@ev.role} changed", **)
  end
end

