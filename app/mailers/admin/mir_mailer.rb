# frozen_string_literal: true

class Admin::MirMailer < ApplicationMailer
  attr_accessor :generic, :courses, :tcourses, :ocourses,
                :depts, :responders, :term

  helper_method :generic, :courses, :tcourses, :ocourses,
                :depts, :responders, :term

  def health(**)
    fmt = ->(gc) { "- #{gc.decorate.with_shid}" }
    @term = Term.default.decorate
    @generic = GenericCourse.mir_health(format: fmt)
    @courses = Course.mir_health(format: fmt)
    @tcourses = Course.mir_health(Course.this_term.issuable, format: fmt)
    @ocourses = Course.mir_health(Course.other_term.issuable, format: fmt)
    @depts = Department.mir_health(format: ->(d) { "- #{d.decorate}" })
    @responders = User.repliers.map(&:decorate)
    set_headers
    mail(
      to: [*user_email(Role.by_name('Secretary').take)],
      cc: [admin, *user_email(Role.vice), *user_email(User.administrator)],
      subject: I18n.t('admin.mir#index', locale: :en),
      **
    )
  end
end

