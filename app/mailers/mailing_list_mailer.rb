# frozen_string_literal: true

class MailingListMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.mailing_list_mailer.create.subject
 
  # TODO: the following contains two opts bc of some weirdness in kwargs in 
  # method_missing (called in ActionMailer::Base), starting with ruby3.2.0
  def create(list, owner, oopts = {}, **opts)
    @list = list
    @owner = owner
    @opts = oopts.deep_merge(opts)
    to = Mail::Address.new AppSetup.ml_admin
    to.display_name = AppSetup.ml_admin_name
    @to = to
    set_headers
    mail(to: @to.format, cc: owner, bcc: admin, reply_to: owner,
         subject: default_i18n_subject(list:), **@opts)
  end
end

