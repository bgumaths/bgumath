# frozen_string_literal: true

class Teaching::IssuesUserMailer < ApplicationMailer
  before_action :set_locale
  after_action :restore_locale

  def account(user, oopts = {}, **opts)
    opts = oopts.deep_merge(opts)
    @user = user.decorate
    @password = @user.pass_changed? ? nil : @user.password
    @admin = admin
    @to = (opts[:to] || [@user]).map { |x| user_email(x) }
    @cc = (opts[:cc] || [User.admin, Role.vice]).map { |x| user_email(x) }
    set_headers(user: @user)
    @subject = tt("teaching.issue.email_subj.#{action_name}", user: @user)
    mail(to: @to, cc: @cc, subject: @subject, gpg: { sign: true })
  end

  alias_method :reset_password, :account
  alias_method :changed_password, :account

  protected

  def set_locale
    @old_locale = I18n.locale
    I18n.locale = :he
  end

  def restore_locale
    I18n.locale = @old_locale
  end
end

