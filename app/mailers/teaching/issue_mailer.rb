# frozen_string_literal: true

class Teaching::IssueMailer < ApplicationMailer
  before_action :set_locale
  after_action :restore_locale

  def created(issue, oopts = {}, **opts)
    @opts = oopts.deep_merge(opts)
    set_vars(issue)
    @opts = @opts.presence || {
      to: [@reporter] + repliers,
      cc: [@lecturer] + @issues_staffs + [@aguda_rep] + [*Role.vice],
      reply_to: repliers + [*Role.vice],
    }
    generic(**@opts)
  end

  def replied(issue, oopts = {}, **opts)
    opts = oopts.deep_merge(opts)
    set_vars(issue)
    opts = opts.presence || {
      to: ([@lecturer] + @issues_staffs).select(&:present?),
      cc: repliers + [*Role.vice],
      reply_to: [@replier, *Role.vice],
    }
    opts[:to] = opts[:to].presence || [Role.vice]
    generic(**opts)
  end

  def published(issue, oopts = {}, **opts)
    opts = oopts.deep_merge(opts)
    set_vars(issue)
    opts = opts.presence || {
      to: [@reporter] + repliers,
      cc: [@lecturer] + @issues_staffs + [@aguda_rep] + [*Role.vice],
      reply_to: [@replier, *Role.vice],
    }
    generic(**opts)
  end

  def reminder(issue, oopts = {}, **opts)
    opts = oopts.deep_merge(opts)
    set_vars(issue)
    opts = opts.presence || {
      to: [@replier],
      cc: [@lecturer, Role.vice],
    }
    generic(**opts)
  end

  def missing_repliers(student, **_opts)
    @student = student.decorate
    set_headers(error: 'responders-missing')
    mail(to: [admin, user_email(Role.vice),
              user_email(User.administrator || '')],
         subject: tt(action_name, scope: %i[teaching issue email_subj]))
  end

  def missing_lecturer(issue, **_opts)
    set_vars(issue)
    set_headers(error: 'lecturer-missing')
    mail(
      to: [admin, user_email(Role.vice),
           user_email(User.administrator || '')],
      subject: tt(action_name, scope: %i[teaching issue email_subj],
                               course: @course),
    )
  end

  protected

  def set_vars(issue)
    @issue = issue.decorate
    @id = @issue&.slug
    @course = @issue&.course
    @reporter = @issue&.reporter
    @replier = @issue&.replier
    @departments = @course&.departments
    @aguda_rep = @issue&.aguda_rep
    @lecturer = @course&.lecturer
    @issues_staffs = @issue&.staffs
  end

  def generic(**opts)
    @opts = opts
    @opts[:bcc] ||= [admin]
    %i[to cc bcc reply_to].each do |k|
      @opts[k] = ([*@opts[k]] || []).select(&:present?)
                                    .map { |x| user_email(x) }
    end
    @subject = @opts[:subject] ||=
      tt(action_name, scope: %i[teaching issue email_subj],
                      course: @course, issue: @issue,
                      id: @id, shid: @course.shid)
    set_headers(id: @id, course_id: @course.shid)
    mail(**@opts)
  end

  def repliers
    @repliers ||= User.repliers
  end

  def set_locale
    @old_locale = I18n.locale
    I18n.locale = :he
  end

  def restore_locale
    I18n.locale = @old_locale
  end
end

