# frozen_string_literal: true

upd = nil

atom_feed(root_url: atom_url, language: atom_locale) do |feed|
  feed.title(atom_title, type: 'html')
  feed.author do |author|
    author.name(atom_author_name)
    author.email(atom_author_email)
    author.uri(atom_author_uri)
  end
  atom_events.decorate.each do |ev|
    feed.updated(upd = ev.atom_date) if upd.blank?
    feed.entry(ev, url: ev.show_url) do |entry|
      ev.to_atom(entry)
    end
  end
end

