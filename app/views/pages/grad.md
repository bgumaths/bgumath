# Graduate program

-------------------

#### The Department of Mathematics offers an international graduate program taught in English leading to M.Sc. and Ph.D. degrees in Mathematics.

## MSc Program

##### Requirements

Applicants for admission to the M.Sc program are expected to hold a B.Sc 
degree or equivalent and have a mathematical preparation that is comparable 
to a B.Sc degree in mathematics from an Israeli university. In particular, 
applicants should be familiar with the material which is required for the 
four basic graduate classes in Algebra, Analysis, Geometry/Topology and 
Logic/Set Theory. Strong applicants with insufficient preparation may be 
admitted contingent upon the completion of supplementary coursework during 
the first year of the program.

##### Program details

During their studies in the M.Sc. program, students are required to complete 
at least 26 course units of coursework, and write a Master's thesis. The 
course work must include:

* At least two courses from the following list:
  - Advanced Analysis
  - Basic Concepts in Topology and Geometry
  - Noncommutative Algebra or Commutative and Homological Algebra
  - One course in Logic or Set Theory
* At least two advanced seminars.

Course requirements may be waived when equivalent coursework has been 
completed elsewhere.

##### Application

Please apply via the University [application 
procedure](http://in.bgu.ac.il/en/Pages/registration.aspx).

**Financial aid** is available for strong applicants.

## PhD program

##### Requirements

Applicants for admission to the Ph.D program are expected to hold an M.Sc 
degree in Mathematics or equivalent degree with an academic preparation 
comparable to our M.Sc. Program graduates (see below).

##### Program details

Students are prepared for a career in mathematical research or mathematically 
intensive fields. Normal time for completion of the program, including a 
research dissertation, is four years.

##### Applications
Before applying, please contact [the secretary](/people/admins) of the 
Mathematics Department.

**Financial aid** is available for strong applicants.

