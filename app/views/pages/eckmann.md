### In memory of Prof. Beno Eckmann

-----------------------------------

<div class="float-right q-pa-md">
  <img alt="Beno Eckmann" src="/images/eckmann.jpg" width="164" height="250">
</div>

Prof. Beno Eckmann, a prominent mathematician and a close friend of our 
department, passed away in November 2008 in Zurich.

Prof. Eckmann was a distinguished mathematician who also exerted great 
influence on the promotion and development of Mathematics all over the globe. 
Here at Ben Gurion University he initiated the establishment of the [Center 
for Advanced Studies in Mathematics](/research/center), and served actively 
on its advisory committee until his passing away.

Prof. Eckmann studied at ETH, where his PhD advisor was the famous Heinz 
Hopf. His brilliant Ph.D. work won him the Kern Prize and silver medal. He 
was a member of the Institute for Advanced Study in Princeton in 1947, 1951 
and 1952, where he worked with Albert Einstein and John von Neumann.

Prof. Eckmann is considered one of the founding fathers of Homological 
Algebra and Category Theory, with special emphasis on Topology and Cohomology 
of Groups. A close cooperation between Eckmann and Peter Hilton, who was a 
frequent guest at ETH since the 1950's, led to numerous papers (over 25) on 
these subjects. Eckmann had over 60 Ph.D. students, many of whom also became 
prominent mathematicians.

In parallel with his scientific endeavors, Prof. Eckmann contributed 
significantly to the promotion and development of Mathematics, often through 
his ability to convince others of the importance of the field. For example, 
in 1964 he established the famous Institute for Advanced Studies at ETH, 
which he headed for 20 years until his retirement.  Later he interested the 
Springer family, through Julius Springer, to establish the prestigious and 
influential series "Lecture Notes in Mathematics". He was the chief editor of 
this series until near his passing away.

Most notably, it was his vision and help that made it possible to establish 
centers of advanced studies in many universities worldwide. In Israel alone 
we can name the Technion, Bar Ilan University and Ben Gurion University.

He was awarded many honorary degrees, one of them from Ben Gurion University. 
He was president of the Swiss Mathematical Society and secretary of the 
International Mathematical Union for 5 years. He also served as an Honorary 
President of the International Congress of Mathematicians in Zurich in 1994.

He will be greatly missed.

