# frozen_string_literal: true

class BaseMiddleware
  attr_accessor :app

  def initialize(app)
    @app = app
  end

  delegate :call, to: :app
end

