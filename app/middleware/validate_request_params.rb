# frozen_string_literal: true

require './app/middleware/base_middleware'

# http://joshfrankel.me/blog/don-t-let-the-null-bytes-bite/
class ValidateRequestParams < BaseMiddleware
  def call(env)
    request = Rack::Request.new(env)

    return [400, {}, ['Bad Request']] if is_invalid?(request.params)
    return [400, {}, ['Bad Request']] if is_invalid?(request.cookies)

    super
  end

  protected

  INVALID_CHARACTERS = [
    "\u0000" # null bytes
  ].freeze

  INVALID_CHARACTERS_REGEX = Regexp.union(INVALID_CHARACTERS).freeze

  MAX_DEPTH = 30

  def is_invalid?(value, depth = 0)
    return true if depth > MAX_DEPTH

    if value.respond_to?(:match?)
      value.match?(INVALID_CHARACTERS_REGEX)
    elsif value.respond_to?(:values)
      is_invalid?(value.values, depth + 1)
      # `value.respond_to?(:any?)` is not good, since any Enumerable responds 
      # to it
    elsif value.is_a?(Array)
      value.any? { |x| is_invalid?(x, depth + 1) }
    else
      # not clear what to do here, let's allow it
      false
    end
  end
end

