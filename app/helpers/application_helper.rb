# frozen_string_literal: true

require 'memoist'
module ApplicationHelper
  include CommonHelper
  extend Memoist

  def id_path(id, *, **)
    public_send("#{id.parameterize(separator: '_')}_path", *, **,
                locale: I18n.locale)
  end

  # create a link with the text and (optionally) path given by id
  def link_to_id(id, **)
    link_to(
      tt(id, default: nil), id_path(id), dir: loc2dir, id:, **
    )
  end

  # helpers for the header
  def menu_item(ns, x, **opts)
    y = [*x]
    opt = y[-1].respond_to?(:key) ? y.pop : {}
    id_suffix = opts[:id_suffix] || ''
    if x.respond_to?(:pop) && y[0].is_a?(String)
      title = y[0]
      path = y[1]
    else
      id = "#{ns}.#{y[0]}"
      ids = y[0].to_s
      sing = ids.singularize
      if (action = opt.delete(:action)&.to_s)
        taction = (action == 'index') ? 'title' : action
      elsif sing == ids
        action = taction = 'show'
      else
        action = 'index'
        taction = 'title'
      end
      cls = begin
        sing.classify.safe_constantize
      rescue LoadError
        nil
      end || sing.to_sym
      permitted = if opt.key?(:permitted)
                    opt.delete(:permitted)
                  else
                    cls.present? && begin
                      policy(cls).try("#{action}?")
                    rescue Pundit::NotDefinedError
                      true
                    rescue Pundit::NotAuthorizedError
                      false
                    end
                  end

      if permitted
        what = t(sing, scope: %i[activerecord models], count: 1)
        whats = t(sing, scope: %i[activerecord models], count: 42)
        thewhat = t(:the, scope: [:activerecord, :models, sing],
                          cascade: true, count: 1, what:)
        thewhats = t(:the, scope: [:activerecord, :models, sing],
                           cascade: true, count: 42, what: whats)
        title = get_title(eid: id.include?('#') ? id : "#{id}##{action}",
                          id: "actions.#{taction}", object: nil, whats:,
                          thewhats:, what:, thewhat:,
                          title: what)
        path = y[1] || id_path(id)
      else
        title = nil
        path = nil
      end
    end
    return unless title

    link_to(title, path, id: id ? id + id_suffix : id,
                         dir: loc2dir, role: :menuitem, **opts, **opt)
    
  end

  def list_to_s(list, land = true, meth = :to_s, *, **)
    res = ''
    list ||= []
    list = [*list]
    if land && list.length > 1
      what = list[-1].try(:decorate) || list[-1]
      what = what.try(meth, *, **) || what.to_s(*, **)
      res = list_to_s(list[0..-2], false, meth, *, **) +
            I18n.t(:and, what:).html_safe
    else
      res = list.map do |x|
        y = x.try(:decorate) || x
        y.try(meth, *, **) || y.to_s(*, **)
      end.join(', ')
    end
    res.html_safe
  end

  def list_to_l(list, land = true)
    list_to_s(list, land, :linked)
  end

  ### icons ###

  @@icon = {
    submit: 'check-lg',
    hourglass: 'hourglass-split',
    check: 'check',
    times: 'x', #'times',
    treant_tree: 'diagram-2', #'tree',
    sigma_graph: 'bezier', #'bezier-curve',
    delete: 'trash',
    destroy: 'trash',
    undo: 'arrow-counterclockwise', #'undo', # 'repeat',
    edit: 'pencil-square', #'edit', # 'edit',
    home: 'house-door', #'home',
    clone: 'files', #'clone',
    duplicate: 'files', #'clone', #'duplicate',
    success: 'hand-thumbs-up', #'thumbs-up',
    notice: 'hand-index-thumb', #'thumbs-up',
    danger: 'exclamation-circle', #'exclamation', #'exclamation-sign',
    alert: 'exclamation-diamond', #'exclamation', #'exclamation-sign',
    ical: 'calendar-date', #'calendar',
    atom: 'rss', #'list-alt',
    previous: 'skip-backwards-btn', #'chevron-left', # 'menu-left',
    next: 'skip-forward-btn', #'chevron-right', # 'menu-right',
    up: 'arrow-up-circle', #'chevron-up',
    down: 'arrow-down-circle', #'chevron-down',
    index: 'list-ul', #'list',
    flash: 'lightning', #'bolt', # 'flash',
    info: 'info-circle', #'info', #'info-sign',
    education: 'mortarboard', #'graduation-cap', #'education',
    login: 'box-arrow-in-right', #'sign-in-alt', #'log-in',
    logout: 'box-arrow-left', #'sign-out-alt', #'log-out',
    pdf: 'file-pdf',
    star: 'star',
    menu: 'menu-button', #'bars', #'menu-hamburger',
    download: 'download',
    plus: 'plus',
    minus: 'dash', #'minus',
    contact: 'envelope',
    ml: 'envelope-open', #'envelope-open-text',
    file: 'file-text', #'file-alt',
    ban: 'slash-circle', #'ban',
    subreq: 'diagram-3', #'sitemap',
    reset_password: 'key',
    question: 'question-circle',
    online: 'diagram-2', #'network-wired',
    external: 'box-arrow-up-right', #'external-link-alt',
    retire: 'person-x', #'sign-out',
  }

  #@@iconset = 'glyphicon glyphicon-'
  #@@iconset = 'fonticon fas fa-'
  @@iconset = 'bi-'

  def icon_class(id, **opts)
    res = @@iconset + @@icon[id.to_sym]
    res += " #{@@iconset}#{opts[:size]}" if opts[:size].present?
    res
  end

  def glyphicon(id, text = nil, tooltip = nil, **opts)
    text ||= id.to_s.camelize
    tooltip ||= tt("tooltip.#{id}", default: text, cascase: false)
    cls = icon_class(id, **(opts.delete(:icon_opts) || {}))
    nosr = opts.delete(:nosr)
    opts = {
      data: {toggle: :tooltip},
      title: tooltip,
      class: cls,
      aria: {hidden: true},
    }.deep_merge(opts)
    tag.i('', **opts) + (nosr ? '' : tag.span(text, class: 'sr-only'))
  end

  def link_to_glyphicon(id, url, **opts)
    return '' if url.blank?

    gi = opts.delete(:glyphicon_opts) || {}
    link_to glyphicon(
      id, opts.delete(:glyphicon_text), opts.delete(:glyphicon_tooltip), **gi
    ), url.to_s, class: %w[noarrow], **opts
  end

  ACADEMICON = {
    pure: 'https://cris.bgu.ac.il/skin/favIcon',
    scopus: 'scopus',
    gs: 'google-scholar',
    wos: 'publons',
    arxiv: 'arxiv',
    orcid: 'orcid',
  }.freeze

  def academicon(id, text = nil, tooltip = nil, **opts)
    unless @academicons_done
      extracss do
        stylesheet_link_tag(
          "https://cdn.jsdelivr.net/gh/jpswalsh/academicons/css/academicons.#{ASSETMIN}css",
        )
      end
      @academicons_done = true
    end

    iid = ACADEMICON[id]

    text ||= (iid || id).to_s.humanize
    tooltip ||= tt("tooltip.#{id}", default: text, cascase: false)
    opts = {
      data: {toggle: :tooltip},
      title: tooltip,
      aria: {hidden: true},
    }.deep_merge(opts)
    if iid&.include?('/')
      image_tag(iid, alt: text, class: 'webid-icon', **opts)
    else
      cls = iid ? "webid-icon ai ai-#{iid}" : "webid-icon #{icon_class(:question)}"
      tag.span('', class: cls, **opts)
    end +
      tag.span(text, class: 'sr-only')
  end

  def academicon_link(who, id, ...)
    wid = who.webid(id)
    return nil unless wid

    link_to(academicon(id, wid.service_name, ...), wid.id_url.to_s, 
            class: %w[noarrow])
  end

  def academicon_links(who, ...)
    tag.span(class: %w[webid-icons]) do
      User::WEB_IDS.map do |id|
        academicon_link(who, id)
      end.join.html_safe
    end
  end

  ### email encoding ###
  def email_html(addr, text = addr, **)
    mail_to(addr, text, class: :email, **)
  end

  ### buttons ###

  def form_button(obj = form_object, pol = policy(obj), **)
    form_modal(obj, **) if pol.create?
  end

  def dismiss_button(what, content = nil, **opts)
    tag.button(**{
      class: :close, data: { dismiss: what }, aria: { label: :Close }, 
      type: :button,
    }.deep_merge(opts)) do
      if block_given?
        yield
      else
        content ||
          tag.span(glyphicon(:times), aria: { hidden: true })
      end
    end
  end

  def footer_gitem(**, &)
    content_for :buttons_footer do
      tag.li(class: 'nav-item', data: { toggle: :tooltip }, **, &)
    end
  end

  def footer_item(text, target, **opts)
    link = opts.delete(:link) || {}
    cls = link.delete(:class) || []
    cls = [cls] unless cls.is_a?(Array)
    cls += %w[btn nav-link font-weight-bold]
    dark = link.delete(:dark)
    cls += (dark ? %w[btn-dark text-light] : %w[btn-warning text-dark])
    footer_gitem(**opts) do
      link_to text, target, dir: :auto, class: cls, **link
    end
  end

  def button(text, target, **opts)
    tag.div(opts.except(:link)) do
      link_to text, target, class: 'btn btn-primary', **(opts[:link] || {})
    end
  end

  def default_policy(action = params[:action], model = try(:model))
    policy(model ||
           controller_path.classify.demodulize.constantize).try("#{action}?")
  end

  def action_url(action, modl = nil)
    modl.try("#{action}_path", **path_opts(action.to_sym)) ||
      try("#{action}_path") ||
      public_send("#{action}_#{controller_path.tr('/', '_').singularize}_path")
  end

  def action_button(action, text = nil, url = nil, policy = nil, **opts)
    modl = text.respond_to?(:model_name) ? text : try(:model)
    policy = default_policy(action, modl) if policy.nil?
    return unless policy

    modl = modl.try(:decorator_class) || modl
    tip = tt("actions.#{action}_tip", modl, default: nil)
    link_opts = { link: { class: ["action-#{action}"] }.deep_merge(
      tip ? { title: tip } : {},
    ) }
    url ||= action_url(action, modl)
    footer_item(
      if text.is_a?(String)
        tt(
          action, what: text, thewhat: text, scope: %w[actions]
        )
      else
        tt(action, modl, scope: %w[actions])
      end, 
      url, **opts.deep_merge(link_opts)
    )
    
  end

  def new_button(*, **opts)
    opts = { link: { class: 'btn-info', dark: true } }.deep_merge(opts)
    action_button(:new, *, **opts)
  end

  def index_button(...)
    action_button(:index, ...)
  end

  def h1(head = get_title)
    tag.div(class: 'page-header') do
      tag.h1(dir: :auto) do
        block_given? ? yield : head
      end
    end
  end

  ### x-editable support ###

  def xeditable?(object = nil)
    !@noedit && policy(object).edit?
  end

  # - pass model: to avoid STI issues
  # - never use editable directly!
  def my_editable(obj, field, **)
    unless @editable_done
      extrajs(
        unpkg_js_tag(
          'Select2', "select2.#{ASSETMIN}js", version: SELECT2_VERSION
        ),
      )
      extrajs(javascript_include_tag('editable'))
      extracss(stylesheet_link_tag('editable', media: :screen))
      @editable_done = true
    end
    dobj = obj.decorate
    path = dobj.form_path
    path.pop
    path << dobj
    editable(path, field, autotext: :always, mode: :popup, escape: false,
                          model: dobj.model_name.element, **)
  end

  def editable_or(obj, field, **opts)
    if xeditable?(obj)
      my_editable(obj, field, **opts)
    elsif block_given?
      yield
      nil
    else
      opts[:default_rend] || obj.public_send(field)
    end
  end

  def editable_md(obj, field, **)
    editable_or(obj, field, **,
                default_rend: markdown(obj.public_send(field), inline: true))
  end

  # select from a list of objects
  def editable_select(obj, field, source, **opts, &)
    field = field.to_s
    field_sing = field.singularize
    id = "#{field_sing}_id"
    id = id.pluralize unless field_sing == field
    src = source.map { |x| { value: x.id, text: x.decorate.to_s } }
    src.unshift(value: '', text: '') if opts[:allowClear]
    editable_or(obj, id, type: :select2, source: src, title: field,
                         default_rend: list_to_l(obj.public_send(field)),
                         select2: {width: '10rem', multiple: true}.merge(opts), &)
  end

  def editable_date_opts(**opts)
    {
      type: :date, viewformat: 'MM d, yyyy',
      mode: :inline, showbuttons: :bottom,
      placement: :right, language: I18n.locale.to_s,
      datepicker: { daysOfWeekDisabled: '6', language: I18n.locale.to_s },
    }.deep_merge(opts)
  end

  def editable_date(obj, field, **opts, &)
    editable_or(obj, field, **editable_date_opts(opts),
      default_rend: l(obj.public_send(field), format: :long), &)
  end

  def editable_date_range(obj, starts, ends, **)
    if xeditable?(obj)
      my_editable(obj, starts, **editable_date_opts(**)) +
        ' &ndash; '.html_safe +
        my_editable(obj, ends, **editable_date_opts(**))
    elsif block_given?
      yield
      nil
    else
      date_range(obj.public_send(starts), obj.public_send(ends), **)
    end.html_safe
  end

  def editable_bool(obj, field, text = field, **, &)
    editable_or(obj, field, type: :checklist,
                            source: [{ value: 1, text: }],
                            default_rend: tag.em(text),
                            emptytext: "#{text}?", **, &)
  end

  ### translated fields ###

  def trasto_field(form, fields, locales = nil, **opts)
    locales ||= I18n.available_locales
    locales = [*locales]
    fields = [*fields]
    label_id = {}
    obj = form.object
    cname = obj.class.model_name.singular
    locales.map do |loc|
      dir = loc2dir(loc)
      tag.div(dir:, lang: loc, **(opts[:langdiv_opts] || {})) do
        fields.map do |f|
          ff = :"#{f}_i18n"
          opts[f] ||= {}
          label_id[f] ||= opts[f].delete(:label_id) ||
                          "activerecord.attributes.#{cname}.#{f}"
          form.simple_fields_for ff do |g|
            g.input loc, **{
              label: tt(label_id[f], locale: loc),
              hint: tt(f, scope: [:simple_form, :hints, cname], locale: loc,
                          cascade: false, default: nil) ||
                    tt(f, scope: %i[simple_form hints defaults],
                          locale: loc, cascade: false, default: nil),
              input_html: {
                value: obj.public_send(ff)[loc.to_s],
              },
            }.deep_merge(opts[f])
          end
        end.join.html_safe
      end
    end.join.html_safe
  end

  def pagedown(**opts)
    { as: :pagedown, input_html: { preview: true, rows: 10 } }.deep_merge(opts)
  end

  def dl_item(dt, dd, **opts)
    return '' unless dd

    opts = { dd: {},
             dt: {}, }.deep_merge(opts)
    tag.tr do
      (tag.td(tt(dt), **opts[:dt]) +
       tag.td(dd, **opts[:dd])).html_safe
    end
  end

  def lesc(text)
    LatexToPdf.escape_latex(text)
  end

  def underlined(text, char = '=')
    "#{text}\n#{char * text.to_s.length}"
  end

  def alt_fmt(fmt, **)
    footer_item(tt(:download, fmt: t(fmt, scope: [:format_name])), 
                perm_params.merge(format: fmt, locale: I18n.locale), **)
  end

  def text_lines(text, width = 72)
    word_wrap(text || '', line_width: width)&.split("\n") || []
  end

  def centered(text, width = 72)
    text_lines(text, width).map do |l|
      (' ' * [0, (width - l.length) / 2].max) + l
    end.join("\n")
  end

  def blockquote(text, width = 72)
    text_lines(text, width - 2).map { |x| "> #{x}" }.join("\n").html_safe
  end

  def available_formats
    (Mime::SET.symbols & %i[html text yaml json pdf])
  end
  memoize :available_formats

  def text_or_icon(text, icon)
    (tag.span(text.html_safe, class: %w[d-none d-lg-inline]).html_safe +
                 tag.span(icon.html_safe,
                          class: %w[d-lg-none text-hidden],
                          'aria-hidden' => true)
    ).html_safe
  end

  def ml_domain(*extra)
    URI.join(AppSetup.ml_domain, *extra)
  end

  def admin_mail(...)
    User.admin.try(:decorate).try(:mailed, ...)
  end

  def hebsite_url(id = 'default')
    "http://in.bgu.ac.il/teva/math/Pages/#{id}.aspx"
  end
  memoize :hebsite_url

  # use teaching_catalog_path
  # by default search for all math courses
  #def shnaton_url(id = '!sc.AnnualSearchResults?on_course_department=201')
  #  CourseCatalog.uri(id).to_s
  #end
  #memoize :shnaton_url

  def to_commas(list, con = :or)
    return nil if list.blank?

    last = list.pop
    return last if list.blank?

    (list.join(', ') + I18n.t(con, what: last)).html_safe
  end

  def to_label(*args)
    args.select(&:presence).join('_')
  end

  def ltr(dir = :ltr, &)
    dir = dir.to_s
    res = <<~EOF

      {::nomarkdown type="latex"}
      \\begin{#{dir.upcase}}
      {:/nomarkdown}

      {::nomarkdown type="html"}
      <div dir="#{dir}" markdown="1">
      {:/nomarkdown}

      #{capture(&)}

      {::nomarkdown type="html"}
      </div>
      {:/nomarkdown}

      {::nomarkdown type="latex"}
      \\end{#{dir.upcase}}
      {:/nomarkdown}

    EOF
    res.html_safe
  end

  def rtl(&)
    ltr(:rtl, &)
  end

  def badge(text, **opts)
    return '' if text.blank?

    type = opts.delete(:type) || :secondary
    tag.span(text, class: [:badge, "badge-#{type}"], **opts)
  end

  def javascript_include_tag(*addr, **opts)
    dopts = {nonce: true, crossorigin: :anonymous}
    dopts.deep_merge!(data: {turbolinks_track: true}) unless
      addr.all? { |aa| aa.start_with?('http') }
    super(*addr, **dopts.deep_merge(opts))
  end

  def unpkg_path(what, path = nil, ver = nil)
    ver = ver ? "@#{ver}" : ''
    path = path ? "/#{path}" : ''
    "#{CDN_BASE}/#{what}#{ver}#{path}"
  end

  def unpkg_js_tag(what, path = nil, **opts)
    javascript_include_tag(
      unpkg_path(what, path, opts.delete(:version)), **opts
    )
  end

  def unpkg_css_tag(what, path = nil, **opts)
    stylesheet_link_tag(
      unpkg_path(what, path, opts.delete(:version)), **opts
    )
  end

  def jsonify(data)
    JSON.pretty_generate(data.as_json)
  end

  def dlify(data, **opts)
    key_method = opts[:key_method] ||
                 ->(k) { k.try(:to_html) || k.to_s.html_safe }
    exclude = (opts[:exclude] || []).map(&:to_s)

    case data
    when Hash
      tag.dl(data.map do |k, v|
        next unless v.present? || opts[:full]
        next if exclude.include?(k.to_s)

        clt = %w[col-3]
        cld = %w[col-9]
        if v.is_a?(Hash) || v.is_a?(Array)
          clt = %w[col-12]
          cld = %w[offset-1 col-11]
        end
        tag.dt(key_method.(k), class: clt) +
          tag.dd(dlify(v, **opts), class: cld)
      end.join("\n").html_safe, class: %w[row])
    when Array
      if opts[:full] || data.count > 1
        tag.ul(data.map do |v|
                 tag.li(dlify(v, **opts))
               end.join("\n").html_safe)
      else
        data[0].present? ? dlify(data[0], **opts) : ''
      end
    else
      val_method = opts[:val_method] ||
                   ->(v) { tag.code(v.try(:to_html) || v.to_s.html_safe) }
      val_method.(data)
    end
  end

  def webid(type, id, **)
    BaseWebid.service_class(type).new(id, helper: self, **)
  end

  # https://github.com/codetriage/maildown/issues/53
  def md_render(partial = nil, **opts)
    render(**{partial:, locals: {
      "_no_cache_#{@current_template.format || ''}" => true,
    }, }.deep_merge(opts)).html_safe
  end

  def extrajs(content = nil, &)
    controller.send(:extrajs, block ? capture(&) : content)
  end

  def extracss(content = nil, &)
    controller.send(:extracss, block ? capture(&) : content)
  end
end

