# frozen_string_literal: true

module ComponentHelper
  Dir[Rails.root.join('app', 'components', '*')].each do |ff|
    h = File.basename(ff, '.rb').delete_suffix('_component')
    next if h == 'base'

    define_method h.to_sym do |*args, **opts, &block|
      render_component("#{h}_component".camelize.constantize, *args, **opts,
                       &block)
    end
  end

  protected

  def render_component(klass, *, **, &block)
    klass.new(*, context: self, block:, **)
  end
end

