# frozen_string_literal: true

module CommonHelper
  # helper included in the controllers

  def titleise(s)
    return nil unless s.respond_to?(:split)

    s.split.map(&:camelcase).join(' ')
  end

  # avoid errors on blank
  def l(arg, **)
    arg.blank? ? '' : I18n.l(arg, **)
  end

  @@rtl = [:he]

  def loc2dir(loc = I18n.locale)
    @@rtl.include?(loc) ? 'rtl' : 'ltr'
  end

  def is_rtl?(loc = I18n.locale)
    loc2dir(loc) == 'rtl'
  end

  @@nocap = %w[the of a an and].to_set

  def cap(res)
    return '' unless res

    # don't touch capitalized translations, or html
    if res.match?(/[A-Z<>]/)
      res
    else
      res.capitalize.split.map do |x|
        @@nocap.include?(x) ? x : x.camelcase
      end.join(' ')
    end
  end

  # A more sophisticated translation mechanism
  # The following additional features are provided:
  #
  # == Gender aware translations:
  # - Use keys with a gender suffix to provide a translation for that suffix,
  #   e.g.:
  #     current: נוכחי
  #     current_f: נוכחית
  #
  # - Use the gender: option to tt to tell which gender to use, e.g.
  #     tt :current, gender: 'f'
  #
  # - Alternatively, provide an object that responds to :to_trans that 
  # returns the gender. The base decorator does this by calling gender on the 
  # object
  #
  # - The default gender method (in the base decorator) uses 
  # activerecord.models.<modelname>.gender to determine the gender
  #
  # - Upshot: if the he translation file contains:
  #
  #     current: נוכחי
  #     current_f: נוכחית
  #     activerecord:
  #       models:
  #         ac_year:
  #           one: שנה אקדמית
  #           gender: f
  #         term:
  #           one: סמסטר
  #
  # Then `tt :current, term` will translate as `נוכחי` and `tt :current, 
  # ac_year` will translate as `נוכחית` (where `term` and `ac_year` are 
  # objects of the respective type)
  #
  # It is also possible to use the `what` key to include the object in the 
  # translation. For example
  #
  #   current: '%{what} נוכחי'
  #   current_f: '%{what} נוכחית'
  #
  # will translate the above into סמסטר נוכחי and שנה אקדמית נוכחית, 
  # respectively.
  #
  # The same logic applies to symbols given to default: A gendered version is 
  # attempted first
  #
  # == Definite article translation:
  # The key `thewhat` is translated into a "the" version of the noun. This is 
  # done by calling `the` in base_decorator. For this to work, include 
  # translations for non-default `the` in the model. For example
  #
  #   activerecord:
  #     models:
  #       generic_course:
  #         one: קורס גנרי
  #         other: קורסים גנריים
  #         the:
  #           one: הקורס הגנרי
  #           other: הקורסים הגנריים
  #
  # This cascades, so if the key is not included, the default `the` 
  # translation (which uses the `what` argument) is used: 
  #
  #   the: "ה%{what}"
  #
  # Then use `thewhat` as a key to use the definite version. For example:
  #
  #   en:
  #     actions:
  #       index: All %{what} # No 'the'
  #
  #   he:
  #     actions:
  #       index: כל %{thewhat} # Yes 'the'
  #
  #
  def tt(id, obj = nil, **opts)
    if !obj && id.is_a?(Class)
      obj = id
      id = 'actions.what'
    end
    # get trans options from class
    if obj.respond_to?(:to_trans)
      opts = obj.to_trans(**opts).merge(opts)
    elsif obj.respond_to?(:decorator_class)
      opts = (obj.decorator_class.try(:to_trans, **opts) || {}).merge(opts)
    end
    opts = { cascade: true }.merge(opts)
    if opts[:the]
      opts[:what] = opts[:thewhat]
      opts[:whats] = opts[:thewhats]
    end
    # :title is sometimes used in place of :what
    opts[:title] ||= opts[:what]
    # get gender
    if (gender = opts[:gender])
      # treat the case opts[:default] == nil correctly
      deflt = opts.key?(:default) ? opts[:default] : []
      deflt = [deflt] unless deflt.respond_to?(:unshift)
      deflt.unshift(id.to_sym)
      deflt = deflt.map do |dd|
        dd.is_a?(Symbol) ? [:"#{dd}_#{gender}", dd] : nil
      end.flatten
      id = deflt.shift
      opts = opts.merge(default: deflt)
    end
    res = I18n.t(id, **opts)
    res = I18n.t(id, **opts, count: 1) if res.respond_to?(:keys) && res[:one]
    opts[:nocap] ? res : cap(res)
  end

  ### locale manipulations ###
  def in_outer_locale(&)
    in_locale(try(:outer_locale), &)
  end

  def in_locale(loc, &)
    I18n.with_locale(loc, &)
  end

  # rendering shortcuts
  def member(email)
    User.find_by(email: email.to_s).try(:decorate).try(:linked)
  end

  def full_email(email = AppSetup.dept_email, domain = AppSetup.mail_domain)
    return '' unless email

    email.include?('@') ? email : "#{email}@#{domain}"
  end

  def lastmod(model)
    model.maximum(:updated_at)
  end

  def format(default = :html)
    logger.debug("Called 'format', rename to 'dformat': #{caller}")
    try(:controller).try(:request).try(:format) || default
  end

  def ordinalise(num, **opts)
    loc = (opts[:locale] || I18n.locale).to_sym
    res = I18n.with_locale(loc) { num.ordinalize }
    female = opts.delete(loc)
    if female
      res = case num
            when 1
              'ראשונה'
            when 2
              'שניה'
            when 3..10
              "#{res}ת"
            else
              res
            end
    end
    res
  end

  def f_to_s(num)
    (num == num.to_i) ? num.to_i : num
  end

  def bidi_range(bt, tp)
    top = f_to_s(tp)
    bot = f_to_s(bt)
    if top == bot
      bot
    else
      is_rtl? ? "#{top}--#{bot}" : "#{bot}--#{top}"
    end
  end

  # include a mailto: link, rotating the href by the given key, a mild email
  # obfuscation. Mostly copied from the actionview implementation, since
  # there is no easy way to delegate. The href is decoded in javascript, see
  # 'decode-email.js'. The key is random if not passed, pass 0 to disable
  # obfuscation
  def mail_to(email_address, name = nil, key = nil, **html_options, &)
    html_options = html_options.stringify_keys

    html_options['from'] ||= try(:current_user)&.decorate&.email_address(true)
    extras = %w[from cc bcc body subject reply_to].map! do |item|
      option = html_options.delete(item).presence || next
      "#{item.dasherize}=#{ERB::Util.url_encode(option)}"
    end.compact
    extras = extras.empty? ? '' : "?#{extras.join('&')}"

    encoded_email_address = ERB::Util.url_encode(email_address)
                                     .gsub('%40', '@')
    href = "mailto:#{encoded_email_address}#{extras}"
    key ||= rand(1..25)
    html_options['data'] ||= {}
    html_options['data']['key'] = key
    unless html_options['class'].respond_to?(:push)
      html_options['class'] = 
        (html_options['class'] || '').to_s.split
    end
    html_options['class'].push('mailto-encoded')
    html_options['href'] = href.chars.map do |ch|
      (ch =~ /[a-z]/) ? (((ch.ord - 97 - key) % 26) + 97).chr : ch
    end.join
    tag.a(name || email_address, **html_options, &)
  end

  def to_graphviz(nodes = [], edges = [], output = String, **)
    require 'graphviz'
    gv = GraphViz.digraph(:gbase, rankdir: is_rtl? ? 'RL' : 'LR',
                                  ratio: 'fill', size: '1200,960!', splines: :spline,
                                  **)
    nodes.each do |node|
      gv.add_node(node[:id], label: node[:label])
    end
    edges.each do |edge|
      gv.add_edges(edge[:source], edge[:target])
    end
    gv.output(dot: output)
  end

  def graph2tex(graph)
    require 'tempfile'
    file = Tempfile.new
    res = nil
    begin
      nodes = graph[:nodes].clone.select do |node|
        graph[:edges].any? do |edge|
          [edge[:source], edge[:target]].include?(node[:id])
        end
      end
      nodes.each do |node|
        node[:label] = "\\RL{#{node[:label]}}" if /[א-ת]/.match?(node[:label])
        node[:label] = "\\footnotesize{#{node[:label]}}"
      end
      to_graphviz(nodes, graph[:edges], file.path, size: '7,8')
      # TODO! need to patch dot2tex.py to use xelatex in place of latex
      res = `dot2tex --debug --format=tikz --texmode=raw --autosize --figonly #{file.path}`
    ensure
      file.close
      file.unlink
    end
    # need this so that entities are not converted
    res&.html_safe
  end

  def layout_graph(nodes = {}, edges = [], **opts)
    require 'graphviz'
    gvo = begin
      GraphViz.parse_string(to_graphviz(nodes.values, edges))
    rescue SplineTypeException => e
      #TODO
      logger.debug("Graphviz issue: #{e}")
      return
    end
    gvo.each_node do |id, node|
      nodes[id][:x], nodes[id][:y] = node[:pos].point
      nodes[id][:border_size] = nodes[id][:size] / 2
      nodes[id].merge!(opts)
    end
    return unless Rails.env.development?

    nodes.each_value do |vv|
      vv[:object].merge!(vv.slice(:x, :y, :size))
    end
  end

  def dept_schema
    @@dept_schema ||= {
      :@type => 'CollegeOrUniversity',
      address: AppSetup.dept_snail,
      email: full_email,
      faxNumber: AppSetup.dept_fax,
      logo: image_url('bgu_logo_large.jpg'),
      parentOrganization: {
        :@type => 'CollegeOrUniversity',
        name: t(:university),
        # TODO
        url: AppSetup.parent_url,
        logo: image_url('bgu_logo.png'),
      },
      telephone: AppSetup.dept_phone,
      alternateName: t(:depname),
      image: image_url('home.jpg'),
      name: t(:depname),
      url: root_url(locale: ''),
    }
  end

  def same_page(**opts)
    {**params.to_unsafe_h.symbolize_keys, **opts}
  end

  def kramdown_doc(text = nil, **)
    defopts = {auto_ids: false, math_engine: nil}
    defopts[:smart_quotes] = 'rsquo,lsquo,rdquo,ldquo' if is_rtl?
    Kramdown::Document.new((text || '').to_s, **defopts, **)
  end

  def markdown(text = '', **opts)
    format = opts.delete(:format) || 'html'
    obj = kramdown_doc(text, **opts)
    meth = (format.to_s == 'pdf') ? 'to_latex' : "to_#{format}"
    res = obj.public_send(meth)
    res.chomp!
    res.chomp!
    # html safe result, otherwise it is escaped in the latex template
    return res.html_safe unless format.to_s == 'html'

    wrapper = 'div'
    if opts[:inline]
      res.chomp!('</p>')
      res.sub!(/^<p>/, '')
      wrapper = 'span'
    end
    wrapper_opts = opts[:wrapper] || { class: 'mathjax' }
    unless opts[:no_wrapper] || res.blank?
      res = (try(:helpers) || self).content_tag(
        wrapper, res.html_safe, **wrapper_opts
      )
    end
    res.html_safe
  end

  def md2tex(text = '', **opts)
    res = markdown(text, format: :xelatex, **opts)
    res.chomp! if opts[:inline]
    res
  end

  def mail2human(mail, **opts)
    fields = opts[:fields] || %w[From To Cc Bcc Subject Date Reply-To]
    format = opts[:format] || 'text'
    res = fields.map { |ff| mail[ff].present? ? "#{ff}: #{mail[ff]}" : nil }
                .select { |x| x }.join("\n")
    res += "\n\n#{mail.public_send(:"#{format}_part").body}"
    res
  end
end

