# frozen_string_literal: true

class ResearchGroupsSeminar < Base
  belongs_to :research_group
  belongs_to :seminar
end

