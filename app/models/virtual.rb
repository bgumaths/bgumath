# frozen_string_literal: true

class Virtual < User
  def self.policy_class
    VirtualPolicy
  end

  def mail_domain
    AppSetup.local_mail_domain
  end
end

