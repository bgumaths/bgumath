# frozen_string_literal: true

class GenericCourse < Base
  DEP_ID = '201'

  resourcify
  include Namable

  # Handbook
  has_many :course_lists_courses, dependent: :destroy, inverse_of: :course,
                                  foreign_key: :course_id
  has_many :course_lists, through: :course_lists_courses
  has_many :plan_items, dependent: :nullify
  has_many :term_plans, through: :plan_items

  # dependencies
  has_many :requirements, dependent: :destroy, class_name: 'AllowsRequire',
                          foreign_key: :require_id, inverse_of: :require
  has_many :requires, through: :requirements, source: :allow

  has_many :allowances, dependent: :destroy, class_name: 'AllowsRequire',
                        foreign_key: :allow_id, inverse_of: :allow
  has_many :allows, through: :allowances, source: :require

  # Course instances
  has_many :courses, inverse_of: :generic_course,
                     dependent: :restrict_with_error

  # Service departments (for issues)
  has_many :departments_generic_courses, inverse_of: :course,
                                         dependent: :destroy
  has_many :departments, through: :departments_generic_courses
  has_many :issues_staffs, through: :departments, source: :people
  belongs_to :aguda_rep, class_name: 'IssuesAgudaRep', optional: true

  # history
  belongs_to :year_added, class_name: 'AcYear', inverse_of: :added_courses,
                          optional: true
  belongs_to :year_removed, class_name: 'AcYear',
                            inverse_of: :removed_courses, optional: true

  # events when this course is replaced by another course
  has_many :replacements,
           dependent: :destroy, class_name: 'ReplacedByReplace',
           foreign_key: :replaced_by_id, inverse_of: :replaced_by
  has_many :replaced_by, through: :replacements, source: :replace

  # events when this course replaces another course
  has_many :renewals,
           dependent: :destroy, class_name: 'ReplacedByReplace',
           foreign_key: :replace_id, inverse_of: :replace
  has_many :replaces, through: :renewals, source: :replaced_by

  scope :fall, -> { where(fall: true) }
  scope :spring, -> { where(spring: true) }

  scope :active, -> { where year_removed_id: nil }
  def active?
    year_removed.blank?
  end

  # added on year or before
  scope :added, ->(year) {
                  left_outer_joins(:year_added).where(
                    'year_added_id IS NULL OR ac_years.year <= ?', year
                  )
                }
  # not removed on year or before
  scope :not_removed, ->(year) {
                        left_outer_joins(:year_removed).where(
                          'year_removed_id IS NULL OR ac_years.year > ?',
                          year,
                        )
                      }
  scope :active_in, ->(starts = CommonUtil.cur_year, ends = starts) {
                      added(starts).not_removed(ends)
                    }
  scope :include_deprecated, -> { unscope :where }
  scope :deprecated, -> { where.not(year_removed: nil) }
  scope :advanced, -> { where(advanced: true) }
  scope :basic, -> { where(advanced: false) }

  # courses that belong to the department
  scope :dept, ->(dep = DEP_ID) { where('shid LIKE :id', id: "#{dep}%") }

  # The conditions :first_year, :advanced_undergrad, :graduate, :service and 
  # :external must form a partition of the courses.

  # courses by other departments
  scope :external, -> { where.not('shid LIKE :id', id: "#{DEP_ID}%") }
  def external?
    shnaton_dep != DEP_ID
  end

  # service courses are those that do not appear in any course list and are 
  # not advanced
  scope :organic, ->(handbooks = [Shnaton.recent]) {
    dept
      .includes(course_lists: [:shnaton])
      .distinct
      .where(course_lists: { shnaton: [handbooks]})
      .or(advanced.distinct)
  }
  def organic?
    !external? && course_lists.for_years.present?
  end

  # our service courses
  scope :service, -> {
    dept.where.not(id: organic.select(:id)).basic
  }
  def service?
    !external? && course_lists.for_years.blank? && !advanced?
  end

  # our first year courses
  scope :first_year, -> { organic.basic }
  def first_year?
    organic? && !advanced?
  end

  scope :advanced_undergrad, -> { advanced.where(graduate: false).dept }
  def advanced_undergrad?
    !external? && advanced? && !graduate?
  end

  scope :graduate, -> { dept.where(graduate: true) }
  def grad?
    !external? && graduate?
  end

  def level
    %w[first_year advanced_undergrad grad service external].find do |l|
      public_send(:"#{l}?")
    end
  end

  # courses to which issues may apply
  scope :issuable, -> { dept.active.service }
  def issuable?
    !external? && service?
  end

  def mandatory?(degree = AcYear.first.shnaton.degrees.find('general-mathematics'))
    degree
      .prereqs
      .precise
      .any? { |pq| course_lists.to_a.intersect?(pq.course_lists) }
  end

  # with courses satisfying the given criterion
  scope :where_courses, ->(cond) {
    joins(:courses).where(courses: cond).distinct
  }

  scope :in_year, ->(year = CommonUtil.cur_year) {
    where_courses(Course.this_year(AcYear.current(year).fall))
  }

  attaches :descfiles
  translates :description

  validates :shid, uniqueness: { conditions: -> { active } },
                   allow_blank: true, if: :active?

  validates :slug, uniqueness: true

  #default_scope { order(shid: :asc) }

  def in_term(term = Term.default)
    courses.joins(:term).find_by(term:)
  end

  def current(date = Date.current)
    in_term(Term.current_no_exams(date))
  end

  def current?(date = Date.current)
    current(date)
  end

  def current_or_next(date = Date.current)
    current(date) || in_term(Term.after(date))
  end

  def self.canon_shid(ssid)
    if (sid = ssid.dup) # avoid frozen errors
      sid.gsub!(/\D/, '')
      if sid.length > 4
        sid.insert(3, '.').insert(5, '.')
      end
    end
    sid
  end

  # return double the credits, to get an integer
  def dcredits
    self[:dcredits] ||
      ((lectures && exercises) ? (lectures * 2) + exercises : nil)
  end

  def credits
    dcredits ? (dcredits / 2.0) : nil
  end

  def credits=(val)
    self.dcredits = val * 2
  end

  def safe_credits
    credits || 0
  end

  def defaults
    if shid
      self.shid = self.class.canon_shid(shid)
      if graduate.nil?
        self.graduate = shid[4] == '2'
      end
    end
    if graduate
      self.prereq = false
    else
      self.core = false
    end
    self.prereq = false unless prereq
    self.core = false unless core
    true
  end

  def init_from_shnaton!(shid = self.shid)
    self.shid = shid
    # make sure graduate is updated
    self.graduate = nil
    defaults
    det = shnaton_info
    raise ShnatonError, det unless det && det[:id]

    name_i18n['en'] = det[:ename] if name_i18n['en'].blank?
    name_i18n['he'] = det[:name] if name_i18n['he'].blank?
    description_i18n['en'] = det[:esyllabus] if description_i18n['en'].blank?
    description_i18n['he'] = det[:syllabus] if description_i18n['he'].blank?
    self.lectures = det[:lectures] if lectures.blank?
    self.exercises = det[:exercises] if exercises.blank?
    (det[:requires] || []).each do |rshid|
      pp = self.class.find_by(shid: rshid)
      requires.push(pp) if pp && requires.exclude?(pp)
    end
    self
  end

  def self.find_or_create_from_shnaton(shid, **opts)
    shid = canon_shid(shid)
    res = find_or_create_by(**opts.merge(shid:))
    res.init_from_shnaton!(shid)
    res.save!
    res
  end

  def self.create_from_shnaton(shid, **)
    shid = canon_shid(shid)
    return nil if find_by(shid:)

    res = create(**, shid:)
    res.init_from_shnaton!(shid)
    res.save!
    res
  end

  # allow finding by shid
  def self.find(id)
    /^\d{3}\.\d\.\d{4}$/.match?(id.to_s) ? find_by(shid: id.to_s) : super
  end

  def shnaton_id
    shid.split('.')[-1]
  rescue StandardError
    ''
  end

  def shnaton_level
    shid.split('.')[1]
  rescue StandardError
    (graduate ? '2' : '1')
  end

  def shnaton_dep
    (begin
      shid.split('.')[0]
    rescue StandardError
      DEP_ID
    end) || DEP_ID
  end

  def short_shid
    return shid unless shnaton_dep == DEP_ID
    return shnaton_id if shnaton_level == '1'

    "#{shnaton_level}.#{shnaton_id}"
  end

  def shnaton_url
    CourseCatalog.course_link(shid).try(:to_s).try(:html_safe)
  end

  # details parsed from the shnaton (require web access to the actual
  # shnaton. see lib/course_catalog.rb)
  def shnaton_info
    CourseCatalog.course_details(shid)
  end

  # shnaton active course for this GC
  def shnaton_for_term(term = Term.default, **opts)
    term = Term.find(term) if term.is_a?(String)
    dd =
      begin
        CourseCatalog.course_details(
          shid, {rn_year: term.year, rn_semester: term.partnum}, debug: true
        )
      rescue ArgumentError => e
        Rails.logger.info("Failed to fetch info from course catalogue: #{e}")
        return nil
      end
    return nil if dd.blank?

    # reject air force courses
    unless opts[:include_af]
      dd[:groups].delete('9')
      dd[:groups].delete('99')
    end
    return nil if dd[:groups].blank?

    dd
  end

  # all active courses for the term
  def self.shnaton_active(term = Term.default, **opts)
    res = {}
    all.find_each do |gc|
      dd = gc.shnaton_for_term(term, **opts)
      res[gc] = dd if dd.present?
    end
    res
  end

  # create a new Course instance for this, in the given term, from shnaton
  # data
  def instance_from_shnaton(term = Term.default, info = nil)
    info ||= shnaton_for_term(term)
    return nil if info.blank?

    res = Course.new(term:, generic_course: self)
    res.init_from_shnaton!(info)
    res
  end

  # not nil
  validates :prereq, :core, :graduate, :advanced,
            inclusion: { in: [true, false] }
  validates :advanced, inclusion: { in: [true] }, if: :graduate
  validates :prereq, inclusion: { in: [false] }, if: :graduate
  validates :core, inclusion: { in: [false] }, unless: :graduate

  def to_s
    "#{name_i18n['en']} #{shid}"
  end

  # find all prereqs, detecting circular
  def all_requires(seen = [])
    nseen = seen + [self]
    res = requires
    rep = res & nseen
    (res - rep).each do |rr|
      rs = rr.all_requires(nseen)
      res += rs[0]
      rep += rs[1]
    end
    [res, rep]
  end

  # return a message with course that miss departments or aguda reps. It is 
  # here rather than in a view for easy access from the console/rake/etc.
  def self.mir_health(base = issuable, format: ->(gc) { "- #{gc}" }, examined: '')
    nodep = base.where.missing(:departments)
    noaguda = base.where.missing(:aguda_rep)
    res = []
    [
      [nodep - noaguda, 'have no departments assigned'],
      [noaguda - nodep, 'have no aguda rep assigned'],
      [nodep.where.missing(:aguda_rep),
       'have neither departments nor aguda rep']
    ].each do |list, msg|
      next if list.blank?

      res.push(
        '',
        "### The following catalogue courses #{msg}:",
        '',
        *list.map { |c| format.(c) },
      )
    end
    res = ['**No problems found!**'] if res.blank?
    res.push(
      '',
      "### The following catalogue courses were examined:#{examined}",
      '',
      *base.all.map { |c| format.(c) },
      examined.present? ? '{: .collapsible .collapsible-generic .collapse}' : '',
    )
    res.join("\n")
  end

  class ShnatonError < RuntimeError
    attr_reader :errors, :params

    def initialize(data)
      if data.is_a?(Hash)
        @errors = data[:errors]
        @params = data[:params]
      end
      super("Failed to fetch from University catalog. Errors: #{@errors || ''}, Params: #{@params || ''}")
    end
  end

  class CircularReqError < RuntimeError
    attr_reader :course, :circ

    def initialize(course, circ)
      @course = course
      @circ = circ
      super("Circular dependency detected for #{@course}: #{@circ}")
    end
  end
end

