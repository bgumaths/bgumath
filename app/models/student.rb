# frozen_string_literal: true

class Student < Academic
  #self.abstract_class = true
  has_many :students_supervisors, inverse_of: :student, dependent: :destroy
  has_many :supervisors, through: :students_supervisors

  validates :state, presence: true

  def self.policy_class
    StudentPolicy
  end

  def supervisor_emails=(emails)
    self.supervisors = User.where(email: emails)
  end

  protected

  def init
    # Statistics, sad but true...
    self.rank ||= 'Mr' if new_record?
    super
  end
end

