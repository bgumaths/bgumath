# frozen_string_literal: true

class MembersResearchGroup < Base
  belongs_to :member, -> { academic }, class_name: 'Academic',
                                       inverse_of: :members_research_groups, foreign_key: :member_id
  belongs_to :research_group
  validates :member, uniqueness: { scope: :research_group }
  validates :research_group, uniqueness: { scope: :member }
end

