# frozen_string_literal: true

## frozen_string_literal: true

class StudentsSupervisor < Base
  belongs_to :supervisor, -> { can_supervise }, class_name: 'Member',
                                                inverse_of: :students_supervisors
  belongs_to :student, -> { where status: %w[Phd Msc] },
             class_name: 'Student', inverse_of: :students_supervisors
end

