# frozen_string_literal: true

class PlanItem < Base
  resourcify
  belongs_to :term_plan
  belongs_to :generic_course, optional: false
  validate :term_consistency

  has_one :ac_year, through: :term_plan
  has_one :shnaton, through: :term_plan
  has_one :degree, through: :term_plan
  has_one :degree_plan, through: :term_plan

  delegate :credits, to: :generic_course

  def safe_credits
    @safe_credits ||= credits || 0
  end

  def term_consistency
    if term_plan.fall? && !generic_course.fall
      term_plan.errors.add(
        :term_plan, :invalid,
        message: "#{generic_course.decorate.linked_with_actions} is not given in the fall"
      )
    elsif term_plan.spring? && !generic_course.spring
      term_plan.errors.add(
        :term_plan, :invalid,
        message: "#{generic_course.decorate.linked_with_actions} is not given in the spring"
      )
    end
  end
end

