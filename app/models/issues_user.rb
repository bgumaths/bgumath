# frozen_string_literal: true

class IssuesUser < Base
  scope :students, -> { where(type: %w[IssuesStudentRep]) }
  scope :staff, -> { where(type: %w[IssuesStaff]) }
  scope :aguda, -> { where(type: %w[IssuesAgudaRep]) }

  validates :type, inclusion: {
    in: %w[IssuesStudentRep IssuesStaff IssuesAgudaRep],
  }

  validates :login,
            uniqueness: { case_sensitive: false },
            length: { maximum: EMAIL_MAX_LEN },
            presence: true,
            format: {
              without: /@/,
              message: I18n.t(
                'activerecord.errors.models.issues_users.login_at',
              ),
            }

  #validates :fullname, presence: true

  validates :password, confirmation: { allow_blank: true }

  validates :terms_of_usage, acceptance: {accept: [1, '1', true, 'true']}

  def self.non_slugged
    true
  end

  def self.mail_domain
    AppSetup.mail_domain
  end

  def mail_domain
    super || self.class.mail_domain
  end

  extend FriendlyId
  friendly_id :login

  def to_slug
    login
  end

  acts_as_authentic do |c|
    c.logged_in_timeout = 50.years
    c.log_in_after_create = false
    c.log_in_after_password_change = false
    c.crypto_provider = ::Authlogic::CryptoProviders::SCrypt
  end

  before_save { login.downcase! }

  # no longer generate password, since we now have bgu login
  #before_validation :generate_pass, unless: :crypted_password?

  include Authen

  def update_bgu_details
    modified = update_detail 'Full name',
                             fullname, "#{@bgu_user.first_name} #{@bgu_user.last_name}" do |val|
      self.fullname = val
    end
    if respond_to?(:department)
      modified ||=
        update_detail 'Department',
                      department, Department.find_by(bgu_name: @bgu_user.unit_name) do |val|
          self.department = val
        end
    end

    save! if modified
    modified
  end

  # find user also by login
  def self.find_gsrow(row, **opts)
    super || find_by(login: row[:login])
  end

  def generate_pass
    self.password_confirmation = self.password = SecureRandom.urlsafe_base64(8)
    self.pass_changed = false
  end

  class << self
    def types
      [IssuesStaff, IssuesStudentRep, IssuesAgudaRep].freeze
    end

    alias orig_from_gsheets! from_gsheets! # rubocop:disable Style/Alias

    def from_gsheets!(...)
      types.map { |type| type.orig_from_gsheets!(...) || [] }.flatten
    end
  end
end

