# frozen_string_literal: true

class ResourceCarouselItem < CarouselItem
  attr_accessor :small, :title

  def self.resource_types
    [Seminar, Course, Event, Role]
  end

  has_many :resources, class_name: 'CarouselItemRecord', inverse_of: :item,
                       dependent: :destroy, foreign_key: :item_id

  resource_types.each do |res|
    has_many res.model_name.collection.to_sym, 
             through: :resources, source: :resource, 
             source_type: res.model_name.name
  end

  scope :with_resource, ->(res) {
    ass = res.model_name.collection.to_sym
    joins(ass).where(ass => { id: [res.id] })
  }

  accepts_nested_attributes_for :resources, allow_destroy: true,
                                            reject_if: :resource_empty?

  def has_content?
    resources.any?(&:has_content?)
  end

  protected

  def resource_empty?(attrs)
    attrs['resource_type'].blank? || attrs['resource_id'].blank?
  end
end

