# frozen_string_literal: true

class Issue < Base
  resourcify
  belongs_to :course
  belongs_to :reporter, class_name: 'IssuesStudentRep'
  #belongs_to :replier, class_name: 'Regular', optional: true
  has_one :response, class_name: 'IssueResponse', dependent: :destroy
  has_many :issues_issues_staffs, dependent: :destroy
  has_many :staffs, through: :issues_issues_staffs
  belongs_to :aguda_rep, class_name: 'IssuesAgudaRep', optional: true

  validates :title, presence: true
  validates :content, presence: true
  validates :correspondence, presence: true
  validates_each :course do |record, attr, value|
    unless record.reporter.courses.include?(value)
      record.errors.add(
        attr, :invalid, message: I18n.t(:reporter_courses, scope: [
                                          :activerecord, :errors, :models, 
                                          record.class.name.downcase, 
                                          :attributes, attr
                                        ], reporter: record.reporter.login)
      )
    end
  end

  validates :title, :content, :correspondence, frozen: true

  has_one :lecturer, through: :course
  has_one :student_rep, through: :course
  has_many :departments, through: :course
  has_one :term, through: :course

  #scope :published, -> { where(published: true) }
  #scope :replied, -> { where.not(reply: nil) }
  #scope :open, -> { where(reply: nil) }

  # We can't attach stuff to issues, since the issue uses uuid...
  #has_one_attached :hardcopy
  delegate :hardcopy, to: :response

  before_create :set_people

  extend FriendlyId
  friendly_id :to_slug

  MAX_SLUG = 1000000

  def self.new_slug
    id = nil
    begin
      id = "#{SecureRandom.random_number(MAX_SLUG)}.#{CommonUtil.cur_year}"
    end while find_by(slug: id).present?
    id
  end

  def to_slug
    self.class.new_slug
  end

  def replied?
    response&.persisted?
  end

  delegate :publish_delay, to: :response

  def open?
    !replied?
  end

  def replied_at
    response&.replied_at
  end

  def published
    raise 'Using old attribute "published" on issue'
  end

  def published?
    response&.published?
  end

  def published_at
    response&.published_at
  end

  def status
    if published?
      :published
    else
      replied? ? :replied : :open
    end
  end

  STATUS_I = {
    open: 10,
    replied: 20,
    published: 30,
  }.freeze

  def status_i
    STATUS_I[status]
  end

  # we go through self.staffs rather than compute via departments, since 
  # this should persist when departments change
  def staff(dept = nil)
    unless @staff
      @staff = {}
      staffs.each do |stff|
        @staff[stff.department.id] ||= []
        @staff[stff.department.id] << stff
      end
    end
    dept ? @staff[dept.try(:id) || dept] : @staff
  end

  def create_response!(**)
    return if self[:reply].blank?

    IssueResponse.create!(
      issue: self,
      creator: replier_id.present? ? User.find(replier_id) : nil,
      content: self[:reply], published_at: self[:published_at], **
    )
  end

  def create_response(...)
    create_response!(...) if response.blank?
    response
  end

  def save_pdf
    SaveIssuePdfJob.perform_later(self)
  end

  protected

  def set_people
    self.staffs = departments.map { |dd| dd.people.where(active: true) }.flatten
    self.aguda_rep = course.aguda_rep
  end

  # the reporter should be set by the controller according to logged in 
  # user. However, we set a default here in case the object is created 
  # outside the controller. This should happen before validations
  def set_reporter
    return if persisted?

    self.reporter ||= course&.student_rep
  end
end

