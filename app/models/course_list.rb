# frozen_string_literal: true

class CourseList < Base
  resourcify

  has_many :course_lists_courses, dependent: :destroy
  has_many :courses, -> { reorder(shid: :asc) },
           through: :course_lists_courses
  has_many :course_lists_prog_prereqs, dependent: :destroy
  has_many :prereqs, through: :course_lists_prog_prereqs, autosave: false
  translates :remarks

  include ModelShnatonPart
  include Namable

  validates :name_i18n, uniqueness: { scope: :shnaton }

  amoeba do
    exclude_association :prereqs
    customize(->(origo, _newo) {
      origo.logger.debug("Cloning #{origo.name}")
    })
  end

  def credits=(val)
    self.dcredits = val * 2
  end

  def credits
    if self[:dcredits].present?
      dcredits / 2
    else
      courses.present? ? courses.sum(&:safe_credits) : nil
    end
  end
end

