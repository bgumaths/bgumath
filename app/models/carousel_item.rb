# frozen_string_literal: true

require 'sti_preload'

class CarouselItem < Base
  include StiPreload

  validates :type, presence: true

  scope :starts_before, ->(date = Date.current) {
    where(arel_table[:activates].lteq(date)).or(where(activates: nil))
  }
  scope :ends_after, ->(date = Date.current) {
    where(arel_table[:expires].gt(date)).or(where(expires: nil))
  }
  scope :current,
        ->(date = Date.current) { starts_before(date).ends_after(date) }

  scope :future, ->(date = Date.current) { where(activates: date...) }
  scope :past, ->(date = Date.current) { where(expires: ...date) }
  

  # include this here so that actionview detects that this field should be 
  # set in the form, even when we have a generic object
  has_one :image, class_name: 'Photo', 
                  dependent: :destroy, as: :imageable, autosave: true

  accepts_nested_attributes_for :image, allow_destroy: true

  def has_content?
    true
  end
end

