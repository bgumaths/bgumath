# frozen_string_literal: true

## frozen_string_literal: true

class HostsPostdoc < Base
  belongs_to :postdoc, inverse_of: :hosts_postdocs
  belongs_to :host, -> { where status: %w[Regular Kamea] }, 
             class_name: 'Member', inverse_of: :hosts_postdocs
end

