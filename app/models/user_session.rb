# frozen_string_literal: true

class UserSession < BaseUserSession
  # disable checking of active?, since it prevents retired from login
  disable_magic_states true
end

