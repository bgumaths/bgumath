# frozen_string_literal: true

class Member < Academic
  #self.abstract_class = true
  has_many :students_supervisors, inverse_of: :supervisor,
                                  foreign_key: :supervisor_id, dependent: :destroy
  has_many :students, through: :students_supervisors
  has_many :hosts_postdocs, inverse_of: :host,
                            foreign_key: :host_id, dependent: :destroy
  has_many :postdocs, through: :hosts_postdocs
  has_many :hosts_visitors, inverse_of: :host,
                            foreign_key: :host_id, dependent: :destroy
  has_many :visitors, through: :hosts_visitors

  validates :state, presence: true

  def student_emails=(emails)
    self.students = User.where(email: emails)
  end

  def postdoc_emails=(emails)
    self.postdocs = User.where(email: emails)
  end

  def cur_students
    students.active
  end

  def past_students
    students.retired
  end

  def self.policy_class
    MemberPolicy
  end
end

