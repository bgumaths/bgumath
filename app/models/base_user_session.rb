# frozen_string_literal: true

class BaseUserSession < Authlogic::Session::Base
  remember_me true
  remember_me_for 50.years
  secure Rails.env.production?
  httponly true
  # same_site None in dev no longer working, since it is not secure and 
  # cookies are rejected. Seems like Lax is good enough
  #same_site 'None'
  same_site 'Lax'
  verify_password_method :authen?
  allow_http_basic_auth false
  last_request_at_threshold 60.minutes

  # https://github.com/binarylogic/authlogic/issues/467
  def enable_cookie_signing
    self.encrypt_cookie = true
  end
  before_save :enable_cookie_signing

  def user_id
    record.try(record.class.primary_key).try(:to_s)
  end

  def user_cookie
    {
      value: user_id,
      secure:,
      httponly: false,
    }
  end

  # update the correct ip when behind proxy, overrides 
  # Authlogic::Session::Base
  def update_login_ip_addresses
    super
    return unless record.respond_to?(:current_login_ip=)

    record.current_login_ip = controller.request.remote_ip
  end
end

