# frozen_string_literal: true

class TermPlan < Base
  resourcify
  belongs_to :degree_plan
  has_many :plan_items, dependent: :destroy, autosave: true
  has_many :free_plan_items, dependent: :destroy, autosave: true
  accepts_nested_attributes_for :free_plan_items

  has_many :generic_courses, through: :plan_items

  has_one :degree, through: :degree_plan
  has_one :shnaton, through: :degree
  has_one :ac_year, through: :shnaton

  enum term: %w[fall spring].index_by(&:to_sym)

  default_scope -> { order(year: :asc).order(term: :asc) }

  amoeba { enable }

  translates :remarks

  def next_term
    fall? ? { year:, term: :spring } : { year: year + 1, term: :fall }
  end

  def credits
    plan_items.sum(&:safe_credits)
  end

  def min_credits
    credits + free_plan_items.reduce(0) do |tot, item|
      tot + (item.minp || 0)
    end
  end

  def max_credits
    credits + free_plan_items.reduce(0) do |tot, item|
      tot + (item.maxp || 0)
    end
  end
end

