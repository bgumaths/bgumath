# frozen_string_literal: true

class Base < ApplicationRecord
  # the following is called whenever an object is instantiated, not just for
  # new records
  after_initialize :init
  # assign defaults
  before_validation :defaults
  # convert blanks to NULL
  before_save :nil_if_blank

  has_paper_trail

  self.abstract_class = true

  def self.translates(*cols)
    super
    cols.each do |col|
      define_method("#{col}_i18n=") do |val|
        send("#{col}_i18n").merge!(val.stringify_keys)
      end
    end
  end

  # return a chainable scope that does nothing
  scope :null_scope, -> { where }

  # default options for associations
  def self.belongs_to(name, scop = nil, **)
    super(name, scop, **belongs_to_options, **)
  end

  # ordered associations
  def self.ordered(kind, name, scop = nil, **options, &)
    tname = table_name
    order = "#{name}_order"
    ids = "#{name.to_s.singularize}_ids"
    fname = options[:class_name] ? options[:class_name].tableize : name
    scop ||= -> {
      joins(tname.singularize.to_sym).order(
        Arel.sql("array_position(#{tname}.#{order}, #{fname}.id)"),
      )
    }
    # can't do the following, since [] is valid
    #validates order, presence: true
    # Following also doesn't work in rails 6.1, it tries to validate each 
    # element...
    # https://github.com/rails/rails/pull/22610
    #validates order, exclusion: { in: [nil], message: "%{value} cannot be 
    #nil" }

    public_send(kind, name, scop, **options, &)

    define_method(:"#{name}=") do |val|
      super(val)
      public_send(:"#{order}_will_change!")
      public_send(:"#{order}=", val.map(&:id))
      val
    end

    define_method(:"#{ids}=") do |val|
      super(val)
      public_send(:"#{order}_will_change!")
      public_send(:"#{order}=", val)
    end
  end

  # who can admin this object? default_admins is set in the controller to
  # provide a list of the default admins chosen in the admins list in the
  # creation form. This is needed because with rolify, we do not have an
  # admin_ids field in the object
  attr_accessor :default_admins

  def admins
    res = id.present? ? User.resadmins(self).map(&:id) : []
    res.presence || default_admins
  end

  # the list of all users allowed to be admins of this
  def can_admin
    User.corporeal.select { |u| Pundit.policy(u, self).create? }
  end

  # users who actually administer us
  def admin_users(role = :Admin)
    User.resadmins(self, role)
  end

  # is the object current? By default, the object is timeless, and therefore
  # always current
  def current?(_date = Date.current)
    true
  end

  def self.from_csv(from, **opts)
    require 'smarter_csv'
    f = File.open(from, 'r:bom|utf-8')
    SmarterCSV.process(f,
                       col_sep: ';', remove_empty_values: false, remove_zero_values: false,
                       chunk_size: 1, file_encoding: 'r:bom|utf-8', **opts) do |rec|
      r = rec.first
      if (item = find_by(csv_id => r[csv_id]))
        item.update!(r)
      end
    end
    
  end

  #### Google sheets interface ####
  def self.gdrive
    @gdrive ||=
      begin
        require 'google_drive'
        GoogleDrive::Session.from_config(
          Rails.root.join('config', 'gdrive.json').to_s,
        )
      end
  end

  def self.gspreadsheet
    @gspreadsheet ||= gdrive.spreadsheet_by_key(AppSetup.gspreadsheet_key)
  end

  def self.gworksheet(*args)
    title = (["#{model_name.route_key.tr('_', '-')}!"] + args).join(' ')
    @gworksheet ||= {}
    @gworksheet[title] ||= gspreadsheet.worksheet_by_title(title)
  end

  def self.gws_each(**opts, &)
    return nil unless (gws = opts[:gws] || gworksheet(**opts))

    gws.list.each(&)
  end

  # find the object in a given row. By default, find by id, but this can be
  # overloaded in derived classes to try other methods (e.g., course by shid)
  def self.find_gsrow(row, ...)
    #return nil unless (gw = opts[:gws] || gworksheet(opts[:args]))

    row[:id].present? ? find_by(id: row[:id]) : nil
  end

  def self.from_gsheets!(attrs = attributes, *args, **opts)
    return nil unless (gws = gworksheet(*args))

    res = []
    gws_each(gws:) do |row|
      hrow = row.to_h.select { |k, _v| k.present? }.to_h do |key, v|
        k = key.tr('-', '_')
        v = { $1.to_sym => v } while k.sub!(%r{/([^/]*)$}, '')
        [k.to_sym, v]
      end
      if attrs[-1].respond_to?(:keys)
        attrs += attrs.pop.keys
      end
      attrv = opts.merge(hrow.slice(*attrs))
      if (item = find_gsrow(row, gws:, args:))
        item.update(**attrv)
      else
        item = create(**attrv)
      end
      res.push(item)
    end
    res
  end

  def to_find_param(params = {})
    { id: }.merge(params)
  end

  def self.to_csv(to = +'', **opts)
    require 'csv'
    CSV.instance(
      to, headers:, write_headers: true, force_quotes: true,
          col_sep: ';', **opts
    ) do |csv|
      find_each do |rec|
        csv << rec.to_h.values_at(*headers)
      end
    end
    to
  end

  amoeba do
    enable
  end

  def am_dup(param = {})
    res = amoeba_dup
    if res
      res.default_admins = admins
      res.slug = try(:slug) if res.respond_to?(:slug=)
      res.assign_attributes(param)
    end
    res
  end

  def cache_key
    self
    #try(:id) ? "#{model_name.name}_#{id}" : self
  end

  def self.attaches(column)
    has_many_attached column

    class_eval <<-RUBY, __FILE__, __LINE__ + 1
      def add_#{column}(items)    # def add_photos(items)
        #{column}.attach(items)   #   photos.attach(items)
      end                         # end

      def set_#{column}(items)            # def set_photos(items)
        if self.persisted?                #   if self.persisted?
          self.update!(#{column}: items)  #     self.update!(photos: items)
        else                              #   else
          self.#{column} = items          #     self.photos = items
        end                               #   end
      rescue ActiveRecord::RecordInvalid => e
        self.#{column} = items
      rescue ActiveRecord::RecordNotFound => e
        errors = e
        false
      end

      def delete_#{column}(ids)
        #{column}_attachments.where(id: ids).find_each(&:purge)
        true
      rescue
        false
      end

      def #{column}_signed_ids
        #{column}.map(&:signed_id).to_json
      end

      def #{column}_signed_ids=(ids)
        ids = JSON.parse(ids) unless ids.respond_to?(:map)
        set_#{column}(ids.map{|xx| ActiveStorage::Blob.find_signed!(xx)})
      end

      def #{column}=(obs)
        super(obs.map{|ob| ob['signed_id'] ? ActiveStorage::Blob.find_signed!(ob['signed_id']) : ob})
      end

    RUBY
  end

  def self.csv_id
    @csv_id ||= :id
  end

  def self.belongs_to_options
    {}
  end

  # names of attributes to serialize
  def self.attr_names
    @attr_names ||= attribute_names
  end

  def self.dropped_relations
    @dropped_relations ||= []
  end

  # http://nathanmlong.com/2013/05/better-single-table-inheritance/
  # put
  #   delegate_info :foo, :bar, to: 'MomoInfo'
  # in the STI class Momo. Fields :foo, :bar come from MomoInfo
  def self.delegate_info(*attrs, **opts)
    info_class = opts[:to] || "#{self}Info"
    inv = opts[:inverse] || info_class.underscore

    has_one :info, class_name: info_class, autosave: true,
                   dependent: :destroy, foreign_key: :"#{inv}_id",
                   inverse_of: :"#{inv}", validate: true

    dropped_relations
    @dropped_relations.push(:info)

    amoeba do
      include_association :info
    end

    default_scope { joins(:info) }

    define_method :info do
      super() || build_info
    end

    extend Forwardable
    attrs.each do |aname|
      def_delegators :info, :"#{aname}", :"#{aname}=", :"#{aname}?"
    end
    # init attr_names
    attr_names
    @attr_names += attrs.map(&:to_s)
  end

  # put
  #   info_of 'Momo', field: :momo
  # in the info class MomoInfo
  def self.info_of(cls, **opts)
    field = opts[:field] || cls.underscore
    belongs_to field, autosave: true, class_name: cls,
                      dependent: :destroy, validate: true, inverse_of: :info,
                      touch: true
  end

  ### carousel
  def create_carousel_item!(**)
    ResourceCarouselItem.create(
      self.class.model_name.collection.to_sym => [self],
      **carousel_opts, **
    )
  end

  def create_carousel_item(...)
    ResourceCarouselItem.with_resource(self).take ||
      create_carousel_item!(...)
  end

  def has_carousel_content?
    true
  end

  def to_api_json(...)
    to_json(...)
  end

  protected

  def carousel_opts
    {}
  end

  def defaults
    true
  end

  def init
  end

  def nil_if_blank
    attribute_names.each do |att|
      self[att] = nil if self[att].blank? && self[att].is_a?(String)
    end
  end

  def touch_updated_at(assoc)
    tt = Time.zone.now
    update(updated_at: tt) unless new_record?
    assoc.update(updated_at: tt) unless assoc.new_record?
  end
end

