# frozen_string_literal: true

class AdvancedCourseList < CourseList
  validates :type, uniqueness: { scope: :shnaton }

  def courses
    GenericCourse.in_year(shnaton.ac_year).advanced_undergrad
  end

  def init
    self.shnaton ||= Shnaton.for_year
    I18n.available_locales.each do |loc|
      name_i18n[loc.to_s] ||= self.class.human_attribute_name(:name, locale: loc)
    end
  end
end

