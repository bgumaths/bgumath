# frozen_string_literal: true

## frozen_string_literal: true

class ReplacedByReplace < Base
  belongs_to :replaced_by, class_name: 'GenericCourse'
  belongs_to :replace, class_name: 'GenericCourse'
end

