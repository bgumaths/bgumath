# frozen_string_literal: true

class Visitor < BaseEvent
  resourcify
  include Ddurable
  translates :first, :last
  has_many :hosts_visitors, dependent: :destroy
  has_many :hosts, through: :hosts_visitors

  scope :centre, -> { where(centre: true) }

  validates :first_i18n, trasto_weak_presence: true
  validates :last_i18n, trasto_weak_presence: true

  def host_emails=(emails)
    self.hosts = emails.map { |e| User.find_by(email: e) }.select { |x| x.is_a?(User) }
  end
end

