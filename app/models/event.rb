# frozen_string_literal: true

class Event < BaseEvent
  resourcify
  include Durable
  attaches :descfiles
  extend FriendlyId
  friendly_id :to_slug

  after_create :create_carousel_item

  def to_slug
    title
  end

  # TODO: move this to settings
  @@old = 6.months.ago
  @@expires = 2.years

  scope :recent, ->(fr = @@old) {
    end_between(fr, Date.current.beginning_of_day)
  }
  scope :fresh, ->(today = Time.zone.today) {
    where('expires IS NULL OR expires > :today', today:)
  }
  scope :past, ->(til = @@old, today = Time.zone.today) {
    end_before(til).fresh(today)
  }

  scope :expired, ->(wh = Time.zone.today) {
    where('expires <= :when', when: wh)
  }

  scope :centre, -> { where(centre: true) }

  protected

  def carousel_opts
    super.merge(expires: ends)
  end

  def init
    super
    self.expires ||= Time.zone.today + @@expires if new_record?
    self.duration ||= Time.zone.now..Time.zone.now
  end
end

