# frozen_string_literal: true

class Meeting < BaseEvent
  resourcify
  include Shiftable

  attaches :descfiles

  belongs_to :seminar, optional: false

  translates :announcement

  delegate :term, :is_colloq, :is_regular, to: :seminar, allow_nil: true

  validates :date, presence: true

  validates_each :date do |rec, att, val|
    # if the date is blank, don't fail here, as we do not have a proof that 
    # this is outside the term (but it will fail another validation)
    # Likewise for term
    unless val.blank? || rec.term.blank? || rec.term.include?(val)
      rec.errors.add(
        att, :invalid, message: I18n.t(
          'activerecord.errors.models.meeting.wrong_term',
          term: rec.term.decorate, date: val,
        )
      )
    end
  end

  def self.default_scope
    order(duration: :asc)
  end

  # all meetings of a seminar with this name (any term)
  scope :of_seminar, ->(sem) { joins(:seminar).where(seminars: { slug: sem }) }

  extend FriendlyId
  friendly_id :to_slug, use: %i[scoped], scope: :seminar

  def to_slug
    if date.blank?
      nil
    else
      duration.first.strftime("%F#{unusual_time ? '_%R' : ''}")
    end
  end

  def should_generate_new_friendly_id?
    slug != to_slug
  end

  def room
    self[:room] || seminar.room
  end

  def unusual_day
    seminar.blank? || date.wday != seminar.day
  end

  def unusual_time
    seminar.blank? || starts != seminar.starts
  end

  def unusual_place
    seminar.blank? || room != seminar.room
  end

  protected

  def init_duration
    now = Time.zone.now
    self.duration ||= seminar ? seminar.duration : now..now
  end

  def init
    super
    return unless seminar && new_record?

    self.duration ||= seminar.duration
    self.date ||= seminar.default_date
    # allow room to be empty, and use the seminar's
    self.room = nil
  end
end

