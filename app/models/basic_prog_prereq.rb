# frozen_string_literal: true

class BasicProgPrereq < ProgPrereq
  delegate_info :minc, :minp, :maxc, :maxp, inverse: 'prereq'

  extend FriendlyId
  friendly_id :to_slug, use: :scoped, scope: :degree
end

