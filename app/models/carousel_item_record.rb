# frozen_string_literal: true

class CarouselItemRecord < Base
  belongs_to :item, class_name: 'ResourceCarouselItem', inverse_of: :resources
  belongs_to :resource, polymorphic: true

  def has_content?
    resource&.has_carousel_content?
  end
end

