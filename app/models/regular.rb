# frozen_string_literal: true

class Regular < Member
  has_many :issue_responses, inverse_of: :creator, foreign_key: :creator_id,
                             dependent: :restrict_with_error
  has_many :issues, through: :issue_responses
end

