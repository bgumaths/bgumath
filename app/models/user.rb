# frozen_string_literal: true

require 'sti_preload'

class User < Base
  self.inheritance_column = :status

  include StiPreload
  resourcify

  before_save :default_oh_expiry
  # The following causes delays and login failure
  before_save :update_webids
  before_save :update_photo

  has_many :leaves, autosave: true, dependent: :destroy

  accepts_nested_attributes_for :leaves, reject_if: ->(atr) {
    atr[:duration].blank? || atr[:duration][:first].blank?
  }

  has_many :role_events, dependent: :nullify

  # is the user on leave on dd?
  def onleave?(dd = Time.zone.today)
    leaves.ongoing(dd).last
  end

  # is the user on leave some time?
  def onleave
    !leaves.empty? && leaves.last.persisted?
  end

  def onleave=(onlv)
    leaves.clear unless onlv && (onlv.to_s == '1')
  end

  extend FriendlyId
  friendly_id :to_slug

  def to_slug
    email.try(:sub, /@.*/, '')
  end

  WEB_IDS = %i[pure orcid gs scopus wos arxiv mathscinet].freeze

  WEB_IDS.each do |wid|
    validates wid, uniqueness: true, allow_nil: true
  end

  def webids
    WEB_IDS.index_with { |x| try(x) }.select { |_k, v| v.present? }
  end

  # authlogic
  acts_as_authentic do |c|
    c.ignore_blank_passwords = true
    c.crypted_password_field = :password_digest
    c.logged_in_timeout = 50.years
    c.transition_from_crypto_providers = [Authlogic::CryptoProviders::Sha512]
    # do not log a user in after they are saved
    c.log_in_after_create = false
    c.log_in_after_password_change = true
    c.crypto_provider = ::Authlogic::CryptoProviders::SCrypt
  end

  # we don't use habtm, but rolify does
  def self.has_and_belongs_to_many(attr, **opts)
    jt = opts.delete(:join_table).to_sym
    has_many jt, inverse_of: :user, dependent: :destroy
    has_many attr, through: jt, source: :role, **opts
  end

  rolify

  def add_role(*, **opts)
    erole = roles.by_name(*).first
    role = super(*)
    unless erole
      role_events.public_send(persisted? ? :create! : :build,
                              type: 'RoleAdded', role:,
                              creator: opts[:modified_by])
    end
    role
  end

  def remove_role(*args, **opts)
    role = args[0].is_a?(Role) ? args[0] : roles.by_name(*args).first
    return unless role

    roles.delete(role)
    role_events.public_send(persisted? ? :create! : :build,
                            type: 'RoleRemoved', role:,
                            creator: opts[:modified_by])
    role
  end

  # for pundit: don't use inheirted policy classes
  def self.policy_class
    UserPolicy
  end

  def syspass
    password_digest.blank?
  end

  def syspass=(val)
    return unless val.to_s == '1'

    self.password_digest = nil
    self.password_salt = nil
  end

  def self.statuses
    #descendants.map { |k| k.name }
    %w[Admin Regular Kamea Virtual Adjunct Postdoc Msc Phd External]
  end

  # this should be fazed out: instead use start, finish and deceased dates
  enum state: {_active: 0, _retired: 1, _deceased: 2}

  scope :alive, -> { where.not(state: states[:_deceased]).where(deceased: nil) }
  scope :deceased, -> {
    where(state: states[:_deceased]).or(where.not(deceased: nil))
  }
  scope :active, -> { alive.where(state: [states[:_active], nil]).end_from }
  scope :retired, -> { alive.where(state: states[:_retired]).or(end_before) }

  # compatibility with duration/events etc.
  # users who started before the given date
  scope :before, ->(twhen = Date.current.beginning_of_day) {
    twhen.present? ? where(start: ..twhen - 1.day) : null_scope
  }

  scope :end_before, ->(twhen = Date.current.beginning_of_day) {
    twhen.present? ? where(finish: ..twhen - 1.day) : null_scope
  }

  # user who started at the given time or later
  scope :from_time, ->(twhen = Date.current.beginning_of_day) {
    twhen.present? ? where(start: twhen..) : null_scope
  }

  scope :end_from, ->(twhen = Date.current.beginning_of_day) {
    return null_scope if twhen.blank?

    res = where(finish: twhen..)
    res = res.or(where(finish: nil)) if twhen >= Date.current.beginning_of_day
    res
  }

  scope :between, ->(starts, ends) {
    from_time(starts)&.before(ends) || null_scope
  }

  scope :end_between, ->(starts, ends) {
    end_from(starts)&.end_before(ends) || null_scope
  }

  scope :on, ->(twhen = Date.current) {
    between(twhen.beginning_of_day, twhen.beginning_of_day + 1.day)
  }

  scope :end_on, ->(twhen = Date.current) {
    end_between(twhen.beginning_of_day, twhen.beginning_of_day + 1.day)
  }

  scope :year, ->(y = CommonUtil.cur_year) {
    between(
      Date.new(y - 1, CommonUtil::YEAR_START, 1),
      Date.new(y, CommonUtil::YEAR_START, 1),
    )
  }

  scope :end_year, ->(y = CommonUtil.cur_year) {
    end_between(
      Date.new(y - 1, CommonUtil::YEAR_START, 1),
      Date.new(y, CommonUtil::YEAR_START, 1),
    )
  }

  # users who can have visitors and postdocs
  scope :can_host, -> { alive.where(status: %w[Regular Kamea]) }

  class << self
    alias_method :can_supervise, :can_host
  end

  scope :around, -> { active.or(can_host) }

  scope :members, -> { active.where(status: %w[Regular Kamea]) }
  scope :regulars, -> { active.where(status: %w[Regular]) }
  scope :kameas, -> { active.where(status: %w[Kamea]) }
  scope :emeriti, -> { retired.where(status: %w[Regular Kamea]) }
  scope :students, -> { active.where(status: %w[Phd Msc]) }
  scope :past_students, -> { retired.where(status: %w[Phd Msc]) }
  scope :corporeal, -> { where.not(status: %w[Virtual]) }
  # all academic
  scope :academic, -> {
    where(status: %w[Regular Kamea Adjunct Phd Msc Postdoc External])
  }
  # currently teaching (have office hours)
  scope :teachers, -> { alive.where.not(hours: '') }
  # people who can teach a course
  scope :lecturers, -> {
    around.where(status: %w[Regular Kamea Adjunct Postdoc External])
  }
  # anyone who could have office hours (e.g., exercise sessions, etc.)
  scope :teaching, -> { around.academic }

  scope :on_leave, ->(dd = Time.zone.today) {
    joins(:leaves).where('leaves.duration @> ?::date', dd)
  }
  scope :present, ->(dd = Time.zone.today) {
    left_outer_joins(:leaves).where(
      'leaves IS NULL OR NOT leaves.duration @> ?::date', dd
    )
  }
  scope :groupless, -> {
    includes(:research_groups).where(research_groups: { id: nil })
  }

  scope :find_by_approx_name, ->(str) {
    where("last_i18n->>'he' = :name", name: str.try(:split).try(:last))
  }

  # regular users who admin this resource
  scope :resadmins, ->(resource, role = :Admin) {
    with_role(role, resource).without_role(role).corporeal
  }

  def alive?
    !_deceased? && deceased.blank?
  end

  def retired?
    alive? && (_retired? || finish.present?)
  end

  def emeritus?
    retired? && is_a?(Member)
  end

  def can_host?
    alive? && is_a?(Member)
  end

  alias_method :can_supervise?, :can_host?

  def hours_modified_since(date = 6.months.ago)
    versions.reverse.find do |vv|
      vv.created_at > date && vv.object && vv.object['hours'] != hours
    end
  end

  # people with precisely this role (argument is a Role instance)
  scope :with_rol, ->(rol) {
    q = with_role(*rol.to_args)
    rol.resource ? q.without_role(rol.name) : q
  }

  # issues repliers
  def self.repliers
    with_any_role(*AppSetup.issues_roles)
  end

  def self.chair
    active.with_role(:Chair).take
  end

  def self.admin
    Virtual.active.with_role(:Admin).take
  end

  def self.secretaries
    active.with_role(:Secretary)
  end

  def self.administrator
    active.with_role('Department Administrator').take
  end

  def self.vice
    active.with_role('Department vice head')
  end

  def self.coordinator
    active.with_role('Student Coordinator').take
  end

  # director of the center for advanced studies. Might be an emeritus, so 
  # not active
  def self.director
    alive.with_role('Center Director').take
  end

  def default_oh_expiry
    self.oh_expiry ||= Term.default.try(:probably_next_date)
  end

  # name of the mailing-list file
  def self.ml_file(who = '')
    who = '' if who.to_s == '_active'
    "#{AppSetup.ml_base}/math_#{name.downcase}#{who}"
  end

  # the list of emails
  def self.ml(who = :_active)
    where(state: states[who]).map(&:email)
  end

  def self.write_ml(who = :_active)
    require 'fileutils'
    # TODO
    FileUtils.mkdir_p AppSetup.ml_base
    File.open(ml_file(who), 'w') { |f| f.puts ml(who) }
  end

  validates :email, presence: true,
                    length: { maximum: EMAIL_MAX_LEN },
                    uniqueness: { case_sensitive: false }
  require 'mail'
  validates_each :email do |rec, attr, val|
    Mail::Address.new(
      (val || '').include?('@') ? val : "#{val}@#{rec.mail_domain}",
    )
  rescue Mail::Field::ParseError => e
    rec.errors.add(attr, :invalid, message: e.message)
  end

  validates :mail_domain,
            format: {
              with: /\A([a-z]*\.)?#{Regexp.escape(AppSetup.mail_domain)}\Z/,
            },
            allow_nil: true

  validates :status, presence: true
  validates :status, inclusion: { in: statuses }

  validates :first, length: { maximum: NAME_MAX_LEN }
  validates :last, length: { maximum: NAME_MAX_LEN }
  validates :password, confirmation: { allow_blank: true }
  #has_secure_password validations: false

  # atm, allow only one leave
  validates :leaves, length: { maximum: 1 }

  before_validation do
    email.slice!(/@.*#{mail_domain}$/) if email.respond_to?(:slice!)
  end

  ### authentication (with authlogic) ###
  # Three options, in order of precedence: native (password stored in our 
  # db), `bgu` with bgu id services, and `pam` using pam with the CS db

  include Authen

  def login
    email
  end

  def crypted_password
    password_digest
  end

  def self.find_by_login(login)
    find_by(email: login)
  end

  def self.auth_methods
    super + %w[pam]
  end

  def authen_pam(pass)
    # for rpam must have unix_chkpwd sgid root, and process owner (wwwmath) 
    # must have read access to /etc/shadow
    require 'rpam'

    logger.info("Authenticating #{login} via cs pam")
    Rpam.auth(login, pass, service: PAM_SERVICE)
  end

  RTRANS = {
    'ד"ר' => 'Dr',
    'מר' => 'Mr',
  }.freeze

  def update_bgu_details
    modified = update_detail 'Hebrew first name', first_i18n['he'],
                             @bgu_user&.first_name do |val|
      first_i18n['he'] = val
    end
    modified ||= update_detail 'Hebrew last name', last_i18n['he'],
                               @bgu_user&.last_name do |val|
      last_i18n['he'] = val
    end
    modified ||= update_detail 'English first name', first_i18n['en'],
                               @bgu_user&.foreign_first_name&.capitalize do |val|
      first_i18n['en'] = val
    end
    modified ||= update_detail 'English last name', last_i18n['en'],
                               @bgu_user&.foreign_last_name&.capitalize do |val|
      last_i18n['en'] = val
    end
    modified ||= update_detail 'Rank', rank,
                               RTRANS[@bgu_user&.title_desc] do |val|
      self.rank = val
    end

    modified ||=
      update_detail! 'Mail domain', mail_domain,
                     @bgu_user&.email_address&.gsub(/\A.*@/, '') do |val|
        self.mail_domain = val
      end

    save! if modified
    modified
  end

  # this is used in the controller to find the roles the user currently has
  def admins
    roles.map(&:id)
  end

  def is_staff?
    roles.to_a.intersect?(AppSetup.staff_roles)
  end

  def is_issues_responder?
    roles.to_a.intersect?(AppSetup.issues_roles)
  end

  def is_Admin?
    has_role?(:Admin)
  end

  def is_creator_of?(obj)
    has_role?(:creator, obj)
  end

  def is_Admin_of?(obj)
    has_role?(:Admin, obj)
  end

  def is_corporeal?
    !is_a?(Virtual)
  end

  # for saml
  def full_email
    email.include?('@') ? email : "#{email}@#{mail_domain}"
  end

  def asserted_attributes
    {
      email: {
        getter: :full_email,
      },
      first_name: {
        getter: ->(u) { u.first_i18n['en'] },
      },
      last_name: {
        getter: ->(u) { u.last_i18n['en'] },
      },
      groups: {
        getter: ->(u) { [u.status] },
      },
      phone: {
        getter: :phone,
      },
      location: {
        getter: :office,
      },
      image: {
        getter: ->(u) { u.photo.try(:normal).try(:url) },
      },
      urls: {
        getter: ->(u) {
          {
            'BGU math' => u.decorate.try(:url),
          }
        },
      },

    }
  end

  # https://github.com/binarylogic/authlogic/issues/462
  def update_sessions?
    super && persistence_token_was.present?
  end

  # seminars this user admins
  def seminars
    Seminar.with_role(:Admin, self)
  end

  # courses by this user
  def courses
    Course.where(lecturer: self)
  end

  # remove seminars from attribute list to avoid recursion
  def attributes(*)
    super.except('seminars')
  end

  ## interface to BGU Pure and web ids
  def pure=(val)
    if val.present? && !val.match?(
      ActiveRecord::ConnectionAdapters::PostgreSQL::OID::Uuid::ACCEPTABLE_UUID,
    )
      @pure_info = get_pure(search: val)
      val = @pure_info.uuid
    end
    super
  end

  def get_pure(...)
    require 'bgu/pure'
    # TODO: no testing yet, use WebMock
    Rails.env.test? ? nil : Bgu::Pure::Person.new(...)
  rescue Bgu::Pure::ApiError => e
    logger.warn("  Failed to access pure: #{e}")
    nil
  end

  def pure_info
    @pure_info ||= get_pure(pure)
  end

  def update_webids
    return unless pure && pure_info

    self.orcid ||= pure_info.orcid
    self.scopus ||= pure_info.scopus
    self.wos ||= pure_info.wos
  end

  ###### Photos #######
  # `photos` is all photos of this user
  # `photo` is the currently selected one
  has_many :photos, dependent: :destroy, as: :imageable, autosave: true
  # the selected photo
  belongs_to :photo, # rubocop: disable Rails/InverseOf
             ->(user) {
               where(imageable: user).or(Photo.where(imageable: nil))
             },
             optional: true

  accepts_nested_attributes_for :photos, allow_destroy: true

  def photo_attachments
    ActiveStorage::Attachment.where(record: photos)
  end

  def photo_signed_id=(signed_id)
    ph = Photo.find_signed(signed_id)
    self.photo = ph if ph
  end

  # A photo could already exist but not be attached to us, so we update ids, 
  # in case it needs to be replaced, and push new ones
  def photos_attributes=(items)
    items.each do |key, att|
      # items could be an array or a hash, if array the value is in key
      att ||= key
      res = Photo.find_by(id: att[:id]) if att[:id].present?
      res ||= Photo.find_signed(att[:signed_id]) if att[:signed_id].present?
      res ||= Photo.find_by(id: key) unless key.is_a?(Hash)
      next if res.blank?

      att[:id] = res.id
      photos.push(res) if res.imageable.blank?
    end
    super
  end

  translates :first, :last

  scope :by_first, ->(loc = I18n.locale) {
    reorder Arel.sql "first_i18n->>'#{loc}'"
  }

  scope :by_last, ->(loc = I18n.locale) {
    reorder Arel.sql "last_i18n->>'#{loc}'"
  }

  before_save { email&.downcase! }

  def self.csv_id
    @csv_id ||= :email
  end

  def self.mail_domain
    AppSetup.mail_domain
  end

  def mail_domain
    super || self.class.mail_domain
  end

  # we might be destroying the selected photo via the photos association, 
  # then get an exception since it is still referenced by photo, so remove 
  # the association first
  def update_photo
    return unless photo

    # for some reason, following is not equal to self.photo
    ph = photos.find { |pp| pp.id == photo&.id }
    return unless ph

    self.photo = nil if ph.marked_for_destruction?
  end
end

