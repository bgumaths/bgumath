# frozen_string_literal: true

class ImageCarouselItem < CarouselItem
  validates :image, presence: true

  def image_signed_ids
    [image&.signed_id].select(&:present?).to_json
  end

  def image_signed_ids=(id)
    id = JSON.parse(id) if id.is_a?(String)
    item = Photo.find_signed([*id].find(&:present?))
    if persisted?
      update!(image: item)
    else
      self.image = item
    end
  end

  def image_attributes=(arg)
    self.image = (Photo.find(arg[:id]) if arg[:id].present?) ||
                 (Photo.find_signed([*arg[:signed_id]].find(&:present?)) if arg[:signed_id].present?)
    arg[:id] ||= image&.id
    super
  end
  
end

