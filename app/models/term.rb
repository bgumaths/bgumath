# frozen_string_literal: true

class Term < Base
  resourcify
  translates :name
  extend FriendlyId
  friendly_id :to_slug

  # check that none of the dates fall within a different term
  validates_each :starts, :ends do |rec, attr, val|
    # Allow ad-hoc terms overlap other terms
    if rec.regular?
      existing = current_no_exams(val)
      if existing && existing != rec
        rec.errors.add(
          attr, :invalid, message: I18n.t(
            'activerecord.errors.models.term.overlap',
            term: existing.decorate,
          )
        )
      end
    end
  end

  #validate :term_limits

  def part
    return nil unless starts.respond_to?(:month)
    return :summer if name.present?

    (starts.month < 7) ? :spring : :fall
  end

  @@partnum = { fall: 1, spring: 2, summer: 3 }.freeze

  def partnum
    @@partnum[part]
  end

  def fall?
    part == :fall
  end

  def spring?
    part == :spring
  end

  def year
    return nil unless starts.respond_to?(:year)

    fall? ? starts.year + 1 : starts.year
  end

  def ac_year
    AcYear.find(year:)
  end

  def to_slug
    name_i18n['en'].presence || "#{part}#{year}"
  end

  has_many :courses, dependent: :destroy
  has_many :seminars, dependent: :destroy

  scope :regular, -> { where(name_i18n: {}) }
  #default_scope { regular }
  scope :adhoc, -> { where.not(name_i18n: {}) }

  PI = Float::INFINITY
  MI = -PI

  scope :starts_before, ->(date = Date.current) {
    where(starts: MI..date.to_date)
  }

  # terms ending before date, excluding exams period
  scope :before_no_exams, ->(date = Date.current) {
    where(ends: MI..date.to_date - 1.day).order(starts: :desc)
  }

  # terms starting after date
  scope :after, ->(date = Date.current) {
    where(starts: date.to_date + 1.day..PI).order(starts: :asc)
  }

  scope :ends_no_exams_after, ->(date = Date.current) {
    where(ends: date.to_date..PI).order(starts: :asc)
  }

  # terms ending before date
  scope :before, ->(date = Date.current) {
    cur = current(date)&.starts
    cur ? before_no_exams(cur) : none
  }

  scope :ends_after, ->(date = Date.current) {
    cur = current(date)&.starts
    cur ? after(cur - 1.day) : none
  }

  scope :contains_no_exams, ->(date = Date.current) {
    starts_before(date).ends_no_exams_after(date)
  }

  scope :with_courses, ->(cscope) {
    joins(:courses).merge(cscope).select('terms.*').distinct
  }

  def self.current(date = Date.current)
    starts_before(date).order(starts: :asc).last
  end

  def self.current_no_exams(date = Date.current)
    res = current(date)
    (res && res.ends < date) ? nil : res
  end

  # fall and spring term for the given _academic_ year, i.e., 2016 returns
  # fall 2016 (which begins in 2015) and spring 2016 (this is consistent 
  # with cur_year)
  def self.fall(year = CommonUtil.cur_year)
    res = current(Date.new(year.to_i - 1, 12, 1))
    (res&.year == year.to_i && res&.fall?) ? res : nil
  end

  def fall
    self.class.fall(year)
  end


  def self.spring(year = CommonUtil.cur_year)
    res = current(Date.new(year.to_i, 5, 1))
    (res&.year == year.to_i && res&.spring?) ? res : nil
  end

  def spring
    self.class.spring(year)
  end

  def self.next(date = Date.current)
    after(date).first
  end

  def next
    self.class.next(ends)
  end

  def after
    self.class.after(ends)
  end

  # longest expected break after this term, in days
  def max_break
    fall? ? 92 : 158
  end

  # a date near the probable start of the next term
  def probably_next_date
    res = self.next&.starts
    return res if res && (res - ends < max_break)

    (if fall?
       ends + 7.weeks
     else
       adhoc? ? ends + 4.weeks : ends + 13.weeks
     end) +
      2.days
  end

  def self.prev(date = Date.current)
    before(date).first
  end

  def prev
    self.class.prev(starts)
  end

  def before
    self.class.before(starts)
  end

  def self.default_with_exams(date = Date.current)
    current(date) || self.next(date) || prev(date)
  end

  def adhoc?
    name.present?
  end

  def regular?
    !adhoc?
  end

  def other
    if adhoc?
      nil
    else
      res = fall? ? spring : fall
      (res == self) ? nil : res
    end
  end

  def self.default_no_exams(date = Date.current)
    current_no_exams(date) || self.next(date) || current(date) || prev(date)
  end

  def self.default(...)
    default_no_exams(...)
  end

  def self.next_or_cur(date = Date.current)
    self.next(date) || current(date)
  end

  def self.next_or_cur_id(date = Date.current)
    next_or_cur(date).try(:id)
  end

  def include?(date = Date.current)
    date >= starts && date < (self.next&.starts || PI)
  end

  alias_method :current?, :include?

  def recent?(date = Date.current)
    current?(date) || self.next.try(:current?, date)
  end

  def nearby?(date = Date.current)
    recent?(date) || prev.try(:current?, date)
  end

  protected

  def self.min_len
    (13.weeks - 3.days).freeze
  end

  def self.max_len
    18.weeks.freeze
  end

  def term_limits
    # Allow ad-hoc terms to start and end anytime
    return if adhoc?

    unless [9, 10, 11, 2, 3, 4].include?(starts.month)
      errors.add(
        :starts, :invalid, message: I18n.t(
          'activerecord.errors.models.term.start_in',
          month: starts.month,
        )
      )
    end

    len = (ends - starts).days
    return unless len < self.class.min_len || len > self.class.max_len

    errors.add(:ends, :invalid, message: I18n.t(
      'activerecord.errors.models.term.length',
      starts:, ends:,
    ))
    
  end
end

