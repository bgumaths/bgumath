# frozen_string_literal: true

require 'sti_preload'

class RoleEvent < Base
  include StiPreload

  belongs_to :role, inverse_of: :events
  belongs_to :user, optional: true
  belongs_to :creator, class_name: 'User', optional: true

  validates :type, inclusion: { in: %w[RoleAdded RoleRemoved] }

  after_create :email_update, if: :role_global?

  scope :global, -> { joins(:role).where(roles: { resource_type: nil }) }

  def user_email=(email)
    self.user = User.find_by(email:)
  end

  def creator_email=(email)
    self.creator = User.find_by(email:)
  end

  delegate :global?, to: :role, prefix: true

  protected

  def email_update
    Admin::RoleMailer.role_changed(self).deliver_later
  end
end

