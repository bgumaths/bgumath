# frozen_string_literal: true

class Shnaton < Base
  resourcify
  belongs_to :ac_year, validate: true, optional: false
  has_many :course_lists, dependent: :destroy, autosave: true
  ordered :has_many, :degrees, dependent: :destroy, autosave: true
  after_create :create_special_lists

  translates :preamble

  validates :ac_year, uniqueness: true

  amoeba { enable }

  default_scope { joins(:ac_year).order('ac_years.year DESC') }

  scope :for_years, ->(yy = AcYear.current) {
    where ac_year: AcYear.current(yy)
  }

  def self.for_year(yy = AcYear.current)
    for_years(yy).first
  end

  def self.recent
    limit(1).take
  end

  def year
    ac_year.try(:year)
  end

  def ac_year=(arg)
    unless arg.is_a?(AcYear)
      arg = AcYear.find(arg.to_s)
    end
    super
  end

  def clone_shnaton_relations!(cloned)
    return unless cloned

    self.degrees_order = []
    cloned.degrees.each do |deg|
      next unless (newdeg = degrees.find_by(name_i18n: deg.name_i18n))

      logger.debug("Updating relations for degree '#{deg.name}'")
      degrees_order << newdeg.id
      newdeg.clone_shnaton_relations!(deg)
    end
  end

  def create_special_lists
    AdvancedCourseList.find_or_create_by(shnaton: self)
  end

  def summary(meth = nil, *args, **opts)
    mth = if meth
            (if meth.is_a?(Symbol)
               ->(pp, *aa) { pp.try(meth, *aa) }
             else
               meth
             end)
          else
            ->(pp, *_aa) { pp }
          end
    by_prereq = {}
    by_degree = {}
    order = 0
    # don't use find_each, since it does not preserve order
    degrees.all.each do |degree| # rubocop:disable Rails/FindEach
      by_degree[degree] ||= {}
      degree.prereqs.all.each do |prereq| # rubocop:disable Rails/FindEach
        by_prereq[prereq.slug] ||= {
          order: order += 1,
          tip: prereq.description,
        }
        val = mth.(prereq, *args)
        if val.present?
          by_prereq[prereq.slug][degree] =
            by_degree[degree][prereq.slug] = val
        end
      end
      by_degree.delete(degree) unless
        by_degree[degree].present? || opts[:keepempty]
    end
    [by_prereq, by_degree]
  end
end

