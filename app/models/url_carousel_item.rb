# frozen_string_literal: true

class UrlCarouselItem < CarouselItem
  validates :url, presence: true
end

