# frozen_string_literal: true

class IssuesUserSession < BaseUserSession
  cookie_key 'issues_user_credentials'
end

