# frozen_string_literal: true

class IssuesStudentRep < IssuesUser
  resourcify
  has_many :courses, inverse_of: :student_rep, foreign_key: :student_rep_id,
                     dependent: :nullify

  def self.mail_domain
    'post.bgu.ac.il'
  end

  has_many :issues, inverse_of: :reporter, foreign_key: :reporter_id,
                    dependent: :restrict_with_error

  scope :current, ->(term = Term.current) {
    left_outer_joins(:courses).distinct.where(courses: { term: })
  }

  scope :not_current, ->(term = Term.current) {
    left_outer_joins(:courses).distinct.where.not(
      id: Course.where(term:).pluck(:student_rep_id),
    )
  }
end

