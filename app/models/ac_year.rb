# frozen_string_literal: true

class AcYear < Base
  resourcify

  has_many :added_courses, class_name: 'GenericCourse',
                           inverse_of: :year_added, dependent: :nullify
  has_many :removed_courses, class_name: 'GenericCourse',
                             inverse_of: :year_removed, dependent: :nullify

  def self.non_slugged
    true
  end

  extend FriendlyId
  friendly_id :year

  def self.current(year = CommonUtil.cur_year)
    year.is_a?(AcYear) ? year : find_or_create_by(year:)
  end

  def self.next_avail(since = CommonUtil.cur_year)
    yy = where(year: since..).order(year: :desc).first
    yy ? yy.year + 1 : since
  end

  validates :year, presence: true

  has_one :shnaton, dependent: :nullify

  default_scope { order(year: :desc) }

  def to_i
    year
  end

  def fall
    Term.fall(year)
  end

  def spring
    Term.spring(year)
  end
end

