# frozen_string_literal: true

class Adjunct < Academic
  def self.policy_class
    AdjunctPolicy
  end

  def self.mail_domain
    'post.bgu.ac.il'
  end
end

