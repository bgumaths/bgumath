# frozen_string_literal: true

## frozen_string_literal: true

class IssuesIssuesStaff < Base
  belongs_to :issue
  belongs_to :staff, class_name: 'IssuesStaff'
end

