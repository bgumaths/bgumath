# frozen_string_literal: true

class Department < Base
  resourcify
  include Namable

  has_many :departments_generic_courses, inverse_of: :department,
                                         dependent: :destroy
  has_many :courses, class_name: 'GenericCourse',
                     through: :departments_generic_courses
  has_many :people, class_name: 'IssuesStaff', dependent: :nullify

  validates :catalog_id, uniqueness: true
  validates :slug, uniqueness: true

  def cur_courses(date = Date.current)
    acourses.current(date)
  end

  def acourses
    Course.for_dept(self)
  end

  def term_courses(term = Term.current)
    acourses.where(term:)
  end

  def self.mir_health(base = self, format: ->(cc) { "- #{cc}" }, examined: '')
    list = base.where.missing(:people)
    res = if list.present?
            [
              '',
              '### The following departments are missing staff members:',
              '',
              *list.map { |c| format.(c) }
            ]
          else
            ['**No problems found!**']
          end
    res.push(
      '',
      "### The following departments were examined:#{examined}",
      '',
      *base.all.map { |c| format.(c) },
      examined.present? ? '{: .collapsible .collapsible-departments .collapse}' : '',
    )
    res.join("\n")
  end
end

