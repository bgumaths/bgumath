# frozen_string_literal: true

class Course < Base
  resourcify
  include Termed
  include Namable
  attaches :descfiles
  translates :title, :content, :abstract

  after_create :create_carousel_item, if: :advanced

  belongs_to :generic_course, inverse_of: :courses
  delegate :graduate, :shid, :name, :name_i18n, :description,
           :advanced, :description_i18n, :lectures, :exercises, :credits,
           :service?, :organic?, :first_year?, :advanced_undergraduate?, 
           :level, :shnaton_url, to: :generic_course, allow_nil: true
  # lecturer is optional, so that we may create the course when the lecturer 
  # is not yet known
  # with scope, gives PG::UndefinedTable with Course.where.missing(:lecturer) 
  # TODO
  belongs_to :lecturer, #-> { User.lecturers },
             class_name: 'Academic', inverse_of: :courses, optional: true

  # issues stuff
  belongs_to :student_rep, class_name: 'IssuesStudentRep', optional: true,
                           inverse_of: :courses
  has_one :aguda_rep, through: :generic_course
  has_many :departments, through: :generic_course
  has_many :issues_staffs, through: :generic_course
  has_many :issues, dependent: :nullify

  amoeba { enable }

  scope :grad, -> {
    joins(:generic_course).where(generic_courses: { graduate: true })
  }
  scope :undergrad, -> {
    joins(:generic_course).where(generic_courses: { graduate: false })
  }
  scope :advanced, -> {
    joins(:generic_course).where(generic_courses: { advanced: true })
  }
  scope :basic, -> {
    joins(:generic_course).where(generic_courses: { advanced: false })
  }
  scope :with_generic_course, -> {
    joins(:generic_course).select('courses.*, generic_courses.shid')
  }
  scope :organic, -> {
    with_generic_course.merge(GenericCourse.organic).distinct
  }
  scope :service, -> {
    with_generic_course.merge(GenericCourse.service).distinct
  }
  scope :first_year, -> {
    with_generic_course.merge(GenericCourse.first_year).distinct
  }
  scope :advanced_undergrad, -> {
    with_generic_course.merge(GenericCourse.advanced_undergrad).distinct
  }

  scope :issuable, -> {
    joins(:generic_course).merge(GenericCourse.issuable).distinct
  }

  scope :for_dept, ->(dept) { where(generic_course: dept.courses) }

  scope :by_shid, -> {
    includes(:generic_course).order('generic_courses.shid')
  }

  scope :where_generic, ->(cond) {
    joins(:generic_course).where(generic_courses: cond)
  }

  scope :recent, ->(count: 10) {
    joins(:term).order('terms.starts DESC').limit(count)
  }

  validates :generic_course, uniqueness: { scope: :term }
  validates :title_i18n, exclusion: { in: [nil] }
  validates :content_i18n, exclusion: { in: [nil] }

  def has_issues?
    %i[student_rep aguda_rep issues_staff issues].any? do |mm|
      try(mm).present?
    end
  end

  def to_s
    shid
  end

  def lecturer_email
    lecturer.try(:email)
  end

  def lecturer_email=(email)
    self.lecturer = User.find_by(email:)
  end

  def student_rep_email
    student_rep.try(:login)
  end

  def student_rep_email=(email)
    self.student_rep = IssuesStudentRep.find_by(login: email)
  end

  def aguda_rep_email
    aguda_rep.try(:login)
  end

  def aguda_rep_email=(email)
    self.aguda_rep = IssuesAgudaRep.find_by(login: email)
  end

  def shid=(shid)
    gc = GenericCourse.find_by(shid:)
    if gc
      self.generic_course = gc
    else
      errors.add(:shid, :not_found, shid:)
    end
    self.shid
  end

  # details parsed from the shnaton (require web access to the actual
  # catalogue.  see lib/course_catalog.rb). If full is true, include also
  # more complete details from the generic course
  def shnaton_info(full = false)
    (full ? generic_course.shnaton_info : {}).merge(
      generic_course.shnaton_for_term(term),
    )
  end

  def to_find_param(...)
    { term_id: term.slug }.merge(super)
  end

  # initialise field from the shnaton
  # we return:
  # - the list of sections if there is more than one lecturer
  # - The lecturer name if there is only one lecturer (perhaps in more than
  # one section). In this case, we also set the lecturer and possibly the
  # hours fields of self, if possible.
  def init_from_shnaton!(info = nil)
    info ||= generic_course.try(:shnaton_for_term, term)
    return nil if info.blank?

    sections = info[:groups].values.select { |x| x[:kind] == 'שעור' }
    lects = sections.pluck(:lecturer).uniq
    if lects.count == 1
      self.lecturer = User.lecturers.find_by_approx_name(lects[0]).first if
        lecturer.blank?
      if sections.count == 1 && hours.blank?
        hrs = sections[0][:meetings]
              .reject { |x| x[:time].blank? }
              .map do |x|
          x[:time].to_s +
            (x[:place].blank? ? '' : " #{I18n.t(:in, what: x[:place])}")
        end
        self.hours =
          ((hrs.count > 1) ? hrs.map { |x| "- #{x}" } : hrs).join("\n")
      end
      return lects[0]
    end
    sections
  end

  # if all else fails, find the course by shid
  def self.find_gsrow(row, **opts)
    res = super
    return res if res
    return nil if row[:shid].blank?

    where_generic(shid: row[:shid]).where_term(slug: opts[:args][0]).first
  end

  def some_info(loc = I18n.locale)
    abstract_i18n[loc.to_s].presence || description_i18n[loc.to_s]
  end

  def to_slug
    generic_course&.slug || generic_course&.to_slug
  end

  # changes to the generic course might affect this
  def cache_key
    [super, generic_course.cache_key]
  end

  def self.mir_health(base = this_year.issuable, format: ->(cc) { "- #{cc}" }, examined: '')
    nolec = base.where.missing(:lecturer)
    nostr = base.where.missing(:student_rep)
    res = []
    [
      [nolec - nostr, 'have no lecturer assigned'],
      [nostr - nolec, 'have no student rep assigned'],
      [nolec.where.missing(:student_rep),
       'have neither lecturer nor student rep']
    ].each do |list, msg|
      next if list.blank?

      res.push(
        '', "### The following active courses #{msg}:", '',
        *list.map { |c| format.(c) }
      )
    end
    res = ['**No problems found!**'] if res.blank?
    res.push(
      '', "### The following courses were examined:#{examined}", '',
      *base.all.map { |c| format.(c) },
      examined.present? ? '{: .collapsible .collapsible-courses .collapse}' : ''
    )
    res.join("\n")
  end
end

