# frozen_string_literal: true

class BaseEvent < Base
  self.abstract_class = true

  scope :desc, -> { order(duration: :desc) }
  scope :asc, -> { order(duration: :asc) }

  class << self
    def default_scope
    end

    def before(twhen = Date.current.beginning_of_day)
      twhen ? where('lower("duration") < ?::timestamp', twhen) : null_scope
    end

    def end_before(twhen = Date.current.beginning_of_day)
      twhen ? where('upper("duration") < ?::timestamp', twhen) : null_scope
    end

    def from(twhen = Date.current.beginning_of_day)
      twhen ? where('lower("duration") >= ?::timestamp', twhen) : null_scope
    end

    def end_from(twhen = Date.current.beginning_of_day)
      twhen ? where('upper("duration") >= ?::timestamp', twhen) : null_scope
    end

    # from collides with relation/query_methods
    def from_time(twhen = Date.current.beginning_of_day)
      from(twhen)
    end

    def between(starts, ends)
      from(starts).try(:before, ends) || null_scope
    end

    def end_between(starts, ends)
      end_from(starts).try(:end_before, ends) || null_scope
    end

    def on(twhen = Date.current)
      between(twhen.beginning_of_day, twhen.beginning_of_day + 1.day)
    end

    def end_on(twhen = Date.current)
      end_between(twhen.beginning_of_day, twhen.beginning_of_day + 1.day)
    end

    def this_week(...)
      between(CommonUtil.week_start(...), CommonUtil.week_end(...))
    end

    def end_this_week(...)
      end_between(CommonUtil.week_start(...), CommonUtil.week_end(...))
    end

    def year(y = CommonUtil.cur_year)
      between(Date.new(y - 1, CommonUtil::YEAR_START, 1),
              Date.new(y, CommonUtil::YEAR_START, 1))
    end

    def end_year(y = CommonUtil.cur_year)
      end_between(Date.new(y - 1, CommonUtil::YEAR_START, 1),
                  Date.new(y, CommonUtil::YEAR_START, 1))
    end
  end

  # CORONA
  CDATE = Date.new(2020, 3, 1)
  CREGULAR = Date.new(2020, 9, 1)

  def cancelled?
    online.blank? && starts(true) > CDATE && starts(true) < CREGULAR
  end

  def future?(now = Time.zone.now)
    starts(true) >= now
  end

  def past?(now = Time.zone.now)
    ends(true) <= now
  end
end

