# frozen_string_literal: true

class CourseListsProgPrereq < Base
  belongs_to :course_list
  belongs_to :prereq, class_name: 'ProgPrereq'
  validates :course_list_id, uniqueness: { scope: :prereq_id }
end

