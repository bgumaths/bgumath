# frozen_string_literal: true

class Degree < Base
  resourcify
  # one day this may become an STI, for now it isn't
  self.inheritance_column = :kind

  ordered :has_many, :prereqs, class_name: 'ProgPrereq', inverse_of: :degree,
                               dependent: :destroy, autosave: true

  has_many :plans, class_name: 'DegreePlan', inverse_of: :degree,
                   dependent: :destroy, autosave: true

  include ModelShnatonPart
  include Namable
  translates :description

  enum type: %w[single dual double special].index_by(&:to_sym)

  amoeba { enable }

  # including subreqs
  def all_prereqs
    # Following doesn't work,
    # https://github.com/take-five/activerecord-hierarchical_query/issues/21
    #ProgPrereq.including_subreqs(prereqs)
    # This is from
    # https://github.com/rails/rails/issues/20077#issuecomment-266427441
    ProgPrereq.including_subreqs(ProgPrereq.where(degree_id: self))
  end

  def clone_shnaton_relations!(old)
    # shnaton is our new shnaton, which we may need since it is not yet saved
    if old.is_a?(Shnaton)
      old = old.degrees.find_by(name_i18n:)
    end
    return unless old.is_a?(self.class)

    self.prereqs_order = []
    old.prereqs.each do |prereq|
      next unless (newpre = prereqs.find_by(name_i18n: prereq.name_i18n))

      prereqs_order.push(newpre.id)
      logger.debug("  Updating relations for requirement '#{prereq.name}'")
      newpre.clone_shnaton_relations!(prereq)
    end
  end
end

