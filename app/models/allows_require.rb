# frozen_string_literal: true

class AllowsRequire < Base
  validate :noself

  belongs_to :require, class_name: 'GenericCourse'
  belongs_to :allow, class_name: 'GenericCourse'

  validates :require, uniqueness: { scope: :allow_id }
  validates :allow, uniqueness: { scope: :require_id }

  protected

  def noself
    return unless require_id == allow_id

    errors.add :generic_course, :invalid,
               message: 'A course cannot depend on itself'
  end
end

