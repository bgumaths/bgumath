# frozen_string_literal: true

class DepartmentsGenericCourse < Base
  belongs_to :course,
             class_name: 'GenericCourse', foreign_key: 'generic_course_id',
             inverse_of: :departments_generic_courses
  belongs_to :department
end

