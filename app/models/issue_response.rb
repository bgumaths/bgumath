# frozen_string_literal: true

class IssueResponse < Base
  belongs_to :issue, inverse_of: :response
  belongs_to :creator, class_name: 'Regular'
  # a hardcopy of the final published issue. Attached here, since we cannot 
  # attach to uuid
  has_one_attached :hardcopy
  def self.dropped_relations
    %i[hardcopy_attachment hardcopy_blob]
  end

  validates :content, presence: true
  validates :content, frozen: true, if: :published?

  after_update :save_pdf, if: :published?

  def has_content?
    content.present?
  end

  def published?
    published_at.present?
  end

  def pending?
    persisted? && !published?
  end

  def replied_at
    created_at
  end

  def publish!(val = Time.zone.now)
    self.published_at ||= val
    save
  end

  def held?
    (Time.zone.now - replied_at) < publish_delay.hours
  end

  # delay from response to publish, in hours
  def publish_delay
    24
  end

  delegate :save_pdf, to: :issue
end

