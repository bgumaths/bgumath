# frozen_string_literal: true

require 'active_record/hierarchical_query'
class ProgPrereq < Base
  resourcify
  extend FriendlyId
  friendly_id :to_slug, use: :scoped, scope: :degree
  belongs_to :degree, inverse_of: :prereqs, touch: true, optional: true
  has_many :subreqs, class_name: 'ProgPrereq', inverse_of: :parent,
                     foreign_key: :parent_id, dependent: :destroy,
                     autosave: true
  belongs_to :parent,
             class_name: 'ProgPrereq', inverse_of: :subreqs,
             touch: true, optional: true

  has_many :course_lists_prog_prereqs,
           dependent: :destroy, inverse_of: :prereq, foreign_key: :prereq_id
  ordered :has_many, :course_lists, ->(obj) {
    acond = "array_position(
             ARRAY#{obj.course_lists_order.compact}::bigint[],\
             course_lists.id)"
    where(shnaton: obj.shnaton)
      .select("course_lists.*, #{acond}").distinct
      .order(Arel.sql(acond))
  }, through: :course_lists_prog_prereqs

  def shnaton
    parent ? parent.shnaton : degree&.shnaton
  end

  delegate :ac_year, to: :shnaton

  scope :top_level, -> { where(Arel.sql('parent_id IS NULL')) }

  scope :unattached, -> { top_level.where(Arel.sql('degree_id IS NULL')) }

  scope :including_subreqs, ->(starts) {
    join_recursive do
      start_with(starts).connect_by(id: :parent_id)
    end
  }

  scope :precise, -> { where(type: 'PreciseProgPrereq') }

  def with_subreqs
    self.class.including_subreqs(id:)
  end

  include Namable

  amoeba do
    enable
    exclude_association :course_lists
    propagate :relaxed
  end

  def clone_shnaton_relations!(old)
    if old.is_a?(Degree)
      old = old.prereqs.find_by(name_i18n:)
    end
    return unless old.is_a?(self.class)

    self.course_lists_order = []
    old.course_lists.each do |list|
      next unless (newcl = shnaton.course_lists.find_by(
        name_i18n: list.name_i18n,
      ))

      logger.debug("    Adding course list '#{list.name}'")
      course_lists.push(newcl)
      course_lists_order.push(newcl.id)
    end
    old.subreqs.find_each do |sr|
      nsr = subreqs.find_by(name_i18n: sr.name_i18n)
      next unless nsr

      logger.debug("    Adding sub-requirement '#{nsr.name}'")
      nsr.clone_shnaton_relations!(sr)
    end
  end

  translates :description

  # we have to override the model name for subclasses, so that form_path
  # works correctly
  #def self.model_name
  #  ActiveModel::Name.new(self, nil, 'ProgPrereq')
  #end

  # ... and as a result we need to define policy_class, so that pundit finds
  # the correct policy )-:
  def self.policy_class
    "#{ActiveModel::Name.new(self)}Policy"
  end
end

