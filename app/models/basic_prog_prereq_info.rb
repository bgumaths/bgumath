# frozen_string_literal: true

class BasicProgPrereqInfo < ProgPrereqInfo
  info_of 'BasicProgPrereq', field: :prereq
end

