# frozen_string_literal: true

class IssuesStaff < IssuesUser
  resourcify
  belongs_to :department, inverse_of: :people
  has_many :generic_courses, through: :department, source: :courses
  has_many :issues_issues_staffs, dependent: :destroy, inverse_of: :staff,
                                  foreign_key: :staff_id
  has_many :issues, through: :issues_issues_staffs

  def courses
    department ? department.acourses : Course.none
  end

  def fall_courses(...)
    department&.term_courses(Term.fall(...))
  end

  def spring_courses(...)
    department&.term_courses(Term.spring(...))
  end
end

