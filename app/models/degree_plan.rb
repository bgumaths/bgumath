# frozen_string_literal: true

class DegreePlan < Base
  resourcify
  belongs_to :degree, inverse_of: :plans
  has_many :term_plans, dependent: :destroy, autosave: true
  has_one :shnaton, through: :degree
  has_one :ac_year, through: :shnaton

  include Namable
  translates :remarks

  amoeba { enable }

  def initialize(...)
    super
    return if term_plans.present?

    self.term_plans = []
    [1, 2, 3].each do |year|
      term_plans.push(TermPlan.new(year:, term: :fall))
      term_plans.push(TermPlan.new(year:, term: :spring))
    end
  end

  def next_term
    if (last = term_plans.last)
      last.next_term
    else
      { year: 1, term: :fall }
    end
  end
end

