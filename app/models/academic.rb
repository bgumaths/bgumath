# frozen_string_literal: true

class Academic < User
  #self.abstract_class = true

  has_many :courses, inverse_of: :lecturer, foreign_key: 'lecturer_id',
                     dependent: :nullify

  has_many :members_research_groups,
           dependent: :destroy, inverse_of: :member, foreign_key: :member_id
  has_many :research_groups, through: :members_research_groups

  protected

  def init
    super
    self.rank ||= 'Dr' if new_record?
  end
end

