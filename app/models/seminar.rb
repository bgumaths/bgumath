# frozen_string_literal: true

class Seminar < Base
  resourcify
  include Shiftable
  include Termed

  amoeba { enable }

  translates :name, :description
  has_many :research_groups_seminars, dependent: :destroy
  has_many :research_groups, through: :research_groups_seminars

  has_many :meetings, dependent: :destroy

  validates :day, inclusion: { in: 0..6 }
  validates :name_i18n, trasto_weak_presence: true
  # TODO: we actually want to verify each locale separately
  validates :list_id, format: { with: /\A\S*\Z/ }

  @@colloqname = 'Colloquium'

  after_create :create_carousel_item

  #def self.default_scope
  #  order(Arel.sql("name_i18n->'#{I18n.locale}'"))
  #end

  scope :colloq, ->(colloq = @@colloqname) {
    where "name_i18n->>'en' = ?", colloq
  }
  scope :regular, ->(colloq = @@colloqname) {
    where "name_i18n->>'en' != ?", colloq
  }

  def is_colloq(colloq = @@colloqname)
    name_i18n['en'] == colloq
  end

  def is_regular(colloq = @@colloqname)
    !is_colloq(colloq)
  end

  # the next standard date for a meeting, no earlier than the given date.
  def next_date(date = Time.zone.today)
    date + ((day - date.wday) % 7).days
  end

  # return available dates in the given range, or starting from the given
  # date
  def available(range = Time.zone.today, one_per_week = false)
    unless range.respond_to?(:begin)
      range = [term.starts, range].max..term.ends
    end
    result = []
    (next_date(range.begin)..range.end).step(7) do |d|
      result.push(d) if (one_per_week ? meetings.this_week(d) : meetings.on(d)).blank?
    end
    result
  end

  # return the first available date for a meeting. Only dates before `before` 
  # are taken into account, where the default value of `before` is the 
  # starting date of the next term that has this seminar
  def first_available(starting = Time.zone.today, one_per_week: false,
                      before: nil, step: 7.days)
    if before.blank?
      nterm = next_terms.first
      before = nterm.present? ? nterm.starts : Float::INFINITY
    end
    on_date = if one_per_week
                ->(d) { meetings.this_week(d) }
              else
                ->(d) { meetings.on(d) }
              end
    result = next_date([term.starts, starting].max)
    result += step while on_date.(result).present? && result < before
    (result < before) ? result : nil
  end
 
  def default_date(date = Time.zone.today, **)
    first_available(date, **) || next_date(term.starts)
  end

  extend FriendlyId
  friendly_id :to_slug, use: %i[scoped history], scope: :term

  def to_slug
    name_i18n['en']
  end

  def self.csv_id
    @csv_id ||= :name_i18n
  end

  def has_carousel_content?
    meetings.from_time.present?
  end

  # create an email asking to create a mailing list
  def create_ml_email(**)
    MailingListMailer.create(list_def,
                             admin_users.first&.decorate&.email_address(true), **)
  end

  def list_def
    list_id.presence || slug
  end
end

