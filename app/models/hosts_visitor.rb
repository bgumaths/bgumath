# frozen_string_literal: true

class HostsVisitor < Base
  belongs_to :visitor
  belongs_to :host, -> { where(status: %w[Regular Kamea]) },
             class_name: 'Member', inverse_of: :hosts_visitors
end

