# frozen_string_literal: true

class PreciseProgPrereqInfo < ProgPrereqInfo
  info_of 'PreciseProgPrereq', field: :prereq
end

