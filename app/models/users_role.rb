# frozen_string_literal: true

class UsersRole < Base
  belongs_to :user, -> { alive } # rubocop:disable Rails/InverseOf
  belongs_to :role
end

