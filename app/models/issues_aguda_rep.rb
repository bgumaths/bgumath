# frozen_string_literal: true

class IssuesAgudaRep < IssuesUser
  resourcify
  has_many :generic_courses, inverse_of: :aguda_rep,
                             foreign_key: :aguda_rep_id, dependent: :nullify
  has_many :courses, through: :generic_courses
  has_many :issues, inverse_of: :aguda_rep, dependent: :nullify,
                    foreign_key: :aguda_rep_id

  def mail_domain
    'aguda.bgu.ac.il'
  end
end

