# frozen_string_literal: true

class ResearchGroup < Base
  resourcify
  extend FriendlyId
  friendly_id :to_slug

  default_scope { order(name: :asc) }

  def to_slug
    name
  end

  has_many :members_research_groups, dependent: :destroy
  has_many :members, through: :members_research_groups
  # TODO: this should be a list of slugs rather than an association, since we 
  # assume seminars with the same slug are the same (or maybe add 
  # "generic_seminar")
  has_many :research_groups_seminars, dependent: :destroy
  has_many :seminars, through: :research_groups_seminars

  validates :name, presence: true
  validates :name, length: { maximum: NAME_MAX_LEN }

  def member_emails=(emails)
    self.members = User.where(email: emails)
  end
end

