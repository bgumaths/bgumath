# frozen_string_literal: true

class IssuesRoleSetup < Base
  belongs_to :app_setup
  belongs_to :role
end

