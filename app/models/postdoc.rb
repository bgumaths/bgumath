# frozen_string_literal: true

class Postdoc < Academic
  has_many :hosts_postdocs, inverse_of: :postdoc, dependent: :destroy
  has_many :hosts, through: :hosts_postdocs

  def host_emails=(emails)
    self.hosts = User.where(email: emails)
  end

  def self.policy_class
    PostdocPolicy
  end
end

