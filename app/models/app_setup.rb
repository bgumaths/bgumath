# frozen_string_literal: true

class AppSetup < Base
  translates :depname, :fdepname, :department,
             :university, :building, :dept_snail

  has_many :chair_role_setups, dependent: :destroy
  has_many :chair_roles, through: :chair_role_setups, source: :role
  has_many :staff_role_setups, dependent: :destroy
  has_many :staff_roles, through: :staff_role_setups, source: :role
  has_many :issues_role_setups, dependent: :destroy
  has_many :issues_roles, through: :issues_role_setups, source: :role

  class << self
    def respond_to_missing?(method, *)
      has_attr?(method.to_s) || super
    end

    def method_missing(method, ...)
      has_attr?(method.to_s) ? _instance.public_send(method, ...) : super
    end

    def _instance
      @_instance ||= last
    end

    delegate :decorate, to: :_instance

    protected

    def has_attr?(attr)
      att = attr.to_s
      @has_attr ||= {}
      @has_attr[att] ||=
        attr_names.include?(att) || attr_names.include?("#{att}_i18n") ||
        reflections[att] || %w[save save!].include?(att)
    end
  end
end

