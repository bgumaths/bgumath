# frozen_string_literal: true

class PreciseProgPrereq < ProgPrereq
  delegate_info inverse: :prereq

  extend FriendlyId
  friendly_id :to_slug, use: :scoped, scope: :degree
end

