# frozen_string_literal: true

## frozen_string_literal: true

class CourseListsCourse < Base
  belongs_to :course_list
  belongs_to :course, class_name: 'GenericCourse'
end

