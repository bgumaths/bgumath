# frozen_string_literal: true

# we expect the model to define `login`, `crypted_password` and 
# `update_bgu_details`

module Authen
  extend ActiveSupport::Concern

  included do
    has_paper_trail ignore: %i[
      last_request_at current_login_at last_login_at
      current_login_ip last_login_ip failed_login_count
    ]
  end

  ### password authentication
  
  def authen_bgu(pass_id, **opts)
    logger.info("Authenticating #{self.class.name} #{login} via bgu service")
    pass, id = pass_id.split(';')
    unless id
      logger.info("Password contained no ID for #{login}")
      return
    end
    
    require 'bgu/user'
    bu_opts = opts[:bu] || {}
    @bgu_user = Bgu::User.new(login, id, **bu_opts)
    res = @bgu_user.auth?(pass)
    if res
      update_bgu_details
    else
      logger.warn(@bgu_user.error.to_s)
      if @bgu_user.error.is_a?(Faraday::SSLError) && !opts[:noretry]
        logger.warn('  Retrying without ssl verification...')
        res = authen_bgu(
          pass_id, **opts.deep_merge(
            noretry: true, bu: { faraday: { ssl: { verify: false } } },
          )
        )
      end
    end
    res
  end

  def update_detail(name, cur, val)
    if cur.present?
      if cur != val
        logger.warn(
          "#{name} mismatch for #{login}: we have '#{cur}', but bgu has '#{val}' (keeping our value)",
        )
      end
      false
    else
      update_detail!(name, cur, val)
    end
  end

  def update_detail!(name, cur, val)
    return false if cur == val

    logger.info("Updating #{name} '#{val}' for #{login}")
    yield val
    true
  end

  def authen_native(pass)
    logger.info(
      "Authenticating #{self.class.name} #{login} via local password",
    )
    # check current object, not saved version
    crypted_password.present? && valid_password?(pass, false)
  end

  module ClassMethods
    def auth_methods
      %w[native bgu]
    end
  end

  def authen?(pass)
    pass.present? &&
      self.class.auth_methods.any? { |meth| try("authen_#{meth}", pass) }
  end

  ### api keys

  def create_api_key(**)
    res = SecureRandom.uuid.tr('-', '_')
    update!(api_key: self.class.hash_api_key(res, **))
    res
  end

  class_methods do
    def hash_api_key(key, **opts)
      require 'scrypt'
      SCrypt::Engine.hash_secret(
        key, opts[:salt] || Rails.application.credentials.api[:key_salt]
      )
    end

    def find_by_api_key(key, **opts)
      find_by(api_key: hash_api_key(key, **opts))
    end
  end
end

