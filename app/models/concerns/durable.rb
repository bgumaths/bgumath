# frozen_string_literal: true

module Durable
  extend ActiveSupport::Concern
  include GenericDuration

  def default_single
    DateTime.current.in_time_zone
  end

  def to_single(arg)
    arg.try(:to_time).try(:in_time_zone)
  rescue ArgumentError
    nil
  end

  # allow argument for compatibility with shiftable
  def starts(_full = false)
    duration.try(:begin).try(:in_time_zone)
  end

  def ends(_full = false)
    duration.try(:end).try(:in_time_zone)
  end
end

