# frozen_string_literal: true

module GenericDuration
  # abstract mixin for a "duration"
  # expect the including class/module to define:
  # - to_single(arg): convert arg to an item in the duration range
  # - starts: return the beginning of the range
  # - ends: return the end of the range
  # - default_single: default value of element in the range

  extend ActiveSupport::Concern

  included do
    validates :duration, presence: true
    validates :starts, presence: true
    validates :ends, presence: true
    validates :ends, comparison: { greater_than: :starts }

    scope :ongoing, ->(dd = default_single) {
      where('lower("duration") <= ?::timestamp', dd)
        .where('upper("duration") >= ?::timestamp', dd)
    }
  end

  # convert value to start of range. Assume to_single converts to arbitrary
  # element in the range
  def to_start(arg)
    to_single(arg)
  end

  # convert value to end of range
  def to_end(arg)
    to_single(arg)
  end

  def duration=(wh)
    if wh.respond_to?(:split)
      # allow wh to be a string range
      ff, ll = wh.split('..')
      wh = { first: ff, last: ll } if ff && ll
    end
    if wh.respond_to?(:has_key?) && wh.key?(:first) && wh.key?(:last)
      wh = to_start(wh[:first])..to_end(wh[:last])
    end
    # if duration is given as a range, don't convert
    self[:duration] = wh
  end

  def starts=(t)
    tt = to_single(t)
    self.duration = tt..(ends || tt)
  end

  def ends=(t)
    tt = to_single(t)
    self.duration = (starts || tt)..tt
  end

  def current?(date = default_single)
    duration.include?(date)
  end
end

