# frozen_string_literal: true

module Shiftable
  extend ActiveSupport::Concern
  include Durable

  def starts(full = false)
    tod(duration.try(:begin), full)
  end

  def ends(full = false)
    tod(duration.try(:end), full)
  end

  def shift
    starts and ends ? Tod::Shift.new(starts, ends) : nil
  end

  def shift=(r)
    if r.blank?
      rr = [nil, nil]
    elsif r.respond_to?(:beginning) # Tod::Shift
      rr = [r.beginning, r.ending]
    elsif r.respond_to?(:begin) # range
      rr = [r.begin, r.end].map { |x| tod(x) }
    elsif r.respond_to?(:has_key?) &&
          r.key?(:beginning) && r.key?(:ending)
      rr = [r[:beginning], r[:ending]].map { |x| tod(x) }
    else
      raise "Bad shift assignment: #{r.inspect}; self: #{inspect}"
    end
    if  rr[0] && rr[1]
      # postgresql does not accept timepstamp without date
      d = date || Time.zone.today
      self.duration = rr[0].on(d)..rr[1].on(d)
    else
      self.duration = nil
    end
  end

  def date
    duration.respond_to?(:begin) ? duration.begin.try(:to_date) : nil
  end

  def date=(d)
    # init duration so that starts and ends work
    init_duration
    dd = d.try(:to_date)
    self.duration = starts.on(dd)..ends.on(dd) if dd
  end

  def starts=(t)
    ee = ends.try(:to_s) || t
    self.shift = { beginning: t, ending: ee }
  end

  def ends=(t)
    ee = starts.try(:to_s) || t
    self.shift = { beginning: ee, ending: t }
  end

  protected

  def tod(t, full = false)
    return nil if t.blank?

    res = begin
      Tod::TimeOfDay(t.try(:in_time_zone))
    rescue StandardError
      nil
    end
    full ? res&.on(t.to_date) : res
  end

  def init_duration
    now = Time.zone.now
    self.duration ||= now..now
  end
end

