# frozen_string_literal: true

module Tree
  extend ActiveSupport::Concern

  included do
    def root
      try(:parent).try(:root) || self
    end

    def to_treant(meth = :to_treant_node)
      node = if block_given?
               yield(self)
             else
               try(meth) || { text: { name: try(:to_s) || self } }
             end
      node[:children] = children.map(&:to_treant)
      node
    end
  end
end

