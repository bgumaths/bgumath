# frozen_string_literal: true

module Ddurable
  # date duration
  extend ActiveSupport::Concern
  include GenericDuration

  def default_single
    Time.zone.today
  end

  def to_single(arg)
    arg.try(:to_date)
  rescue ArgumentError
    nil
  end

  def to_start(arg)
    arg.blank? ? -Date::Infinity.new : to_single(arg)
  end

  def to_end(arg)
    arg.blank? ? Date::Infinity.new : to_single(arg)
  end

  # use max and min, since the range may be exclusive
  def starts(_full = false)
    to_single(duration.try(:min))
  rescue RangeError
    # startless
    nil
  end

  def ends(_full = false)
    to_single(duration.try(:max))
  rescue RangeError
    # endless
    nil
  end

  def starts=(t)
    tt = to_single(t)
    self.duration = tt..(duration.try(:max) || tt)
  end

  def ends=(t)
    tt = to_single(t)
    self.duration = (duration.try(:min) || tt)..tt
  end

  def ongoing?(dd = Time.zone.today)
    starts <= dd && dd <= ends
  end
end

