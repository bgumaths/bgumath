# frozen_string_literal: true

module Termed
  extend ActiveSupport::Concern

  included do
    belongs_to :term, optional: false

    scope :this_term, ->(tt = Term.default) {
      where term: tt
    }

    scope :other_term, ->(tt = Term.default) {
      this_term(tt.other)
    }

    scope :this_year, ->(tt = Term.default) {
      where term: [tt, tt&.other].select(&:presence)
    }

    scope :current, ->(date = Date.current) {
      this_term(Term.current(date))
    }

    scope :current_or_following, ->(date = Date.current) {
      this_term(Term.ends_after(date))
    }

    scope :by_term, -> {
      includes(:term).order('terms.starts ASC')
    }

    scope :where_term, ->(cond) { joins(:term).where(terms: cond) }
  end

  # terms where this exists
  def avail_terms(terms = Term)
    field = self.class.model_name.collection.to_sym
    terms.joins(field).where(field => { slug: }).order(starts: :asc)
  end

  def prev_terms
    avail_terms(term.before)
  end

  def next_terms
    avail_terms(term.after)
  end

  def current?(_date = Date.current)
    term == Term.default
  end

  protected

  def carousel_opts
    super.merge(activates: term.starts - 1.month,
                expires: term.starts + 1.month)
  end
end

