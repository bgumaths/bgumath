# frozen_string_literal: true

module SigmaGraph
  extend ActiveSupport::Concern

  included do
    def node_id
      @node_id ||= "#{(try(:object) || self).class}-#{id}"
    end

    def sigma_node_opts
      {}
    end

    def to_node(**opts)
      {
        id: node_id, label: try(:to_s) || try(:slug), size: 3,
      }.deep_merge(sigma_node_opts).deep_merge(opts)
    end

    def edge_id(ch)
      "#{node_id}--#{ch.node_id}"
    end

    def to_edge(ch, **opts)
      {
        id: edge_id(ch),
        source: ch.node_id,
        target: node_id,
        size: 0.1,
      }.deep_merge(opts)
    end

    def to_sig_graph(nodes = {}, edges = {},
                     children = ->(obj) { obj.children })
      if nodes[node_id].blank?
        node = nodes[node_id] = to_node
        children.(self).each do |ch|
          ch.to_sig_graph(nodes, edges, children)
          cnode = nodes[ch.node_id]
          node[:size] = cnode[:size] + 1 unless node[:size] > cnode[:size]
          edges[edge_id(ch)] = to_edge(ch)
        end
      end
      { nodes: nodes.values, edges: edges.values }
    end
  end
end

