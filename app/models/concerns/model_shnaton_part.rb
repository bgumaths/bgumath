# frozen_string_literal: true

module ModelShnatonPart
  extend ActiveSupport::Concern

  included do
    belongs_to :shnaton, optional: true
    #validates :shnaton, presence: true
    delegate :ac_year, to: :shnaton

    scope :for_years, ->(year = AcYear.current) {
      joins(:shnaton).where(shnaton: { ac_year: AcYear.current(year) })
    }
    scope :not_for_years, ->(year = AcYear.current) {
      joins(:shnaton).where.not(shnaton: { ac_year: AcYear.current(year) })
    }
  end
end

