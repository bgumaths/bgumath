# frozen_string_literal: true

module Namable
  extend ActiveSupport::Concern

  included do
    translates :name
    validates :name_i18n, trasto_presence: true

    extend FriendlyId
    if (bt = reflect_on_all_associations(:belongs_to).presence)
      friendly_id :to_slug, use: :scoped, scope: bt.map(&:name)
    else
      friendly_id :to_slug
    end

    def to_slug
      name_i18n['en']
    end

    scope :by_name, ->(loc = I18n.locale) {
      ff = "name_i18n->>'#{loc}'"
      reorder Arel.sql ff
    }
  end
end

