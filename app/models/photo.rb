# frozen_string_literal: true

class Photo < ApplicationRecord
  # The imageable is optional, since we create a photo during upload process, 
  # without attaching it
  belongs_to :imageable, polymorphic: true, optional: true

  has_one_attached :image
  delegate_missing_to :image
  # make sure that signed_id is delegated to blob, to get the right one
  delegate :filename, :signed_id, to: :blob

  before_validation :fix_crop

  validates :x, numericality: { greater_than_or_equal_to: 0 },
                allow_nil: true
  validates :y, numericality: { greater_than_or_equal_to: 0 },
                allow_nil: true
  validates :width, numericality: { greater_than: 0 }, allow_nil: true
  validates :height, numericality: { greater_than: 0 }, allow_nil: true

  validates_each :width do |rec, atr, val|
    mw = rec.metadata[atr] if rec.metadata.present?
    if val && mw && val + rec.x > mw
      rec.errors.add(
        atr, :invalid, message: 'Crop dimension must no exceed image dimension'
      )
    end
  end

  validates_each :height do |rec, atr, val|
    mh = rec.metadata[atr] if rec.metadata.present?
    if val && mh && val + rec.y > mh
      rec.errors.add(
        atr, :invalid, message: 'Crop dimension must no exceed image dimension'
      )
    end
  end

  def self.find_blob(id)
    ActiveStorage::Blob.find_signed!(id)
  rescue ActiveSupport::MessageVerifier::InvalidSignature => e
    logger.warn(e)
    nil
  end

  def self.find_signed(id)
    blb = find_blob(id)
    ActiveStorage::Attachment.find_by(blob: blb)&.record
  end

  def signed_id=(id)
    image.attach(self.class.find_blob(id))
  end

  # some AR methods call this with nil as first arg, so can't replace opts 
  # with **opts
  def initialize(opts = nil)
    opts ||= {}
    img = opts.delete(:image)
    img ||= self.class.find_blob(opts.delete(:signed_id)) if opts[:signed_id].present?
    super
    image.attach(img) if img.present?
  end
 
  def selected?
    imageable&.photo == self
  end

  def variant(**)
    return nil if image.blank?

    crop = (width && height) ? { crop: [x, y, width, height].map(&:to_i) } : {}
    image.variant(**crop, **)
  end

  def metadata
    return unless (blob = image&.blob)

    # can't analyze before it is saved, throws an error
    blob.analyze if blob.persisted? && !blob.analyzed?
    blob.metadata
  end

  protected
  
  # when saving for the first time, the blob might not be saved yet, so 
  # can't be analyzed and there are no dimension in the metadata
  def fix_crop
    self.x = 0 if x&.negative?
    self.y = 0 if y&.negative?
    return unless metadata

    mw = metadata[:width]
    mh = metadata[:height]
    self.width = mw - x if mw && width && (width <= 0 || width + x > mw)
    self.height = mh - y if mh && height && (height <= 0 || height + y > mh)
  end
end

