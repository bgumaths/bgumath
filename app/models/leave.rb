# frozen_string_literal: true

class Leave < BaseEvent
  resourcify
  include Ddurable
  translates :note
  belongs_to :user, -> { alive }, autosave: true, touch: true, validate: true,
                                  inverse_of: :leaves, optional: false

  default_scope { order(duration: :asc) }
end

