# frozen_string_literal: true

class FreePlanItem < Base
  resourcify
  belongs_to :term_plan
  has_one :ac_year, through: :term_plan
  has_one :shnaton, through: :term_plan
  has_one :degree, through: :term_plan
  has_one :degree_plan, through: :term_plan

  translates :title
end

