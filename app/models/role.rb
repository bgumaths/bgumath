# frozen_string_literal: true

class Role < Base
  has_many :users_roles, dependent: :destroy
  has_many :users, through: :users_roles

  belongs_to :resource, polymorphic: true, optional: true
  has_many :events, class_name: 'RoleEvent', inverse_of: :role,
                    dependent: :destroy

  scopify
  translates :title, :description

  # we need to keep name, since this is what rolify uses
  before_validation :update_name
  # don't save decorators
  def resource=(val)
    super
    fix_resource
  end

  before_save :fix_resource

  validates :name, presence: true, length: { maximum: NAME_MAX_LEN }
  validates :title_i18n, presence: true
  validates :description_i18n, presence: true

  # remove this validation, as it does not work with inheritance, and anyway
  # we resourcify the base
  #validates :resource_type,
  #          :inclusion => { :in => Rolify.resource_types },
  #          :allow_nil => true

  # permissive visibility should come later
  enum visibility: {auto: 0, hidden: 1, internal: 2, open: 3, staff: 4, head: 5}
  validates :visibility, presence: true
  #validates :description, length: { maximum: TEXT_MAX_LEN }

  scope :manual, -> { where.not(visibility: 'auto') }
  scope :global, -> { where(resource: nil) }
  scope :local, -> { where.not(resource: nil) }
  # copied from rolify, for some reason this is not in the public api
  scope :by_name, ->(name, resource = nil) {
    cond = { name: }
    if resource
      if resource.is_a?(Class)
        cond[:resource_type] = resource.to_s
      else
        cond[:resource_type] = resource.class.name
        cond[:resource_id] = resource.id
      end
    else
      cond[:resource_type] = nil
      cond[:resource_id] = nil
    end
    where(cond)
  }

  def self.admin
    by_name('Admin').take
  end

  def self.vice
    by_name('Department vice head').take
  end

  def to_args
    [name, resource]
  end

  # not an old role
  def nearby?(date = Date.current)
    global? || !resource.try(:term) || resource.term.nearby?(date)
  end

  def current?(date = Date.current)
    global? || resource.current?(date)
  end

  def global?
    !resource
  end

  protected

  def defaults
    self.title_i18n = { en: name, he: nil } if title_i18n.blank?
    true
  end

  def init
    super
    if name.present?
      locname = "roles.#{name.to_s.underscore}"
      I18n.available_locales.each do |loc|
        title_i18n[loc.to_s] ||= I18n.t locname, locale: loc, default: name
      end
    end
    self.description_i18n = { en: nil, he: nil } if description_i18n.blank?
    self.visibility ||= :open
  end

  def update_name
    self.name ||= title_i18n['en']
  end

  def fix_resource
    return unless (rt = resource_type&.safe_constantize)

    self.resource_type = rt.object_class if rt < Draper::Decorator
  end
end

