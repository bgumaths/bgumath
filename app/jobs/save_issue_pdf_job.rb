# frozen_string_literal: true

class SaveIssuePdfJob < ApplicationJob
  queue_as :default

  def perform(issue)
    @issue = issue.decorate
    pdf = LatexToPdf.generate_pdf(latex, {})
    require 'fileutils'
    FileUtils.mkdir_p dir
    out = File.join(dir, fname)
    File.write(out, pdf)
    # for some reason, the following puts Que into an infinite loop
    #if issue.response.present?
    #issue.response.hardcopy.attach(
    #io: File.open(out), filename: fname,
    #content_type: 'application/pdf', identify: false
    #)
    #end
    out
  end

  def latex
    @latex ||= I18n.with_locale :he do
      ActionController::Base.render formats: [:tex],
                                    layout: 'application', 
                                    template: 'teaching/issues/show',
                                    locals:
    end
  end

  attr_reader :issue

  delegate :course, to: :issue

  def locals
    res = { issue:, course:,
            show_response: issue.replied?, }
    %i[reporter lecturer student_rep replier reply departments aguda_rep 
       staff].each { |it| res[it] = issue.try(it) }
    res
  end

  def dir
    @dir ||= Rails.root.join('issues', course.term.slug)
  end

  def fname
    "#{issue.slug}.pdf"
  end
end

