# frozen_string_literal: true

class SendPublishReminderJob < ApplicationJob
  queue_as :default

  def perform(issue)
    return if issue.published?

    # it could happen that the issue was replied, this job was scheduled, 
    # then the reply was modified. In this case, we do nothing, and let the 
    # other job do its thing
    return if issue.response&.held?

    Teaching::IssueMailer.reminder(issue).deliver_now
  end
end

