# frozen_string_literal: true

class UserResource < ApplicationResource
  attribute :id, :string

  filter :id do
    eq do |scope, value|
      scope.where(id: value.map { |x| fix_value(x) })
    end
  end

  # only support true
  filter :lecturers, :boolean do
    eq do |scope, _value|
      scope.lecturers
    end
  end

  def self.dropped_relations
    super + %i[role_events]
  end

  include Durized

  defaults

  # add these here so that we don't get an exception for general users.  
  # Define this as `has_many` since Graphiti fails for fake many_to_many
  has_many :seminars, writable: false
  has_many :courses, writable: false
  has_many :research_groups, writable: false

  protected

  def special_users
    %w[me administrator chair coordinator director admin secretaries].freeze
  end

  def special?(who)
    special_users.include?(who.to_s)
  end

  def special_user(who)
    (who == 'me') ? [current_user] : [*User.try(who)]
  end

  def fix_value(sid)
    special?(sid) ? special_user(sid).first.try(:id) : sid
  end
end

