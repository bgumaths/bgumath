# frozen_string_literal: true

load 'app/resources/member_resource.rb'
class RegularResource < MemberResource
  load 'app/resources/student_resource.rb'
  defaults
end

