# frozen_string_literal: true

class IssueResource < ApplicationResource
  attribute :id, :uuid
  defaults

  after_attributes :set_reporter, only: %i[create]

  delegate :current_issues_user, to: :context

  protected

  def set_reporter(mod)
    mod.reporter ||= current_issues_user
  end
end

