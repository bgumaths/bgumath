# frozen_string_literal: true

class PhotoResource < ApplicationResource
  def self.decorator_attributes
    super.merge(url: :string)
  end
  defaults
end

