# frozen_string_literal: true

class StudentResource < AcademicResource
  secondary_endpoint '/students', %i[index show create update destroy]
  defaults
end

