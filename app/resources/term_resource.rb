# frozen_string_literal: true

class TermResource < ApplicationResource
  defaults

  attribute :title_i18n, :hash do
    year = @object.year
    I18n.available_locales.to_h do |loc|
      [loc.to_s,
       I18n.t(@object.part,
              scope: %i[teaching term name],
              year: I18n.t('time_will_tell.date_range.year_range',
                           from: year - 1, to: year % 100, locale: loc),
              locale: loc, default: '')]
    end
  end

  filter :ends, :date, single: true do
    gte do |scope, value|
      scope.ends_after(value)
    end
  end
end

