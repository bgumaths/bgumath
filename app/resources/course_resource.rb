# frozen_string_literal: true

class CourseResource < ApplicationResource
  # remove collision with departments.courses
  def self.dropped_relations
    super + %i[departments]
  end

  defaults

  include Termized

  %i[shid shnaton_url].each do |attr|
    attribute attr, :string
    filter attr do
      eq do |scope, value|
        scope.where_generic(attr => value)
      end
    end
  end

  filter :level, :string, allow:
  %w[basic advanced service first_year organic advanced_undergrad graduate] do
    eq do |scope, value|
      vals = [*value]
      res = scope.public_send(vals.shift)
      vals.each do |val|
        res = res.or(scope.public_send(val))
      end
      res
    end
  end
end

