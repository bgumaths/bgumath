# frozen_string_literal: true

READONLY_ATTRS = %i[created_at updated_at].freeze
ARRAY_ATTRS = %i[array_of_dates array_of_datetimes].freeze

# ApplicationResource is similar to ApplicationRecord - a base class that
# holds configuration/methods for subclasses.
# All Resources should inherit from ApplicationResource.
class ApplicationResource < Graphiti::Resource
  self.abstract_class = true
  self.endpoint_namespace = '/api/v1'
  # Use the ActiveRecord Adapter for all subclasses.
  # Subclasses can still override this default.
  self.adapter = Graphiti::Adapters::ActiveRecord
  self.base_url =
    "https://#{Rails.application.routes.default_url_options[:host]}"
  # interpret filter[foo]=null as nil rather than string
  self.filters_accept_nil_by_default = true

  ### automatic attributes and relations ###
  class << self
    # which attributes to use
    def model_attrs
      model_attribute_names.map(&:to_sym) - dropped_attributes.map(&:to_sym) -
        model_relation_names.map(&:to_sym)
    rescue Graphiti::Errors::ModelNotFound => e
      warn e
      []
    end

    delegate :attribute_names, to: :model, prefix: true

    # which relations to use
    def model_relation_names
      model.reflections.values.map(&:foreign_key)
    end

    # which attributes to drop
    def dropped_attributes
      Set[*%w[id type]]
    end

    # which relations to drop
    def dropped_relations
      Set[*%i[versions slugs resource object descfiles_blobs image_blob
              descfiles_attachments leaves image_attachment]] +
        (model.try(:dropped_relations) || [])
    end

    def default_polymorphic
      # `self.polymorphic = []` is not the same as omitting it, see
      # https://github.com/graphiti-api/graphiti/issues/199
      #return if model.descendants.blank?
      # if the class is not abstract, we do not want it to be polymorphic, 
      # since we want all items in its listing to have the same jsonapi type
      return unless abstract_class?

      self.polymorphic = model.descendants.map { |k| "#{k.name}Resource" }
    rescue Graphiti::Errors::ModelNotFound => e
      warn e
    end

    # assign default attributes
    def default_attributes
      model_attrs.each do |attr|
        type_opts = type_of(attr)
        # declare the attribute. We convert ranges to arrays of [first, 
        # last]
        if ARRAY_ATTRS.include? type_opts[0]
          attribute attr, *type_opts do
            res = @object.try(attr)
            [res.first, res.last]
          end
        else
          attribute attr, *type_opts
        end

        # policy methods
        define_method :"attr_readable_#{attr}?" do |mod|
          attr_readable?(attr, mod)
        end

        define_method :"attr_writable_#{attr}?" do
          attr_writable?(attr)
        end

        # filters for hash values
        if model.column_for_attribute(attr).type == :jsonb
          filter attr, :hash, single: true do
            eq do |scope, value|
              scope.where("#{attr}->>'#{value.keys[0]}' = :val",
                          val: value.values[0])
            end
            match do |scope, value|
              scope.where("#{attr}->>'#{value.keys[0]}' ~* :val",
                          val: value.values[0])
            end
          end
        end

        # trasto fields. We define a translated variant, using the current 
        # locale
        next unless attr.to_s =~ /(.*)_i18n$/

        tattr = $1.to_sym
        attribute tattr, :string, **opts_for_attr(tattr)
        define_method :"attr_readable_#{tattr}?" do |mod|
          attr_readable?(tattr, mod)
        end

        define_method :"attr_writable_#{tattr}?" do |mod|
          attr_writable?(tattr, mod)
        end
        filter tattr, :string do
          match do |scope, value|
            locs = I18n.available_locales.dup
            res = scope.where("#{attr}->>'#{locs.shift}' ~* :val",
                              val: value)
            locs.reduce(res) do |acc, loc|
              acc.or(scope.where("#{attr}->>'#{loc}' ~* :val", val: value))
            end
          end
        end
      end
    end

    def relation_opts(assoc)
      opts = {
        always_include_resource_ids: assoc.is_a?(
          ActiveRecord::Reflection::BelongsToReflection,
        ),
      }
      if assoc.polymorphic?
        opts
      else
        opts.merge(resource: "#{assoc.class_name}Resource".constantize)
      end
    end

    # assign default relations
    def default_relations
      return if abstract_class?

      model.reflect_on_all_associations.each do |assoc|
        name = assoc.name.to_sym
        next if dropped_relations.include?(name)

        # do not add again existing relations (in STI)
        next if config[:sideloads][name].present?

        opts = relation_opts(assoc)
        case assoc
        when ActiveRecord::Reflection::HasOneReflection
          opts[:foreign_key] ||= assoc.foreign_key.to_sym
          Rails.logger.debug { "#{model.model_name}: Adding has_one relation #{name} (#{opts})" }
          has_one name, **opts
        when ActiveRecord::Reflection::HasManyReflection
          if assoc.options[:as].present?
            opts[:as] = assoc.options[:as]
            Rails.logger.debug { "#{model.model_name}: Adding polymorphic_has_many relation #{name} (#{opts})" }
            polymorphic_has_many name, **opts
          else
            opts[:foreign_key] ||= assoc.foreign_key.to_sym
            Rails.logger.debug { "#{model.model_name}: Adding has_many relation #{name} (#{opts})" }
            has_many name, **opts
          end
        when ActiveRecord::Reflection::BelongsToReflection
          opts[:foreign_key] ||= assoc.foreign_key.to_sym
          if assoc.polymorphic?
            Rails.logger.debug { "#{model.model_name}: Adding polymorphic_belongs_to relation #{name} (#{opts})" }
            polymorphic_belongs_to name, **opts do
              group_by("#{name}_type") do
                # TODO! we have no auto way of guessing this
                on(:User)
                on(:Seminar)
                on(:Role)
                on(:Course)
                on(:CarouselItem)
              end
            end
          else
            Rails.logger.debug { "#{model.model_name}: Adding belongs_to relation #{name} (#{opts})" }
            belongs_to name, **opts
          end
          attribute opts[:foreign_key], :integer, only: %i[filterable]
        when ActiveRecord::Reflection::ThroughReflection
          if assoc.collection?
            Rails.logger.debug { "#{model.model_name}: Adding many_to_many relation #{name} (#{opts})" }
            many_to_many name, **opts
          else
            Rails.logger.debug { "#{model.model_name}: Adding has_one relation #{name} (#{opts})" }
            has_one name, **opts
          end
        else
          Rails.logger.warn(
            "Unfamiliar relation type #{assoc} for attribute #{name} of model #{model}. Ignoring",
          )
        end
      end
    rescue Graphiti::Errors::ModelNotFound => e
      warn e
    end

    def set_default_filters
      return if abstract_class?

      # logical or
      filter :|, :hash do
        eq do |scope, vals|
          vals.reduce(scope) do |res, fil|
            fil.reduce(res) do |scop, (k, v)|
              filt = filters[k]
              v.reduce(scop) do |cur, (op, val)|
                (
                  filt[:operators][op]&.call(base_scope, val, context) ||
                  adapter.try(
                    "filter_#{filt[:type]}_#{op}", base_scope, k, [*val]
                  )
                ).or(cur)
              end
            end
          end
        end
      end
    end

    def decorator_attributes
      {web: :string, allowed_locales: :array_of_strings}
    end

    def default_decorator_attributes
      return if abstract_class?

      decorator_attributes.each do |attr, type|
        attribute attr, type, only: %i[readable] do
          @object.decorate.try(attr)
        end
      end
    end

    def defaults
      return if ENV['SKIP_GRAPHITI_RESOURCES']

      self.default_page_size = 1000

      self.abstract_class = model.abstract_class?

      unless abstract_class?
        self.model = infer_model
        self.type = infer_type
      end

      default_polymorphic
      default_attributes
      default_relations
      set_default_filters
      default_decorator_attributes

      return if abstract_class?

      attribute :writable, :array_of_strings, only: %i[readable] do
        policy(@object).permitted_keys.map { |x| x.camelize(:lower) }
      end
    end

    ### generate Spraypaint model files
    def to_spraypaint
      @@spraypaint ||= {ApplicationResource => {writable: {}, id: {}}}
      @@to_spraypaint ||= {ApplicationResource => ''}
      return @@to_spraypaint[self] if @@to_spraypaint[self]

      spimports = { }
      type = model.model_name.collection
      base = if superclass == ApplicationResource
               'ApplicationRecord'
             else
               model.superclass.name
             end
      @@to_spraypaint[superclass] ||= superclass.to_spraypaint
      rels = []
      @@spraypaint[self] ||= {}
      model.reflect_on_all_associations.each do |assoc|
        name = assoc.name
        next if dropped_relations.include?(name.to_sym)

        @@spraypaint[self][name] = assoc
        next if @@spraypaint[superclass][name]

        cname = assoc.class_name.underscore.pluralize
        arg = (cname == assoc.plural_name) ? '' : "'#{cname}'"
        aname = name.to_s.camelize(:lower)
        case assoc
        when ActiveRecord::Reflection::HasOneReflection
          kind = 'hasOne'
        when ActiveRecord::Reflection::HasManyReflection
          kind = 'hasMany'
        when ActiveRecord::Reflection::BelongsToReflection
          kind = 'belongsTo'
        when ActiveRecord::Reflection::ThroughReflection
          kind = assoc.collection? ? 'hasMany' : 'hasOne'
        else
          # rubocop:disable Rails/Output
          puts(
            "Unfamiliar relation type #{assoc} for attribute #{name} of model #{model}. Ignoring",
          )
          # rubocop:enable Rails/Output
        end
        spimports[kind] = true
        rels.push("#{aname}: #{kind}(#{arg}),")
      end

      attributes.each do |k, v|
        next unless v[:readable]

        @@spraypaint[self][k] = v
        next if @@spraypaint[superclass][k]

        spimports[:attr] = true
        args = READONLY_ATTRS.include?(k) ? '{ persist: false }' : ''
        rels.push("#{k.to_s.camelize(:lower)}: attr(#{args}),")
      end

      attrs = if rels.present?
                <<EOF
  attrs: {
    #{rels.join("\n    ")}
  },
EOF
              else
                ''
              end
      result = <<~EOF
        import #{base} from '@/models/#{base}'
        
        export default #{base}.extend({
          static: {
            jsonapiType: '#{type}',
          },
        #{attrs}
        })
      EOF
      if spimports.present?
        result = <<~EOF
          import { #{spimports.keys.join(', ')} } from 'spraypaint'
          #{result}
        EOF
      end
      result
    end

    def write_spraypaint(fname = "#{model.name}.js", **opts)
      warn("  Writing #{opts[:prefix] || ''}#{fname}")
      File.write("#{opts[:prefix] || ''}#{fname}", to_spraypaint)
    end

    def write_all_spraypaint(prefix = 'spraypaint/')
      descendants.each { |rr| rr.write_spraypaint(prefix:) }
    end

    protected

    def types
      @types ||= Graphiti::Types.map.freeze
    end

    def opts_for_attr(attr)
      {
        readable: :"attr_readable_#{attr}?",
        #writable: "attr_writable_#{attr}?".to_sym,
      }
    end

    def type_of(attr)
      @type_trans ||= {
        jsonb: :hash,
        text: :string,
        inet: :string,
        daterange: :array_of_dates,
        tstzrange: :array_of_datetimes,
        decimal: :big_decimal,
      }.freeze
      type = model.type_for_attribute(attr)
      res = :string
      opts = opts_for_attr(attr)
      if type.is_a?(ActiveRecord::Enum::EnumType)
        res = :string_enum
        # TODO: don't use private functions...
        opts[:allow] = type.send(:mapping).keys
      else
        res = type.type
        res = @type_trans[res] || res
      end
      [res, opts]
    end
  end

  def record
    @record ||= @object || (if context.params['data']['id'].present?
                              model.find(context.params['data']['id'])
                            end)
  end

  def attr_readable?(attr, mod = record)
    policy(mod)&.visible?(attr)
  end

  def attr_writable?(attr, mod = record || model.new)
    policy(mod)&.writable?(attr)
  end

  before_save do |obj|
    context.send(:authorize, obj, "#{Graphiti.context[:namespace]}?") if obj.has_changes_to_save?
  end

  def delete(obj)
    context.send(:authorize, obj, "#{Graphiti.context[:namespace]}?")
    super
  end

  def assign_attributes(object, attributes)
    ActionController::Parameters.action_on_unpermitted_parameters = :raise
    par = ActionController::Parameters.new(attributes)
    object.attributes = par.permit(policy(object).permitted_attributes)
  rescue ActionController::UnpermittedParameters => e
    raise Errors::UnpermittedParameters, e
  end

  def policy(rec = record)
    # TODO: there is some over-caching here, possibly related to:
    # https://github.com/graphiti-api/graphiti/issues/186
    # So we avoid the cache
    Pundit.policy(context.presence && pundit_user, rec)
  end

  delegate :current_user, :action_name, :pundit_user, to: :context

  # use this to build a new object with the given params
  def build_object(params)
    obj = build(model)
    assign_attributes(obj, params)
    obj
  end
end

