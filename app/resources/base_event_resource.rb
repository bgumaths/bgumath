# frozen_string_literal: true

class BaseEventResource < ApplicationResource
  class << self
    def decorator_attributes
      super.merge(duration_text: :hash)
    end
  end

  include Durized

  defaults
end

