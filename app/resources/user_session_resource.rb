# frozen_string_literal: true

class UserSessionResource < ApplicationResource
  self.adapter = Graphiti::Adapters::Null
  self.type = 'user_session'
  primary_endpoint '/user_session'

  attribute :email, :string, only: :writable
  attribute :password, :string, only: :writable
  has_one :user, only: :readable

  delegate :create, to: :model

  def destroy(*_args)
    raise Graphiti::Errors::RecordNotFound unless session

    session.destroy
    session
  end

  def resolve(_scope)
    session ? [session] : []
  end

  protected

  def session
    @session ||= model.find
  end
end

