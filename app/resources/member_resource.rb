# frozen_string_literal: true

load 'app/resources/academic_resource.rb'
class MemberResource < AcademicResource
  load 'app/resources/student_resource.rb'
  primary_endpoint '/members', %i[index show create update destroy]
  defaults
  many_to_many :research_groups
end

