# frozen_string_literal: true

class GenericCourseResource < ApplicationResource
  defaults

  filter :level, :string, allow:
  %w[basic advanced service first_year organic advanced_undergrad graduate] do
    eq do |scope, value|
      vals = [*value]
      res = scope.public_send(vals.shift)
      vals.each do |val|
        res = res.or(scope.public_send(val))
      end
      res
    end
  end

  # for some reason, many_to_many doesn't work as advertised
  filter :require_id, allow_nil: true do
    eq do |scope, value|
      scope.includes(:allowances).where(allowances: { require_id: value })
    end
  end

  filter :allow_id, allow_nil: true do
    eq do |scope, value|
      scope.includes(:requirements).where(requirements: { allow_id: value })
    end
  end
end

