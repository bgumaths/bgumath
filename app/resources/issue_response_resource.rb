# frozen_string_literal: true

class IssueResponseResource < ApplicationResource
  defaults

  after_attributes :set_creator, only: %i[create]

  protected

  def set_creator(mod)
    mod.creator ||= current_user
  rescue ActiveRecord::AssociationTypeMismatch
    nil
  end
end

