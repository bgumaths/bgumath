# frozen_string_literal: true

class RoleResource < ApplicationResource
  def self.dropped_relations
    super + %i[events]
  end

  defaults
end

