# frozen_string_literal: true

class SeminarResource < ApplicationResource
  defaults

  # allow filtering by admin user
  filter :user_id, :integer do
    eq do |scope, value|
      scope.with_role(:Admin, User.find(value.first))
    end
  end
end

