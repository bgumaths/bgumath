# frozen_string_literal: true

module Termized
  extend ActiveSupport::Concern

  included do
    filter :term_name, :string do
      eq do |scope, value|
        scope.where_term(slug: value)
      end
    end
  end
end

