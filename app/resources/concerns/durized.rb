# frozen_string_literal: true

module Durized
  extend ActiveSupport::Concern

  included do
    filter :year, :integer, single: true do
      eq do |scope, value|
        scope.year(value)
      end
      gt do |scope, value|
        scope.from_time(Date.new(value, CommonUtil::YEAR_START, 1))
      end
      gte do |scope, value|
        scope.from_time(Date.new(value - 1, CommonUtil::YEAR_START, 1))
      end
      lt do |scope, value|
        scope.before(Date.new(value - 1, CommonUtil::YEAR_START, 1))
      end
      lte do |scope, value|
        scope.before(Date.new(value, CommonUtil::YEAR_START, 1))
      end
    end

    filter :week, :integer, single: true do
      eq do |scope, value|
        scope.between(
          CommonUtil.week_start + value.weeks,
          CommonUtil.week_end + value.weeks,
        )
      end
      lt do |scope, value|
        scope.before(CommonUtil.week_start + (value - 1).weeks)
      end
      lte do |scope, value|
        scope.before(CommonUtil.week_start + value.weeks)
      end
      gte do |scope, value|
        scope.from_time(CommonUtil.week_start + (value - 1).weeks)
      end
      gt do |scope, value|
        scope.from_time(CommonUtil.week_start + value.weeks)
      end
    end
  end
end

