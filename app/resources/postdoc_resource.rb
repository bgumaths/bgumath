# frozen_string_literal: true

class PostdocResource < AcademicResource
  secondary_endpoint '/postdocs', %i[index show create update destroy]
  defaults
end

