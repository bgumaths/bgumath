# frozen_string_literal: true

class AppSetupDecorator < BaseDecorator
  decorates_associations :chair_roles, :staff_roles, :issues_roles

  def self.pnamespace
    'admin'
  end

  def self.singular?
    true
  end

  def to_fs(*)
    Rails.env
  end

  def self.inertia_params
    super.vdeep_merge(
      only: attr_names.map(&:to_sym) - %i[created_at updated_at],
      methods: %i[chair_role_ids staff_role_ids issues_role_ids],
    )
  end
end

