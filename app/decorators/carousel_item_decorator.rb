# frozen_string_literal: true

class CarouselItemDecorator < BaseDecorator
  def self.pnamespace
    'admin'
  end

  def self.mmname
    CarouselItem.model_name
  end

  def to_fs(_loc = I18n.locale)
    ''
  end

  def type_html(_loc = I18n.locale)
    nil
  end

  def form_path
    [self.class.pnamespace.to_sym, *path_args, object.becomes(CarouselItem)].select(&:presence)
  end

  def show_partial
    object.model_name.singular
  end

  def activation
    h.date_range(activates, expires)
  end

  def css_class
    ' ' + show_partial
  end
end

