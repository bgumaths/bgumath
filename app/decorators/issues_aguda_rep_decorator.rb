# frozen_string_literal: true

class IssuesAgudaRepDecorator < IssuesUserDecorator
  def to_partial_path
    IssuesUser.model_name.singular
  end
end

