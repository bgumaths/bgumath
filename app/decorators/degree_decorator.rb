# frozen_string_literal: true

class DegreeDecorator < ShnatonPartDecorator
  decorates_associations :prereqs
  decorates_associations :plans

  def desc(loc = I18n.locale)
    description_i18n[loc] || description
  end

  def new_prereq_path(**opts)
    h.in_outer_locale do
      h.new_teaching_ac_year_shnaton_degree_prog_prereq_path(
        try(:ac_year), self, **opts
      )
    end
  end

  def new_plan_path(**opts)
    h.in_outer_locale do
      h.new_teaching_ac_year_shnaton_degree_degree_plan_path(
        try(:ac_year), self, **opts
      )
    end
  end

  def top_prereqs
    object.prereqs.top_level.decorate
  end

  def to_sigma_graph(nodes = {}, edges = {}, layout = nodes.blank?)
    prereqs.each do |cl|
      cl.to_sigma_graph(nodes, edges, false)
    end
    h.layout_graph(nodes, edges.values) if layout

    { nodes: nodes.values, edges: edges.values }
  end
end

