# frozen_string_literal: true

class BaseUserDecorator < BaseDecorator
  def self.pnamespace
    'people'
  end

  def to_fs(loc = nil, **opts)
    loc ||= opts[:locale] || I18n.locale
    unless locale_allowed?(loc)
      loc = locale_allowed?(I18n.locale) ? I18n.locale : I18n.default_locale
    end
    (rank.present? ? "#{h.tt("rank.#{rank}", **opts, locale: loc)} " : '') +
      "#{first_i18n[loc.to_s]} #{last_i18n[loc.to_s]}"
  end

  def fullname(**opts)
    self.class.allowed_locales
        .select { |ll| locale_allowed?(ll) }
        .index_with { |ll| [ll, to_fs(ll, **opts)] }
  end

  def locale_allowed?(loc = I18n.locale)
    last_i18n[loc.to_s].present? || first_i18n[loc.to_s].present?
  end

  def email_address(with_name = false)
    require 'mail'
    res = Mail::Address.new h.full_email(email, try(:mail_domain))
    res.display_name = to_fs(:en) if with_name
    res.format
  rescue Mail::Field::ParseError => e
    logger.warn("email_address: #{e.message}")
    h.full_email(email, try(:mail_domain))
  end

  def mailed(str = self, **)
    str ? h.email_html(email_address, str, **) : ''
  end

  def email_td(**opts)
    h.tag.td(data_order: email_address, **opts) do
      mailed(email)
    end
  end

  def serialize_attributes
    super + %i[email_address]
  end

  def self.inertia_index_params
    super.vdeep_merge(
      only: %i[email_address email office phone],
    ).freeze
  end
end

