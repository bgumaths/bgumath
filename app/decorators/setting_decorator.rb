# frozen_string_literal: true

class SettingDecorator < BaseDecorator
  def self.pnamespace
    'admin'
  end

  def svar
    @svar ||= var.sub(/_i18n$/, '')
  end

  def to_s(_loc = I18n.locale)
    if I18n.exists?(:name, scope: [:settings, :attributes, svar])
      h.tt(:name, scope: [:settings, :attributes, svar])
    else
      svar
    end
  end

  # for editable
  def respond_to_missing?(name, *)
    (value.is_a?(Hash) && name.start_with?('value_')) || super
  end

  def method_missing(name, *args, &)
    if object.value.is_a?(Hash)
      case name
      when /^value_(.*)=/
        return (object.value[$1] = args[0])
      when /^value_(.*)/
        return object.value[$1]
      end
    end
    super
  end
        
end

