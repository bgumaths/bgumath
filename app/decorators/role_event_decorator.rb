# frozen_string_literal: true

class RoleEventDecorator < BaseDecorator
  decorates_associations :role

  def user
    object.user.try(:decorate) || object.user
  end

  def creator
    object.creator.try(:decorate) || object.creator
  end

  def self.pnamespace
    'admin'
  end

  def to_fs(meth = 'to_s', **gopts)
    opts = { show_user: true, show_creator: true }.merge(gopts)
    opts[:who] = user.try(meth, **gopts) || h.tt(:anonymous, **gopts) if opts[:show_user]
    res = "#{h.l(created_at, format: :long, **gopts)}: #{h.tt Role, **gopts} "
    res += "#{role.public_send(meth, **gopts)} " if opts[:show_role]
    res += typed(**opts)
    if opts[:show_creator]
      res += " (#{h.tt :updated_by,
                       who: creator.try(meth, **gopts) || h.tt(:anonymous, **gopts),
                       **gopts})"
    end
    res.html_safe
  end

  def self.inertia_index_params
    super.vdeep_merge(
      methods: %i[created_at],
      include: {
        creator: UserDecorator.inertia_index_params,
        role: RoleDecorator.inertia_index_params,
      },
    )
  end
end

