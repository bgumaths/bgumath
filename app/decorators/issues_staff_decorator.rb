# frozen_string_literal: true

class IssuesStaffDecorator < IssuesUserDecorator
  decorates_associations :department, :generic_courses
  def to_partial_path
    IssuesStaff.model_name.singular
  end

  def with_dept(meth = :to_s, *, **)
    try(meth, *, **) +
      " (#{department.try(meth, *, **)})".html_safe
  end
end

