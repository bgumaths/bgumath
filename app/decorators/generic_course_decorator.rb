# frozen_string_literal: true

class GenericCourseDecorator < BaseDecorator
  include Descfiles
  decorates_association :courses
  decorates_association :requires
  decorates_association :allows
  decorates_association :course_lists
  decorates_associations :departments, :issues_staffs, :aguda_rep
  decorates_associations :year_added, :year_removed, :replaces, :replaced_by

  include SigmaGraph
  alias_method :children, :requires

  def self.pnamespace
    'teaching'
  end

  def duplicate_path(**)
    new_path(generic_course_id: generic_course.slug, **)
  end

  def to_fs(loc = I18n.locale)
    (name_i18n[loc] || name || '').html_safe
  end

  alias_method :to_s, :to_fs

  def locale_allowed?(loc = I18n.locale)
    name_i18n[loc.to_s].presence
  end

  def to_html(loc = I18n.locale)
    super + marks
  end

  def marks(**opts)
    res = ''
    opts = { class: %w[small mx-1] }.merge(opts)
    if prereq
      res += h.tag.span(h.glyphicon('flash', '(*)'), **opts)
    end
    if core
      res += h.tag.span(h.glyphicon('education', '(#)'), **opts)
    end
    if year_removed.present?
      res += h.tag.span(
        h.glyphicon('ban', '(!)',
                    h.t(
                      :deprecated, scope: %i[teaching generic_course type]
                    )), **opts
      )
    end
    res.html_safe
  end

  def with_shid
    [to_s, shid].select(&:present?).join(', ')
  end

  def to_label # for simple_form
    cls = []
    cls << 'gcfall' if fall
    cls << 'gcspring' if spring
    h.tag.span(h.markdown(with_shid, inline: true), class: cls)
  end

  def desc(loc = I18n.locale)
    description_i18n[loc] || description
  end

  def shid_linked(**opts)
    sid = opts.delete(:short) ? short_shid : shid
    if sid
      h.link_to(sid, shnaton_url,
                **{ data: { toggle: :tooltip }, class: %w[shid],
                    title: h.tt('teaching.catalog'), }.deep_merge(opts))
    else
      ''
    end
  end

  def sigma_node_opts
    {
      dir: h.is_rtl? ? -1 : 1,
      url: show_path,
      object: {
        title: linked,
        description: h.markdown(desc),
        shid: shid_linked,
        credits: h.tt('teaching.credits', count: credits),
      },
    }
  end

  def to_treant_node
    {
      text: {
        name: to_s,
        #desc: desc,
        contact: {
          val: shid,
          href: shnaton_url,
        },
      },
      link: {
        href: show_path,
      },
      #collapsable: true,
    }
  end

  def cred_hours_tex(_loc = I18n.locale)
    [lectures, exercises].select(&:present?).join('/').html_safe
  end

  def map_terms(all = false)
    trms = []
    if fall || all
      trms.push(yield 'fall')
    end
    if spring || all
      trms.push(yield 'spring')
    end
    trms.select(&:present?)
  end

  def terms_s
    map_terms { |trm| h.tt("g#{trm}", scope: %w[teaching term name]) }
      .join(', ').html_safe
  end

  # Don't include deleting a course by default, this is usually a mistake
  def action_list(*actions, **dopts)
    super(*(actions.presence || %w[edit duplicate]), **dopts)
  end

  def to_sigma_graph(nodes = {}, edges = {}, layout = false,
                     children = ->(obj) { obj.children })
    to_sig_graph(nodes, edges, children)
    h.layout_graph(nodes, edges.values) if layout
    { nodes: nodes.values, edges: edges.values }
  end

  def to_graph
    to_sigma_graph({}, {}, true)
  end

  def self.to_schema
    {
      :@type => 'Course',
    }
  end

  def to_schema
    @to_schema ||= self.class.to_schema.deep_merge(
      courseCode: shid,
      coursePrerequisites: requires.map(&:to_schema),
      hasCourseInstance: current_or_next.try(:decorate).try(:to_schema),
      about: to_s,
      accessMode: :auditory,
      isFamilyFriendly: true,
      provider: h.dept_schema,
      description: h.markdown(desc, inline: true),
      url: web,
    ).compact
  rescue SystemStackError
    _, circ = all_requires
    raise CircularReqError, self, circ
  end

  def avail_terms
    courses.map(&:term)
  end

  def self.inertia_index_params
    super.vdeep_merge(
      only: %i[shid description_i18n],
      methods: %i[shnaton_url level],
    ).freeze
  end

  def self.inertia_edit_params
    super.vdeep_merge(
      methods: %i[aguda_rep_id department_ids year_added_id year_removed_id
                  replace_ids replaced_by_ids require_ids course_list_ids],
    )
  end

  def self.inertia_params
    super.vdeep_merge(
      only: %i[name_i18n slug lectures exercises advanced graduate prereq core
               fall spring],
      methods: %i[to_graph],
      include: {
        departments: DepartmentDecorator.inertia_essential_params,
        issues_staffs: IssuesStaffDecorator.inertia_essential_params,
        aguda_rep: IssuesAgudaRepDecorator.inertia_essential_params,
        year_added: AcYearDecorator.inertia_essential_params,
        year_removed: AcYearDecorator.inertia_essential_params,
        replaces: inertia_essential_params,
        replaced_by: inertia_essential_params,
        requires: inertia_essential_params,
        course_lists: CourseListDecorator.inertia_essential_params,
      },
    ).freeze
  end
end

