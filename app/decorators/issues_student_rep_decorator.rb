# frozen_string_literal: true

class IssuesStudentRepDecorator < IssuesUserDecorator
  def to_partial_path
    IssuesStudentRep.model_name.singular
  end

  def term_courses(term = Term.default)
    @term_courses ||= {}
    @term_courses[term] ||=
      if term.is_a?(Term)
        courses.this_term(term).map do |cc|
          cc.linked_with_id(false)
        end
      else
        [courses.map(&:linked_with_id).join('; ').html_safe]
      end
  end
end

