# frozen_string_literal: true

class ResourceCarouselItemDecorator < CarouselItemDecorator
  decorates_association :resources

  def to_fs(_loc = I18n.locale)
    resources.map(&:to_s).join('; ')
  end

  def type_html(loc = I18n.locale)
    h.tt res_type&.safe_constantize, locale: loc, count: resources.count
  end

  def css_class
    super + " #{res_type&.downcase}_#{super}"
  end

  protected

  def res_type
    resources[0]&.resource_type
  end
end

