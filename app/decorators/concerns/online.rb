# frozen_string_literal: true

module Online
  extend ActiveSupport::Concern

  def online_btn(**opts)
    return '' if online.blank?

    past = opts.key?(:past) ? opts[:past] : try(:past?)
    h.link_to(
      h.tt(past ? 'watch_online' : 'attend_online'),
      online, class: %w[btn btn-primary], **opts
    )
  end

  def online_icon(**)
    return '' if online.blank?

    h.link_to(h.glyphicon(:online), online, **)
  end

  def cancelled?
    online.blank? && object.cancelled?
  end

  class_methods do
    def inertia_index_params
      super.vdeep_merge(
        methods: %i[online],
      )
    end
  end
end

