# frozen_string_literal: true

module Descfiles
  extend ActiveSupport::Concern

  def descfiles
    object.descfiles.decorate
  end

  def descfiles_attachments
    object.descfiles_attachments.decorate
  end

  class_methods do
    def inertia_index_params
      super.vdeep_merge(
        include: {
          descfiles: {
            only: %i[],
            methods: %i[url thumb_url to_s signed_id filename],
          },
        },
      )
    end
  end
end

