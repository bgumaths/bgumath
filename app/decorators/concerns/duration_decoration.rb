# frozen_string_literal: true

module DurationDecoration
  extend ActiveSupport::Concern

  # TODO: change this to duration_i18n for compatibility with trasto
  def duration_text
    res = {}
    I18n.available_locales.each do |loc|
      res[loc] = h.date_range(starts(true), ends(true), locale: loc.to_sym)
    end
    res
  end

  def duration_str(loc = I18n.locale)
    duration_text[loc]
  end

  def starts(...)
    object.starts(...)&.strftime('%H:%M')
  end

  def ends(...)
    object.ends(...)&.strftime('%H:%M')
  end

  class_methods do
    def inertia_params
      super.vdeep_merge(
        methods: %i[starts ends],
      )
    end
  end
end

