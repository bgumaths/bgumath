# frozen_string_literal: true

class PlanItemDecorator < ShnatonPartDecorator
  decorates_association :term_plan

  decorates_associations :generic_course, :ac_year, :shnaton, :degree, :degreen_plan

  delegate :to_s, :to_html, :to_label, :terms, :terms_s, :to_trans, :web,
           :show_path, to: :generic_course

  def path_args
    [term_plan.ac_year, term_plan.degree]
  end

  def self.nesting
    %i[ac_year shnaton degree]
  end
end

