# frozen_string_literal: true

class CarouselItemRecordDecorator < BaseDecorator
  decorates_association :resource
  decorates_association :item

  def to_fs(loc = I18n.locale)
    "#{resource_type}: #{resource&.to_s(loc) || 'NIL'}"
  end
end

