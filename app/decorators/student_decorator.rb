# frozen_string_literal: true

class StudentDecorator < AcademicDecorator
  decorates_association :supervisors

  def self.inertia_index_params
    super.vdeep_merge(
      include: {
        supervisors: MemberDecorator.inertia_index_params,
      },
    )
  end

  def self.inertia_params
    super.vdeep_merge(
      include: {
        supervisors: MemberDecorator.inertia_index_params,
      },
    )
  end
  #def self.mmname
  #  Student.model_name
  #end
end

