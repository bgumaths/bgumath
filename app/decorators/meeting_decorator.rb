# frozen_string_literal: true

class MeetingDecorator < BaseEventDecorator
  include Descfiles
  include Online

  decorates_association :seminar
  decorates_association :term

  def self.pnamespace
    'research'
  end

  def to_fs(loc = I18n.locale)
    announcement_i18n[loc.to_s].presence || title || h.tt('tba', locale: loc)
  end

  def locale_allowed?(loc = I18n.locale)
    seminar.try(:locale_allowed?, loc)
  end

  # comply with base class
  def description
    abstract
  end

  def location
    room
  end

  def online
    object.online || (date >= Time.zone.today && seminar.online)
  end

  def summary
    "#{speaker}: #{self}"
  end

  alias_method :has_abstract?, :has_description?

  def day
    h.l date, format: :daylong
  end

  def linked_speaker
    return '' unless speaker

    h.link_to_if(speaker_web, speaker, speaker_web)
  end

  def full_speaker(*args, **opts)
    opts[:speaker_tag] ||= :span
    opts[:from_tag] ||= :span
    opts[:bdi] = true unless opts.key?(:bdi)
    linked = args.first
    # use bdi to ensure parentheses displayed correctly
    res = h.content_tag(
      opts[:speaker_tag], ((linked ? linked_speaker : speaker) || '')
    ) +
          (
             from.blank? ? '' : " (#{h.content_tag(opts[:from_tag], from)})"
           ).html_safe
    opts[:bdi] ? h.tag.bdi(res) : res
  end

  def full_speaker_text
    return '' if speaker.blank?

    speaker + (from.blank? ? '' : " (*#{from}*)")
  end

  def place(**)
    h.markdown(room, inline: true, **)
  end

  def unusual
    u = []
    u.push(I18n.t('coord.day')) if unusual_day
    u.push(I18n.t('coord.time')) if unusual_time
    u.push(I18n.t('coord.place')) if unusual_place
    h.to_commas(u, :and)
  end

  def unusual_html(**opts)
    opts = { title_tag: :strong, item_tag: :p }.merge(opts)
    title_tag = opts[:title_tag] || :span
    item_tag = opts[:item_tag] || :div
    res = ''.html_safe
    if unusual_day
      res << h.content_tag(item_tag) do
        h.content_tag(title_tag, h.tt('coord.day')) +
          ": #{h.l date, format: :daylong}"
      end
    end
    if unusual_time
      res << h.content_tag(item_tag) do
        h.content_tag(title_tag, h.tt('coord.time')) +
          ": #{h.l starts}&ndash;#{h.l ends}".html_safe
      end
    end
    if unusual_place
      res << h.content_tag(item_tag) do
        h.content_tag(title_tag, h.tt('coord.place')) + ": #{room}"
      end
    end
    res.html_safe
  end

  def fullcal_tooltip
    h.tag.h5(seminar.to_s) + super
  end

  # override form_path to _not_ include the term, since we wish to deduce it
  # from the date
  def form_path
    [self.class.pnamespace.to_sym, seminar, object]
  end

  def self.nesting
    %i[term seminar]
  end

  def speaker_url
    url(speaker_web)
  end

  def to_atom(ev)
    return unless super

    ev.author do |author|
      author.name(speaker)
      author.uri(speaker_url)
      author.email(from)
    end
    ev
  end

  def serialize_attributes
    super + %i[summary full_speaker_text unusual unusual_place unusual_time
               unusual_day]
  end

  def to_ical
    ev = super
    ev.organizer = seminar.admin_users.first&.decorate&.email_address ||
                   ev.organizer
    ev
  end

  def to_schema
    return nil if announcement.present?

    @to_schema ||=
      super.deep_merge(
        location: {
          name: h.t(:depname),
          sameAs: h.root_url(locale: ''),
          address: "#{room}, #{h.t(:fdepname)}",
        },
        performer: {
          :@type => 'Person',
          name: speaker,
          sameAs: speaker_web,
          affiliation: from,
        },
      )
  end

  def seminar_id
    seminar&.to_param || seminar&.id
  end

  def term_id
    term&.to_param || term&.id
  end

  def self.inertia_index_params
    super.vdeep_merge(
      only: %i[web abstract speaker from speaker_web location title
               announcement_i18n],
      methods: %i[date seminar_id term_id starts ends],
    ).freeze
  end

  def self.to_jsstr(...)
    "`1900-01-01T${#{super}}`"
  end

  def self.inertia_params
    super.vdeep_merge(
      only: %i[room],
      include: {
        seminar: SeminarDecorator.inertia_index_params.vdeep_merge(
          methods: %i[meeting_dates],
        ),
        term: TermDecorator.inertia_index_params,
      },
    ).freeze
  end
end

