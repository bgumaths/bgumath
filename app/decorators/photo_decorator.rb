# frozen_string_literal: true

class PhotoDecorator < Draper::Decorator
  delegate_all
  decorates_finders

  decorates_association :image

  delegate :attachment, to: :image

  def self.collection_decorator_class
    CollectionDecorator
  end

  def imageable
    object.imageable&.decorate
  end

  def policy
    imageable&.policy
  end

  def destroy_path
    imageable&.destroy_path
  end

  def to_fs(loc = I18n.locale)
    if imageable.present?
      h.tt(:photoof, 
           who: imageable.to_s(loc), locale: loc)
    else
      image.to_s
    end
  end

  def variant(**opts)
    return image if opts[:size] == :full

    if (size = opts.delete(:size))
      opts = (IMG_TRANS[size] || {}).merge(opts)
    end
    # imagemagick version:
    #object.variant(auto_orient: true, **opts)
    # vips version:
    object.variant(autorot: true, **opts)
  end

  def normal
    variant(size: :normal)
  end

  def web
    h.rails_blob_url(image)
  end

  def url(...)
    h.url_for(variant(...))
  end

  def url_full
    url(size: :full)
  end

  def to_html(**opts)
    if image?
      normal = opts.delete(:size)
      # the size could be nil, to allow cropping without resizing
      normal = :normal unless defined?(normal)
      small = (normal == :large) ? :normal : :sm
      small = normal if [:full, nil].include?(normal)
    
      h.tag.picture do
        h.tag.source(srcset: url(size: small), 
                     media: '(max-width: 700px)') +
          to_img(normal, **opts)
      end.html_safe
    elsif video?
      opts.delete(:variant)
      opts.delete(:size)
      h.video_tag(
        h.url_for(image),
        muted: true,
        autoplay: true,
        loop: true,
        playsinline: true,
        height: image.metadata[:height],
        width: image.metadata[:width],
        **opts,
      )
    else
      h.tag.p("Unsupported media type #{content_type}").html_safe
    end
  end

  def to_s
    filename&.to_s
  end

  def to_img(size = :normal, **opts)
    vri = variant(size:, **(opts.delete(:variant) || {}))
    return ''.html_safe if vri.blank?

    h.image_tag(vri, class: %w[img-fluid], alt: to_s, longdesc: imageable&.web,
                     **opts)
  end

  def cropper_opts
    {
      aspectRatio: PHOTO_WIDTH.to_f / PHOTO_HEIGHT,
      autoCropArea: 1.0,
      minContainerWidth: PHOTO_WIDTH,
      minContainerHeight: PHOTO_HEIGHT,
      viewMode: 2,
      zoomOnWheel: false,
    }
  end

  def to_label(field, context = nil, **opts)
    bb = opts[:builder] ||
         DataFormBuilder.new(
           imageable.class.try(:model_name).try(:i18n_key),
           imageable, context, {}
         )
    res = h.tag.div(
      to_img(
        :full, 
        class: %w[resizable],
        id: "img-#{id}",
        data: {
          cropperOpts: cropper_opts,
        },
        **(opts[:img] || {})
      ),
      class: 'img-wrap',
      **(opts[:wrap] || {})
    )
    return res if attachment.blank?

    res + h.tag.div(class: %w[row my-2 crop-coords], id: "img-#{id}-crop", dir: :ltr) do
      bb.simple_fields_for field, self, wrapper: :horizontal_form, child_index: id do |form|
        %i[x y width height].map do |ff|
          h.tag.div(class: 'col') do
            form.input(ff, readonly: true, disabled: false,
                           label: "#{ff.to_s[0].upcase}:",
                           wrapper_html: { class: 'mb-0 mt-1' },
                           input_html: { data: { crop: ff },
                                         class: 'crop-data pr-1',
                                         id: "img-#{id}-#{ff}", })
          end
        end.join("\n").html_safe +
          h.tag.div(
            attachment.destroy_btn,
            class: %w[col col-1 d-flex justify-content-between align-items-center]
          ) +
          form.input(:signed_id, disabled: false,
                                 wrapper: false, as: :hidden)
      end
    end
  end
end

