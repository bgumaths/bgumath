# frozen_string_literal: true

class TermDecorator < BaseDecorator
  #decorates_associations :courses
  decorates_associations :seminars
  def self.pnamespace
    'teaching'
  end

  def to_fs(loc = I18n.locale, **opts)
    return name_i18n[loc.to_s] if adhoc?

    res = ''
    if year.present? && !opts[:noyear]
      myear = h.tt :year_range,
                   scope: %i[time_will_tell date_range],
                   from: year - 1, to: year % 100, locale: loc, default: ''
      res = h.tt part, scope: %i[teaching term name],
                       year: myear, locale: loc, default: ''
    elsif part.present?
      res = h.tt "g#{part}", scope: %i[teaching term name], locale: loc
    end
    if opts[:full]
      res = h.tt :full, 
                 scope: %i[teaching term], what: res, locale: loc
    end
    res
  end

  # ensure the id starts with a letter
  def to_id
    ('term-' + to_fs('en')).parameterize
  end

  # define to_label, since otherwise simple_form uses {term#name}, which is 
  # usually empty
  def to_label
    to_s
  end

  def term
    self
  end
end

