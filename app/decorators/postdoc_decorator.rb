# frozen_string_literal: true

class PostdocDecorator < AcademicDecorator
  decorates_association :hosts

  def self.inertia_index_params
    super.vdeep_merge(
      include: {
        hosts: BaseDecorator.inertia_index_params,
      },
    )
  end

  def self.inertia_params
    super.vdeep_merge(
      include: {
        hosts: MemberDecorator.inertia_index_params,
      },
    )
  end
end

