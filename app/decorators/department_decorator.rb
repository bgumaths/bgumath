# frozen_string_literal: true

class DepartmentDecorator < BaseDecorator
  decorates_associations :people, :courses

  def self.pnamespace
    'teaching'
  end

  def to_fs(loc = I18n.locale)
    name_i18n[loc] || name
  end
end

