# frozen_string_literal: true

class VisitorDecorator < BaseUserDecorator
  decorates_associations :hosts
  include DurationDecoration

  def serialize_attributes
    super + %i[starts ends]
  end

  def web = webpage

  def editable_field(...) = h.my_editable(self, ...)

  def self.inertia_index_params
    super.vdeep_merge(
      methods: %i[starts ends],
      include: {
        hosts: MemberDecorator.inertia_index_params,
      },
    )
  end

  def self.inertia_params
    super.vdeep_merge(
      only: %i[first_i18n last_i18n rank email office phone webpage origin],
    )
  end
end

