# frozen_string_literal: true

class ImageCarouselItemDecorator < CarouselItemDecorator
  decorates_association :image

  def to_fs(_loc = I18n.locale)
    image ? "Image: #{image.filename}" : 'Nil image'
  end
end

