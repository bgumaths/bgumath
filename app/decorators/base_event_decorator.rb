# frozen_string_literal: true

class BaseEventDecorator < BaseDecorator
  include DurationDecoration
  include Descfiles
  include Online

  def to_fs(_loc = I18n.locale)
    title || ''
  end

  def serialize_attributes
    super + %i[duration_text starts ends]
  end

  def summary
    to_s
  end

  def to_ical
    require 'icalendar'
    ev = Icalendar::Event.new
    if starts(true).localtime.seconds_since_midnight.positive?
      ev.dtstart = Icalendar::Values::DateTime.new starts(true), tzid: TZID
      ev.dtend = Icalendar::Values::DateTime.new ends(true), tzid: TZID
    else
      ev.dtstart = Icalendar::Values::Date.new starts(true), tzid: TZID
      ev.dtend = Icalendar::Values::Date.new ends(true), tzid: TZID
    end
    ev.summary = summary
    ev.description = description
    ev.location = online.presence || location
    ev.ip_class = 'PUBLIC'
    ev.url = show_url
    ev.created = created_at
    ev.last_modified = updated_at
    ev.status = cancelled? ? 'CANCELLED' : 'CONFIRMED'
    ev.organizer = h.full_email
    ev.append_custom_property('x-alt-desc', h.markdown(description))
    descfiles.each do |f|
      ev.append_custom_property(
        "attach;filename=#{f}", Icalendar::Values::Uri.new(f.show_path)
      )
    end
    ev
  end

  def to_atom(ev)
    return nil if cancelled?

    #ev.author do |author|
    #  author.name(h.tt(:depname))
    #end
    ev.title(h.markdown(summary, inline: true))
    ev.summary("#{h.date_range(starts(true), ends(true), format: :daylong)}, #{location}")
    ev.content(h.markdown(description), type: 'html')
    ev
  end

  def atom_date
    created_at
  end

  def fullcal_tooltip
    h.tag.h6(
      h.markdown(summary, inline: true), class: cancelled? ? 'cancelled' : ''
    ).html_safe + h.markdown(description)
  end

  def to_fullcal
    classes = cancelled? ? %i[fullcalevent cancelled] : %i[fullcalevent]
    {
      title: summary,
      start: starts(true),
      end: ends(true),
      id: to_slug,
      allDay: !h.has_time(starts(true)),
      url: show_path,
      classNames: classes,
      rendering: cancelled? ? 'background' : '',
      description: h.tag.div(fullcal_tooltip, dir: :auto),
      # https://github.com/fullcalendar/fullcalendar/issues/5533
      display: 'block',
    }
  end

  def has_description?
    description.present? || descfiles.present?
  end

  def self.to_schema
    {
      :@type => 'EducationEvent',
    }
  end

  def to_schema
    @to_schema ||= self.class.to_schema.merge(
      name: h.markdown(to_s, inline: true),
      url:,
      eventStatus: cancelled? ? 'EventCancelled' : 'EventMovedOnline',
      description: h.markdown(description, inline: true),
      image: descfiles.first.try(:url),
      eventAttendanceMode: (try(:online).present? ? 'On' : 'Off') + 'lineEventAttendanceMode',
      startDate: object.starts(true).to_fs(:iso8601),
      endDate: object.ends(true).to_fs(:iso8601),
      location: {
        :@type => 'Place',
        name: location,
        address: {
          :@type => 'PostalAddress',
          streetAddress: location,
        },
      },
    ).compact
  end
end

