# frozen_string_literal: true

require 'auto/core_ext/hash'

class UserDecorator < BaseUserDecorator
  decorates_associations :photo, :photos, :role_events, :roles
  def self.mmname
    User.model_name
  end

  def self.index_path(*args, **opts)
    actions_path([namespace(**opts), model_name.plural, 'path'], *args, **opts)
  end

  def self.chair
    User.chair&.decorate || ''
  end

  def self.administrator
    User.administrator&.decorate || ''
  end

  # return a list of pairs role => person
  def self.with_roles(roles = [], scope = h.policy_scope(User))
    roles.flat_map do |x|
      r = x.is_a?(Role) ? x : Role.find_by(name: x, resource: nil)
      if r
        scope.with_rol(r).map { |y| [r.decorate, y.decorate] }
      else
        []
      end
    end
  end

  def roles_html
    r = relevant_roles
    return '' if r.empty?

    h.tag.ul(r.map do |x|
      h.tag.li(x.linked)
    end.join("\n").html_safe)
  end

  def relevant_roles
    RolePolicy::Scope.new(current_user, object.roles).resolve.decorate.select(&:current?)
  end

  def relevant_role_events
    object.role_events.global.order(created_at: :desc).decorate
  end

  def web
    if webpage == '1'
      return super unless email

      email.include?('@') ? super : "#{h.root_url("~#{email}")}/"
    elsif webpage.blank?
      super
    else
      webpage
    end
  end

  def marks(**_opts)
    ''.html_safe
  end

  def has_webpage?
    webpage.present? && !((webpage == '1') && email.include?('@'))
  end

  def oh_location
    object.oh_location.presence || office
  end

  def all_notes
    leaves.map(&:note)
  end

  def photo_html(...)
    photo.try(:to_html, ...) || ''
  end

  def photo_path(...)
    photo.try(:url, size: :normal)
  end

  def photo_url(...)
    url(photo_path)
  end

  def photo_signed_id
    photo.try(:signed_id)
  end

  def photos_attributes(fields = %i[x y width height signed_id])
    photos.map { |pp| pp.slice(fields).merge(url: pp.url_full) }
  end

  def form_path
    [self.class.pnamespace.to_sym, *path_args, object.becomes(User)].select(&:presence)
  end

  def serialize_attributes
    super + %i[oh_location all_notes web]
  end

  def self.to_schema
    {
      :@type => 'Person',
      affiliation: h.dept_schema,
    }
  end

  def to_schema
    @to_schema ||= self.class.to_schema.merge(
      name: to_s,
      address: oh_location,
      email: email_address,
      familyName: last,
      givenName: first,
      honorificPrefix: h.tt("rank.#{rank || 'norank'}", default: ''),
      jobTitle: status,
      description: h.markdown(interests),
      image: photo_url,
      url: web,
    ).compact
  end

  # the type and id of the list of elements in which this should appear
  def list_id
    'item-list-' + (if alive?
                      emeritus? ? 'Emeriti' : status
                    else
                      'Memoriam'
                    end)
  end

  def list_type
    emeritus? ? '' : 'dt'
  end

  # layout for rendering the item in the list
  def list_layout
    'people/table_row'
  end

  def has_any_field?(fields)
    fields.any? { |x| try(x).present? }
  end

  def has_webids?
    has_any_field? User::WEB_IDS
  end

  def webids_html(**)
    h.dlify(
      User::WEB_IDS.select { |x| try(x) }.to_h { |wid| [wid, wid] },
      key_method: ->(k) { h.tt(k, scope: %i[research webid]).html_safe },
      val_method: ->(k) { webid_html(k) }, **
    )
  end

  def has_research_info?
    has_webids? || has_any_field?(%i[interests research_groups])
  end

  def has_teaching_info?
    has_any_field? :courses
  end

  def webid_html(...)
    wi = webid(...)
    h.render wi if wi
  end

  # XXX doesn't work
  def webid_icon_link(...)
    h.webid(...).try(:icon_link)
  end

  def webid(wid)
    @webid ||= {}
    return @webid[wid] if @webid.key?(wid)
    return @webid[wid] = nil unless (widv = try(wid))

    @webid[wid] ||= h.webid(wid, widv)
  end

  def wid_service(wid)
    @@wid_service ||= {}
    @@wid_service[wid] ||= BaseWebid.service_class(wid)
  end

  def webid_hint(wid)
    res = h.tt("#{wid}_tip", scope: %i[research webid]).html_safe
    return res unless wid_service(wid).readonly? && webid(:pure).present?

    res + h.tag.p(h.tt(:readonly, scope: %i[research webid],
                                  webid: wid_service(wid).tt,
                                  upstream: webid(:pure).id_url).html_safe)
  end

  def webid_generic_link(wid)
    h.link_to(wid_service(wid).tt, wid_service(wid).generic_url.to_s)
  end

  # 'what' is :seminars or :courses
  def menu_items(what)
    res = try(what).current_or_following.by_term.decorate.map(&:as_menu_item)
    res.unshift('|') if res.present?
    res
  end

  def self.retire_path(...)
    actions_path(['retire', sing_model_name, 'path'], ...)
  end

  def retire_path(...)
    return nil unless persisted?

    self.class.retire_path(*spath_args, ...)
  end

  def action_list(*actions, **dopts)
    policy = dopts[:policy] || self.policy
    return super unless (actions.blank? || actions.include?('retire')) &&
                        policy && policy.try(:retire?)

    bare = dopts[:bare]
    res = super(*actions, **dopts, bare: true)
    if dopts[:plain]
      res[:retire] = retire_path
      return res
    end
    ret = h.button_to(
      h.tt('badge.retire'),
      {
        action: :retire,
        controller: '/people/users',
        id:,
      },
      method: :patch,
      title: h.tt('badge.retire_tip'),
      data: { toggle: :tooltip },
      remote: true,
      form: { data: { type: :json } },
      form_class: 'button_to badge badge-success',
    )
    res.push(ret.html_safe)
    if bare
      res
    else
      return '' if res.blank?

      h.tag.span(res.join('|').html_safe, class: %w[actions mx-1])
    end
  end

  def estate
    if retired?
      :retired
    else
      alive? ? :active : :deceased
    end
  end

  def self.inertia_index_params
    super.vdeep_merge(
      only: %i[status web all_notes],
      methods: %i[photo_url webids actions estate],
      include: {
        relevant_roles: RoleDecorator.inertia_index_params,
      },
    ).freeze
  end

  def self.inertia_params
    super.vdeep_merge(
      only: %i[
        first_i18n last_i18n status rank email email_address office phone
        web webpage all_notes start finish deceased hours oh_location
      ] + User::WEB_IDS,
      methods: %i[
        photo_url photo_signed_id menu_item_title 
        webids actions photos_attributes admins
      ],
      include: {
        relevant_roles: RoleDecorator.inertia_index_params,
        relevant_role_events: RoleEventDecorator.inertia_index_params,
      },
    ).freeze
  end
end

