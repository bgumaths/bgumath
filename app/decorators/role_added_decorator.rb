# frozen_string_literal: true

class RoleAddedDecorator < RoleEventDecorator
  protected

  def typed(**opts)
    h.tt(opts[:who] ? 'added_to' : 'added', scope: %i[roles], **opts)
  end
end

