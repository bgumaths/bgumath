# frozen_string_literal: true

class MemberDecorator < AcademicDecorator
  decorates_associations :students, :postdocs, :visitors

  def kamea
    false
  end

  def cur_students
    object.cur_students.decorate
  end

  def past_students
    object.past_students.decorate
  end

  def self.inertia_index_params
    super.vdeep_merge(
      methods: %i[kamea postdoc_ids student_ids visitor_ids],
    ).freeze
  end

  def self.inertia_params
    super.vdeep_merge(
      include: {
        students: StudentDecorator.inertia_index_params,
        postdocs: PostdocDecorator.inertia_index_params,
        visitors: VisitorDecorator.inertia_index_params,
      },
    )
  end
end

