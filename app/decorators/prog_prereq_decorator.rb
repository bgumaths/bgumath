# frozen_string_literal: true

class ProgPrereqDecorator < ShnatonPartDecorator
  delegate_all
  decorates_association :subreqs
  decorates_association :parent
  # gives wrong results for some reason
  #decorates_association :course_lists
  decorates_association :degree

  def course_lists
    object.course_lists.decorate
  end

  def self.mmname
    ProgPrereq.model_name
  end

  def to_short(_loc = I18n.locale)
    ''
  end

  def to_long(_loc = I18n.locale)
    ''
  end

  def to_tooltip
    "#{self} #{to_long}"
  end

  def to_sigma_graph(nodes = {}, edges = {}, layout = nodes.blank?)
    course_lists.each do |cl|
      cl.to_sigma_graph(nodes, edges, false)
    end
    h.layout_graph(nodes, edges.values) if layout

    { nodes: nodes.values, edges: edges.values }
  end

  def degree
    parent ? parent.degree : object.degree&.decorate
  end

  def ac_year
    parent ? parent.ac_year : object.ac_year.decorate
  end

  def duplicate_path(**)
    new_path(dup_obj: id, **)
  end

  def form_path
    [self.class.pnamespace.to_sym, ac_year, :shnaton, degree, object]
  end

  def path_args
    [ac_year, degree]
  end

  def self.nesting
    super.push(:degree)
  end

  def breadcrumb_parent
    parent || super
  end
end

