# frozen_string_literal: true

class IssueDecorator < BaseDecorator
  decorates_associations :course, :staffs, :response

  # can't decorates_associations, see CourseDecorator
  def reporter
    object.reporter.try(:decorate) || object.reporter
  end

  def replier
    response&.creator
  end

  def lecturer
    course.try(:lecturer)
  end

  def student_rep
    course.try(:student_rep)
  end

  def aguda_rep
    course.try(:aguda_rep)
  end

  def departments
    course.try(:departments)
  end

  def self.pnamespace
    'teaching'
  end

  def to_fs(_loc = I18n.locale)
    title
  end

  def self.locale_allowed?(loc = I18n.locale)
    loc.to_sym == :he
  end

  def locale_allowed?(loc = I18n.locale)
    self.class.locale_allowed?(loc)
  end

  # support pre-slug issues
  def slug
    object.slug.presence || id
  end
end

