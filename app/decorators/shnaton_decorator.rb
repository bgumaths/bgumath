# frozen_string_literal: true

class ShnatonDecorator < BaseDecorator
  decorates_association :ac_year
  decorates_association :degrees

  def self.pnamespace
    'teaching'
  end

  def self.singular?
    true
  end

  def to_fs(loc = I18n.locale)
    h.tt(Shnaton, locale: loc) +
      (ac_year ? " #{h.tt(:for, what: ac_year, locale: loc)}" : '')
  end

  # if the year breadcrumb exists, don't include the year
  def breadcrumb_title(loc = I18n.locale)
    ac_year.try(:breadcrumb).present? ? h.tt(Shnaton, locale: loc) : super
  end

  def duplicate_path(**)
    new_path(ac_year: ac_year.year, **)
  end

  def form_path
    [self.class.pnamespace.to_sym, ac_year, object]
  end

  def self.nesting
    [:ac_year]
  end
end

