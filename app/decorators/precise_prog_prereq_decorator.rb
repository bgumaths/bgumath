# frozen_string_literal: true

class PreciseProgPrereqDecorator < ProgPrereqDecorator
  delegate_all

  def to_short(loc = I18n.locale)
    h.tt('teaching.credits',
         count: course_lists.filter_map(&:credits).sum,
         locale: loc)
  end

  alias_method :to_long, :to_short
end

