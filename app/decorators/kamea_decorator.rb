# frozen_string_literal: true

class KameaDecorator < MemberDecorator
  def self.marks(**)
    h.glyphicon('star', '(*)', **)
  end

  delegate :marks, to: :class

  def kamea
    true
  end
end

