# frozen_string_literal: true

class CollectionDecorator < Draper::CollectionDecorator
  delegate :includes, :where, :find, :find_by, :build, to: :object

  def decorate
    self
  end

  def respond_to_missing?(method, ...)
    object.respond_to?(method)
  end

  def method_missing(method, ...)
    res = object.send(method, ...)
    res.try(:decorate) || res
  end
    
end

