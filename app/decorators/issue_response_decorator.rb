# frozen_string_literal: true

class IssueResponseDecorator < BaseDecorator
  

  decorates_association :issue

  # creator gets decorated as a collection, since it responds to :first. So
  # we have to do it manually
  #decorates_association :creator
  def creator
    if object.creator.respond_to?(:decorate)
      object.creator.decorate
    else
      object.creator
    end
  end

  def self.pnamespace
    'teaching'
  end

  def self.locale_allowed?(loc = I18n.locale)
    loc.to_sym == :he
  end

  def locale_allowed?(loc = I18n.locale)
    self.class.locale_allowed?(loc)
  end

  def to_fs(loc = I18n.locale)
    h.tt(object.class, locale: loc) + ' ' +
      h.tt(:for, what: issue.to_s(loc), locale: loc)
  end
end

