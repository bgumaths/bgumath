# frozen_string_literal: true

JS_ACTIONS = %w[destroy undo reset_password].freeze

class BaseDecorator < Draper::Decorator
  def self.mmname
    model_name
  end

  # model name for the sake of path calculations
  def self.pmmname
    mmname
  end

  # model name for translation
  def self.tmmname
    model_name
  end

  def self.gender(**)
    tmmname.human(**, count: nil).try(:[], :gender)
  rescue StandardError
    nil
  end

  def self.the(**opts)
    I18n.t(
      :the, scope:
      [try(:i18n_scope) || :activerecord, :models, tmmname.i18n_key, :typed],
            cascade: true, what: tmmname.human(opts), count: 1, **opts
    )
  end

  delegate :gender, :the, to: :class

  def self.to_trans(key = :what, **opts)
    skey = key.to_s
    okey = tmmname.i18n_key
    scope = [try(:i18n_scope) || :activerecord, :models]
    res = I18n.t(okey, scope:)
    if res.respond_to?(:keys) && res.key?(:typed) && opts.key?(:type)
      scope << okey
      okey = :typed
    end
    what = I18n.t(okey, **opts, scope:, count: 1)
    whats = I18n.t(okey, **opts, scope:, count: 42)
    {
      key.to_sym => what,
      :"the#{skey}" => the(**opts.except(:scope).merge(what:)),
      :"#{skey}s" => whats,
      :"the#{skey}s" =>
        the(**opts.except(:scope).merge(what: whats, count: 42)),
      gender: gender(**opts),
    }.merge(opts[:"#{tmmname.i18n_key}_opts"] || {})
  end

  def to_trans(key = :what, **opts)
    res = self.class.to_trans(key, **opts)
    res[:title] = try(:to_fs, opts[:locale] || I18n.locale)
    res
  end

  delegate_all
  decorates_finders

  def to_s
    to_fs
  end

  # upgrade collection decorators to our own
  def self.collection_decorator_class
    (super == Draper::CollectionDecorator) ? CollectionDecorator : super
  end

  # decorate all associations
  # def self.decorate_all_assoc(cls = self.mmname.name.constantize)
  #   cls.reflect_on_all_assocations.each do |a|
  #     decorates_association a.name if a.klass.method_defined? :decorate
  #   end
  # rescue
  # end

  # decorate_all_assoc

  # symbols for classes where this is nested, eg [:term]
  def self.nesting
    []
  end

  def self.namespace(sep = '_', **opts)
    k = (opts.delete(:nesting) || nesting.count) - nesting.count - 1
    [pnamespace, *nesting[0..k]].select(&:present?).join(sep)
  end

  def namespace(sep = '_', **)
    self.class.namespace(sep, **)
  end

  def self.pnamespace
    nil
  end

  def model_name
    self.class.mmname
  end

  def to_partial_path
    model_name.singular
  end

  def self.sing_model_name(sep = '_', **)
    (namespace(**).present? ? namespace(**) + sep : '') +
      pmmname.singular
  end

  def self.coll_model_name(sep = '_', **)
    return sing_model_name(sep, **) if singular?

    (namespace(**).present? ? namespace(**) + sep : '') +
      pmmname.plural
  end

  def self.model_controller_path(sep = '/')
    ll = (pnamespace ? [pnamespace] : []) + [pmmname.plural]
    sep ? ll.join(sep) : ll
  end

  # the path methods will normally be used to create links, so we use the
  # outer locale

  def self.actions_path(segments, *, **)
    h.in_outer_locale do
      Rails.application.routes.url_helpers.try(
        segments.join('_'), *, locale: I18n.locale, **
      )
    end
  rescue NoMethodError
    # in some cases, in_outer_locale will not be defined
    Rails.application.routes.url_helpers.try(
      segments.join('_'), *, locale: I18n.locale, **
    )
  rescue ActionController::UrlGenerationError
    nil
  end

  def self.index_path(*, **)
    actions_path([coll_model_name(**), 'path'], *, **)
  end

  def self.index_url(*, **)
    actions_path([coll_model_name(**), 'url'], *, **)
  end

  def index_path(...)
    self.class.index_path(*path_args, ...)
  end

  def index_url(...)
    self.class.index_url(*path_args, ...)
  end

  def self.new_path(*, **)
    actions_path(['new', sing_model_name(**), 'path'], *, **)
  end

  def new_path(...)
    self.class.new_path(*path_args, ...)
  end

  # return true if singular resource
  def self.singular?
    false
  end

  def singular?
    self.class.singular?
  end

  def show_path(...)
    return nil unless persisted? || singular?

    self.class.actions_path([self.class.sing_model_name, 'path'],
                            *spath_args, ...)
  end

  def show_url(...)
    return nil unless persisted? || singular?

    self.class.actions_path([self.class.sing_model_name, 'url'],
                            *spath_args, ...)
  end

  def self.edit_path(...)
    actions_path(['edit', sing_model_name, 'path'], ...)
  end

  def edit_path(...)
    return nil unless persisted?

    self.class.edit_path(*spath_args, ...)
  end

  def self.edit_url(...)
    actions_path(['edit', sing_model_name, 'url'], ...)
  end

  def edit_url(...)
    return nil unless persisted?

    self.class.edit_url(*spath_args, ...)
  end

  def destroy_path(...)
    show_path(...)
  end

  def self.gsheets_update_path(...)
    actions_path(['gsheets_update', model_controller_path('_'), 'path'], ...)
  end

  def gsheets_update_path(...)
    self.class.gsheets_update_path(*path_args, ...)
  end

  def web
    object.try(:web) || (persisted? ? show_path : nil)
  end

  def to_id
    try(:to_formatted_s, 'en').try(:parameterize)
  end

  def <=>(other)
    (try(:to_s) || '') <=> (other.try(:to_s) || '')
  end

  def url(addr = web, root = h.root_url(''))
    return addr if addr.blank?

    addr = addr.sub(/^ */, '').sub(/ *$/, '')
    URI(addr).scheme ? addr : (root + addr)
  rescue URI::InvalidURIError
    nil
  end

  def to_html(*, **)
    h.markdown(to_fs(*), inline: true, **).html_safe
  end

  def to_tex(...)
    h.md2tex(to_fs(...))
  end

  def fullname_opts(meth = :to_formatted_s)
    {resource: try(meth), 
     restype: self.class.tmmname.human, therestype: the, }
  end

  def fullname(meth = :to_formatted_s)
    I18n.t('resources.fullname', **fullname_opts(meth)).html_safe
  end

  def the_fullname(meth = :to_formatted_s, show_term = nil)
    tt = try(:term)
    if tt && (show_term || (show_term.nil? && tt != Term.default))
      I18n.t('resources.the_fullname_term', 
             **fullname_opts(meth), term: tt).html_safe
    else
      I18n.t('resources.the_fullname', **fullname_opts(meth)).html_safe
    end
  end

  def linked(str = nil, extra = nil, **)
    str ||= to_html(**)
    str ? h.link_to_if(web, str, web, extra) : ''
  end

  def editable_or_linked
    return linked unless respond_to?(:editable)

    if policy.edit?
      editable
    else
      linked
    end
  end

  ## breadcrumbs
  # normally override only this in subclasses, to call upper breadcrumbs
  def breadcrumb_parent
    try(self.class.nesting[-1] || '')
  end

  def breadcrumb_title(...)
    to_html(...)
  end

  def breadcrumbs(active = true, **)
    (breadcrumb_parent.try(:breadcrumbs, false, **) || '').html_safe +
      breadcrumb(active, **)
  end

  def breadcrumb(active = true, **opts)
    return '' unless policy.show?

    cls = %w[breadcrumb-item] + (opts.delete(:class) || [])
    if active
      cls << :active
      opts = { aria: { current: :page } }.deep_merge(opts)
    end
    h.tag.li(active ? breadcrumb_title : linked(breadcrumb_title), class: cls,
                                                                   **opts)
  end

  def to_breadcrumbs(...)
    h.tag.nav(aria: { label: :breadcrumb }) do
      h.tag.ol(class: :breadcrumb) do
        breadcrumbs(...)
      end
    end
  end

  def spath_args
    singular? ? path_args : [*path_args, object]
  end

  def path_args
    self.class.nesting.map { |n| public_send(n) }
  end

  def form_path
    [self.class.pnamespace&.to_sym, *path_args, object].select(&:presence)
    #[self.class.pnamespace, *path_args,
    #self.becomes(self.class.model_name.name.constantize)]
  end

  def policy
    @policy ||= h.policy(self)
  end

  def current_user
    @current_user ||= policy&.user
  end

  def action_btn(action, **opts)
    btn_cls = opts.delete(:btn_cls) || %w[badge badge-success]
    h.text_or_icon(
      h.tag.span(
        h.tt("badge.#{action}"),
        title: opts[:tooltip] || h.tt("badge.#{action}_tip", what: self),
        class: btn_cls, data: { toggle: 'tooltip' }
      ),
      h.glyphicon(action, opts[:tooltip]),
    )
  end

  def to_html_id
    "#{object.class.name.parameterize}-#{try(:to_slug) || id.to_s}"
  end

  def action_list(*actions, **dopts)
    bare = dopts.delete(:bare)
    # return a hash with path info
    plain = dopts.delete(:plain)
    actions = %w[edit duplicate destroy] if actions.blank?
    result = []
    res = {}
    policy = dopts.delete(:policy) || self.policy

    return '' unless policy

    actions.each do |action|
      if action.respond_to?(:pop)
        path = action.last
        action = action.first
      end
      next unless policy.try("#{action}?")

      path_meth = "#{action}_path"
      path ||= respond_to?(path_meth) ? send(path_meth) : self
      next unless path

      if plain
        res[action] = path
        next
      end

      opts = if JS_ACTIONS.include?(action.to_s)
               {
                 id: "#{action.to_s.tr('_', '-')}-#{to_html_id}",
                 method: :delete,
                 data: {
                   confirm:
                   h.tt(:confirm_action,
                        scope: [self.class.sing_model_name('.'), action],
                        cascade: true,
                        act: I18n.t("actions.to_#{action}",
                                    title: "#{the} *#{self}*")),
                 },
               }
             else
               {}
             end
      lopts = opts.deep_merge(dopts)
      lopts.delete(:icon)

      result.push(
        h.link_to(
          action_btn(action, **dopts),
          path,
          **opts.deep_merge(lopts),
        ),
      )
    end
    if plain
      return res
    end

    if bare
      result.map(&:html_safe)
    else
      return '' if result.blank?

      h.tag.span(result.join('|').html_safe, class: %w[actions mx-1])
    end
  end

  def actions(*acts, **opts)
    action_list(*acts, **opts, plain: true)
  end

  def linked_with_actions(*actions)
    linked + action_list(*actions)
  end

  def linked_admins
    h.list_to_l(admin_users)
  end

  # hash to return via ajax when the form values for this object are required
  def to_form_content(*args, **opts)
    args = h.policy.permitted_attributes if args.blank?
    args += args.pop.keys if args[-1].is_a?(Hash)
    prefix = opts[:prefix] || ''
    res = {}
    args.each do |ff|
      next unless respond_to?(ff)

      val = public_send(ff)
      key = "#{prefix}#{model_name.singular}_#{ff}"
      if val.is_a?(Hash)
        val.each do |k, v|
          res["#{key}_#{k}"] = v
        end
      else
        res[key] = val
      end
    end
    res
  end

  def self.locale_allowed?(_loc = I18n.locale)
    true
  end

  def locale_allowed?(loc = I18n.locale)
    to_fs(loc).presence
  rescue ArgumentError
    # this means we did not redefine to_s, allow locale by default
    true
  rescue NoMethodError
    false
  end

  def self.allowed_locales
    I18n.available_locales.select { |x| locale_allowed?(x) }.map(&:to_s)
  end

  def allowed_locales
    I18n.available_locales.select { |x| locale_allowed?(x) }.map(&:to_s)
  end

  def allowed_locale(loc = I18n.locale)
    locale_allowed?(loc) ? loc : allowed_locales.first
  end

  def in_allowed_locales(meth = :to_fs, **opts)
    locs = opts.delete(:all) ? I18n.available_locales : allowed_locales
    locs.index_with { |ll| I18n.with_locale(ll) { try(meth, **opts) } }
  end

  def to_fs_i18n
    in_allowed_locales
  end

  def to_html_i18n
    in_allowed_locales(:to_html)
  end

  def to_fullname_opts_i18n
    in_allowed_locales(:fullname_opts, all: true)
  end

  # serialization including extra attributes from decorator
  def serialize_attributes
    []
  end

  def attributes
    object.attributes.merge(serialize_attributes.to_h { |x| [x.to_s, nil] })
  end

  def changes_filtered_fields
    %w[password_salt crypted_password persistence_token].freeze
  end

  def changes_ignored_fields
    %w[updated_at].freeze
  end

  def changes(version = nil)
    changeset = version.try(:changeset) || saved_changes
    res = {}
    changeset.each do |k, v|
      next if changes_ignored_fields.include?(k)

      if k =~ /(.*)_id$/
        cls = self.class.reflect_on_association($1.to_sym).klass
        res[$1] = v.map { |id| cls.find_by(id:).try(:decorate) }
      elsif changes_filtered_fields.include?(k)
        res[k] = v.map { |x| x.present? ? '*hidden*' : nil }
      else
        res[k] = v
      end
    end
    res
  end

  def role_title(role_name, ...)
    I18n.t 'admin.roleof', role: role_name, the_fullname: the_fullname(...)
  end

  # displaying this object in a header menu
  def menu_item_title
    to_html(no_wrapper: true)
  end

  # should be valid input to menu_item helper
  def as_menu_item(**_opts)
    [
      menu_item_title,
      show_path,
      {class: %w[bg-success my-1 dropdown-item]}
    ]
  end

  def to_label_i18n
    in_allowed_locales(:to_label)
  end

  def to_hints_i18n
    {}
  end

  def editable
    policy&.edit?
  rescue Pundit::NotDefinedError
    false
  end

  def self.meta
    {
      klass: object_class.name,
    }
  end

  delegate :meta, to: :class

  # we need term to be callable
  def term
    object.try(:term)&.decorate
  end

  # base for inertia params for all actions
  def self.inertia_essential_params
    {
      only: %i[id],
      methods: %i[to_fs_i18n to_param meta],
    }.freeze
  end

  # params about *other* objects needed in forms (e.g., list of all research 
  # groups in user form)
  def self.inertia_form_params
    inertia_essential_params.vdeep_merge(
      methods: %i[to_label_i18n to_hints_i18n],
    ).freeze
  end

  # params for index
  def self.inertia_index_params
    inertia_essential_params.vdeep_merge(
      methods: %i[editable],
    ).freeze
  end

  # params for show
  def self.inertia_params
    inertia_index_params
  end

  # params for edit
  def self.inertia_edit_params
    inertia_params
  end

  delegate :inertia_params, to: :class

  def inertia_json(**opts)
    as_json(inertia_params.vdeep_merge(opts))
  end

  def inertia_edit_json(**opts)
    as_json(self.class.inertia_edit_params.vdeep_merge(opts))
  end

  def inertia_index_json(**opts)
    as_json(self.class.inertia_index_params.vdeep_merge(opts))
  end

  # TODO
  @@to_jsdate = {
    starts: 'ostarts',
    ends: 'oends',
    date: 'odate',
  }
  def self.to_jsdate(field)
    @@to_jsdate[field.to_sym]
  end

  def self.to_jsstr(field)
    "this.#{field}"
  end

  def self.to_jsmodel
    name = object_class.name
    base = object_class.superclass.name
    clss = (name == 'Base') ? {} : { base => true }
    bp = (name == 'Base') ? {} : superclass.inertia_params
    np = inertia_params
    newf = (np[:only] || []) + (np[:methods] || [])
    basf = (bp[:only] || []) + (bp[:methods] || [])
    getters = []
    temporals = {}
    fields = newf - basf
    fields.each do |k|
      getters.push($1) if k =~ /^(.*)_i18n/
      if (jsd = to_jsdate(k))
        temporals[to_jsstr(k)] = jsd
      end
    end
    rels = {}
    hasm = false
    ((np[:include] || {}).keys - (bp[:include] || {}).keys).each do |k|
      next unless (ref = object_class.reflect_on_association(k))

      cls = ref.polymorphic? ? 'Base' : ref.klass.name
      clss[cls] = true
      if ref.macro == :has_many
        rels[k] = "hasMany(#{cls})"
        hasm = true
      else
        rels[k] = cls
      end
    end
    deps = clss.keys
    result = ApplicationController.render(
      template: 'jsmodels/base',
      locals: {
        name:,
        base:,
        rels:,
        fields:,
        getters:,
        temporals:,
        deps:,
        hasm:,
        singular: sing_model_name,
        collection: coll_model_name,
        type: mmname.param_key,
      },
    )
    {content: result, deps:}
  end

  def self.write_jsmodel(fname = "#{object_class.name}.js", **opts)
    opts[:written] ||= { Base: true }
    return if opts[:written][object_class.name.to_sym]

    ffname = "#{opts[:prefix] || 'jsmodels/'}#{fname}"
    res = to_jsmodel
    File.write(ffname, res[:content])
    Rails.logger.info "Written to #{ffname}"
    opts[:written][object_class.name.to_sym] = true
    res[:deps].each do |dep|
      dep.safe_constantize&.decorator_class&.write_jsmodel(**opts)
    end
  end
end

