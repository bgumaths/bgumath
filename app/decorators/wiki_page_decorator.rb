# frozen_string_literal: true

class WikiPageDecorator < BaseDecorator
  def to_fs(*_)
    h.render inline: rtitle
  end

  delegate :url, to: :object

  def action_list(*actions, **dopts)
    dopts = {
      policy: h.policy("#{h.params[:controller]}/#{h.params[:id]}"),
      data: {
        ptitle: rtitle,
        pcontent: markdown,
        toggle: 'modal',
        pageid: url,
        plocale: locale,
      },
    }.deep_merge(dopts)
    destroy_url = begin
      h.url_for(controller: '/pages', action: :destroy,
                pageid: dopts[:data][:pageid])
    rescue ActionController::UrlGenerationError
      h.url_for(controller: '/pages', action: :destroy,
                id: dopts[:data][:pageid],
                pageid: dopts[:data][:pageid])
    rescue StandardError
      nil
    end

    if actions.blank?
      actions = [
        ['edit', '#snippetModal'],
        ['destroy', destroy_url]
      ]
    end
    super(*actions, dopts)
  end

  def html_title_actions(*actions)
    to_html + action_list(*actions)
  end

  def body_html
    h.markdown(h.render(inline: markdown))
  end

  def body_tex
    h.md2tex(h.render(inline: markdown))
  end
end

