# frozen_string_literal: true

class JobDecorator < BaseDecorator
  def self.pnamespace
    'admin'
  end

  def self.sing_model_name(sep = '_')
    @sing_model_name ||= "#{namespace}#{sep}job"
  end

  def self.coll_model_name(sep = '_')
    @coll_model_name ||= "#{namespace}#{sep}jobs"
  end

  def to_fs(*_)
    "#{args[0]['job_class']} ##{id}"
  end

  def locale_allowed?(loc = I18n.locale)
    loc.to_sym == :en
  end

  def web
    nil
  end

  def h_hash
    res = {}
    %w[priority run_at job_class queue error_count args data].each do |ff|
      res[ff.humanize] = try(ff)
    end
    res
  end
end

