# frozen_string_literal: true

class ShnatonPartDecorator < BaseDecorator
  decorates_associations :shnaton

  def self.pnamespace
    'teaching'
  end

  def to_fs(loc = I18n.locale)
    name_i18n[loc] || name
  end

  def locale_allowed?(loc = I18n.locale)
    to_fs(loc.to_s).presence
  end

  def form_path
    [self.class.pnamespace.to_sym, ac_year, :shnaton, object]
  end

  def path_args
    [ac_year]
  end

  def self.nesting
    %i[ac_year shnaton]
  end
end

