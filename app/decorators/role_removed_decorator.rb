# frozen_string_literal: true

class RoleRemovedDecorator < RoleEventDecorator
  protected

  def typed(**opts)
    h.tt(opts[:who] ? 'removed_from' : 'removed', scope: %i[roles], **opts)
  end
end

