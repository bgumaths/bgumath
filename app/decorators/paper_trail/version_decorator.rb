# frozen_string_literal: true

class PaperTrail::VersionDecorator < BaseDecorator
  def self.pnamespace
    'admin'
  end

  def self.sing_model_name(sep = '_')
    @sing_model_name ||= "#{namespace}#{sep}version"
  end

  def self.coll_model_name(sep = '_')
    @coll_model_name ||= "#{namespace}#{sep}versions"
  end

  def undo_path(...)
    show_path(...)
  end

  def to_fs(olink = false)
    # Don't reify, it causes db corruption!
    #obj =
    #  begin
    #    object.try(:reify, dup: true).try(:decorate).try(:to_s)
    #  rescue ArgumentError, NoMethodError, NameError,
    #  ActiveRecord::HasManyThroughNestedAssociationsAreReadonly
    #    nil
    #  end
    #obj = obj ? "'#{obj}'" : "##{item_id}"
    obj = "##{item_id}"
    if olink
      cur = item_type.constantize.find_by(id: item_id)
      if (url = cur.try(:decorate).try(:show_path))
        obj = h.link_to(obj, url)
      end
    end
    "#{event.camelize} #{item_type} #{obj}".html_safe
  end

  def locale_allowed?(loc = I18n.locale)
    loc.to_sym == :en
  end

  def blame(meth = :linked)
    # previously regular user email was saved instead of id, so we try that,
    # but last
    res = User.find_by(id: whodunnit) || IssuesUser.find_by(login: whodunnit) || User.find_by(slug: whodunnit)
    if res.present?
      res = res.try(:decorate).try(meth) || res.try(meth) || res
    else
      logger.warn("  Failed to find whodunnit: #{whodunnit}")
      res = whodunnit
    end
    res
  end

  def orig
    object.reify.try(:decorate)
  end
end

