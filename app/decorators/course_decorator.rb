# frozen_string_literal: true

class CourseDecorator < BaseDecorator
  include Descfiles
  decorates_association :term
  decorates_associations :student_rep, :aguda_rep, :issues_staffs,
                         :departments
  # lecturer gets decorated as a collection, since it responds to :first. So
  # we have to do it manually
  #decorates_association :lecturer
  def lecturer
    if object.lecturer.respond_to?(:decorate)
      object.lecturer.decorate
    else
      object.lecturer
    end
  end

  decorates_association :generic_course

  delegate :marks, :shnaton_url, :shid_linked, :desc,
           to: :generic_course, allow_nil: true

  def self.pnamespace
    'teaching'
  end

  def duplicate_path(**)
    new_path(course_id: course.id, **)
  end

  def to_fs(loc = I18n.locale)
    title(loc)
  end

  def title(loc = I18n.locale)
    res = title_i18n[loc.to_s]
    res.presence || generic_course.try(:to_fs, loc)
  end

  def some_info_i18n
    in_allowed_locales(:some_info)
  end

  def with_shid(...)
    [to_fs(...), shid].select(&:present?).join(', ')
  end

  def to_label(term = self.term, meth = :to_html, **opts) # for simple_form
    if term.nil?
      term = (self.term == Term.current) ? false : self.term
    end
    # for default term, allow specifying meth by an option
    meth = opts[:meth] || meth
    (
      ((meth.to_s == 'to_s') ? with_shid : h.markdown(with_shid, inline: true)) +
        (term ? ", #{term.public_send(meth)}" : '').html_safe
    ).html_safe
  end

  def linked_with_id(term = nil)
    linked(h.markdown(to_label(term), inline: true))
  end

  def linked_lecturer(loc = I18n.locale)
    return '' unless lecturer

    lecturer.linked(lecturer.to_fs(loc))
  end

  def self.nesting
    [:term]
  end

  def serialize_attributes
    [:shnaton_url]
  end

  def self.to_schema
    { :@type => 'CourseInstance' }
  end

  def to_schema
    @to_schema ||= self.class.to_schema.deep_merge(
      courseMode: :onsite,
      instructor: lecturer.to_schema,
      name: to_s,
      url: web,
      description: content,
      location: hours,
      startDate: term.starts,
      endDate: term.ends,
    ).compact
  end

  def menu_item_title
    if current?
      super
    else
      super + " #{term.to_html(allowed_locale, no_wrapper: true)}"
    end
  end

  def self.inertia_index_params
    super.vdeep_merge(
      only: %i[web graduate],
      methods: %i[some_info_i18n],
      include: {
        lecturer: UserDecorator.inertia_index_params,
        generic_course: GenericCourseDecorator.inertia_index_params,
      },
    ).freeze
  end

  def avail_terms
    object.avail_terms.decorate
  end

  def self.inertia_params
    super.vdeep_merge(
      only: %i[hours],
      methods: %i[abstract_i18n],
      include: {
        term: TermDecorator.inertia_index_params,
        avail_terms: TermDecorator.inertia_essential_params,
        student_rep: IssuesStudentRepDecorator.inertia_essential_params,
      },
    ).freeze
  end

  def self.inertia_edit_params
    super.vdeep_merge(
      only: %i[term_id generic_course_id lecturer_id student_rep_id],
      methods: %i[title_i18n content_i18n],
    )
  end
end

