# frozen_string_literal: true

class TermPlanDecorator < BaseDecorator
  decorates_association :degree_plan
  decorates_association :plan_items
  decorates_association :free_plan_items
  decorates_association :generic_courses

  def self.pnamespace
    'teaching'
  end

  def to_fs(loc = I18n.locale)
    I18n.with_locale(loc) do
      h.tt("teaching.term.name.#{term}",
           year: h.t('year', which: h.ordinalise(year, he: true)))
    end
  end

  def cred_range
    h.bidi_range(min_credits, max_credits)
  end

  def form_path
    [self.class.pnamespace.to_sym, ac_year, :shnaton, degree, degree_plan, object]
  end

  def path_args
    [ac_year, degree, degree_plan]
  end

  def self.nesting
    %i[ac_year shnaton degree degree_plan]
  end
end

