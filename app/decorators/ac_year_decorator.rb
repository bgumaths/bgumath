# frozen_string_literal: true

class AcYearDecorator < BaseDecorator
  decorates_association :shnaton

  def to_fs(loc = I18n.locale)
    if year
      h.tt('ac_year',
           year: "#{year - 1}–#{year}", locale: loc)
    else
      model_name.human
    end
  end

  def to_label
    to_s
  end
end

