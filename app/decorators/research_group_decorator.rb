# frozen_string_literal: true

class ResearchGroupDecorator < BaseDecorator
  #decorates_association :members
  decorates_association :seminars

  def self.pnamespace
    'research'
  end

  def to_fs(_loc = I18n.locale)
    name || ''
  end

  def locale_allowed?(loc = I18n.locale)
    loc.to_sym == :en
  end

  # living members
  def lmembers
    object.members.around.decorate
  end

  def members
    object.members.decorate
  end

  def cseminars
    object.seminars.this_term.decorate
  end

  def self.inertia_index_params
    super.vdeep_merge(
      only: %i[name],
    ).freeze
  end

  def self.inertia_params
    super.vdeep_merge(
      only: %i[description],
      methods: %i[member_ids seminar_ids],
      include: {
        members: AcademicDecorator.inertia_index_params,
        seminars: SeminarDecorator.inertia_index_params.vdeep_merge(
          include: {
            term: TermDecorator.inertia_index_params,
          },
        ),
      },
    ).freeze
  end
end

