# frozen_string_literal: true

class SeminarDecorator < BaseDecorator
  decorates_associations :research_groups, :meetings, :term
  include Online

  def self.pnamespace
    'research'
  end

  def duplicate_path(**opts)
    new_path(**{ seminar: { term_id: Term.next_or_cur_id },
                 dup_obj: slug, }.deep_merge(opts))
  end

  def to_fs(loc = I18n.locale)
    name_i18n[loc] || name || ''
  end

  def to_label(term = nil)
    if term.nil?
      term = (self.term == Term.current) ? false : self.term
    end
    res = to_s
    res += ", #{term}" if term
    res
  end

  def locale_allowed?(loc = I18n.locale)
    name_i18n[loc.to_s].presence
  end

  #def day(loc = I18n.locale)
  #  I18n.t('date.day_names', locale: loc)[object.day]
  #end

  def fullname(meth = :to_s)
    if name_i18n['en'].casecmp('colloquium').zero?
      public_send(meth)
    else
      super
    end
  end

  def place(format = :html, loc = I18n.locale)
    (I18n.t("place.#{room}_#{format}",
            default: h.markdown(room, inline: true, format:),
            locale: loc) || '')
      .html_safe
  end

  def meetings_path(**)
    h.research_seminar_meetings_path(self, **)
  end

  def meetings_url(**)
    h.research_seminar_meetings_url(self, **)
  end

  def ical_url(**)
    meetings_url(format: :ics, protocol: 'webcal', **)
  end

  def feed_url(**)
    meetings_url(format: :atom, **)
  end

  def new_meeting_path(**opts)
    h.in_outer_locale do
      h.new_research_term_seminar_meeting_path(*spath_args, **opts)
    end
  end

  def ml_path
    return nil if list_id.blank?

    URI(list_id).scheme ? list_id : h.ml_domain + list_id
  rescue StandardError
    nil
  end

  def self.nesting
    [:term]
  end

  def serialize_attributes
    %i[ml_path place day fullname]
  end

  class << self
    @@bgcolours = %w[
      23164E
      B1365F
      0F4B38
      AB8B00
      875509
      5F6B02
      113F47
      8D6F47
      711616
      125A12
    ]
    @@colour = []

    def colour(coli)
      @@colour[coli] ||= (@@bgcolours[coli % @@bgcolours.count].to_i(16) +
                          (1500 * coli)).to_s(16).rjust(6, '0')
    end
  end

  def colour
    self.class.colour(id)
  end

  def to_fullcal(**opts)
    { events: meetings.map(&:to_fullcal), backgroundColor: "##{colour}",
      **opts, }
  end

  def role_title(role_name, ...)
    if role_name == I18n.t('roles.admin')
      super(I18n.t('roles.organiser'), ...)
    else
      super
    end
  end

  def menu_item_title
    if current?
      super
    else
      super + " #{term.to_html(allowed_locale, no_wrapper: true)}"
    end
  end

  def starts
    object.starts&.to_formatted_s(:time)
  end

  def ends
    object.ends&.to_formatted_s(:time)
  end

  def term_id
    term.slug
  end

  def self.inertia_index_params
    super.vdeep_merge(
      only: %i[name_i18n description_i18n room day slug],
      methods: %i[term_id starts ends],
    ).freeze
  end

  def avail_terms
    object.avail_terms.decorate
  end

  def meeting_dates
    meetings.select(&:to_param).map { |m| m.date.strftime('%Y/%m/%d') }
  end

  def self.to_jsstr(...)
    "`1900-01-01T${#{super}}`"
  end

  def self.inertia_params
    super.vdeep_merge(
      include: {
        term: TermDecorator.inertia_index_params,
        meetings: MeetingDecorator.inertia_index_params,
        avail_terms: TermDecorator.inertia_essential_params,
      },
    ).freeze
  end
end

