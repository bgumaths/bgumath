# frozen_string_literal: true

class AcademicDecorator < UserDecorator
  decorates_associations :research_groups, :courses

  def current_courses
    h.policy_scope(object.courses).this_term&.decorate
  end

  def self.inertia_index_params
    super.vdeep_merge(
      only: %i[interests phd_year phd_from],
      methods: %i[research_group_ids course_ids],
    )
  end

  def self.inertia_params
    super.vdeep_merge(
      include: {
        research_groups: ResearchGroupDecorator.inertia_index_params,
        current_courses: CourseDecorator.inertia_index_params,
      },
    ).freeze
  end
end

