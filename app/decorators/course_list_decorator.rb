# frozen_string_literal: true

class CourseListDecorator < ShnatonPartDecorator
  decorates_associations :courses
  decorates_associations :prereqs

  def to_sigma_graph(nodes = {}, edges = {}, layout = nodes.blank?)
    courses.each do |cc|
      cc.to_sigma_graph(nodes, edges, false, ->(obj) {
        obj.children.object.active_in(ac_year.year).organic.decorate
      })
    end
    h.layout_graph(nodes, edges.values) if layout

    { nodes: nodes.values, edges: edges.values }
  end
end

