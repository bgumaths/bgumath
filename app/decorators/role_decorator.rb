# frozen_string_literal: true

class RoleDecorator < BaseDecorator
  def self.pnamespace
    'admin'
  end

  decorates_associations :users, :events

  # visibility options for menu
  def self.visibility_collection
    object_class.visibilities.keys.reject { |x| x == 'auto' }.map do |x|
      [h.cap(x), x, {
        title: h.t(x, scope: %w[roles visibility_hints]),
        data: { toggle: :tooltip },
      }]
    end
  end

  def email_address(with_name = false)
    if object.email
      require 'mail'
      res = Mail::Address.new h.full_email(object.email)
      res.display_name = to_fs(:en, true) if with_name
      res.format
    else
      users.active.first.try(:email_address, with_name)
    end
  rescue Mail::Field::ParseError => e
    logger.warn("role email_address: #{e.message}")
    h.full_email(object.email)
  end

  def resource
    res = begin
      object.try(:resource)
    rescue StandardError
      nil
    end
    res.try(:decorate) || res
  end

  def to_fs(loc = nil, show_term = nil, **opts)
    loc ||= opts[:locale] || I18n.locale
    s = title_i18n.try('[]', loc.to_s)
    if s.blank?
      tid = (title || name).try(:parameterize, separator: '_')
      s = I18n.t("roles.#{tid}", default: '')
    end
    s = title if s.blank?
    s = name if s.blank?
    if resource && opts[:resource]
      meth = opts[:method] || 'to_s'
      resource.role_title(s, meth, show_term).html_safe
    else
      s
    end
  end

  def to_label
    linked + 
      " [#{h.tag.em(h.tt(visibility, 
                         scope: %i[roles visibility],
                         default: visibility.to_s))}]".html_safe
  end

  def locale_allowed?(loc = I18n.locale)
    title_i18n[loc.to_s].presence || loc.to_sym == :en
  end

  def linked(str = self, _extra = nil, **dopts)
    return '' unless str

    opts = {data: { toggle: :tooltip}, title: description}.deep_merge(dopts)
    (h.policy(self).show? && resource.blank?) ? super(str, opts) : h.tag.span(str.to_fs(method: :linked), **opts)
  end

  def to_hints
    description_i18n
  end

  def self.inertia_index_params
    super.vdeep_merge(
      include: {
        resource: BaseDecorator.inertia_index_params.vdeep_merge(
          methods: %i[to_fullname_opts_i18n],
        ),
      },
    ).freeze
  end
end

