# frozen_string_literal: true

class FreePlanItemDecorator < ShnatonPartDecorator
  decorates_association :term_plan

  def to_fs(loc = I18n.locale)
    title_i18n[loc] || title
  end

  def cred_range
    h.bidi_range(minp, maxp)
  end

  def path_args
    [ac_year, degree]
  end

  def self.nesting
    %i[ac_year shnaton degree]
  end
end

