# frozen_string_literal: true

class IssuesUserDecorator < BaseUserDecorator
  decorates_associations :courses
  def self.pnamespace
    'teaching'
  end

  def self.pmmname
    IssuesUser.model_name
  end

  def to_fs(_loc = I18n.locale)
    object.fullname || object.login
  end

  def locale_allowed?(loc = I18n.locale)
    loc.to_s == 'he'
  end

  def to_label
    to_s # + ', ' + h.tt(object.class, count: 1, locale: :he)
  end

  def to_html(...)
    res = super
    classes = []
    classes << :pass_change unless pass_changed?
    classes << :user_disabled unless active
    return res if classes.blank?

    h.tag.span(
      res, class: classes, data: { toggle: :tooltip },
           title: classes.map { |cls| h.tt("session.#{cls}") }.join(', ')
    )
  end

  def action_list(*actions, **dopts)
    if actions.blank?
      actions = %w[edit destroy]
      if persisted?
        actions.push(
          ['reset_password', h.reset_password_teaching_issues_user_path(self)],
        )
      end
    end
    super
  end

  def email
    login
  end
end

