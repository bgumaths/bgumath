# frozen_string_literal: true

class AdminDecorator < UserDecorator
  def list_layout
    nil
  end
end

