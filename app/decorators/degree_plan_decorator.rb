# frozen_string_literal: true

class DegreePlanDecorator < BaseDecorator
  decorates_association :degree
  decorates_association :term_plans

  def self.pnamespace
    'teaching'
  end

  def to_fs(loc = I18n.locale)
    name_i18n[loc] || name
  end

  def new_term_path(**opts)
    h.in_outer_locale do
      h.new_teaching_ac_year_shnaton_degree_degree_plan_term_plan_path(
        try(:ac_year), degree, self, term_plan: next_term, **opts
      )
    end
  end

  def duplicate_path(**)
    new_path(degree_plan_id: id, **)
  end

  def form_path
    [self.class.pnamespace.to_sym, ac_year, :shnaton, degree, object]
  end

  def path_args
    [ac_year, degree]
  end

  def self.nesting
    %i[ac_year shnaton degree]
  end
end

