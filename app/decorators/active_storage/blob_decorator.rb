# frozen_string_literal: true

class ActiveStorage::BlobDecorator < BaseDecorator
  delegate_all
  decorates_finders

  decorates_association :attachments

  def to_fs(*_)
    filename.to_s
  end

  def show_path
    h.url_for(self)
  end

  def image_tag(**opts)
    h.image_tag(representation(IMG_TRANS[opts.delete(:size) || :large]),
                class: %w[img-fluid], **opts)
  rescue ActiveStorage::UnrepresentableError
    '???'
  end

  def to_html(**)
    image_tag(size: :normal, class: %w[rounded img-fluid img-thumbnail],
              **)
  end

  def linked(str = nil, **opts)
    str ||= to_html(**(opts.delete(:img_opts) || {}))
    h.link_to(str, web, class: %w[img-link], **opts)
  end

  def thumb_url(size: :normal)
    representation(IMG_TRANS[size]).processed.url
  rescue ActiveStorage::FileNotFoundError => e
    logger.warn("Blob file not found for #{self}: #{e}")
  end

  def action_btn(action, **opts)
    h.tag.span(
      class: %w[border-top border-left border-warning rounded-left] <<
      "#{action}-button",
      data: { signed_id: },
    ) do
      h.glyphicon(action,
                  opts[:tooltip] || h.tt("badge.#{action}_tip", what: self),
                  icon_opts: {size: '2x'}.deep_merge(opts[:icon] || {}))
    end
  end

  # destroy only the element, and remove it from the attach list (via js)
  def destroy_btn(**)
    h.tag.a(action_btn(:destroy, class: %w[d-inline-flex], **),
            class: %w[destroy-attach])
  end

  def to_partial_path
    'active_storage/blob'
  end
end

