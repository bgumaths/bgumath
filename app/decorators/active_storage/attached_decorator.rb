# frozen_string_literal: true

class ActiveStorage::AttachedDecorator < Draper::Decorator
  decorates_finders
  
  def self.collection_decorator_class
    CollectionDecorator
  end

  # don't use delegate_all since a method might be missing from the object, 
  # and it is then passed to its attachments, without being decorated
  def respond_to_missing?(method, *)
    del_object(method).respond_to?(method)
  end

  def method_missing(method, ...)
    del_object(method).send(method, ...)
  end
    

  delegate :model_name, to: ActiveStorage::Attachment

  # the following will not have any effect in the "many" case, because of 
  # https://github.com/rails/rails/issues/37883. The logic for dealing with 
  # that bit is anyway more natural in AttachmentPreviewInput
  def to_partial_path
    object.class.to_s.underscore
  end

  protected

  # to which object is method delegated?
  def del_object(method)
    object.methods.include?(method) ? object : public_send(self.class.attached_name)
  end
    
end

