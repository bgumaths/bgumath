# frozen_string_literal: true

class ActiveStorage::AttachmentDecorator < BaseDecorator
  delegate_all
  decorates_finders

  decorates_association :record
  decorates_association :blob

  delegate :policy, :destroy_path, to: :record
  delegate :image_tag, :show_path, :to_html, :linked, :to_s, 
           :thumb_url, :action_btn, to: :blob

  def destroy_btn(**opts)
    return blob.destroy_btn(**opts) if destroy_path.blank?

    action_list(:destroy, **{
      bare: true, class: %w[destroy-attach],
      data: { remote: true, params: {attachment_id: id}.to_param },
    }.deep_merge(opts)).first || ''
  end

  def to_partial_path
    'active_storage/blob'
  end
end

