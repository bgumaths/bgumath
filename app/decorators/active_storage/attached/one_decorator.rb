# frozen_string_literal: true

require 'active_storage/attached'

class ActiveStorage::Attached::OneDecorator < ActiveStorage::AttachedDecorator
  def self.attached_name
    :attachment
  end

  decorates_association attached_name
end

