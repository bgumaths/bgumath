# frozen_string_literal: true

require 'active_storage/attached'

class ActiveStorage::Attached::ManyDecorator < ActiveStorage::AttachedDecorator
  def self.attached_name
    :attachments
  end

  decorates_association attached_name

  def as_tabs(**opts)
    h.embed(self, opts)
  end
end

