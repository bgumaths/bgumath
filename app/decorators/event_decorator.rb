# frozen_string_literal: true

class EventDecorator < BaseEventDecorator
  def self.pnamespace
    'research'
  end

  def to_pagehtml(centre_link = nil, centre_text = h.tt('research.center'))
    res = linked
    res += h.link_to(h.glyphicon(:star, '(*)', centre_text), centre_link) if centre && centre_link
    res += h.tag.span(class: %w[event-coords]) do
      rr = ' '.html_safe + h.tag.em(duration_text[:en])
      rr += ', '.html_safe + h.tag.strong(location) if location.present?
      "#{rr}."
    end
    res.html_safe
  end

  # for events, always use the full url
  def web
    object.try(:web) || (persisted? ? show_url(locale: nil) : nil)
  end

  def fullcal_tooltip
    h.tag.h5(h.tt(Event, count: 1)) + super
  end

  def expires(...)
    object.expires(...)&.strftime('%Y-%m-%d')
  end

  def starts(...)
    object.starts(...)&.strftime('%Y-%m-%d %H:%M')
  end

  def ends(...)
    object.ends(...)&.strftime('%Y-%m-%d %H:%M')
  end

  class << self
    def colour
      '#23164E'
    end

    def to_fullcal(**opts)
      { events: all.map(&:to_fullcal), backgroundColor: colour, **opts }
    end

    def inertia_index_params
      super.vdeep_merge(
        only: %i[web location description],
        methods: %i[starts ends],
      ).freeze
    end

    def to_jsdate(field)
      (field.to_s == 'expires') ? 'oexpires' : super
    end

    def inertia_params
      super.vdeep_merge(
        only: %i[centre title],
        methods: %i[expires],
      ).freeze
    end

    def inertia_edit_params
      super.vdeep_merge(
        only: %i[slug],
      ).freeze
    end
  end
end

