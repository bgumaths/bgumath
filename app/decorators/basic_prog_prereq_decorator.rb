# frozen_string_literal: true

class BasicProgPrereqDecorator < ProgPrereqDecorator
  delegate_all
  decorates_association :course_list

  def to_short(loc = I18n.locale)
    res = []
    if maxp
      res.push(
        I18n.t('teaching.credits',
               count: h.bidi_range(minp || 0, maxp), locale: loc),
      )
    elsif minp&.positive?
      res.push(I18n.t('teaching.credits',
                      count: I18n.t(:atleast, count: minp, locale: loc),
                      locale: loc))
    end
    res.join(', ').html_safe
  end

  def courses_str(loc = I18n.locale)
    res = ''
    if minc.present? && minc.positive?
      res = I18n.t(:atleast, count: minc, locale: loc)
    end
    if maxc.present?
      maxcs = I18n.t(:atmost, count: maxc, locale: loc)
      if res.present?
        if minc == maxc
          res = I18n.t(:precisely, count: minc, locale: loc)
        else
          res += I18n.t(:and, what: maxcs, locale: loc)
        end
      else
        res = maxcs
      end
    end
    res = I18n.t('teaching.ncourses', scount: res, count: [minc || 0, maxc || 0].max, locale: loc) if res.present?
    res.html_safe
  end

  def credits_str(loc = I18n.locale)
    res = ''
    if minp.present? && minp.positive?
      res = I18n.t(:atleast, count: minp, locale: loc)
    end
    if maxp.present?
      maxcs = I18n.t(:atmost, count: maxp, locale: loc)
      if res.present?
        if minp == maxp
          res = I18n.t(:precisely, count: minp, locale: loc)
        else
          res += I18n.t(:and, what: maxcs, locale: loc)
        end
      else
        res = maxcs
      end
    end
    res = I18n.t('teaching.credits', count: res, locale: loc) if res.present?
    res.html_safe
  end

  def to_long(str = {}, loc = I18n.locale)
    res = [courses_str(loc), credits_str(loc)].select(&:present?).join(', ')
    if str[loc.to_sym].present? && res.present?
      res += " #{str[loc.to_sym]}"
    end
    res.html_safe
  end
end

