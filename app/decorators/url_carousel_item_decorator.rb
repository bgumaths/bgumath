# frozen_string_literal: true

class UrlCarouselItemDecorator < CarouselItemDecorator
  def to_fs(_loc = I18n.locale)
    "URL: #{url}"
  end

  def web
    object.url
  end
end

