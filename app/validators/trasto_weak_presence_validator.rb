# frozen_string_literal: true

# validate that at least on locale is present
class TrastoWeakPresenceValidator < ActiveModel::EachValidator
  def validate_each(record, attr, value)
    locs = options[:locales] || I18n.available_locales
    if value.respond_to?(:has_key?)
      unless locs.count { |x| value[x.to_s].present? }.positive?
        record.errors.add(
          attr, :blank,
          message: "#{attr_name(record, attr)} must be non-blank in some locale"
        )
      end
    else
      record.errors.add(attr, :invalid, message: 'must be a hash')
    end
  end

  def attr_name(record, attr)
    if (ns = record.try(:namespace))
      ns += '.'
    else
      ns = ''
    end
    at = attr.to_s.sub(/.i18n$/, '')
    I18n.t("#{ns}#{record.model_name.param_key}!#{at}", default: '').presence || at
  end
end

