# frozen_string_literal: true

class TrastoPresenceValidator < ActiveModel::EachValidator
  def validate_each(record, attr, value)
    locs = options[:locales] || I18n.available_locales
    if value.respond_to?(:has_key?)
      locs.each do |loc|
        next if value[loc.to_s].present?

        record.errors.add(
          attr,
          :blank,
          message: "#{attr_name(record, attr, loc)} must not be blank",
        )
      end
    else
      record.errors.add(attr, :invalid, message: 'must be a hash')
    end
  end

  def attr_name(record, attr, loc)
    if (ns = record.try(:namespace))
      ns += '.'
    else
      ns = ''
    end
    I18n.t(
      "#{ns}#{record.model_name.param_key}!#{attr.to_s.sub(/.i18n$/, '')}",
      locale: loc,
      default: '',
    ).presence || "locale '#{loc}'"
  end
end

