# frozen_string_literal: true

class FrozenValidator < ActiveModel::EachValidator
  def validate_each(record, attr, _value)
    return unless record.persisted?

    return unless record.attribute_changed?(attr)

    record.errors.add(attr, :frozen)
    
  end
end

